import React from "react";
import Category from "@theme-original/DocSidebarItem/Category";

export default function CategoryWrapper(props) {
    if (
        props.item &&
        props.item.items &&
        props.item.items.length > 0 &&
        !props.item.href
    ) {
        props.item.href = props.item.items[0].href + "#";
    }

    return (
        <>
            <Category {...props} />
        </>
    );
}
