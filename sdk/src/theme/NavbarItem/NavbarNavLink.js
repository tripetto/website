import React from "react";
import NavbarNavLink from "@theme-original/NavbarItem/NavbarNavLink";

export default function NavbarNavLinkWrapper(props) {
    if (props.href && props.href.indexOf("https://tripetto.com/sdk/") === 0) {
       props = { ...props, target: undefined, rel: undefined, className: props.className + " navbar__link_internal" };
    }

    return (
        <>
            <NavbarNavLink {...props} />
        </>
    );
}
