import React from "react";
import useBaseUrl from "@docusaurus/useBaseUrl";

export default function Preview({ src, spacer, width }) {
    return (
        <div style={{
            display: "inline-block",
            padding: "40px",
            borderRadius: "6px",
            backgroundColor: "white",
            backgroundImage: "linear-gradient(45deg, rgba(0,0,0,0.05) 25%, transparent 25%),linear-gradient(135deg, rgba(0,0,0,0.05) 25%, transparent 25%),linear-gradient(45deg, transparent 75%, rgba(0,0,0,0.05) 75%),linear-gradient(135deg, transparent 75%, rgba(0,0,0,0.05) 25%)",
            backgroundSize: "20px 20px",
            backgroundPosition: "0 0, 10px 0, 10px -10px, 0px 10px",
            boxShadow: "rgba(0, 0, 0, 0.1) 0px 1px 2px 0px",
            marginBottom: spacer && "16px" || undefined
        }}><img src={useBaseUrl(`/img/${src}`)} width={width || "400"} /></div>
    )
}
