import React from "react";
import useBaseUrl from "@docusaurus/useBaseUrl";

const Readonly = () => (
  <span
    style={{
      backgroundColor: "rgba(0,0,0,0.3)",
      color: "#fff",
      borderRadius: "4px",
      textTransform: "uppercase",
      fontFamily: "Tahoma,Arial",
      fontSize: "9px",
      padding: "2px 4px",
      position: "relative",
      top: "-1px",
      marginLeft: "5px",
      cursor: "help",
    }}
    title="ℹ️ You can only read this property and not write to it after the object is created."
  >
    Readonly
  </span>
);

const Optional = () => (
  <span
    style={{
      backgroundColor: "#007bff",
      color: "#fff",
      borderRadius: "4px",
      textTransform: "uppercase",
      fontFamily: "Tahoma,Arial",
      fontSize: "9px",
      padding: "2px 4px",
      position: "relative",
      top: "-1px",
      marginLeft: "5px",
      cursor: "help",
    }}
    title="ℹ️ This property is optional and can therefore be `undefined` (or left out when declaring a new object)."
  >
    Optional
  </span>
);

const Event = () => (
  <span
    style={{
      backgroundColor: "#e5b335",
      color: "#fff",
      borderRadius: "4px",
      textTransform: "uppercase",
      fontFamily: "Tahoma,Arial",
      fontSize: "9px",
      padding: "2px 4px",
      position: "relative",
      top: "-1px",
      marginLeft: "5px",
      cursor: "help",
    }}
    title="ℹ️ This property is an event you can listen to."
  >
    Event
  </span>
);

const Function = () => (
  <span
    style={{
      backgroundColor: "#e06b6b",
      color: "#fff",
      borderRadius: "4px",
      textTransform: "uppercase",
      fontFamily: "Tahoma,Arial",
      fontSize: "9px",
      padding: "2px 4px",
      position: "relative",
      top: "-1px",
      marginLeft: "5px",
      cursor: "help",
    }}
    title="ℹ️ This property is a callable function."
  >
    Function
  </span>
);

const Deprecated = () => (
  <span
    style={{
      backgroundColor: "#f00",
      color: "#fff",
      borderRadius: "4px",
      textTransform: "uppercase",
      fontFamily: "Tahoma,Arial",
      fontSize: "9px",
      padding: "2px 4px",
      position: "relative",
      top: "-1px",
      marginLeft: "5px",
      cursor: "help",
    }}
    title="ℹ️ This property is deprecated and here for legacy purposes. Do not use in new code!"
  >
    Deprecated
  </span>
);

function parseTypes(type, symbols, baseUrl) {
  if (!symbols) {
    return type;
  }

  const types = [];
  const findNextSymbol = (s) => {
    let n = -1;
    let l = 0;

    Object.keys(symbols)
      .sort((a, b) => b.length - a.length)
      .forEach((symbol) => {
        const i = type.indexOf(symbol);

        if (i !== -1 && (n === -1 || i < n)) {
          n = i;
          l = symbol.length;
        }
      });

    return n !== -1
      ? {
          from: n,
          to: n + l,
        }
      : undefined;
  };

  let nextSymbol = findNextSymbol(type);

  while (nextSymbol) {
    if (nextSymbol.from > 0) {
      types.push(type.substring(0, nextSymbol.from));
    }

    types.push(type.substring(nextSymbol.from, nextSymbol.to));

    type = type.substring(nextSymbol.to);
    nextSymbol = findNextSymbol(type);
  }

  types.push(type);

  return (
    (types.length > 0 &&
      types.map((type, index) => (
        <React.Fragment key={index}>
          {symbols[type] ? (
            <a
              href={
                symbols[type].indexOf("/") === 0
                  ? baseUrl + symbols[type]
                  : symbols[type]
              }
              target={
                (symbols[type].indexOf("https://") === 0 && "_blank") ||
                undefined
              }
            >
              {type}
            </a>
          ) : (
            type
          )}
        </React.Fragment>
      ))) ||
    ""
  );
}

function parseDescription(description) {
  let value = "";

  if (
    description.nextElementSibling &&
    description.nextElementSibling.tagName === "UL" &&
    description.nextElementSibling.childElementCount > 0
  ) {
    let li = description.nextElementSibling.firstElementChild;

    while (li) {
      if (li.tagName === "LI" && li.textContent) {
        value += `\n- ${li.textContent}`;
      }

      li = li.nextElementSibling;
    }
  }

  return value;
}

export default function InterfaceDeclaration({ src, prefix, symbols }) {
  const baseUrl = useBaseUrl("/docs");
  const interfaceDeclaration = React.useMemo(() => {
    if (
      src.indexOf("interface ") === 0 &&
      src.indexOf(" {") > 10 &&
      src.lastIndexOf("}") === src.length - 1
    ) {
      const name = src.substring(10, src.indexOf(" {"));
      const fields = [];
      let hasIndex = false;
      let offset = 2;

      src
        .substring(10 + name.length + 3, src.length - 2)
        .split("\n")
        .forEach((row) => {
          if (!hasIndex && row.substr(0, 2) === "  " && row.charAt(2) === "[" && !fields.length) {
            hasIndex = row;
            offset = 4;
          } else if (row.substr(0, offset) === (offset === 4 ? "    " : "  ")) {
            const deprecated = row.indexOf("🗑️") !== -1;

            if (
              row.charAt(offset) === " " ||
              row.charAt(offset) === "}" ||
              row.charAt(offset) === ")"
            ) {
              if (fields.length > 0) {
                fields[fields.length - 1].type +=
                  "\n" + row.replace(" 🗑️", "").replace("🗑️", "");

                if (deprecated) {
                  fields[fields.length - 1].deprecated = true;
                }
              }
            } else {
              const readOnly = row.indexOf("readonly ") === offset;
              const name = row.substring(
                offset + (readOnly ? 9 : 0),
                row.indexOf(":")
              );
              const type = row
                .substr(row.indexOf(": ") + 2)
                .replace(" 🗑️", "")
                .replace("🗑️", "");
              const optional = name.charAt(name.length - 1) === "?";
              const isFunction =
                (type.indexOf(") => ") !== -1 &&
                  (type.indexOf("(") === 0 || type.indexOf("<") === 0)) ||
                type.indexOf("//@function") !== -1;
              const isEvent =
                name.indexOf("on") === 0 &&
                name.charAt(2) === name.charAt(2).toUpperCase() &&
                type.indexOf("//@function") === -1;

              fields.push({
                name: optional ? name.substr(0, name.length - 1) : name,
                type: type
                  .replace(" //@function", "")
                  .replace("//@function", ""),
                optional,
                readOnly,
                deprecated,
                isFunction: isFunction && !isEvent,
                isEvent,
              });
            }
          }
        });

      return (
        <>
          <pre>
            <span
              style={{
                opacity: 0.6,
              }}
            >
              interface
            </span>{" "}
            {name.split(" extends ").map((part, index) =>
              index > 0 ? (
                <>
                  <span
                    style={{
                      opacity: 0.6,
                    }}
                  >
                    {" "}
                    extends{" "}
                  </span>
                  {parseTypes(part, symbols, baseUrl)}
                </>
              ) : (
                part
              )
            )}{" "}
            <span
              style={{
                opacity: 0.6,
              }}
            >
              {"{"}
            </span>
            {hasIndex && `\n${hasIndex}`}
            {fields.map((field, index) => (
              <React.Fragment key={index}>
                {hasIndex ? "\n    " : "\n  "}
                {prefix ? (
                  <a
                    href={prefix + field.name}
                    onMouseOver={(ev) => {
                      if (!ev.target.title) {
                        const anchor = document.getElementById(
                          prefix.replace("#", "") + field.name
                        );

                        if (anchor) {
                          const description = anchor.nextElementSibling;

                          if (description && description.tagName === "P") {
                            ev.target.title = `ℹ️ ${
                              description.textContent
                            }${parseDescription(description)}`;
                          }
                        }
                      }
                    }}
                  >
                   {field.name}
                  </a>
                ) : (
                  field.name
                )}
                {field.optional ? "?" : ""}:{" "}
                {parseTypes(field.type, symbols, baseUrl)}
                {field.isEvent ? (
                  <Event />
                ) : field.isFunction ? (
                  <Function />
                ) : (
                  field.readOnly && <Readonly />
                )}
                {field.optional && <Optional />}
                {field.deprecated && <Deprecated />}
              </React.Fragment>
            ))}
            {hasIndex && "\n  }"}
            <span
              style={{
                opacity: 0.6,
              }}
            >
              {"\n}"}
            </span>
          </pre>
          <div
            style={{
              marginTop: "-16px",
              marginLeft: "4px",
              fontSize: "11px",
            }}
          >
            🖱️{" "}
            <span
              style={{
                opacity: "0.6",
              }}
            >
              Hover with the mouse over a property name for a tooltip with the
              description of that property. Click it for more information.
            </span>
          </div>
        </>
      );
    }

    return "Invalid interface declaration!";
  }, [src, prefix, symbols]);

  return interfaceDeclaration;
}
