---
title: Usage with React
sidebar_position: 4
description: Learn how to quickly start implementing the Tripetto FormBuilder SDK with React.
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# ![](/img/logo-react.svg) Usage with React
Starting with Tripetto is very easy when you are using [React](https://react.dev). Depending on your project's requirements there are two main Tripetto components to implement:

- **Form Runner component to run forms and collect responses.** It handles form execution (including complex logic and response collection), form UI and form UX. Form respondents filling out a form are working in a runner;
- **Form Builder component to create forms.** It is a visual UI component that allows building smart flowing forms and surveys like flowcharts in the browser. End users who edit forms are working in the builder.

## 🏃 Add a form to your React app {#runner}
To add a form to your app, you first need to create a form. To do so, you can use the [Tripetto Studio web app](https://tripetto.app) or add the [form builder](#builder) to your app (in case you want to allow users to create forms inside your app). Next, add the React component of one of the [stock runners](../runner/stock/introduction.md) to your project and supply the [form definition](../builder/api/interfaces/IDefinition.mdx) of your form to it.

<div className="youtube-embed"><iframe width="560" height="315" src="https://www.youtube.com/embed/fixp89Yievg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>

### 1️⃣ Add packages to your project {#runner-packages}
First, you need to add the required packages to your project. To do so, run the following command:

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```bash
npm install @tripetto/runner-autoscroll @tripetto/runner
```

</TabItem>
<TabItem value="chat" label="Chat">

```bash
npm install @tripetto/runner-chat @tripetto/runner
```

</TabItem>
<TabItem value="classic" label="Classic">

```bash
npm install @tripetto/runner-classic @tripetto/runner
```

</TabItem>
</Tabs>

### 2️⃣ Implement runner component {#runner-component}
The next step is to import and implement the React component. Here's an example:

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```ts showLineNumbers
//highlight-start
import { AutoscrollRunner } from "@tripetto/runner-autoscroll";
//highlight-end

function ExampleApp() {
  return (
    <div>
      <h1>Example app</h1>
      //highlight-start
      <AutoscrollRunner definition={/* Supply your form definition here */} />
      //highlight-end
    </div>
  );
}
```
[![Run](/img/button-run.svg)](https://ovhd8.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-autoscroll-react-basic-implementation-ovhd8?file=/src/index.js)

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
//highlight-start
import { ChatRunner } from "@tripetto/runner-chat";
//highlight-end

function ExampleApp() {
  return (
    <div>
      <h1>Example app</h1>
      //highlight-start
      <ChatRunner definition={/* Supply your form definition here */} />
      //highlight-end
    </div>
  );
}
```
[![Run](/img/button-run.svg)](https://6o6y7.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-chat-react-basic-implementation-6o6y7?file=/src/index.js)

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
//highlight-start
import { ClassicRunner } from "@tripetto/runner-classic";
//highlight-end

function ExampleApp() {
  return (
    <div>
      <h1>Example app</h1>
      //highlight-start
      <ClassicRunner definition={/* Supply your form definition here */} />
      //highlight-end
    </div>
  );
}
```
[![Run](/img/button-run.svg)](https://5u6m2.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-classic-react-basic-implementation-5u6m2?file=/src/index.js)

</TabItem>
</Tabs>

▶️ [Learn more](../runner/stock/guides/index.md)

## 🏗️ Add the form builder to your React app {#builder}
The Tripetto builder package comes with a built-in React component. This makes it very easy to use the builder in a React application.

<div className="youtube-embed"><iframe width="560" height="315" src="https://www.youtube.com/embed/78v901HZbD0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>

### 1️⃣ Add package to your project {#builder-package}
First of all, you need to add the [Tripetto builder package](https://www.npmjs.com/package/@tripetto/builder) to your project. This package is published on [npm](https://www.npmjs.com). To do so, run the following command:

```bash npm2yarn
npm install @tripetto/builder
```

### 2️⃣ Implement builder component {#builder-component}
The next step is to import the [`TripettoBuilder`](../builder/api/components/react.mdx) React component. This component is located in a subfolder named `react` inside the builder package. So make sure, to use the right import statement to import it. Here's an example:

```ts showLineNumbers
//highlight-start
import { TripettoBuilder } from "@tripetto/builder/react";
//highlight-end

function ExampleApp() {
  return (
    <div>
      <h1>Example app</h1>
      //highlight-start
      <TripettoBuilder />
      //highlight-end
    </div>
  );
}
```
[![Run](/img/button-run.svg)](https://25tpib.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-react-basic-implementation-25tpib)

▶️ [Learn more](../builder/integrate/guides/index.md)
