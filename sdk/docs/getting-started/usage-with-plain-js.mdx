---
title: Usage with plain JS
sidebar_position: 6
description: Learn how to quickly start implementing the Tripetto FormBuilder SDK with plain JS.
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# ![](/img/logo-js.svg) Usage with plain JS
Starting with Tripetto is very easy when you are using plain JS. Depending on your project's requirements there are two main Tripetto components to implement:

- **Form Runner component to run forms and collect responses.** It handles form execution (including complex logic and response collection), form UI and form UX. Form respondents filling out a form are working in a runner;
- **Form Builder component to create forms.** It is a visual UI component that allows building smart flowing forms and surveys like flowcharts in the browser. End users who edit forms are working in the builder.

## 🏃 Add a form to your plain JS project {#runner}
To add a form to your project, you first need to create a form. To do so, you can use the [Tripetto Studio web app](https://tripetto.app) or add the [form builder](#builder) to your project (in case you want to allow users to create forms inside your project). Next, add one of the [stock runners](../runner/stock/introduction.md) to your project and supply the [form definition](../builder/api/interfaces/IDefinition.mdx) of your form to it.

### 1️⃣ Add packages to your project {#runner-packages}
First, you need to add the required packages to your project. To do so, run the following command:

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```bash
npm install @tripetto/runner-autoscroll @tripetto/runner
```

</TabItem>
<TabItem value="chat" label="Chat">

```bash
npm install @tripetto/runner-chat @tripetto/runner
```

</TabItem>
<TabItem value="classic" label="Classic">

```bash
npm install @tripetto/runner-classic @tripetto/runner
```

</TabItem>
</Tabs>

### 2️⃣ Implement runner component {#runner-component}
The next step is to display the runner. It uses the [`run`](../runner/stock/quickstart/plain-js.mdx#reference) function to bootstrap the runner using a single call. This will display the form in the body element of the browser. Here's an example:

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";

// Open the runner
run({
  definition: /* Supply your form definition here */,
  display: "page" // Let the runner know it runs full-page
});
```
[![Run](/img/button-run.svg)](https://eyq4x.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-autoscroll-basic-implementation-eyq4x?file=/src/index.ts)

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
import { run } from "@tripetto/runner-chat";

// Open the runner
run({
  definition: /* Supply your form definition here */,
  display: "page" // Let the runner know it runs full-page
});
```
[![Run](/img/button-run.svg)](https://t6end.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-chat-basic-implementation-t6end?file=/src/index.ts)

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
import { run } from "@tripetto/runner-classic";

// Open the runner
run({
  definition: /* Supply your form definition here */,
  display: "page" // Let the runner know it runs full-page
});
```
[![Run](/img/button-run.svg)](https://nlxbq.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-classic-basic-implementation-nlxbq?file=/src/index.ts)

</TabItem>
</Tabs>

▶️ [Learn more](../runner/stock/guides/index.md)

## 🏗️ Add the form builder to your plain JS project {#builder}
The Tripetto builder package makes it very easy to use the builder in a plain JS project.

### 1️⃣ Add package to your project {#builder-package}
First of all, you need to add the [Tripetto builder package](https://www.npmjs.com/package/@tripetto/builder) to your project. This package is published on [npm](https://www.npmjs.com). To do so, run the following command:

```bash npm2yarn
npm install @tripetto/builder
```

### 2️⃣ Implement builder component {#builder-component}
The next step is to show the builder in your project. It uses the static [`open`](../builder/api/classes/Builder.mdx#static-open) method to open the builder in a single call. This will open the builder in the body element of the browser. Here's an example:

```js showLineNumbers
import { Builder } from "@tripetto/builder";

// Open the builder
Builder.open();
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/f7884595bf8178137fab2b4894f9fc20) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/f7884595bf8178137fab2b4894f9fc20)

▶️ [Learn more](../builder/integrate/guides/index.md)
