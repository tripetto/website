---
title: Usage with HTML
sidebar_position: 7
description: Learn how to quickly start implementing the Tripetto FormBuilder SDK with HTML.
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# ![](/img/logo-html.svg) Usage with HTML
Starting with Tripetto is very easy when you are using HTML. Depending on your project's requirements there are two main Tripetto components to implement:

- **Form Runner component to run forms and collect responses.** It handles form execution (including complex logic and response collection), form UI and form UX. Form respondents filling out a form are working in a runner;
- **Form Builder component to create forms.** It is a visual UI component that allows building smart flowing forms and surveys like flowcharts in the browser. End users who edit forms are working in the builder.

## 🏃 Add a form to your HTML project {#runner}
To add a form to your project, you first need to create a form. To do so, you can use the [Tripetto Studio web app](https://tripetto.app) or add the [form builder](#builder) to your project (in case you want to allow users to create forms inside your project). Next, add one of the [stock runners](../runner/stock/introduction.md) to your project and supply the [form definition](../builder/api/interfaces/IDefinition.mdx) of your form to it.

### 1️⃣ Implement runner component {#runner-component}
You can immediately display the form runner in HTML. To do so, you need to load the Runner library from a CDN or [host the library yourself](../runner/stock/quickstart/html.mdx#self-host). If you want to use a CDN, you could go with [unpkg](https://unpkg.com), or [jsDelivr](https://cdn.jsdelivr.net/). This will display the form in the body element of the browser. Here's an example:

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

<Tabs groupId="cdn">
<TabItem value="jsdelivr" label="jsDelivr">

```html showLineNumbers
<html>
  <body>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/runner"></script>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/runner-autoscroll"></script>
    <script>
    TripettoAutoscroll.run({
      definition: /* Supply a form definition here. */,
      display: "page" // Let the runner know it runs full-page
    });
    </script>
  </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/1c5d1b2cb4b8ea10219f00b31b4c30cc) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/1c5d1b2cb4b8ea10219f00b31b4c30cc)

</TabItem>
<TabItem value="unpkg" label="unpkg">

```html showLineNumbers
<html>
  <body>
    <script src="https://unpkg.com/@tripetto/runner"></script>
    <script src="https://unpkg.com/@tripetto/runner-autoscroll"></script>
    <script>
    TripettoAutoscroll.run({
      definition: /* Supply a form definition here. */,
      display: "page" // Let the runner know it runs full-page
    });
    </script>
  </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/4ea97880f9777c59ef1c638a79fcff34) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/4ea97880f9777c59ef1c638a79fcff34)

</TabItem>
</Tabs>

</TabItem>
<TabItem value="chat" label="Chat">

<Tabs groupId="cdn">
<TabItem value="jsdelivr" label="jsDelivr">

```html showLineNumbers
<html>
  <body>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/runner"></script>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/runner-chat"></script>
    <script>
    TripettoChat.run({
      definition: /* Supply a form definition here. */,
      display: "page" // Let the runner know it runs full-page
    });
    </script>
  </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/d471ec4f3e208c21c4e2971bc964ef10) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/d471ec4f3e208c21c4e2971bc964ef10)

</TabItem>
<TabItem value="unpkg" label="unpkg">

```html showLineNumbers
<html>
  <body>
    <script src="https://unpkg.com/@tripetto/runner"></script>
    <script src="https://unpkg.com/@tripetto/runner-chat"></script>
    <script>
    TripettoChat.run({
      definition: /* Supply a form definition here. */,
      display: "page" // Let the runner know it runs full-page
    });
    </script>
  </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/8ccf2465b70c431ea81a05f6fc6cb737) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/8ccf2465b70c431ea81a05f6fc6cb737)

</TabItem>
</Tabs>

</TabItem>
<TabItem value="classic" label="Classic">

<Tabs groupId="cdn">
<TabItem value="jsdelivr" label="jsDelivr">

```html showLineNumbers
<html>
  <body>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/runner"></script>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/runner-classic"></script>
    <script>
    TripettoClassic.run({
      definition: /* Supply a form definition here. */,
      display: "page" // Let the runner know it runs full-page
    });
    </script>
  </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/50b06cf0c6bb7c10dd39259dd8b6b540) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/50b06cf0c6bb7c10dd39259dd8b6b540)

</TabItem>
<TabItem value="unpkg" label="unpkg">

```html showLineNumbers
<html>
  <body>
    <script src="https://unpkg.com/@tripetto/runner"></script>
    <script src="https://unpkg.com/@tripetto/runner-classic"></script>
    <script>
    TripettoClassic.run({
      definition: /* Supply a form definition here. */,
      display: "page" // Let the runner know it runs full-page
    });
    </script>
  </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/7d1de497c510a80ac71ae60aa77188d8) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/7d1de497c510a80ac71ae60aa77188d8)

</TabItem>
</Tabs>

</TabItem>
</Tabs>

▶️ [Learn more](../runner/stock/guides/index.md)

## 🏗️ Add the form builder to your HTML project {#builder}
The Tripetto builder package makes it very easy to use the builder in a HTML project.

### 1️⃣ Implement builder component {#builder-component}
You can immediately display the form builder in HTML. To do so, you need to load the builder library from a CDN or [host the library yourself](../builder/integrate/quickstart/html.mdx#self-host). If you want to use a CDN, you could go with [jsDelivr](https://cdn.jsdelivr.net/) or [unpkg](https://unpkg.com). This will display the form builder in the body element of the browser. Here's an example:

<Tabs groupId="cdn">
<TabItem value="jsdelivr" label="jsDelivr">

```html showLineNumbers
<html>
    <body>
        <!-- Load the Tripetto builder library -->
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/builder"></script>

        <!-- Fire it up! -->
        <script>
            Tripetto.Builder.open();
        </script>
    </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/29a188ee6c49b3568eeab94794639e16) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/29a188ee6c49b3568eeab94794639e16)

</TabItem>
<TabItem value="unpkg" label="unpkg">

```html showLineNumbers
<html>
    <body>
        <!-- Load the Tripetto builder library -->
        <script src="https://unpkg.com/@tripetto/builder"></script>

        <!-- Fire it up! -->
        <script>
            Tripetto.Builder.open();
        </script>
    </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/79c4785b567ce98a0042dae8d144be6a) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/79c4785b567ce98a0042dae8d144be6a)

</TabItem>
</Tabs>

▶️ [Learn more](../builder/integrate/guides/index.md)
