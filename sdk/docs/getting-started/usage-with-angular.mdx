---
title: Usage with Angular
sidebar_position: 5
description: Learn how to quickly start implementing the Tripetto FormBuilder SDK with Angular.
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# ![](/img/logo-angular.svg) Usage with Angular
Starting with Tripetto is very easy when you are using [Angular](https://angular.io). Depending on your project's requirements there are two main Tripetto components to implement:

- **Form Runner component to run forms and collect responses.** It handles form execution (including complex logic and response collection), form UI and form UX. Form respondents filling out a form are working in a runner;
- **Form Builder component to create forms.** It is a visual UI component that allows building smart flowing forms and surveys like flowcharts in the browser. End users who edit forms are working in the builder.

## 🏃 Add a form to your Angular app {#runner}
To add a form to your app, you first need to create a form. To do so, you can use the [Tripetto Studio web app](https://tripetto.app) or add the [form builder](#builder) to your app (in case you want to allow users to create forms inside your app). Next, add the Angular component of one of the [stock runners](../runner/stock/introduction.md) to your project and supply the [form definition](../builder/api/interfaces/IDefinition.mdx) of your form to it.

### 1️⃣ Add packages to your project {#runner-packages}
First, you need to add the required packages to your project. To do so, run the following command:

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```bash
npm install @tripetto/runner-autoscroll @tripetto/runner
```

</TabItem>
<TabItem value="chat" label="Chat">

```bash
npm install @tripetto/runner-chat @tripetto/runner
```

</TabItem>
<TabItem value="classic" label="Classic">

```bash
npm install @tripetto/runner-classic @tripetto/runner
```

</TabItem>
</Tabs>

### 2️⃣ Implement runner component {#runner-component}
The next step is to import and implement the Angular component. Here's an example:

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

<Tabs>
<TabItem value="app-module" label="App module">

```ts showLineNumbers title="app.module.ts"
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
//highlight-start
import { TripettoAutoscrollModule } from "@tripetto/runner-autoscroll/angular";
//highlight-end

@NgModule({
  declarations: [AppComponent],
  //highlight-start
  imports: [BrowserModule, TripettoAutoscrollModule],
  //highlight-end
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
```

</TabItem>
<TabItem value="app-html" label="App HTML">

```html showLineNumbers title="app.component.html"
<tripetto-runner-autoscroll [definition]="definition"></tripetto-runner-autoscroll>
```

</TabItem>
<TabItem value="app-component" label="App component">

```ts showLineNumbers title="app.component.ts"
import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  //highlight-start
  definition = /* Supply your form definition here */;
  //highlight-end
}
```

</TabItem>
</Tabs>

[![Run](/img/button-run.svg)](https://s3o6j.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-autoscroll-angular-basic-implementation-s3o6j?file=/src/app/app.component.html)

</TabItem>
<TabItem value="chat" label="Chat">

<Tabs>
<TabItem value="app-module" label="App module">

```ts showLineNumbers title="app.module.ts"
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
//highlight-start
import { TripettoChatModule } from "@tripetto/runner-chat/angular";
//highlight-end

@NgModule({
  declarations: [AppComponent],
  //highlight-start
  imports: [BrowserModule, TripettoChatModule],
  //highlight-end
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
```

</TabItem>
<TabItem value="app-html" label="App HTML">

```html showLineNumbers title="app.component.html"
<tripetto-runner-chat [definition]="definition"></tripetto-runner-chat>
```

</TabItem>
<TabItem value="app-component" label="App component">

```ts showLineNumbers title="app.component.ts"
import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  //highlight-start
  definition = /* Supply your form definition here */;
  //highlight-end
}
```

</TabItem>
</Tabs>

[![Run](/img/button-run.svg)](https://wtvhr.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-chat-angular-basic-implementation-wtvhr?file=/src/app/app.component.html)

</TabItem>
<TabItem value="classic" label="Classic">

<Tabs>
<TabItem value="app-module" label="App module">

```ts showLineNumbers title="app.module.ts"
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
//highlight-start
import { TripettoClassicModule } from "@tripetto/runner-classic/angular";
//highlight-end

@NgModule({
  declarations: [AppComponent],
  //highlight-start
  imports: [BrowserModule, TripettoClassicModule],
  //highlight-end
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
```

</TabItem>
<TabItem value="app-html" label="App HTML">

```html showLineNumbers title="app.component.html"
<tripetto-runner-classic [definition]="definition"></tripetto-runner-classic>
```

</TabItem>
<TabItem value="app-component" label="App component">

```ts showLineNumbers title="app.component.ts"
import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  //highlight-start
  definition = /* Supply your form definition here */;
  //highlight-end
}
```

</TabItem>
</Tabs>

[![Run](/img/button-run.svg)](https://de1fh.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-classic-angular-basic-implementation-de1fh?file=/src/app/app.component.html)

</TabItem>
</Tabs>

▶️ [Learn more](../runner/stock/guides/index.md)

## 🏗️ Add the form builder to your Angular app {#builder}
The Tripetto builder package comes with a built-in Angular component. This makes it very easy to use the builder in an Angular application.

### 1️⃣ Add package to your project {#builder-package}
First of all, you need to add the [Tripetto builder package](https://www.npmjs.com/package/@tripetto/builder) to your project. This package is published on [npm](https://www.npmjs.com). To do so, run the following command:

```bash npm2yarn
npm install @tripetto/builder
```

### 2️⃣ Implement builder component {#builder-component}
To use the component, simply import `TripettoBuilderModule` from the package and feed it to your application's `@NgModule` imports array. This makes the [`<tripetto-builder>`](../builder/api/components/angular.mdx) selector available in your application. The Angular component is located in a subfolder named `angular` inside the builder package. Here's an example:

<Tabs>
<TabItem value="app-module" label="App module">

```ts showLineNumbers title="app.module.ts"
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
//highlight-start
import { TripettoBuilderModule } from "@tripetto/builder/angular";
//highlight-end

@NgModule({
  declarations: [AppComponent],
  //highlight-start
  imports: [BrowserModule, TripettoBuilderModule],
  //highlight-end
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
```

</TabItem>
<TabItem value="app-html" label="App HTML">

```html showLineNumbers title="app.component.html"
<tripetto-builder (onSave)="formSaved($event)"></tripetto-builder>
```

</TabItem>
<TabItem value="app-component" label="App component">

```ts showLineNumbers title="app.component.ts"
import { Component } from "@angular/core";
//highlight-start
import { IDefinition } from "@tripetto/builder";
//highlight-end

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  //highlight-start
  formSaved(definition: IDefinition) {
    // Do something with the form definition
    console.log(definition);
  }
  //highlight-end
}
```

</TabItem>
</Tabs>

[![Run](/img/button-run.svg)](https://6lc5e.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-angular-basic-implementation-6lc5e)

▶️ [Learn more](../builder/integrate/guides/index.md)
