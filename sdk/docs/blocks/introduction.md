---
title: Blocks
sidebar_label: Introduction
sidebar_position: 1
description: Blocks are the question types in Tripetto.
---

# Blocks

🎓 Blocks are the **question types** in Tripetto.

📇 All popular question types are available as convienant **[stock blocks](stock/index.mdx)**.

👩‍💻 Developers can extend Tripetto with their own **[custom blocks](custom/introduction.md)**!

## 👩‍🏫 Introduction
Blocks are the third core pillar of Tripetto. It is our synonym for question types. But they're certainly not limited to just question types, as you'll soon discover! Besides a collection of [stock blocks](stock/index.mdx) (blocks developed and maintained by the Tripetto team), you can extend Tripetto with [custom blocks](custom/introduction.md). You can build them yourself with the SDK. The sky is the limit here because you can build whatever you want.

## 📺 Preview {#preview}
![Blocks preview](/img/blocks.png)

## 🗂️ Types of blocks
There are three types of blocks used in Tripetto:
- Visual blocks
- Headless blocks (also called action blocks)
- Condition blocks

Visual blocks are - as the name suggests - blocks that the user can see and interact with (for example, a text input field). A block is not limited to a single input field. It could also define a set of multiple-input fields (for example, an address input block).

Headless blocks, on the other hand, are a special kind of blocks that can perform actions. Headless blocks are invisible and never render to the UI. For example, the [calculator stock block](stock/calculator.mdx) is a headless block that can perform advanced calculations inside a form. But you could also develop a headless block to communicate with external services such as APIs.

The last type of blocks are condition blocks. These are also invisible blocks that do not appear in the UI of a runner. Instead, they contain logic and are used to define branching conditions.

## 🔩 Block parts
All types of blocks need **two** parts to let them work properly:
- 🏗️ The **builder part**;
- 🏃 The **runner part**.

The builder part instructs the [builder](../builder/introduction.md) how to manage the block. It defines the configurable properties and settings for the block, which can then be managed with the visual builder.

The runner part, on the other hand, contains the rendering or logic of the block inside the [runner](../runner/introduction.md) (the thing that runs the form). It defines the UI for the block and makes the block visible in the runner. For headless blocks, the runner part contains the actions to perform. For condition blocks, the runner part contains the logic for validating the condition.

:::info
It is good to remember that these two parts of a block do not necessarily have to live within the same codebase. They can completely be separated. We tend to do that with the stock blocks we develop. We often publish the builder part of the blocks as npm package, but keep the visual runner part inside the runner repositories. This makes sense since the look and feel of each block in the runner greatly depends on the type of runner.
:::

## ⏭️ Next steps
Want to use one of the available stock blocks that are developed and maintained by the Tripetto team?

📇 [Browse through the list of stock blocks](stock/index.mdx)

Or, want to start building a custom block? Then start reading the custom block documentation and learn how to develop your own block for Tripetto.

👩‍💻 [Start building a custom block](custom/introduction.md)
