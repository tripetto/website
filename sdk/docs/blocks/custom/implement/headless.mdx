---
title: Headless block - Custom blocks
sidebar_label: Headless block
sidebar_position: 3
toc_max_heading_level: 4
description: Headless blocks can perform actions that don't need a visual counterpart in the runner.
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';
import UpNextTopics from './up-next-topics.include.mdx'

# Creating a headless block
Headless blocks can perform actions that don't need a visual counterpart in the runner. A good example of a headless block is the [calculator stock block](../../stock/calculator.mdx). It can perform complex calculations in a form, and it does that silently in the background. Of course, the outcome of a headless block is useable by other blocks and can often be recalled somewhere else in a form. This tutorial explains how to set up such a headless block. Just like a [visual](visual.mdx) block, it needs two parts to work properly:
- A **builder part** that makes the block available to the [visual builder](../../../builder/introduction.md), so the block becomes usable in a form;
- A **runner part** that does the actual operation of the block in the [runner](../../../runner/introduction.md) that runs the form.

This tutorial covers both parts, beginning with the builder part. And to keep it practical, we're going to build a simple headless block that retrieves the current day of the week and exposes it to a variable (slot), so it can be recalled somewhere else in the form.

## 🏗️ Builder part {#builder-part}
The builder part of a headless block allows the visual builder to consume the block. It allows the builder user (form editor) to select the new block, attach it to a node, and configure its properties and settings. For this tutorial, we'll start from scratch and prepare a new project for our weekday block (you may skip this and jump straight to the [block implementation](#builder-implementation) when you don't need help preparing your project).

*Can't wait to see the result of the builder part tutorial? Click the buttons below to run or try a live code example.*

[![Run](/img/button-run.svg)](https://1gr6yn.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-blocks-headless-block-weekday-block-example-builder-part-1gr6yn?file=/src/weekday.ts)

---
### ▶️ Prepare your project {#builder-prepare}

#### 1️⃣ Create a project {#builder-prepare-step-1}
First, create a new folder for the project (for example, `weekday-block`). Then open a command-line terminal for that new folder and run the following command to initiate the project:

```bash npm2yarn
npm init
```
:::tip
You can just hit the `enter` key for all questions asked when running the `init` command. The default configuration is good for now.
:::

#### 2️⃣ Add dependencies {#builder-prepare-step-2}
Now we can add the required packages for our block. In this example, we'll use [webpack](https://webpack.js.org/) as bundler. Besides the webpack dependencies we need the [TypeScript](https://www.typescriptlang.org/) compiler and Tripetto's [Builder package](https://www.npmjs.com/package/@tripetto/builder). We also add [image-webpack-loader](https://www.npmjs.com/package/image-webpack-loader) and [url-loader](https://www.npmjs.com/package/url-loader) to enable webpack to process images, [ts-loader](https://www.npmjs.com/package/ts-loader) to process TypeScript with webpack and [concurrently](https://www.npmjs.com/package/concurrently) to allow to run Tripetto's builder and webpack simultaneous during development. Let's add these dependencies by running the following command in your project folder:

```bash npm2yarn
npm install @tripetto/builder typescript webpack webpack-cli image-webpack-loader url-loader ts-loader concurrently
```

#### 3️⃣ Add configuration files {#builder-prepare-step-3}
Next, add the following files to the root of your project folder. It's the configuration for the TypeScript compiler and webpack bundler.

<Tabs>
<TabItem value="tsconfig.json" label="tsconfig.json">

```json showLineNumbers
{
  "compileOnSave": false,
  "compilerOptions": {
    "target": "ES5",
    "moduleResolution": "Node",
    "newLine": "LF",
    "strict": true,
    "experimentalDecorators": true
  },
  "include": ["./src/**/*.ts"]
}
```
:::info
Make sure the [`experimentalDecorators`](https://www.typescriptlang.org/tsconfig#experimentalDecorators) feature is enabled. We need support for [decorators](https://www.typescriptlang.org/docs/handbook/decorators.html).
:::

</TabItem>
<TabItem value="webpack.config.js" label="webpack.config.js">

```js showLineNumbers
const path = require("path");

module.exports = {
  target: ["web", "es5"],
  entry: "./src/index.ts",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "builder.bundle.js",
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        loader: "ts-loader",
      },
      {
        test: /\.svg$/,
        use: ["url-loader", "image-webpack-loader"],
      },
    ],
  },
  resolve: {
    extensions: [".ts", ".js"],
  },
  externals: {
    // Make sure to exclude the builder from the bundle!
    "@tripetto/builder": "Tripetto",
  },
};
```

</TabItem>
</Tabs>

#### 4️⃣ Project configuration {#builder-prepare-step-4}
Now open the `package.json` file that was generated in the first step and add the highlighted lines to it:

```json showLineNumbers
{
  "name": "weekday-block",
  //highlight-start
  "main": "./dist/builder.bundle.js",
  "scripts": {
    "test": "webpack --mode development && concurrently -n \"tripetto,webpack\" -c \"blue.bold,green\" -k -s \"first\" \"tripetto ./example.json --verbose\" \"webpack --mode development --watch\""
  },
  //highlight-end
  "dependencies": {
    "@tripetto/builder": "^5.0.30",
    "image-webpack-loader": "^8.1.0",
    "ts-loader": "^9.2.8",
    "typescript": "^4.6.3",
    "webpack": "^5.72.0",
    "webpack-cli": "^4.9.2"
  },
  //highlight-start
  "tripetto": {
    "blocks": [
      "."
    ]
  }
  //highlight-end
}
```
Here the  `main` property specifies the location of the JS bundle that webpack will generate. The `tripetto` section contains the [configuration](../../../builder/cli/configuration.md#using-packagejson) for the builder. In this case, it tries to load the block that's in the same folder (`.`). Tripetto will lookup the `package.json` file and read the `main` property to find the right JS file for the block.
:::info
More information about configuring the Tripetto CLI using `package.json` can be found in the [configuration](../../../builder/cli/configuration.md#using-packagejson) section of the builder documentation.
:::

#### 5️⃣ Prepare source file {#builder-prepare-step-5}
The last step before we will test the setup is to add an empty source file that we'll use later on to declare the block in. Create a folder with the name `src` and add an empty file to it with the name `index.ts`. This will be the entry point for the block. The structure of the project should now look something like this:

```
project/
├─ src/
│  └─ index.ts
├─ package.json
├─ tsconfig.json
└─ webpack.config.js
```

#### 6️⃣ Test it! {#builder-prepare-step-6}
Now you should be able to run this setup. Run the following command to start webpack together with the Tripetto builder:
```bash npm2yarn
npm test
```

:::info
Webpack will now monitor code changes and update the bundle on each change. Hit `F5` in the browser to reload the builder with the updated bundle. Or follow the [bonus step](#builder-prepare-bonus) below to enable live-reloading of the builder.
:::

#### 7️⃣ Live-reloading (bonus) {#builder-prepare-bonus}
If you want, you can enable live-reloading of the builder on each code change. To enable that, we need the [webpack-livereload-plugin](https://www.npmjs.com/package/webpack-livereload-plugin) package:

```bash npm2yarn
npm install webpack-livereload-plugin
```
Then, open `webpack.config.js` and add the highlighted lines:

```js showLineNumbers title="webpack.config.js"
const path = require("path");
//highlight-start
const webpackLiveReload = require("webpack-livereload-plugin");
//highlight-end

module.exports = {
  target: ["web", "es5"],
  entry: "./src/index.ts",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "builder.bundle.js",
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        loader: "ts-loader",
      },
      {
        test: /\.svg$/,
        use: ["url-loader", "image-webpack-loader"],
      },
    ],
  },
  resolve: {
    extensions: [".ts", ".js"],
  },
  externals: {
    // Make sure to exclude the builder from the bundle!
    "@tripetto/builder": "Tripetto",
  },
  //highlight-start
  plugins: [
    new webpackLiveReload({
      appendScriptTag: true,
    }),
  ]
  //highlight-end
};
```
Now the builder should automatically reload the bundle when the code is changed.

---
### ▶️ Block implementation {#builder-implementation}
When everything is prepared properly, we can begin building the actual headless block.

#### 1️⃣ Declare the block {#builder-implementation-step-1}
Let's start with the basic code of a headless block and then go through that code. Update your empty `index.ts` file to the code shown below and add the file `icon.svg` to the same folder (it's the [icon](../guides/icon.mdx) that Tripetto will use for this block).

<Tabs>
<TabItem value="index.ts" label="index.ts">

```ts showLineNumbers
import { tripetto, NodeBlock } from "@tripetto/builder";
import icon from "./icon.svg";

@tripetto({
  type: "node",
  kind: "headless",
  identifier: "weekday",
  label: "Retrieve weekday",
  icon
})
class WeekdayBlock extends NodeBlock {
  // Block implementation here
}
```

</TabItem>
<TabItem value="icon.svg" label="icon.svg">

```xml showLineNumbers
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="20" height="20" viewBox="0 0 20 20">
<path d="M18.5 2h-2.5v-0.5c0-0.276-0.224-0.5-0.5-0.5s-0.5 0.224-0.5 0.5v0.5h-10v-0.5c0-0.276-0.224-0.5-0.5-0.5s-0.5 0.224-0.5 0.5v0.5h-2.5c-0.827 0-1.5 0.673-1.5 1.5v14c0 0.827 0.673 1.5 1.5 1.5h17c0.827 0 1.5-0.673 1.5-1.5v-14c0-0.827-0.673-1.5-1.5-1.5zM1.5 3h2.5v1.5c0 0.276 0.224 0.5 0.5 0.5s0.5-0.224 0.5-0.5v-1.5h10v1.5c0 0.276 0.224 0.5 0.5 0.5s0.5-0.224 0.5-0.5v-1.5h2.5c0.276 0 0.5 0.224 0.5 0.5v2.5h-18v-2.5c0-0.276 0.224-0.5 0.5-0.5zM18.5 18h-17c-0.276 0-0.5-0.224-0.5-0.5v-10.5h18v10.5c0 0.276-0.224 0.5-0.5 0.5z" class="tripetto-fill"></path>
<path d="M7.5 10h-1c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h1c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z" class="tripetto-fill"></path>
<path d="M10.5 10h-1c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h1c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z" class="tripetto-fill"></path>
<path d="M13.5 10h-1c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h1c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z" class="tripetto-fill"></path>
<path d="M16.5 10h-1c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h1c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z" class="tripetto-fill"></path>
<path d="M4.5 12h-1c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h1c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z" class="tripetto-fill"></path>
<path d="M7.5 12h-1c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h1c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z" class="tripetto-fill"></path>
<path d="M10.5 12h-1c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h1c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z" class="tripetto-fill"></path>
<path d="M13.5 12h-1c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h1c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z" class="tripetto-fill"></path>
<path d="M16.5 12h-1c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h1c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z" class="tripetto-fill"></path>
<path d="M4.5 14h-1c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h1c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z" class="tripetto-fill"></path>
<path d="M7.5 14h-1c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h1c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z" class="tripetto-fill"></path>
<path d="M10.5 14h-1c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h1c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z" class="tripetto-fill"></path>
<path d="M13.5 14h-1c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h1c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z" class="tripetto-fill"></path>
<path d="M16.5 14h-1c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h1c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z" class="tripetto-fill"></path>
<path d="M4.5 16h-1c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h1c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z" class="tripetto-fill"></path>
<path d="M7.5 16h-1c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h1c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z" class="tripetto-fill"></path>
<path d="M10.5 16h-1c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h1c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z" class="tripetto-fill"></path>
<path d="M13.5 16h-1c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h1c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z" class="tripetto-fill"></path>
<path d="M16.5 16h-1c-0.276 0-0.5-0.224-0.5-0.5s0.224-0.5 0.5-0.5h1c0.276 0 0.5 0.224 0.5 0.5s-0.224 0.5-0.5 0.5z" class="tripetto-fill"></path>
</svg>
```
</TabItem>
</Tabs>

This code is all you need to declare a new [class](https://www.typescriptlang.org/docs/handbook/2/classes.html) with the name `WeekdayBlock`. The class is derived from the base class [`NodeBlock`](../../api/classes/NodeBlock.mdx) that is imported from the Tripetto builder package. The base class has all the core functionality of a block, so you can easily extend it with the functionality you need. This block has no further implementation yet, so the class is still empty. We'll come to that in a moment.

First, let's talk about the [`@tripetto`](../../api/decorators/tripetto.mdx) decorator. This so-called [class decorator](https://www.typescriptlang.org/docs/handbook/decorators.html#class-decorators) is used to register the block to the Tripetto builder. Tripetto uses [dependency injection](https://en.wikipedia.org/wiki/Dependency_injection) to retrieve blocks that are registered using the decorator. The [`identifier`](../../api/decorators/tripetto.mdx#INodeBlockDecorator-identifier), [`label`](../../api/decorators/tripetto.mdx#INodeBlockDecorator-label), and [`icon`](../../api/decorators/tripetto.mdx#INodeBlockDecorator-icon) of the block are all supplied to that decorator and are used by the visual builder to allow the user to select the block and assign it to a node. For headless blocks, you also need to supply the [`kind`](../../api/decorators/tripetto.mdx#INodeBlockDecorator-kind) property to the decorator and set it to `headless`.

Now we can add the functionality to the block that we need for our weekday block. We need to do three things for that on the builder part:
1. Define the data the block will collect;
2. Automatically update the node name;
3. Instruct the builder how the user can manage the block properties and settings.

Let's do that in the following steps!

#### 2️⃣ Define block slots {#builder-implementation-step-2}
The next step is to specify the type of data this block will collect. In the Tripetto world, data is collected using **slots**. Each slot can contain a single data item. In this case, we need a single slot that will hold the current day of the week, which is a string. So, we are going to use the [`String`](../../api/modules/Slots/String.mdx) slot type for that. To define the slot, we need to add a method to our block class and decorate it with the [`@slots`](../../api/decorators/slots.mdx) decorator. This decorator instructs Tripetto to use that method when it needs the slots for the block. Update the highlighted lines in `index.ts`:

```ts showLineNumbers title="index.ts"
//highlight-start
import { tripetto, slots, NodeBlock, Slots } from "@tripetto/builder";
//highlight-end
import icon from "./icon.svg";

@tripetto({
  type: "node",
  kind: "headless",
  identifier: "weekday",
  label: "Retrieve weekday",
  icon
})
class WeekdayBlock extends NodeBlock {
  //highlight-start
  @slots
  onSlots(): void {
    this.slots.static({
      type: Slots.String,
      reference: "weekday",
      label: "Current day of the week"
    });
  }
  //highlight-end
}
```

As you can see, we've added a new method `onSlots` (you can choose any name you want for this method), and decorated it with [`@slots`](../../api/decorators/slots.mdx). The method implements the [`slots`](../../api/classes/NodeBlock.mdx#slots) field of the `WeekdayBlock` class to create a new [static slot](../../api/modules/Slots/SlotsClass.mdx#static) for the day of the week. And that's all you need to do on the builder part. You will see later on in this tutorial how we will use this slot to supply a value to it in the runner part of the block.

:::tip
Read the [Slots guide](../guides/slots.mdx) to learn more about slots and its possibilities.
:::

#### 3️⃣ Update node name {#builder-implementation-step-3}
For most blocks the [name](../../api/classes/Node.mdx#name) of the node is flexible and configurable by the builder user. But for our weekday block, the name can be set to a fixed value `Current weekday`. So, how do we do that? We can use the [`@assigned`](../../api/decorators/assigned.mdx) decorator to decorate a method that is invoked when the block is attached to a block. That's a perfect moment to update the name of the node to our fixed value. Update the highlighted lines in `index.ts`:

```ts showLineNumbers title="index.ts"
//highlight-start
import { tripetto, slots, assigned, NodeBlock, Slots } from "@tripetto/builder";
//highlight-end
import icon from "./icon.svg";

@tripetto({
  type: "node",
  kind: "headless",
  identifier: "weekday",
  label: "Retrieve weekday",
  icon
})
class WeekdayBlock extends NodeBlock {
  @slots
  onSlots(): void {
    this.slots.static({
      type: Slots.String,
      reference: "weekday",
      label: "Current day of the week"
    });
  }

  //highlight-start
  @assigned
  onAssign(): void {
    this.node.name = "Current weekday";
  }
  //highlight-end
}
```

#### 4️⃣ Define block editor {#builder-implementation-step-4}
The final step is to instruct the builder how to manage the properties and settings for the block. That's simple in this case, as there are no properties or settings that needs to be managed for the weekday block. Instead, we can show a message when the editor panel is opened for this block. Let's implement that.

In this case, we need to add a method to the `WeekdayBlock` class and then decorate it with the [`@editor`](../../api/decorators/editor.mdx) decorator. This method will be invoked by the builder when the editor panel for the block is requested. We can use the [`editor`](../../api/classes/NodeBlock.mdx#editor) field of the `WeekdayBlock` class to define UI controls to manage the block. Update the highlighted lines in `index.ts`:

```ts showLineNumbers title="index.ts"
//highlight-start
import { tripetto, slots, assigned, editor, NodeBlock, Slots, Forms } from "@tripetto/builder";
//highlight-end
import icon from "./icon.svg";

@tripetto({
  type: "node",
  kind: "headless",
  identifier: "weekday",
  label: "Retrieve weekday",
  icon
})
class WeekdayBlock extends NodeBlock {
  @slots
  onSlots(): void {
    this.slots.static({
      type: Slots.String,
      reference: "weekday",
      label: "Current day of the week"
    });
  }

  @assigned
  onAssign(): void {
    this.node.name = "Current weekday";
  }

  //highlight-start
  @editor
  onEdit(): void {
    this.editor.form({
      controls: [
        new Forms.Notification(
          "This block will retrieve the current day of the week.",
          "success"
        )
      ]
    });
  }
  //highlight-end
}
```

[![Run](/img/button-run.svg)](https://1gr6yn.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-blocks-headless-block-weekday-block-example-builder-part-1gr6yn?file=/src/weekday.ts)

This code uses the [`form`](../../api/classes/EditorOrchestrator.mdx#form) method to define a custom form with a [`Notification`](../../api/modules/Forms/Notification.mdx) control in it.

:::tip
Read the [Block editor guide](../guides/properties.mdx) to learn more about building block editor panels.
:::

#### 💯 Builder part done! {#builder-part-done}
And there, we have completed the builder part of the block! Next, we're going to define the runner part of it.

## 🏃 Runner part {#runner-part}
The runner part of a headless block performs the actual operation. Since headless blocks don't require rendering to a runner UI, they can be reused across runners. So, you don't need a specific implementation for each runner. Let's build the runner part of the weekday example block. The runner part should retrieve the current day of the week and store that value in the weekday slot.

:::info
This tutorial assumes you have a runner project up and running. If not, follow the instructions to prepare a project in the [visual block tutorial](visual.mdx#runner-prepare).
:::

*Can't wait to see the end result of the runner part tutorial? Click the buttons below to run or try a live code example.*

[![Run](/img/button-run.svg)](https://l88cyi.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-blocks-headless-block-runner-part-weekday-block-example-l88cyi?file=/src/weekday.ts)

### ▶️ Block implementation {#runner-implementation}

#### 1️⃣ Declare the block {#runner-implementation-step-1}
Let's start with the basic code of the runner part of a headless block and then go through that code. Create a file `weekday.ts` in your runner project with the following code:

```ts showLineNumbers title="weekday.ts"
import { tripetto, HeadlessBlock } from "@tripetto/runner";

@tripetto({
  type: "headless",
  identifier: "weekday",
})
class WeekdayBlock extends HeadlessBlock {
  do(): void {
    // Perform block operation here
  }
}
```

This code declares a new [class](https://www.typescriptlang.org/docs/handbook/2/classes.html) with the name `WeekdayBlock`. The class is derived from the base class [`HeadlessBlock`](../../../runner/api/library/classes/HeadlessBlock.mdx) that is imported from the Tripetto Runner library. The [`@tripetto`](../../../runner/api/library/decorators/tripetto.mdx) decorator is used to register the block to the Tripetto runner.

:::caution
If you want to use a headless block in one of the stock runners, make sure to make the block available in the blocks namespace of the stock runner. You can do that using the [`namespace`](../../../runner/api/library/decorators/tripetto.mdx#namespace) property of the [`@tripetto`](../../../runner/api/library/decorators/tripetto.mdx) decorator (when you only target one runner) or by using [`mountNamespace`](../../../runner/api/library/functions/mountNamespace.mdx).
:::

#### 2️⃣ Define block operation {#runner-implementation-step-2}
The next step is to implement the operation for the block. To do so, we need to implement the [`do`](../../../runner/api/library/classes/HeadlessBlock.mdx#do) method of the [`HeadlessBlock`](../../../runner/api/library/classes/HeadlessBlock.mdx). Update the highlighted lines in the `WeekdayBlock` class:

```ts showLineNumbers title="weekday.ts"
import { tripetto, HeadlessBlock } from "@tripetto/runner";

@tripetto({
  type: "headless",
  identifier: "weekday",
})
class WeekdayBlock extends HeadlessBlock {
  do(): void {
    //highlight-start
    this.valueOf("weekday")?.set(
      new Date().toLocaleString("default", { weekday: "long" })
    );
    //highlight-end
  }
}
```

This code retrieves the `weekday` slot (that was defined in the [builder part](#builder-implementation-step-2) of the block) using the [`valueOf`](../../../runner/api/library/classes/HeadlessBlock.mdx#valueOf) method and sets the value to the current day of the week.

[![Run](/img/button-run.svg)](https://l88cyi.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-blocks-headless-block-runner-part-weekday-block-example-l88cyi?file=/src/weekday.ts)

#### 💯 Runner part done! {#runner-part-done}
That's all we need to do for the runner part of our example weekday block.

## ⏭️ Up next {#up-next}
<UpNextTopics />
