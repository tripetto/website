---
title: Guides - Custom blocks
sidebar_label: Guides
description: When you have the basic implementation of your block up and running, it is time to dive deeper and add more features.
---

# Guides
When you have the [basic implementation](../implement) of your block up and running, it is time to dive deeper and add more features. The following guides help you to master the development of blocks for Tripetto.

- 🎭 [Block icon](icon.mdx)
- 🏷️ [Block properties](properties.mdx)
- 🎛️ [Block editor](editor.mdx)
- 🗃️ [Slots](slots.mdx)
- 📇 [Collections](collections.mdx)
- ✅ [Validation](validation.mdx)
- ⤵️ [Conditions](conditions.mdx)
- 🎬 [Post-processing actions](post-processing.mdx)
- 🌍 [Translations](l10n.mdx)
- 📂 [Boilerplate](boilerplate.mdx)
