---
title: Post-processing actions - Custom blocks
sidebar_label: Post-processing actions
sidebar_position: 8
description: Some action blocks generate data that needs post-processing after completion of a form. This is where post-processing actions come into play.
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Post-processing actions
Tripetto supports the use of [headless action blocks](../implement/headless.mdx). These blocks can be added to a form like normal question blocks. The difference is that an action block does not ask for a certain input from the respondent using visual input elements. Instead, it acts in the background. For example, the [calculator](../../stock/calculator.mdx) block that can perform realtime calculations in the form.

Some action blocks generate data that needs post-processing after completion of a form. A good example of such an action block is the [mailer](../../stock/mailer.mdx) block. It is used to compose an email message within a form. However, this message still needs to be sent when the form completes. This is where post-processing actions come into play.

:::info
In most setups, the post-processing needs to be done at the (server) endpoint that processes/receives your form data. That's because the runner is a purely client-side component.
:::

## 🎬 Obtaining post-processing data {#obtaining-data}
To make it easy to retrieve the data you need for post-processing, there is a special [export](../../../runner/api/library/modules/Export.mdx) function called [`actionables`](../../../runner/api/library/modules/Export.mdx#actionables). It retrieves all fields from a form necessary for post-processing. So, for example, when your form includes a mailer block, the function retrieves the data fields necessary for composing and sending the email message.

A best practice is to submit the action data together with your [exportable](../../../runner/stock/guides/collecting.mdx) data to your data collection endpoint and then process it there. The following example shows how to submit both data structures to an endpoint.

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";
import { Export } from "@tripetto/runner";

run({
  definition: /* Supply your form definition here */,
  onSubmit: (instance) => new Promise((resolve, reject) => {
    fetch("/example-server", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        //highlight-start
        // Post both the exportable and actionable data to the endpoint
        exportables: Export.exportables(instance),
        actionables: Export.actionables(instance)
        //highlight-end
      })
    })
      .then((response) => {
        if (response.ok) {
          // All good, resolve the promise
          resolve();
        } else {
          // Not so good, reject the promise
          reject();
        }
      })
      .catch((error) => {
        // Error occurred, reject with error message
        reject(error.message);
      });
  })
});
```

## 🍳 Post-processing the data {#post-processing-data}
After the endpoint receives the data, it needs to post-process it. In general, there is no need to store the actionable data. It is volatile data only necessary to perform the actions. We recommend performing validation and storage of the exportable data first. If that went well, process the actionable data and perform the actions.

The code for the post-processing of the action blocks depends on the technology stack used for your endpoint. Please have a look at the implementation in the Tripetto Studio (TypeScript, Node.js) or the Tripetto WordPress plugin (PHP) for a complete example:

- [Node.js (TypeScript) example](https://gitlab.com/tripetto/studio/-/blob/production/src/server/controllers/runner/index.ts)
- [PHP example](https://gitlab.com/tripetto/wordpress/-/blob/master/src/runner/runner.php)

Both implementations use the same approach. They iterate through the actionable data looking for the block identifier of the action blocks that need post-processing. Then the data required for the action is retrieved, and after some validation, the action executes.

<Tabs>
<TabItem value="typescript" label="Node.js (TypeScript)">

```ts showLineNumbers
// Retrieve the actionables object from the POST data
const actionables = req.body.actionables;

// Iterate through the nodes
if (actionables) {
  actionables.nodes.forEach((node) => {
    // Retrieve the type of node
    switch (node.type) {
      // Process the mailer block
      case "@tripetto/block-mailer":
        // Find the recipient
        const recipient = node.data.find((data) => data.slot === "recipient")?.string || "";
        // Find the subject
        const subject = node.data.find((data) => data.slot === "subject")?.string || "";
        // Find the message.
        const message = node.data.find((data) => data.slot === "message")?.string || "-";
        // Find the sender.
        const sender = node.data.find((data) => data.slot === "sender")?.string || "";

        // Do some validation
        if (recipient && subject) {
          // Send the email message here
        }
        break;
    }
  });
}
```

</TabItem>
<TabItem value="PHP" label="PHP">

```php showLineNumbers
// Retrieve the actionables object from the POST data
$actionables = !empty($_POST["actionables"]) ? json_decode($_POST["actionables"]) : "";

// Check if it is valid
if (json_last_error() === JSON_ERROR_NONE && is_object($actionables)) {
    // Iterate through the nodes
    foreach ($actionables->nodes as $node) {
        // Retrieve the type of node
        switch ($node->type) {
            // Process the mailer block
            case "@tripetto/block-mailer":
                // Find the recipient
                $recipientField = array_filter($node->data, function ($data) {
                    return $data->slot == "recipient";
                });
                $recipient = count($recipientField) == 1 ? reset($recipientField)->string : "";

                // Find the subject
                $subjectField = array_filter($node->data, function ($data) {
                    return $data->slot == "subject";
                });
                $subject = count($subjectField) == 1 ? reset($subjectField)->string : "";

                // Find the message
                $messageField = array_filter($node->data, function ($data) {
                    return $data->slot == "message";
                });
                $message = count($messageField) == 1 ? reset($messageField)->string : "";

                // Find the sender
                $senderField = array_filter($node->data, function ($data) {
                    return $data->slot == "sender";
                });
                $sender = count($senderField) == 1 ? reset($senderField)->string : "";

                // Do some validation
                if (!empty($recipient) && !empty($subject)) {
                    // Send the email message here
                    mail($recipient, $subject, $message, !empty($sender) ? "From: " . $sender : "");
                }
                break;
        }
    }
}
```

</TabItem>
</Tabs>

## 🧱 Blocks that need post-processing {#blocks}
The following blocks used in the stock runners need post-processing:

- [Mailer block](../../stock/mailer.mdx)
