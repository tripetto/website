---
title: Block icon - Custom blocks
sidebar_label: Block icon
sidebar_position: 1
description: The block icon is used in the builder as a visual indication for the blocks.
---

import Preview from '@site/src/components/preview.js';

# Block icon
The block icon is used in the [builder](../../../builder/introduction.md) as a visual indication for the blocks. It is supplied by the builder part of a block in the block register decorator [`@tripetto`](../../api/decorators/tripetto.mdx). The icon is used in multiple locations in the builder, as you can see in the screenshot below:

<Preview src="builder-blocks.png" width="800" spacer={true} />

You may have noticed that the icon is rendered in different colors depending on the occasion. To make this possible, it is necessary to follow two guidelines for the icon.

## 1️⃣ Guideline 1
Supply the icon as a [Base64 encoded](https://www.base64-image.de/) SVG image instead of a URL. The code of a Base64 encoded image is a string and looks something like this:

```
data:image/svg+xml;base64,PHN2ZyAvPg==
```
:::caution
Make sure to always include the `data:image/svg+xml;base64,` part.
:::

### Supply as string
You can supply such a string directly to the [`icon`](../../api/decorators/tripetto.mdx#INodeBlockDecorator-icon) property of the [`@tripetto`](../../api/decorators/tripetto.mdx) decorator, as shown in the following example:

```ts showLineNumbers
import { tripetto, NodeBlock } from "@tripetto/builder";

@tripetto({
  type: "node",
  identifier: "example",
  label: "Example",
  //highlight-start
  icon: "data:image/svg+xml;base64,PHN2ZyAvPg=="
  //highlight-end
})
class ExampleBlock extends NodeBlock {}
```

### Supply using webpack
When using [webpack](https://webpack.js.org/), you can also configure it to process SVG images automatically and encode them. This makes it possible to directly import an SVG image in the source file of the block. It keeps your code tidy. To use that, you need to add the packages [image-webpack-loader](https://www.npmjs.com/package/image-webpack-loader) and [url-loader](https://www.npmjs.com/package/url-loader) to your project:

```bash npm2yarn
npm install image-webpack-loader url-loader
```

After that, you need to update your webpack configuration and add the highlighted lines:

```js showLineNumbers title="webpack.config.js"
module.exports = {
  //highlight-start
  module: {
    rules: [
      {
        test: /\.svg$/,
        use: ["url-loader", "image-webpack-loader"],
      },
    ],
  },
  //highlight-end
};
```

And then, you can import the icon directly, as shown in the following example:

```ts showLineNumbers
import { tripetto, NodeBlock } from "@tripetto/builder";
//highlight-start
import icon from "./icon.svg";
//highlight-end

@tripetto({
  type: "node",
  identifier: "example",
  label: "Example",
  //highlight-start
  icon
  //highlight-end
})
class ExampleBlock extends NodeBlock {}
```

## 2️⃣ Guideline 2
The second guideline is to customize the SVG image so Tripetto can control its primary color. You can apply specific classes to the [class attribute](https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/class) of elements in your SVG image to do so. There are two classes available:

- `tripetto-fill`: Changes the fill color of an element;
- `tripetto-stroke`: Changes the stroke color of an element.

For both classes, the same color is applied. So this works best with images that only need one color (like the icons we use for our [stock blocks](../../stock/index.mdx)).

### Example
In this example, the defined stroke color in the SVG image is blue. But because the class `tripetto-stroke` is applied, Tripetto can now change that color into something else during runtime.
```html showLineNumbers
<svg viewBox="0 0 10 10" xmlns="http://www.w3.org/2000/svg">
  <circle cx="5" cy="5" r="4" stroke="blue" class="tripetto-stroke" />
</svg>
```
