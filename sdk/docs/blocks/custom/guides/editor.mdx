---
title: Block editor - Custom blocks
sidebar_label: Block editor
sidebar_position: 3
description: Tripetto uses editor panels for managing the properties of blocks. Editor panels slide into view whenever the user wants to edit the properties of a block.
---

import Preview from '@site/src/components/preview.js';

# Block editor
Tripetto uses editor panels for managing the properties of blocks. Editor panels slide into view whenever the user wants to edit the properties of a block. The editor panel itself consists of two parts. The **features** part on the left side contains a list of all features that can be activated (or deactivated) for a block. The right side of the editor panel contains the **feature cards** for all activated features. Each feature card holds the settings for that particular feature. The idea behind this concept is to keep the screen as tidy as possible by only showing the relevant settings.

<Preview src="block-editor.png" width="800" spacer={true} />

To define the features of an editor panel, you need to add a method to your block class. This method needs to be decorated with the [`@editor`](../../api/decorators/editor.mdx) decorator. This instructs the builder to invoke the method whenever it needs the editor panel for a block or item. Here is an example of what that looks like in your code:

```ts showLineNumbers title="example-block.ts"
import { tripetto, editor, NodeBlock } from "@tripetto/builder";

@tripetto({
  type: "node",
  identifier: "example",
  label: "Example block",
  icon: "data:image/svg+xml;base64,PHN2ZyAvPg=="
})
class ExampleBlock extends NodeBlock {
  //highlight-start
  @editor
  onEdit(): void {
    this.editor.groups.general();
    this.editor.name();
    this.editor.description();
  }
  //highlight-end
}
```

You can use the [`editor`](../../api/classes/NodeBlock.mdx#editor) field of a the block to specify features for the editor panel. There a ready-made feature cards available for common used features, like the [name](../../api/classes/EditorOrchestrator.mdx#name) and [description](../../api/classes/EditorOrchestrator.mdx#description) of a block. But you can also define custom feature cards for managing the properties of a certain feature.

## 🗂️ Common used feature cards
- [`alias`](../../api/classes/EditorOrchestrator.mdx#alias): Adds the alias feature that manages the `alias` property of an object;
- [`description`](../../api/classes/EditorOrchestrator.mdx#description): Adds the description feature which manages the [`description`](../../api/classes/Node.mdx#description) property of a [`Node`](../../api/classes/Node.mdx);
- [`collection`](../../api/classes/EditorOrchestrator.mdx#collection): Adds a collection control that can manage the items in a collection;
- [`explanation`](../../api/classes/EditorOrchestrator.mdx#explanation): Adds the explanation feature which manages the [`explanation`](../../api/classes/Node.mdx#explanation) property of a [`Node`](../../api/classes/Node.mdx);
- [`exportable`](../../api/classes/EditorOrchestrator.mdx#exportable): Adds the exportable feature that manages the `exportable` property of an object;
- [`form`](../../api/classes/EditorOrchestrator.mdx#form): Adds a custom form with an invisible feature;
- [`group`](../../api/classes/EditorOrchestrator.mdx#group): Adds a group label in the features list;
- [`groups`](../../api/classes/EditorOrchestrator.mdx#groups): Contains croup templates with common used groups titles;
- [`name`](../../api/classes/EditorOrchestrator.mdx#name): Adds the name feature which manages the [`name`](../../api/classes/Node.mdx#name) property of a [`Node`](../../api/classes/Node.mdx);
- [`numeric`](../../api/classes/EditorOrchestrator.mdx#numeric): Adds a numeric feature;
- [`option`](../../api/classes/EditorOrchestrator.mdx#option): Adds an optional feature with a form or collection;
- [`placeholder`](../../api/classes/EditorOrchestrator.mdx#placeholder): Adds the placeholder feature which manages the [`placeholder`](../../api/classes/Node.mdx#placeholder) property of a [`Node`](../../api/classes/Node.mdx);
- [`required`](../../api/classes/EditorOrchestrator.mdx#required): Adds the required feature that manages the `required` property of an object;
- [`scores`](../../api/classes/EditorOrchestrator.mdx#scores): Adds the scores feature;
- [`transformations`](../../api/classes/EditorOrchestrator.mdx#transformations): Adds the transformations feature that manages the `transformation` property of an object;
- [`visibility`](../../api/classes/EditorOrchestrator.mdx#visibility): Adds the visibility feature which manages the [`disabled`](../../api/classes/Node.mdx#disabled) property of a [`Node`](../../api/classes/Node.mdx).

## 👩‍💻 Custom features
You can define custom features to manage additional settings for a block.

### Toggleable features
Toggleable features are features that can be activated or deactivated by the builder user. They are used for managing optional settings for a block. Use the [`option`](../../api/classes/EditorOrchestrator.mdx#option) method to define a new feature. You can specify a form or a collection as a feature card. For example:

```ts showLineNumbers
import { tripetto, editor, isBoolean, NodeBlock } from "@tripetto/builder";

@tripetto({
  type: "node",
  identifier: "example-block",
  label: "Example",
  icon: "data:image/svg+xml;base64,PHN2ZyAvPg=="
})
class ExampleBlock extends NodeBlock {
  @definition
  example?: boolean;

  @editor
  onEdit(): void {
    //highlight-start
    this.editor.option({
      // The feature is activated when `example` is a valid boolean value
      activated: isBoolean(this.example),
      name: "Example feature",
      form: {
        title: "Feature for managing the optional example field",
        controls: [
          new Forms.Checkbox(
            "Example checkbox",
            // When the feature is activated and `example` is undefined, its initial value is set to `true`
            Forms.Checkbox.bind(this, "example", undefined, true)
          )
        ]
      }
    });
    //highlight-end
  }
}
```

### Fixed form cards
If you want to add a fixed form (without a toggleable feature), you can use the [`form`](../../api/classes/EditorOrchestrator.mdx#form) method to define a fixed feature card. Fixed cards are not listed in the features list on the left side of an editor panel. Here is an example:

```ts showLineNumbers
import { tripetto, editor, NodeBlock } from "@tripetto/builder";

@tripetto({
  type: "node",
  identifier: "example-block",
  label: "Example",
  icon: "data:image/svg+xml;base64,PHN2ZyAvPg=="
})
class ExampleBlock extends NodeBlock {
  @definition
  example: boolean;

  @editor
  onEdit(): void {
    //highlight-start
    this.editor.form({
      title: "Toggle the example property",
      controls: [
        new Forms.Checkbox(
          "Example checkbox",
          Forms.Checkbox.bind(this, "example", false)
        )
      ]
    });
    //highlight-end
  }
}
```

### Form controls
When you define a form card, you can choose from the following form controls:
- [`Button`](../../api/modules/Forms/Button.mdx): Defines a button for a form;
- [`Checkbox`](../../api/modules/Forms/Checkbox.mdx): Defines a checkbox control for a form;
- [`ColorPicker`](../../api/modules/Forms/ColorPicker.mdx): Defines a color picker control for a form;
- [`DateTime`](../../api/modules/Forms/DateTime.mdx): Defines a date/time control for a form;
- [`Dropdown`](../../api/modules/Forms/Dropdown.mdx): Defines a dropdown control for a form;
- [`Email`](../../api/modules/Forms/Email.mdx): Defines an email address input control for a form;
- [`Group`](../../api/modules/Forms/Group.mdx): Defines a group of controls for a form;
- [`HTML`](../../api/modules/Forms/HTML.mdx): Defines static HTML for a form;
- [`Notification`](../../api/modules/Forms/Notification.mdx): Defines a notification bar for a form;
- [`Numeric`](../../api/modules/Forms/Numeric.mdx): Defines a number input control for a form;
- [`Radiobutton`](../../api/modules/Forms/Radiobutton.mdx): Defines a radio button control for a form;
- [`Spacer`](../../api/modules/Forms/Spacer.mdx): Defines a spacer for a form to add space between two controls;
- [`Static`](../../api/modules/Forms/Static.mdx): Defines static text for a form;
- [`Text`](../../api/modules/Forms/Text.mdx): Defines a text input control for a form.

