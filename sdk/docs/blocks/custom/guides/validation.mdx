---
title: Validation - Custom blocks
sidebar_label: Validation
sidebar_position: 6
description: Validation is applied in the runner part of a block to check whether a block meets all the requirements.
---

# Validation
Validation is applied in the runner part of a block to check whether a block meets all the requirements. Only a form where all blocks pass the validation can be completed. For example, a required text input field must have a valid value for the block validation to be successful. There are two types of validation that can apply to a block:
- [Automatic validation](#automatic);
- [Custom validation](#custom).

:::info
Validation only applies to the runner part of a block based on the [`NodeBlock`](../../../runner/api/library/classes/NodeBlock.mdx) or [`HeadlessBlock`](../../../runner/api/library/classes/HeadlessBlock.mdx) classes.
:::

## 🤖 Automatic validation {#automatic}
Automatic validation is applied for all [required](../../api/modules/Slots/Slot.mdx#required) slots. A required slot needs to have a value set before the validation of the block can pass. Therefore, for a mandatory text input field, no custom validation code is needed, as the automatic validation will suffice.

:::info
Automatic validation is always applied and cannot be disabled.
:::

## 👩‍💻 Custom validation {#custom}
If your block needs more advanced validation, you can implement your custom validation code. To supply your custom validation, you need to add a validation method to the runner part of the block class. This method then needs to be decorated with the [`@validator`](../../../runner/api/library/decorators/validator.mdx) decorator. This decorator instructs the runner to execute the validation method whenever the validation state of the block is requested. There are two modes of operation for custom validation:
- [Synchronous validation](#synchronous-validation)
- [Asynchronous validation](#asynchronous-validation)

:::caution
Custom validation is only applied when the automatic validation is passed. Custom validation never replaces automatic validation.
:::

### Synchronous validation
Synchronous validation is the most common mode for validation. The validation method is simple in this case. It should return `true` if the validation passes or `false` if it fails. In the following example, the validation method checks if a slot has a string value that has more than 2 characters:

```ts showLineNumbers
import { tripetto, validator, NodeBlock, Callback } from "@tripetto/runner";

@tripetto({
  type: "node",
  identifier: "example",
})
class ExampleBlock extends NodeBlock {
  //highlight-start
  @validator
  validate(): boolean {
    // Retrieve the slot with reference `value`
    const slot = this.valueOf("value");

    // Check if the slot is valid and has more than 2 characters
    return slot && slot.string.length > 2 ? true : false;
  }
  //highlight-end
}
```

### Asynchronous validation
Asynchronous validation is useful when the validation action involves an asynchronous process. For example, a block that uses an external API to verify a value.

:::info
To indicate asynchronous validation, the validation method should implement the `callback` argument and return it as return value.
:::

:::info
Whenever asynchronous validation is active, the [`isEvaluating`](../../../runner/api/library/classes/Storyline.mdx#isEvaluating) field of the runner may be set to `true` in some cases (for example, if all other blocks pass validation and the whole form waits for this last asynchronous validation result). Runners can use this to indicate the form respondent that the form is "busy" (for example, by showing a loading spinner in the UI).
:::


```ts showLineNumbers
import { tripetto, validator, NodeBlock, Callback } from "@tripetto/runner";

@tripetto({
  type: "node",
  identifier: "example",
})
class ExampleBlock extends NodeBlock {
  //highlight-start
  @validator
  validate({ callback }: { callback: Callback<boolean> }): Callback<boolean> {
    // Retrieve the slot with reference `value`
    const slot = this.valueOf("value");

    // Post it to an endpoint
    fetch("/example-server", {
        method: "POST",
        body: slot.string,
      })
      .then((response) => {
        if (response.ok) {
          // All ok, validation passed!
          callback.return(true);
        } else {
          // Not so good, validation failed!
          callback.return(false);
        }
      });

    // Indicate asynchronous validation
    return callback;
  }
  //highlight-end
}
```
