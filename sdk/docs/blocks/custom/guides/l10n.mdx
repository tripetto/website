---
title: Translations - Custom blocks
sidebar_label: Translations
sidebar_position: 9
description: If you want to make it possible to translate your custom block, you need to use Tripetto's built-in gettext functions.
---

# Block translations
If you want to make it possible to translate your custom block, you need to use Tripetto's built-in [gettext](../../api/modules/L10n/index.mdx) functions (based on the GNU [gettext](https://en.wikipedia.org/wiki/Gettext) system) for all the static translatable labels and texts in your block.

## 🏗️ Builder part
For the builder part of your block, you need to pull all your static labels and texts through one of the translation functions. The builder package contains the following functions for translating the builder part of a block:
- [_](../../api/functions/_.mdx) *(the default function to use)*
- [_n](../../api/functions/_n.mdx) *(the default function to use for plural forms)*
- [npgettext](../../api/functions/npgettext.mdx)
- [pgettext](../../api/functions/pgettext.mdx)

:::tip
In most situations you just want to use the [_](../../api/functions/_.mdx) and [_n](../../api/functions/_n.mdx) functions.
:::

### Example
Here is an example of a translatable block:
:::info
Pay special attention to the block [`label`](../../api/decorators/tripetto.mdx#INodeBlockDecorator-label) in the [`@tripetto`](../../api/decorators/tripetto.mdx) decorator. We need to define that property using a getter. This assures that the correct translation is used for the block label.
:::
```ts showLineNumbers
//highlight-start
import { _, tripetto, slots, definition, editor, isBoolean, NodeBlock, Slots, Forms } from "@tripetto/builder";
//highlight-end
import icon from "./icon.svg";

@tripetto({
  type: "node",
  identifier: "text-input",
  //highlight-start
  get label() {
    return _("Text input");
  },
  //highlight-end
  icon
})
class TextInputBlock extends NodeBlock {
  @definition
  isMultiline?: boolean;

  // Add a field that holds a reference to the slot
  valueSlot!: Slots.String;

  @slots
  onSlots(): void {
    this.valueSlot = this.slots.static({
      type: Slots.String,
      reference: "value",
      //highlight-start
      label: _("Text input value")
      //highlight-end
    });
  }

  @editor
  onEdit(): void {
    this.editor.groups.general();
    this.editor.name();
    this.editor.placeholder();
    this.editor.groups.settings();
    this.editor.required(this.valueSlot);
    this.editor.alias(this.valueSlot);
    this.editor.exportable(this.valueSlot);

    this.editor.option({
      //highlight-start
      name: _("Input mode"),
      //highlight-end
      form: {
        //highlight-start
        title: _("Text input mode"),
        //highlight-end
        controls: [
          new Forms.Checkbox(
            //highlight-start
            _("Allow multi-line text input"),
            //highlight-end
            Forms.Checkbox.bind(this, "isMultiline", false, true)
          )
        ],
      },
      activated: isBoolean(this.isMultiline)
    });
  }
}
```

## 🏃 Runner part
For the runner part, the procedure is identical to that for the builder part. You need to pull all your static labels and texts through one of the translation functions. The Runner library contains the following functions for translating the runner part of a block:
- [L10n._](../../../runner/api/library/modules/L10n/index.mdx#_) *(the default function to use)*
- [L10n._n](../../../runner/api/library/modules/L10n/index.mdx#_n) *(the default function to use for plural forms)*
- [L10n.dgettext](../../../runner/api/library/modules/L10n/index.mdx#dgettext)
- [L10n.dngettext](../../../runner/api/library/modules/L10n/index.mdx#dngettext)
- [L10n.dnpgettext](../../../runner/api/library/modules/L10n/index.mdx#dnpgettext)
- [L10n.dpgettext](../../../runner/api/library/modules/L10n/index.mdx#dpgettext)
- [L10n.gettext](../../../runner/api/library/modules/L10n/index.mdx#gettext)
- [L10n.ngettext](../../../runner/api/library/modules/L10n/index.mdx#ngettext)
- [L10n.npgettext](../../../runner/api/library/modules/L10n/index.mdx#npgettext)
- [L10n.pgettext](../../../runner/api/library/modules/L10n/index.mdx#pgettext)

:::info Stock runners
If you are building the runner part of a block for use in one of the stock runners, you should use the [`l10n`](../../../runner/api/stock/autoscroll/interfaces/IAutoscrollRenderProps.mdx#l10n) property that is supplied to the render method, as shown in the following example:

```ts showLineNumbers
import { tripetto, NodeBlock } from "@tripetto/runner";
import { namespace, IAutoscrollRenderProps, IAutoscrollRendering } from "@tripetto/runner-autoscroll";
import { ReactNode } from "react";

@tripetto({
    type: "node",
    namespace,
    identifier: "your-custom-block",
})
export class YourCustomBlock extends NodeBlock implements IAutoscrollRendering {
    render(props: IAutoscrollRenderProps): ReactNode {
        return (
            <>
                //highlight-start
                {props.l10n._("Welcome to your custom block!")}
                //highlight-end
            </>
        );
    }
}
```
:::

## 🌎 Creating translations
If you want to create translations for your block, you first need to generate the `POT` file for it. You can use the [`xgettext`](https://www.gnu.org/software/gettext/manual/html_node/xgettext-Invocation.html) tool for that. This will extract all translatable strings from your block code. Have a look at the [block boilerplate](boilerplate.mdx) for an example of how to do that.

## 📂 Loading translations
▶️ More information about loading **builder part** translations in the [Builder localization guide](../../../builder/integrate/guides/l10n.mdx#blocks).

▶️ More information about loading **runner part** translations in the [Stock runner localization guide](../../../runner/stock/guides/l10n.mdx#translations).
