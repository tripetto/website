---
title: Prerequisites - Custom blocks
sidebar_label: Prerequisites
sidebar_position: 2
description: Overview of prerequisites to develop a custom block.
---

# Prerequisites
To develop a custom block for Tripetto you need programming skills. [TypeScript](https://www.typescriptlang.org/) is required when building custom blocks, because we rely heavily on the use of [decorators](https://www.typescriptlang.org/docs/handbook/decorators.html). Also, you need a bundler like [webpack](https://webpack.js.org/) to bundle all the block assets.

:::info
Tripetto uses [decorators](https://www.typescriptlang.org/docs/handbook/decorators.html) for building custom blocks. Therefore, you should always use [TypeScript](https://www.typescriptlang.org/) and enable the [`experimentalDecorators`](https://www.typescriptlang.org/tsconfig#experimentalDecorators) feature of it.
:::

:::danger Caution when using Babel
If you use [Babel](https://babeljs.io/) in your project, you need an additional build step for the builder part of a custom block. That's because the implementation of decorators slightly differs between TypeScript and Babel. The code that Babel produces can therefore not run properly out-of-the-box. The solution is simple: If you are using Babel, add an additional build step that compiles the builder part of a block from TypeScript to JavaScript ES5 (not ES6!) code first (using the [TypeScript](https://www.typescriptlang.org/) compiler). This assures the decorators are compiled the right way.
:::
