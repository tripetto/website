---
title: Custom blocks
sidebar_label: Introduction
sidebar_position: 1
description: You can extend Tripetto with more question types and functionality by building a custom block.
---

# Custom blocks
You can extend Tripetto with more question types and functionality by building a custom block. Do you need a question type that's not available as a [stock block](../stock/index.mdx)? Or want to develop a condition block that controls if certain branches of the form are taken or not? Or perhaps build a block that performs a certain operation for the form. Anything is possible!

🎓 This chapter contains all the information you need to **start developing a custom block**.

✍️ Building a custom block requires **TypeScript programming skills**.

📦 You will use the builder and Runner library, both published on **npm**.

:::tip
If you haven't read the [introduction](../introduction.md) to Tripetto blocks yet, we recommend doing that first. It explains the types of blocks you can develop and introduces some terms we use in the rest of the documentation.
:::

## 🚀 Quickstart {#quickstart}
Follow one of the quick start tutorials to jumpstart developing a custom block. When you have the basic implementation of your block ready, have a look at the [guides](guides/index.md) to add more features to your block. But first, what kind of block do you want to develop?

▶️ [Visual block](implement/visual.mdx): A new visual block (question type) for a form;

▶️ [Condition block](implement/condition.mdx): A condition block for use in conditional branches of a form;

▶️ [Headless block](implement/headless.mdx): An invisible block that performs a certain operation for a form.
