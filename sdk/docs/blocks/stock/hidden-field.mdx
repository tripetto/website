---
title: Hidden field - Stock blocks
sidebar_label: Hidden field
description: Use the hidden field block to gather and store data from outside the form.
---

import Preview from '@site/src/components/preview.js';
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Hidden field block
Use the hidden field block to gather and store data from outside the form.

:::info
This is a headless block. It doesn't require a UI implementation in the runner. See the [runner](#usage-runner) section for instructions on how to use the block in a runner.
:::

## 📺 Preview {#preview}
<Preview src="block-hidden-field.png" />

## 📽️ Demo {#demo}
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/KKZjyVa/4e88d5e7e0fc62151306f63b651b8569) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/KKZjyVa/4e88d5e7e0fc62151306f63b651b8569)

## 📦 Package contents {#package}

### 🏗️ Builder part {#package-builder}
The builder part instructs the [builder](../../builder/introduction.md) how to manage the block. It defines the configurable properties and settings for the block, which can then be managed with the visual builder. This package contains:

- Classes:
  - `HiddenField`: Hidden field headless block;
  - `HiddenFieldDateCondition`: Hidden field condition block to verify a date;
  - `HiddenFieldNumberCondition`: Hidden field condition block to verify a number;
  - `HiddenFieldOrientationCondition`: Hidden field condition block to verify an orientation;
  - `HiddenFieldStringCondition`: Hidden field condition block to verify a string;
- TypeScript typings (should work out-of-the-box);
- Translations (located in the `./translations` folder of the package).

### 🏃 Runner part {#package-runner}
The runner part of this headless block contains all the code to use the block in a [runner](../../runner/introduction.md) (the thing that runs the form). This package contains:

- Classes:
  - `HiddenField`: Base class for implementing the hidden field block in a runner;
  - `HiddenFieldDateCondition`: Runner part of the hidden field condition block for dates;
  - `HiddenFieldNumberCondition`: Runner part of the hidden field condition block for numbers;
  - `HiddenFieldOrientationCondition`: Runner part of the hidden field condition block for orientations;
  - `HiddenFieldStringCondition`: Runner part of the hidden field condition block for strings;
- TypeScript typings (should work out-of-the-box).

## 👩‍💻 Usage {#usage}

### ✨ Installation {#usage-installation}
```bash npm2yarn
npm install @tripetto/block-hidden-field
```

:::tip Typescript support
This package contains type declarations and supports [TypeScript](https://www.typescriptlang.org/) out-of-the-box.
:::

### 🏗️ Builder part {#usage-builder}

#### ESM/ES5
Importing the block is all you need to do to self-register the block to the builder (see the [builder plain JS implementation](../../builder/integrate/quickstart/plain-js.mdx#blocks) for an example or the [Block loading guide](../../builder/integrate/guides/blocks.mdx) for more information about loading blocks).
```ts showLineNumbers
import "@tripetto/block-hidden-field";

// The code above imports the ES5 or ESM version based on your project configuration.
// If you want to use the ES5 version, you can do an explicit import:
import "@tripetto/block-hidden-field/es5";
```

#### CDN
You can also use the builder part directly in a browser using a CDN (see the [builder HTML implementation](../../builder/integrate/quickstart/html.mdx#blocks) for more information).

<Tabs groupId="cdn">
<TabItem value="jsdelivr" label="jsDelivr">

```html showLineNumbers
<html>
  <body>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/builder"></script>
    //highlight-start
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-hidden-field"></script>
    //highlight-end
    <script>
      Tripetto.Builder.open();
    </script>
  </body>
</html>
```

</TabItem>
<TabItem value="unpkg" label="unpkg">

```html showLineNumbers
<html>
  <body>
    <script src="https://unpkg.com/@tripetto/builder"></script>
    //highlight-start
    <script src="https://unpkg.com/@tripetto/block-hidden-field"></script>
    //highlight-end
    <script>
      Tripetto.Builder.open();
    </script>
  </body>
</html>
```

</TabItem>
</Tabs>

#### Custom variables
You can extend the hidden field block with more variables. Therefore you need to supply the variables to the builder part of the block, as shown in the following example:

```ts showLineNumbers
import { HiddenField } from "@tripetto/block-hidden-field";

HiddenField.customVariables = {
  // This is the name of the group that contains the custom variables
  name: "Some custom variables",

  // Here you can list all the variables you want to add
  variables: [
    {
      name: "EXAMPLE1",
      description: "Example 1"
    },
    {
      name: "EXAMPLE2",
      description: "Example 2"
    },
  ]
}
```

### 🏃 Runner part {#usage-runner}
Since this is a headless block, the runner part contains everything you need to use this block in a [runner](../../runner/introduction.md) (the thing that runs the form). You only need to import the block somewhere in your runner project, and you are good to go:

```ts showLineNumbers
import "@tripetto/block-hidden-field/runner";

// The code above imports the ES5 or ESM version based on your project configuration.
// If you want to use the ES5 version, you can do an explicit import:
import "@tripetto/block-hidden-field/runner/es5";
```

#### Custom variables
If you'd supplied custom variables to the builder part, you need to make the variables available to the runner part. To do so, you need to add a global variable named `TRIPETTO_CUSTOM_VARIABLES` that contains an object with all the custom variables and their values:

```ts
window.TRIPETTO_CUSTOM_VARIABLES = {
  "EXAMPLE1": "Value for example 1",
  "EXAMPLE2": "Value for example 2"
};
```

When a custom variable is selected for the hidden field, the runner part will try to retrieve the variable value from `TRIPETTO_CUSTOM_VARIABLES`.

## 🎭 Stock runners {#stock-runners}
This block is included in the following stock runners:
- [Autoscroll runner](../../runner/stock/faces/autoscroll.mdx)
- [Chat runner](../../runner/stock/faces/chat.mdx)
- [Classic runner](../../runner/stock/faces/classic.mdx)

:::tip
If you are integrating the [builder](../../builder/integrate/introduction.md) together with one of the [stock runners](../../runner/stock/introduction.md), you can use the builder block bundle that is included in the stock runner packages to load the builder part of all the blocks with a single import. See the [Import block bundles guide](../../builder/integrate/guides/blocks.mdx#import-block-bundles) for more information.
:::

## 🚢 Distribution [![](/img/logo-npm-icon.svg)](https://www.npmjs.com/package/@tripetto/block-hidden-field) {#distribution}
This block is distributed through [npm](https://www.npmjs.com/package/@tripetto/block-hidden-field):

▶️ https://www.npmjs.com/package/@tripetto/block-hidden-field

## 📁 Source code [![](/img/logo-gitlab.svg)](https://gitlab.com/tripetto/blocks/hidden-field) {#source}
This block is open-source and the code is on [GitLab](https://gitlab.com/tripetto/blocks/hidden-field):

▶️ https://gitlab.com/tripetto/blocks/hidden-field
