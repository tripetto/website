---
title: Mailer - Stock blocks
sidebar_label: Mailer
description: Use the mailer block to send an email message from a form to a fixed email address or an email address from the dataset of a form.
---

import Preview from '@site/src/components/preview.js';
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Mailer block
Use the mailer block to send an email message from a form to a fixed email address or an email address from the dataset of a form.

:::info
This is a headless block. It doesn't require a UI implementation in the runner. See the [runner](#usage-runner) section for instructions on how to use the block in a runner.
:::

:::caution
This block requires post-processing. See the [post-processing](#usage-post-processing) section to learn what needs to be processed for this block.
:::

## 📺 Preview {#preview}
<Preview src="block-mailer.png" />

## 📽️ Demo {#demo}
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/oNprWKa/3fcfbb0cb484a0fb35fd9d31274b233c) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/oNprWKa/3fcfbb0cb484a0fb35fd9d31274b233c)

## 📦 Package contents {#package}

### 🏗️ Builder part {#package-builder}
The builder part instructs the [builder](../../builder/introduction.md) how to manage the block. It defines the configurable properties and settings for the block, which can then be managed with the visual builder. This package contains:

- Classes:
  - `Mailer`: Mailer headless block;
  - `MailerCondition`: Mailer condition block to verify if the mail will be sent;
- TypeScript typings (should work out-of-the-box);
- Translations (located in the `./translations` folder of the package).

### 🏃 Runner part {#package-runner}
The runner part of this headless block contains all the code to use the block in a [runner](../../runner/introduction.md) (the thing that runs the form). This package contains:

- Classes:
  - `Mailer`: Class for implementing the mailer block in a runner;
  - `MailerCondition`: Runner part of the mailer condition block;
- TypeScript typings (should work out-of-the-box).

## 👩‍💻 Usage {#usage}

### ✨ Installation {#usage-installation}
```bash npm2yarn
npm install @tripetto/block-mailer
```

:::tip Typescript support
This package contains type declarations and supports [TypeScript](https://www.typescriptlang.org/) out-of-the-box.
:::

### 🏗️ Builder part {#usage-builder}

#### ESM/ES5
Importing the block is all you need to do to self-register the block to the builder (see the [builder plain JS implementation](../../builder/integrate/quickstart/plain-js.mdx#blocks) for an example or the [Block loading guide](../../builder/integrate/guides/blocks.mdx) for more information about loading blocks).
```ts showLineNumbers
import "@tripetto/block-mailer";

// The code above imports the ES5 or ESM version based on your project configuration.
// If you want to use the ES5 version, you can do an explicit import:
import "@tripetto/block-mailer/es5";
```

#### CDN
You can also use the builder part directly in a browser using a CDN (see the [builder HTML implementation](../../builder/integrate/quickstart/html.mdx#blocks) for more information).

<Tabs groupId="cdn">
<TabItem value="jsdelivr" label="jsDelivr">

```html showLineNumbers
<html>
  <body>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/builder"></script>
    //highlight-start
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-mailer"></script>
    //highlight-end
    <script>
      Tripetto.Builder.open();
    </script>
  </body>
</html>
```

</TabItem>
<TabItem value="unpkg" label="unpkg">

```html showLineNumbers
<html>
  <body>
    <script src="https://unpkg.com/@tripetto/builder"></script>
    //highlight-start
    <script src="https://unpkg.com/@tripetto/block-mailer"></script>
    //highlight-end
    <script>
      Tripetto.Builder.open();
    </script>
  </body>
</html>
```

</TabItem>
</Tabs>

#### Translations
The available translation for the block are located in the `translations` folder of the package.

:::tip
See the [Loading a translation for a stock block guide](../../builder/integrate/guides/l10n.mdx#loading-a-translation-for-a-stock-block) to learn how to load the block translation into the builder.
:::

### 🏃 Runner part {#usage-runner}
Since this is a headless block, the runner part contains everything you need to use this block in a [runner](../../runner/introduction.md) (the thing that runs the form). You only need to import the block somewhere in your runner project, and you are good to go:

```ts showLineNumbers
import "@tripetto/block-mailer/runner";

// The code above imports the ES5 or ESM version based on your project configuration.
// If you want to use the ES5 version, you can do an explicit import:
import "@tripetto/block-mailer/runner/es5";
```

### 🍳 Post-processing {#usage-post-processing}
This mailer block allows forms to send email messages. Of course, that's not possible from the runner itself (since it runs on the client). So, the actual sending of the messages needs to be performed by an endpoint. A good place to handle this is on the same endpoint that receives the form data when a respondent completes the form. Tripetto contains a special export function to export all the so-called [actionable](../../runner/api/library/modules/Export.mdx#actionables) data. This is a separate data structure you could send to your endpoint when a form completes (see the [Post-processing guide](../custom/guides/post-processing.mdx#obtaining-data) for an example). It only contains the data that needs post-processing. This makes it easier to process as you don't have to iterate through the form data itself to get the required data for the post-processing.

:::tip
See the [Post-processing guide](../custom/guides/post-processing.mdx) for more information about post-processing.
:::

#### Data fields
The mailer block supplies the following data fields:
- `recipient`: Contains the email address of the recipient;
- `subject`: Contains the subject for email message;
- `message`: Contains the email message itself;
- `sender`: Contains the sender address.

#### Example
Here is an example for Node.js and PHP for extracting the data fields:

<Tabs>
<TabItem value="typescript" label="Node.js (TypeScript)">

```ts showLineNumbers
// Retrieve the actionables object from the POST data
const actionables = req.body.actionables;

// Iterate through the nodes
if (actionables) {
  actionables.nodes.forEach((node) => {
    // Retrieve the type of node
    switch (node.type) {
      // Process the mailer block
      case "@tripetto/block-mailer":
        // Find the recipient
        const recipient = node.data.find((data) => data.slot === "recipient")?.string || "";
        // Find the subject
        const subject = node.data.find((data) => data.slot === "subject")?.string || "";
        // Find the message.
        const message = node.data.find((data) => data.slot === "message")?.string || "-";
        // Find the sender.
        const sender = node.data.find((data) => data.slot === "sender")?.string || "";

        // Do some validation
        if (recipient && subject) {
          // Send the email message here
        }
        break;
    }
  });
}
```

</TabItem>
<TabItem value="PHP" label="PHP">

```php showLineNumbers
// Retrieve the actionables object from the POST data
$actionables = !empty($_POST["actionables"]) ? json_decode($_POST["actionables"]) : "";

// Check if it is valid
if (json_last_error() === JSON_ERROR_NONE && is_object($actionables)) {
    // Iterate through the nodes
    foreach ($actionables->nodes as $node) {
        // Retrieve the type of node
        switch ($node->type) {
            // Process the mailer block
            case "@tripetto/block-mailer":
                // Find the recipient
                $recipientField = array_filter($node->data, function ($data) {
                    return $data->slot == "recipient";
                });
                $recipient = count($recipientField) == 1 ? reset($recipientField)->string : "";

                // Find the subject
                $subjectField = array_filter($node->data, function ($data) {
                    return $data->slot == "subject";
                });
                $subject = count($subjectField) == 1 ? reset($subjectField)->string : "";

                // Find the message
                $messageField = array_filter($node->data, function ($data) {
                    return $data->slot == "message";
                });
                $message = count($messageField) == 1 ? reset($messageField)->string : "";

                // Find the sender
                $senderField = array_filter($node->data, function ($data) {
                    return $data->slot == "sender";
                });
                $sender = count($senderField) == 1 ? reset($senderField)->string : "";

                // Do some validation
                if (!empty($recipient) && !empty($subject)) {
                    // Send the email message here
                    mail($recipient, $subject, $message, !empty($sender) ? "From: " . $sender : "");
                }
                break;
        }
    }
}
```

</TabItem>
</Tabs>

## 🎭 Stock runners {#stock-runners}
This block is included in the following stock runners:
- [Autoscroll runner](../../runner/stock/faces/autoscroll.mdx)
- [Chat runner](../../runner/stock/faces/chat.mdx)
- [Classic runner](../../runner/stock/faces/classic.mdx)

:::tip
If you are integrating the [builder](../../builder/integrate/introduction.md) together with one of the [stock runners](../../runner/stock/introduction.md), you can use the builder block bundle that is included in the stock runner packages to load the builder part of all the blocks with a single import. See the [Import block bundles guide](../../builder/integrate/guides/blocks.mdx#import-block-bundles) for more information.
:::

## 🚢 Distribution [![](/img/logo-npm-icon.svg)](https://www.npmjs.com/package/@tripetto/block-mailer) {#distribution}
This block is distributed through [npm](https://www.npmjs.com/package/@tripetto/block-mailer):

▶️ https://www.npmjs.com/package/@tripetto/block-mailer

## 📁 Source code [![](/img/logo-gitlab.svg)](https://gitlab.com/tripetto/blocks/mailer) {#source}
This block is open-source and the code is on [GitLab](https://gitlab.com/tripetto/blocks/mailer):

▶️ https://gitlab.com/tripetto/blocks/mailer
