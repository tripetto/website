---
title: Setter - Stock blocks
sidebar_label: Setter
description: Use the setter block to set, change, clear and lock/unlock values in forms.
---

import Preview from '@site/src/components/preview.js';
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Setter block
Use the setter block to set, change, clear and lock/unlock values in forms.

:::info
This is a headless block. It doesn't require a UI implementation in the runner. See the [runner](#usage-runner) section for instructions on how to use the block in a runner.
:::

## 📺 Preview {#preview}
<Preview src="block-setter.gif" />

## 📽️ Demo {#demo}
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/NWXZwbR/eb6a19b3406971826661fe65be5eb2bf) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/NWXZwbR/eb6a19b3406971826661fe65be5eb2bf)

## 📦 Package contents {#package}

### 🏗️ Builder part {#package-builder}
The builder part instructs the [builder](../../builder/introduction.md) how to manage the block. It defines the configurable properties and settings for the block, which can then be managed with the visual builder. This package contains:

- Classes:
  - `Setter`: Setter headless block;
  - `Value`: Value item in the setter collection;
- TypeScript typings (should work out-of-the-box);
- Translations (located in the `./translations` folder of the package).

### 🏃 Runner part {#package-runner}
The runner part of this headless block contains all the code to use the block in a [runner](../../runner/introduction.md) (the thing that runs the form). This package contains:

- Classes:
  - `Setter`: Base class for implementing the setter block in a runner;
- TypeScript typings (should work out-of-the-box).

## 👩‍💻 Usage {#usage}

### ✨ Installation {#usage-installation}
```bash npm2yarn
npm install @tripetto/block-setter
```

:::tip Typescript support
This package contains type declarations and supports [TypeScript](https://www.typescriptlang.org/) out-of-the-box.
:::

### 🏗️ Builder part {#usage-builder}

#### ESM/ES5
Importing the block is all you need to do to self-register the block to the builder (see the [builder plain JS implementation](../../builder/integrate/quickstart/plain-js.mdx#blocks) for an example or the [Block loading guide](../../builder/integrate/guides/blocks.mdx) for more information about loading blocks).
```ts showLineNumbers
import "@tripetto/block-setter";

// The code above imports the ES5 or ESM version based on your project configuration.
// If you want to use the ES5 version, you can do an explicit import:
import "@tripetto/block-setter/es5";
```

#### CDN
You can also use the builder part directly in a browser using a CDN (see the [builder HTML implementation](../../builder/integrate/quickstart/html.mdx#blocks) for more information).

<Tabs groupId="cdn">
<TabItem value="jsdelivr" label="jsDelivr">

```html showLineNumbers
<html>
  <body>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/builder"></script>
    //highlight-start
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-setter"></script>
    //highlight-end
    <script>
      Tripetto.Builder.open();
    </script>
  </body>
</html>
```

</TabItem>
<TabItem value="unpkg" label="unpkg">

```html showLineNumbers
<html>
  <body>
    <script src="https://unpkg.com/@tripetto/builder"></script>
    //highlight-start
    <script src="https://unpkg.com/@tripetto/block-setter"></script>
    //highlight-end
    <script>
      Tripetto.Builder.open();
    </script>
  </body>
</html>
```

</TabItem>
</Tabs>

#### Translations
The available translation for the block are located in the `translations` folder of the package.

:::tip
See the [Loading a translation for a stock block guide](../../builder/integrate/guides/l10n.mdx#loading-a-translation-for-a-stock-block) to learn how to load the block translation into the builder.
:::

### 🏃 Runner part {#usage-runner}
Since this is a headless block, the runner part contains everything you need to use this block in a [runner](../../runner/introduction.md) (the thing that runs the form). You only need to import the block somewhere in your runner project, and you are good to go:

```ts showLineNumbers
import "@tripetto/block-setter/runner";

// The code above imports the ES5 or ESM version based on your project configuration.
// If you want to use the ES5 version, you can do an explicit import:
import "@tripetto/block-setter/runner/es5";
```

## 🎭 Stock runners {#stock-runners}
This block is included in the following stock runners:
- [Autoscroll runner](../../runner/stock/faces/autoscroll.mdx)
- [Chat runner](../../runner/stock/faces/chat.mdx)
- [Classic runner](../../runner/stock/faces/classic.mdx)

:::tip
If you are integrating the [builder](../../builder/integrate/introduction.md) together with one of the [stock runners](../../runner/stock/introduction.md), you can use the builder block bundle that is included in the stock runner packages to load the builder part of all the blocks with a single import. See the [Import block bundles guide](../../builder/integrate/guides/blocks.mdx#import-block-bundles) for more information.
:::

## 🚢 Distribution [![](/img/logo-npm-icon.svg)](https://www.npmjs.com/package/@tripetto/block-setter) {#distribution}
This block is distributed through [npm](https://www.npmjs.com/package/@tripetto/block-setter):

▶️ https://www.npmjs.com/package/@tripetto/block-setter

## 📁 Source code [![](/img/logo-gitlab.svg)](https://gitlab.com/tripetto/blocks/setter) {#source}
This block is open-source and the code is on [GitLab](https://gitlab.com/tripetto/blocks/setter):

▶️ https://gitlab.com/tripetto/blocks/setter
