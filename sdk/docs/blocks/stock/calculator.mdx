---
title: Calculator - Stock blocks
sidebar_label: Calculator
description: Use the calculator block to instantly perform calculations inside forms, using given answers.
---

import Preview from '@site/src/components/preview.js';
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Calculator block
Use the calculator block to instantly perform calculations inside forms, using given answers. The block includes operations, scores, comparisons, formulas, functions, constants and lots more features to be able to do all kinds of calculations.

:::info
This is a headless block. It doesn't require a UI implementation in the runner. See the [runner](#usage-runner) section for instructions on how to use the block in a runner.
:::

## 📺 Preview {#preview}
<Preview src="block-calculator.gif" />

## 📽️ Demo {#demo}
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/yLpdoKw/a426a8e1f34f2dce736b56000319f99e) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/yLpdoKw/a426a8e1f34f2dce736b56000319f99e)

## 🧮 Supported operations

| Operation | Description |
| --- | --- |
| **INPUTS** | |
| Static number | Supplies a static number to the calculator. |
| Recall value | Supplies the value of another question block (for example from a number block, rating block or scale block) to the calculator. |
| Subcalculation | Performs a (sub) calculation for multistep formulas and supplies the result to the calculator. |
| **FUNCTIONS** | |
| *Limiting* | |
| min | Returns the input with the lowest number. |
| max | Returns the input with the highest number. |
| clamp | Clamps (restricts) the input between the specified minimum and maximum value ([learn more](https://en.wikipedia.org/wiki/Clamping_%28graphics%29)). |
| *Floating point* | |
| round | Rounds a floating point number ([learn more](https://en.wikipedia.org/wiki/Rounding)). |
| floor | Rounds a floating point number down ([learn more](https://en.wikipedia.org/wiki/Floor_and_ceiling_functions)). |
| ceil | Rounds a floating point number up ([learn more](https://en.wikipedia.org/wiki/Floor_and_ceiling_functions)). |
| trunc | Removes decimals from a floating point number. |
| *Exponentiation* | |
| x² | Multiplies the given input by itself ([learn more](https://en.wikipedia.org/wiki/Square_%28algebra%29)). |
| √ | Finds the principal square root for the given input ([learn more](https://en.wikipedia.org/wiki/Square_root)). |
| pow | Calculates the base to the power of the given exponent ([learn more](https://en.wikipedia.org/wiki/Exponentiation)). |
| exp | Calculates `e` to the power of the given exponent ([learn more](https://en.wikipedia.org/wiki/Exponential_function)). |
| ln | Calculates the natural logarithm of the input ([learn more](https://en.wikipedia.org/wiki/Natural_logarithm)). |
| log | Calculates the base 10 logarithm of the input ([learn more](https://en.wikipedia.org/wiki/Logarithm)). |
| *Trigonometry* | |
| sin | Calculates the sine of the given angle ([learn more](https://en.wikipedia.org/wiki/Trigonometric_functions)). |
| cos | Calculates the cosine of the given angle ([learn more](https://en.wikipedia.org/wiki/Trigonometric_functions)). |
| tan | Calculates the tangent of the given angle ([learn more](https://en.wikipedia.org/wiki/Trigonometric_functions)). |
| sin⁻¹ | Calculates the inverse sine (arcsine) of the given number ([learn more](https://en.wikipedia.org/wiki/Inverse_trigonometric_functions)). |
| cos⁻¹ | Calculates the inverse cosine (arccosine) of the given number ([learn more](https://en.wikipedia.org/wiki/Inverse_trigonometric_functions)). |
| tan⁻¹ | Calculates the inverse tangent (arctangent) of the given number ([learn more](https://en.wikipedia.org/wiki/Inverse_trigonometric_functions)). |
| sinh | Calculates the hyperbolic sine of the given hyperbolic angle ([learn more](https://en.wikipedia.org/wiki/Hyperbolic_functions)). |
| cosh | Calculates the hyperbolic cosine of the given hyperbolic angle ([learn more](https://en.wikipedia.org/wiki/Hyperbolic_functions)). |
| tanh | Calculates the hyperbolic tangent of the given hyperbolic angle ([learn more](https://en.wikipedia.org/wiki/Hyperbolic_functions)). |
| sinh⁻¹ | Calculates the inverse hyperbolic sine (arcsine) of the given number ([learn more](https://en.wikipedia.org/wiki/Inverse_hyperbolic_functions)). |
| cosh⁻¹ | Calculates the inverse hyperbolic cosine (arccosine) of the given number ([learn more](https://en.wikipedia.org/wiki/Inverse_hyperbolic_functions)). |
| tanh⁻¹ | Calculates the inverse hyperbolic tangent (arctangent) of the given number ([learn more](https://en.wikipedia.org/wiki/Inverse_hyperbolic_functions)). |
| *Factorial* | |
| n! | Calculates the factorial of a positive number ([learn more](https://en.wikipedia.org/wiki/Factorial)). |
| gamma | Calculates the gamma of a positive number ([learn more](https://en.wikipedia.org/wiki/Gamma_function)). |
| *Miscellaneous* | |
| abs | Retrieves the absolute value (or modulus) of a number ([learn more](https://en.wikipedia.org/wiki/Absolute_value)). |
| age | Calculates the age based on the input of a date block, for example a date of birth. |
| year | Calculates the year of a given date. |
| month | Calculates the month of a given date. |
| day of month | Calculates the day of the month of a given date. |
| day of week | Calculates the day of the week of a given date. |
| hour | Calculates the hour of a given time. |
| minute | Calculates the minute of a given time. |
| second | Calculates the second of a given time. |
| millisecond | Calculates the millisecond of a given time. |
| count | Counts the number of selected options, for example from a checkboxes block, or a picture choice block (multiple selection). |
| mod | Calculates the remainder of a division ([learn more](https://en.wikipedia.org/wiki/Modulo_operation)). |
| score | Scores options of a block (for example a dropdown block, radio buttons block or a picture choice block). |
| sgn | Extracts the sign of a number ([learn more](https://en.wikipedia.org/wiki/Sign_function)). |
| % | Calculates a percentage of the input ([learn more](https://en.wikipedia.org/wiki/Percentage)). |
| **COMPARATORS** | |
| Compare value | Compares the recalled value of another block and outputs a value based on the result of the comparison. |
| Compare current outcome | Compares the current outcome of the calculator and outputs a value based on the result of the comparison. |
| Compare number | Compares a static number and outputs a value based on the result of the comparison. |
| Compare date/time | Compares a date (and time) and outputs a value based on the result of the comparison. |
| Check selected option | Checks if a certain option is checked (for example from a multiple choice block) and outputs a value based on the result of the comparison. |
| **TEXT FUNCTIONS** | |
| Character count | Counts the number of characters in a text. |
| Word count | Counts the number of words in a text. |
| Line count | Counts the number of lines in a text. |
| Count occurrences | Counts the number of occurrences of a certain text or character. |
| Convert to number | Converts a text value to a number. |
| **CONSTANTS** | |
| π | Supplies the constant value of `π (pi ≈ 3.14159)` to the calculator ([learn more](https://en.wikipedia.org/wiki/Pi)). |
| e | Supplies the constant value of `e (Euler's constant ≈ 2.71828)` to the calculator ([learn more](https://en.wikipedia.org/wiki/E_%28mathematical_constant%29)). |
| γ | Supplies the constant value of `γ (Euler–Mascheroni constant ≈ 0.57722)` to the calculator ([learn more](https://en.wikipedia.org/wiki/Euler%E2%80%93Mascheroni_constant)). |
| c | Supplies the constant value of `c (speed of light = 299792458 m/s)` to the calculator ([learn more](https://en.wikipedia.org/wiki/Speed_of_light)). |
| Random value | Supplies a random value (0 to less than 1) to the calculator. |
| UNIX time | Supplies the current UNIX time (seconds since Unix Epoch) to the calculator. |
| Year | Supplies the current year to the calculator. |
| Month | Supplies the current month (January = 1) to the calculator. |
| Day of month | Supplies the current day of month (1-31) to the calculator. |
| Day of week | Supplies the current day of week (Sunday = 0) to the calculator. |
| Hour | Supplies the current hour (0-23) to the calculator. |
| Minute | Supplies the current minute (0-59) to the calculator. |
| Second | Supplies the current second (0-59) to the calculator. |
| Millisecond | Supplies the current millisecond (0-999) to the calculator. |
| Timezone | Supplies the current timezone (in milliseconds) to the calculator. |

## 📦 Package contents {#package}

### 🏗️ Builder part {#package-builder}
The builder part instructs the [builder](../../builder/introduction.md) how to manage the block. It defines the configurable properties and settings for the block, which can then be managed with the visual builder. This package contains:

- Classes:
  - `Calculator`: Calculator headless block;
  - `Operation`: Operation item in the calculator collection;
  - `CalculatorCondition`: Calculator condition block to verify a calculator outcome;
- TypeScript typings (should work out-of-the-box);
- Translations (located in the `./translations` folder of the package).

### 🏃 Runner part {#package-runner}
The runner part of this headless block contains all the code to use the block in a [runner](../../runner/introduction.md) (the thing that runs the form). This package contains:

- Classes:
  - `Calculator`: Base class for implementing the calculator block in a runner;
  - `CalculatorCondition`: Runner part of the calculator condition block;
- TypeScript typings (should work out-of-the-box).

## 👩‍💻 Usage {#usage}

### ✨ Installation {#usage-installation}
```bash npm2yarn
npm install @tripetto/block-calculator
```

:::tip Typescript support
This package contains type declarations and supports [TypeScript](https://www.typescriptlang.org/) out-of-the-box.
:::

### 🏗️ Builder part {#usage-builder}

#### ESM/ES5
Importing the block is all you need to do to self-register the block to the builder (see the [builder plain JS implementation](../../builder/integrate/quickstart/plain-js.mdx#blocks) for an example or the [Block loading guide](../../builder/integrate/guides/blocks.mdx) for more information about loading blocks).
```ts showLineNumbers
import "@tripetto/block-calculator";

// The code above imports the ES5 or ESM version based on your project configuration.
// If you want to use the ES5 version, you can do an explicit import:
import "@tripetto/block-calculator/es5";
```

#### CDN
You can also use the builder part directly in a browser using a CDN (see the [builder HTML implementation](../../builder/integrate/quickstart/html.mdx#blocks) for more information).

<Tabs groupId="cdn">
<TabItem value="jsdelivr" label="jsDelivr">

```html showLineNumbers
<html>
  <body>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/builder"></script>
    //highlight-start
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-calculator"></script>
    //highlight-end
    <script>
      Tripetto.Builder.open();
    </script>
  </body>
</html>
```

</TabItem>
<TabItem value="unpkg" label="unpkg">

```html showLineNumbers
<html>
  <body>
    <script src="https://unpkg.com/@tripetto/builder"></script>
    //highlight-start
    <script src="https://unpkg.com/@tripetto/block-calculator"></script>
    //highlight-end
    <script>
      Tripetto.Builder.open();
    </script>
  </body>
</html>
```

</TabItem>
</Tabs>

#### Translations
The available translation for the block are located in the `translations` folder of the package.

:::tip
See the [Loading a translation for a stock block guide](../../builder/integrate/guides/l10n.mdx#loading-a-translation-for-a-stock-block) to learn how to load the block translation into the builder.
:::

### 🏃 Runner part {#usage-runner}
Since this is a headless block, the runner part contains everything you need to use this block in a [runner](../../runner/introduction.md) (the thing that runs the form). You only need to import the block somewhere in your runner project, and you are good to go:

```ts showLineNumbers
import "@tripetto/block-calculator/runner";

// The code above imports the ES5 or ESM version based on your project configuration.
// If you want to use the ES5 version, you can do an explicit import:
import "@tripetto/block-calculator/runner/es5";
```

## 🎭 Stock runners {#stock-runners}
This block is included in the following stock runners:
- [Autoscroll runner](../../runner/stock/faces/autoscroll.mdx)
- [Chat runner](../../runner/stock/faces/chat.mdx)
- [Classic runner](../../runner/stock/faces/classic.mdx)

:::tip
If you are integrating the [builder](../../builder/integrate/introduction.md) together with one of the [stock runners](../../runner/stock/introduction.md), you can use the builder block bundle that is included in the stock runner packages to load the builder part of all the blocks with a single import. See the [Import block bundles guide](../../builder/integrate/guides/blocks.mdx#import-block-bundles) for more information.
:::

## 🚢 Distribution [![](/img/logo-npm-icon.svg)](https://www.npmjs.com/package/@tripetto/block-calculator) {#distribution}
This block is distributed through [npm](https://www.npmjs.com/package/@tripetto/block-calculator):

▶️ https://www.npmjs.com/package/@tripetto/block-calculator

## 📁 Source code [![](/img/logo-gitlab.svg)](https://gitlab.com/tripetto/blocks/calculator) {#source}
This block is open-source and the code is on [GitLab](https://gitlab.com/tripetto/blocks/calculator):

▶️ https://gitlab.com/tripetto/blocks/calculator
