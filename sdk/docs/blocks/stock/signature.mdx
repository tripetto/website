---
title: Signature - Stock blocks
sidebar_label: Signature
description: Use the signature block to let respondents draw a signature.
---

import Preview from '@site/src/components/preview.js';
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Signature block
Use the signature block to let respondents draw a signature.

:::tip
The signature block uses the file upload mechanism to store signatures. Read the [File upload guide](../../runner/stock/guides/attachments.mdx) for more information about handling file uploads.
:::

## 📺 Preview {#preview}
<Preview src="block-signature.gif" />

## 📽️ Demo {#demo}
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/xxeVJge/83fccb37b13a2811f82b5ef7eef91360) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/xxeVJge/83fccb37b13a2811f82b5ef7eef91360)

## 📦 Package contents {#package}

### 🏗️ Builder part {#package-builder}
The builder part instructs the [builder](../../builder/introduction.md) how to manage the block. It defines the configurable properties and settings for the block, which can then be managed with the visual builder. This package contains:

- Classes:
  - `Signature`: Signature visual block;
  - `SignatureCondition`: Signature condition block to verify a signature;
- TypeScript typings (should work out-of-the-box);
- Translations (located in the `./translations` folder of the package).

### 🏃 Runner part {#package-runner}
The runner part of the block is responsible for the rendering of the block in a [runner](../../runner/introduction.md) (the thing that runs the form). This block package contains all the non-UI-related parts of the block and a base class that is useful for implementing the UI rendering in a runner:

- Classes:
  - `Signature`: Base class for implementing the signature question type in a runner UI;
  - `SignatureCondition`: Runner part of the signature condition block;
- TypeScript typings (should work out-of-the-box).

## 👩‍💻 Usage {#usage}

### ✨ Installation {#usage-installation}
```bash npm2yarn
npm install @tripetto/block-signature
```

:::tip Typescript support
This package contains type declarations and supports [TypeScript](https://www.typescriptlang.org/) out-of-the-box.
:::

### 🏗️ Builder part {#usage-builder}

#### ESM/ES5
Importing the block is all you need to do to self-register the block to the builder (see the [builder plain JS implementation](../../builder/integrate/quickstart/plain-js.mdx#blocks) for an example or the [Block loading guide](../../builder/integrate/guides/blocks.mdx) for more information about loading blocks).
```ts showLineNumbers
import "@tripetto/block-signature";

// The code above imports the ES5 or ESM version based on your project configuration.
// If you want to use the ES5 version, you can do an explicit import:
import "@tripetto/block-signature/es5";
```

#### CDN
You can also use the builder part directly in a browser using a CDN (see the [builder HTML implementation](../../builder/integrate/quickstart/html.mdx#blocks) for more information).

<Tabs groupId="cdn">
<TabItem value="jsdelivr" label="jsDelivr">

```html showLineNumbers
<html>
  <body>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/builder"></script>
    //highlight-start
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-signature"></script>
    //highlight-end
    <script>
      Tripetto.Builder.open();
    </script>
  </body>
</html>
```

</TabItem>
<TabItem value="unpkg" label="unpkg">

```html showLineNumbers
<html>
  <body>
    <script src="https://unpkg.com/@tripetto/builder"></script>
    //highlight-start
    <script src="https://unpkg.com/@tripetto/block-signature"></script>
    //highlight-end
    <script>
      Tripetto.Builder.open();
    </script>
  </body>
</html>
```

</TabItem>
</Tabs>

#### Translations
The available translation for the block are located in the `translations` folder of the package.

:::tip
See the [Loading a translation for a stock block guide](../../builder/integrate/guides/l10n.mdx#loading-a-translation-for-a-stock-block) to learn how to load the block translation into the builder.
:::

### 🏃 Runner part {#usage-runner}
The runner part contains a base class for implementing the signature question type in a runner UI (read the [visual block tutorial](../custom/implement/visual.mdx#runner-part) to learn how to implement a runner UI for a block):

```ts showLineNumbers
import { Signature } from "@tripetto/block-signature/runner";

// The code above imports the ES5 or ESM version based on your project configuration.
// If you want to use the ES5 version, you can do an explicit import:
import { Signature } from "@tripetto/block-signature/runner/es5";

// Now you can extend the signature question type with your UI rendering
class SignatureBlock extends Signature {
  // UI rendering implementation depending on the runner used
}
```

## 🎭 Stock runners {#stock-runners}
This block is included in the following stock runners:
- [Autoscroll runner](../../runner/stock/faces/autoscroll.mdx)
- [Chat runner](../../runner/stock/faces/chat.mdx)
- [Classic runner](../../runner/stock/faces/classic.mdx)

:::tip
If you are integrating the [builder](../../builder/integrate/introduction.md) together with one of the [stock runners](../../runner/stock/introduction.md), you can use the builder block bundle that is included in the stock runner packages to load the builder part of all the blocks with a single import. See the [Import block bundles guide](../../builder/integrate/guides/blocks.mdx#import-block-bundles) for more information.
:::

## 🚢 Distribution [![](/img/logo-npm-icon.svg)](https://www.npmjs.com/package/@tripetto/block-signature) {#distribution}
This block is distributed through [npm](https://www.npmjs.com/package/@tripetto/block-signature):

▶️ https://www.npmjs.com/package/@tripetto/block-signature

## 📁 Source code [![](/img/logo-gitlab.svg)](https://gitlab.com/tripetto/blocks/signature) {#source}
This block is open-source and the code is on [GitLab](https://gitlab.com/tripetto/blocks/signature):

▶️ https://gitlab.com/tripetto/blocks/signature
