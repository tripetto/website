---
title: Form ColorPicker class - Blocks
sidebar_label: ColorPicker
description: The ColorPicker class defines a color input control for the Tripetto builder.
---

import Preview from '@site/src/components/preview.js';

# ColorPicker class

## 📖 Description {#description}
The `ColorPicker` class defines a color input control for the Tripetto builder. It is derived from the [`DataControl`](DataControl.mdx) class.

## 📺 Preview {#preview}
<Preview src="forms-color-picker.png" />

---
## 🆕 `constructor` {#constructor}
Creates a new `ColorPicker` instance.
#### Signature
```ts
constructor(color?: string | Binding, style?: IColorPickerStyle): ColorPicker
```
#### Parameters
| Name    | Type                                                                        | Optional | Description                                                                                                                                                                                                                                                                                                                                                                                                                                              |
|:--------|:----------------------------------------------------------------------------|:---------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `color` | string \| `Binding`                                                         | Yes      | Specifies the initially color or a data binding created with [`bind`](#bind).                                                                                                                                                                                                                                                                                                                                                                            |
| `style` | [`IColorPickerStyle`](#IColorPickerStyle)                                   | Yes      | Specifies the input style (when omitted the default global style will be used).                                                                                                                                                                                                                                                                                                                                                                          |
#### Return value
Returns a reference to the new `ColorPicker` instance.

## 📌 Statics {#statics}

---
### 🏷️ `style` {#statics-style}
Contains the global default style for the color input control.
#### Type {#signature}
[`IColorPickerStyle`](#IColorPickerStyle)

---
### 🔧 `bind` {#bind}
Binds an object property to the control allowing its value to update automatically when the value of the control changes. If the control is disabled or invisible, the supplied `valueWhenInactive` is automatically set as the property value. When a control is enabled and visible, the supplied `defaultWhenActive` is set automatically if the property value is currently not defined.
#### Signature
```ts
bind<Target, P extends keyof Target>(
  target: Target,
  property: P,
  valueWhenInactive: string,
  defaultWhenActive?: string,
  modifier?: (value?: string) => string
): Binding;
```
#### Parameters
| Name                | Type                       | Optional | Description                                                                                               |
|:--------------------|:---------------------------|:---------|:----------------------------------------------------------------------------------------------------------|
| `target`            | `Target`                   | No       | Reference to the target object which contains the property.                                               |
| `property`          | `P`                        | No       | Specifies the name of the property.                                                                       |
| `valueWhenInactive` | string                     | No       | Specifies the value which is set to the property when the control is inactive (disabled or invisible).    |
| `defaultWhenActive` | string                     | Yes      | Specifies the default (initial) value that will be used when the control is active (enabled and visible). |
| `modifier`          | (value?: string) => string | Yes      | Specifies a modifier function for the data.                                                               |
#### Return value
Returns a `Binding` instance that can be used in the controls constructor to bind a value to a control.
#### Example
```ts showLineNumbers
import { Forms } from "@tripetto/builder";

const example = {
  color: ""
};

const colorPicker = new Forms.ColorPicker(Forms.ColorPicker.bind(example, "color", ""));
```

## 🗃️ Fields {#fields}

---
### 🏷️ `color` {#color}
Sets or retrieves the color value.
#### Type {#signature}
string

---
### 🏷️ `hasFocus` {#hasFocus}
Retrieves if the control has input focus.
#### Type
boolean

---
### 🏷️ `isDisabled` {#isDisabled}
Sets or retrieves if the control is disabled.
#### Type {#signature}
boolean

---
### 🏷️ `isLocked` {#isLocked}
Sets or retrieves if the control is locked (readonly).
#### Type {#signature}
boolean

---
### 🏷️ `isObservable` {#isObservable}
Retrieves if the control is observable.
#### Type {#signature}
boolean

---
### 🏷️ `isRequired` {#isRequired}
Sets or retrieves if the control is required.
#### Type {#signature}
boolean

---
### 🏷️ `isVisible` {#isVisible}
Sets or retrieves the visibility of the control.
#### Type {#signature}
boolean

---
### 🏷️ `style` {#style}
Contains the text control style.
#### Type {#signature}
[`IColorPickerStyle`](#IColorPickerStyle)

---
### 🏷️ `value` {#value}
Sets or retrieves the color value.
#### Type {#signature}
string

## ▶️ Methods {#methods}

---
### 🔧 `autoValidate` {#autoValidate}
Enables automatic control validation.
#### Signature
```ts
autoValidate(): this
```
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

---
### 🔧 `blur` {#blur}
Blurs the focus of the control.
#### Signature
```ts
blur(): void
```

---
### 🔧 `disable` {#disable}
Disables the control.
#### Signature
```ts
disable(): this
```
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

---
### 🔧 `disabled` {#disabled}
Specifies the state of the control.
#### Signature
```ts
disabled(disabled?: boolean): this
```
#### Parameters
| Name       | Type    | Optional | Description                                               |
|:-----------|:--------|:---------|:----------------------------------------------------------|
| `disabled` | boolean | Yes      | Specifies if the control is disabled (default is `true`). |
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

---
### 🔧 `enable` {#enable}
Enables the control.
#### Signature
```ts
enable(): this
```
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

---
### 🔧 `escape` {#escape}
Specifies the function which is invoked when the user presses the escape key.
#### Signature
```ts
escape(fn: (colorPicker: ColorPicker) => boolean): this
```
#### Parameters
| Name | Type                                    | Optional | Description                                                                                        |
|:-----|:----------------------------------------|:---------|:---------------------------------------------------------------------------------------------------|
| `fn` | (colorPicker: `ColorPicker`) => boolean | No       | Specifies the function to invoke. To cancel the normal behavior return `true` within the function. |
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

### 🔧 `focus` {#focus}
Sets the focus to the control.
#### Signature
```ts
focus(): boolean
```
#### Return value
Returns `true` if the focus is set.

---
### 🔧 `format` {#format}
Specifies the color format to use.
:::info
See https://developer.mozilla.org/en-US/docs/Web/CSS/color_value for more information about color models.
:::
#### Signature
```ts
format(format: "auto" | "hex" | "hex-unsigned" | "rgb" | "hsl" | "hwb"): this
```
#### Parameters
| Name | Type                                                             | Optional | Description                                                                                                                                                                                                                                                                                                          |
|:-----|:-----------------------------------------------------------------|:---------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `format` | "auto" \| "hex" \| "hex-unsigned" \| "rgb" \| "hsl" \| "hwb" | No       | Specifies the color format to use. Can be one of the following values:<br/>- `auto`: Allows any valid color notation;<br/>- `hex`: Uses hex notation;<br/>- `rgb`: Uses RGB notation;<br/>- `hsl`: Uses HSL notation;<br/>- `hwb`: Uses HWB notation;<br/>- `hex-unsigned`: Uses hex notation without the hash sign. |
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

---
### 🔧 `hide` {#hide}
Hides the control.
#### Signature
```ts
hide(): this
```
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

---
### 🔧 `indent` {#indent}
Enables control indentation. This can only be set during construction.
#### Signature
```ts
indent(size: number): this
```
#### Parameters
| Name   | Type   | Optional | Description                          |
|:-------|:-------|:---------|:-------------------------------------|
| `size` | number | No       | Specifies the indent size in pixels. |
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

---
### 🔧 `label` {#label}
Sets the label for the control.
:::tip
If you want to use [markdown](https://en.wikipedia.org/wiki/Markdown) in the label, activate it first using the [`markdown`](#markdown) method.
:::
#### Signature
```ts
label(label: string): this
```
#### Parameters
| Name    | Type   | Optional | Description                  |
|:--------|:-------|:---------|:-----------------------------|
| `label` | string | No       | Specifies the control label. |
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

---
### 🔧 `labels` {#labels}
Specifies the labels for the color picker panel.
#### Signature
```ts
labels(labels: {
    palette: string;
    history: string;
}): this
```
#### Parameters
| Name     | Type    | Optional | Description           |
|:---------|:--------|:---------|:----------------------|
| `labels` | object  | No       | Specifies the labels. |
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

---
### 🔧 `lock` {#lock}
Locks the control.
#### Signature
```ts
lock(): this
```
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

---
### 🔧 `locked` {#locked}
Specifies if the control is locked (readonly).
#### Signature
```ts
locked(locked?: boolean): this
```
#### Parameters
| Name     | Type    | Optional | Description                                                      |
|:---------|:--------|:---------|:-----------------------------------------------------------------|
| `locked` | boolean | Yes      | Specifies if the control is locked/readonly (default is `true`). |
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

---
### 🔧 `markdown` {#markdown}
Specifies if the label should support markdown formatting.
#### Signature
```ts
markdown(options?: IMarkdownOptions): this
```
#### Parameters
| Name      | Type                                                         | Optional | Description                     |
|:----------|:-------------------------------------------------------------|:---------|:--------------------------------|
| `options` | [`IMarkdownOptions`](../Markdown/index.mdx#IMarkdownOptions) | Yes      | Specifies the markdown options. |
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

---
### 🔧 `on` {#on}
Specifies the function which is invoked when the input value changes.
#### Signature
```ts
on(fn: (colorPicker: ColorPicker) => void): this
```
#### Parameters
| Name | Type                                 | Optional | Description                    |
|:-----|:-------------------------------------|:---------|:-------------------------------|
| `fn` | (colorPicker: `ColorPicker`) => void | No       | Specifies the change function. |
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

---
### 🔧 `placeholder` {#placeholder}
Sets a placeholder for the input control.
#### Signature
```ts
placeholder(placeholder: string): this
```
#### Parameters
| Name          | Type   | Optional | Description                     |
|:--------------|:-------|:---------|:--------------------------------|
| `placeholder` | string | No       | Specifies the placeholder text. |
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

---
### 🔧 `readonly` {#readonly}
Makes the control readonly.
#### Signature
```ts
readonly(): this
```
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

---
### 🔧 `require` {#require}
Makes the control required.
#### Signature
```ts
require(): this
```
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

---
### 🔧 `required` {#required}
Specifies if the control is required.
#### Signature
```ts
required(required?: boolean): this
```
#### Parameters
| Name       | Type    | Optional | Description                                               |
|:-----------|:--------|:---------|:----------------------------------------------------------|
| `required` | boolean | Yes      | Specifies if the control is required (default is `true`). |
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

---
### 🔧 `select` {#select}
Selects the current value.
#### Signature
```ts
select(): this
```
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

---
### 🔧 `show` {#show}
Shows the control.
#### Signature
```ts
show(): this
```
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

---
### 🔧 `supportAlpha` {#supportAlpha}
Specifies if alpha values for transparency are supported.
:::tip
Alpha value support is enabled by default. If you don't want translucent colors, you need to disable it with this function.
:::
#### Signature
```ts
supportAlpha(supportAlpha: boolean): this
```
#### Parameters
| Name           | Type    | Optional | Description                              |
|:---------------|:--------|:---------|:-----------------------------------------|
| `supportAlpha` | boolean | No       | Specifies if alpha values are supported. |
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

---
### 🔧 `swatches` {#swatches}
Specifies if color swatches are generated and shown. Color swatches are color presets that are shown in the color picker overlay. The color palette swatches are generated using all color picker controls in a form. The history keeps track of all previously used colors.
#### Signature
```ts
swatches(
  palette?: boolean,
  history?: boolean,
  sorting?: "auto" | "hue" | "rgb"
): this
```
#### Parameters
| Name      | Type                     | Optional | Description                                                                                                                                                                                                                                  |
|:----------|:-------------------------|:---------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `palette` | boolean                  | Yes      | Specifies if a color palette is generated (default is `true`).                                                                                                                                                                              |
| `history` | boolean                  | Yes      | Specifies if the color history is enabled (default is `true`).                                                                                                                                                                              |
| `sorting` | "auto" \| "hue" \| "rgb" | Yes      | Specifies how the palette swatches are sorted. Can be one of the following values:<br/>- `auto`: Swatches are sorted by order of use;<br/>- `hue`: Swatches are sorted on the hue value;<br/>- `rgb`: Swatches are sorted on the RGB value. |
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

---
### 🔧 `unlock` {#unlock}
Unlocks the control.
#### Signature
```ts
unlock(): this
```
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

---
### 🔧 `visible` {#visible}
Specifies the visibility of the control.
#### Signature
```ts
visible(visible?: boolean): this
```
#### Parameters
| Name      | Type    | Optional | Description                                              |
|:----------|:--------|:---------|:---------------------------------------------------------|
| `visible` | boolean | Yes      | Specifies if the control is visible (default is `true`). |
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

---
### 🔧 `width` {#width}
Sets the width of the control.
#### Signature
```ts
width(width: number): this
```
#### Parameters
| Name    | Type   | Optional | Description                            |
|:--------|:-------|:---------|:---------------------------------------|
| `width` | number | No       | Specifies the control width in pixels. |
#### Return value
Returns a reference to the `ColorPicker` instance to allow chaining.

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `IColorPickerStyle` {#IColorPickerStyle}
Describes the interface for the color picker styles.
#### Type declaration
```ts
interface IColorPickerStyle {
  /* Color picker input appearance. */
  appearance?: IStyles;

  /* Color picker input label. */
  label?: IStyles;

  /* Color picker input field. */
  input?: IStyles;

  /* Color picker panel. */
  picker?: {
    /* Color picker panel appearance. */
    appearance?: IStyles;

    /* Specifies the horizontal padding in pixels. */
    horizontalPadding?: number;

    /* Specifies the horizontal padding in pixels. */
    verticalPadding?: number;

    /* Specifies the spacing in pixels. */
    spacing?: Spacing;
  };

  /* Color picker input disabled. */
  disabled?: IStyles;

  /* Placeholder style. */
  placeholder?: IStyles;

  /* Contains the text selection styles. */
  selection?: IStyles;

  /* Color picker input required. */
  required?: IStyles;

  /* Color picker input locked. */
  locked?: IStyles;

  /* Color picker input focus. */
  focus?: IStyles;

  /* Color picker input hover. */
  hover?: IStyles;

  /* Validation passed. */
  passed?: IStyles;

  /* Validation failed. */
  failed?: IStyles;

  /* Validation awaiting. */
  awaiting?: IStyles;

  /* Color picker input when fused with form card. */
  fused?: {
    /* Color picker input appearance. */
    appearance?: IStyles;

    /* Color picker input label. */
    label?: IStyles;

    /* Color picker input required. */
    required?: IStyles;

    /* Color picker input locked. */
    locked?: IStyles;

    /* Color picker input focus. */
    focus?: IStyles;

    /* Color picker input hover. */
    hover?: IStyles;

    /* Validation passed. */
    passed?: IStyles;

    /* Validation failed. */
    failed?: IStyles;

    /* Validation awaiting. */
    awaiting?: IStyles;
  };
}
```
