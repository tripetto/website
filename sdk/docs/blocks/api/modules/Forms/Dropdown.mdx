---
title: Form Dropdown class - Blocks
sidebar_label: Dropdown
description: The Dropdown class defines a dropdown control for the Tripetto builder.
---

import Preview from '@site/src/components/preview.js';

# Dropdown class

## 📖 Description {#description}
The `Dropdown` class defines a dropdown control for the Tripetto builder. It is derived from the [`DataControl`](DataControl.mdx) class.

## 📺 Preview {#preview}
<Preview src="forms-dropdown.png" />

---
## 🆕 `constructor` {#constructor}
Creates a new `Dropdown` instance.
#### Signature
```ts
constructor(
  options:
    (IOption | IOptionGroup)[] |
    ((done: (options: IOption | IOptionGroup[]) => void) => void),
  value: any | Binding,
  style?: IDropdownStyle
): Dropdown
```
#### Parameters
| Name      | Type                                                                                                                                                            | Optional | Description                                                                                   |
|:----------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------|:---------|:----------------------------------------------------------------------------------------------|
| `options` | ([`IOption`](#IOption) \| [`IOptionGroup`](#IOptionGroup))[] \| ((done: (options: [`IOption`](#IOption) \| [`IOptionGroup`](#IOptionGroup)[]) => void) => void) | No       | Specifies the option array or a function in case you want to load the options asynchronously. |
| `value`   | any \| `Binding`                                                                                                                                                | No       | Specifies the initially selected value or a data binding created with [`bind`](#bind).        |
| `style`   | [`IDropdownStyle`](#IDropdownStyle)                                                                                                                             | Yes      | Specifies the style (when omitted the default global style will be used).                     |
#### Return value
Returns a reference to the new `Dropdown` instance.

## 📌 Statics {#statics}

---
### 🏷️ `style` {#statics-style}
Contains the global default style for the dropdown control.
#### Type
[`IDropdownStyle`](#IDropdownStyle)

---
### 🔧 `bind` {#bind}
Binds an object property to the control allowing its value to update automatically when the value of the control changes. If the control is disabled or invisible, the supplied `valueWhenInactive` is automatically set as the property value. When a control is enabled and visible, the supplied `defaultWhenActive` is set automatically if the property value is currently not defined.
#### Signature
```ts
bind<Target, P extends keyof Target>(
  target: Target,
  property: P,
  valueWhenInactive: any,
  defaultWhenActive?: any,
  modifier?: (value?: any) => any
): Binding;
```
#### Parameters
| Name                | Type                 | Optional | Description                                                                                               |
|:--------------------|:---------------------|:---------|:----------------------------------------------------------------------------------------------------------|
| `target`            | `Target`             | No       | Reference to the target object which contains the property.                                               |
| `property`          | `P`                  | No       | Specifies the name of the property.                                                                       |
| `valueWhenInactive` | any                  | No       | Specifies the value which is set to the property when the control is inactive (disabled or invisible).    |
| `defaultWhenActive` | any                  | Yes      | Specifies the default (initial) value that will be used when the control is active (enabled and visible). |
| `modifier`          | (value?: any) => any | Yes      | Specifies a modifier function for the data.                                                               |
#### Return value
Returns a `Binding` instance that can be used in the controls constructor to bind a value to a control.
#### Example
```ts showLineNumbers
import { Forms } from "@tripetto/builder";

const example = {
  selected: "a"
};

const dropdown = new Forms.Dropdown(
  [
    {
      label: "Option A",
      value: "a"
    },
    {
      label: "Option B",
      value: "b"
    }
  ],
  Forms.dropdown.bind(example, "selected", "a"));
```

## 🗃️ Fields {#fields}

---
### 🏷️ `hasFocus` {#hasFocus}
Retrieves if the control has input focus.
#### Type
boolean

---
### 🏷️ `isDisabled` {#isDisabled}
Sets or retrieves if the control is disabled.
#### Type
boolean

---
### 🏷️ `isLocked` {#isLocked}
Sets or retrieves if the control is locked (readonly).
#### Type
boolean

---
### 🏷️ `isObservable` {#isObservable}
Retrieves if the control is observable.
#### Type
boolean

---
### 🏷️ `isRequired` {#isRequired}
Sets or retrieves if the control is required.
#### Type
boolean

---
### 🏷️ `isVisible` {#isVisible}
Sets or retrieves the visibility of the control.
#### Type
boolean

---
### 🏷️ `style` {#style}
Contains the dropdown control style.
#### Type
[`IDropdownStyle`](#IDropdownStyle)

---
### 🏷️ `value` {#value}
Sets or retrieves the selected value.
#### Type
any

## ▶️ Methods {#methods}

---
### 🔧 `blur` {#blur}
Blurs the focus of the control.
#### Signature
```ts
blur(): void
```

---
### 🔧 `disable` {#disable}
Disables the control.
#### Signature
```ts
disable(): this
```
#### Return value
Returns a reference to the `Dropdown` instance to allow chaining.

---
### 🔧 `disabled` {#disabled}
Specifies the state of the control.
#### Signature
```ts
disabled(disabled?: boolean): this
```
#### Parameters
| Name       | Type    | Optional | Description                                               |
|:-----------|:--------|:---------|:----------------------------------------------------------|
| `disabled` | boolean | Yes      | Specifies if the control is disabled (default is `true`). |
#### Return value
Returns a reference to the `Dropdown` instance to allow chaining.

---
### 🔧 `enable` {#enable}
Enables the control.
#### Signature
```ts
enable(): this
```
#### Return value
Returns a reference to the `Dropdown` instance to allow chaining.

---
### 🔧 `focus` {#focus}
Sets the focus to the control.
#### Signature
```ts
focus(): boolean
```
#### Return value
Returns `true` if the focus is set.

---
### 🔧 `hide` {#hide}
Hides the control.
#### Signature
```ts
hide(): this
```
#### Return value
Returns a reference to the `Dropdown` instance to allow chaining.

---
### 🔧 `indent` {#indent}
Enables control indentation. This can only be set during construction.
#### Signature
```ts
indent(size: number): this
```
#### Parameters
| Name   | Type   | Optional | Description                          |
|:-------|:-------|:---------|:-------------------------------------|
| `size` | number | No       | Specifies the indent size in pixels. |
#### Return value
Returns a reference to the `Dropdown` instance to allow chaining.

---
### 🔧 `label` {#label}
Sets the label for the control.
:::tip
If you want to use [markdown](https://en.wikipedia.org/wiki/Markdown) in the label, activate it first using the [`markdown`](#markdown) method.
:::
#### Signature
```ts
label(label: string): this
```
#### Parameters
| Name    | Type   | Optional | Description                  |
|:--------|:-------|:---------|:-----------------------------|
| `label` | string | No       | Specifies the control label. |
#### Return value
Returns a reference to the `Dropdown` instance to allow chaining.

---
### 🔧 `lock` {#lock}
Locks the control.
#### Signature
```ts
lock(): this
```
#### Return value
Returns a reference to the `Dropdown` instance to allow chaining.

---
### 🔧 `locked` {#locked}
Specifies if the control is locked (readonly).
#### Signature
```ts
locked(locked?: boolean): this
```
#### Parameters
| Name     | Type    | Optional | Description                                                      |
|:---------|:--------|:---------|:-----------------------------------------------------------------|
| `locked` | boolean | Yes      | Specifies if the control is locked/readonly (default is `true`). |
#### Return value
Returns a reference to the `Dropdown` instance to allow chaining.

---
### 🔧 `markdown` {#markdown}
Specifies if the label should support markdown formatting.
#### Signature
```ts
markdown(options?: IMarkdownOptions): this
```
#### Parameters
| Name      | Type                                                         | Optional | Description                     |
|:----------|:-------------------------------------------------------------|:---------|:--------------------------------|
| `options` | [`IMarkdownOptions`](../Markdown/index.mdx#IMarkdownOptions) | Yes      | Specifies the markdown options. |
#### Return value
Returns a reference to the `Dropdown` instance to allow chaining.

---
### 🔧 `on` {#on}
Specifies the function which is invoked when the dropdown selection is changed.
#### Signature
```ts
on(fn: (dropdown: Dropdown) => void): this
```
#### Parameters
| Name | Type                           | Optional | Description                    |
|:-----|:-------------------------------|:---------|:-------------------------------|
| `fn` | (dropdown: `Dropdown`) => void | No       | Specifies the change function. |
#### Return value
Returns a reference to the `Dropdown` instance to allow chaining.

---
### 🔧 `optionDisabled` {#optionDisabled}
Sets or retrieves the disabled state of the specified option or the current option if no option value is specified.
#### Signature
```ts
optionDisabled(value?: any, disabled?: boolean): boolean
```
#### Parameters
| Name       | Type    | Optional | Description                   |
|:-----------|:--------|:---------|:------------------------------|
| `value`    | any     | Yes      | Specifies the option value.   |
| `disabled` | boolean | Yes      | Specifies the disabled state. |
#### Return value
Returns the disabled state.

---
### 🔧 `optionLabel` {#optionLabel}
Sets or retrieves the label of the specified option or the current option if no option value is specified.
#### Signature
```ts
optionLabel(value?: any, label?: string): string
```
#### Parameters
| Name    | Type   | Optional | Description                           |
|:--------|:-------|:---------|:--------------------------------------|
| `value` | any    | Yes      | Specifies the option value.           |
| `label` | string | Yes      | Specifies a new label for the option. |
#### Return value
Returns the label.

---
### 🔧 `options` {#options}
Sets the options for the dropdown.
#### Signature
```ts
options(options: (IOption | IOptionGroup)[]): this
```
#### Parameters
| Name      | Type                                                         | Optional | Description                    |
|:----------|:-------------------------------------------------------------|:---------|:-------------------------------|
| `options` | ([`IOption`](#IOption) \| [`IOptionGroup`](#IOptionGroup))[] | No       | Specifies an array of options. |
#### Return value
Returns a reference to the `Dropdown` instance to allow chaining.

---
### 🔧 `placeholder` {#placeholder}
Sets a placeholder for the control.
#### Signature
```ts
placeholder(placeholder: string): this
```
#### Parameters
| Name          | Type   | Optional | Description                       |
|:--------------|:-------|:---------|:----------------------------------|
| `placeholder` | string | No       | Specifies the placeholder string. |
#### Return value
Returns a reference to the `Dropdown` instance to allow chaining.

---
### 🔧 `readonly` {#readonly}
Makes the control readonly.
#### Signature
```ts
readonly(): this
```
#### Return value
Returns a reference to the `Dropdown` instance to allow chaining.

---
### 🔧 `require` {#require}
Makes the control required.
#### Signature
```ts
require(): this
```
#### Return value
Returns a reference to the `Dropdown` instance to allow chaining.

---
### 🔧 `required` {#required}
Specifies if the control is required.
#### Signature
```ts
required(required?: boolean): this
```
#### Parameters
| Name       | Type    | Optional | Description                                               |
|:-----------|:--------|:---------|:----------------------------------------------------------|
| `required` | boolean | Yes      | Specifies if the control is required (default is `true`). |
#### Return value
Returns a reference to the `Dropdown` instance to allow chaining.

---
### 🔧 `select` {#select}
Selects the specified option.
#### Signature
```ts
select(value: any): this
```
#### Parameters
| Name    | Type | Optional | Description                 |
|:--------|:-----|:---------|:----------------------------|
| `value` | any  | No       | Specifies the option value. |
#### Return value
Returns a reference to the `Dropdown` instance to allow chaining.

---
### 🔧 `show` {#show}
Shows the control.
#### Signature
```ts
show(): this
```
#### Return value
Returns a reference to the `Dropdown` instance to allow chaining.

---
### 🔧 `unlock` {#unlock}
Unlocks the control.
#### Signature
```ts
unlock(): this
```
#### Return value
Returns a reference to the `Dropdown` instance to allow chaining.

---
### 🔧 `visible` {#visible}
Specifies the visibility of the control.
#### Signature
```ts
visible(visible?: boolean): this
```
#### Parameters
| Name      | Type    | Optional | Description                                              |
|:----------|:--------|:---------|:---------------------------------------------------------|
| `visible` | boolean | Yes      | Specifies if the control is visible (default is `true`). |
#### Return value
Returns a reference to the `Dropdown` instance to allow chaining.

---
### 🔧 `width` {#width}
Sets the width of the control.
#### Signature
```ts
width(width: number | "auto" | "full"): this
```
#### Parameters
| Name    | Type                       | Optional | Description                                                                  |
|:--------|:---------------------------|:---------|:-----------------------------------------------------------------------------|
| `width` | number \| "auto" \| "full" | No       | Specifies the control width in pixels or sets the width to `auto` or `full`. |
#### Return value
Returns a reference to the `Dropdown` instance to allow chaining.

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `IDropdownStyle` {#IDropdownStyle}
Describes the interface for the dropdown styles.
#### Type declaration
```ts
interface IDropdownStyle {
  /* Dropdown appearance. */
  appearance?: IStyles;

  /* Options. */
  options?: IStyles;

  /* Option groups. */
  optGroups?: IStyles;

  /* Placeholder option. */
  placeholder?: IStyles;

  /* Dropdown label. */
  label?: IStyles;

  /* Dropdown disabled. */
  disabled?: IStyles;

  /* Dropdown required. */
  required?: IStyles;

  /* Dropdown locked. */
  locked?: IStyles;

  /* Dropdown focus. */
  focus?: IStyles;

  /* Dropdown hover. */
  hover?: IStyles;

  /* Validation passed. */
  passed?: IStyles;

  /* Validation failed. */
  failed?: IStyles;

  /* Validation awaiting. */
  awaiting?: IStyles;

  /* Dropdown when fused with form card. */
  fused?: {
    /* Dropdown appearance. */
    appearance?: IStyles;

    /* Dropdown label. */
    label?: IStyles;

    /* Dropdown required. */
    required?: IStyles;

    /* Dropdown locked. */
    locked?: IStyles;

    /* Dropdown focus. */
    focus?: IStyles;

    /* Dropdown hover. */
    hover?: IStyles;

    /* Validation passed. */
    passed?: IStyles;

    /* Validation failed. */
    failed?: IStyles;

    /* Validation awaiting. */
    awaiting?: IStyles;
  };
}
```

---
### 🔗 `IOption` {#IOption}
Describes the interface for a dropdown option.
#### Type declaration
```ts
interface IOption {
  /* Label for the option. */
  label: string;

  /* Value for the option. */
  value: any;

  /* Specifies if the option is disabled. */
  disabled?: boolean;

  /* Specifies an indentation level for an option. */
  indent?: number;
}
```

---
### 🔗 `IOptionGroup` {#IOptionGroup}
Describes the interface for a dropdown option group.
#### Type declaration
```ts
interface IOptionGroup {
  /* Label for the option group. */
  optGroup: string;
}
```
