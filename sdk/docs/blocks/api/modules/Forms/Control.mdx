---
title: Form Control class - Blocks
sidebar_label: Control
description: The Control class is the base from which UI control classes are derived.
---

# Control class

## 📖 Description {#description}
The `Control` class is the base from which UI control classes are derived. It is an [abstract class](https://www.typescriptlang.org/docs/handbook/2/classes.html#abstract-classes-and-members) and cannot be instantiated directly.

## 📦 Derived controls {#derives}
The following controls are derived from this class and are available in the [`Forms`](index.mdx) module:
- [`Button`](Button.mdx)
- [`Checkbox`](Checkbox.mdx)
- [`ColorPicker`](ColorPicker.mdx)
- [`DateTime`](DateTime.mdx)
- [`Dropdown`](Dropdown.mdx)
- [`Email`](Email.mdx)
- [`Group`](Group.mdx)
- [`HTML`](HTML.mdx)
- [`Notification`](Notification.mdx)
- [`Numeric`](Numeric.mdx)
- [`Radiobutton`](Radiobutton.mdx)
- [`Spacer`](Spacer.mdx)
- [`Static`](Static.mdx)
- [`Text`](Text.mdx)

---
## 🆕 `constructor` {#constructor}
Creates a new `Control` instance.
#### Signature
```ts
constructor(properties: IControlProperties): Control
```
#### Parameters
| Name         | Type                                        | Optional | Description                       |
|:-------------|:--------------------------------------------|:---------|:----------------------------------|
| `properties` | [`IControlProperties`](#IControlProperties) | No       | Specifies the control properties. |
#### Return value
Returns a reference to the new `Control` instance.

## 🗃️ Fields {#fields}

---
### 🏷️ `feature` {#feature}
Retrieves a reference to the feature of the form card.
#### Type
[`Feature`](../Components/Feature.mdx) | undefined

---
### 🏷️ `form` {#form}
Sets or retrieves the parent form.
#### Type
[`Form`](Form.mdx) | undefined

---
### 🏷️ `hasFocus` {#hasFocus}
Retrieves if the control has input focus.
#### Type
boolean

---
### 🏷️ `hasLabel` {#hasLabel}
Retrieves if a label is set.
#### Type
boolean

---
### 🏷️ `isAwaiting` {#isAwaiting}
Sets or retrieves if the control validation is awaiting.
#### Type
boolean

---
### 🏷️ `isDisabled` {#isDisabled}
Sets or retrieves if the control is disabled.
#### Type
boolean

---
### 🏷️ `isFailed` {#isFailed}
Sets or retrieves if the control validation has failed.
#### Type
boolean

---
### 🏷️ `isFeatureEnabled` {#isFeatureEnabled}
Retrieves if the control is attached to an enabled feature.
#### Type
boolean

---
### 🏷️ `isFormEnabled` {#isFormEnabled}
Retrieves if the form is enabled.
#### Type
boolean

---
### 🏷️ `isFormReady` {#isFormReady}
Retrieves if the form is ready.
#### Type
boolean

---
### 🏷️ `isFormVisible` {#isFormVisible}
Retrieves if the form is visible.
#### Type
boolean

---
### 🏷️ `isFused` {#isFused}
Retrieves if the control is fused with the form card. This is only possible with controls which have no separate label and are the one and only control inside of the form.
#### Type
boolean

---
### 🏷️ `isGroupVisible` {#isGroupVisible}
Retrieves if the form group is visible.
#### Type
boolean

---
### 🏷️ `isHovered` {#isHovered}
Retrieves if the control is hovered.
#### Type
boolean

---
### 🏷️ `isInteractable` {#isInteractable}
Retrieves if the control is interactable.
#### Type
boolean

---
### 🏷️ `isInvalid` {#isInvalid}
Sets or retrieves if the control validation is invalid.
#### Type
boolean

---
### 🏷️ `isLocked` {#isLocked}
Sets or retrieves if the control is locked (readonly).
#### Type
boolean

---
### 🏷️ `isObservable` {#isObservable}
Retrieves if the control is observable.
#### Type
boolean

---
### 🏷️ `isPassed` {#isPassed}
Sets or retrieves if the control validation has passed.
#### Type
boolean

---
### 🏷️ `isReadable` {#isReadable}
Specifies it the control value is readable.
#### Type
boolean

---
### 🏷️ `isRequired` {#isRequired}
Sets or retrieves if the control is required.
#### Type
boolean

---
### 🏷️ `isVisible` {#isVisible}
Sets or retrieves the visibility of the control.
#### Type
boolean

---
### 🏷️ `modes` {#modes}
Retrieves the supported modes for the control.
#### Type
[`Mode`](../Components/Card.mdx#Mode)

---
### 🏷️ `style` {#style}
Retrieves the control style.
#### Type
`Style` | undefined

---
### 🏷️ `stylesheet` {#stylesheet}
Retrieves the stylesheet for the control.
#### Type
`Stylesheet`

---
### 🏷️ `tab` {#tab}
Retrieves the tab index for the control.
#### Type
number

---
### 🏷️ `validation` {#validation}
Sets or retrieves the validation state of the control.
#### Type
"unknown" | "fail" | "invalid" | "pass" | "await"

## ▶️ Methods {#methods}

---
### 🔧 `autoFocus` {#autoFocus}
Enables auto-focus for the control.
#### Signature
```ts
autoFocus(focusAuto?: boolean): this
```
#### Parameters
| Name        | Type    | Optional | Description                                |
|:------------|:--------|:---------|:-------------------------------------------|
| `focusAuto` | boolean | Yes      | Specifies if auto-focus is enabled or not. |
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `autoValidate` {#autoValidate}
Enables automatic control validation.
#### Signature
```ts
autoValidate(
  fn: (
    control: Control,
    sType: "initial" | "revalidate" | "data" | "visible" | "disabled" | "required" | "locked" | "focus" | "blur",
    callback: Callback
  ) => "unknown" | "fail" | "invalid" | "pass" | "await" | Callback
): this
```
#### Parameters
| Name | Type          | Optional | Description                       |
|:-----|:--------------|:---------|:----------------------------------|
| `fn` | See signature | No       | Specifies the validator function. |
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `await` {#await}
Sets the control validation to waiting.
#### Signature
```ts
await(): this
```
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `bind` {#bind}
Specifies a bind function (invoked when the control is initialized).
#### Signature
```ts
bind(fn: (control: this) => void): this
```
#### Parameters
| Name | Type                 | Optional | Description                        |
|:-----|:---------------------|:---------|:-----------------------------------|
| `fn` | (control: this) => void | No       | Specifies the function to execute. |
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `blur` {#blur}
Blurs the focus of the control.
#### Signature
```ts
blur(): void
```

---
### 🔧 `compactModeOnly` {#compactModeOnly}
Sets the control mode to compact only.
#### Signature
```ts
compactModeOnly(): this
```
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `destroy` {#destroy}
Destroys the control.
#### Signature
```ts
destroy(): void
```

---
### 🔧 `disable` {#disable}
Disables the control.
#### Signature
```ts
disable(): this
```
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `disabled` {#disabled}
Specifies the state of the control.
#### Signature
```ts
disabled(disabled?: boolean): this
```
#### Parameters
| Name       | Type    | Optional | Description                                               |
|:-----------|:--------|:---------|:----------------------------------------------------------|
| `disabled` | boolean | Yes      | Specifies if the control is disabled (default is `true`). |
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `draw` {#draw}
Draws the control.
#### Signature
```ts
draw(parent: Element): void
```
#### Parameters
| Name     | Type      | Optional | Description                           |
|:---------|:----------|:---------|:--------------------------------------|
| `parent` | `Element` | No       | Reference to the parent form element. |

---
### 🔧 `enable` {#enable}
Enables the control.
#### Signature
```ts
enable(): this
```
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `execute` {#execute}
Specifies a function which is invoked when the control is initialized.
#### Signature
```ts
execute(fn: (control: this) => void): this
```
#### Parameters
| Name | Type                    | Optional | Description                        |
|:-----|:------------------------|:---------|:-----------------------------------|
| `fn` | (control: this) => void | No       | Specifies the function to execute. |
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `fail` {#fail}
Sets the control validation to failed.
#### Signature
```ts
fail(): this
```
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `focus` {#focus}
Sets the focus to the control.
#### Signature
```ts
focus(to?: "first" | "last"): boolean
```
#### Parameters
| Name | Type              | Optional | Description                                                                                                           |
|:-----|:------------------|:---------|:----------------------------------------------------------------------------------------------------------------------|
| `to` | "first" \| "last" | Yes      | Specifies if the focus should be set to the first or last item in the control (in case a control has multiple items). |
#### Return value
Returns `true` if the focus is set.

---
### 🔧 `focusTo` {#focusTo}
Sets the focus to the previous or next item in the control. Override this function if you need to shift focus within a control.
#### Signature
```ts
focusTo(to: "previous" | "next"): boolean
```
#### Parameters
| Name | Type                 | Optional | Description                                                    |
|:-----|:---------------------|:---------|:---------------------------------------------------------------|
| `to` | "previous" \| "next" | No       | Specifies if the focus is set to the previous or next control. |
#### Return value
Returns `true` if the focus is set.

---
### 🔧 `groupVisibility` {#groupVisibility}
Specifies the visibility of the control group.
#### Signature
```ts
groupVisibility(visible: boolean): void
```
#### Parameters
| Name      | Type    | Optional | Description                                |
|:----------|:--------|:---------|:-------------------------------------------|
| `visible` | boolean | No       | Specifies if the control group is visible. |

---
### 🔧 `hide` {#hide}
Hides the control.
#### Signature
```ts
hide(): this
```
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔒 `hover` {#hover}
Sets the hover state.
#### Signature
```ts
hover(hover: boolean): void
```
#### Parameters
| Name    | Type    | Optional | Description                       |
|:--------|:--------|:---------|:----------------------------------|
| `hover` | boolean | No       | Specifies if the hover is active. |

---
### 🔧 `indent` {#indent}
Enables control indentation. This can only be set during construction.
#### Signature
```ts
indent(size: number): this
```
#### Parameters
| Name   | Type   | Optional | Description                          |
|:-------|:-------|:---------|:-------------------------------------|
| `size` | number | No       | Specifies the indent size in pixels. |
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔒 `initialized` {#initialized}
Marks the control as initialized.
#### Signature
```ts
initialized(): void
```

---
### 🔧 `invalid` {#invalid}
Sets the control validation to invalid.
#### Signature
```ts
invalid(): this
```
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔒 `keyboard` {#keyboard}
Attach keyboard events to the control.
#### Signature
```ts
keyboard(
  element: Element,
  onEnter?: () => boolean,
  enterOverride?: boolean,
  onKeyPress?: (keyboardEvent: IKeyboardEvent) => boolean
): void
```
#### Parameters
| Name            | Type                                       | Optional | Description                                                                                          |
|:----------------|:-------------------------------------------|:---------|:-----------------------------------------------------------------------------------------------------|
| `element`       | `Element`                                  | No       | Reference to the element to bind the events to.                                                      |
| `onEnter`       | () => boolean                              | Yes      | Specifies the function which is invoked when an enter is detected.                                   |
| `enterOverride` | boolean                                    | Yes      | Overrides the default enter behavior from an element (for example the textarea, default is `false`). |
| `onKeyPress`    | (keyboardEvent: IKeyboardEvent) => boolean | Yes      | Invoked when a key is pressed.                                                                       |

---
### 🔧 `label` {#label}
Sets the label for the control.
:::tip
If you want to use [markdown](https://en.wikipedia.org/wiki/Markdown) in the label, activate it first using the [`markdown`](#markdown) method.
:::
#### Signature
```ts
label(label: string): this
```
#### Parameters
| Name    | Type   | Optional | Description                  |
|:--------|:-------|:---------|:-----------------------------|
| `label` | string | No       | Specifies the control label. |
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `labelCompact` {#labelCompact}
Sets the compact label for the control.
#### Signature
```ts
labelCompact(label: string): this
```
#### Parameters
| Name    | Type   | Optional | Description                          |
|:--------|:-------|:---------|:-------------------------------------|
| `label` | string | No       | Specifies the compact control label. |
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `lock` {#lock}
Locks the control.
#### Signature
```ts
lock(): this
```
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `locked` {#locked}
Specifies if the control is locked (readonly).
#### Signature
```ts
locked(locked?: boolean): this
```
#### Parameters
| Name     | Type    | Optional | Description                                                      |
|:---------|:--------|:---------|:-----------------------------------------------------------------|
| `locked` | boolean | Yes      | Specifies if the control is locked/readonly (default is `true`). |
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `markdown` {#markdown}
Specifies if the label should support markdown formatting.
#### Signature
```ts
markdown(options?: IMarkdownOptions): this
```
#### Parameters
| Name      | Type                                                         | Optional | Description                     |
|:----------|:-------------------------------------------------------------|:---------|:--------------------------------|
| `options` | [`IMarkdownOptions`](../Markdown/index.mdx#IMarkdownOptions) | Yes      | Specifies the markdown options. |
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `mode` {#mode}
Sets the supported mode for the control.
#### Signature
```ts
mode(mode: "compact" | "normal" | "both"): this
```
#### Parameters
| Name   | Type                            | Optional | Description                 |
|:-------|:--------------------------------|:---------|:----------------------------|
| `mode` | "compact" \| "normal" \| "both" | No       | Specifies the desired mode. |
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `normalModeOnly` {#normalModeOnly}
Sets the control mode to normal only.
#### Signature
```ts
normalModeOnly(): this
```
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔒 `onBlur` {#onBlur}
Invoked when the control loses focus.
#### Signature
```ts
onBlur(): void
```

---
### 🔒 `onChange` {#onChange}
Invoked when the control changed.
#### Signature
```ts
onChange(
  change: "visible" | "disabled" | "required" | "locked" | "focus" | "hover" | "validation"
): void
```
#### Parameters
| Name     | Type                                                                                    | Optional | Description                         |
|:---------|:----------------------------------------------------------------------------------------|:---------|:------------------------------------|
| `change` | "visible" \| "disabled" \| "required" \| "locked" \| "focus" \| "hover" \| "validation" | No       | Specifies the change that occurred. |

---
### 🔒 `onDisable` {#onDisable}
Invoked when the control is disabled.
#### Signature
```ts
onDisable(): void
```

---
### 🔒 `onDraw` {#onDraw}
:::caution Abstract method
This method is abstract and needs implementation in the derived control class.
:::
Invoked when the control should be drawn.
#### Signature
```ts
onDraw(parent: Element): Element
```
#### Parameters
| Name     | Type      | Optional | Description                           |
|:---------|:----------|:---------|:--------------------------------------|
| `parent` | `Element` | No       | Reference to the parent form element. |
#### Return value
Returns a reference to the control element.

---
### 🔒 `onEnable` {#onEnable}
Invoked when the control is enabled.
#### Signature
```ts
onEnable(): void
```

---
### 🔒 `onEnter` {#onEnter}
Invoked when the enter (return) key is pressed.
#### Signature
```ts
onEnter(element: Element): boolean
```
#### Parameters
| Name      | Type      | Optional | Description               |
|:----------|:----------|:---------|:--------------------------|
| `element` | `Element` | No       | Reference to the element. |
#### Return value
Return `true` if you want to cancel key bubbling.

---
### 🔒 `onEscape` {#onEscape}
Invoked when the escape key is pressed.
#### Signature
```ts
onEscape(element: Element): boolean
```
#### Parameters
| Name      | Type      | Optional | Description               |
|:----------|:----------|:---------|:--------------------------|
| `element` | `Element` | No       | Reference to the element. |
#### Return value
Return `true` if you want to cancel key bubbling.

---
### 🔒 `onExecute` {#onExecute}
Invoked when the control is executed.
#### Signature
```ts
onExecute(): void
```

---
### 🔒 `onFocus` {#onFocus}
Invoked when the control captures focus.
#### Signature
```ts
onFocus(autoFocus: boolean): void
```
#### Parameters
| Name        | Type    | Optional | Description                            |
|:------------|:--------|:---------|:---------------------------------------|
| `autoFocus` | boolean | No       | Specifies if an auto-focus was active. |

---
### 🔒 `onFocusOptions` {#onFocusOptions}
Invoked when the focus state of the control is changed.
#### Signature
```ts
onFocusOptions(): void
```

---
### 🔧 `onFormActivate` {#onFormActivate}
Invoked when the control form is activated.
#### Signature
```ts
onFormActivate(): void
```

---
### 🔧 `onFormDeactivate` {#onFormDeactivate}
Invoked when the control form is deactivated.
#### Signature
```ts
onFormDeactivate(): void
```

---
### 🔧 `onFormDisable` {#onFormDisable}
Invoked when the control form is disabled.
#### Signature
```ts
onFormDisable(): void
```

---
### 🔧 `onFormEnable` {#onFormEnable}
Invoked when the control form is enabled.
#### Signature
```ts
onFormEnable(): void
```

---
### 🔧 `onFormResize` {#onFormResize}
Invoked when the parent form is resized.
#### Signature
```ts
onFormResize(): void
```

---
### 🔧 `onHide` {#onHide}
Invoked when the control is hidden.
#### Signature
```ts
onHide(): void
```

---
### 🔒 `onHover` {#onHover}
Invoked when the control is hovered.
#### Signature
```ts
onHover(): void
```

---
### 🔧 `onInit` {#onInit}
Invoked when the control is initialized.
#### Signature
```ts
onInit(): boolean
```
#### Return value
Return `true` if the control is initialized.

---
### 🔒 `onLock` {#onLock}
Invoked when the control is locked.
#### Signature
```ts
onLock(): void
```

---
### 🔧 `onMeasure` {#onMeasure}
Invoked when the control is measured. A measure correction in pixels can be supplied in the return value.
#### Signature
```ts
onMeasure(): number
```
#### Return value
Returns the measure correction for the control.

---
### 🔧 `onMode` {#onMode}
Invoked when the control mode is changed.
#### Signature
```ts
onMode(): void
```

---
### 🔒 `onOptional` {#onOptional}
Invoked when the control is optional.
#### Signature
```ts
onOptional(): void
```

---
### 🔧 `onRequestAutoFocus` {#onRequestAutoFocus}
Fired when auto-focus is requested.
#### Signature
```ts
onRequestAutoFocus(): boolean
```
#### Return value
Returns `true` if the focus is set.

---
### 🔒 `onRequire` {#onRequire}
Invoked when the control is required.
#### Signature
```ts
onRequire(): void
```

---
### 🔧 `onResized` {#onResized}
Invoked when the control is resized.
#### Signature
```ts
onResized(): void
```

---
### 🔧 `onShow` {#onShow}
Invoked when the control is shown.
#### Signature
```ts
onShow(): void
```

---
### 🔒 `onUnlock` {#onUnlock}
Invoked when the control is unlocked.
#### Signature
```ts
onUnlock(): void
```

---
### 🔒 `onUpdate` {#onUpdate}
Invoked when the control needs to be updated.
#### Signature
```ts
onUpdate(): void
```

---
### 🔒 `onValidate` {#onValidate}
Invoked when the control validation state is changed.
#### Signature
```ts
onValidate(): void
```

---
### 🔧 `pass` {#pass}
Sets the control validation to passed.
#### Signature
```ts
pass(): this
```
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `readonly` {#readonly}
Makes the control readonly.
#### Signature
```ts
readonly(): this
```
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `redraw` {#redraw}
Redraws a control.
#### Signature
```ts
redraw(): this
```
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `reference` {#reference}
Sets the control reference.
:::tip
This can be used in combination with the [`controlWithReference`](Form.mdx#controlWithReference) method of the [`Form`](Form.mdx) class to retrieve a control from a form (in case you didn't assign the control instance to a variable).
:::
#### Signature
```ts
reference(reference: string): this
```
#### Parameters
| Name        | Type   | Optional | Description              |
|:------------|:-------|:---------|:-------------------------|
| `reference` | string | No       | Specifies the reference. |
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `require` {#require}
Makes the control required.
#### Signature
```ts
require(): this
```
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `required` {#required}
Specifies if the control is required.
#### Signature
```ts
required(required?: boolean): this
```
#### Parameters
| Name       | Type    | Optional | Description                                               |
|:-----------|:--------|:---------|:----------------------------------------------------------|
| `required` | boolean | Yes      | Specifies if the control is required (default is `true`). |
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `resize` {#resize}
Resizes the parent form.
#### Signature
```ts
resize(): void
```

---
### 🔧 `show` {#show}
Shows the control.
#### Signature
```ts
show(): this
```
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `unbind` {#unbind}
Specifies an unbind function (invoked when the control is destroyed).
#### Signature
```ts
unbind(fn: (control: this) => void): this
```
#### Parameters
| Name | Type                 | Optional | Description                        |
|:-----|:---------------------|:---------|:-----------------------------------|
| `fn` | (control: this) => void | No       | Specifies the function to execute. |
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `unlock` {#unlock}
Unlocks the control.
#### Signature
```ts
unlock(): this
```
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔒 `updateControl` {#updateControl}
Updates the control state.
#### Signature
```ts
updateControl(
  change: "visible" | "disabled" | "required" | "locked" | "focus" | "hover" | "validation",
): void
```
| Name     | Type                                                                                    | Optional | Description                |
|:---------|:----------------------------------------------------------------------------------------|:---------|:---------------------------|
| `change` | "visible" \| "disabled" \| "required" \| "locked" \| "focus" \| "hover" \| "validation" | No       | Specifies what is changed. |

---
### 🔒 `updateFocus` {#updateFocus}
Updates the focus state of a control.
#### Signature
```ts
updateFocus(
  hover: boolean,
  autoBlur: boolean,
  virtualElement?: Element,
  physicalElement?: Element
): void
```
#### Parameters
| Name              | Type      | Optional | Description                                                                                                       |
|:------------------|:----------|:---------|:------------------------------------------------------------------------------------------------------------------|
| `hover`           | boolean   | No       | Specifies if the control has captured focus.                                                                      |
| `autoBlur`        | boolean   | No       | Specifies if the auto-blur should be used.                                                                        |
| `virtualElement`  | `Element` | Yes      | Specifies the virtual focus subject (the element that renders the visual focus indication).                       |
| `physicalElement` | `Element` | Yes      | Specifies the physical focus subject (the element that actually gets the focus, for example, a text input field). |

---
### 🔧 `validate` {#validate}
Sets the validation state of the control or invokes a validation cycle if automatic validation is activated.
#### Signature
```ts
validate(
  validation?: "unknown" | "fail" | "invalid" | "pass" | "await" | "initial" | "revalidate" | "data" | "visible" | "disabled" | "required" | "locked" | "focus" | "blur"
): this
```
#### Parameters
| Name         | Type                                                                                                                                                                   | Optional | Description                     |
|:-------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|:---------|:--------------------------------|
| `validation` | "unknown" \| "fail" \| "invalid" \| "pass" \| "await" \| "initial" \| "revalidate" \| "data" \| "visible" \| "disabled" \| "required" \| "locked" \| "focus" \| "blur" | Yes      | Specifies the validation state. |
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `visible` {#visible}
Specifies the visibility of the control.
#### Signature
```ts
visible(visible?: boolean): this
```
#### Parameters
| Name      | Type    | Optional | Description                                              |
|:----------|:--------|:---------|:---------------------------------------------------------|
| `visible` | boolean | Yes      | Specifies if the control is visible (default is `true`). |
#### Return value
Returns a reference to the `Control` instance to allow chaining.

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `IControlProperties` {#IControlProperties}
Describes the interface for the control properties.
#### Type declaration
```ts
interface IControlProperties {
  /* Specifies the control style. */
  style?: IControlStyle;

  /* Specifies the index name for the control styles in the form style object. */
  styleName?: string;

  /* Specifies the default control style. */
  styleDefault?: Style;

  /* Label for the control. */
  label?: string;
}
```
