---
title: Form Checkbox class - Blocks
sidebar_label: Checkbox
description: The Checkbox class defines a checkbox control for the Tripetto builder.
---

import Preview from '@site/src/components/preview.js';

# Checkbox class

## 📖 Description {#class-description}
The `Checkbox` class defines a checkbox control for the Tripetto builder. It is derived from the [`DataControl`](DataControl.mdx) class.

## 📺 Preview {#preview}
<Preview src="forms-checkbox.png" />

---
## 🆕 `constructor` {#constructor}
Creates a new `Checkbox` instance.
#### Signature
```ts
constructor(
  label: string,
  checked?: boolean | Binding,
  style?: ICheckboxStyle
): Checkbox
```
#### Parameters
| Name      | Type                                | Optional | Description                                                                  |
|:----------|:------------------------------------|:---------|:-----------------------------------------------------------------------------|
| `label`   | string                              | No       | Specifies the label.                                                         |
| `checked` | boolean \| `Binding`                | Yes      | Specifies the checkbox state or a data binding created with [`bind`](#bind). |
| `style`   | [`ICheckboxStyle`](#ICheckboxStyle) | Yes      | Specifies the style (when omitted the default global style will be used).    |
#### Return value
Returns a reference to the new `Checkbox` instance.

## 📌 Statics {#statics}

---
### 🏷️ `style` {#statics-style}
Contains the global default style for the checkbox control.
#### Type
[`ICheckboxStyle`](#ICheckboxStyle)

---
### 🔧 `bind` {#bind}
Binds an object property to the control allowing its value to update automatically when the value of the control changes. If the control is disabled or invisible, the supplied `valueWhenInactive` is automatically set as the property value. When a control is enabled and visible, the supplied `defaultWhenActive` is set automatically if the property value is currently not defined.
#### Signature
```ts
bind<Target, P extends keyof Target>(
  target: Target,
  property: P,
  valueWhenInactive: boolean,
  defaultWhenActive?: boolean,
  modifier?: (value?: boolean) => boolean
): Binding;
```
#### Parameters
| Name                | Type                         | Optional | Description                                                                                               |
|:--------------------|:-----------------------------|:---------|:----------------------------------------------------------------------------------------------------------|
| `target`            | `Target`                     | No       | Reference to the target object which contains the property.                                               |
| `property`          | `P`                          | No       | Specifies the name of the property.                                                                       |
| `valueWhenInactive` | boolean                      | No       | Specifies the value which is set to the property when the control is inactive (disabled or invisible).    |
| `defaultWhenActive` | boolean                      | Yes      | Specifies the default (initial) value that will be used when the control is active (enabled and visible). |
| `modifier`          | (value?: boolean) => boolean | Yes      | Specifies a modifier function for the data.                                                               |
#### Return value
Returns a `Binding` instance that can be used in the controls constructor to bind a value to a control.
#### Example
```ts showLineNumbers
import { Forms } from "@tripetto/builder";

const example = {
  state: false
};

const checkbox = new Forms.Checkbox(
  "Toggles the state in the example object",
  Forms.Checkbox.bind(example, "state", false));
```

## 🗃️ Fields {#fields}

---
### 🏷️ `hasFocus` {#hasFocus}
Retrieves if the control has input focus.
#### Type
boolean

---
### 🏷️ `isChecked` {#isChecked}
Sets or retrieves the checkbox state.
#### Type
boolean

---
### 🏷️ `isDisabled` {#isDisabled}
Sets or retrieves if the control is disabled.
#### Type
boolean

---
### 🏷️ `isLocked` {#isLocked}
Sets or retrieves if the control is locked (readonly).
#### Type
boolean

---
### 🏷️ `isObservable` {#isObservable}
Retrieves if the control is observable.
#### Type
boolean

---
### 🏷️ `isVisible` {#isVisible}
Sets or retrieves the visibility of the control.
#### Type
boolean

---
### 🏷️ `style` {#style}
Contains the checkbox style.
#### Type
[`ICheckboxStyle`](#ICheckboxStyle)

## ▶️ Methods {#methods}

---
### 🔧 `blur` {#blur}
Blurs the focus of the control.
#### Signature
```ts
blur(): void
```

---
### 🔧 `checked` {#checked}
Sets the checked state of the checkbox.
#### Signature
```ts
checked(checked?: boolean): this
```
#### Parameters
| Name      | Type    | Optional | Description          |
|:----------|:--------|:---------|:---------------------|
| `checked` | boolean | Yes      | Specifies the state. |
#### Return value
Returns a reference to the `Checkbox` instance to allow chaining.

---
### 🔧 `check` {#check}
Checks the checkbox.
#### Signature
```ts
check(): this
```
#### Return value
Returns a reference to the `Checkbox` instance to allow chaining.

---
### 🔧 `description` {#description}
Sets a description for the control.
#### Signature
```ts
description(description: string): this
```
#### Parameters
| Name          | Type   | Optional | Description                       |
|:--------------|:-------|:---------|:----------------------------------|
| `description` | string | No       | Specifies the description string. |
#### Return value
Returns a reference to the `Checkbox` instance to allow chaining.

---
### 🔧 `disable` {#disable}
Disables the control.
#### Signature
```ts
disable(): this
```
#### Return value
Returns a reference to the `Checkbox` instance to allow chaining.

---
### 🔧 `disabled` {#disabled}
Specifies the state of the control.
#### Signature
```ts
disabled(disabled?: boolean): this
```
#### Parameters
| Name       | Type    | Optional | Description                                               |
|:-----------|:--------|:---------|:----------------------------------------------------------|
| `disabled` | boolean | Yes      | Specifies if the control is disabled (default is `true`). |
#### Return value
Returns a reference to the `Checkbox` instance to allow chaining.

---
### 🔧 `enable` {#enable}
Enables the control.
#### Signature
```ts
enable(): this
```
#### Return value
Returns a reference to the `Checkbox` instance to allow chaining.

---
### 🔧 `focus` {#focus}
Sets the focus to the control.
#### Signature
```ts
focus(): boolean
```
#### Return value
Returns `true` if the focus is set.

---
### 🔧 `hide` {#hide}
Hides the control.
#### Signature
```ts
hide(): this
```
#### Return value
Returns a reference to the `Checkbox` instance to allow chaining.

---
### 🔧 `indent` {#indent}
Enables control indentation. This can only be set during construction.
#### Signature
```ts
indent(size: number): this
```
#### Parameters
| Name   | Type   | Optional | Description                          |
|:-------|:-------|:---------|:-------------------------------------|
| `size` | number | No       | Specifies the indent size in pixels. |
#### Return value
Returns a reference to the `Control` instance to allow chaining.

---
### 🔧 `label` {#label}
Sets the label for the control.
:::tip
If you want to use [markdown](https://en.wikipedia.org/wiki/Markdown) in the label, activate it first using the [`markdown`](#markdown) method.
:::
#### Signature
```ts
label(label: string): this
```
#### Parameters
| Name    | Type   | Optional | Description                  |
|:--------|:-------|:---------|:-----------------------------|
| `label` | string | No       | Specifies the control label. |
#### Return value
Returns a reference to the `Checkbox` instance to allow chaining.

---
### 🔧 `lock` {#lock}
Locks the control.
#### Signature
```ts
lock(): this
```
#### Return value
Returns a reference to the `Checkbox` instance to allow chaining.

---
### 🔧 `locked` {#locked}
Specifies if the control is locked (readonly).
#### Signature
```ts
locked(locked?: boolean): this
```
#### Parameters
| Name     | Type    | Optional | Description                                                      |
|:---------|:--------|:---------|:-----------------------------------------------------------------|
| `locked` | boolean | Yes      | Specifies if the control is locked/readonly (default is `true`). |
#### Return value
Returns a reference to the `Checkbox` instance to allow chaining.

---
### 🔧 `markdown` {#markdown}
Specifies if the label should support markdown formatting.
#### Signature
```ts
markdown(options?: IMarkdownOptions): this
```
#### Parameters
| Name      | Type                                                         | Optional | Description                     |
|:----------|:-------------------------------------------------------------|:---------|:--------------------------------|
| `options` | [`IMarkdownOptions`](../Markdown/index.mdx#IMarkdownOptions) | Yes      | Specifies the markdown options. |
#### Return value
Returns a reference to the `Checkbox` instance to allow chaining.

---
### 🔧 `on` {#on}
Specifies the function which is invoked when the checkbox is toggled.
#### Signature
```ts
on(fn: (checkbox: Checkbox) => void): this
```
#### Parameters
| Name | Type                           | Optional | Description                    |
|:-----|:-------------------------------|:---------|:-------------------------------|
| `fn` | (checkbox: `Checkbox`) => void | No       | Specifies the toggle function. |
#### Return value
Returns a reference to the `Checkbox` instance to allow chaining.

---
### 🔧 `readonly` {#readonly}
Makes the control readonly.
#### Signature
```ts
readonly(): this
```
#### Return value
Returns a reference to the `Checkbox` instance to allow chaining.

---
### 🔧 `show` {#show}
Shows the control.
#### Signature
```ts
show(): this
```
#### Return value
Returns a reference to the `Checkbox` instance to allow chaining.

---
### 🔧 `toggle` {#toggle}
Toggles the checkbox.
#### Signature
```ts
toggle(): this
```
#### Return value
Returns a reference to the `Checkbox` instance to allow chaining.

---
### 🔧 `uncheck` {#uncheck}
Unchecks the checkbox.
#### Signature
```ts
uncheck(): this
```
#### Return value
Returns a reference to the `Checkbox` instance to allow chaining.

---
### 🔧 `unlock` {#unlock}
Unlocks the control.
#### Signature
```ts
unlock(): this
```
#### Return value
Returns a reference to the `Checkbox` instance to allow chaining.

---
### 🔧 `visible` {#visible}
Specifies the visibility of the control.
#### Signature
```ts
visible(visible?: boolean): this
```
#### Parameters
| Name      | Type    | Optional | Description                                              |
|:----------|:--------|:---------|:---------------------------------------------------------|
| `visible` | boolean | Yes      | Specifies if the control is visible (default is `true`). |
#### Return value
Returns a reference to the `Checkbox` instance to allow chaining.

---
### 🔧 `width` {#width}
Sets the width of the control.
#### Signature
```ts
width(width: number): this
```
#### Parameters
| Name    | Type   | Optional | Description                            |
|:--------|:-------|:---------|:---------------------------------------|
| `width` | number | No       | Specifies the control width in pixels. |
#### Return value
Returns a reference to the `Checkbox` instance to allow chaining.

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `ICheckboxStyle` {#ICheckboxStyle}
Describes the interface for the checkbox styles.
#### Type declaration
```ts
interface ICheckboxStyle {
  /* Checkbox appearance. */
  appearance?: IStyles;

  /* Checkbox label. */
  label?: IStyles;

  /* Checkbox description. */
  description?: IStyles;

  /* Checked state. */
  checked?: IStyles;

  /* Unchecked state. */
  unchecked?: IStyles;

  /* Checkbox disabled. */
  disabled?: ICheckboxStyles;

  /* Checkbox required. */
  required?: ICheckboxStyles;

  /* Checkbox locked. */
  locked?: ICheckboxStyles;

  /* Checkbox focus. */
  focus?: ICheckboxStyles;

  /* Checkbox hover. */
  hover?: ICheckboxStyles;

  /* Validation passed. */
  passed?: ICheckboxStyles;

  /* Validation failed. */
  failed?: ICheckboxStyles;

  /* Validation awaiting. */
  awaiting?: ICheckboxStyles;

  /* Fused with form card. */
  fused?: {
    /* Checkbox appearance. */
    appearance?: IStyles;

    /* Checkbox required. */
    required?: IStyles;

    /* Checkbox focus. */
    focus?: ICheckboxStyles;

    /* Checkbox hover. */
    hover?: ICheckboxStyles;
  };
}
```
