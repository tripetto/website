---
title: Form Email class - Blocks
sidebar_label: Email
description: The Email class defines an email input control for the Tripetto builder.
---

import Preview from '@site/src/components/preview.js';

# Email class

## 📖 Description {#description}
The `Email` class defines an email input control for the Tripetto builder. It is derived from the [`Text`](Text.mdx) class.

## 📺 Preview {#preview}
<Preview src="forms-email.png" />

---
## 🆕 `constructor` {#constructor}
Creates a new `Email` instance.
#### Signature
```ts
constructor(value?: string | Binding, style?: ITextStyle): Email
```
#### Parameters
| Name    | Type                                | Optional | Description                                                                     |
|:--------|:------------------------------------|:---------|:--------------------------------------------------------------------------------|
| `value` | string \| `Binding`                 | Yes      | Specifies the initially value or a data binding created with [`bind`](#bind).   |
| `style` | [`ITextStyle`](Text.mdx#ITextStyle) | Yes      | Specifies the input style (when omitted the default global style will be used). |
#### Return value
Returns a reference to the new `Email` instance.

## 📌 Statics {#statics}

---
### 🏷️ `style` {#statics-style}
Contains the global default style for the input control.
#### Type
[`ITextStyle`](Text.mdx#ITextStyle)

---
### 🔧 `bind` {#bind}
Binds an object property to the control allowing its value to update automatically when the value of the control changes. If the control is disabled or invisible, the supplied `valueWhenInactive` is automatically set as the property value. When a control is enabled and visible, the supplied `defaultWhenActive` is set automatically if the property value is currently not defined.
#### Signature
```ts
bind<Target, P extends keyof Target>(
  target: Target,
  property: P,
  valueWhenInactive: string,
  defaultWhenActive?: string,
  modifier?: (value?: string) => string
): Binding;
```
#### Parameters
| Name                | Type                       | Optional | Description                                                                                               |
|:--------------------|:---------------------------|:---------|:----------------------------------------------------------------------------------------------------------|
| `target`            | `Target`                   | No       | Reference to the target object which contains the property.                                               |
| `property`          | `P`                        | No       | Specifies the name of the property.                                                                       |
| `valueWhenInactive` | string                     | No       | Specifies the value which is set to the property when the control is inactive (disabled or invisible).    |
| `defaultWhenActive` | string                     | Yes      | Specifies the default (initial) value that will be used when the control is active (enabled and visible). |
| `modifier`          | (value?: string) => string | Yes      | Specifies a modifier function for the data.                                                               |
#### Return value
Returns a `Binding` instance that can be used in the controls constructor to bind a value to a control.
#### Example
```ts showLineNumbers
import { Forms } from "@tripetto/builder";

const example = {
  emailAddress: ""
};

const emailAddress = new Forms.Email(Forms.Email.bind(example, "emailAddress", ""));
```

## 🗃️ Fields {#fields}

---
### 🏷️ `hasFocus` {#hasFocus}
Retrieves if the control has input focus.
#### Type
boolean

---
### 🏷️ `isDisabled` {#isDisabled}
Sets or retrieves if the control is disabled.
#### Type
boolean

---
### 🏷️ `isLocked` {#isLocked}
Sets or retrieves if the control is locked (readonly).
#### Type
boolean

---
### 🏷️ `isObservable` {#isObservable}
Retrieves if the control is observable.
#### Type
boolean

---
### 🏷️ `isRequired` {#isRequired}
Sets or retrieves if the control is required.
#### Type
boolean

---
### 🏷️ `isVisible` {#isVisible}
Sets or retrieves the visibility of the control.
#### Type
boolean

---
### 🏷️ `style` {#style}
Contains the email control style.
#### Type
[`ITextStyle`](Text.mdx#ITextStyle)

---
### 🏷️ `value` {#value}
Sets or retrieves the input value.
#### Type
string

## ▶️ Methods {#methods}

---
### 🔧 `blur` {#blur}
Blurs the focus of the control.
#### Signature
```ts
blur(): void
```

---
### 🔧 `align` {#align}
Sets the alignment of the control.
#### Signature
```ts
align(align: "left" | "center" | "right"): this
```
#### Parameters
| Name    | Type                          | Optional | Description              |
|:--------|:------------------------------|:---------|:-------------------------|
| `align` | "left" \| "center" \| "right" | No       | Specifies the alignment. |
#### Return value
Returns a reference to the `Email` instance to allow chaining.

---
### 🔧 `autoSelect` {#autoSelect}
Specifies if the text needs to be selected automatically.
#### Signature
```ts
autoSelect(select?: "no" | "focus" | "auto-focus"): this
```
#### Parameters
| Name     | Type                            | Optional | Description                                 |
|:---------|:--------------------------------|:---------|:--------------------------------------------|
| `select` | "no" \| "focus" \| "auto-focus" | Yes      | Specifies if the text needs to be selected. |
#### Return value
Returns a reference to the `Email` instance to allow chaining.

---
### 🔧 `autoValidate` {#autoValidate}
Enables automatic control validation.
#### Signature
```ts
autoValidate(): this
```
#### Return value
Returns a reference to the `Email` instance to allow chaining.

---
### 🔧 `copyToClipboard` {#copyToClipboard}
Copies the contents of the input control to the clipboard.
#### Signature
```ts
copyToClipboard(): void
```

---
### 🔧 `disable` {#disable}
Disables the control.
#### Signature
```ts
disable(): this
```
#### Return value
Returns a reference to the `Email` instance to allow chaining.

---
### 🔧 `disabled` {#disabled}
Specifies the state of the control.
#### Signature
```ts
disabled(disabled?: boolean): this
```
#### Parameters
| Name       | Type    | Optional | Description                                               |
|:-----------|:--------|:---------|:----------------------------------------------------------|
| `disabled` | boolean | Yes      | Specifies if the control is disabled (default is `true`). |
#### Return value
Returns a reference to the `Email` instance to allow chaining.

---
### 🔧 `enable` {#enable}
Enables the control.
#### Signature
```ts
enable(): this
```
#### Return value
Returns a reference to the `Email` instance to allow chaining.

---
### 🔧 `enter` {#enter}
Specifies the function which is invoked when the user presses the enter key.
#### Signature
```ts
enter(fn: (email: Email) => boolean): this
```
#### Parameters
| Name | Type                        | Optional | Description                                                                                        |
|:-----|:----------------------------|:---------|:---------------------------------------------------------------------------------------------------|
| `fn` | (email: `Email`) => boolean | No       | Specifies the function to invoke. To cancel the normal behavior return `true` within the function. |
#### Return value
Returns a reference to the `Email` instance to allow chaining.

---
### 🔧 `escape` {#escape}
Specifies the function which is invoked when the user presses the escape key.
#### Signature
```ts
escape<T extends Text>(fn: (email: Email) => boolean): this
```
#### Parameters
| Name | Type                        | Optional | Description                                                                                        |
|:-----|:----------------------------|:---------|:---------------------------------------------------------------------------------------------------|
| `fn` | (email: `Email`) => boolean | No       | Specifies the function to invoke. To cancel the normal behavior return `true` within the function. |
#### Return value
Returns a reference to the `Email` instance to allow chaining.

---
### 🔧 `focus` {#focus}
Sets the focus to the control.
#### Signature
```ts
focus(): boolean
```
#### Return value
Returns `true` if the focus is set.

---
### 🔧 `hide` {#hide}
Hides the control.
#### Signature
```ts
hide(): this
```
#### Return value
Returns a reference to the `Email` instance to allow chaining.

---
### 🔧 `indent` {#indent}
Enables control indentation. This can only be set during construction.
#### Signature
```ts
indent(size: number): this
```
#### Parameters
| Name   | Type   | Optional | Description                          |
|:-------|:-------|:---------|:-------------------------------------|
| `size` | number | No       | Specifies the indent size in pixels. |
#### Return value
Returns a reference to the `Email` instance to allow chaining.

---
### 🔧 `label` {#label}
Sets the label for the control.
:::tip
If you want to use [markdown](https://en.wikipedia.org/wiki/Markdown) in the label, activate it first using the [`markdown`](#markdown) method.
:::
#### Signature
```ts
label(label: string): this
```
#### Parameters
| Name    | Type   | Optional | Description                  |
|:--------|:-------|:---------|:-----------------------------|
| `label` | string | No       | Specifies the control label. |
#### Return value
Returns a reference to the `Email` instance to allow chaining.

---
### 🔧 `lock` {#lock}
Locks the control.
#### Signature
```ts
lock(): this
```
#### Return value
Returns a reference to the `Email` instance to allow chaining.

---
### 🔧 `locked` {#locked}
Specifies if the control is locked (readonly).
#### Signature
```ts
locked(locked?: boolean): this
```
#### Parameters
| Name     | Type    | Optional | Description                                                      |
|:---------|:--------|:---------|:-----------------------------------------------------------------|
| `locked` | boolean | Yes      | Specifies if the control is locked/readonly (default is `true`). |
#### Return value
Returns a reference to the `Email` instance to allow chaining.

---
### 🔧 `markdown` {#markdown}
Specifies if the label should support markdown formatting.
#### Signature
```ts
markdown(options?: IMarkdownOptions): this
```
#### Parameters
| Name      | Type                                                         | Optional | Description                     |
|:----------|:-------------------------------------------------------------|:---------|:--------------------------------|
| `options` | [`IMarkdownOptions`](../Markdown/index.mdx#IMarkdownOptions) | Yes      | Specifies the markdown options. |
#### Return value
Returns a reference to the `Email` instance to allow chaining.

---
### 🔧 `on` {#on}
Specifies the function which is invoked when the input value changes.
#### Signature
```ts
on(fn: (email: Email) => void): this
```
#### Parameters
| Name | Type                   | Optional | Description                    |
|:-----|:-----------------------|:---------|:-------------------------------|
| `fn` | (email: Email) => void | No       | Specifies the change function. |
#### Return value
Returns a reference to the `Email` instance to allow chaining.

---
### 🔧 `placeholder` {#placeholder}
Sets a placeholder for the email control.
#### Signature
```ts
placeholder(placeholder: string): this
```
#### Parameters
| Name          | Type   | Optional | Description                     |
|:--------------|:-------|:---------|:--------------------------------|
| `placeholder` | string | No       | Specifies the placeholder text. |
#### Return value
Returns a reference to the `Email` instance to allow chaining.

---
### 🔧 `readonly` {#readonly}
Makes the control readonly.
#### Signature
```ts
readonly(): this
```
#### Return value
Returns a reference to the `Email` instance to allow chaining.

---
### 🔧 `require` {#require}
Makes the control required.
#### Signature
```ts
require(): this
```
#### Return value
Returns a reference to the `Email` instance to allow chaining.

---
### 🔧 `required` {#required}
Specifies if the control is required.
#### Signature
```ts
required(required?: boolean): this
```
#### Parameters
| Name       | Type    | Optional | Description                                               |
|:-----------|:--------|:---------|:----------------------------------------------------------|
| `required` | boolean | Yes      | Specifies if the control is required (default is `true`). |
#### Return value
Returns a reference to the `Email` instance to allow chaining.

---
### 🔧 `select` {#select}
Selects the text.
#### Signature
```ts
select(): this
```
#### Return value
Returns a reference to the `Email` instance to allow chaining.

---
### 🔧 `show` {#show}
Shows the control.
#### Signature
```ts
show(): this
```
#### Return value
Returns a reference to the `Email` instance to allow chaining.

---
### 🔧 `unlock` {#unlock}
Unlocks the control.
#### Signature
```ts
unlock(): this
```
#### Return value
Returns a reference to the `Email` instance to allow chaining.

---
### 🔧 `visible` {#visible}
Specifies the visibility of the control.
#### Signature
```ts
visible(visible?: boolean): this
```
#### Parameters
| Name      | Type    | Optional | Description                                              |
|:----------|:--------|:---------|:---------------------------------------------------------|
| `visible` | boolean | Yes      | Specifies if the control is visible (default is `true`). |
#### Return value
Returns a reference to the `Email` instance to allow chaining.

---
### 🔧 `width` {#width}
Sets the width of the control.
#### Signature
```ts
width(width: number): this
```
#### Parameters
| Name    | Type   | Optional | Description                            |
|:--------|:-------|:---------|:---------------------------------------|
| `width` | number | No       | Specifies the control width in pixels. |
#### Return value
Returns a reference to the `Email` instance to allow chaining.
