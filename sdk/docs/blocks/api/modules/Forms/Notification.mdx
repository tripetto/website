---
title: Form Notification class - Blocks
sidebar_label: Notification
description: The Notification class defines a notification control for the Tripetto builder.
---

import Preview from '@site/src/components/preview.js';

# Notification class

## 📖 Description {#description}
The `Notification` class defines a notification control for the Tripetto builder. It is derived from the [`Control`](Control.mdx) class.

## 📺 Preview {#preview}
<Preview src="forms-notification.png" />

---
## 🆕 `constructor` {#constructor}
Creates a new `Notification` instance.
#### Signature
```ts
constructor(
  label: string,
  type?: "normal" | "info" | "success" | "warning" | "error",
  style?: INotificationStyle
): Notification
```
#### Parameters
| Name    | Type                                                    | Optional | Description                                                                            |
|:--------|:--------------------------------------------------------|:---------|:---------------------------------------------------------------------------------------|
| `label` | string                                                  | No       | Specifies the label to show.                                                           |
| `type`  | "normal" \| "info" \| "success" \| "warning" \| "error" | Yes      | Specifies the type.                                                                    |
| `style` | [`INotificationStyle`](#INotificationStyle)             | Yes      | Specifies the notification style (when omitted the default global style will be used). |
#### Return value
Returns a reference to the new `Notification` instance.

## 📌 Statics {#statics}

---
### 🏷️ `style` {#statics-style}
Contains the global default style for the notification control.
#### Type {#signature}
[`INotificationStyle`](#INotificationStyle)

## 🗃️ Fields {#fields}

---
### 🏷️ `isDisabled` {#isDisabled}
Sets or retrieves if the control is disabled.
#### Type {#signature}
boolean

---
### 🏷️ `isVisible` {#isVisible}
Sets or retrieves the visibility of the control.
#### Type {#signature}
boolean

---
### 🏷️ `notificationType` {#notificationType}
Sets or retrieves the notification type.
#### Type {#signature}
"normal" | "info" | "success" | "warning" | "error"

---
### 🏷️ `style` {#style}
Contains the style for the notification control.
#### Type {#signature}
[`INotificationStyle`](#INotificationStyle)

## ▶️ Methods {#methods}

---
### 🔧 `disable` {#disable}
Disables the control.
#### Signature
```ts
disable(): this
```
#### Return value
Returns a reference to the `Notification` instance to allow chaining.

---
### 🔧 `disabled` {#disabled}
Specifies the state of the control.
#### Signature
```ts
disabled(disabled?: boolean): this
```
#### Parameters
| Name       | Type    | Optional | Description                                               |
|:-----------|:--------|:---------|:----------------------------------------------------------|
| `disabled` | boolean | Yes      | Specifies if the control is disabled (default is `true`). |
#### Return value
Returns a reference to the `Notification` instance to allow chaining.

---
### 🔧 `enable` {#enable}
Enables the control.
#### Signature
```ts
enable(): this
```
#### Return value
Returns a reference to the `Notification` instance to allow chaining.

---
### 🔧 `hide` {#hide}
Hides the control.
#### Signature
```ts
hide(): this
```
#### Return value
Returns a reference to the `Notification` instance to allow chaining.

---
### 🔧 `indent` {#indent}
Enables control indentation. This can only be set during construction.
#### Signature
```ts
indent(size: number): this
```
#### Parameters
| Name   | Type   | Optional | Description                          |
|:-------|:-------|:---------|:-------------------------------------|
| `size` | number | No       | Specifies the indent size in pixels. |
#### Return value
Returns a reference to the `Notification` instance to allow chaining.

---
### 🔧 `show` {#show}
Shows the control.
#### Signature
```ts
show(): this
```
#### Return value
Returns a reference to the `Notification` instance to allow chaining.

---
### 🔧 `type` {#type}
Specifies the notification type.
#### Signature
```ts
type(type: "normal" | "info" | "success" | "warning" | "error"): this
```
#### Parameters
| Name   | Type                                                    | Optional | Description         |
|:-------|:--------------------------------------------------------|:---------|:--------------------|
| `type` | "normal" \| "info" \| "success" \| "warning" \| "error" | No       | Specifies the type. |
#### Return value
Returns a reference to the `Notification` instance to allow chaining.

---
### 🔧 `visible` {#visible}
Specifies the visibility of the control.
#### Signature
```ts
visible(visible?: boolean): this
```
#### Parameters
| Name      | Type    | Optional | Description                                              |
|:----------|:--------|:---------|:---------------------------------------------------------|
| `visible` | boolean | Yes      | Specifies if the control is visible (default is `true`). |
#### Return value
Returns a reference to the `Notification` instance to allow chaining.

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `INotificationStyle` {#INotificationStyle}
Describes the interface for the notification styles.
#### Type declaration
```ts
interface INotificationStyle {
  /* Notification appearance. */
  appearance?: IStyles;

  /* Notification appearance when fused with the form card. */
  fused?: IStyles;

  /* Normal notification. */
  normal?: IStyles;

  /* Info notification. */
  info?: IStyles;

  /* Success notification. */
  success?: IStyles;

  /* Warning notification. */
  warning?: IStyles;

  /* Error notification. */
  error?: IStyles;
}
```
