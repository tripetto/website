---
title: Form Form class - Blocks
sidebar_label: Form
toc_max_heading_level: 4
description: The Form class defines a form for the Tripetto builder.
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# Form class

## 📖 Description {#description}
The `Form` class defines a form for the Tripetto builder. It is derived from the [`Components.Card`](../Components/Card.mdx) class. You can use any of the following form controls (or make your own control with the [`Control`](Control.mdx) or [`DataControl`](DataControl.mdx) abstract classes):

- [`Button`](Button.mdx)
- [`Checkbox`](Checkbox.mdx)
- [`ColorPicker`](ColorPicker.mdx)
- [`DateTime`](DateTime.mdx)
- [`Dropdown`](Dropdown.mdx)
- [`Email`](Email.mdx)
- [`Group`](Group.mdx)
- [`HTML`](HTML.mdx)
- [`Notification`](Notification.mdx)
- [`Numeric`](Numeric.mdx)
- [`Radiobutton`](Radiobutton.mdx)
- [`Spacer`](Spacer.mdx)
- [`Static`](Static.mdx)
- [`Text`](Text.mdx)

---
## 🆕 `constructor` {#constructor}
Creates a new `Form` instance.
##### Signature
```ts
constructor(properties: IFormProperties): Form
```
##### Parameters
| Name         | Type                                  | Optional | Description                    |
|:-------------|:--------------------------------------|:---------|:-------------------------------|
| `properties` | [`IFormProperties`](#IFormProperties) | No       | Specifies the form properties. |
##### Return value
Returns a reference to the new `Form` instance.

## 📌 Statics {#statics}

---
### 🏷️ `style` {#style}
Contains the global default style for the form.
##### Type
[`IFormStyle`](#IFormStyle)

## 🗃️ Fields {#fields}

---
### 🏷️ `count` {#count}
Retrieves the number of controls in the form.
##### Type
number

---
### 🏷️ `feature` {#feature}
Sets or retrieves a reference to the feature.
##### Type
[`Feature`](../Components/Feature.mdx) | undefined

---
### 🏷️ `hasFocus` {#hasFocus}
Retrieves if the form has focus.
##### Type
boolean

---
### 🏷️ `isActivated` {#isActivated}
Retrieves if the form is activated.
##### Type
boolean

---
### 🏷️ `isAwaiting` {#isAwaiting}
Retrieves if the form validation is awaiting.
##### Type
boolean

---
### 🏷️ `isDisabled` {#isDisabled}
Sets or retrieves if the form is disabled.
##### Type
boolean

---
### 🏷️ `isFailed` {#isFailed}
Retrieves if the form validation has failed.
##### Type
boolean

---
### 🏷️ `isInvalid` {#isInvalid}
Retrieves if the form validation is invalid.
##### Type
boolean

---
### 🏷️ `isObservable` {#isObservable}
Retrieves if the form is observable.
##### Type
boolean

---
### 🏷️ `isPassed` {#isPassed}
Retrieves if the form validation has passed.
##### Type
boolean

---
### 🏷️ `isVisible` {#isVisible}
Sets or retrieves if the form is visible.
##### Type
boolean

---
### 🏷️ `title` {#title}
Sets or retrieves the title of the form.
##### Type
string

---
### 🏷️ `validation` {#validation}
Retrieves the validation state of the form.
##### Type
"unknown" | "fail" | "invalid" | "pass" | "await"

## ▶️ Methods {#methods}

---
### 🔧 `control` {#control}
Retrieves the control at the specified index.
##### Signature
```ts
control(index: number): Control | undefined
```
##### Parameters
| Name    | Type   | Optional | Description                                                                    |
|:--------|:-------|:---------|:-------------------------------------------------------------------------------|
| `index` | number | No       | Specifies the control index where the first control in the form has index `0`. |
##### Return value
Returns the [`Control`](Control.mdx) instance or `undefined` if the control is not found.

---
### 🔧 `controlWithReference` {#controlWithReference}
Retrieves the control with the specified [reference](Control.mdx#reference).
:::tip
The reference of a control is set using the [`reference`](Control.mdx#reference) method of a control derived from the [`Control`](Control.mdx) base class.
:::
##### Signature
```ts
controlWithReference(reference: string): Control | undefined
```
##### Parameters
| Name        | Type   | Optional | Description                      |
|:------------|:-------|:---------|:---------------------------------|
| `reference` | string | No       | Specifies the control reference. |
##### Return value
Returns the [`Control`](Control.mdx) instance or `undefined` if the control is not found.

---
### 🔧 `deactivate` {#deactivate}
Deactivates the form.
##### Signature
```ts
deactivate(): this
```
##### Return value
Returns a reference to the `Form` instance to allow chaining.

---
### 🔧 `each` {#each}
Iterates through all controls in a form.
##### Signature
```ts
each(fn: (control: Control) => void): void
```
##### Parameters
| Name | Type                                        | Optional | Description                                              |
|:-----|:--------------------------------------------|:---------|:---------------------------------------------------------|
| `fn` | (control: [`Control`](Control.mdx)) => void | No       | Contains the function which is invoked for each control. |

---
### 🔧 `focus` {#focus}
Sets focus to the first or last control in the form.
##### Signature
```ts
focus(to?: "first" | "last"): boolean
```
##### Parameters
| Name | Type              | Optional | Description                                                                      |
|:-----|:------------------|:---------|:---------------------------------------------------------------------------------|
| `to` | "first" \| "last" | Yes      | Specifies if the focus is set to the first or last control (default is `first`). |
##### Return value
Returns `true` if a control captured focus.

---
### 🔧 `focusTo` {#focusTo}
Sets the focus to the previous or next control.
##### Signature
```ts
focusTo(to: "previous" | "next"): boolean
```
##### Parameters
| Name | Type                 | Optional | Description                                                    |
|:-----|:---------------------|:---------|:---------------------------------------------------------------|
| `to` | "previous" \| "next" | No       | Specifies if the focus is set to the previous or next control. |
##### Return value
Returns `true` if the focus is shifted.

---
### 🔧 `hide` {#hide}
Hides the form.
##### Signature
```ts
hide(): this
```
##### Return value
Returns a reference to the `Form` instance to allow chaining.

---
### 🔧 `scrollIntoView` {#scrollIntoView}
Scrolls the form into view.
##### Signature
```ts
scrollIntoView(): this
```
##### Return value
Returns a reference to the `Form` instance to allow chaining.

---
### 🔧 `show` {#show}
Shows the form.
##### Signature
```ts
show(): this
```
##### Return value
Returns a reference to the `Form` instance to allow chaining.

---
### 🔧 `validate` {#validate}
Validates the form.
:::info
Form validation is performed automatically. You only need to call this method to force a validation cycle.
:::
##### Signature
```ts
validate(): this
```
##### Return value
Returns a reference to the `Form` instance to allow chaining.

---
### 🔧 `visible` {#visible}
Shows or hides the form.
##### Signature
```ts
visible(visible: boolean, scrollIntoView?: boolean): this
```
##### Parameters
| Name             | Type    | Optional | Description                                    |
|:-----------------|:--------|:---------|:-----------------------------------------------|
| `visible`        | boolean | No       | Specifies if the form is visible.              |
| `scrollIntoView` | boolean | Yes      | Specifies if the form should scroll into view. |
##### Return value
Returns a reference to the `Form` instance to allow chaining.

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `IFormProperties` {#IFormProperties}
Describes the interface for declaring the form properties.
##### Type declaration
<InterfaceDeclaration prefix="#IFormProperties-" src={`interface IFormProperties {
  controls: (Control | Group)[];
  title?: string;
  markdown?: boolean | IMarkdownOptions;
  disabled?: boolean;
  mode?: "normal" | "compact" | "both";
}`} symbols={{
  "Control": "/blocks/api/modules/Forms/Control/",
  "Group": "/blocks/api/modules/Forms/Group/",
  "IMarkdownOptions": "/blocks/api/modules/Markdown/#IMarkdownOptions/"
}} />

---
#### 🏷️ `controls` {#IFormProperties-controls}
Specifies the controls for the form.
##### Type
([`Control`](Control.mdx) | [`Group`](Group.mdx))[]

---
#### 🏷️ `disabled` {#IFormProperties-disabled}
Specifies if the form is disabled.
##### Type
boolean

---
#### 🏷️ `markdown` {#IFormProperties-markdown}
Specifies if markdown is supported in the form title.
##### Type
boolean | [`IMarkdownOptions`](../Markdown/index.mdx#IMarkdownOptions)

---
#### 🏷️ `mode` {#IFormProperties-mode}
Specifies the supported modes for the form (default is `both`).
##### Type
"normal" | "compact" | "both"

---
#### 🏷️ `title` {#IFormProperties-title}
Specifies the form title.
##### Type
string

---
### 🔗 `IFormStyle` {#IFormStyle}
Describes the interface for the form styles.
##### Type declaration
```ts
interface IFormStyle {
  /* Form appearance. */
  appearance?: IStyles;

  /* Form title. */
  title?: IStyles;

  /* Form disabled. */
  disabled?: IStyles;

  /* Form locked. */
  locked?: IStyles;

  /* Form required. */
  required?: IStyles;

  /* Form focus. */
  focus?: IStyles;

  /* Form validation passed. */
  passed?: IStyles;

  /* Form validation failed. */
  failed?: IStyles;

  /* Form validation awaiting. */
  awaiting?: IStyles;

  /* Form normal mode. */
  normal?: IStyles;

  /* Form Compact mode. */
  compact?: IStyles;

  /* Contains the number of indent pixels per level. */
  indentation?: number;

  /* Fused form. */
  fused?: {
    /* Fused form appearance. */
    appearance?: IStyles;

    /* Fused form title. */
    title?: IStyles;
  };

  /* Control styles. */
  controls?: {
    [control: string]: IControlStyle;
  };
}
```
