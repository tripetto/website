---
title: Form Button class - Blocks
sidebar_label: Button
description: The Button class defines a button control for the Tripetto builder.
---

import Preview from '@site/src/components/preview.js';

# Button class

## 📖 Description {#description}
The `Button` class defines a button control for the Tripetto builder. It is derived from the [`Control`](Control.mdx) class.

## 📺 Preview {#preview}
<Preview src="forms-button.png" />

---
## 🆕 `constructor` {#constructor}
Creates a new `Button` instance.
#### Signature
```ts
constructor(
  label: string,
  type?: "normal" | "accept" | "warning" | "cancel",
  style?: IButtonStyle
): Button
```
#### Parameters
| Name    | Type                                          | Optional | Description                                                                      |
|:--------|:----------------------------------------------|:---------|:---------------------------------------------------------------------------------|
| `label` | string                                        | No       | Specifies the label for the button.                                              |
| `type`  | "normal" \| "accept" \| "warning" \| "cancel" | Yes      | Specifies the button type.                                                       |
| `style` | [`IButtonStyle`](#IButtonStyle)               | Yes      | Specifies the button style (when omitted the default global style will be used). |
#### Return value
Returns a reference to the new `Button` instance.

## 📌 Statics {#statics}

---
### 🏷️ `style` {#statics-style}
Contains the global default style for the button control.
#### Type {#signature}
[`IButtonStyle`](#IButtonStyle)

## 🗃️ Fields {#fields}

---
### 🏷️ `buttonType` {#buttonType}
Sets or retrieves the button type.
#### Type {#signature}
"normal" | "accept" | "warning" | "cancel"

---
### 🏷️ `hasFocus` {#hasFocus}
Retrieves if the control has input focus.
#### Type
boolean

---
### 🏷️ `isDisabled` {#isDisabled}
Sets or retrieves if the control is disabled.
#### Type {#signature}
boolean

---
### 🏷️ `isObservable` {#isObservable}
Retrieves if the control is observable.
#### Type {#signature}
boolean

---
### 🏷️ `isVisible` {#isVisible}
Sets or retrieves the visibility of the control.
#### Type {#signature}
boolean

---
### 🏷️ `style` {#style}
Contains the style for the button control.
#### Type {#signature}
[`IButtonStyle`](#IButtonStyle)

## ▶️ Methods {#methods}

---
### 🔧 `blur` {#blur}
Blurs the focus of the control.
#### Signature
```ts
blur(): void
```

---
### 🔧 `disable` {#disable}
Disables the control.
#### Signature
```ts
disable(): this
```
#### Return value
Returns a reference to the `Button` instance to allow chaining.

---
### 🔧 `disabled` {#disabled}
Specifies the state of the control.
#### Signature
```ts
disabled(disabled?: boolean): this
```
#### Parameters
| Name       | Type    | Optional | Description                                               |
|:-----------|:--------|:---------|:----------------------------------------------------------|
| `disabled` | boolean | Yes      | Specifies if the control is disabled (default is `true`). |
#### Return value
Returns a reference to the `Button` instance to allow chaining.

---
### 🔧 `enable` {#enable}
Enables the control.
#### Signature
```ts
enable(): this
```
#### Return value
Returns a reference to the `Button` instance to allow chaining.

---
### 🔧 `focus` {#focus}
Sets the focus to the control.
#### Signature
```ts
focus(): boolean
```
#### Return value
Returns `true` if the focus is set.

---
### 🔧 `hide` {#hide}
Hides the control.
#### Signature
```ts
hide(): this
```
#### Return value
Returns a reference to the `Button` instance to allow chaining.

---
### 🔧 `indent` {#indent}
Enables control indentation. This can only be set during construction.
#### Signature
```ts
indent(size: number): this
```
#### Parameters
| Name   | Type   | Optional | Description                          |
|:-------|:-------|:---------|:-------------------------------------|
| `size` | number | No       | Specifies the indent size in pixels. |
#### Return value
Returns a reference to the `Button` instance to allow chaining.

---
### 🔧 `label` {#label}
Sets the label for the control.
:::tip
If you want to use [markdown](https://en.wikipedia.org/wiki/Markdown) in the label, activate it first using the [`markdown`](#markdown) method.
:::
#### Signature
```ts
label(label: string): this
```
#### Parameters
| Name    | Type   | Optional | Description                  |
|:--------|:-------|:---------|:-----------------------------|
| `label` | string | No       | Specifies the control label. |
#### Return value
Returns a reference to the `Button` instance to allow chaining.

---
### 🔧 `markdown` {#markdown}
Specifies if the label should support markdown formatting.
#### Signature
```ts
markdown(options?: IMarkdownOptions): this
```
#### Parameters
| Name      | Type                                                         | Optional | Description                     |
|:----------|:-------------------------------------------------------------|:---------|:--------------------------------|
| `options` | [`IMarkdownOptions`](../Markdown/index.mdx#IMarkdownOptions) | Yes      | Specifies the markdown options. |
#### Return value
Returns a reference to the `Button` instance to allow chaining.

---
### 🔧 `on` {#on}
Specifies the tap function.
#### Signature
```ts
on(fn: (button: Button) => void): this
```
#### Parameters
| Name | Type                       | Optional | Description                 |
|:-----|:---------------------------|:---------|:----------------------------|
| `fn` | (button: `Button`) => void | No       | Specifies the tap function. |
#### Return value
Returns a reference to the `Button` instance to allow chaining.

---
### 🔧 `show` {#show}
Shows the control.
#### Signature
```ts
show(): this
```
#### Return value
Returns a reference to the `Button` instance to allow chaining.

---
### 🔧 `type` {#type}
Sets the button type.
#### Signature
```ts
type(type: "normal" | "accept" | "warning" | "cancel"): this
```
#### Parameters
| Name   | Type                                          | Optional | Description                |
|:-------|:----------------------------------------------|:---------|:---------------------------|
| `type` | "normal" \| "accept" \| "warning" \| "cancel" | No       | Specifies the button type. |
#### Return value
Returns a reference to the `Button` instance to allow chaining.

---
### 🔧 `url` {#url}
Specifies an URL for the button.
#### Signature
```ts
url(link?: string, target?: "blank" | "self"): this
```
#### Parameters
| Name     | Type              | Optional | Description                       |
|:---------|:------------------|:---------|:----------------------------------|
| `link`   | string            | Yes      | Specifies the URL.                |
| `target` | "blank" \| "self" | Yes      | Specifies the target for the URL. |
#### Return value
Returns a reference to the `Button` instance to allow chaining.

---
### 🔧 `visible` {#visible}
Specifies the visibility of the control.
#### Signature
```ts
visible(visible?: boolean): this
```
#### Parameters
| Name      | Type    | Optional | Description                                              |
|:----------|:--------|:---------|:---------------------------------------------------------|
| `visible` | boolean | Yes      | Specifies if the control is visible (default is `true`). |
#### Return value
Returns a reference to the `Button` instance to allow chaining.

---
### 🔧 `width` {#width}
Sets the width of the control.
#### Signature
```ts
width(width: number | "auto" | "full"): this
```
#### Parameters
| Name    | Type                       | Optional | Description                                                                  |
|:--------|:---------------------------|:---------|:-----------------------------------------------------------------------------|
| `width` | number \| "auto" \| "full" | No       | Specifies the control width in pixels or sets the width to `auto` or `full`. |
#### Return value
Returns a reference to the `Button` instance to allow chaining.

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `IButtonStyle` {#IButtonStyle}
Describes the interface for the button styles.
#### Type declaration
```ts
interface IButtonStyle {
  /* Button appearance. */
  appearance?: IStyles;

  /* Button fused with form. */
  fused?: IStyles;

  /* Button disabled. */
  disabled?: IStyles;

  /* Button focus. */
  focus?: IStyles;

  /* Button hover. */
  hover?: IStyles;

  /* Button tapped. */
  tap?: IStyles;

  /* Normal button. */
  normal?: {
    /* Button appearance. */
    appearance?: IStyles;

    /* Button focus. */
    focus?: IStyles;

    /* Button hover. */
    hover?: IStyles;

    /* Button tapped. */
    tap?: IStyles;
  };

  /* Accept button. */
  accept?: {
    /* Button appearance. */
    appearance?: IStyles;

    /* Button focus. */
    focus?: IStyles;

    /* Button hover. */
    hover?: IStyles;

    /* Button tapped. */
    tap?: IStyles;
  };

  /* Warning button. */
  warning?: {
    /* Button appearance. */
    appearance?: IStyles;

    /* Button focus. */
    focus?: IStyles;

    /* Button hover. */
    hover?: IStyles;

    /* Button tapped. */
    tap?: IStyles;
  };

  /* Cancel button. */
  cancel?: {
    /* Button appearance. */
    appearance?: IStyles;

    /* Button focus. */
    focus?: IStyles;

    /* Button hover. */
    hover?: IStyles;

    /* Button tapped. */
    tap?: IStyles;
  };
}
```
