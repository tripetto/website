---
title: Form Text class - Blocks
sidebar_label: Text
description: The Text class defines a text input control for the Tripetto builder.
---

import Preview from '@site/src/components/preview.js';

# Text class

## 📖 Description {#description}
The `Text` class defines a text input control for the Tripetto builder. It is derived from the [`DataControl`](DataControl.mdx) class.

## 📺 Preview {#preview}
<Preview src="forms-text.png" />

---
## 🆕 `constructor` {#constructor}
Creates a new `Text` instance.
#### Signature
```ts
constructor(
  type?: "singleline" | "multiline" | "multiline-wo-crlf" | "password" | "email",
  value?: string | Binding,
  lines?: number,
  style?: ITextStyle
): Text
```
#### Parameters
| Name    | Type                                                                        | Optional | Description                                                                                                                                                                                                                                                                                                                                                                                                                                              |
|:--------|:----------------------------------------------------------------------------|:---------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `type`  | "singleline" \| "multiline" \| "multiline-wo-crlf" \| "password" \| "email" | Yes      | Specifies the input type (default is `singleline`). It can be one of the following values:<br/>- `singleline`: The control accepts single line text;<br/>- `multiline`: The control accepts multi line text (including hard returns);<br/>- `multiline-wo-crlf`: The control input can break to a next line, but hard returns (CRLFs) are not allowed;<br/>- `password`: Password input mode (input is masked);<br/>- `email`: Email address input mode. |
| `value` | string \| `Binding`                                                         | Yes      | Specifies the initially value or a data binding created with [`bind`](#bind).                                                                                                                                                                                                                                                                                                                                                                            |
| `lines` | number                                                                      | Yes      | Specifies the minimum number of lines for multi-line input.                                                                                                                                                                                                                                                                                                                                                                                              |
| `style` | [`ITextStyle`](#ITextStyle)                                                 | Yes      | Specifies the input style (when omitted the default global style will be used).                                                                                                                                                                                                                                                                                                                                                                          |
#### Return value
Returns a reference to the new `Text` instance.

## 📌 Statics {#statics}

---
### 🏷️ `style` {#statics-style}
Contains the global default style for the input control.
#### Type {#signature}
[`ITextStyle`](#ITextStyle)

---
### 🔧 `bind` {#bind}
Binds an object property to the control allowing its value to update automatically when the value of the control changes. If the control is disabled or invisible, the supplied `valueWhenInactive` is automatically set as the property value. When a control is enabled and visible, the supplied `defaultWhenActive` is set automatically if the property value is currently not defined.
#### Signature
```ts
bind<Target, P extends keyof Target>(
  target: Target,
  property: P,
  valueWhenInactive: string,
  defaultWhenActive?: string,
  modifier?: (value?: string) => string
): Binding;
```
#### Parameters
| Name                | Type                       | Optional | Description                                                                                               |
|:--------------------|:---------------------------|:---------|:----------------------------------------------------------------------------------------------------------|
| `target`            | `Target`                   | No       | Reference to the target object which contains the property.                                               |
| `property`          | `P`                        | No       | Specifies the name of the property.                                                                       |
| `valueWhenInactive` | string                     | No       | Specifies the value which is set to the property when the control is inactive (disabled or invisible).    |
| `defaultWhenActive` | string                     | Yes      | Specifies the default (initial) value that will be used when the control is active (enabled and visible). |
| `modifier`          | (value?: string) => string | Yes      | Specifies a modifier function for the data.                                                               |
#### Return value
Returns a `Binding` instance that can be used in the controls constructor to bind a value to a control.
#### Example
```ts showLineNumbers
import { Forms } from "@tripetto/builder";

const example = {
  text: ""
};

const text = new Forms.Text("singleline", Forms.Text.bind(example, "text", ""));
```

## 🗃️ Fields {#fields}

---
### 🏷️ `hasFocus` {#hasFocus}
Retrieves if the control has input focus.
#### Type
boolean

---
### 🏷️ `isDisabled` {#isDisabled}
Sets or retrieves if the control is disabled.
#### Type {#signature}
boolean

---
### 🏷️ `isLocked` {#isLocked}
Sets or retrieves if the control is locked (readonly).
#### Type {#signature}
boolean

---
### 🏷️ `isObservable` {#isObservable}
Retrieves if the control is observable.
#### Type {#signature}
boolean

---
### 🏷️ `isRequired` {#isRequired}
Sets or retrieves if the control is required.
#### Type {#signature}
boolean

---
### 🏷️ `isVisible` {#isVisible}
Sets or retrieves the visibility of the control.
#### Type {#signature}
boolean

---
### 🏷️ `style` {#style}
Contains the text control style.
#### Type {#signature}
[`ITextStyle`](#ITextStyle)

---
### 🏷️ `suggestion` {#suggestion}
Retrieves the current selected suggestion.
#### Type {#signature}
[ITextSuggestion](#ITextSuggestion)

---
### 🏷️ `type` {#type}
Retrieves the input type. It can be one of the following values:
- `singleline`: The control accepts single line text;
- `multiline`: The control accepts multi line text (including hard returns);
- `multiline-wo-crlf`: The control input can break to a next line, but hard returns (CRLFs) are not allowed;
- `password`: Password input mode (input is masked);
- `email`: Email address input mode.
#### Type {#signature}
"singleline" | "multiline" | "multiline-wo-crlf" | "password" | "email"

---
### 🏷️ `value` {#value}
Sets or retrieves the input value.
#### Type {#signature}
string

## ▶️ Methods {#methods}

---
### 🔧 `action` {#action}
Attaches an key action to the control.
:::info
This method can be used to implement variables support in the text control.
:::
#### Signature
```ts
action(
  key: string,
  fn: (
    done: (text?: string) => void,
    text: Element
  ) => ((key: string) => "capture" | "cancel" | "continue") | undefined | void
): this
```
#### Parameters
| Name  | Type                                                                                                                          | Optional | Description                               |
|:------|:------------------------------------------------------------------------------------------------------------------------------|:---------|:------------------------------------------|
| `key` | string                                                                                                                        | No       | Specifies the key to bind the action to.  |
| `fn`  | (done: (text?: string) => void, text: Element) => ((key: string) => "capture" \| "cancel" \| "continue") \| undefined \| void | No       | Specifies the action function to execute. |
#### Return value
Returns a reference to the `Text` instance to allow chaining.
#### Example
```ts showLineNumbers
import { insertVariable, Forms } from "@tripetto/builder";

// Use this code inside a block (`this` refers to the block instance)
// The `insertVariable` function will supply a list of possible variables
// when the user enters the `@` key.
new Forms.Text()
  .action("@", insertVariable(this));
```

---
### 🔧 `align` {#align}
Sets the alignment of the control.
#### Signature
```ts
align(align: "left" | "center" | "right"): this
```
#### Parameters
| Name    | Type                          | Optional | Description              |
|:--------|:------------------------------|:---------|:-------------------------|
| `align` | "left" \| "center" \| "right" | No       | Specifies the alignment. |
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `autoSelect` {#autoSelect}
Specifies if the text needs to be selected automatically.
#### Signature
```ts
autoSelect(select?: "no" | "focus" | "auto-focus"): this
```
#### Parameters
| Name     | Type                            | Optional | Description                                 |
|:---------|:--------------------------------|:---------|:--------------------------------------------|
| `select` | "no" \| "focus" \| "auto-focus" | Yes      | Specifies if the text needs to be selected. |
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `autoValidate` {#autoValidate}
Enables automatic control validation.
#### Signature
```ts
autoValidate(): this
```
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `blur` {#blur}
Blurs the focus of the control.
#### Signature
```ts
blur(): void
```

---
### 🔧 `copyToClipboard` {#copyToClipboard}
Copies the contents of the input control to the clipboard.
#### Signature
```ts
copyToClipboard(): void
```

---
### 🔧 `disable` {#disable}
Disables the control.
#### Signature
```ts
disable(): this
```
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `disabled` {#disabled}
Specifies the state of the control.
#### Signature
```ts
disabled(disabled?: boolean): this
```
#### Parameters
| Name       | Type    | Optional | Description                                               |
|:-----------|:--------|:---------|:----------------------------------------------------------|
| `disabled` | boolean | Yes      | Specifies if the control is disabled (default is `true`). |
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `enable` {#enable}
Enables the control.
#### Signature
```ts
enable(): this
```
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `enter` {#enter}
Specifies the function which is invoked when the user presses the enter key.
#### Signature
```ts
enter(fn: (text: Text) => boolean): this
```
#### Parameters
| Name | Type                      | Optional | Description                                                                                        |
|:-----|:--------------------------|:---------|:---------------------------------------------------------------------------------------------------|
| `fn` | (text: `Text`) => boolean | No       | Specifies the function to invoke. To cancel the normal behavior return `true` within the function. |
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `escape` {#escape}
Specifies the function which is invoked when the user presses the escape key.
#### Signature
```ts
escape(fn: (text: Text) => boolean): this
```
#### Parameters
| Name | Type                      | Optional | Description                                                                                        |
|:-----|:--------------------------|:---------|:---------------------------------------------------------------------------------------------------|
| `fn` | (text: `Text`) => boolean | No       | Specifies the function to invoke. To cancel the normal behavior return `true` within the function. |
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `fixedLines` {#fixedLines}
Sets a fixed number of lines for the control (auto-sizing will be disabled).
#### Signature
```ts
fixedLines(lines: number): this
```
#### Parameters
| Name    | Type   | Optional | Description                    |
|:--------|:-------|:---------|:-------------------------------|
| `lines` | number | No       | Specifies the number of lines. |
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `focus` {#focus}
Sets the focus to the control.
#### Signature
```ts
focus(): boolean
```
#### Return value
Returns `true` if the focus is set.

---
### 🔧 `hide` {#hide}
Hides the control.
#### Signature
```ts
hide(): this
```
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `indent` {#indent}
Enables control indentation. This can only be set during construction.
#### Signature
```ts
indent(size: number): this
```
#### Parameters
| Name   | Type   | Optional | Description                          |
|:-------|:-------|:---------|:-------------------------------------|
| `size` | number | No       | Specifies the indent size in pixels. |
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `inputMode` {#inputMode}
Sets the input mode for the text control.
#### Signature
```ts
inputMode(mode: "text" | "decimal" | "numeric" | "tel" | "search" | "email" | "url"): this
```
#### Parameters
| Name   | Type                                                                      | Optional | Description         |
|:-------|:--------------------------------------------------------------------------|:---------|:--------------------|
| `mode` | "text" \| "decimal" \| "numeric" \| "tel" \| "search" \| "email" \| "url" | No       | Specifies the mode. |
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `label` {#label}
Sets the label for the control.
:::tip
If you want to use [markdown](https://en.wikipedia.org/wiki/Markdown) in the label, activate it first using the [`markdown`](#markdown) method.
:::
#### Signature
```ts
label(label: string): this
```
#### Parameters
| Name    | Type   | Optional | Description                  |
|:--------|:-------|:---------|:-----------------------------|
| `label` | string | No       | Specifies the control label. |
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `lock` {#lock}
Locks the control.
#### Signature
```ts
lock(): this
```
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `locked` {#locked}
Specifies if the control is locked (readonly).
#### Signature
```ts
locked(locked?: boolean): this
```
#### Parameters
| Name     | Type    | Optional | Description                                                      |
|:---------|:--------|:---------|:-----------------------------------------------------------------|
| `locked` | boolean | Yes      | Specifies if the control is locked/readonly (default is `true`). |
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `markdown` {#markdown}
Specifies if the label should support markdown formatting.
#### Signature
```ts
markdown(options?: IMarkdownOptions): this
```
#### Parameters
| Name      | Type                                                         | Optional | Description                     |
|:----------|:-------------------------------------------------------------|:---------|:--------------------------------|
| `options` | [`IMarkdownOptions`](../Markdown/index.mdx#IMarkdownOptions) | Yes      | Specifies the markdown options. |
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `maxLength` {#maxLength}
Specifies the maximum text length.
#### Signature
```ts
maxLength(maxLength: number): this
```
#### Parameters
| Name        | Type   | Optional | Description                                                                   |
|:------------|:-------|:---------|:------------------------------------------------------------------------------|
| `maxLength` | number | No       | Specifies the maximum text length (supply `0` to disable the maximum length). |
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `name` {#name}
Sets a name for the text control.
#### Signature
```ts
name(name: string): this
```
#### Parameters
| Name   | Type   | Optional | Description         |
|:-------|:-------|:---------|:--------------------|
| `name` | string | No       | Specifies the name. |
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `on` {#on}
Specifies the function which is invoked when the input value changes.
#### Signature
```ts
on(fn: (text: Text) => void): this
```
#### Parameters
| Name | Type                   | Optional | Description                    |
|:-----|:-----------------------|:---------|:-------------------------------|
| `fn` | (text: `Text`) => void | No       | Specifies the change function. |
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `placeholder` {#placeholder}
Sets a placeholder for the text control.
#### Signature
```ts
placeholder(placeholder: string): this
```
#### Parameters
| Name          | Type   | Optional | Description                     |
|:--------------|:-------|:---------|:--------------------------------|
| `placeholder` | string | No       | Specifies the placeholder text. |
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `readonly` {#readonly}
Makes the control readonly.
#### Signature
```ts
readonly(): this
```
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `require` {#require}
Makes the control required.
#### Signature
```ts
require(): this
```
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `required` {#required}
Specifies if the control is required.
#### Signature
```ts
required(required?: boolean): this
```
#### Parameters
| Name       | Type    | Optional | Description                                               |
|:-----------|:--------|:---------|:----------------------------------------------------------|
| `required` | boolean | Yes      | Specifies if the control is required (default is `true`). |
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `sanitize` {#sanitize}
Specifies if string sanitizing should be applied.
#### Signature
```ts
sanitize(sanitize?: boolean): this
```
#### Parameters
| Name       | Type    | Optional | Description                                       |
|:-----------|:--------|:---------|:--------------------------------------------------|
| `sanitize` | boolean | Yes      | Specifies if string sanitizing should be applied. |
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `select` {#select}
Selects the text.
#### Signature
```ts
select(): this
```
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `show` {#show}
Shows the control.
#### Signature
```ts
show(): this
```
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `suggestions` {#suggestions}
Specifies text suggestions for the text control.
#### Signature
```ts
suggestions(suggestions?: (ITextSuggestion | string)[]): this
```
#### Parameters
| Name          | Type                                                | Optional | Description                        |
|:--------------|:----------------------------------------------------|:---------|:-----------------------------------|
| `suggestions` | ([`ITextSuggestion`](#ITextSuggestion) \| string)[] | Yes      | Specifies the list of suggestions. |
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `transformation` {#transformation}
Specifies the text transformation.
#### Signature
```ts
transformation(transformation: Transformations): this
```
#### Parameters
| Name             | Type                                                                                                 | Optional | Description                        |
|:-----------------|:-----------------------------------------------------------------------------------------------------|:---------|:-----------------------------------|
| `transformation` | "none" \| "capitalize" \| "capitalize-words" \| "capitalize-sentences" \| "uppercase" \| "lowercase" | No       | Specifies the transformation type. |
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `trim` {#trim}
Specifies if string trim should be applied.
#### Signature
```ts
trim(trim?: boolean): this
```
#### Parameters
| Name   | Type    | Optional | Description                                     |
|:-------|:--------|:---------|:------------------------------------------------|
| `trim` | boolean | Yes      | Specifies if string trimming should be applied. |
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `unlock` {#unlock}
Unlocks the control.
#### Signature
```ts
unlock(): this
```
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `visible` {#visible}
Specifies the visibility of the control.
#### Signature
```ts
visible(visible?: boolean): this
```
#### Parameters
| Name      | Type    | Optional | Description                                              |
|:----------|:--------|:---------|:---------------------------------------------------------|
| `visible` | boolean | Yes      | Specifies if the control is visible (default is `true`). |
#### Return value
Returns a reference to the `Text` instance to allow chaining.

---
### 🔧 `width` {#width}
Sets the width of the control.
#### Signature
```ts
width(width: number): this
```
#### Parameters
| Name    | Type   | Optional | Description                            |
|:--------|:-------|:---------|:---------------------------------------|
| `width` | number | No       | Specifies the control width in pixels. |
#### Return value
Returns a reference to the `Text` instance to allow chaining.

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `ITextStyle` {#ITextStyle}
Describes the interface for the input styles.
#### Type declaration
```ts
interface ITextStyle {
  /* Text input appearance. */
  appearance?: IStyles;

  /* Text input label. */
  label?: IStyles;

  /* Text input disabled. */
  disabled?: IStyles;

  /* Placeholder style. */
  placeholder?: IStyles;

  /* Contains the text selection styles. */
  selection?: IStyles;

  /* Text input required. */
  required?: IStyles;

  /* Text input locked. */
  locked?: IStyles;

  /* Text input focus. */
  focus?: IStyles;

  /* Text input hover. */
  hover?: IStyles;

  /* Validation passed. */
  passed?: IStyles;

  /* Validation failed. */
  failed?: IStyles;

  /* Validation awaiting. */
  awaiting?: IStyles;

  /* Text input when fused with form card. */
  fused?: {
    /* Text input appearance. */
    appearance?: IStyles;

    /* Text input label. */
    label?: IStyles;

    /* Text input required. */
    required?: IStyles;

    /* Text input locked. */
    locked?: IStyles;

    /* Text input focus. */
    focus?: IStyles;

    /* Text input hover. */
    hover?: IStyles;

    /* Validation passed. */
    passed?: IStyles;

    /* Validation failed. */
    failed?: IStyles;

    /* Validation awaiting. */
    awaiting?: IStyles;
  };
}
```

---
### 🔗 `ITextSuggestion` {#ITextSuggestion}
Describes the interface for the text suggestions.
#### Type declaration
```ts
interface ITextSuggestion {
    /* Text suggestion identifier. */
    id?: string;

    /* Text suggestion name. */
    name: string;
}
```
