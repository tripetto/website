---
title: Form Numeric class - Blocks
sidebar_label: Numeric
description: The Numeric class defines a number input control for the Tripetto builder.
---

import Preview from '@site/src/components/preview.js';

# Numeric class

## 📖 Description {#description}
The `Numeric` class defines a number input control for the Tripetto builder. It is derived from the [`DataControl`](DataControl.mdx) class.

## 📺 Preview {#preview}
<Preview src="forms-numeric.png" />

---
## 🆕 `constructor` {#constructor}
Creates a new `Numeric` instance.
#### Signature
```ts
constructor(value?: number | Binding, style?: INumericStyle): Numeric
```
#### Parameters
| Name    | Type                              | Optional | Description                                                                     |
|:--------|:----------------------------------|:---------|:--------------------------------------------------------------------------------|
| `value` | number \| `Binding`               | Yes      | Specifies the initially value or a data binding created with [`bind`](#bind).   |
| `style` | [`INumericStyle`](#INumericStyle) | Yes      | Specifies the input style (when omitted the default global style will be used). |
#### Return value
Returns a reference to the new `Numeric` instance.

## 📌 Statics {#statics}

---
### 🏷️ `style` {#statics-style}
Contains the global default style for the input control.
#### Type
[`INumericStyle`](#INumericStyle)

---
### 🔧 `bind` {#bind}
Binds an object property to the control allowing its value to update automatically when the value of the control changes. If the control is disabled or invisible, the supplied `valueWhenInactive` is automatically set as the property value. When a control is enabled and visible, the supplied `defaultWhenActive` is set automatically if the property value is currently not defined.
#### Signature
```ts
bind<Target, P extends keyof Target>(
  target: Target,
  property: P,
  valueWhenInactive: number,
  defaultWhenActive?: number,
  modifier?: (value?: number) => number
): Binding;
```
#### Parameters
| Name                | Type                       | Optional | Description                                                                                               |
|:--------------------|:---------------------------|:---------|:----------------------------------------------------------------------------------------------------------|
| `target`            | `Target`                   | No       | Reference to the target object which contains the property.                                               |
| `property`          | `P`                        | No       | Specifies the name of the property.                                                                       |
| `valueWhenInactive` | number                     | No       | Specifies the value which is set to the property when the control is inactive (disabled or invisible).    |
| `defaultWhenActive` | number                     | Yes      | Specifies the default (initial) value that will be used when the control is active (enabled and visible). |
| `modifier`          | (value?: number) => number | Yes      | Specifies a modifier function for the data.                                                               |
#### Return value
Returns a `Binding` instance that can be used in the controls constructor to bind a value to a control.
#### Example
```ts showLineNumbers
import { Forms } from "@tripetto/builder";

const example = {
  price: 0
};

const price = new Forms.Numeric(Forms.Numeric.bind(example, "price", 0))
  .prefix("$ ")
  .precision(2);
```

## 🗃️ Fields {#fields}

---
### 🏷️ `hasFocus` {#hasFocus}
Retrieves if the control has input focus.
#### Type
boolean

---
### 🏷️ `isDisabled` {#isDisabled}
Sets or retrieves if the control is disabled.
#### Type
boolean

---
### 🏷️ `isLocked` {#isLocked}
Sets or retrieves if the control is locked (readonly).
#### Type
boolean

---
### 🏷️ `isObservable` {#isObservable}
Retrieves if the control is observable.
#### Type
boolean

---
### 🏷️ `isRequired` {#isRequired}
Sets or retrieves if the control is required.
#### Type
boolean

---
### 🏷️ `isVisible` {#isVisible}
Sets or retrieves the visibility of the control.
#### Type
boolean

---
### 🏷️ `string` {#string}
Retrieves the formatted value.
#### Type
string

---
### 🏷️ `style` {#style}
Contains the numeric control style.
#### Type
[`INumericStyle`](#INumericStyle)

---
### 🏷️ `value` {#value}
Sets or retrieves the input value.
#### Type
number

## ▶️ Methods {#methods}

---
### 🔧 `align` {#align}
Sets the alignment of the control.
#### Signature
```ts
align(align: "left" | "center" | "right"): this
```
#### Parameters
| Name    | Type                          | Optional | Description              |
|:--------|:------------------------------|:---------|:-------------------------|
| `align` | "left" \| "center" \| "right" | No       | Specifies the alignment. |
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `alignLeftOnFocus` {#alignLeftOnFocus}
Specifies if the alignment should be set to the left on focus.
#### Signature
```ts
alignLeftOnFocus(alignLeftOnFocus: boolean): this
```
#### Parameters
| Name               | Type    | Optional | Description                                                |
|:-------------------|:--------|:---------|:-----------------------------------------------------------|
| `alignLeftOnFocus` | boolean | No       | Specifies if the alignment should be set to left on focus. |
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `autoSelect` {#autoSelect}
Specifies if the number needs to be selected automatically.
#### Signature
```ts
autoSelect(select?: "no" | "focus" | "auto-focus"): this
```
#### Parameters
| Name     | Type                            | Optional | Description                                   |
|:---------|:--------------------------------|:---------|:----------------------------------------------|
| `select` | "no" \| "focus" \| "auto-focus" | Yes      | Specifies if the number needs to be selected. |
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `blur` {#blur}
Blurs the focus of the control.
#### Signature
```ts
blur(): void
```

---
### 🔧 `decimalSign` {#decimalSign}
Sets the decimal sign.
#### Signature
```ts
decimalSign(decimal: string): this
```
#### Parameters
| Name      | Type   | Optional | Description                 |
|:----------|:-------|:---------|:----------------------------|
| `decimal` | string | No       | Specifies the decimal sign. |
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `digits` {#digits}
Specify the number of digits for the number.
#### Signature
```ts
digits(digits: number): this
```
#### Parameters
| Name     | Type   | Optional | Description                   |
|:---------|:-------|:---------|:------------------------------|
| `digits` | number | No       | Specifies the desired length. |
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `disable` {#disable}
Disables the control.
#### Signature
```ts
disable(): this
```
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `disabled` {#disabled}
Specifies the state of the control.
#### Signature
```ts
disabled(disabled?: boolean): this
```
#### Parameters
| Name       | Type    | Optional | Description                                               |
|:-----------|:--------|:---------|:----------------------------------------------------------|
| `disabled` | boolean | Yes      | Specifies if the control is disabled (default is `true`). |
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `enable` {#enable}
Enables the control.
#### Signature
```ts
enable(): this
```
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `enter` {#enter}
Specifies the function which is invoked when the user presses the enter key.
#### Signature
```ts
enter(fn: (number: Numeric) => boolean): this
```
#### Parameters
| Name | Type                         | Optional | Description                                                                                        |
|:-----|:-----------------------------|:---------|:---------------------------------------------------------------------------------------------------|
| `fn` | (number: Numeric) => boolean | No       | Specifies the function to invoke. To cancel the normal behavior return `true` within the function. |
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `escape` {#escape}
Specifies the function which is invoked when the user presses the escape key.
#### Signature
```ts
escape(fn: (number: Numeric) => boolean): this
```
#### Parameters
| Name | Type                           | Optional | Description                                                                                        |
|:-----|:-------------------------------|:---------|:---------------------------------------------------------------------------------------------------|
| `fn` | (number: `Numeric`) => boolean | No       | Specifies the function to invoke. To cancel the normal behavior return `true` within the function. |
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `focus` {#focus}
Sets the focus to the control.
#### Signature
```ts
focus(): boolean
```
#### Return value
Returns `true` if the focus is set.

---
### 🔧 `hide` {#hide}
Hides the control.
#### Signature
```ts
hide(): this
```
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `indent` {#indent}
Enables control indentation. This can only be set during construction.
#### Signature
```ts
indent(size: number): this
```
#### Parameters
| Name   | Type   | Optional | Description                          |
|:-------|:-------|:---------|:-------------------------------------|
| `size` | number | No       | Specifies the indent size in pixels. |
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `label` {#label}
Sets the label for the control.
:::tip
If you want to use [markdown](https://en.wikipedia.org/wiki/Markdown) in the label, activate it first using the [`markdown`](#markdown) method.
:::
#### Signature
```ts
label(label: string): this
```
#### Parameters
| Name    | Type   | Optional | Description                  |
|:--------|:-------|:---------|:-----------------------------|
| `label` | string | No       | Specifies the control label. |
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `lock` {#lock}
Locks the control.
#### Signature
```ts
lock(): this
```
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `locked` {#locked}
Specifies if the control is locked (readonly).
#### Signature
```ts
locked(locked?: boolean): this
```
#### Parameters
| Name     | Type    | Optional | Description                                                      |
|:---------|:--------|:---------|:-----------------------------------------------------------------|
| `locked` | boolean | Yes      | Specifies if the control is locked/readonly (default is `true`). |
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `markdown` {#markdown}
Specifies if the label should support markdown formatting.
#### Signature
```ts
markdown(options?: IMarkdownOptions): this
```
#### Parameters
| Name      | Type                                                         | Optional | Description                     |
|:----------|:-------------------------------------------------------------|:---------|:--------------------------------|
| `options` | [`IMarkdownOptions`](../Markdown/index.mdx#IMarkdownOptions) | Yes      | Specifies the markdown options. |
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `max` {#max}
Sets a maximum value.
#### Signature
```ts
max(max: number | undefined): this
```
#### Parameters
| Name  | Type                | Optional | Description                  |
|:------|:--------------------|:---------|:-----------------------------|
| `max` | number \| undefined | No       | Specifies the maximum value. |
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `min` {#min}
Sets a minimum value.
#### Signature
```ts
min(min: number | undefined): this
```
#### Parameters
| Name  | Type                | Optional | Description                  |
|:------|:--------------------|:---------|:-----------------------------|
| `min` | number \| undefined | No       | Specifies the minimum value. |
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `on` {#on}
Specifies the function which is invoked when the input value changes.
#### Signature
```ts
on(fn: (number: Numeric) => boolean): this
```
#### Parameters
| Name | Type                         | Optional | Description                    |
|:-----|:-----------------------------|:---------|:-------------------------------|
| `fn` | (number: Numeric) => boolean | No       | Specifies the change function. |
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `precision` {#precision}
Specifies the precision.
#### Signature
```ts
precision(precision: number | "auto"): this
```
#### Parameters
| Name        | Type             | Optional | Description                               |
|:------------|:-----------------|:---------|:------------------------------------------|
| `precision` | number \| "auto" | No       | Specifies the number of precision digits. |
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `prefix` {#prefix}
Specifies the prefix.
#### Signature
```ts
prefix(prefix: string): this;
```
#### Parameters
| Name     | Type   | Optional | Description                  |
|:---------|:-------|:---------|:-----------------------------|
| `prefix` | string | No       | Specifies the prefix string. |
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `prefixPlural` {#prefixPlural}
Specifies the plural prefix.
#### Signature
```ts
prefixPlural(prefixPlural: string | undefined): this
```
#### Parameters
| Name           | Type                | Optional | Description                  |
|:---------------|:--------------------|:---------|:-----------------------------|
| `prefixPlural` | string \| undefined | No       | Specifies the prefix string. |
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `readonly` {#readonly}
Makes the control readonly.
#### Signature
```ts
readonly(): this
```
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `require` {#require}
Makes the control required.
#### Signature
```ts
require(): this
```
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `required` {#required}
Specifies if the control is required.
#### Signature
```ts
required(required?: boolean): this
```
#### Parameters
| Name       | Type    | Optional | Description                                               |
|:-----------|:--------|:---------|:----------------------------------------------------------|
| `required` | boolean | Yes      | Specifies if the control is required (default is `true`). |
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `select` {#select}
Selects the number.
#### Signature
```ts
select(): this
```
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `show` {#show}
Shows the control.
#### Signature
```ts
show(): this
```
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `suffix` {#suffix}
Specifies the suffix.
#### Signature
```ts
suffix(suffix: string): this;
```
#### Parameters
| Name     | Type   | Optional | Description                  |
|:---------|:-------|:---------|:-----------------------------|
| `suffix` | string | No       | Specifies the suffix string. |
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `suffixPlural` {#suffixPlural}
Specifies the suffix prefix.
#### Signature
```ts
suffixPlural(suffixPlural: string | undefined): this
```
#### Parameters
| Name           | Type                | Optional | Description                  |
|:---------------|:--------------------|:---------|:-----------------------------|
| `suffixPlural` | string \| undefined | No       | Specifies the suffix string. |
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `thousands` {#thousands}
Specifies if the thousands separator should be shown and which sign should be used.
#### Signature
```ts
thousands(thousands: boolean, thousandsSign?: string): this
```
#### Parameters
| Name            | Type    | Optional | Description                                           |
|:----------------|:--------|:---------|:------------------------------------------------------|
| `thousands`     | boolean | No       | Specifies if the thousands separator should be shown. |
| `thousandsSign` | string  | Yes      | Specifies the thousands separator sign.               |
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `unlock` {#unlock}
Unlocks the control.
#### Signature
```ts
unlock(): this
```
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `visible` {#visible}
Specifies the visibility of the control.
#### Signature
```ts
visible(visible?: boolean): this
```
#### Parameters
| Name      | Type    | Optional | Description                                              |
|:----------|:--------|:---------|:---------------------------------------------------------|
| `visible` | boolean | Yes      | Specifies if the control is visible (default is `true`). |
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

---
### 🔧 `width` {#width}
Sets the width of the control.
#### Signature
```ts
width(width: number): this
```
#### Parameters
| Name    | Type   | Optional | Description                            |
|:--------|:-------|:---------|:---------------------------------------|
| `width` | number | No       | Specifies the control width in pixels. |
#### Return value
Returns a reference to the `Numeric` instance to allow chaining.

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `INumericStyle` {#INumericStyle}
Describes the interface for the input styles.
#### Type declaration
```ts
interface INumericStyle {
  /* Number input appearance. */
  appearance?: IStyles;

  /* Number input label. */
  label?: IStyles;

  /* Number input disabled. */
  disabled?: IStyles;

  /* Placeholder style. */
  placeholder?: IStyles;

  /* Contains the number selection styles. */
  selection?: IStyles;

  /* Number input required. */
  required?: IStyles;

  /* Number input locked. */
  locked?: IStyles;

  /* Number input focus. */
  focus?: IStyles;

  /* Number input hover. */
  hover?: IStyles;

  /* Validation passed. */
  passed?: IStyles;

  /* Validation failed. */
  failed?: IStyles;

  /* Validation awaiting. */
  awaiting?: IStyles;

  /* Number input when fused with form card. */
  fused?: {
    /* Number input appearance. */
    appearance?: IStyles;

    /* Number input label. */
    label?: IStyles;

    /* Number input required. */
    required?: IStyles;

    /* Number input locked. */
    locked?: IStyles;

    /* Number input focus. */
    focus?: IStyles;

    /* Number input hover. */
    hover?: IStyles;

    /* Validation passed. */
    passed?: IStyles;

    /* Validation failed. */
    failed?: IStyles;

    /* Validation awaiting. */
    awaiting?: IStyles;
  };
}
```
