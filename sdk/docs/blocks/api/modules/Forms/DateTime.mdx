---
title: Form DateTime class - Blocks
sidebar_label: DateTime
description: The DateTime class defines a date/time selector control for the Tripetto builder.
---

import Preview from '@site/src/components/preview.js';

# DateTime class

## 📖 Description {#description}
The `DateTime` class defines a date/time selector control for the Tripetto builder. It is derived from the [`DataControl`](DataControl.mdx) class.

## 📺 Preview {#preview}
<Preview src="forms-datetime.png" />

---
## 🆕 `constructor` {#constructor}
Creates a new `DateTime` instance.
#### Signature
```ts
constructor(value?: number | Binding, style?: IDateTimeStyle): DateTime
```
#### Parameters
| Name    | Type                                | Optional | Description                                                                 |
|:--------|:------------------------------------|:---------|:----------------------------------------------------------------------------|
| `value` | number \| `Binding`                 | Yes      | Specifies the initial value or a data binding created with [`bind`](#bind). |
| `style` | [`IDateTimeStyle`](#IDateTimeStyle) | Yes      | Specifies the style (when omitted the default global style will be used).   |
#### Return value
Returns a reference to the new `DateTime` instance.

## 📌 Statics {#statics}

---
### 🏷️ `style` {#statics-style}
Contains the global default style for the date/time control.
#### Type
[`IDateTimeStyle`](#IDateTimeStyle)

---
### 🔧 `bind` {#bind}
Binds an object property to the control allowing its value to update automatically when the value of the control changes. If the control is disabled or invisible, the supplied `valueWhenInactive` is automatically set as the property value. When a control is enabled and visible, the supplied `defaultWhenActive` is set automatically if the property value is currently not defined.
#### Signature
```ts
bind<Target, P extends keyof Target>(
  target: Target,
  property: P,
  valueWhenInactive: number,
  defaultWhenActive?: number,
  modifier?: (value?: number) => number
): Binding;
```
#### Parameters
| Name                | Type                       | Optional | Description                                                                                               |
|:--------------------|:---------------------------|:---------|:----------------------------------------------------------------------------------------------------------|
| `target`            | `Target`                   | No       | Reference to the target object which contains the property.                                               |
| `property`          | `P`                        | No       | Specifies the name of the property.                                                                       |
| `valueWhenInactive` | number                     | No       | Specifies the value which is set to the property when the control is inactive (disabled or invisible).    |
| `defaultWhenActive` | number                     | Yes      | Specifies the default (initial) value that will be used when the control is active (enabled and visible). |
| `modifier`          | (value?: number) => number | Yes      | Specifies a modifier function for the data.                                                               |
#### Return value
Returns a `Binding` instance that can be used in the controls constructor to bind a value to a control.
#### Example
```ts showLineNumbers
import { Forms } from "@tripetto/builder";

const example = {
  date: 0
};

const dateTime = new Forms.DateTime(Forms.DateTime.bind(example, "date", 0));
```

## 🗃️ Fields {#fields}

---
### 🏷️ `dayOfMonth` {#dayOfMonth}
Sets or retrieves the day of the month.
#### Type
1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 | 29 | 30 | 31

---
### 🏷️ `hasFocus` {#hasFocus}
Retrieves if the control has input focus.
#### Type
boolean

---
### 🏷️ `hasValue` {#hasValue}
Retrieves if the control has a value.
#### Type
boolean

---
### 🏷️ `hour` {#hour}
Sets or retrieves the hour.
#### Type
number

---
### 🏷️ `isDisabled` {#isDisabled}
Sets or retrieves if the control is disabled.
#### Type
boolean

---
### 🏷️ `isLocked` {#isLocked}
Sets or retrieves if the control is locked (readonly).
#### Type
boolean

---
### 🏷️ `isObservable` {#isObservable}
Retrieves if the control is observable.
#### Type
boolean

---
### 🏷️ `isRequired` {#isRequired}
Sets or retrieves if the control is required.
#### Type
boolean

---
### 🏷️ `isVisible` {#isVisible}
Sets or retrieves the visibility of the control.
#### Type
boolean

---
### 🏷️ `minute` {#minute}
Sets or retrieves the minute.
#### Type
number

---
### 🏷️ `month` {#month}
Sets or retrieves the month.
#### Type
0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11

---
### 🏷️ `second` {#second}
Sets or retrieves the second.
#### Type
number

---
### 🏷️ `style` {#style}
Contains the style for the date/time control.
#### Type
[`IDateTimeStyle`](#IDateTimeStyle)

---
### 🏷️ `value` {#value}
Sets or retrieves the selected value.
#### Type
number

---
### 🏷️ `year` {#year}
Sets or retrieves the year.
#### Type
number

## ▶️ Methods {#methods}

---
### 🔧 `blur` {#blur}
Blurs the focus of the control.
#### Signature
```ts
blur(): void
```

---
### 🔧 `disable` {#disable}
Disables the control.
#### Signature
```ts
disable(): this
```
#### Return value
Returns a reference to the `DateTime` instance to allow chaining.

---
### 🔧 `disabled` {#disabled}
Specifies the state of the control.
#### Signature
```ts
disabled(disabled?: boolean): this
```
#### Parameters
| Name       | Type    | Optional | Description                                               |
|:-----------|:--------|:---------|:----------------------------------------------------------|
| `disabled` | boolean | Yes      | Specifies if the control is disabled (default is `true`). |
#### Return value
Returns a reference to the `DateTime` instance to allow chaining.

---
### 🔧 `enable` {#enable}
Enables the control.
#### Signature
```ts
enable(): this
```
#### Return value
Returns a reference to the `DateTime` instance to allow chaining.

---
### 🔧 `features` {#features}
Specifies the date control features which should be enabled.
#### Signature
```ts
features(features: DateTimeFeatures): this
```
#### Parameters
| Name   | Type                                        | Optional | Description                                 |
|:-------|:--------------------------------------------|:---------|:--------------------------------------------|
| `features` | [`DateTimeFeatures`](#DateTimeFeatures) | No       | Specifies the features which should be enabled. |
#### Return value
Returns a reference to the `DateTime` instance to allow chaining.

---
### 🔧 `focus` {#focus}
Sets the focus to the control.
#### Signature
```ts
focus(): boolean
```
#### Return value
Returns `true` if the focus is set.

---
### 🔧 `hide` {#hide}
Hides the control.
#### Signature
```ts
hide(): this
```
#### Return value
Returns a reference to the `DateTime` instance to allow chaining.

---
### 🔧 `indent` {#indent}
Enables control indentation. This can only be set during construction.
#### Signature
```ts
indent(size: number): this
```
#### Parameters
| Name   | Type   | Optional | Description                          |
|:-------|:-------|:---------|:-------------------------------------|
| `size` | number | No       | Specifies the indent size in pixels. |
#### Return value
Returns a reference to the `DateTime` instance to allow chaining.

---
### 🔧 `interval` {#interval}
Specifies the minute/second interval.
#### Signature
```ts
interval(interval: number): this
```
#### Parameters
| Name       | Type   | Optional | Description             |
|:-----------|:-------|:---------|:------------------------|
| `interval` | number | No       | Specifies the interval. |
#### Return value
Returns a reference to the `DateTime` instance to allow chaining.

---
### 🔧 `label` {#label}
Sets the label for the control.
:::tip
If you want to use [markdown](https://en.wikipedia.org/wiki/Markdown) in the label, activate it first using the [`markdown`](#markdown) method.
:::
#### Signature
```ts
label(label: string): this
```
#### Parameters
| Name    | Type   | Optional | Description                  |
|:--------|:-------|:---------|:-----------------------------|
| `label` | string | No       | Specifies the control label. |
#### Return value
Returns a reference to the `DateTime` instance to allow chaining.

---
### 🔧 `lock` {#lock}
Locks the control.
#### Signature
```ts
lock(): this
```
#### Return value
Returns a reference to the `DateTime` instance to allow chaining.

---
### 🔧 `locked` {#locked}
Specifies if the control is locked (readonly).
#### Signature
```ts
locked(locked?: boolean): this
```
#### Parameters
| Name     | Type    | Optional | Description                                                      |
|:---------|:--------|:---------|:-----------------------------------------------------------------|
| `locked` | boolean | Yes      | Specifies if the control is locked/readonly (default is `true`). |
#### Return value
Returns a reference to the `DateTime` instance to allow chaining.

---
### 🔧 `markdown` {#markdown}
Specifies if the label should support markdown formatting.
#### Signature
```ts
markdown(options?: IMarkdownOptions): this
```
#### Parameters
| Name      | Type                                                         | Optional | Description                     |
|:----------|:-------------------------------------------------------------|:---------|:--------------------------------|
| `options` | [`IMarkdownOptions`](../Markdown/index.mdx#IMarkdownOptions) | Yes      | Specifies the markdown options. |
#### Return value
Returns a reference to the `DateTime` instance to allow chaining.

---
### 🔧 `on` {#on}
Specifies the function which is invoked when the date is changed.
#### Signature
```ts
on(fn: (date: DateTime) => void): this
```
#### Parameters
| Name | Type                       | Optional | Description                    |
|:-----|:---------------------------|:---------|:-------------------------------|
| `fn` | (date: `DateTime`) => void | No       | Specifies the change function. |
#### Return value
Returns a reference to the `DateTime` instance to allow chaining.

---
### 🔧 `placeholder` {#placeholder}
Sets a placeholder for the control.
#### Signature
```ts
placeholder(placeholder: string): this
```
#### Parameters
| Name          | Type   | Optional | Description                       |
|:--------------|:-------|:---------|:----------------------------------|
| `placeholder` | string | No       | Specifies the placeholder string. |
#### Return value
Returns a reference to the `DateTime` instance to allow chaining.

---
### 🔧 `readonly` {#readonly}
Makes the control readonly.
#### Signature
```ts
readonly(): this
```
#### Return value
Returns a reference to the `DateTime` instance to allow chaining.

---
### 🔧 `require` {#require}
Makes the control required.
#### Signature
```ts
require(): this
```
#### Return value
Returns a reference to the `DateTime` instance to allow chaining.

---
### 🔧 `required` {#required}
Specifies if the control is required.
#### Signature
```ts
required(required?: boolean): this
```
#### Parameters
| Name       | Type    | Optional | Description                                               |
|:-----------|:--------|:---------|:----------------------------------------------------------|
| `required` | boolean | Yes      | Specifies if the control is required (default is `true`). |
#### Return value
Returns a reference to the `DateTime` instance to allow chaining.

---
### 🔧 `show` {#show}
Shows the control.
#### Signature
```ts
show(): this
```
#### Return value
Returns a reference to the `DateTime` instance to allow chaining.

---
### 🔧 `showDayOfMonthSelector` {#showDayOfMonthSelector}
Shows the day of month selector.
#### Signature
```ts
showDayOfMonthSelector(): void
```

---
### 🔧 `showHourSelector` {#showHourSelector}
Shows the hour selector.
#### Signature
```ts
showHourSelector(): void
```

---
### 🔧 `showMinuteSelector` {#showMinuteSelector}
Shows the minute selector.
#### Signature
```ts
showMinuteSelector(): void
```

---
### 🔧 `showMonthSelector` {#showMonthSelector}
Shows the month selector.
#### Signature
```ts
showMonthSelector(): void
```

---
### 🔧 `showOptions` {#showOptions}
Shows the options menu.
#### Signature
```ts
showOptions(): void
```

---
### 🔧 `showSecondSelector` {#showSecondSelector}
Shows the seconds selector.
#### Signature
```ts
showSecondSelector(): void
```

---
### 🔧 `showWeekdaySelector` {#showWeekdaySelector}
Shows the weekday selector.
#### Signature
```ts
showWeekdaySelector(): void
```

---
### 🔧 `showYearSelector` {#showYearSelector}
Shows the year selector.
#### Signature
```ts
showYearSelector(): void
```

---
### 🔧 `unlock` {#unlock}
Unlocks the control.
#### Signature
```ts
unlock(): this
```
#### Return value
Returns a reference to the `DateTime` instance to allow chaining.

---
### 🔧 `visible` {#visible}
Specifies the visibility of the control.
#### Signature
```ts
visible(visible?: boolean): this
```
#### Parameters
| Name      | Type    | Optional | Description                                              |
|:----------|:--------|:---------|:---------------------------------------------------------|
| `visible` | boolean | Yes      | Specifies if the control is visible (default is `true`). |
#### Return value
Returns a reference to the `DateTime` instance to allow chaining.

---
### 🔧 `width` {#width}
Sets the width of the control.
#### Signature
```ts
width(width: number | "auto" | "full"): this
```
#### Parameters
| Name    | Type                       | Optional | Description                                                                  |
|:--------|:---------------------------|:---------|:-----------------------------------------------------------------------------|
| `width` | number \| "auto" \| "full" | No       | Specifies the control width in pixels or sets the width to `auto` or `full`. |
#### Return value
Returns a reference to the `DateTime` instance to allow chaining.

---
### 🔧 `years` {#years}
Specifies the years range.
#### Signature
```ts
years(from: number, to: number): this
```
#### Parameters
| Name   | Type   | Optional | Description              |
|:-------|:-------|:---------|:-------------------------|
| `from` | number | No       | Specifies the from year. |
| `to`   | number | No       | Specifies the to year.   |
#### Return value
Returns a reference to the `DateTime` instance to allow chaining.

---
### 🔧 `zone` {#zone}
Specifies the date zone.
#### Signature
```ts
zone(zone: "local" | "UTC"): this
```
#### Parameters
| Name   | Type             | Optional | Description                                 |
|:-------|:-----------------|:---------|:--------------------------------------------|
| `zone` | "local" \| "UTC" | No       | Specifies the date zone (`local` or `UTC`). |
#### Return value
Returns a reference to the `DateTime` instance to allow chaining.

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `IDateTimeStyle` {#IDateTimeStyle}
Describes the interface for the date/time styles.
#### Type declaration
```ts
interface IDateTimeStyle {
  /* Specifies the control height. */
  height: number;

  /* Date/time appearance. */
  appearance?: IStyles;

  /* Date appearance. */
  date?: IStyles;

  /* Time appearance. */
  time?: {
    /* Time appearance. */
    appearance?: IStyles;

    /* Hour-minute digit separator. */
    hourMinuteSeparator?: IStyles;

    /* Minute-second digit separator. */
    minuteSecondSeparator?: IStyles;
  };

  /* Date plane appearance. */
  plane?: {
    /* Idle appearance. */
    appearance: IStyles;

    /* Hover appearance. */
    hover?: IStyles;

    /* Selected appearance. */
    selected?: IStyles;

    /* Focus style. */
    focus?: IStyles;

    /* Selector menu style. */
    selector?: IMenuStyle;
  };

  /* Spacing in pixels between the date and time. */
  spacing?: number;

  /* Placeholder style. */
  placeholder?: {
    /* Base appearance. */
    appearance: IStyles;

    /* Text appearance. */
    text: IStyles;
  };

  /* Date label. */
  label?: IStyles;

  /* Date disabled. */
  disabled?: IStyles;

  /* Date required. */
  required?: IStyles;

  /* Date locked. */
  locked?: IStyles;

  /* Date focus. */
  focus?: IStyles;

  /* Date hover. */
  hover?: IStyles;

  /* Validation passed. */
  passed?: IStyles;

  /* Validation failed. */
  failed?: IStyles;

  /* Validation awaiting. */
  awaiting?: IStyles;

  /* Date appearance when fused with the form card. */
  fused?: {
    /* Date appearance. */
    appearance?: IStyles;

    /* Date label. */
    label?: IStyles;

    /* Date required. */
    required?: IStyles;

    /* Date locked. */
    locked?: IStyles;

    /* Date focus. */
    focus?: IStyles;

    /* Date hover. */
    hover?: IStyles;

    /* Validation passed. */
    passed?: IStyles;

    /* Validation failed. */
    failed?: IStyles;

    /* Validation awaiting. */
    awaiting?: IStyles;
  };

  /* Contains text labels. */
  labels: {
    /* Enable date. */
    enable: string;

    /* Disable date. */
    disable: string;

    /* Reset date. */
    reset?: string;
  };
}
```

## 📇 Enums {#enums}

---
### 📇 `DateTimeFeatures` {#DateTimeFeatures}
Describes the enum for the date/time features.
#### Type declaration
```ts
enum DateTimeFeatures {
  /* Specifies if a date can be set. */
  Date = 1,

  /* Specifies if a time can be set. */
  Time = 2,

  /* Specifies if the minutes can be set. */
  TimeHoursOnly = 4,

  /* Specifies if the seconds can be set. */
  TimeHoursAndMinutesOnly = 8,

  /* Specifies if the control has a reset option. */
  NoResetOption = 16,

  /* Specifies if the control shows the weekday. */
  Weekday = 32,

  /* Specifies the default features. */
  Default = 35
}
```
