---
title: L10n module - Blocks
sidebar_label: L10n
description: The L10n module contains functions for localization (builder package).
---

# L10n module

## 📖 Description {#description}
The `L10n` module contains functions for localization. Tripetto uses the GNU [gettext](https://en.wikipedia.org/wiki/Gettext) system.

## 👩‍💻 Example {#example}
```ts showLineNumbers
import { L10n } from "@tripetto/builder";

// Translate message
L10n.gettext("Lorem ipsum dolor sit amet");

// Translate message with shorthand
L10n._("Lorem ipsum dolor sit amet");

// Translate message with arguments
L10n.gettext("Hello %1", "there");
// Shorthand
L10n._("Hello %1", "there");

// Translate plural message
L10n.ngettext("1 user", "%1 users", 2);
// Shorthand
L10n._n("1 user", "%1 users", 2);
```

## 🗃️ Properties {#properties}

---
### 🏷️ `current` {#current}
Retrieves the current translation domain (this is the ISO 639-1 language code of the domain).
#### Type
string

---
### 🏷️ `domains` {#domains}
Retrieves a list of all available translation domains.
#### Type
string[]

---
### 🏷️ `locale` {#locale}
Retrieves a reference to the [`Locales`](Locales.mdx) instance that holds the locale information.
#### Type
[`Locales`](Locales.mdx)

## ▶️ Functions {#functions}

---
### 🔧 `_` {#_}
Translates a message (short for [`gettext`](#gettext)).
#### Signature
```ts
_(message: string, ...arguments: string[]): string
```
#### Parameters
| Name        | Type     | Optional | Description                                                                                                                                                              |
|:------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `message`   | string   | No       | Specifies the message to translate.                                                                                                                                      |
| `arguments` | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The first argument is referenced with `%1`. |
#### Return value
Returns the translated message.

---
### 🔧 `_n` {#_n}
Translates a plural message (short for [`ngettext`](#ngettext)).
#### Signature
```ts
_n(message: string, messagePlural: string, count: number, ...arguments: string[]): string
```
#### Parameters
| Name            | Type     | Optional | Description                                                                                                                                                                                            |
|:----------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `message`       | string   | No       | Specifies the message to translate.                                                                                                                                                                    |
| `messagePlural` | string   | No       | Specifies the plural message to translate.                                                                                                                                                             |
| `count`         | number   | No       | Specifies the count for the plural (can be reference in the message with `%1`).                                                                                                                        |
| `arguments`     | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The `count` value is automatically included as the first argument (`%1`). |
#### Return value
Returns the translated message.

---
### 🔧 `dgettext` {#dgettext}
Translates a message using the specified translation domain.
#### Signature
```ts
dgettext(domain: string, message: string, ...arguments: string[]): string
```
#### Parameters
| Name        | Type     | Optional | Description                                                                                                                                                              |
|:------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `domain`    | string   | No       | Specifies the translation domain to use.                                                                                                                                 |
| `message`   | string   | No       | Specifies the message to translate.                                                                                                                                      |
| `arguments` | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The first argument is referenced with `%1`. |
#### Return value
Returns the translated message.

---
### 🔧 `dngettext` {#dngettext}
Translates a plural message using the specified translation domain.
#### Signature
```ts
dngettext(
  domain: string,
  message: string,
  messagePlural: string,
  count: number,
  ...arguments: string[]
): string
```
#### Parameters
| Name            | Type     | Optional | Description                                                                                                                                                                                            |
|:----------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `domain`        | string   | No       | Specifies the translation domain to use.                                                                                                                                                               |
| `message`       | string   | No       | Specifies the message to translate.                                                                                                                                                                    |
| `messagePlural` | string   | No       | Specifies the plural message to translate.                                                                                                                                                             |
| `count`         | number   | No       | Specifies the count for the plural (can be reference in the message with `%1`).                                                                                                                        |
| `arguments`     | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The `count` value is automatically included as the first argument (`%1`). |
#### Return value
Returns the translated message.

---
### 🔧 `dnpgettext` {#dnpgettext}
Translates a plural message with the specified context using the specified translation domain.
#### Signature
```ts
dnpgettext(
  domain: string,
  context: string,
  message: string,
  messagePlural: string,
  count: number,
  ...arguments: string[]
): string
```
#### Parameters
| Name            | Type     | Optional | Description                                                                                                                                                                                            |
|:----------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `domain`        | string   | No       | Specifies the translation domain to use.                                                                                                                                                               |
| `context`       | string   | No       | Specifies the translation context.                                                                                                                                                                     |
| `message`       | string   | No       | Specifies the message to translate.                                                                                                                                                                    |
| `messagePlural` | string   | No       | Specifies the plural message to translate.                                                                                                                                                             |
| `count`         | number   | No       | Specifies the count for the plural (can be reference in the message with `%1`).                                                                                                                        |
| `arguments`     | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The `count` value is automatically included as the first argument (`%1`). |
#### Return value
Returns the translated message.

---
### 🔧 `dpgettext` {#dpgettext}
Translates a message with the specified context using the specified translation domain.
#### Signature
```ts
dpgettext(domain: string, context: string, message: string, ...arguments: string[]): string
```
#### Parameters
| Name        | Type     | Optional | Description                                                                                                                                                              |
|:------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `domain`    | string   | No       | Specifies the translation domain to use.                                                                                                                                 |
| `context`   | string   | No       | Specifies the translation context.                                                                                                                                       |
| `message`   | string   | No       | Specifies the message to translate.                                                                                                                                      |
| `arguments` | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The first argument is referenced with `%1`. |
#### Return value
Returns the translated message.

---
### 🔧 `gettext` {#gettext}
Translates a message.
#### Signature
```ts
gettext(message: string, ...arguments: string[]): string
```
#### Parameters
| Name        | Type     | Optional | Description                                                                                                                                                              |
|:------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `message`   | string   | No       | Specifies the message to translate.                                                                                                                                      |
| `arguments` | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The first argument is referenced with `%1`. |
#### Return value
Returns the translated message.

---
### 🔧 `ngettext` {#ngettext}
Translates a plural message.
#### Signature
```ts
ngettext(
  message: string,
  messagePlural: string,
  count: number,
  ...arguments: string[]
): string
```
#### Parameters
| Name            | Type     | Optional | Description                                                                                                                                                                                            |
|:----------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `message`       | string   | No       | Specifies the message to translate.                                                                                                                                                                    |
| `messagePlural` | string   | No       | Specifies the plural message to translate.                                                                                                                                                             |
| `count`         | number   | No       | Specifies the count for the plural (can be reference in the message with `%1`).                                                                                                                        |
| `arguments`     | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The `count` value is automatically included as the first argument (`%1`). |
#### Return value
Returns the translated message.

---
### 🔧 `npgettext` {#npgettext}
Translates a plural message with the specified context.
#### Signature
```ts
npgettext(
  context: string,
  message: string,
  messagePlural: string,
  count: number,
  ...arguments: string[]
): string
```
#### Parameters
| Name            | Type     | Optional | Description                                                                                                                                                                                            |
|:----------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `context`       | string   | No       | Specifies the translation context.                                                                                                                                                                     |
| `message`       | string   | No       | Specifies the message to translate.                                                                                                                                                                    |
| `messagePlural` | string   | No       | Specifies the plural message to translate.                                                                                                                                                             |
| `count`         | number   | No       | Specifies the count for the plural (can be reference in the message with `%1`).                                                                                                                        |
| `arguments`     | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The `count` value is automatically included as the first argument (`%1`). |
#### Return value
Returns the translated message.

---
### 🔧 `pgettext` {#pgettext}
Translates a message with the specified context.
#### Signature
```ts
pgettext(context: string, message: string, ...arguments: string[]): string
```
#### Parameters
| Name        | Type     | Optional | Description                                                                                                                                                              |
|:------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `context`   | string   | No       | Specifies the translation context.                                                                                                                                       |
| `message`   | string   | No       | Specifies the message to translate.                                                                                                                                      |
| `arguments` | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The first argument is referenced with `%1`. |
#### Return value
Returns the translated message.

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `ILocale` {#ILocale}
Describes the interface for the [`locale`](../../../../builder/api/interfaces/IBuilderProperties.mdx#locale) object. Locale information is stored in a JSON file per locale and are stored in the [locales](https://unpkg.com/browse/tripetto/locales/) folder of the Tripetto Builder package.
#### Type declaration
```ts
interface ILocale {
  locale: string;
  domain: string;
  direction: "ltr" | "rtl";
  countryCode: string;
  country: string;
  countryNative: string;
  language: string;
  languageNative: string;
  translations: {
    months: {
      formatted: {
        abbreviated: string[];
        narrow: string[];
        wide: string[];
      };
      nominative: {
        abbreviated: string[];
        narrow: string[];
        wide: string[];
      };
    };
    days: {
      formatted: {
        abbreviated: string[];
        narrow: string[];
        short: string[];
        wide: string[];
      };
      nominative: {
        abbreviated: string[];
        narrow: string[];
        short: string[];
        wide: string[];
      };
    };
    time: {
      AM: string;
      PM: string;
    };
  };
  formats: {
    date: {
      full: string;
      long: string;
      medium: string;
      short: string;
    };
    time: {
      full: string;
      long: string;
      medium: string;
      short: string;
    };
    dateTime: {
      full: string;
      long: string;
      medium: string;
      short: string;
    };
    numbers: {
      decimals: string;
      grouping: string;
      minus: string;
    };
  };
}
```

## 📜 Types {#types}

---
### 📃 `TTranslation` {#TTranslation}
Defines a translation.
#### Type
```ts
{
  "": {
    language?: string;
    "plural-forms"?: string;
    "plural-family"?: string;
  };
} & {
  [id: string]: [null | string, ...string[]] | [null | string, [string]];
}
```
