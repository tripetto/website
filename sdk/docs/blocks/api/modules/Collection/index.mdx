---
title: Collection module - Blocks
sidebar_label: Collection
toc_max_heading_level: 4
description: The Collection module contains classes and controls to define item collections in the builder.
---

import Preview from '@site/src/components/preview.js';
import InterfaceDeclaration from '@site/src/components/interface.js';

# Collection module

## 📖 Description {#description}
The `Collection` module contains classes and controls to define item collections in the builder. Item collections are used to enable the user to create lists with items of a certain type. For example, options in a dropdown list.

:::tip
See the [Collections guide](../../../custom/guides/collections.mdx) for more information.
:::

## 💎 Classes {#classes}
- [`Card`](Card.mdx)
- [`Item`](Item.mdx)
- [`Provider`](Provider.mdx)

## 📺 Preview {#preview}
<Preview src="editor-collection.png" />

## ▶️ Functions {#functions}

---
### 🔧 `find` {#find}
Finds the collection that serves the list of possible values for a certain slot. For example, let's say you have a dropdown block with a collection that contains all the possible dropdown options and a single slot that holds the selected option for the dropdown. This function can retrieve a reference to the collection [`Provider`](Provider.mdx) for that slot.
:::info
To let this work, you need to decorate the collection property in a [`NodeBlock`](../../classes/NodeBlock.mdx) with the [`@supplies`](../../decorators/supplies.mdx) decorator.
:::
##### Signature
```ts
find(slot: ISlotIdentifier): ICollectionSupply | undefined
```
##### Parameters
| Name   | Type                                                      | Optional | Description                                    |
|:-------|:----------------------------------------------------------|:---------|:-----------------------------------------------|
| `slot` | [`ISlotIdentifier`](../../interfaces/ISlotIdentifier.mdx) | No       | Specifies the slot to find the collection for. |
##### Return value
Returns a [`ICollectionSupply`](#ICollectionSupply) object or `undefined` if the collection was not found.

---
### 🔧 `get` {#get}
Retrieves the collection with the specified property name from a [`NodeBlock`](../../classes/NodeBlock.mdx).
##### Signature
```ts
get(block: NodeBlock, name: string): Provider | undefined
```
##### Parameters
| Name    | Type                                       | Optional | Description                                         |
|:--------|:-------------------------------------------|:---------|:----------------------------------------------------|
| `block` | [`NodeBlock`](../../classes/NodeBlock.mdx) | No       | Reference to the node block to search in.           |
| `name`  | string                                     | No       | The name of the property that holds the collection. |
##### Return value
Returns a reference to the [`Provider`](Provider.mdx) instance or `undefined` if the collection was not found.

---
### 🔧 `of` {#of}
Creates a new collection of the supplied item type class. The type class needs to be derived from the [`Item`](Item.mdx) class. This function returns a reference to the [`Provider`](Provider.mdx) that provides access to the collection in the builder.
:::info
Collections are automatically saved to and retrieved from the [form definition](../../../../builder/api/interfaces/IDefinition.mdx) (as if the collection field was decorated with the [`@definition`](../../decorators/definition.mdx) decorator).
:::
##### Signature
```ts
of(constructor: typeof Item, reference: NodeBlock | ConditionBlock | Item): Provider
```
##### Parameters
| Name          | Type                                                                                                                     | Optional | Description                                                                                                                                              |
|:--------------|:-------------------------------------------------------------------------------------------------------------------------|:---------|:---------------------------------------------------------------------------------------------------------------------------------------------------------|
| `constructor` | [`typeof Item`](Item.mdx)                                                                                                | No       | Specifies the type of items for the collection.                                                                                                          |
| `reference`   | [`NodeBlock`](../../classes/NodeBlock.mdx) \| [`ConditionBlock`](../../classes/ConditionBlock.mdx) \| [`Item`](Item.mdx) | No       | A reference to the parent instance that holds the collection. This reference is exposed to the [`ref`](Item.mdx#ref) property of the [`Item`](Item.mdx). |
##### Return value
Returns a reference to the [`Provider`](Provider.mdx) instance.
##### Example
```ts showLineNumbers
import { _, tripetto, definition, name, Collection, NodeBlock } from "@tripetto/builder";

//highlight-start
// Let's define an item type that holds an option for a dropdown list
class DropdownOption extends Collection.Item<Dropdown> {
  @definition
  @name
  name = "";
}
//highlight-end

@tripetto({
  type: "node",
  identifier: "dropdown",
  label: "Dropdown",
  icon: "data:image/svg+xml;base64,PHN2ZyAvPg=="
})
class Dropdown extends NodeBlock {
  //highlight-start
  // Create a collection of dropdown options
  options = Collection.of<DropdownOption, Dropdown>(DropdownOption, this);
  //highlight-end
}
```

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `IProperties` {#IProperties}
Describes the interface for declaring a collection card using the [`collection`](../../classes/EditorOrchestrator.mdx#collection) method of the [`EditorOrchestrator`](../../classes/EditorOrchestrator.mdx) class for defining editor panels.

##### Type declaration
<InterfaceDeclaration prefix="#IProperties-" src={`interface IProperties {
  collection: Provider;
  title: string;
  banner?: string;
  sorting?: "ascending" | "descending" | "manual";
  placeholder?: string;
  emptyMessage?: string | {
    message: string;
    height: number;
  };
  icon?: boolean | string;
  editable?: boolean;
  autoOpen?: boolean;
  allowAutoSorting?: boolean;
  allowCleanup?: boolean | string;
  allowVariables?: boolean;
  allowFormatting?: boolean;
  allowImport?: boolean;
  allowExport?: boolean;
  allowDedupe?: boolean;
  showAliases?: boolean | ((item: Item) => string | undefined);
  showScores?: boolean | ((item: Item) => number | string | undefined);
  markdown?: MarkdownFeatures;
  menu?: MenuOption[] | (() => MenuOption[]);
  width?: number;
  indicator?: (item: Item) => string | undefined;
  onAdd?: (item: Item) => void;
  onRename?: (item: Item) => void;
  onReposition?: (item: Item) => void;
  onResize?: (collection: Provider) => void;
  onDelete?: (item: Item) => void;
  onOpen?: (item: Item) => void;
  onClose?: (item: Item) => void;
}`} symbols={{
  "Provider": "/blocks/api/modules/Collection/Provider/",
  "Item": "/blocks/api/modules/Collection/Item/",
  "MarkdownFeatures": "/blocks/api/modules/Markdown/#MarkdownFeatures/",
  "MenuOption": "/blocks/api/modules/Components/MenuOption/",
}} />

---
#### 🏷️ `allowAutoSorting` {#IProperties-allowAutoSorting}
Specifies if the option to automatically sort the list is shown (enabled by default).
:::info
This only applies to collection cards with [`sorting`](#IProperties-sorting) set to `manual`.
:::
##### Type
boolean

---
#### 🏷️ `allowCleanup` {#IProperties-allowCleanup}
Specifies if the option to delete all unnamed items is shown (enabled by default). When a string is supplied the option is enabled and that string is shown as menu option to execute the cleanup.
##### Type
boolean | string

---
#### 🏷️ `allowDedupe` {#IProperties-allowDedupe}
Specifies if the deduplication function is allowed. Deduplication deletes items with the same name.
##### Type
boolean

---
#### 🏷️ `allowExport` {#IProperties-allowExport}
Specifies if the bulk item exporter is allowed.
##### Type
boolean

---
#### 🏷️ `allowFormatting` {#IProperties-allowFormatting}
Specifies if markdown formatting is rendered in the list.
##### Type
boolean

---
#### 🏷️ `allowImport` {#IProperties-allowImport}
Specifies if the bulk item importer is allowed.
##### Type
boolean

---
#### 🏷️ `allowVariables` {#IProperties-allowVariables}
Specifies if the use of variables (recalling values) is supported.
##### Type
boolean

---
#### 🏷️ `autoOpen` {#IProperties-autoOpen}
Specifies whether to open the editor panel of new items when they are added.
##### Type
boolean

---
#### 🏷️ `banner` {#IProperties-banner}
Specifies a banner for collection card. The banner is shown above the card.
##### Type
string

---
#### 🏷️ `collection` {#IProperties-collection}
Reference to the collection [`Provider`](Provider.mdx) instance.
##### Type
[`Provider`](Provider.mdx)

---
#### 🏷️ `editable` {#IProperties-editable}
Specifies if the name of the items can be edited in the list by double tapping on the name.
##### Type
boolean

---
#### 🏷️ `emptyMessage` {#IProperties-emptyMessage}
Specifies the message that is shown when the collection is empty.
##### Type
```ts
string | {
  /* Specifies the message to show. */
  message: string;

  /* Specifies the height of the collection card in pixels when the message is shown. */
  height: number;
}
```

---
#### 🏷️ `icon` {#IProperties-icon}
Specifies if an icon is shown for each item (a default icon can be supplied). The [`Item`](Item.mdx) property that is decorated with the [`@icon`](../../decorators/icon.mdx) decorator is used as icon for the item.
##### Type
boolean | string | [`SVGImage`](../SVGImage.mdx)

---
#### 🏷️ `indicator` {#IProperties-indicator}
Specifies an indicator for each [`Item`](Item.mdx) that is shown in the collection card.
##### Type
(item: [`Item`](Item.mdx)) => string | undefined

---
#### 🏷️ `markdown` {#IProperties-markdown}
Specifies the markdown features that are enabled when [`allowFormatting`](#IProperties-allowFormatting) is set to `true`.
##### Type
[`MarkdownFeatures`](../Markdown/index.mdx#MarkdownFeatures)

---
#### 🏷️ `menu` {#IProperties-menu}
Specifies a menu that is shown when the user wants to add something to the collection.
##### Type
[`MenuOption[]`](../Components/MenuOption.mdx) | (() => [`MenuOption[]`](../Components/MenuOption.mdx))

---
#### 🏷️ `placeholder` {#IProperties-placeholder}
Specifies the placeholder for unnamed items.
##### Type
string

---
#### 🏷️ `showAliases` {#IProperties-showAliases}
Specifies to show aliases. If this property is set to `true` it will use the [`Item`](Item.mdx) property that is decorated with the [`@alias`](../../decorators/alias.mdx) decorator. A function can be supplied to return an alternative alias for an [`Item`](Item.mdx).
##### Type
boolean | ((item: [`Item`](Item.mdx)) => string | undefined)

---
#### 🏷️ `showScores` {#IProperties-showScores}
Specifies to show the score of an item. If this property is set to `true` it will use the [`Item`](Item.mdx) property that is decorated with the [`@score`](../../decorators/score.mdx) decorator. A function can be supplied to return an alternative score for an [`Item`](Item.mdx).
##### Type
boolean | ((item: [`Item`](Item.mdx)) => number | string | undefined)

---
#### 🏷️ `sorting` {#IProperties-sorting}
Specifies the collection sorting type (defaults to `manual`). In manual mode, the user can rearrange the list using drag and drop.
##### Type
"ascending" | "descending" | "manual"

---
#### 🏷️ `title` {#IProperties-title}
Title for the collection card. The title is shown inside the collection card.
:::tip
If you want to have a title above the card, use the [`banner`](#IProperties-banner) property.
:::
##### Type
string

---
#### 🏷️ `width` {#IProperties-width}
Specifies the width if the item editor panel. Can be overruled per item with the [`@width`](../../decorators/width.mdx) decorator.
##### Type
number

---
#### 🔔 `onAdd` {#IProperties-onAdd}
Invoked when an item is added to the collection.
##### Signature
```ts
(item: Item) => void
```
##### Parameters
| Name   | Type               | Optional | Description                |
|:-------|:-------------------|:---------|:---------------------------|
| `item` | [`Item`](Item.mdx) | No       | Reference to the new item. |

---
#### 🔔 `onClose` {#IProperties-onClose}
Invoked when the editor panel of an item is closed.
##### Signature
```ts
(item: Item) => void
```
##### Parameters
| Name   | Type               | Optional | Description            |
|:-------|:-------------------|:---------|:-----------------------|
| `item` | [`Item`](Item.mdx) | No       | Reference to the item. |

---
#### 🔔 `onDelete` {#IProperties-onDelete}
Invoked when an item is removed from the collection.
##### Signature
```ts
(item: Item) => void
```
##### Parameters
| Name   | Type               | Optional | Description                    |
|:-------|:-------------------|:---------|:-------------------------------|
| `item` | [`Item`](Item.mdx) | No       | Reference to the deleted item. |

---
#### 🔔 `onOpen` {#IProperties-onOpen}
Invoked when the editor panel of an item is opened.
##### Signature
```ts
(item: Item) => void
```
##### Parameters
| Name   | Type               | Optional | Description            |
|:-------|:-------------------|:---------|:-----------------------|
| `item` | [`Item`](Item.mdx) | No       | Reference to the item. |

---
#### 🔔 `onRename` {#IProperties-onRename}
Invoked when the name of the item is changed.
##### Signature
```ts
(item: Item) => void
```
##### Parameters
| Name   | Type               | Optional | Description                    |
|:-------|:-------------------|:---------|:-------------------------------|
| `item` | [`Item`](Item.mdx) | No       | Reference to the changed item. |

---
#### 🔔 `onReposition` {#IProperties-onReposition}
Invoked when the position of the item is changed.
##### Signature
```ts
(item: Item) => void
```
##### Parameters
| Name   | Type               | Optional | Description                    |
|:-------|:-------------------|:---------|:-------------------------------|
| `item` | [`Item`](Item.mdx) | No       | Reference to the changed item. |

---
#### 🔔 `onResize` {#IProperties-onResize}
Invoked when the number of items in the collection has changed.
##### Signature
```ts
(collection: Provider) => void
```
##### Parameters
| Name         | Type                       | Optional | Description                           |
|:-------------|:---------------------------|:---------|:--------------------------------------|
| `collection` | [`Provider`](Provider.mdx) | No       | Reference to the collection provider. |

---
### 🔗 `ICollectionSupply` {#ICollectionSupply}
Describes the interface for a collection supply returned by the [`find`](#find) function.
##### Type declaration
<InterfaceDeclaration prefix="#ICollectionSupply-" src={`interface ICollectionSupply {
  readonly name: string;
  readonly collection: Provider;
  readonly origin: boolean;
  readonly sole: boolean;
}`} symbols={{
  "Provider": "/blocks/api/modules/Collection/Provider/"
}} />

---
#### 🏷️ `collection` {#ICollectionSupply-collection}
Contains a reference to the collection [`Provider`](Provider.mdx) instance.
##### Type
[`Provider`](Provider.mdx)

---
#### 🏷️ `name` {#ICollectionSupply-name}
Contains the name of the collection property in the [`NodeBlock`](../../classes/NodeBlock.mdx) derived class.
##### Type
string

---
#### 🏷️ `origin` {#ICollectionSupply-origin}
Specifies if the collection is the origin for the slot. This means the slot is created/maintained by the collection. For example, you have a multiple choice block where the choices are maintained using a collection. For each choice, a
##### Type
boolean

---
#### 🏷️ `sole` {#ICollectionSupply-sole}
Specifies if the collection is the sole supplier of values for the slot. For example, if your slot is used for a fixed dropdown list and the dropdown options are supplied by the collection, that collection is the sole supplier of possible values for the slot. If your slot is used for a text field with autocomplete suggestions and the collection is the supplier for the suggestions, the collection is not the sole supplier, because the respondent can also specify another value that is not in the collection list.
##### Type
boolean
