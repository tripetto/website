---
title: Collection Item class - Blocks
sidebar_label: Item
description: The Item class is an abstract class for creating item types for collections (builder package).
---

# Item class

## 📖 Description {#description}
The `Item` class is an [abstract class](https://www.typescriptlang.org/docs/handbook/2/classes.html#abstract-classes-and-members) for creating item types for collections. Item collections are used to enable the user to create lists with items of a certain type. For example, options in a dropdown list.

## 🎀 Applicable decorators {#decorators}
The following decorators can be applied in this class:
#### Property decorators [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#property-decorators)
- [`@affects`](../../decorators/affects.mdx)
- [`@alias`](../../decorators/alias.mdx)
- [`@definition`](../../decorators/definition.mdx)
- [`@icon`](../../decorators/icon.mdx)
- [`@menu`](../../decorators/menu.mdx)
- [`@name`](../../decorators/name.mdx)
- [`@score`](../../decorators/score.mdx)
- [`@title`](../../decorators/title.mdx)
- [`@width`](../../decorators/width.mdx)
#### Method decorators [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#method-decorators)
- [`@created`](../../decorators/created.mdx)
- [`@deleted`](../../decorators/deleted.mdx)
- [`@editor`](../../decorators/editor.mdx)
- [`@refreshed`](../../decorators/refreshed.mdx)
- [`@renamed`](../../decorators/renamed.mdx)
- [`@reordered`](../../decorators/reordered.mdx)

---
## 🆕 `constructor` {#constructor}
Creates a new `Item` instance.
#### Signature
```ts
constructor(): Item
```
#### Return value
Returns a reference to the new `Item` instance.

## 🗃️ Fields {#fields}

---
### 🏷️ `collection` {#collection}
Retrieves a reference to the parent collection provider.
#### Type
[`Provider`](Provider.mdx)

---
### 🏷️ `editor` {#editor}
Retrieves the [`EditorOrchestrator`](../../classes/EditorOrchestrator.mdx) instance used to define the controls to manage the item.
:::info
You should use the [`@editor`](../../decorators/editor.mdx) decorator to mark the method that implements the controls.
:::
#### Type
[`EditorOrchestrator`](../../classes/EditorOrchestrator.mdx)

---
### 🏷️ `id` {#id}
Retrieves the identifier of the item.
#### Type
string

---
### 🏷️ `index` {#index}
Retrieves the zero-based index of the item in the collection.
#### Type
number

---
### 🏷️ `isFirst` {#isFirst}
Retrieves if the item is the first item in the collection.
#### Type
boolean

---
### 🏷️ `isLast` {#isLast}
Retrieves if the item is the last item in the collection.
#### Type
boolean

---
### 🏷️ `predecessor` {#predecessor}
Retrieves a reference to the predecessor item (if any).
#### Type
`Item` | undefined

---
### 🏷️ `ref` {#ref}
Retrieves a reference to the parent of the collection.
:::info
The reference is set using the `reference` argument of the [`of`](index.mdx#of) function.
:::
#### Type
[`NodeBlock`](../../classes/NodeBlock.mdx) | [`ConditionBlock`](../../classes/ConditionBlock.mdx) | [`Item`](Item.mdx)

---
### 🏷️ `successor` {#successor}
Retrieves a reference to the successor item (if any).
#### Type
`Item` | undefined

## ▶️ Methods {#methods}

---
### 🔧 `delete` {#delete}
Deletes the item.
#### Signature
```ts
delete(): this
```
#### Return value
Returns a reference to the `Item` instance to allow chaining.

---
### 🔧 `refresh` {#refresh}
Refreshes the item.
#### Signature
```ts
refresh(type?: "icon" | "name"): void
```
#### Parameters
| Name   | Type             | Optional | Description                |
|:-------|:-----------------|:---------|:---------------------------|
| `type` | "icon" \| "name" | Yes      | Specifies what to refresh. |
