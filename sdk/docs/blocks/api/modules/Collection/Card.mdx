---
title: Collection Card class - Blocks
sidebar_label: Card
description: The Card class defines a UI component for managing a collection of items in the Tripetto builder (builder package).
---

import Preview from '@site/src/components/preview.js';

# Card class

## 📖 Description {#description}
The `Card` class defines a UI component for managing a collection of items in the Tripetto builder. It is derived from the [`Components.Card`](../Components/Card.mdx) class.

:::tip
This card is used by the [`collection`](../../classes/EditorOrchestrator.mdx#collection) method of the [`EditorOrchestrator`](../../classes/EditorOrchestrator.mdx) class used to define editor panels.
:::

## 📺 Preview {#preview}
<Preview src="editor-collection.png" />

---
## 🆕 `constructor` {#constructor}
Creates a new `Card` instance.
#### Signature
```ts
constructor(properties: IProperties): Card
```
#### Parameters
| Name         | Type                                   | Optional | Description                                  |
|:-------------|:---------------------------------------|:---------|:---------------------------------------------|
| `properties` | [`IProperties`](index.mdx#IProperties) | No       | Specifies the properties for the collection. |
#### Return value
Returns a reference to the new `Card` instance.

## 🗃️ Fields {#fields}

---
### 🏷️ `banner` {#banner}
Sets or retrieves the current banner. The banner is shown above the collection card.
#### Type {#signature}
string

---
### 🏷️ `icon` {#icon}
Sets or retrieves the default icon for collection items. This icon is used when an item doesn't have an icon set using the [`@icon`](../../decorators/icon.mdx) decorator.
#### Type {#signature}
string

---
### 🏷️ `placeholder` {#placeholder}
Retrieves the item placeholder that is shown for items that don't have a name.
#### Type {#signature}
string

## ▶️ Methods {#methods}

---
### 🔧 `aliases` {#aliases}
Changes the aliases setting. Aliases are used to identify items in a list. If an item has an alias it is shown in the list.
#### Signature
```ts
aliases(aliases: boolean | ((item: Item) => string | undefined)): this
```
#### Parameters
| Name      | Type                                                           | Optional | Description                                                                            |
|:----------|:---------------------------------------------------------------|:---------|:---------------------------------------------------------------------------------------|
| `aliases` | boolean \| ((item: [`Item`](Item.mdx)) => string \| undefined) | No       | Specifies if the aliases are enabled or not (or returns a specific alias for an item). |
#### Return value
Returns a reference to the `Card` instance to allow chaining.

---
### 🔧 `deduplicate` {#deduplicate}
Removes items with duplicate names from the collection.
#### Signature
```ts
deduplicate(): void
```

---
### 🔧 `deleteAll` {#deleteAll}
Deletes all items from the collection.
#### Signature
```ts
deleteAll(): void
```

---
### 🔧 `deleteUnnamed` {#deleteUnnamed}
Deletes all unnamed items from the collection.
#### Signature
```ts
deleteUnnamed(): void
```

---
### 🔧 `export` {#export}
Shows the item export panel for bulk exporting items.
#### Signature
```ts
export(): void
```

---
### 🔧 `import` {#import}
Shows the item import panel for bulk importing items.
#### Signature
```ts
import(): void
```

---
### 🔧 `indicators` {#indicators}
Changes the indicators function. Indicators can be used to show additional information for an item in the list.
#### Signature
```ts
indicators(indicators: ((item: Item) => string | undefined)): this
```
#### Parameters
| Name         | Type                                                            | Optional | Description                                                                                   |
|:-------------|:----------------------------------------------------------------|:---------|:----------------------------------------------------------------------------------------------|
| `indicators` | indicators: ((item: [`Item`](Item.mdx)) => string \| undefined) | No       | Specifies if the indicators are enabled or not (or returns a specific indicator for an item). |
#### Return value
Returns a reference to the `Card` instance to allow chaining.

---
### 🔧 `refresh` {#refresh}
Refreshes all data in the collection.
#### Signature
```ts
refresh(): void
```

---
### 🔧 `scores` {#scores}
Changes the scores setting.
#### Signature
```ts
scores(scores: boolean | ((item: Item) => number | string | undefined)): this
```
#### Parameters
| Name     | Type                                                                     | Optional | Description                                                                       |
|:---------|:-------------------------------------------------------------------------|:---------|:----------------------------------------------------------------------------------|
| `scores` | boolean \| ((item: [`Item`](Item.mdx)) => number \| string \| undefined) | No       | Specifies if scores are enabled or not (or returns a specific score for an item). |
#### Return value
Returns a reference to the `Card` instance to allow chaining.

---
### 🔧 `sort` {#sort}
Sorts the list with items.
#### Signature
```ts
sort(direction: "ascending" | "descending"): void
```
#### Parameters
| Name        | Type                        | Optional | Description                   |
|:------------|:----------------------------|:---------|:------------------------------|
| `direction` | "ascending" \| "descending" | No       | Specifies the sort direction. |
