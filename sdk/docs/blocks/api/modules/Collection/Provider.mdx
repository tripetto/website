---
title: Collection Provider class - Blocks
sidebar_label: Provider
description: The Provider class provides access to a collection in the Tripetto builder (builder package).
---

# Provider class

## 📖 Description {#description}
The `Provider` class provides access to a collection in the Tripetto builder.

:::tip
To construct new collections, use the [`of`](index.mdx#of) function.
:::

## 🗃️ Fields {#fields}

---
### 🏷️ `all` {#all}
Retrieves all the items that are in the collection.
#### Type
[`Item[]`](Item.mdx)

---
### 🏷️ `block` {#block}
Retrieves the block for the collection (if available).
#### Type
[`NodeBlock`](../../classes/NodeBlock.mdx) | [`ConditionBlock`](../../classes/ConditionBlock.mdx) | undefined

---
### 🏷️ `count` {#count}
Retrieves the number of items in the collection.
#### Type
number

---
### 🏷️ `firstItem` {#firstItem}
Retrieves the first item in the collection.
#### Type
[`Item`](Item.mdx) | undefined

---
### 🏷️ `lastItem` {#lastItem}
Retrieves the last item in the collection.
#### Type
[`Item`](Item.mdx) | undefined

---
### 🏷️ `ref` {#ref}
Retrieves a reference to the parent of the collection.
:::info
The reference is set using the `reference` argument of the [`of`](index.mdx#of) function.
:::
#### Type
[`NodeBlock`](../../classes/NodeBlock.mdx) | [`ConditionBlock`](../../classes/ConditionBlock.mdx) | [`Item`](Item.mdx)

## ▶️ Methods {#methods}

---
### 🔧 `append` {#append}
Appends a new item to the end of the collection.
:::tip
The optional `onCreate` parameter is only used under certain conditions. In most cases, you can omit it and call `append()`.
:::
#### Signature
```ts
append(onCreate?: (item: Item) => void): Item
```
#### Parameters
| Name       | Type                               | Optional | Description                                                                                                                                   |
|:-----------|:-----------------------------------|:---------|:----------------------------------------------------------------------------------------------------------------------------------------------|
| `onCreate` | (item: [`Item`](Item.mdx)) => void | Yes      | Specifies a function that can be used to set item properties before [`@created`](../../decorators/created.mdx) decorated methods are invoked. |
#### Return value
Returns a reference to the new [`Item`](Item.mdx) instance.

---
### 🔧 `duplicate` {#duplicate}
Duplicates an item in the collection.
:::caution
Duplicating an item in a collection requires two steps:
- Step 1: Call this `duplicate` method;
- Step 2: Set the properties of the new duplicated item;
- Step 3: Call the `done` function returned by the `duplicate` method to indicate the item is ready.
:::
#### Signature
```ts
duplicate(src: Item): {
  item: Item;
  done: () => void;
}
```
#### Parameters
| Name  | Type               | Optional | Description                             |
|:------|:-------------------|:---------|:----------------------------------------|
| `src` | [`Item`](Item.mdx) | No       | Specifies the source item to duplicate. |
#### Return value
Returns an object with a reference to the duplicate and a function that should be called when the duplicated item is ready. This allows you to configure the duplicated item.

---
### 🔧 `each` {#each}
Iterates through all items in the collection.
#### Signature
```ts
each(fn: (item: Item) => boolean | void): boolean
```
#### Parameters
| Name | Type                                          | Optional | Description                                                                                                                                    |
|:-----|:----------------------------------------------|:---------|:-----------------------------------------------------------------------------------------------------------------------------------------------|
| `fn` | (item: [`Item`](Item.mdx)) => boolean \| void | No       | Function that is called for each item in the collection. If you want to stop the iteration, you can return `true` from this function to do so. |
#### Return value
Returns `true` if the iteration was stopped (break).

---
### 🔧 `insert` {#insert}
Inserts a new item to the collection.
:::tip
The optional `onCreate` parameter is only used under certain conditions. In most cases, you can omit it and call `insert()`.
:::
#### Signature
```ts
insert(index?: number, onCreate?: (item: Item) => void): Item
```
#### Parameters
| Name       | Type                               | Optional | Description                                                                                                                                   |
|:-----------|:-----------------------------------|:---------|:----------------------------------------------------------------------------------------------------------------------------------------------|
| `index`    | number                             | Yes      | Specifies the index of the new item (default is `0`).                                                                                         |
| `onCreate` | (item: [`Item`](Item.mdx)) => void | Yes      | Specifies a function that can be used to set item properties before [`@created`](../../decorators/created.mdx) decorated methods are invoked. |
#### Return value
Returns a reference to the new [`Item`](Item.mdx) instance.

---
### 🔧 `itemByIdentifier` {#itemByIdentifier}
Retrieves the item with the specified identifier.
#### Signature
```ts
itemByIdentifier(id: string): Item | undefined
```
#### Parameters
| Name | Type   | Optional | Description               |
|:-----|:-------|:---------|:--------------------------|
| `id` | string | No       | Specifies the identifier. |
#### Return value
Returns the  [`Item`](Item.mdx) or `undefined` if the item is not found.

---
### 🔧 `refresh` {#refresh}
Refreshes all items in the collection.
#### Signature
```ts
refresh(): void
```

---
### 🔧 `reverseEach` {#reverseEach}
Iterates through all items in the collection in reverse direction (starting with the last item).
#### Signature
```ts
reverseEach(fn: (item: Item) => boolean | void): boolean
```
#### Parameters
| Name | Type                                                   | Optional | Description                                                                                                                           |
|:-----|:-------------------------------------------------------|:---------|:--------------------------------------------------------------------------------------------------------------------------------------|
| `fn` | (item: [`Item`](Item.mdx)) => boolean \| void | No       | Function that is called for each item in the collection. If you want to stop the iteration, you can return `true` from this function to do so. |
#### Return value
Returns `true` if the iteration was stopped (break).

---
### 🔧 `sort` {#sort}
Sorts the items using the supplied compare function.
#### Signature
```ts
sort(fn: (a: Item, b: Item) => number): boolean
```
#### Parameters
| Name | Type                                                      | Optional | Description                                                                                                                                                                                                                     |
|:-----|:----------------------------------------------------------|:---------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `fn` | (a: [`Item`](Item.mdx), b: [`Item`](Item.mdx)) => boolean | No       | Specifies the compare function. The compare function should return a number according to the following rules:<br/>- `a` is less than `b` return -1;<br/>- `a` is greater than `b` return 1;<br/>- `a` is equal to `b` return 0. |
#### Return value
Returns `true` if the sort invoked an item position change.
