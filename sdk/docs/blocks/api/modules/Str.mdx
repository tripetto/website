---
title: Str module - Blocks
sidebar_label: Str
sidebar_position: 12
description: The Str module contains helper functions related to strings (builder package).
---

# Str module

## 📖 Description {#description}
The `Str` module contains helper functions related to strings.

:::info Rationale
The functions in this module are primitive and probably widely available in many other open-source packages on [npm](https://npmjs.com). So why are they included in the Tripetto Builder package? That's because the Tripetto Builder doesn't have any dependency on other npm packages. So all the code needed to build a Tripetto form is included and these functions are simply required. Since they are part of the package, they are exported so you can use them too. For example when building custom blocks.
:::

## ▶️ Functions {#functions}

---
### 🔧 `capitalize` {#capitalize}
Capitalizes the first character of a string or the first character of each word.
#### Signature
```ts
capitalize(
  s: string,
  mode?: "first-character" | "each-word" | "each-sentence",
  lowercase?: boolean
): string
```
#### Parameters
| Name        | Type                                                | Optional | Description                                                                   |
|:------------|:----------------------------------------------------|:---------|:------------------------------------------------------------------------------|
| `s`         | string                                              | No       | Specifies the input string.                                                   |
| `mode`      | "first-character" \| "each-word" \| "each-sentence" | Yes      | Specifies the mode (default is `"first-character"`).                          |
| `lowercase` | boolean                                             | Yes      | Converts the string to lowercase before capitalizing it (default is `false`). |
#### Return value
Returns the capitalized string.
#### Example
```ts showLineNumbers
import { Str } from "@tripetto/builder";

Str.capitalize("lorem ipsum. dolor"); // Returns `Lorem ipsum. dolor`
Str.capitalize("lorem ipsum. dolor", "each-word"); // Returns `Lorem Ipsum. Dolor`
Str.capitalize("lorem ipsum. dolor", "each-sentence"); // Returns `Lorem ipsum. Dolor`
```

---
### 🔧 `CRLFToHTML` {#CRLFToHTML}
Converts carriage returns and/or newlines to HTML breaks.
#### Signature
```ts
CRLFToHTML(s: string): string
```
#### Parameters
| Name | Type   | Optional | Description                           |
|:-----|:-------|:---------|:--------------------------------------|
| `s`  | string | No       | Specifies the string to be converted. |
#### Return value
Returns the converted string.
#### Example
```ts showLineNumbers
import { Str } from "@tripetto/builder";

Str.CRLFToHTML("Row 1\r\nRow 2"); // Returns `Row 1<br />Row 2`
```

---
### 🔧 `djb2Hash` {#djb2Hash}
Creates a simple hash for the supplied string using the [djb2-algorithm](http://www.cse.yorku.ca/~oz/hash.html) written by Dan Bernstein. This hash function is similar to a linear congruential generator and is absolutely not collision resistant.
#### Signature
```ts
djb2Hash(s: string, prefix?: string): string
```
#### Parameters
| Name     | Type   | Optional | Description                             |
|:---------|:-------|:---------|:----------------------------------------|
| `s`      | string | No       | Specifies the input string.             |
| `prefix` | string | Yes      | Specifies a prefix for the hash result. |
#### Return value
Returns the hash string.
#### Example
```ts showLineNumbers
import { Str } from "@tripetto/builder";

Str.djb2Hash("Abc"); // Returns `ABCAA`
Str.djb2Hash("Abc", "prefix-"); // Returns `prefix-ABCAA`
```

---
### 🔧 `extract` {#extract}
Extracts the part of the string between the first occurrence of `left` and the optional occurrence of `right`.
#### Signature
```ts
extract<T>(
  s: string,
  options: {
    left: string;
    right?: string;
    fromEnd?: boolean;
    ignoreCase?: boolean;
  }
): string
```
#### Parameters
| Name      | Type    | Optional | Description                                                                                                                                                                                                                                                                                                                                                                                                                     |
|:----------|:--------|:---------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `s`       | string  | No       | Specifies the string to be extracted (variable will cast to a string if necessary).                                                                                                                                                                                                                                                                                                                                             |
| `options` | object  | No       | Specifies the extract options. Supports the following options:<br/>- `left`: Left string selector:<br/>- `right`: Right string selector:<br/>- `fromEnd`: Specifies the search direction of the right string selector. If `false` is specified, the function searches for the first occurrence of `Right` directly after the position of `Left`.<br/>- `ignoreCase`: Specifies if a case insensitive check should be performed. |

#### Return value
Returns the string between `left` and `right` or an empty string if an error occurs.
#### Example
```ts showLineNumbers
import { Str } from "@tripetto/builder";

Str.extract("Lorem ipsum dolor", { left: "Lo", right: "m" }); // Returns `re`
Str.extract("Lorem ipsum dolor", { left: "Lo", right: "m", fromEnd: true }); // Returns `rem ipsu`
```

---
### 🔧 `fill` {#fill}
Fills a string for the specified times with the specified string or characters.
#### Signature
```ts
fill(s: string, count: number): string
```
#### Parameters
| Name    | Type   | Optional | Description                               |
|:--------|:-------|:---------|:------------------------------------------|
| `s`     | string | No       | Specifies the input string.               |
| `count` | number | No       | Specifies the number of copies to insert. |
#### Return value
Returns the filled string.
#### Example
```ts showLineNumbers
import { Str } from "@tripetto/builder";

Str.fill("A", 5); // Returns `AAAAA`
```

---
### 🔧 `iterateToString` {#iterateToString}
Converts an array or object list to a concatenated string.
#### Signature
```ts
iterateToString<T>(
    list: TList<T>,
    separator?: string,
    fn?: (item: T, ...arguments: any[]) => string,
    ...arguments: any[]
): string
```
#### Parameters
| Name        | Type                                     | Optional | Description                                                                                                                                                                                                                                                         |
|:------------|:-----------------------------------------|:---------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `list`      | `T[]` \| undefined                       | No       | Specifies the array, collection or enumerable object to iterate through.                                                                                                                                                                                            |
| `separator` | string                                   | Yes      | Optional parameter which specifies the separator string or char.                                                                                                                                                                                                    |
| `fn`        | (item: T, ...arguments: any[]) => string | Yes      | Optional function to be invoked for each element (instead of automatic casting to strings). The element value will be exposed to the function as the first argument of the argument list. Additional arguments can be specified and will be pushed to the function. |
| `arguments` | any[]                                    | Yes      | Optional arguments which should be passed to the callee.                                                                                                                                                                                                            |

#### Return value
Returns the concatenated string.
#### Example
```ts showLineNumbers
import { Str } from "@tripetto/builder";

Str.iterateToString(["a", "b", "c"]); // Returns `abc`
Str.iterateToString(["a", "b", "c"], "-"); // Returns `a-b-c`
Str.iterateToString(["a", "b", "c"], "/", (sValue: string) => sValue + sValue); // Returns `aa/bb/cc`
```

---
### 🔧 `limit` {#limit}
Limits a string that is greater than the specified number of characters and appends an optional string. If an append string is specified any trailing spaces in the trimmed string are removed.
#### Signature
```ts
limit(s: string, max: number, append?: string): string
```
#### Parameters
| Name     | Type   | Optional | Description                                            |
|:---------|:-------|:---------|:-------------------------------------------------------|
| `s`      | string | No       | Specifies the input string.                            |
| `max`    | number | No       | Maximum string length.                                 |
| `append` | string | Yes      | Optional string which is appended to a limited string. |
#### Return value
Returns the limited string.
#### Example
```ts showLineNumbers
import { Str } from "@tripetto/builder";

Str.limit("Lorem ipsum", 5, "..."); // Returns `Lorem...`
Str.limit("Lorem ipsum", 6, "..."); // Returns `Lorem...`
Str.limit("Lorem ipsum", 6); // Returns `Lorem `
```

---
### 🔧 `lowercase` {#lowercase}
Converts a string to lowercase.
#### Signature
```ts
lowercase(s: string): string
```
#### Parameters
| Name | Type   | Optional | Description                 |
|:-----|:-------|:---------|:----------------------------|
| `s`  | string | No       | Specifies the input string. |
#### Return value
Returns the converted string.
#### Example
```ts showLineNumbers
import { Str } from "@tripetto/builder";

Str.lowercase("HELLO"); // Returns `hello`
```

---
### 🔧 `makeHTMLSafe` {#makeHTMLSafe}
Converts HTML brackets to HTML entities so it is safe to display without HTML parsing.
:::caution
Careful, only the brackets (`<` and `>`) are converted to HTML entities.
:::
#### Signature
```ts
makeHTMLSafe(s: string): string
```
#### Parameters
| Name | Type   | Optional | Description                           |
|:-----|:-------|:---------|:--------------------------------------|
| `s`  | string | No       | Specifies the string to be converted. |
#### Return value
Returns the converted string.
#### Example
```ts showLineNumbers
import { Str } from "@tripetto/builder";

Str.makeHTMLSafe("<b>Hello</b>"); // Returns `&lt;b&gt;Hello&lt;b&gt;`
```

---
### 🔧 `padLeft` {#padLeft}
Converts source variable to a string and pads the string on the left with the supplied number of characters.
#### Signature
```ts
padLeft(
  value: string | number,
  fill: string,
  length: number,
  crop?: boolean,
  treatAsNumber?: boolean
): string
```
#### Parameters
| Name            | Type             | Optional | Description                                                                                                                |
|:----------------|:-----------------|:---------|:---------------------------------------------------------------------------------------------------------------------------|
| `value`         | string \| number | No       | Specifies the input string or number.                                                                                      |
| `fill`          | string           | No       | Contains the string which will be used as fill string.                                                                     |
| `length`        | number           | No       | Specifies the desired string length.                                                                                       |
| `crop`          | boolean          | Yes      | Optional boolean value which enables string cropping if the source string length is larger than the desired string length. |
| `treatAsNumber` | boolean          | Yes      | Optional boolean value which specifies the input string should be treated as a number.                                     |
#### Return value
Returns the padded string.
#### Example
```ts showLineNumbers
import { Str } from "@tripetto/builder";

Str.padLeft("ABC", "A", 5); // Returns `AAABC`
```

---
### 🔧 `padRight` {#padRight}
Converts source variable to a string and pads the string on the right with the supplied number of characters.
#### Signature
```ts
padRight(value: string | number, fill: string, length: number, crop?: boolean): string
```
#### Parameters
| Name            | Type             | Optional | Description                                                                                                                |
|:----------------|:-----------------|:---------|:---------------------------------------------------------------------------------------------------------------------------|
| `value`         | string \| number | No       | Specifies the input string or number.                                                                                      |
| `fill`          | string           | No       | Contains the string which will be used as fill string.                                                                     |
| `length`        | number           | No       | Specifies the desired string length.                                                                                       |
| `crop`          | boolean          | Yes      | Optional boolean value which enables string cropping if the source string length is larger than the desired string length. |
#### Return value
Returns the padded string.
#### Example
```ts showLineNumbers
import { Str } from "@tripetto/builder";

Str.padRight("ABC", "A", 5); // Returns `ABCAA`
```

---
### 🔧 `removeWhitespaces` {#removeWhitespaces}
Removes all white spaces from the specified string (`_ab__cd_` -> `abcd`).
#### Signature
```ts
removeWhitespaces(s: string): string
```
#### Parameters
| Name | Type   | Optional | Description                 |
|:-----|:-------|:---------|:----------------------------|
| `s`  | string | No       | Specifies the input string. |
#### Return value
Returns the sanitized string.
#### Example
```ts showLineNumbers
import { Str } from "@tripetto/builder";

Str.removeWhitespaces(" ab  cd "); // Returns `abcd`
```

---
### 🔧 `replace` {#replace}
Replaces all occurrences of `what` with `with` in the specified string.
#### Signature
```ts
replace(s: string, what: string, with?: string, ignoreCase?: boolean): string
```
#### Parameters
| Name         | Type    | Optional | Description                                                                      |
|:-------------|:--------|:---------|:---------------------------------------------------------------------------------|
| `s`          | string  | No       | Specifies the input string (haystack).                                           |
| `what`       | string  | No       | Specifies the string to search for (needle).                                     |
| `with`       | string  | Yes      | Specifies the replace string. If omitted an empty string will be used.           |
| `ignoreCase` | boolean | Yes      | Specifies if the string replace should be case insensitive (default is `false`). |
#### Return value
Returns the replaced string.
#### Example
```ts showLineNumbers
import { Str } from "@tripetto/builder";

Str.replace("Hello", "l", "-"); // Returns `He--o`
```

---
### 🔧 `replaceMultiple` {#replaceMultiple}
Replaces all occurrences of strings in `what` with `with` in the specified string.
#### Signature
```ts
replaceMultiple(s: string, what: string[], with?: string, ignoreCase?: boolean): string
```
#### Parameters
| Name         | Type     | Optional | Description                                                                      |
|:-------------|:---------|:---------|:---------------------------------------------------------------------------------|
| `s`          | string   | No       | Specifies the input string (haystack).                                           |
| `what`       | string[] | No       | Specifies the strings to search for (needles).                                   |
| `with`       | string   | Yes      | Specifies the replace string. If omitted an empty string will be used.           |
| `ignoreCase` | boolean  | Yes      | Specifies if the string replace should be case insensitive (default is `false`). |
#### Return value
Returns the replaced string.
#### Example
```ts showLineNumbers
import { Str } from "@tripetto/builder";

Str.replaceMultiple("Hello", ["ll", "o"], "-"); // Returns `He---`
```

---
### 🔧 `sanitize` {#sanitize}
Sanitize a string by removing all leading, trailing and multiple whitespaces (`_ab__cd_` -> `ab_cd`).
#### Signature
```ts
sanitize(s: string): string
```
#### Parameters
| Name | Type   | Optional | Description                 |
|:-----|:-------|:---------|:----------------------------|
| `s`  | string | No       | Specifies the input string. |
#### Return value
Returns the sanitized string.
#### Example
```ts showLineNumbers
import { Str } from "@tripetto/builder";

Str.sanitize(" ab  cd "); // Returns `ab cd`
```

---
### 🔧 `trim` {#trim}
Trims a string by removing all leading and trailing whitespaces (`_ab__cd_` -> `ab__cd`).
#### Signature
```ts
trim(s: string): string
```
#### Parameters
| Name | Type   | Optional | Description                 |
|:-----|:-------|:---------|:----------------------------|
| `s`  | string | No       | Specifies the input string. |
#### Return value
Returns the trimmed string.
#### Example
```ts showLineNumbers
import { Str } from "@tripetto/builder";

Str.trim(" ab  cd "); // Returns `ab  cd`
```

---
### 🔧 `trimLeft` {#trimLeft}
Trims a string at the left side by removing all leading whitespaces (`_ab__cd_` -> `ab__cd_`).
#### Signature
```ts
trimLeft(s: string): string
```
#### Parameters
| Name | Type   | Optional | Description                 |
|:-----|:-------|:---------|:----------------------------|
| `s`  | string | No       | Specifies the input string. |
#### Return value
Returns the trimmed string.
#### Example
```ts showLineNumbers
import { Str } from "@tripetto/builder";

Str.trimLeft(" ab  cd "); // Returns `ab  cd `
```

---
### 🔧 `trimMultiple` {#trimMultiple}
Trims a string by removing all multiple whitespaces (`_ab__cd_` -> `_ab_cd_`).
#### Signature
```ts
trimMultiple(s: string): string
```
#### Parameters
| Name | Type   | Optional | Description                 |
|:-----|:-------|:---------|:----------------------------|
| `s`  | string | No       | Specifies the input string. |
#### Return value
Returns the trimmed string.
#### Example
```ts showLineNumbers
import { Str } from "@tripetto/builder";

Str.trimMultiple(" ab  cd "); // Returns ` ab cd `
```

---
### 🔧 `trimRight` {#trimRight}
Trims a string at the right by removing all trailing whitespaces (`_ab__cd_` -> `_ab__cd`).
#### Signature
```ts
trimRight(s: string): string
```
#### Parameters
| Name | Type   | Optional | Description                 |
|:-----|:-------|:---------|:----------------------------|
| `s`  | string | No       | Specifies the input string. |
#### Return value
Returns the trimmed string.
#### Example
```ts showLineNumbers
import { Str } from "@tripetto/builder";

Str.trimRight(" ab  cd "); // Returns ` ab  cd`
```

---
### 🔧 `uppercase` {#uppercase}
Converts a string to uppercase.
#### Signature
```ts
uppercase(s: string): string
```
#### Parameters
| Name | Type   | Optional | Description                 |
|:-----|:-------|:---------|:----------------------------|
| `s`  | string | No       | Specifies the input string. |
#### Return value
Returns the converted string.
#### Example
```ts showLineNumbers
import { Str } from "@tripetto/builder";

Str.uppercase("Hello"); // Returns `HELLO`
```
