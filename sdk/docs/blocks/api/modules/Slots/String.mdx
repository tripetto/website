---
title: Slots String class - Blocks
sidebar_label: String
description: The String class defines a slot that can hold string data (builder package).
---

# String class

## 📖 Description {#description}
The `String` class defines a slot that can hold string data. It is derived from the [`Slot`](Slot.mdx) class.

## 🗃️ Fields {#fields}

---
### 🏷️ `actionable` {#actionable}
Sets or retrieves if the slot is actionable. When set to `true` the slot will be included in the [`actionables`](../../../../runner/api/library/modules/Export.mdx#actionables) export function.
:::info
When set to `true`, the default value for the [`exportable`](#exportable) property will be `false`. If you want to make a slot both actionable and exportable, you should set both properties to `true`.
:::
:::tip
More information about actionable data can be found [here](../../../../runner/api/library/modules/Export.mdx#exportable-vs-actionable).
:::
#### Type {#signature}
boolean

---
### 🏷️ `alias` {#alias}
Sets or retrieves the slot alias. This alias is used to identify the slot in the dataset of a form. The alias is, for example, used in the [`NVPs`](../../../../runner/api/library/modules/Export.mdx#NVPs) and [`CSV`](../../../../runner/api/library/modules/Export.mdx#CSV) export functions.
:::tip
When [prefilling](../../../../runner/stock/guides/prefilling.mdx) (or importing data into) forms the alias can be used to easily specify the right data for the right slot.
:::
#### Type {#signature}
string

---
### 🏷️ `default` {#default}
Sets or retrieves the default value. This default value will be used when there is no explicit value set for a slot or when the slot value is cleared.
#### Type {#signature}
string

---
### 🏷️ `exportable` {#exportable}
Sets or retrieves if the slot is exportable. When set to `true`, the slot will be included in the [`exportables`](../../../../runner/api/library/modules/Export.mdx#exportables) export function.
:::info
This property defaults to `true` when the `exportable` property is omitted, and the [`actionable`](#actionable) property is either set to `false` or omitted as well.
:::
:::tip
More information about exportable data can be found [here](../../../../runner/api/library/modules/Export.mdx#exportable-vs-actionable).
:::
#### Type {#signature}
boolean

---
### 🏷️ `id` {#id}
Retrieves the identifier of the slot.
#### Type {#signature}
string

---
### 🏷️ `kind` {#kind}
Retrieves the slot kind.
#### Type {#signature}
"static" | "dynamic" | "feature" | "meta"

---
### 🏷️ `label` {#label}
Sets or retrieves the (localized) label for the slot. This label is often a description of the kind of data the slot holds. For example, `Text` for a text input block. When a block has multiple slots, the label is used to distinguish the different slots.
#### Type {#signature}
string

---
### 🏷️ `name` {#name}
Sets or retrieves the slot name.
:::tip
For [`dynamic`](SlotsClass.mdx#dynamic) slots, this name is often the same as the name of the item for which the slot is intended.
:::
#### Type {#signature}
string

---
### 🏷️ `pipeable` {#pipeable}
Sets or retrieves if the slot is pipeable. Piping is the process of recalling slot values in (markdown) text in the form. There are three possible values:
- `true`: Slot can be used as piping value (this is the default behavior);
- `false`: Slot cannot be used as piping value;
- Or a custom configuration to instruct Tripetto how to recall the slot.

:::info
To simply enable or disable piping for the slot (based on the slot value), supply a boolean value. If you need more control over the pipe, you can supply an object with a more specific configuration.
:::
#### Type {#signature}
```ts
boolean | {
  /* Optional name for the pipe. This is used to group slot values that have the same pipe name. */
  pipe?: string;

  /* Optional localized label for the pipe. */
  label?: string;

  /* Optional alias for the pipe. */
  alias?: string;

  /*
   * Specifies the field or content that should be used as the data that goes
   * into the pipe. It can be one of the following values:
   * - `value`: Use the current string value of the slot (this is the default behavior);
   * - `label`: Use the slot label;
   * - `name`: Use the name of the slot;
   * - Custom configuration to supply the data that goes into the pipe.
   */
  content?: "value" | "label" | "name" | {
    /* Contains the content as a string without any markup or variables. */
    string: string;

    /* Contains the content as text with support for variables. */
    text?: string;

    /* Contains markdown content with support for basic formatting, hyperlinks, and variables. */
    markdown?: string;
  };

  /*
   * Specifies the name of a legacy pipe. Only here for backward compatibility. Do not use.
   * @deprecated
   */
  legacy?: string; 🗑️
}
```

---
### 🏷️ `placeholder` {#placeholder}
Sets or retrieves the slot placeholder that can be used when a slot doesn't hold a value.
#### Type {#signature}
string

---
### 🏷️ `protected` {#protected}
Sets or retrieves whether the slot is write-protected and can only be changed by the block that created the slot. Other blocks in the form (like the [Setter block](../../../stock/setter.mdx)) cannot change the data of the slot.
#### Type {#signature}
boolean

---
### 🏷️ `reference` {#reference}
Retrieves the slot reference. This is a unique reference to the slot within a block. You use the reference to retrieve a certain slot in the runner part of a block.
#### Type {#signature}
string

---
### 🏷️ `required` {#required}
Sets or retrieves if the slot is required. When set to `true`, the block validation will only pass when a slot has a valid value.
#### Type {#signature}
boolean
:::caution
The Runner library will automatically validate if all required slots have a valid value.
:::

---
### 🏷️ `sequence` {#sequence}
Sets or retrieves the sequence number that is used for sorting the slot collection.
#### Type {#signature}
number | undefined

---
### 🏷️ `slots` {#slots}
Retrieves a reference to the slots collection.
#### Type {#signature}
[`Slots`](SlotsClass.mdx)

---
### 🏷️ `type` {#type}
Retrieves the slot type identifier.
#### Type {#signature}
string

## ▶️ Methods {#methods}

---
### 🔧 `delete` {#delete}
Deletes a slot from the slots collection.
#### Signature
```ts
delete(): this
```
#### Return value
Returns a reference to the `Slot` instance.

---
### 🔧 `deprecate` {#deprecate}
Deprecates a slot. This removes a slot from the slots collection and is used when a new version of a block needs to remove a slot that was created by an earlier version of the block.
#### Signature
```ts
deprecate(): this
```
#### Return value
Returns a reference to the `Slot` instance.

---
### 🔧 `toString` {#toString}
Converts the supplied data to a string representation.
#### Signature
```ts
toString(data: any): string
```
#### Parameters
| Name   | Type | Optional | Description         |
|:-------|:-----|:---------|:--------------------|
| `data` | any  | No       | Specifies the data. |
#### Return value
Returns the data formatted to a string.

---
### 🔧 `toValue` {#toValue}
Converts the supplied data to a valid string.
#### Signature
```ts
toValue(data: any): string
```
#### Parameters
| Name   | Type | Optional | Description         |
|:-------|:-----|:---------|:--------------------|
| `data` | any  | No       | Specifies the data. |
#### Return value
Returns the string value.
