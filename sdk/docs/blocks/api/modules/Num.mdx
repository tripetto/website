---
title: Num module - Blocks
sidebar_label: Num
sidebar_position: 9
description: The Num module contains helper functions related to numbers (builder package).
---

# Num module

## 📖 Description {#description}
The `Num` module contains helper functions related to numbers.

:::info Rationale
The functions in this module are primitive and probably widely available in many other open-source packages on [npm](https://npmjs.com). So why are they included in the Tripetto Builder package? That's because the Tripetto Builder doesn't have any dependency on other npm packages. So all the code needed to build a Tripetto form is included and these functions are simply required. Since they are part of the package, they are exported so you can use them too. For example when building custom blocks.
:::

## ▶️ Functions {#functions}
---
### 🔧 `ceil` {#ceil}
Round a floating point number upward to its nearest integer.
#### Signature
```ts
ceil(n: number): number
```
#### Parameters
| Name | Type   | Optional | Description                 |
|:-----|:-------|:---------|:----------------------------|
| `n`  | number | No       | Specifies the input number. |
#### Return value
Returns the ceil number.
#### Example
```ts showLineNumbers
import { Num } from "@tripetto/builder";

Num.ceil(1.6); // Returns `2`
```

---
### 🔧 `conform` {#conform}
Conforms the supplied number to the specified precision.
#### Signature
```ts
conform(n: number, precision: number): number
```
#### Parameters
| Name        | Type   | Optional | Description                       |
|:------------|:-------|:---------|:----------------------------------|
| `n`         | number | No       | Specifies the input number.       |
| `precision` | number | No       | Specifies the number of decimals. |
#### Return value
Returns the conformed number.
#### Example
```ts showLineNumbers
import { Num } from "@tripetto/builder";

Num.conform(1.1235, 3); // Returns `1.124`
Num.conform(1.1235, 2); // Returns `1.12`
```

---
### 🔧 `floor` {#floor}
Round a floating point number downward to its nearest integer.
#### Signature
```ts
floor(n: number): number
```
#### Parameters
| Name | Type   | Optional | Description                 |
|:-----|:-------|:---------|:----------------------------|
| `n`  | number | No       | Specifies the input number. |
#### Return value
Returns the floored number.
#### Example
```ts showLineNumbers
import { Num } from "@tripetto/builder";

Num.floor(1.6); // Returns `1`
```

---
### 🔧 `format` {#format}
Formats a number by inserting thousands and decimal separators while taking rounding into account.
#### Signature
```ts
format(
  input: number | string,
  precision?: number | "auto",
  separator?: string,
  decimal?: string,
  minus?: string
): string
```
#### Parameters
| Name        | Type             | Optional | Description                                                                                             |
|:------------|:-----------------|:---------|:--------------------------------------------------------------------------------------------------------|
| `input`     | number \| string | No       | Specifies the input value as a number or string.                                                        |
| `precision` | number \| "auto" | Yes      | Specifies the precision (default is `0`). Set it to `auto` to keep the precision of the supplied input. |
| `separator` | string           | Yes      | Separator sign (default is `,`).                                                                        |
| `decimal`   | string           | Yes      | Decimal sign (default is `.`).                                                                          |
| `minus`     | string           | Yes      | Minus sign (default is `-`).                                                                            |
#### Return value
Returns the formatted number as a string.
#### Example
```ts showLineNumbers
import { Num } from "@tripetto/builder";

Num.format("1000.235", 2); // Returns `1,000.24`
Num.format("1000.235", "auto"); // Returns `1,000.235`
```

---
### 🔧 `inRange` {#inRange}
Checks if the given value is within the specified range.
#### Signature
```ts
inRange(n: number, min: number, max: number, edgeMin?: boolean, edgeMax?: boolean): boolean
```
#### Parameters
| Name      | Type    | Optional | Description                                                             |
|:----------|:--------|:---------|:------------------------------------------------------------------------|
| `n`       | number  | No       | Specifies the input number.                                             |
| `min`     | number  | No       | Specifies the minimum value.                                            |
| `max`     | number  | No       | Specifies the maximum value.                                            |
| `edgeMin` | boolean | Yes      | Specifies if the edge of the min value is allowed (default is `true`).  |
| `edgeMax` | boolean | Yes      | Specifies if the edge of the max value is allowed (default is `false`). |
#### Return value
Returns `true` if the value is in range.
#### Example
```ts showLineNumbers
import { Num } from "@tripetto/builder";

Num.inRange(5, 5, 10); // Returns `true`
Num.inRange(0, 5, 10); // Returns `false`
```

---
### 🔧 `max` {#max}
Compares two numbers and returns the highest value.
#### Signature
```ts
max(a: number, b: number): number
```
#### Parameters
| Name | Type   | Optional | Description                   |
|:--------------|:---------|:---------|:-------------------|
| `a`  | number | No       | Specifies the input number A. |
| `b`  | number | No       | Specifies the input number B. |
#### Return value
Returns the number with the highest value.
#### Example
```ts showLineNumbers
import { Num } from "@tripetto/builder";

Num.max(1, 2); // Returns `2`
```

---
### 🔧 `maxL` {#maxL}
Compares the supplied arguments returns the highest value.
#### Signature
```ts
maxL(...arguments: number[]): number
```
#### Parameters
| Name        | Type     | Optional | Description           |
|:------------|:---------|:---------|:----------------------|
| `arguments` | number[] | No       | Arguments to compare. |
#### Return value
Returns the number with the highest value.
#### Example
```ts showLineNumbers
import { Num } from "@tripetto/builder";

Num.maxL(1, 2, 5, 3); // Returns `5`
```

---
### 🔧 `min` {#min}
Compares two numbers and returns the lowest value.
#### Signature
```ts
min(a: number, b: number): number
```
#### Parameters
| Name | Type   | Optional | Description                   |
|:--------------|:---------|:---------|:-------------------|
| `a`  | number | No       | Specifies the input number A. |
| `b`  | number | No       | Specifies the input number B. |
#### Return value
Returns the number with the lowest value.
#### Example
```ts showLineNumbers
import { Num } from "@tripetto/builder";

Num.min(1, 2); // Returns `1`
```

---
### 🔧 `minL` {#minL}
Compares the supplied arguments returns the lowest value.
#### Signature
```ts
minL(...arguments: number[]): number
```
#### Parameters
| Name        | Type     | Optional | Description           |
|:------------|:---------|:---------|:----------------------|
| `arguments` | number[] | No       | Arguments to compare. |
#### Return value
Returns the number with the lowest value.
#### Example
```ts showLineNumbers
import { Num } from "@tripetto/builder";

Num.minL(1, 2, 5, 3); // Returns `1`
```

---
### 🔧 `negative` {#negative}
Checks if the supplied number is negative and returns this negative value or `0` if the value is positive.
#### Signature
```ts
negative(n: number): number
```
#### Parameters
| Name | Type   | Optional | Description                 |
|:-----|:-------|:---------|:----------------------------|
| `n`  | number | No       | Specifies the input number. |
#### Return value
Returns n if the value is negative otherwise `0`.
#### Example
```ts showLineNumbers
import { Num } from "@tripetto/builder";

Num.negative(1); // Returns `0`
Num.negative(-1); // Returns `-1`
```

---
### 🔧 `positive` {#positive}
Checks if the supplied number is positive and returns this positive value or `0` if the value is negative.
#### Signature
```ts
positive(n: number): number
```
#### Parameters
| Name | Type   | Optional | Description                 |
|:-----|:-------|:---------|:----------------------------|
| `n`  | number | No       | Specifies the input number. |
#### Return value
Returns n if the value is positive otherwise `0`.
#### Example
```ts showLineNumbers
import { Num } from "@tripetto/builder";

Num.positive(1); // Returns `1`
Num.positive(-1); // Returns `0`
```

---
### 🔧 `range` {#range}
Adjusts a number so it is in range between the specifies minimum and maximum.
#### Signature
```ts
range(n: number, min: number, max?: number): number
```
#### Parameters
| Name  | Type   | Optional | Description                  |
|:---------------|:---------|:---------|:------------------|
| `n`   | number | No       | Specifies the input number.  |
| `min` | number | No       | Specifies the minimum value. |
| `max` | number | Yes      | Specifies the maximum value. |
#### Return value
Returns the ranged number.
#### Example
```ts showLineNumbers
import { Num } from "@tripetto/builder";

Num.range(20, 5, 10); // Returns `10`
Num.range(0, 5, 10); // Returns `5`
```

---
### 🔧 `round` {#round}
Round a floating point number to the nearest integer.
#### Signature
```ts
round(n: number): number
```
#### Parameters
| Name | Type   | Optional | Description                 |
|:-----|:-------|:---------|:----------------------------|
| `n`  | number | No       | Specifies the input number. |
#### Return value
Returns the rounded number.
#### Example
```ts showLineNumbers
import { Num } from "@tripetto/builder";

Num.round(1.49); // Returns `1`
Num.round(1.5); // Returns `2`
```
