---
title: Component Menu class - Blocks
sidebar_label: Menu
description: The Menu class defines a menu control for the Tripetto builder (builder package).
---

# Menu class

## 📖 Description {#description}
The `Menu` class defines a menu control for the Tripetto builder.

## 📌 Statics {#statics}

---
### 🏷️ `style` {#statics-style}
Contains the default style.
#### Type {#signature}
[`IMenuStyle`](#IMenuStyle)

---
### 🏷️ `stylesheet` {#stylesheet}
Contains the stylesheet reference.
#### Type {#signature}
`Stylesheet`

## 🗃️ Fields {#fields}

---
### 🏷️ `isActive` {#isActive}
Retrieves if there is a menu active.
#### Type
boolean

---
### 🏷️ `isDestroyed` {#isDestroyed}
Retrieves if the menu is destroyed.
#### Type
boolean

---
### 🏷️ `isDrawn` {#isDrawn}
Retrieves if the menu is drawn.
#### Type
boolean

---
### 🏷️ `isHovered` {#isHovered}
Retrieves if the menu is hovered.
#### Type
boolean

---
### 🏷️ `isOpen` {#isOpen}
Retrieves if the menu is opened.
#### Type
boolean

---
### 🏷️ `options` {#options}
Retrieves the menu options array.
#### Type
[`MenuOption[]`](MenuOption.mdx)

---
### 🏷️ `style` {#style}
Retrieves the menu style.
#### Type
IMenuStyle

---
### 🏷️ `stylesheet` {#stylesheet}
Retrieves the stylesheet for the menu.
#### Type
Stylesheet

## ▶️ Methods {#methods}

---
### 🔧 `close` {#close}
Closes the current active menu.
#### Signature
```ts
close(animate?: boolean, fn?: (menu: Menu) => void): void
```
#### Parameters
| Name      | Type                   | Optional | Description                                                          |
|:----------|:-----------------------|:---------|:---------------------------------------------------------------------|
| `animate` | boolean                | Yes      | Specifies if an animation should be used.                            |
| `fn`      | (menu: `Menu`) => void | Yes      | Optional callback function which is invoked when the menu is closed. |

---
### 🔧 `destroy` {#destroy}
Destroys the menu.
#### Signature
```ts
destroy(): void
```

---
### 🔧 `focus` {#focus}
Sets the focus to the first option of the menu.
#### Signature
```ts
focus(): this
```
#### Return value
Returns a reference to the `Menu` instance to allow chaining.

---
### 🔧 `open` {#open}
Creates a new menu and opens it at the specified position in the rectangle.
#### Signature
```ts
open(options: MenuOption[], rectangle: Rectangle, properties?: IMenuProperties): Menu
```
#### Parameters
| Name         | Type                                  | Optional | Description                      |
|:-------------|:--------------------------------------|:---------|:---------------------------------|
| `options`    | [`MenuOption[]`](MenuOption.mdx)      | No       | Specifies the menu options.      |
| `rectangle`  | `Rectangle`                           | No       | Specifies the display rectangle. |
| `properties` | [`IMenuProperties`](#IMenuProperties) | Yes      | Specifies the menu properties.   |
#### Return value
Returns a reference to the `Menu` instance to allow chaining.

---
### 🔧 `openAtElement` {#openAtElement}
Creates a new menu and opens it at the specified asset.
#### Signature
```ts
openAtElement(options: MenuOption[], asset: Asset, properties?: IMenuProperties): Menu
```
#### Parameters
| Name         | Type                                  | Optional | Description                    |
|:-------------|:--------------------------------------|:---------|:-------------------------------|
| `options`    | [`MenuOption[]`](MenuOption.mdx)      | No       | Specifies the menu options.    |
| `asset`      | `Asset`                               | No       | Specifies the asset.           |
| `properties` | [`IMenuProperties`](#IMenuProperties) | Yes      | Specifies the menu properties. |
#### Return value
Returns a reference to the `Menu` instance to allow chaining.

---
### 🔧 `openAtPosition` {#openAtPosition}
Opens the menu at the specified position.
#### Signature
```ts
openAtPosition(options: MenuOption[], x: number, y: number, properties?: IMenuProperties): this
```
#### Parameters
| Name         | Type                                  | Optional | Description                        |
|:-------------|:--------------------------------------|:---------|:-----------------------------------|
| `options`    | [`MenuOption[]`](MenuOption.mdx)      | No       | Specifies the menu options.        |
| `x`          | number                                | No       | Specifies the horizontal position. |
| `y`          | number                                | No       | Specifies the vertical position.   |
| `properties` | [`IMenuProperties`](#IMenuProperties) | Yes      | Specifies the menu properties.     |
#### Return value
Returns a reference to the `Menu` instance to allow chaining.

## 📢 Events {#events}

---
### 🔔 `onClose` {#onClose}
Fired when the menu is closed.
#### Signature
```ts
(menu: Menu) => void
```
#### Parameters
| Name   | Type   | Optional | Description            |
|:-------|:-------|:---------|:-----------------------|
| `menu` | `Menu` | No       | Reference to the menu. |

---
### 🔔 `onClosed` {#onClosed}
Fired after the menu is closed and the item tap event is invoked.
#### Signature
```ts
(menu: Menu) => void
```
#### Parameters
| Name   | Type   | Optional | Description            |
|:-------|:-------|:---------|:-----------------------|
| `menu` | `Menu` | No       | Reference to the menu. |

---
### 🔔 `onClosing` {#onClosing}
Fired when the menu is closing.
#### Signature
```ts
(menu: Menu) => void
```
#### Parameters
| Name   | Type   | Optional | Description            |
|:-------|:-------|:---------|:-----------------------|
| `menu` | `Menu` | No       | Reference to the menu. |

---
### 🔔 `onDestroy` {#onDestroy}
Fired when the menu is destroyed.
#### Signature
```ts
(menu: Menu) => void
```
#### Parameters
| Name   | Type   | Optional | Description            |
|:-------|:-------|:---------|:-----------------------|
| `menu` | `Menu` | No       | Reference to the menu. |

---
### 🔔 `onOpen` {#onOpen}
Fired when the menu is opened.
#### Signature
```ts
(menu: Menu) => void
```
#### Parameters
| Name   | Type   | Optional | Description            |
|:-------|:-------|:---------|:-----------------------|
| `menu` | `Menu` | No       | Reference to the menu. |

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `IMenuProperties` {#IMenuProperties}
Describes the menu properties.
#### Type declaration {#IMenuProperties-type}
```ts
interface IMenuProperties {
  /**
   * Specifies the stylesheet to write the menu styles to. If omitted
   * the default application global stylesheet will be used.
   */
  stylesheet?: Stylesheet;

  /* Specifies the menu style. */
  style?: IMenuStyle;

  /* Specifies the parent asset. If omitted `Menu.Parent` will be used. */
  parent?: Asset;

  /* Specifies if the overlay should be activated (default is `false`). */
  overlay?: boolean;

  /* Specifies if the menu should capture all controller events as long as the menu is active (default is `false`). */
  capture?: boolean;

  /* Specifies if the menu is modal. A modal menu won't close when it loses focus (default is `false`). */
  modal?: boolean;

  /* Specifies the minimum menu width. */
  minWidth?: number;

  /* Specifies the maximum menu width. */
  maxWidth?: number;

  /* Specifies if the supplied minimum and maximum width also applies for submenus (default is `false`). */
  delegateWidths?: boolean;

  /* Specifies if the menu should be destroyed when closed (default is `true`). */
  destroyOnClose?: boolean;

  /* Specifies the preferred position relative to the display rectangle. */
  position?: TPosition;

  /* Specifies an optional margin. */
  margin?: Spacing;

  /* Specifies if the menu gains focus automatically (default is `true`). */
  autoFocus?: boolean;

  /* Fired when the menu is opened. */
  onOpen?: (menu: Menu) => void;

  /* Fired when the menu is closing. */
  onClosing?: (menu: Menu) => void;

  /* Fired when the menu is closed. */
  onClose?: (menu: Menu) => void;

  /* Fired after the menu is closed and the item tap event is invoked. */
  onClosed?: (menu: Menu) => void;

  /* Fired when the menu is destroyed. */
  onDestroy?: (menu: Menu) => void;
}
```

---
### 🔗 `IMenuStyle` {#IMenuStyle}
Describes the menu style.
#### Type declaration {#IMenuStyle-type}
```ts
interface IMenuStyle {
  /* Specifies the styles for the menu. */
  menu?: {
    /* Styles for the outer menu element. */
    outer?: IStyles;

    /* Styles for the inner menu element. */
    inner?: IStyles;

    /* The menu is closed. */
    open?: IStyles;

    /* The menu is opened. */
    close?: IStyles;

    /* The menu is a submenu. */
    submenu?: IStyles;

    /* The menu is hovered. */
    hover?: IStyles;

    /* The menu is center aligned. */
    center?: IStyles;

    /* The menu is left/top aligned according to the context. */
    context?: IStyles;

    /* The menu is left aligned. */
    left?: IStyles;

    /* The menu is top aligned. */
    top?: IStyles;

    /* The menu is right aligned. */
    right?: IStyles;

    /* The menu is bottom aligned. */
    bottom?: IStyles;
  };

  /* Specifies the styles for menu options. */
  options?: {
    label?: ILabelStyle;
    separator?: ISeparatorStyle;
    item?: IItemStyle;
    submenu?: ISubmenuStyle;
    image?: IImageStyle;
  };

  /* Specifies the styles for the optional menu scroll indicators. */
  indicators?: {
    /* Scroll up indicator. */
    up: {
      /* Scroll up indicator appearance. */
      appearance: IStyles;

      /* A pointing device hovers the indicator. */
      hover?: IStyles;

      /* The menu is crawling up. */
      crawl?: IStyles;
    };

    /* Scroll down indicator. */
    down: {
      /* Scroll down indicator appearance. */
      appearance: IStyles;

      /* A pointing device hovers the indicator. */
      hover?: IStyles;

      /* The menu is crawling down. */
      crawl?: IStyles;
    };
  };

  /* Defines the styles for the overlay. */
  overlay?: {
    /* Overlay appearance. */
    appearance?: IStyles;

    /* The menu is opened. */
    open?: IStyles;

    /* The menu is closed. */
    close?: IStyles;
  };

  /* Specifies if a `transform-origin` should be calculated for the menu class. Only applies to menus which have one of the following positions: `left`, `right`, `top` or `bottom`. */
  origin?: boolean;

  /* Specifies if scrollbars should be displayed and describes their styles. */
  scrollbars?: IScrollbarsStyle;

  /* Specifies if the scrollbars are interactable and can be used for scrolling (default is `false`). */
  scrollbarsInteractable?: boolean;

  /* Specifies the minimum menu width. */
  minWidth?: number;

  /* Specifies the maximum menu width. */
  maxWidth?: number;

  /* Specifies if the supplied minimum and maximum width also applies for submenus (default is `false`). */
  delegateWidths?: boolean;
}
```
