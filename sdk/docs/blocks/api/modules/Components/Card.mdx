---
title: Component Card class - Blocks
sidebar_label: Card
description: The Card class is an abstract class for creating feature cards for the Tripetto builder (builder package).
---

# Card class

## 📖 Description {#description}
The `Card` class is an [abstract class](https://www.typescriptlang.org/docs/handbook/2/classes.html#abstract-classes-and-members) for creating feature cards for the Tripetto builder. Feature cards are used to define (groups of) UI elements that control a certain feature. For example, the [`Forms.Form`](../Forms/Form.mdx) card is used to define a collection of input controls.

---
## 🆕 `constructor` {#constructor}
Creates a new `Card` instance.
#### Signature
```ts
constructor(
  modes?: Mode
): Card
```
#### Parameters
| Name    | Type            | Optional | Description                                                     |
|:--------|:----------------|:---------|:----------------------------------------------------------------|
| `modes` | [`Mode`](#Mode) | Yes      | Specifies the supported modes for the card (default is `Both`). |
#### Return value
Returns a reference to the new `Card` instance.

## 🗃️ Fields {#fields}

---
### 🏷️ `context` {#context}
Retrieves a reference to the card context.
#### Type {#signature}
Element | undefined

---
### 🏷️ `element` {#element}
Retrieves a reference to the card element.
#### Type {#signature}
Element | undefined

---
### 🏷️ `feature` {#feature}
Sets or retrieves a reference to the feature.
#### Type {#signature}
[`Feature`](Feature.mdx) | undefined

---
### 🏷️ `isActivated` {#isActivated}
Retrieves if the card is activated.
#### Type {#signature}
boolean

---
### 🏷️ `isAwaiting` {#isAwaiting}
Retrieves if the card validation is awaiting.
#### Type {#signature}
boolean

---
### 🏷️ `isDrawn` {#isDrawn}
Retrieves if the card is drawn.
#### Type {#signature}
boolean

---
### 🏷️ `isEmpty` {#isEmpty}
Sets or retrieves if the card is empty (has no visible content).
#### Type {#signature}
boolean

---
### 🏷️ `isFailed` {#isFailed}
Retrieves if the card validation has failed.
#### Type {#signature}
boolean

---
### 🏷️ `isFirstObservable` {#isFirstObservable}
Retrieves if this card is the first observable card.
#### Type {#signature}
boolean

---
### 🏷️ `isInitialized` {#isInitialized}
Retrieves if the card is initialized.
#### Type {#signature}
boolean

---
### 🏷️ `isInvalid` {#isInvalid}
Retrieves if the card validation is invalid.
#### Type {#signature}
boolean

---
### 🏷️ `isLastObservable` {#isLastObservable}
Retrieves if this card is the last observable card.
#### Type {#signature}
boolean

---
### 🏷️ `isMeasurable` {#isMeasurable}
Retrieves if the card is measurable.
#### Type {#signature}
boolean

---
### 🏷️ `isMeasured` {#isMeasured}
Retrieves if the card is measured.
#### Type {#signature}
boolean

---
### 🏷️ `isModeCompact` {#isModeCompact}
Retrieves if the compact mode is active.
#### Type {#signature}
boolean

---
### 🏷️ `isModeNormal` {#isModeNormal}
Retrieves if the normal mode is active.
#### Type {#signature}
boolean

---
### 🏷️ `isObservable` {#isObservable}
Retrieves if the card is observable.
#### Type {#signature}
boolean

---
### 🏷️ `isPassed` {#isPassed}
Retrieves if the card validation has passed.
#### Type {#signature}
boolean

---
### 🏷️ `isReady` {#isReady}
Retrieves if the card is ready.
#### Type {#signature}
boolean

---
### 🏷️ `isVisible` {#isVisible}
Sets or retrieves if the card is visible.
#### Type {#signature}
boolean

---
### 🏷️ `mode` {#mode}
Retrieves the mode.
#### Type {#signature}
[`Mode`](#Mode)

---
### 🏷️ `modes` {#modes}
Retrieves the supported modes for the card.
#### Type {#signature}
[`Mode`](#Mode)

---
### 🏷️ `position` {#position}
Returns the position of the card.
#### Type {#signature}
number

---
### 🏷️ `stylesheet` {#stylesheet}
Retrieves a reference to the stylesheet.
#### Type {#signature}
Stylesheet

---
### 🏷️ `tail` {#tail}
Returns the tail for the card.
#### Type {#signature}
number

---
### 🏷️ `validation` {#validation}
Retrieves the validation state of the card.
#### Type {#signature}
"unknown" | "fail" | "invalid" | "pass" | "await"

## ▶️ Methods {#methods}

---
### 🔧 `deactivate` {#deactivate}
Deactivates the card.
#### Signature
```ts
deactivate(): this
```
#### Return value
Returns a reference to the `Card` instance to allow chaining.

---
### 🔧 `destroy` {#destroy}
Destroys the card.
#### Signature
```ts
destroy(): void
```

---
### 🔧 `focus` {#focus}
Captures focus to the card.
#### Signature
```ts
focus(to?: "first" | "last"): boolean
```
#### Parameters
| Name | Type              | Optional | Description                                                          |
|:-----|:------------------|:---------|:---------------------------------------------------------------------|
| `to` | "first" \| "last" | Yes      | Specifies if the focus is set to the first or last item in the card. |
#### Return value
Returns `true` if the focus is captured.

---
### 🔧 `hide` {#hide}
Hides the card.
#### Signature
```ts
hide(): this
```
#### Return value
Returns a reference to the `Card` instance to allow chaining.

---
### 🔧 `initialized` {#initialized}
Marks the card as initialized.
#### Signature
```ts
initialized(): void
```

---
### 🔧 `measure` {#measure}
Measures the size of the card.
#### Signature
```ts
measure(size?: number): boolean
```
#### Parameters
| Name   | Type   | Optional | Description         |
|:-------|:-------|:---------|:--------------------|
| `size` | number | Yes      | Specifies the size. |
#### Return value
Returns `true` if the size is changed.

---
### 🔧 `measured` {#measured}
Card is measured.
#### Signature
```ts
measured(): void
```

---
### 🔧 `onActivate` {#onActivate}
Invoked when the card is activated.
#### Signature
```ts
onActivate(): void
```

---
### 🔧 `onDeactivate` {#onDeactivate}
Invoked when the card is deactivated.
#### Signature
```ts
onDeactivate(): void
```

---
### 🔧 `onDestroy` {#onDestroy}
Invoked when the card is destroyed.
#### Signature
```ts
onDestroy(): void
```

---
### 🔧 `onDraw` {#onDraw}
Draws the card.
#### Signature
```ts
onDraw(context: Element): void
```
#### Parameters
| Name      | Type    | Optional | Description                          |
|:----------|:--------|:---------|:-------------------------------------|
| `context` | Element | No       | Reference to the element to draw to. |

---
### 🔧 `onHide` {#onHide}
Invoked when the card is hidden.
#### Signature
```ts
onHide(): void
```

---
### 🔧 `onInit` {#onInit}
Invoked when the card is initialized.
#### Signature
```ts
onInit(): void
```
#### Return value
Returns the initialization state. If `true` is returned this indicates the card is initialized. If you return `false` the system waits for a call to the `Initialized` method.

---
### 🔧 `onMeasure` {#onMeasure}
Invoked when the card is measured. A measure correction in pixels can be supplied in the return value.
#### Signature
```ts
onMeasure(): number
```
#### Return value
Returns the measure correction for the card.

---
### 🔧 `onMode` {#onMode}
Invoked when the mode of the card is changed.
#### Signature
```ts
onMode(): void
```

---
### 🔧 `onPop` {#onPop}
Invoked when a card is popped.
#### Signature
```ts
onPop(): void
```

---
### 🔧 `onReady` {#onReady}
Invoked when the card is ready.
#### Signature
```ts
onReady(): void
```

---
### 🔧 `onRequestAutoFocus` {#onRequestAutoFocus}
Invoked when the card receives an auto-focus request.
#### Signature
```ts
onRequestAutoFocus(): void
```
#### Return value
Returns `true` if the card accepted the auto-focus request.

---
### 🔧 `onRequestFocus` {#onRequestFocus}
Invoked when the card receives a focus request.
#### Signature
```ts
onRequestFocus(): void
```
#### Return value
Returns `true` if the card accepted the focus request.

---
### 🔧 `onResize` {#onResize}
 Invoked when the card layer is resized.
#### Signature
```ts
onResize(): void
```

---
### 🔧 `onResized` {#onResized}
Invoked when the card itself is resized.
#### Signature
```ts
onResized(): void
```

---
### 🔧 `onShow` {#onShow}
Invoked when the card is shown.
#### Signature
```ts
onShow(): void
```

---
### 🔧 `onValidate` {#onValidate}
Invoked when the card needs to be validated.
#### Signature
```ts
onValidate(callback: Callback): "unknown" | "fail" | "invalid" | "pass" | "await" | Callback
```
#### Parameters
| Name       | Type     | Optional | Description                                                                                                 |
|:-----------|:---------|:---------|:------------------------------------------------------------------------------------------------------------|
| `callback` | Callback | No       | Reference to the callback. Pass this object as return value to enable the asynchronous validation callback. |
#### Return value
Returns the validation state.

---
### 🔧 `onValidated` {#onValidated}
Invoked when the card is validated.
#### Signature
```ts
onValidated(): void
```

---
### 🔧 `ready` {#ready}
Marks the card as ready.
#### Signature
```ts
ready(): void
```

---
### 🔧 `resize` {#resize}
Resizes the card.
#### Signature
```ts
resize(): void
```

---
### 🔧 `scrollIntoView` {#scrollIntoView}
Scrolls the card (or an specific element of the card) into view.
#### Signature
```ts
scrollIntoView(element?: Element, onlyWhenInvisible?: boolean): this
```
#### Parameters
| Name                | Type    | Optional | Description                                                                   |
|:--------------------|:--------|:---------|:------------------------------------------------------------------------------|
| `element`           | Element | Yes      | Specifies an optional element of the card to scroll into view.                |
| `onlyWhenInvisible` | boolean | Yes      | Specifies if the scroll into view is only done when the element is invisible. |
#### Return value
Returns a reference to the `Card` instance to allow chaining.

---
### 🔧 `show` {#show}
Shows the card.
#### Signature
```ts
show(): this
```
#### Return value
Returns a reference to the `Card` instance to allow chaining.

---
### 🔧 `validate` {#validate}
Validates the card.
#### Signature
```ts
validate(): this
```
#### Return value
Returns a reference to the `Card` instance to allow chaining.

---
### 🔧 `visible` {#visible}
Shows or hides the card.
#### Signature
```ts
visible(visible: boolean, scrollIntoView?: boolean): this
```
#### Parameters
| Name             | Type    | Optional | Description                                    |
|:-----------------|:--------|:---------|:-----------------------------------------------|
| `visible`        | boolean | No       | Specifies if the card is visible.              |
| `scrollIntoView` | boolean | Yes      | Specifies if the card should scroll into view. |
#### Return value
Returns a reference to the `Card` instance to allow chaining.

## 📇 Enums {#enums}

---
### 🔗 `Mode` {#Mode}
Describes the enum for the modes.
#### Type declaration
```ts
enum Mode {
  /* Card can only be used in normal mode. */
  Normal = 1,

  /* Card can only be used in compact mode. */
  Compact = 2,

  /* Card can be used in both modes. */
  Both = 3,
}
```
