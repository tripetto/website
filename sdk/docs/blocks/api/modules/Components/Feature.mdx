---
title: Component Feature class - Blocks
sidebar_label: Feature
toc_max_heading_level: 4
description: The Feature class defines a feature in a Features collection (builder package).
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# Feature class

## 📖 Description {#description}
The `Feature` class defines a feature in a [`Features`](Features.mdx) collection. Features are used in Tripetto to create panels for managing the properties of elements like blocks. Each feature can control a single [`Card`](Card.mdx).

:::tip
For some examples of built-in features, see the [`EditorOrchestrator`](../../classes/EditorOrchestrator.mdx#methods) class.

See the [Block editor guide](../../../custom/guides/editor.mdx) for more information about editor panels.
:::

---
## 🆕 `constructor` {#constructor}
Creates a new `Feature` instance.
##### Signature
```ts
constructor(features: Features, properties: IFeature): Feature
```
##### Parameters
| Name         | Type                       | Optional | Description                               |
|:-------------|:---------------------------|:---------|:------------------------------------------|
| `features`   | [`Features`](Features.mdx) | No       | Reference to the features collection.     |
| `properties` | [`IFeature`](#IFeature)    | No       | Specifies the properties for the feature. |
##### Return value
Returns a reference to the new `Feature` instance.

## 🗃️ Fields {#fields}

---
### 🏷️ `card` {#card}
Retrieves the card.
##### Type {#signature}
[`Card`](Card.mdx) | undefined

---
### 🏷️ `element` {#element}
Retrieves a reference to the feature element.
##### Type {#signature}
Element | undefined

---
### 🏷️ `features` {#features}
Retrieves a reference to the [`Features`](Features.mdx) collection.
##### Type {#signature}
[`Features`](Features.mdx)

---
### 🏷️ `isActivated` {#isActivated}
Sets or retrieves if the feature is activated.
##### Type {#signature}
boolean

---
### 🏷️ `isAwaiting` {#isAwaiting}
Sets or retrieves if the feature validation is awaiting.
##### Type {#signature}
boolean

---
### 🏷️ `isDisabled` {#isDisabled}
Sets or retrieves if the feature is disabled.
##### Type {#signature}
boolean

---
### 🏷️ `isFailed` {#isFailed}
Sets or retrieves if the feature validation has failed.
##### Type {#signature}
boolean

---
### 🏷️ `isInvalid` {#isInvalid}
Sets or retrieves if the feature validation is invalid.
##### Type {#signature}
boolean

---
### 🏷️ `isLocked` {#isLocked}
Sets or retrieves if the feature is locked.
##### Type {#signature}
boolean

---
### 🏷️ `isPassed` {#isPassed}
Sets or retrieves if the feature validation has passed.
##### Type {#signature}
boolean

---
### 🏷️ `isVisible` {#isVisible}
Sets or retrieves if the feature is visible.
##### Type {#signature}
boolean

---
### 🏷️ `name` {#name}
Sets or retrieves the feature name.
##### Type {#signature}
string

---
### 🏷️ `type` {#type}
Retrieves the feature type.
##### Type {#signature}
"card" | "toggle" | "static"

---
### 🏷️ `validation` {#validation}
Sets or retrieves the validation state of the feature.
##### Type {#signature}
"unknown" | "fail" | "invalid" | "pass" | "await"

## ▶️ Methods {#methods}

---
### 🔧 `activate` {#activate}
Activates the feature.
##### Signature
```ts
activate(): this
```
##### Return value
Returns a reference to the `Feature` instance to allow chaining.

---
### 🔧 `activated` {#activated}
Sets the feature state.
##### Signature
```ts
activated(activated?: boolean): this
```
##### Parameters
| Name        | Type    | Optional | Description                                                |
|:------------|:--------|:---------|:-----------------------------------------------------------|
| `activated` | boolean | Yes      | Specifies if the feature is activated (default is `true`). |
##### Return value
Returns a reference to the `Feature` instance to allow chaining.

---
### 🔧 `deactivate` {#deactivate}
Deactivate the feature.
##### Signature
```ts
deactivate(): this
```
##### Return value
Returns a reference to the `Feature` instance to allow chaining.

---
### 🔧 `disable` {#disable}
Disables the feature.
##### Signature
```ts
disable(): this
```
##### Return value
Returns a reference to the `Feature` instance to allow chaining.

---
### 🔧 `disabled` {#disabled}
Sets the feature disabled state.
##### Signature
```ts
disabled(disabled?: boolean): this
```
##### Parameters
| Name       | Type    | Optional | Description                                               |
|:-----------|:--------|:---------|:----------------------------------------------------------|
| `disabled` | boolean | Yes      | Specifies if the feature is disabled (default is `true`). |
##### Return value
Returns a reference to the `Feature` instance to allow chaining.

---
### 🔧 `enable` {#enable}
Enables the feature.
##### Signature
```ts
enable(): this
```
##### Return value
Returns a reference to the `Feature` instance to allow chaining.

---
### 🔧 `focus` {#focus}
Sets focus to the feature.
##### Signature
```ts
focus(): boolean
```
##### Return value
boolean

---
### 🔧 `hide` {#hide}
Hides the feature.
##### Signature
```ts
hide(): this
```
##### Return value
Returns a reference to the `Feature` instance to allow chaining.

---
### 🔧 `lock` {#lock}
Locks the feature.
##### Signature
```ts
lock(): this
```
##### Return value
Returns a reference to the `Feature` instance to allow chaining.

---
### 🔧 `locked` {#locked}
Sets the feature locking state.
##### Signature
```ts
locked(locked?: boolean): this
```
##### Parameters
| Name     | Type    | Optional | Description                                             |
|:---------|:--------|:---------|:--------------------------------------------------------|
| `locked` | boolean | Yes      | Specifies if the feature is locked (default is `true`). |
##### Return value
Returns a reference to the `Feature` instance to allow chaining.

---
### 🔧 `onDisable` {#onDisable}
Attaches an `OnDisable` function to the feature. This function is invoked when the disabled state of the feature changes.
##### Signature
```ts
onDisable(fn: (feature: Feature) => void, mode?: "synchronous" | "asynchronous"): this
```
##### Parameters
| Name   | Type                            | Optional | Description                                             |
|:-------|:--------------------------------|:---------|:--------------------------------------------------------|
| `fn`   | (feature: `Feature`) => void    | No       | Specifies the callback function.                        |
| `mode` | "synchronous" \| "asynchronous" | Yes      | Specifies the callback mode (default is `synchronous`). |
##### Return value
Returns a reference to the `Feature` instance to allow chaining.

---
### 🔧 `onLock` {#onLock}
Attaches an `OnLock` function to the feature. This function is invoked when the lock state of the feature changes.
##### Signature
```ts
onLock(fn: (feature: Feature) => void, mode?: "synchronous" | "asynchronous"): this
```
##### Parameters
| Name   | Type                            | Optional | Description                                             |
|:-------|:--------------------------------|:---------|:--------------------------------------------------------|
| `fn`   | (feature: `Feature`) => void    | No       | Specifies the callback function.                        |
| `mode` | "synchronous" \| "asynchronous" | Yes      | Specifies the callback mode (default is `synchronous`). |
##### Return value
Returns a reference to the `Feature` instance to allow chaining.

---
### 🔧 `onToggle` {#onToggle}
Attaches an `OnToggle` function to the feature. This function is invoked when the toggle state of the feature changes.
##### Signature
```ts
onToggle(fn: (feature: Feature) => void, mode?: "synchronous" | "asynchronous"): this
```
##### Parameters
| Name   | Type                            | Optional | Description                                             |
|:-------|:--------------------------------|:---------|:--------------------------------------------------------|
| `fn`   | (feature: `Feature`) => void    | No       | Specifies the callback function.                        |
| `mode` | "synchronous" \| "asynchronous" | Yes      | Specifies the callback mode (default is `synchronous`). |
##### Return value
Returns a reference to the `Feature` instance to allow chaining.

---
### 🔧 `onVisible` {#onVisible}
Attaches an `OnVisible` function to the feature. This function is invoked when the visibility of the feature changes.
##### Signature
```ts
onVisible(fn: (feature: Feature) => void, mode?: "synchronous" | "asynchronous"): this
```
##### Parameters
| Name   | Type                            | Optional | Description                                             |
|:-------|:--------------------------------|:---------|:--------------------------------------------------------|
| `fn`   | (feature: `Feature`) => void    | No       | Specifies the callback function.                        |
| `mode` | "synchronous" \| "asynchronous" | Yes      | Specifies the callback mode (default is `synchronous`). |
##### Return value
Returns a reference to the `Feature` instance to allow chaining.

---
### 🔧 `scrollIntoView` {#scrollIntoView}
Scrolls the feature into view.
##### Signature
```ts
scrollIntoView(): void
```

---
### 🔧 `show` {#show}
Shows the feature.
##### Signature
```ts
show(): this
```
##### Return value
Returns a reference to the `Feature` instance to allow chaining.

---
### 🔧 `style` {#style}
Applies styles to the feature.
##### Signature
```ts
style(style: IStyles): this
```
##### Parameters
| Name    | Type      | Optional | Description           |
|:--------|:----------|:---------|:----------------------|
| `style` | `IStyles` | No       | Specifies the styles. |
##### Return value
Returns a reference to the `Feature` instance to allow chaining.

---
### 🔧 `unlock` {#unlock}
Unlocks the feature.
##### Signature
```ts
unlock(): this
```
##### Return value
Returns a reference to the `Feature` instance to allow chaining.

---
### 🔧 `visible` {#visible}
Sets the feature visibility.
##### Signature
```ts
visible(visible?: boolean): this
```
##### Parameters
| Name      | Type    | Optional | Description                                              |
|:----------|:--------|:---------|:---------------------------------------------------------|
| `visible` | boolean | Yes      | Specifies if the feature is visible (default is `true`). |
##### Return value
Returns a reference to the `Feature` instance to allow chaining.

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `IFeature` {#IFeature}
Defines the feature properties.
##### Type declaration {#IFeature-type}
<InterfaceDeclaration prefix="#IFeature-" src={`interface IFeature {
  type: "card" | "toggle" | "static";
  option: boolean;
  name: string;
  card?: Card;
}`} symbols={{
  "Card": "/blocks/api/modules/Components/Card/"
}} />

---
#### 🏷️ `card` {#IFeature-card}
Reference to the [`Card`](Card.mdx) that is controlled by the feature.
##### Type
[`Card`](Card.mdx)

---
#### 🏷️ `name` {#IFeature-name}
Specifies the feature name.
##### Type
string

---
#### 🏷️ `option` {#IFeature-option}
Specifies if the feature is an option.
##### Type
boolean

---
#### 🏷️ `type` {#IFeature-type}
Specifies the feature type.
##### Type
"card" | "toggle" | "static"
