---
title: "@tripetto decorator - Blocks"
sidebar_label: "@tripetto"
toc_max_heading_level: 4
description: The @tripetto decorator is used to register the builder part of node and condition blocks (builder package).
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# @tripetto

The `@tripetto` decorator is used to register the **builder part** of node and condition blocks. It can be used to register the following classes:
- [`NodeBlock`](../classes/NodeBlock.mdx): A block that can be assigned to a node (this can be a visual block or a headless block);
- [`ConditionBlock`](../classes/NodeBlock.mdx): A block that can be assigned to branch conditions.

:::tip
See the [blocks](../../custom/introduction.md) documentation to learn how to develop custom blocks.
:::

#### Decorator type
Class [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#class-decorators)

#### Applies to
- [`NodeBlock`](../classes/NodeBlock.mdx)
- [`ConditionBlock`](../classes/ConditionBlock.mdx)

#### Decorator signature
```ts
@tripetto(properties: INodeBlockDecorator | IConditionBlockDecorator)
```

#### Decorator parameters
| Name         | Type                                                                                                     | Optional | Description                                                                 |
|:-------------|:---------------------------------------------------------------------------------------------------------|:---------|:----------------------------------------------------------------------------|
| `properties` | [`INodeBlockDecorator`](#INodeBlockDecorator) \| [`IConditionBlockDecorator`](#IConditionBlockDecorator) | No       | Specifies the block properties that implicitly determine the type of block. |

#### Example
```ts showLineNumbers
import { tripetto, _, NodeBlock, ConditionBlock } from "@tripetto/builder";

// Register a node block
@tripetto({
  type: "node",
  identifier: "example-block",
  get label() {
    return _("Example");
  },
  icon: /* Icon for the block. */
})
class ExampleBlock extends NodeBlock {
  // Block implementation here
}

// Register a condition block
@tripetto({
  type: "condition",
  identifier: "example-block-condition",
  context: ExampleBlock,
  get label() {
    return _("Condition for the example block");
  },
  icon: /* Icon for the block. */
})
class ExampleCondition extends ConditionBlock {
  // Block implementation here
}
```

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `INodeBlockDecorator` {#INodeBlockDecorator}
Describes the interface for registering a [`NodeBlock`](../classes/NodeBlock.mdx).
##### Type declaration
<InterfaceDeclaration prefix="#INodeBlockDecorator-" src={`interface INodeBlockDecorator {
  type: "node";
  identifier: string;
  label: string;
  icon: SVGImage | string;
  version?: string;
  alias?: string | string[];
  namespace?: string | string[];
  kind?: "ui" | "headless";
}`} symbols={{
  "SVGImage": "/blocks/api/modules/SVGImage/"
}} />

---
#### 🏷️ `alias` {#INodeBlockDecorator-alias}
Specifies optional type aliases (alternative type identifiers for the block).
##### Type
string | string[]

---
#### 🏷️ `icon` {#INodeBlockDecorator-icon}
Specifies the icon image for the block (either an URL or Base64-encoded image data).
:::tip
See the [Block icon guide](../../custom/guides/icon.mdx) for more information about the block icon.
:::
##### Type
[`SVGImage`](../modules/SVGImage.mdx) | string
##### Example
```ts showLineNumbers
@tripetto({
  type: "node",
  identifier: "example-block",
  label: "Example block",
  //highlight-start
  icon: "data:image/svg+xml;base64,PHN2ZyAvPg=="
  //highlight-end
})
```
---
#### 🏷️ `identifier` {#INodeBlockDecorator-identifier}
Specifies the unique type identifier for the block.
##### Type
string

---
#### 🏷️ `kind` {#INodeBlockDecorator-kind}
Specifies the kind of block. Can be one of the following values:
- `ui`: Specifies the block needs a user interface in the runner (this is the default kind);
- `headless`: Specifies the block is headless (doesn't need a user interface).
##### Type
"ui" | "headless"

---
#### 🏷️ `label` {#INodeBlockDecorator-label}
Specifies the localized block label (if you use `gettext`, return the localized string using a getter function).
##### Type
string
##### Example
```ts showLineNumbers
import { _, tripetto } from "@tripetto/builder";

@tripetto({
  get label() {
    return _("Example");
  },
})
```

---
#### 🏷️ `namespace` {#INodeBlockDecorator-namespace}
Specifies the namespace(s) for the block.
##### Type
string | string[]

---
#### 🏷️ `type` {#INodeBlockDecorator-type}
Specifies to register a node block.
##### Type
"node"

---
#### 🏷️ `version` {#INodeBlockDecorator-version}
Specifies the version of the block in SemVer format (defaults to `0.0.0`).
##### Type
"node"

---
### 🔗 `IConditionBlockDecorator` {#IConditionBlockDecorator}
Describes the interface for registering a [`ConditionBlock`](../classes/ConditionBlock.mdx).
##### Type declaration
<InterfaceDeclaration prefix="#IConditionBlockDecorator-" src={`interface IConditionBlockDecorator {
  type: "condition";
  identifier: string;
  context: "*" | "section" | "branch" | "node" | string | typeof NodeBlock
  label: string;
  icon: SVGImage | string;
  version?: string;
  alias?: string | string[];
  namespace?: string | string[];
  autoOpen?: boolean;
}`} symbols={{
  "NodeBlock": "/blocks/api/classes/NodeBlock/",
  "SVGImage": "/blocks/api/modules/SVGImage/"
}} />

---
#### 🏷️ `alias` {#IConditionBlockDecorator-alias}
Specifies optional type aliases (alternative type identifiers for the block).
##### Type
string | string[]

---
#### 🏷️ `autoOpen` {#IConditionBlockDecorator-autoOpen}
Specifies if the condition editor panel in the builder should automatically open when the condition is added to the form.
##### Type
boolean

---
#### 🏷️ `context` {#IConditionBlockDecorator-context}
Specifies the block context. Can be one of the following values:
- `*`: The block defines a global condition (it can be used anywhere and it is not dependent on a section, branch, node or block type);
- `section`: The block defines a condition that applies to a section;
- `branch`: The block defines a condition that applies to a branch;
- `node`: The block defines a condition that applies to a node;
- `typeof NodeBlock`: Specifies the [`NodeBlock`](../classes/NodeBlock.mdx) type to which the condition applies;
- The type string identifier of the [`NodeBlock`](../classes/NodeBlock.mdx) type to which the condition applies.
##### Type
"*" | "section" | "branch" | "node" | string | `typeof NodeBlock`

---
#### 🏷️ `icon` {#IConditionBlockDecorator-icon}
Specifies the icon image for the block (either an URL or Base64-encoded image data).
:::tip
See the [Block icon guide](../../custom/guides/icon.mdx) for more information about the block icon.
:::
##### Type
[`SVGImage`](../modules/SVGImage.mdx) | string
##### Example
```ts showLineNumbers
@tripetto({
  type: "condition",
  identifier: "example-block",
  context: "*",
  label: "Example block",
  //highlight-start
  icon: "data:image/svg+xml;base64,PHN2ZyAvPg=="
  //highlight-end
})
```

---
#### 🏷️ `identifier` {#IConditionBlockDecorator-identifier}
Specifies the unique type identifier for the block.
##### Type
string

---
#### 🏷️ `label` {#IConditionBlockDecorator-label}
Specifies the localized block label (if you use `gettext`, return the localized string using a getter function).
##### Type
string
##### Example
```ts showLineNumbers
import { _, tripetto } from "@tripetto/builder";

@tripetto({
  get label() {
    return _("Example");
  },
})
```

---
#### 🏷️ `namespace` {#IConditionBlockDecorator-namespace}
Specifies the namespace(s) for the block.
##### Type
string | string[]

---
#### 🏷️ `type` {#IConditionBlockDecorator-type}
Specifies to register a condition block.
##### Type
"condition"

---
#### 🏷️ `version` {#IConditionBlockDecorator-version}
Specifies the version of the block in SemVer format (defaults to `0.0.0`).
##### Type
string
