---
title: "@collection decorator - Blocks"
sidebar_label: "@collection"
description: The @collection decorator can be used in a ConditionBlock to mark a property that is a reference to a Collection.Item of a Collection.Provider.
---

# @collection

The `@collection` decorator can be used in a [`ConditionBlock`](../classes/ConditionBlock.mdx) to mark a property that is a reference to a [`Collection.Item`](../modules/Collection/Item.mdx) of a [`Collection.Provider`](../modules/Collection/Provider.mdx) that is defined in a related [`NodeBlock`](../classes/NodeBlock.mdx). For example, let's say you have a dropdown block that has a collection of dropdown options and a condition block that verifies if a certain option from that dropdown is selected. The option you want to verify in the [`ConditionBlock`](../classes/ConditionBlock.mdx) originates from the dropdown [`Collection.Provider`](../modules/Collection/Provider.mdx) in the [`NodeBlock`](../classes/NodeBlock.mdx). This decorator helps to specify this connection.

:::info
When this decorator is applied to a property, the property automatically becomes part of the [form definition](../../../builder/api/interfaces/IDefinition.mdx). This means you don't need to apply the [`@definition`](definition.mdx) decorator that is normally used to indicate that a property is part of the form definition.
:::

#### Decorator type
Property [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#property-decorators)

#### Applies to
- [`ConditionBlock`](../classes/ConditionBlock.mdx)

#### Decorator signature
```ts
@collection(reference: string, mode?: "rw" | "r" | "w")
```

#### Decorator parameters
| Name        | Type               | Optional | Description                                                                                                                                                                                                                                                                                                                                                                 |
|:------------|:-------------------|:---------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `reference` | string             | No       | Specifies the property name of the [`Collection.Provider`](../modules/Collection/Provider.mdx) in the [`NodeBlock`](../classes/NodeBlock.mdx) prefixed with a `#`                                                                                                                                                                                                           |
| `mode`      | "rw" \| "r" \| "w" | Yes      | Specifies the operation mode for the property in the [form definition](../../../builder/api/interfaces/IDefinition.mdx). It can be one of the following values:<br/>- `rw`: Reads and writes the property from/to the definition (this is the default mode);<br/>- `r`: Only reads the property from the definition;<br/>- `w`: Only writes the property to the definition. |

#### Example
```ts showLineNumbers
import { tripetto, affects, collection, ConditionBlock } from "@tripetto/builder";

@tripetto({
  type: "condition",
  identifier: "dropdown-condition",
  context: DropdownBlock,
  label: "Dropdown condition",
  icon: "data:image/svg+xml;base64,PHN2ZyAvPg=="
})
class DropdownConditionBlock extends ConditionBlock {
  //highlight-start
  // In this example there is a `NodeBlock` with the name `DropdownBlock`. It has
  // a property `options` which is a collection of `DropdownOption` instances.
  @collection("#options")
  @affects("#name")
  option: DropdownOption | undefined;

  get name() {
    return this.option ? this.option.name : "Nothing selected";
  }
  //highlight-end
}
```
