---
title: "@supplies decorator - Blocks"
sidebar_label: "@supplies"
description: The @supplies decorator is used to mark a Collection.Provider property of a NodeBlock as the list of possible values for a certain slot.
---

# @supplies

The `@supplies` decorator is used to mark a [`Collection.Provider`](../modules/Collection/Provider.mdx) property of a [`NodeBlock`](../classes/NodeBlock.mdx) as the list of possible values for a certain slot. For example, let's say you have a dropdown block with a collection that contains all the possible dropdown options and a single slot that holds the selected option for the dropdown. This decorator tells Tripetto the collection of options is the supply of possible values for the slot.

:::info
The `@supplies` decorator helps the builder to understand the possible values that a certain slot can contain. Some blocks, like the [`calculator block`](../../stock/calculator.mdx) make use of that to optimize the user experience.
:::

#### Decorator type
Property [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#property-decorators)

#### Applies to
- [`NodeBlock`](../classes/NodeBlock.mdx)

#### Decorator signature
```ts
@supplies(
  type: "#slots",
  reference: string,
  sole?: boolean | string,
  condition?: string
)
```

#### Decorator parameters
| Name        | Type              | Optional | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
|:------------|:------------------|:---------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `type`      | "#slots"          | No       | Specifies this is a supply for a slot (that's currently the only supported type).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| `reference` | string            | No       | Specifies the reference of the slot. You can use a wildcard (using an asterisk) to reference multiple slots (for example, `slot-*`).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| `sole`      | boolean \| string | Yes      | Specifies if the collection is the sole values supplier (`true` by default). For example, if your slot is used for a fixed dropdown list and the dropdown options are supplied by the collection, that collection is the sole supplier of values for the slot. If your slot is used for a text field with autocomplete suggestions and the collection is the supplier for the possible suggestions, the collection is not a sole supplier, because the user can also specify another value that is not in the collection list. It is also possible to supply the property name of a boolean value of the node block to dynamically specify if the supply is the sole supplier or not. |
| `condition` | string            | Yes      | Specifies the property name of a boolean value of the node block that specifies if the supply is enabled or not.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |

#### Example
```ts showLineNumbers
import { tripetto, supplies, slots, Collection, NodeBlock } from "@tripetto/builder";

@tripetto({
  type: "node",
  identifier: "dropdown",
  label: "Dropdown",
  icon: "data:image/svg+xml;base64,PHN2ZyAvPg=="
})
class Dropdown extends NodeBlock {
  //highlight-start
  // Let's bind the options as a value supply to the slot with reference `option`
  // The builder now understands that the possible values of the slot with
  // reference `option` comes from the collection of dropdown options.
  @supplies<Dropdown>("#slot", "option")
  options = Collection.of<DropdownOption, Dropdown>(DropdownOption, this);

  @slots
  onSlots(): void {
    this.slots.static({
      type: Slots.String,
      reference: "option",
      label: "Selected option"
    });
  }
  //highlight-end
}
```
