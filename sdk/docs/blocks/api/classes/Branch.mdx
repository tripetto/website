---
title: Branch class - Blocks
sidebar_label: Branch
description: The Branch class represents a branch of the form definition in the builder (builder package).
---

# Branch class

## 📖 Description {#description}
The `Branch` class represents a branch of the [form definition](../../../builder/api/interfaces/IDefinition.mdx) in the builder.

## 🗃️ Fields {#fields}

---
### 🏷️ `conditions` {#conditions}
Retrieves the collection of conditions.
#### Type
[`Conditions`](Conditions.mdx)

---
### 🏷️ `culling` {#culling}
Specifies the culling mode of the branch. This culling mode defines when the branch is taken. It can be one of the following values:
- `first`: Branch is taken when one or more of the conditions match;
- `each`: Branch is taken for each condition that matches (this allows a single branch to be taken multiple times);
- `all`: Branch is taken when all of the conditions match;
- `none`: Branch is taken when none of the conditions match.
#### Type
"first" | "each" | "all" | "none"

:::info
When a branch doesn't contain any conditions it will only be taken when the culling mode is `first` or `all`.
:::

---
### 🏷️ `epilogue` {#epilogue}
Specifies the epilogue (closing message) for the branch when the [`terminator`](#terminator) mode is set to `epilogue`.
#### Type
[`IEpilogue`](../../../builder/api/interfaces/IEpilogue.mdx) | undefined

---
### 🏷️ `id` {#id}
Retrieves the identifier of the branch.
#### Type
string

---
### 🏷️ `index` {#index}
Retrieves the zero-based index of the branch in the [`Branches`](Branches.mdx) collection.
#### Type
number

---
### 🏷️ `isFirst` {#isFirst}
Retrieves if the branch is the first branch in the [`Branches`](Branches.mdx) collection.
#### Type
boolean

---
### 🏷️ `isLast` {#isLast}
Retrieves if the branch is the last branch in the [`Branches`](Branches.mdx) collection.
#### Type
boolean

---
### 🏷️ `isMoving` {#isMoving}
Retrieves if the branch is being moved (dragged) in the builder.
#### Type
boolean

---
### 🏷️ `isRendered` {#isRendered}
Retrieves if the branch is rendered by the builder.
#### Type
boolean

---
### 🏷️ `jump` {#jump}
Specifies the section to jump to when the branch ends.
:::tip
The [`terminator`](#terminator) mode is automatically updated to `jump` when a section is set. When `undefined` is supplied, the [`terminator`](#terminator) mode is automatically set to `continuous` when it was previously set to `jump`.
:::
#### Type
[`Section`](Section.mdx) | undefined

---
### 🏷️ `name` {#name}
Sets or retrieves the name of the branch.
#### Type
string

---
### 🏷️ `parent` {#parent}
Retrieves a reference to parent [`Branches`](Branches.mdx) collection.
#### Type
[`Branches`](Branches.mdx) | undefined

---
### 🏷️ `predecessor` {#predecessor}
Retrieves a reference to the predecessor `Branch` (if any).
#### Type
`Branch` | undefined

---
### 🏷️ `section` {#section}
Retrieves the parent section.
#### Type
[`Section`](Section.mdx) | undefined

---
### 🏷️ `sections` {#sections}
Retrieves the collection of child sections.
#### Type
[`Sections`](Sections.mdx)

---
### 🏷️ `successor` {#successor}
Retrieves a reference to the successor `Branch` (if any).
#### Type
`Branch` | undefined

---
### 🏷️ `terminator` {#terminator}
Specifies the termination mode of the branch. This termination mode defines what happens when the branch ends. It can be one of the following values:
- `continuous`: When the branch ends, the form continues with the next subsequent branch or section;
- `jump`: When the branch ends a jump is made to another section defined by [`jump`](#jump);
- `return`: When the branch ends, the return to the parent form (only available for subforms);
- `abort`: When the branch ends, the whole form ends;
- `epilogue`: When the branch ends the form ends with a specific epilogue (form closing message).
#### Type
"continuous"| "jump" | "return" | "abort" | "epilogue"

## ▶️ Methods {#methods}

---
### 🔧 `delete` {#delete}
Deletes a branch from the [`Branches`](Branches.mdx) collection.
#### Signature
```ts
delete(): this
```
#### Return value
Returns a reference to the deleted `Branch`.

---
### 🔧 `duplicate` {#duplicate}
Duplicates the branch.
#### Signature
```ts
duplicate(dest?: Branch): Branch
```
#### Parameters
| Name   | Type     | Optional | Description                                                                                                                                                                                           |
|:-------|:---------|:---------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `dest` | `Branch` | Yes      | Specifies the destination branch that is used to duplicate to. All current content of that branch is removed and replaced by the branch content that is duplicated. If omitted a new branch is added. |
#### Return value
Returns a reference to the duplicated `Branch`.

---
### 🔧 `insertAfter` {#insertAfter}
Inserts a new branch after this branch.
#### Signature
```ts
insertAfter(): Branch
```
#### Return value
Returns a reference to the new `Branch`.

---
### 🔧 `insertBefore` {#insertBefore}
Inserts a new branch before this branch.
#### Signature
```ts
insertBefore(): Branch
```
#### Return value
Returns a reference to the new `Branch`.

---
### 🔧 `load` {#load}
Loads a nested branch (subform).
:::info
You can only load nested branches (subforms) to branches that have a parent section of [type](Section.mdx#type) `nest`.
:::
#### Signature
```ts
load(
  definition: IDefinition,
  reference?: string,
  version?: string,
  alias?: string,
  readonly?: boolean
): boolean
```
#### Parameters
| Name         | Type                                                             | Optional | Description                                  |
|:-------------|:-----------------------------------------------------------------|:---------|:---------------------------------------------|
| `definition` | [`IDefinition`](../../../builder/api/interfaces/IDefinition.mdx) | No       | Specifies the definition to load as subform. |
| `reference`  | string                                                           | Yes      | Specifies an optional reference to store.    |
| `version`    | string                                                           | Yes      | Specifies an optional version identifier.    |
| `alias`      | string                                                           | Yes      | Specifies an optional alias for the subform. |
| `readonly`   | boolean                                                          | Yes      | Specifies if the subform is read-only.       |
#### Return value
Returns `true` if the subform was loaded succesfully.
:::tip
See the [Subforms guide](../../../builder/integrate/guides/subforms.mdx) for more information.
:::

---
### 🔧 `moveToIndex` {#moveToIndex}
Moves the branch to the specified index.
#### Signature
```ts
moveToIndex(index: number): boolean
```
#### Parameters
| Name    | Type   | Optional | Description                                                                    |
|:--------|:-------|:---------|:-------------------------------------------------------------------------------|
| `index` | number | No       | Specifies the new index position for the branch where the first branch is `0`. |
#### Return value
Returns `true` if the move succeeded.

---
### 🔧 `moveUpOrDown` {#moveUpOrDown}
Moves the branch up or down.
#### Signature
```ts
moveUpOrDown(up: boolean): boolean
```
#### Parameters
| Name | Type    | Optional | Description                                                       |
|:-----|:--------|:---------|:------------------------------------------------------------------|
| `up` | boolean | No       | Specifies if the move direction is up (`true`) or down (`false`). |
#### Return value
Returns `true` if the move succeeded.

---
### 🔧 `rerender` {#rerender}
Invokes a rerendering of the branch.
#### Signature
```ts
rerender(type: "refresh" | "update"): void
```
#### Parameters
| Name   | Type                  | Optional | Description                                                                                                                                                                |
|:-------|:----------------------|:---------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `type` | "refresh" \| "update" | No       | Specifies the rerender type. It can be one of the following values:<br/>- `refresh`: Refresh the whole rendering (slower);<br/>- `update`: Updates the rendering (faster). |

---
### 🔧 `reset` {#reset}
Resets the branch and removes all content in the branch.
#### Signature
```ts
reset(): void
```

---
### 🔧 `swap` {#swap}
Swaps the branch with another branch.
#### Signature
```ts
swap(with: Branch): boolean
```
#### Parameters
| Name   | Type     | Optional | Description                        |
|:-------|:---------|:---------|:-----------------------------------|
| `with` | `Branch` | No       | Specifies the branch to swap with. |
#### Return value
Returns `true` if the swap succeeded.
