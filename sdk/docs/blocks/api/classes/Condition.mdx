---
title: Condition class - Blocks
sidebar_label: Condition
description: The Condition class represents a condition of the form definition in the builder (builder package).
---

# Condition class

## 📖 Description {#description}
The `Condition` class represents a condition of the [form definition](../../../builder/api/interfaces/IDefinition.mdx) in the builder.

## 🗃️ Fields {#fields}

---
### 🏷️ `block` {#block}
Retrieves the attached [`ConditionBlock`](ConditionBlock.mdx) instance for the condition.
#### Type
[`ConditionBlock`](ConditionBlock.mdx) | undefined

---
### 🏷️ `blockTypeIdentifier` {#blockTypeIdentifier}
Retrieves the block type identifier for the condition.
#### Type
string

---
### 🏷️ `branch` {#branch}
Retrieves the parent branch.
#### Type
[`Branch`](Branch.mdx) | undefined

---
### 🏷️ `disabled` {#disabled}
Sets or retrieves if the condition is disabled.
#### Type
boolean

---
### 🏷️ `hasBlock` {#hasBlock}
Retrieves if the condition has a block attached to it.
#### Type
boolean

---
### 🏷️ `id` {#id}
Retrieves the identifier of the condition.
#### Type
string

---
### 🏷️ `index` {#index}
Retrieves the zero-based index of the condition in the [`Conditions`](Conditions.mdx) collection.
#### Type
number

---
### 🏷️ `isBlockInvalid` {#isBlockInvalid}
Retrieves if the block is invalid. In that case, there is no block implementation found for the specified block type.
#### Type
boolean

---
### 🏷️ `isFirst` {#isFirst}
Retrieves if the condition is the first condition in the [`Conditions`](Conditions.mdx) collection.
#### Type
boolean

---
### 🏷️ `isLast` {#isLast}
Retrieves if the condition is the last condition in the [`Conditions`](Conditions.mdx) collection.
#### Type
boolean

---
### 🏷️ `isMoving` {#isMoving}
Retrieves if the condition is being moved (dragged) in the builder.
#### Type
boolean

---
### 🏷️ `isRendered` {#isRendered}
Retrieves if the condition is rendered by the builder.
#### Type
boolean

---
### 🏷️ `label` {#label}
Retrieves label of the condition (this is often a friendly name for the block type).
#### Type
string

---
### 🏷️ `name` {#name}
Retrieves name of the condition.
#### Type
string

---
### 🏷️ `nameMarkdown` {#nameMarkdown}
Retrieves a [`MarkdownParser`](../modules/Markdown/MarkdownParser.mdx) instance for the name.
#### Type
[`MarkdownParser`](../modules/Markdown/MarkdownParser.mdx)

---
### 🏷️ `parent` {#parent}
Retrieves a reference to parent [`Conditions`](Conditions.mdx) collection.
#### Type
[`Conditions`](Conditions.mdx)

---
### 🏷️ `predecessor` {#predecessor}
Retrieves the predecessor of the condition.
#### Type
`Condition` | undefined

---
### 🏷️ `section` {#section}
Retrieves the parent section.
#### Type
[`Section`](Section.mdx) | undefined

---
### 🏷️ `successor` {#successor}
Retrieves the successor of the condition.
#### Type
`Condition` | undefined

## ▶️ Methods {#methods}

---
### 🔧 `assignBlock` {#assignBlock}
Assigns a block to the condition. If a block type is supplied, a new instance for the specified block is created.
#### Signature
```ts
assignBlock(block: ConditionBlock | typeof ConditionBlock): ConditionBlock | undefined
```
#### Parameters
| Name    | Type                                                                                    | Optional | Description                                                                                                                   |
|:--------|:----------------------------------------------------------------------------------------|:---------|:------------------------------------------------------------------------------------------------------------------------------|
| `block` | [`ConditionBlock`](ConditionBlock.mdx) \| [`typeof ConditionBlock`](ConditionBlock.mdx) | No       | Specifies the block instance or type to attach. When a block type is supplied, a new block instance is automatically created. |
#### Return value
Returns a reference to the [`ConditionBlock`](ConditionBlock.mdx) instance.

---
### 🔧 `delete` {#delete}
Deletes a condition from the [`Conditions`](Conditions.mdx) collection.
#### Signature
```ts
delete(): this
```
#### Return value
Returns a reference to the deleted `Condition`.

---
### 🔧 `disable` {#disable}
Disable the condition.
#### Signature
```ts
disable(): this
```
#### Return value
Returns a reference to itself.

---
### 🔧 `duplicate` {#duplicate}
Duplicates the condition.
#### Signature
```ts
duplicate(dest?: Condition): Condition
```
#### Parameters
| Name   | Type        | Optional | Description                                                                                                                                                                                                       |
|:-------|:------------|:---------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `dest` | `Condition` | Yes      | Specifies the destination condition that is used to duplicate to. All current content of that condition is removed and replaced by the condition content that is duplicated. If omitted a new condition is added. |
#### Return value
Returns a reference to the duplicated `Condition`.

---
### 🔧 `enable` {#enable}
Enable the condition.
#### Signature
```ts
enable(): this
```
#### Return value
Returns a reference to itself.

---
### 🔧 `insertAfter` {#insertAfter}
Inserts a new condition after this condition.
#### Signature
```ts
insertAfter(): Condition
```
#### Return value
Returns a reference to the new `Condition`.

---
### 🔧 `insertBefore` {#insertBefore}
Inserts a new condition before this condition.
#### Signature
```ts
insertBefore(): Condition
```
#### Return value
Returns a reference to the new `Condition`.

---
### 🔧 `moveToIndex` {#moveToIndex}
Moves the condition to the specified index.
#### Signature
```ts
moveToIndex(index: number): boolean
```
#### Parameters
| Name    | Type   | Optional | Description                                                                          |
|:--------|:-------|:---------|:-------------------------------------------------------------------------------------|
| `index` | number | No       | Specifies the new index position for the condition where the first condition is `0`. |
#### Return value
Returns `true` if the move succeeded.

---
### 🔧 `moveUpOrDown` {#moveUpOrDown}
Moves the condition up or down.
#### Signature
```ts
moveUpOrDown(up: boolean): boolean
```
#### Parameters
| Name | Type    | Optional | Description                                                       |
|:-----|:--------|:---------|:------------------------------------------------------------------|
| `up` | boolean | No       | Specifies if the move direction is up (`true`) or down (`false`). |
#### Return value
Returns `true` if the move succeeded.

---
### 🔧 `refresh` {#refresh}
Refreshes a condition so it rerenders in the builder.
#### Signature
```ts
refresh(): this
```
#### Return value
Returns a reference to the condition itself.

---
### 🔧 `rerender` {#rerender}
Invokes a rerendering of the condition.
#### Signature
```ts
rerender(type: "refresh" | "update"): void
```
#### Parameters
| Name   | Type                  | Optional | Description                                                                                                                                                                |
|:-------|:----------------------|:---------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `type` | "refresh" \| "update" | No       | Specifies the rerender type. It can be one of the following values:<br/>- `refresh`: Refresh the whole rendering (slower);<br/>- `update`: Updates the rendering (faster). |

---
### 🔧 `reset` {#reset}
Resets the condition and removes all content in the condition.
#### Signature
```ts
reset(): void
```

---
### 🔧 `swap` {#swap}
Swaps the condition with another condition.
#### Signature
```ts
swap(with: Condition): boolean
```
#### Parameters
| Name   | Type        | Optional | Description                           |
|:-------|:------------|:---------|:--------------------------------------|
| `with` | `Condition` | No       | Specifies the condition to swap with. |
#### Return value
Returns `true` if the swap succeeded.

---
### 🔧 `unassignBlock` {#unassignBlock}
Unassigns a block.
#### Signature
```ts
unassignBlock(): ConditionBlock | undefined
```
#### Return value
Returns the previously assigned [`ConditionBlock`](ConditionBlock.mdx) instance or `undefined` if there was none.
