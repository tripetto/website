---
title: NodeBlock class - Blocks
sidebar_label: NodeBlock
description: The NodeBlock class is an abstract class for creating node blocks for the builder (builder package).
---

# NodeBlock class

## 📖 Description {#description}
The `NodeBlock` class is an [abstract class](https://www.typescriptlang.org/docs/handbook/2/classes.html#abstract-classes-and-members) for creating node blocks for the builder. A node block defines a behavior for a [`Node`](Node.mdx). An instance of a derived `NodeBlock` class is created using the [`assignBlock`](Node.mdx#assignBlock) method of a [`Node`](Node.mdx).

:::tip
Follow [this tutorial](../../custom/implement/visual.mdx) to learn how to build a custom block.
:::

## 🎀 Applicable decorators {#decorators}
The following decorators can be applied in this class:
#### Class decorators [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#class-decorators)
- [`@tripetto`](../decorators/tripetto.mdx)
#### Property decorators [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#property-decorators)
- [`@affects`](../decorators/affects.mdx)
- [`@definition`](../decorators/definition.mdx)
- [`@metadata`](../decorators/metadata.mdx)
- [`@supplies`](../decorators/supplies.mdx)
#### Method decorators [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#method-decorators)
- [`@assigned`](../decorators/assigned.mdx)
- [`@conditions`](../decorators/conditions.mdx)
- [`@created`](../decorators/created.mdx)
- [`@destroyed`](../decorators/destroyed.mdx)
- [`@editor`](../decorators/editor.mdx)
- [`@renamed`](../decorators/renamed.mdx)
- [`@slots`](../decorators/slots.mdx)
- [`@unassigned`](../decorators/unassigned.mdx)
- [`@upgraded`](../decorators/upgraded.mdx)

## 📌 Statics {#statics}

---
### 🏷️ `icon` {#icon}
Contains the icon of the block.
#### Type
[`SVGImage`](../modules/SVGImage.mdx) | string

---
### 🏷️ `identifier` {#identifier}
Contains the type identifier of the block.
#### Type
string

---
### 🏷️ `kind` {#kind}
Contains the kind of block. It can be one of the following values:
- `ui`: Visual block that the user can see and interact with (for example, a text input field);
- `headless`: Invisible block that can perform a certain action.
#### Type
"ui" | "headless"

---
### 🏷️ `label` {#label}
Retrieves the localized label (name) of the block.
#### Type
string

---
### 🏷️ `version` {#version}
Contains the version of the block.
#### Type
string

---
### 🔧 `flag` {#flag}
Verifies if the block has the specified flag enabled.
:::tip
Flags can be set using the [`flag`](../../../builder/api/modules/Namespaces.mdx#flag) function.
:::
#### Signature
```ts
flag(id: string): boolean
```
#### Parameters
| Name | Type   | Optional | Description                              |
|:-----|:-------|:---------|:-----------------------------------------|
| `id` | string | No       | Specifies the flag identifier to verify. |
#### Return value
Returns `true` if the specified flag was enabled.

## 🗃️ Fields {#fields}

---
### 🏷️ `allowMarkdown` {#allowMarkdown}
Specifies if markdown is supported in the label of the block.
:::info
When omitted, basic markdown formatting (bold, italic, underline, strikethrough and hyperlinks) is supported.
:::
#### Type
boolean

---
### 🏷️ `conditions` {#conditions}
Retrieves the [`ConditionsOrchestrator`](ConditionsOrchestrator.mdx) instance used to define conditions for the block.
:::info
You should use the [`@conditions`](../decorators/conditions.mdx) decorator to mark the method that defines the conditions.
:::
#### Type
[`ConditionsOrchestrator`](ConditionsOrchestrator.mdx)

---
### 🏷️ `editor` {#editor}
Retrieves the [`EditorOrchestrator`](EditorOrchestrator.mdx) instance used to define the controls to manage the block.
:::info
You should use the [`@editor`](../decorators/editor.mdx) decorator to mark the method that implements the controls.
:::
#### Type
[`EditorOrchestrator`](EditorOrchestrator.mdx)

---
### 🏷️ `icon` {#icon}
Retrieves the icon of the node block.
#### Type
[`SVGImage`](../modules/SVGImage.mdx) | string

---
### 🏷️ `id` {#id}
Retrieves the identifier of the node.
#### Type
string

---
### 🏷️ `isInitialized` {#isInitialized}
Retrieves if the block is initialized.
#### Type
boolean

---
### 🏷️ `isRequired` {#isRequired}
Retrieves if the block is required or can be skipped.
:::tip
This property is used by the builder to visually indicate if a node is required.
:::
#### Type
boolean

---
### 🏷️ `label` {#label}
Retrieves the label of the node block. This label is shown in the builder.
#### Type
string

---
### 🏷️ `node` {#node}
Retrieves a reference to the [`Node`](Node.mdx) instance that holds the block.
#### Type
[`Node`](Node.mdx)

---
### 🏷️ `slots` {#slots}
Retrieves a reference to the [`Slots`](../modules/Slots/SlotsClass.mdx) collection instance.
#### Type
[`Slots`](../modules/Slots/SlotsClass.mdx)

## ▶️ Methods {#methods}

---
### 🔧 `detectChange` {#detectChange}
Notifies the builder that a change has occurred and the definition has changed.
:::info
Change detection is normally handled automatically. The builder monitors all block properties that are decorated with the [`@definition`](../decorators/definition.mdx) decorator.
:::
#### Signature
```ts
detectChange(): void
```

---
### 🔧 `rerender` {#rerender}
Rerenders the block.
#### Signature
```ts
rerender(force?: boolean): void
```
#### Parameters
| Name    | Type    | Optional | Description                                              |
|:--------|:--------|:---------|:---------------------------------------------------------|
| `force` | boolean | Yes      | Specifies to force the rerendering (default is `false`). |
