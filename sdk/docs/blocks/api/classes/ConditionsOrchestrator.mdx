---
title: ConditionsOrchestrator class - Blocks
sidebar_label: ConditionsOrchestrator
toc_max_heading_level: 4
description: The ConditionsOrchestrator class instance is supplied to the @conditions decorated method of a NodeBlock (builder package).
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# ConditionsOrchestrator class

## 📖 Description
The `ConditionsOrchestrator` class instance is supplied to the [`@conditions`](../decorators/conditions.mdx) decorated method of a [`NodeBlock`](NodeBlock.mdx). It is used to define condition templates for the block. Condition templates can be used to create new conditions. They are listed in the builder and the user can select one of the templates when adding new conditions.

:::tip
The `ConditionsOrchestrator` instance is only available within the method of the [`NodeBlock`](NodeBlock.mdx) marked with the [`@conditions`](../decorators/conditions.mdx) decorator. The instance is supplied as argument to that decorated method or through the [`conditions`](../classes/NodeBlock.mdx#conditions) property of the [`NodeBlock`](../classes/NodeBlock.mdx) instance.
:::

## 🗃️ Fields {#fields}

---
### 🏷️ `templates` {#templates}
Retrieves an array with the available condition templates.
##### Type
([`IConditionTemplate`](#IConditionTemplate) | [`IConditionGroup`](#IConditionGroup))[]

## ▶️ Methods {#methods}

---
### 🔧 `template` {#template}
Creates a new condition template and adds it to the list of templates.
##### Signature
```ts
template(properties: ITemplateProperties): this
```
##### Parameters
| Name         | Type                                          | Optional | Description                        |
|:-------------|:----------------------------------------------|:---------|:-----------------------------------|
| `properties` | [`ITemplateProperties`](#ITemplateProperties) | No       | Specifies the template properties. |
##### Return value
Returns a reference to the `ConditionsOrchestrator` instance.
##### Example
```ts showLineNumbers
import { tripetto, conditions, NodeBlock } from "@tripetto/builder";

@tripetto({
  type: "node",
  identifier: "example-block",
  label: "Example",
  icon: "data:image/svg+xml;base64,PHN2ZyAvPg=="
})
class ExampleBlock extends NodeBlock {
  //highlight-start
  @conditions
  onConditions(): void {
    this.conditions.template({
      condition: ExampleCondition
    });
  }
  //highlight-end
}
```

---
### 🔧 `custom` {#custom}
Adds a custom command to the list of templates. A custom command is execute for a certain [`Condition`](Condition.mdx).
##### Signature
```ts
custom(
  label: string,
  command: (condition: Condition) => void,
  icon?: string,
  markdown: boolean
): this
```
##### Parameters
| Name       | Type                           | Optional | Description                                                                                                             |
|:-----------|:-------------------------------|:---------|:------------------------------------------------------------------------------------------------------------------------|
| `label`    | string                         | No       | Specifies the label for the custom command.                                                                             |
| `command`  | (condition: Condition) => void | No       | Specifies the command to execute for a condition. A reference to the [`Condition`](Condition.mdx) instance is supplied. |
| `icon`     | string                         | Yes      | Specifies the icon for the custom command.                                                                              |
| `markdown` | boolean                        | Yes      | Specifies if markdown is supported in the label (default is `false`).                                                   |
##### Return value
Returns a reference to the `ConditionsOrchestrator` instance.

---
### 🔧 `group` {#group}
Adds a group that can contain [templates](#template) and [custom commands](#custom).
##### Signature
```ts
group(
  label: string,
  icon?: string,
  markdown?: boolean,
  separator?: boolean
): ConditionsOrchestrator
```
##### Parameters
| Name        | Type    | Optional | Description                                                                                |
|:------------|:--------|:---------|:-------------------------------------------------------------------------------------------|
| `label`     | string  | No       | Specifies the label for the group.                                                         |
| `icon`      | string  | Yes      | Specifies the icon for the group.                                                          |
| `markdown`  | boolean | Yes      | Specifies if markdown is supported in the label (default is `false`).                      |
| `separator` | boolean | Yes      | Specifies if the group should be preceded by a separator in the menu (default is `false`). |
##### Return value
Returns a new `ConditionsOrchestrator` instance that can be used to create new [templates](#template) and [custom commands](#custom) for the group.

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `ITemplateProperties` {#ITemplateProperties}
Describes the interface for declaring condition templates.
##### Type declaration
<InterfaceDeclaration prefix="#ITemplateProperties-" src={`interface ITemplateProperties<T> {
  condition: typeof ConditionBlock;
  label?: string;
  markdown?: string;
  icon?: SVGImage | string;
  props?: Partial<Readonly<T>>;
  burst?: boolean | "branch";
  autoOpen?: boolean;
  separator?: boolean;
}`} symbols={{
  "ConditionBlock": "/blocks/api/classes/ConditionBlock/",
  "SVGImage": "/blocks/api/modules/SVGImage/",
  "Partial": "https://www.typescriptlang.org/docs/handbook/utility-types.html#partialtype",
  "Readonly": "https://www.typescriptlang.org/docs/handbook/utility-types.html#readonlytype"
}} />

---
#### 🏷️ `autoOpen` {#ITemplateProperties-autoOpen}
Contains if the condition editor panel should be opened automatically after creation.
##### Type
boolean

---
#### 🏷️ `burst` {#ITemplateProperties-burst}
Contains if the condition allows burst creation. Burst creation allows a user to create a collection of conditions at once. For example, it allows to create a condition for each option in a dropdown block in a single action. It can be one of the following values:
- `false`: Burst creation not supported for this condition;
- `true: Burst creation of conditions in separate branches supported;
- `branch`: Burst creation of conditions in a single branch supported.
##### Type
boolean | "branch"

---
#### 🏷️ `condition` {#ITemplateProperties-condition}
Specifies the condition block the template is for.
##### Type
[`typeof ConditionBlock`](ConditionBlock.mdx)

---
#### 🏷️ `icon` {#ITemplateProperties-icon}
Specifies the icon for the condition.
##### Type
[`SVGImage`](../modules/SVGImage.mdx) | string

---
#### 🏷️ `label` {#ITemplateProperties-label}
Specifies the label for the condition.
##### Type
string

---
#### 🏷️ `markdown` {#ITemplateProperties-markdown}
Specifies a label for the condition that has markdown in it (overrules the [`label`](#ITemplateProperties-label) property).
##### Type
string

---
#### 🏷️ `props` {#ITemplateProperties-props}
Specifies the properties to set on the condition. All properties implemented in the condition block are allowed.
##### Type
`{}`

---
#### 🏷️ `separator` {#ITemplateProperties-separator}
Specifies if the template should be preceded by a separator in the condition template menu.
##### Type
boolean

---
### 🔗 `IConditionTemplate` {#IConditionTemplate}
Describes the interface for template object.
##### Type declaration
<InterfaceDeclaration prefix="#IConditionTemplate-" src={`interface IConditionTemplate {
  readonly label: string;
  readonly markdown: boolean;
  readonly icon?: SVGImage | string;
  readonly separator?: boolean;
  readonly burst?: "branches" | "branch" | "pipe";
  readonly command: (condition: Condition, burst: boolean) => void;
}`} symbols={{
  "Condition": "/blocks/api/classes/Condition/",
  "SVGImage": "/blocks/api/modules/SVGImage/"
}} />

---
#### 🏷️ `burst` {#IConditionTemplate-burst}
Contains if the condition allows burst creation. It can be one of the following values:
- `branches`: Separate branches are created for each condition;
- `branch`: The conditions are created within a single branch with the [culling mode](Branch.mdx#culling) `first`;
- `pipe`: The conditions are created within a single branch with the [culling mode](Branch.mdx#culling) `each`.
##### Type
"branches" | "branch" | "pipe"

---
#### 🏷️ `icon` {#IConditionTemplate-icon}
Icon for the template.
##### Type
[`SVGImage`](../modules/SVGImage.mdx) | string

---
#### 🏷️ `label` {#IConditionTemplate-label}
Label for the template.
##### Type
string

---
#### 🏷️ `markdown` {#IConditionTemplate-markdown}
Specifies if the label has possible markdown in it.
##### Type
boolean

---
#### 🏷️ `separator` {#IConditionTemplate-separator}
Specifies if the template should be preceded by a separator.
##### Type
boolean

---
#### 🔧 `command` {#IConditionTemplate-command}
Command that creates the actual condition block.
##### Signature
```ts
(condition: Condition, burst: boolean) => void
```
##### Parameters
| Name        | Type                         | Optional | Description                                    |
|:------------|:-----------------------------|:---------|:-----------------------------------------------|
| `condition` | [`Condition`](Condition.mdx) | No       | Reference to the [`Condition`](Condition.mdx). |
| `burst`     | boolean                      | No       | Specifies if the burst mode is active.         |

---
### 🔗 `IConditionGroup` {#IConditionGroup}
Describes the interface for group object.
##### Type declaration
<InterfaceDeclaration prefix="#IConditionGroup-" src={`interface IConditionGroup {
  readonly label: string;
  readonly markdown: boolean;
  readonly icon?: SVGImage | string;
  readonly templates: IConditionTemplate[];
}`} symbols={{
  "IConditionTemplate": "#IConditionTemplate",
  "SVGImage": "/blocks/api/modules/SVGImage/"
}} />

---
#### 🏷️ `icon` {#IConditionGroup-icon}
Icon for the template.
##### Type
[`SVGImage`](../modules/SVGImage.mdx) | string

---
#### 🏷️ `label` {#IConditionGroup-label}
Label for the template.
##### Type
string

---
#### 🏷️ `markdown` {#IConditionGroup-markdown}
Specifies if the label has possible markdown in it.
##### Type
boolean

---
#### 🏷️ `templates` {#IConditionGroup-templates}
Specifies the templates in the group.
##### Type
[`IConditionTemplate[]`](#IConditionTemplate)
