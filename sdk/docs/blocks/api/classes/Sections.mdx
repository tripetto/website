---
title: Sections class - Blocks
sidebar_label: Sections
description: The Sections class represents a collection of sections of a form definition in the builder (builder package).
---

# Sections class

## 📖 Description {#description}
The `Sections` class represents a collection of sections of a [form definition](../../../builder/api/interfaces/IDefinition.mdx) in the builder. Each section in the collection is a [`Section`](Section.mdx) instance.

## 🗃️ Fields {#fields}

---
### 🏷️ `all` {#all}
Retrieves all the sections that are in the collection.
#### Type
[`Section[]`](Section.mdx)

---
### 🏷️ `branch` {#branch}
Retrieves the parent branch.
#### Type
[`Branch`](Branch.mdx)

---
### 🏷️ `count` {#count}
Retrieves the number of sections in the collection.
#### Type
number

---
### 🏷️ `firstItem` {#firstItem}
Retrieves the first section in the collection.
#### Type
[`Section`](Section.mdx) | undefined

---
### 🏷️ `lastItem` {#lastItem}
Retrieves the last section in the collection.
#### Type
[`Section`](Section.mdx) | undefined

## ▶️ Methods {#methods}

---
### 🔧 `append` {#append}
Appends a new section to the end of the collection.
#### Signature
```ts
append(): Section
```
#### Return value
Returns a reference to the new [`Section`](Section.mdx) instance.

---
### 🔧 `appendBranch` {#appendBranch}
Appends a new branch section to the end of the collection.
#### Signature
```ts
appendBranch(): [Branch, Section]
```
#### Return value
Returns a reference to the new [`Branch`](Branch.mdx) and [`Section`](Section.mdx) instances.

---
### 🔧 `appendNestedBranch` {#appendNestedBranch}
Appends a new nested branch section to the end of the collection.
#### Signature
```ts
appendNestedBranch(): [Branch, Section]
```
#### Return value
Returns a reference to the new [`Branch`](Branch.mdx) and [`Section`](Section.mdx) instances.

---
### 🔧 `each` {#each}
Iterates through all sections in the collection.
#### Signature
```ts
each(fn: (section: Section) => boolean | void): boolean
```
#### Parameters
| Name | Type                                                   | Optional | Description                                                                                                                                       |
|:-----|:-------------------------------------------------------|:---------|:--------------------------------------------------------------------------------------------------------------------------------------------------|
| `fn` | (section: [`Section`](Section.mdx)) => boolean \| void | No       | Function that is called for each section in the collection. If you want to stop the iteration, you can return `true` from this function to do so. |
#### Return value
Returns `true` if the iteration was stopped (break).

---
### 🔧 `insert` {#insert}
Inserts a new section at the top of the collection.
#### Signature
```ts
insert(): Section
```
#### Return value
Returns a reference to the new [`Section`](Section.mdx) instance.

---
### 🔧 `insertAfter` {#insertAfter}
Inserts a new section after the supplied section.
#### Signature
```ts
insertAfter(section: Section): Section
```
#### Parameters
| Name      | Type                     | Optional | Description                                                    |
|:----------|:-------------------------|:---------|:---------------------------------------------------------------|
| `section` | [`Section`](Section.mdx) | No       | Specifies the section after which the new section is inserted. |
#### Return value
Returns a reference to the new [`Section`](Section.mdx) instance.

---
### 🔧 `insertBefore` {#insertBefore}
Inserts a new section before the supplied section.
#### Signature
```ts
insertBefore(section: Section): Section
```
#### Parameters
| Name      | Type                     | Optional | Description                                                  |
|:----------|:-------------------------|:---------|:-------------------------------------------------------------|
| `section` | [`Section`](Section.mdx) | No       | Specifies the section for which the new section is inserted. |
#### Return value
Returns a reference to the new [`Section`](Section.mdx) instance.

---
### 🔧 `insertBranch` {#insertBranch}
Inserts a new branch section at the top of the collection.
#### Signature
```ts
insertBranch(): [Branch, Section]
```
#### Return value
Returns a reference to the new [`Branch`](Branch.mdx) and [`Section`](Section.mdx) instances.

---
### 🔧 `insertBranchAfter` {#insertBranchAfter}
Inserts a new branch section after the supplied section.
#### Signature
```ts
insertBranchAfter(section: Section): [Branch, Section]
```
#### Parameters
| Name      | Type                     | Optional | Description                                                    |
|:----------|:-------------------------|:---------|:---------------------------------------------------------------|
| `section` | [`Section`](Section.mdx) | No       | Specifies the section after which the new section is inserted. |
#### Return value
Returns a reference to the new [`Branch`](Branch.mdx) and [`Section`](Section.mdx) instances.

---
### 🔧 `insertBranchBefore` {#insertBranchBefore}
Inserts a new branch section before the supplied section.
#### Signature
```ts
insertBranchBefore(section: Section): [Branch, Section]
```
#### Parameters
| Name      | Type                     | Optional | Description                                                    |
|:----------|:-------------------------|:---------|:---------------------------------------------------------------|
| `section` | [`Section`](Section.mdx) | No       | Specifies the section after which the new section is inserted. |
#### Return value
Returns a reference to the new [`Branch`](Branch.mdx) and [`Section`](Section.mdx) instances.

---
### 🔧 `insertNestedBranch` {#insertNestedBranch}
Inserts a new nested branch section at the top of the collection.
#### Signature
```ts
insertNestedBranch(): [Branch, Section]
```
#### Return value
Returns a reference to the new [`Branch`](Branch.mdx) and [`Section`](Section.mdx) instances.

---
### 🔧 `insertNestedBranchAfter` {#insertNestedBranchAfter}
Inserts a new nested branch section after the supplied section.
#### Signature
```ts
insertNestedBranchAfter(section: Section): [Branch, Section]
```
#### Parameters
| Name      | Type                     | Optional | Description                                                    |
|:----------|:-------------------------|:---------|:---------------------------------------------------------------|
| `section` | [`Section`](Section.mdx) | No       | Specifies the section after which the new section is inserted. |
#### Return value
Returns a reference to the new [`Branch`](Branch.mdx) and [`Section`](Section.mdx) instances.

---
### 🔧 `insertNestedBranchBefore` {#insertNestedBranchBefore}
Inserts a new nested branch section before the supplied section.
#### Signature
```ts
insertNestedBranchBefore(section: Section): [Branch, Section]
```
#### Parameters
| Name      | Type                     | Optional | Description                                                    |
|:----------|:-------------------------|:---------|:---------------------------------------------------------------|
| `section` | [`Section`](Section.mdx) | No       | Specifies the section after which the new section is inserted. |
#### Return value
Returns a reference to the new [`Branch`](Branch.mdx) and [`Section`](Section.mdx) instances.

---
### 🔧 `reverseEach` {#reverseEach}
Iterates through all sections in the collection in reverse direction (starting with the last section).
#### Signature
```ts
reverseEach(fn: (section: Section) => boolean | void): boolean
```
#### Parameters
| Name | Type                                                   | Optional | Description                                                                                                                                       |
|:-----|:-------------------------------------------------------|:---------|:--------------------------------------------------------------------------------------------------------------------------------------------------|
| `fn` | (section: [`Section`](Section.mdx)) => boolean \| void | No       | Function that is called for each section in the collection. If you want to stop the iteration, you can return `true` from this function to do so. |
#### Return value
Returns `true` if the iteration was stopped (break).

---
### 🔧 `sort` {#sort}
Sorts the sections using the supplied compare function.
#### Signature
```ts
sort(fn: (a: Section, b: Section) => number): boolean
```
#### Parameters
| Name | Type                                                                  | Optional | Description                                                                                                                                                                                                                     |
|:-----|:----------------------------------------------------------------------|:---------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `fn` | (a: [`Section`](Section.mdx), b: [`Section`](Section.mdx)) => boolean | No       | Specifies the compare function. The compare function should return a number according to the following rules:<br/>- `a` is less than `b` return -1;<br/>- `a` is greater than `b` return 1;<br/>- `a` is equal to `b` return 0. |
#### Return value
Returns `true` if the sort invoked a section position change.
