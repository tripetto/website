---
title: Section class - Blocks
sidebar_label: Section
description: The Section class represents a section of the form definition in the builder (builder package).
---

# Section class

## 📖 Description {#description}
The `Section` class represents a section of the [form definition](../../../builder/api/interfaces/IDefinition.mdx) in the builder.

## 🗃️ Fields {#fields}

---
### 🏷️ `branch` {#branch}
Retrieves a reference to parent [`Branch`](Branch.mdx).
#### Type {#signature}
[`Branch`](Branch.mdx) | undefined

---
### 🏷️ `branches` {#branches}
Retrieves the collection of child branches.
#### Type {#signature}
[`Branches`](Branches.mdx)

---
### 🏷️ `hasBranches` {#hasBranches}
Retrieves if the section has any branches.
#### Type {#signature}
boolean

---
### 🏷️ `id` {#id}
Retrieves the identifier of the section.
#### Type {#signature}
string

---
### 🏷️ `index` {#index}
Retrieves the zero-based index of the section in the [`Sections`](Sections.mdx) collection.
#### Type {#signature}
number

---
### 🏷️ `isFirst` {#isFirst}
Retrieves if the section is the first section in the [`Sections`](Sections.mdx) collection.
#### Type {#signature}
boolean

---
### 🏷️ `isLast` {#isLast}
Retrieves if the section is the last section in the [`Sections`](Sections.mdx) collection.
#### Type {#signature}
boolean

---
### 🏷️ `isMoving` {#isMoving}
Retrieves if the section is being moved (dragged) in the builder.
#### Type {#signature}
boolean

---
### 🏷️ `isReadonly` {#isReadonly}
Sets or retrieves if the nested branch (subform) is read-only.
#### Type {#signature}
boolean

:::tip
See the [Subforms guide](../../../builder/integrate/guides/subforms.mdx) for more information.
:::

---
### 🏷️ `isRendered` {#isRendered}
Retrieves if the section is rendered by the builder.
#### Type {#signature}
boolean

---
### 🏷️ `jumps` {#jumps}
Retrieves a list of branches which have a jump to this section.
#### Type {#signature}
[`Branch[]`](Branch.mdx)

---
### 🏷️ `name` {#name}
Sets or retrieves the name of the section.
#### Type {#signature}
string

---
### 🏷️ `nodes` {#nodes}
Retrieves the collection of child nodes.
#### Type {#signature}
[`Nodes`](Nodes.mdx)

---
### 🏷️ `parent` {#parent}
Retrieves a reference to parent [`Sections`](Sections.mdx) collection.
#### Type {#signature}
[`Sections`](Sections.mdx)

---
### 🏷️ `predecessor` {#predecessor}
Retrieves a reference to the predecessor `Section` (if any).
#### Type {#signature}
`Section` | undefined

---
### 🏷️ `reference` {#reference}
Sets or retrieves the optional reference for a nested branch (subform). This property can be used to track the origin of a subform when it was loaded from an external source. For example, the identifier of a subform can be stored in this property.
#### Type {#signature}
string

:::tip
See the [Subforms guide](../../../builder/integrate/guides/subforms.mdx) for more information.
:::

---
### 🏷️ `successor` {#successor}
Retrieves a reference to the successor `Section` (if any).
#### Type {#signature}
`Section` | undefined

---
### 🏷️ `type` {#type}
Retrieves the section type, which can be one of the following values:
- `section`: The section is a regular section;
- `branch`: The section serves as a branch;
- `nest`: The section holds a nested branch (subform).
#### Type {#signature}
"section" | "branch" | "nest"

---
### 🏷️ `version` {#version}
Sets or retrieves a version indicator for a section that is a nested branch (subform). This property can be used to track the origin of a subform when it was loaded from an external source. For example, the version number of a subform can be stored in this property.
#### Type {#signature}
string

:::tip
See the [Subforms guide](../../../builder/integrate/guides/subforms.mdx) for more information.
:::

## ▶️ Methods {#methods}

---
### 🔧 `convertToBranch` {#convertToBranch}
Converts a section to a branch.
#### Signature
```ts
convertToBranch(): boolean
```
#### Return value
Returns `true` if the conversion was successfully.

---
### 🔧 `convertToSection` {#convertToSection}
Convert a (nested) section back to a regular section.
#### Signature
```ts
convertToSection(): boolean
```
#### Return value
Returns `true` if the conversion was successfully.

---
### 🔧 `convertToNestedBranch` {#convertToNestedBranch}
Converts a section to a nested branch.
#### Signature
```ts
convertToNestedBranch(): void
```

---
### 🔧 `delete` {#delete}
Deletes a section from the [`Sections`](Sections.mdx) collection.
#### Signature
```ts
delete(): this
```
#### Return value
Returns a reference to the deleted `Section`.

---
### 🔧 `duplicate` {#duplicate}
Duplicates the section.
#### Signature
```ts
duplicate(): Section
```
#### Return value
Returns a reference to the duplicated `Section`.

---
### 🔧 `extract` {#extract}
Extracts a nested branch to the parent form.
#### Signature
```ts
extract(): boolean
```
#### Return value
Returns `true` if the subform was extracted successfully.

---
### 🔧 `insertAfter` {#insertAfter}
Inserts a new section after this section.
#### Signature
```ts
insertAfter(): Section
```
#### Return value
Returns a reference to the new `Section`.

---
### 🔧 `insertBefore` {#insertBefore}
Inserts a new section before this section.
#### Signature
```ts
insertBefore(): Section
```
#### Return value
Returns a reference to the new `Section`.

---
### 🔧 `insertBranchAfter` {#insertBranchAfter}
Inserts a new branch section after this section.
#### Signature
```ts
insertBranchAfter(): [Branch, Section]
```
#### Return value
Returns a reference to the new branch and section.

---
### 🔧 `insertBranchBefore` {#insertBranchBefore}
Inserts a new branch section before this section.
#### Signature
```ts
insertBranchBefore(): [Branch, Section]
```
#### Return value
Returns a reference to the new branch and section.

---
### 🔧 `insertNestedBranchAfter` {#insertNestedBranchAfter}
Inserts a new nested branch section after this section.
#### Signature
```ts
insertNestedBranchAfter(): [Branch, Section]
```
#### Return value
Returns a reference to the new branch and section.

---
### 🔧 `insertNestedBranchBefore` {#insertNestedBranchBefore}
Inserts a new nested branch section before this section.
#### Signature
```ts
insertNestedBranchBefore(): [Branch, Section]
```
#### Return value
Returns a reference to the new branch and section.

---
### 🔧 `loadNestedBranch` {#loadNestedBranch}
Loads a nested branch (subform).
:::info
You can only load nested branches (subforms) to sections of [type](#type) `nest`.
:::
#### Signature
```ts
loadNestedBranch(
  definition: IDefinition,
  reference?: string,
  version?: string,
  alias?: string,
  readonly?: boolean
): boolean
```
#### Parameters
| Name         | Type                                                             | Optional | Description                                  |
|:-------------|:-----------------------------------------------------------------|:---------|:---------------------------------------------|
| `definition` | [`IDefinition`](../../../builder/api/interfaces/IDefinition.mdx) | No       | Specifies the definition to load as subform. |
| `reference`  | string                                                           | Yes      | Specifies an optional reference to store.    |
| `version`    | string                                                           | Yes      | Specifies an optional version identifier.    |
| `alias`      | string                                                           | Yes      | Specifies an optional alias for the subform. |
| `readonly`   | boolean                                                          | Yes      | Specifies if the subform is read-only.       |
#### Return value
Returns `true` if the subform was loaded succesfully.
:::tip
See the [Subforms guide](../../../builder/integrate/guides/subforms.mdx) for more information.
:::

---
### 🔧 `moveToIndex` {#moveToIndex}
Moves the section to the specified index.
#### Signature
```ts
moveToIndex(index: number): boolean
```
#### Parameters
| Name    | Type   | Optional | Description                                                                      |
|:--------|:-------|:---------|:---------------------------------------------------------------------------------|
| `index` | number | No       | Specifies the new index position for the section where the first section is `0`. |
#### Return value
Returns `true` if the move succeeded.

---
### 🔧 `moveToNestedBranch` {#moveToNestedBranch}
Moves the section to a nested branch (subform).
#### Signature
```ts
moveToNestedBranch(section: Section): boolean
```
#### Parameters
| Name      | Type      | Optional | Description                                   |
|:----------|:----------|:---------|:----------------------------------------------|
| `section` | `Section` | No       | Specifies the section to move the section to. |
#### Return value
Returns `true` if the section was moved succesfully.

---
### 🔧 `moveToParent` {#moveToParent}
Moves the section to the parent form if it is in a nested branch (subform).
#### Signature
```ts
moveToParent(): boolean
```
#### Return value
Returns `true` if the section was moved succesfully.

---
### 🔧 `moveUpOrDown` {#moveUpOrDown}
Moves the section up or down.
#### Signature
```ts
moveUpOrDown(up: boolean): boolean
```
#### Parameters
| Name | Type    | Optional | Description                                                       |
|:-----|:--------|:---------|:------------------------------------------------------------------|
| `up` | boolean | No       | Specifies if the move direction is up (`true`) or down (`false`). |
#### Return value
Returns `true` if the move succeeded.

---
### 🔧 `rerender` {#rerender}
Invokes a rerendering of the section.
#### Signature
```ts
rerender(type: "refresh" | "update"): void
```
#### Parameters
| Name   | Type                  | Optional | Description                                                                                                                                                                |
|:-------|:----------------------|:---------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `type` | "refresh" \| "update" | No       | Specifies the rerender type. It can be one of the following values:<br/>- `refresh`: Refresh the whole rendering (slower);<br/>- `update`: Updates the rendering (faster). |

---
### 🔧 `reset` {#reset}
Resets the section and removes all content in the section.
#### Signature
```ts
reset(): void
```

---
### 🔧 `swap` {#swap}
Swaps the section with another section.
#### Signature
```ts
swap(with: Section): boolean
```
#### Parameters
| Name   | Type      | Optional | Description                         |
|:-------|:----------|:---------|:------------------------------------|
| `with` | `Section` | No       | Specifies the section to swap with. |
#### Return value
Returns `true` if the swap succeeded.
