---
title: ConditionBlock class - Blocks
sidebar_label: ConditionBlock
description: The ConditionBlock class is an abstract class for creating condition blocks for the builder (builder package).
---

# ConditionBlock class

## 📖 Description {#description}
The `ConditionBlock` class is an [abstract class](https://www.typescriptlang.org/docs/handbook/2/classes.html#abstract-classes-and-members) for creating condition blocks for the builder. A condition block defines a behavior for a [`Condition`](Condition.mdx). An instance of a derived `ConditionBlock` class is created using the [`assignBlock`](Condition.mdx#assignBlock) method of a [`Condition`](Condition.mdx).

:::tip
Follow [this tutorial](../../custom/implement/condition.mdx) to learn how to build a custom condition block.
:::

## 🎀 Applicable decorators {#decorators}
The following decorators can be applied in this class:
#### Class decorators [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#class-decorators)
- [`@tripetto`](../decorators/tripetto.mdx)
#### Property decorators [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#property-decorators)
- [`@affects`](../decorators/affects.mdx)
- [`@collection`](../decorators/collection.mdx)
- [`@definition`](../decorators/definition.mdx)
#### Method decorators [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#method-decorators)
- [`@assigned`](../decorators/assigned.mdx)
- [`@created`](../decorators/created.mdx)
- [`@destroyed`](../decorators/destroyed.mdx)
- [`@detached`](../decorators/detached.mdx)
- [`@editor`](../decorators/editor.mdx)
- [`@renamed`](../decorators/renamed.mdx)
- [`@unassigned`](../decorators/unassigned.mdx)
- [`@upgraded`](../decorators/upgraded.mdx)

## 📌 Statics {#statics}

---
### 🏷️ `context` {#context}
Contains the context for the block. It can be one of the following values:
- `*`: The block defines a global condition (it can be used anywhere and it is not dependent on a section, branch, node or block type);
- `section`: The block defines a condition that applies to a section;
- `branch`: The block defines a condition that applies to a branch;
- `node`: The block defines a condition that applies to a node;
- `typeof NodeBlock`: The blocks defines a condition that applies to the specified [`NodeBlock`](NodeBlock.mdx).
#### Type
"*" | "section" | "branch" | "node" | `typeof NodeBlock`

---
### 🏷️ `icon` {#icon}
Contains the icon of the block.
#### Type
[`SVGImage`](../modules/SVGImage.mdx) | string

---
### 🏷️ `identifier` {#identifier}
Contains the type identifier of the block.
#### Type
string

---
### 🏷️ `label` {#label}
Retrieves the localized label (name) of the block.
#### Type
string

---
### 🏷️ `version` {#version}
Contains the version of the block.
#### Type
string

---
### 🔧 `flag` {#flag}
Verifies if the block has the specified flag enabled.
:::tip
Flags can be set using the [`flag`](../../../builder/api/modules/Namespaces.mdx#flag) function.
:::
#### Signature
```ts
flag(id: string): boolean
```
#### Parameters
| Name | Type   | Optional | Description                              |
|:-----|:-------|:---------|:-----------------------------------------|
| `id` | string | No       | Specifies the flag identifier to verify. |
#### Return value
Returns `true` if the specified flag was enabled.

## 🗃️ Fields {#fields}

---
### 🏷️ `allowMarkdown` {#allowMarkdown}
Specifies if markdown is supported in the label of the block.
:::info
When omitted, basic markdown formatting (bold, italic, underline, strikethrough and hyperlinks) is supported.
:::
#### Type
boolean

---
### 🏷️ `branch` {#branch}
Contains the branch attached to the block.
#### Type
[`Branch`](Branch.mdx) | undefined

---
### 🏷️ `condition` {#condition}
Retrieves a reference to the [`Condition`](Condition.mdx) instance that holds the block.
#### Type
[`Condition`](Condition.mdx)

---
### 🏷️ `editor` {#editor}
Retrieves the [`EditorOrchestrator`](EditorOrchestrator.mdx) instance used to define the controls to manage the block.
#### Type
[`EditorOrchestrator`](EditorOrchestrator.mdx)

---
### 🏷️ `icon` {#icon}
Retrieves the icon of the condition block.
#### Type
[`SVGImage`](../modules/SVGImage.mdx) | string

---
### 🏷️ `id` {#id}
Retrieves the identifier of the condition.
#### Type
string

---
### 🏷️ `isInitialized` {#isInitialized}
Retrieves if the block is initialized.
#### Type
boolean

---
### 🏷️ `label` {#label}
Retrieves the label of the condition block.
#### Type
string

---
### 🏷️ `name` {#name}
Retrieves the name of the condition block.
#### Type
string

---
### 🏷️ `node` {#node}
Contains the node attached to the block.
#### Type
[`Node`](Node.mdx) | undefined

---
### 🏷️ `section` {#section}
Contains the section attached to the block.
#### Type
[`Section`](Section.mdx) | undefined

---
### 🏷️ `slot` {#slot}
Contains the slot attached to the block.
#### Type
[`Slot`](../modules/Slots/Slot.mdx) | undefined

---
### 🏷️ `title` {#title}
Retrieves the title of the condition block. This is often the name of the item that is attached to the block.
#### Type
string | undefined

## ▶️ Methods {#methods}

---
### 🔧 `detectChange` {#detectChange}
Notifies the builder that a change has occurred and the definition has changed.
:::info
Change detection is normally handled automatically. The builder monitors all block properties that are decorated with the [`@definition`](../decorators/definition.mdx) decorator.
:::
#### Signature
```ts
detectChange(): void
```

---
### 🔧 `rerender` {#rerender}
Rerenders the block.
#### Signature
```ts
rerender(force?: boolean): void
```
#### Parameters
| Name    | Type    | Optional | Description                                              |
|:--------|:--------|:---------|:---------------------------------------------------------|
| `force` | boolean | Yes      | Specifies to force the rerendering (default is `false`). |
