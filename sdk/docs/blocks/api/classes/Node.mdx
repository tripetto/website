---
title: Node class - Blocks
sidebar_label: Node
description: The Node class represents a node of the form definition in the builder (builder package).
---

# Node class

## 📖 Description {#class-description}
The `Node` class represents a node of the [form definition](../../../builder/api/interfaces/IDefinition.mdx) in the builder.

## 🗃️ Fields {#fields}

---
### 🏷️ `alias` {#alias}
Retrieves the alias of the node.
#### Type
string | undefined

---
### 🏷️ `block` {#block}
Retrieves the attached [`NodeBlock`](NodeBlock.mdx) instance for the node.
#### Type
[`NodeBlock`](NodeBlock.mdx) | undefined

---
### 🏷️ `blockTypeIdentifier` {#blockTypeIdentifier}
Retrieves the block type identifier for the node.
#### Type
string

---
### 🏷️ `description` {#description}
Sets or retrieves the description of the node.
#### Type
string | undefined

---
### 🏷️ `disabled` {#disabled}
Sets or retrieves if the node is disabled.
#### Type
boolean

---
### 🏷️ `explanation` {#explanation}
Sets or retrieves the explanation of the node.
#### Type
string | undefined

---
### 🏷️ `hasBlock` {#hasBlock}
Retrieves if the node has a block attached to it.
#### Type
boolean

---
### 🏷️ `id` {#id}
Retrieves the identifier of the node.
#### Type
string

---
### 🏷️ `index` {#index}
Retrieves the zero-based index of the node in the [`Nodes`](Nodes.mdx) collection.
#### Type
string

---
### 🏷️ `isBlockInvalid` {#isBlockInvalid}
Retrieves if the block is invalid. In that case, there is no block implementation found for the specified block type.
#### Type
boolean

---
### 🏷️ `isFirst` {#isFirst}
Retrieves if the node is the first node in the [`Nodes`](Nodes.mdx) collection.
#### Type
boolean

---
### 🏷️ `isLast` {#isLast}
Retrieves if the node is the last node in the [`Nodes`](Nodes.mdx) collection.
#### Type
boolean

---
### 🏷️ `isMoving` {#isMoving}
Retrieves if the node is being moved (dragged) in the builder.
#### Type
boolean

---
### 🏷️ `isRendered` {#isRendered}
Retrieves if the node is rendered by the builder.
#### Type
boolean

---
### 🏷️ `label` {#label}
Retrieves the node label in plain text (all markdown is removed). This label is the name of the node or the placeholder if no name is specified.
#### Type
string

---
### 🏷️ `labelMarkdown` {#labelMarkdown}
Retrieves a [`MarkdownParser`](../modules/Markdown/MarkdownParser.mdx) instance for the label.
#### Type
[`MarkdownParser`](../modules/Markdown/MarkdownParser.mdx)

---
### 🏷️ `labelRaw` {#labelRaw}
Returns the raw label which includes all markdown formatting.
#### Type
string

---
### 🏷️ `name` {#name}
Sets or retrieves name of the node.
#### Type
string

---
### 🏷️ `nameMarkdown` {#nameMarkdown}
Retrieves a [`MarkdownParser`](../modules/Markdown/MarkdownParser.mdx) instance for the name.
#### Type
[`MarkdownParser`](../modules/Markdown/MarkdownParser.mdx)

---
### 🏷️ `nameVisible` {#nameVisible}
Sets or retrieves if the name of the node is visible.
#### Type
boolean

---
### 🏷️ `parent` {#parent}
Retrieves a reference to parent [`Nodes`](Nodes.mdx) collection.
#### Type
[`Nodes`](Nodes.mdx)

---
### 🏷️ `placeholder` {#placeholder}
Sets or retrieves the placeholder of the node.
#### Type
string | undefined

---
### 🏷️ `predecessor` {#predecessor}
Retrieves the predecessor of the node.
#### Type
`Node` | undefined

---
### 🏷️ `section` {#section}
Retrieves the parent section.
#### Type
[`Section`](Section.mdx) | undefined

---
### 🏷️ `successor` {#successor}
Retrieves the successor of the node.
#### Type
`Node` | undefined

## ▶️ Methods {#methods}

---
### 🔧 `assignBlock` {#assignBlock}
Assigns a block to the node. If a block type is supplied, a new instance for the specified block is created.
#### Signature
```ts
assignBlock(block: NodeBlock | typeof NodeBlock): NodeBlock | undefined
```
#### Parameters
| Name    | Type                                                                | Optional | Description                                                                                                                   |
|:--------|:--------------------------------------------------------------------|:---------|:------------------------------------------------------------------------------------------------------------------------------|
| `block` | [`NodeBlock`](NodeBlock.mdx) \| [`typeof NodeBlock`](NodeBlock.mdx) | No       | Specifies the block instance or type to attach. When a block type is supplied, a new block instance is automatically created. |
#### Return value
Returns a reference to the [`NodeBlock`](NodeBlock.mdx) instance.

---
### 🔧 `delete` {#delete}
Deletes a node from the [`Nodes`](Nodes.mdx) collection.
#### Signature
```ts
delete(): this
```
#### Return value
Returns a reference to the deleted `Node`.

---
### 🔧 `disable` {#disable}
Disable the node.
#### Signature
```ts
disable(): this
```
#### Return value
Returns a reference to itself.

---
### 🔧 `duplicate` {#duplicate}
Duplicates the node.
#### Signature
```ts
duplicate(dest?: Node): Node
```
#### Parameters
| Name   | Type   | Optional | Description                                                                                                                                                                                                       |
|:-------|:-------|:---------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `dest` | `Node` | Yes      | Specifies the destination node that is used to duplicate to. All current content of that node is removed and replaced by the node content that is duplicated. If omitted a new node is added. |
#### Return value
Returns a reference to the duplicated `Node`.

---
### 🔧 `enable` {#enable}
Enable the node.
#### Signature
```ts
enable(): this
```
#### Return value
Returns a reference to itself.

---
### 🔧 `insertAfter` {#insertAfter}
Inserts a new node after this node.
#### Signature
```ts
insertAfter(): Node
```
#### Return value
Returns a reference to the new `Node`.

---
### 🔧 `insertBefore` {#insertBefore}
Inserts a new node before this node.
#### Signature
```ts
insertBefore(): Node
```
#### Return value
Returns a reference to the new `Node`.

---
### 🔧 `moveToIndex` {#moveToIndex}
Moves the node to the specified index.
#### Signature
```ts
moveToIndex(index: number): boolean
```
#### Parameters
| Name    | Type   | Optional | Description                                                                          |
|:--------|:-------|:---------|:-------------------------------------------------------------------------------------|
| `index` | number | No       | Specifies the new index position for the node where the first node is `0`. |
#### Return value
Returns `true` if the move succeeded.

---
### 🔧 `moveUpOrDown` {#moveUpOrDown}
Moves the node up or down.
#### Signature
```ts
moveUpOrDown(up: boolean): boolean
```
#### Parameters
| Name | Type    | Optional | Description                                                       |
|:-----|:--------|:---------|:------------------------------------------------------------------|
| `up` | boolean | No       | Specifies if the move direction is up (`true`) or down (`false`). |
#### Return value
Returns `true` if the move succeeded.

---
### 🔧 `refresh` {#refresh}
Refreshes a node so it rerenders in the builder.
#### Signature
```ts
refresh(): this
```
#### Return value
Returns a reference to the node itself.

---
### 🔧 `rerender` {#rerender}
Invokes a rerendering of the node.
#### Signature
```ts
rerender(type: "refresh" | "update"): void
```
#### Parameters
| Name   | Type                  | Optional | Description                                                                                                                                                                |
|:-------|:----------------------|:---------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `type` | "refresh" \| "update" | No       | Specifies the rerender type. It can be one of the following values:<br/>- `refresh`: Refresh the whole rendering (slower);<br/>- `update`: Updates the rendering (faster). |

---
### 🔧 `reset` {#reset}
Resets the node and removes all content in the node.
#### Signature
```ts
reset(): void
```

---
### 🔧 `slot` {#slot}
Retrieves a slot.
#### Signature
```ts
slot(reference: string): Slot | undefined
```
#### Parameters
| Name        | Type   | Optional | Description                   |
|:------------|:-------|:---------|:------------------------------|
| `reference` | string | No       | Specifies the slot reference. |
#### Return value
Returns a reference to the [`Slot`](../modules/Slots/Slot.mdx) instance or `undefined` if the slot was not found.

---
### 🔧 `swap` {#swap}
Swaps the node with another node.
#### Signature
```ts
swap(with: Node): boolean
```
#### Parameters
| Name   | Type   | Optional | Description                           |
|:-------|:-------|:---------|:--------------------------------------|
| `with` | `Node` | No       | Specifies the node to swap with. |
#### Return value
Returns `true` if the swap succeeded.

---
### 🔧 `unassignBlock` {#unassignBlock}
Unassigns a block.
#### Signature
```ts
unassignBlock(): NodeBlock | undefined
```
#### Return value
Returns the previously assigned [`NodeBlock`](NodeBlock.mdx) instance or `undefined` if there was none.
