---
title: Blocks API
description: This API reference contains all the block-related exports of the Tripetto Builder package.
---

# ![](/img/logo-ts.svg) Blocks API reference
This API reference contains all the block-related exports of the [Tripetto Builder](https://www.npmjs.com/package/@tripetto/builder) package. These exports are used when developing custom blocks.

:::info
This package is also used when [integrating the builder](../../builder/integrate/introduction.md) into custom projects. The API reference for integrating the builder is located in the [builder documentation](../../builder/api/index.md).
:::

## ✨ Installation [![](/img/logo-npm-icon.svg)](https://www.npmjs.com/package/@tripetto/builder) {#installation}

```bash npm2yarn
npm install @tripetto/builder
```

:::tip Typescript support
This package contains type declarations and supports [TypeScript](https://www.typescriptlang.org/) out-of-the-box.
:::

## 💎 Classes {#classes}
- [`Await`](classes/Await.mdx)
- [`Branch`](classes/Branch.mdx)
- [`Branches`](classes/Branches.mdx)
- [`Callback`](classes/Callback.mdx)
- [`Section`](classes/Section.mdx)
- [`Sections`](classes/Sections.mdx)
- [`Condition`](classes/Condition.mdx)
- [`ConditionBlock`](classes/ConditionBlock.mdx)
- [`Conditions`](classes/Conditions.mdx)
- [`ConditionsOrchestrator`](classes/ConditionsOrchestrator.mdx)
- [`EditorOrchestrator`](classes/EditorOrchestrator.mdx)
- [`Node`](classes/Node.mdx)
- [`NodeBlock`](classes/NodeBlock.mdx)
- [`Nodes`](classes/Nodes.mdx)

## 🎀 Decorators {#decorators}
#### Class decorators [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#class-decorators)
- [`@tripetto`](decorators/tripetto.mdx)
#### Property decorators [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#property-decorators)
- [`@affects`](decorators/affects.mdx)
- [`@alias`](decorators/alias.mdx)
- [`@collection`](decorators/collection.mdx)
- [`@definition`](decorators/definition.mdx)
- [`@icon`](decorators/icon.mdx)
- [`@menu`](decorators/menu.mdx)
- [`@metadata`](decorators/metadata.mdx)
- [`@name`](decorators/name.mdx)
- [`@score`](decorators/score.mdx)
- [`@supplies`](decorators/supplies.mdx)
- [`@title`](decorators/title.mdx)
- [`@width`](decorators/width.mdx)
#### Method decorators [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#method-decorators)
- [`@assigned`](decorators/assigned.mdx)
- [`@conditions`](decorators/conditions.mdx)
- [`@created`](decorators/created.mdx)
- [`@deleted`](decorators/deleted.mdx)
- [`@destroyed`](decorators/destroyed.mdx)
- [`@detached`](decorators/detached.mdx)
- [`@editor`](decorators/editor.mdx)
- [`@refreshed`](decorators/refreshed.mdx)
- [`@renamed`](decorators/renamed.mdx)
- [`@reordered`](decorators/reordered.mdx)
- [`@slots`](decorators/slots.mdx)
- [`@unassigned`](decorators/unassigned.mdx)
- [`@upgraded`](decorators/upgraded.mdx)

## ▶️ Functions {#functions}
- [`_`](functions/_.mdx)
- [`_n`](functions/_n.mdx)
- [`arrayItem`](functions/arrayItem.mdx)
- [`arraySize`](functions/arraySize.mdx)
- [`assert`](functions/assert.mdx)
- [`call`](functions/call.mdx)
- [`callAsync`](functions/callAsync.mdx)
- [`callBind`](functions/callBind.mdx)
- [`callPromise`](functions/callPromise.mdx)
- [`cancelFrame`](functions/cancelFrame.mdx)
- [`cancelPromise`](functions/cancelPromise.mdx)
- [`cast`](functions/cast.mdx)
- [`castToBoolean`](functions/castToBoolean.mdx)
- [`castToFloat`](functions/castToFloat.mdx)
- [`castToNumber`](functions/castToNumber.mdx)
- [`castToString`](functions/castToString.mdx)
- [`clone`](functions/clone.mdx)
- [`compare`](functions/compare.mdx)
- [`count`](functions/count.mdx)
- [`destroy`](functions/destroy.mdx)
- [`each`](functions/each.mdx)
- [`eachReverse`](functions/eachReverse.mdx)
- [`extend`](functions/extend.mdx)
- [`extendImmutable`](functions/extendImmutable.mdx)
- [`filter`](functions/filter.mdx)
- [`find`](functions/find.mdx)
- [`findFirst`](functions/findFirst.mdx)
- [`findLast`](functions/findLast.mdx)
- [`firstArrayItem`](functions/firstArrayItem.mdx)
- [`forEach`](functions/forEach.mdx)
- [`get`](functions/get.mdx)
- [`getBoolean`](functions/getBoolean.mdx)
- [`getFloat`](functions/getFloat.mdx)
- [`getHelpTopic`](functions/getHelpTopic.mdx)
- [`getMetadata`](functions/getMetadata.mdx)
- [`getNumber`](functions/getNumber.mdx)
- [`getSlotIcon`](functions/getSlotIcon.mdx)
- [`getString`](functions/getString.mdx)
- [`hasOneOrMore`](functions/hasOneOrMore.mdx)
- [`hasOnly`](functions/hasOnly.mdx)
- [`insertVariable`](functions/insertVariable.mdx)
- [`isArray`](functions/isArray.mdx)
- [`isBoolean`](functions/isBoolean.mdx)
- [`isCollection`](functions/isCollection.mdx)
- [`isDate`](functions/isDate.mdx)
- [`isDefined`](functions/isDefined.mdx)
- [`isError`](functions/isError.mdx)
- [`isFilledString`](functions/isFilledString.mdx)
- [`isFloat`](functions/isFloat.mdx)
- [`isFunction`](functions/isFunction.mdx)
- [`isNull`](functions/isNull.mdx)
- [`isNumber`](functions/isNumber.mdx)
- [`isNumberFinite`](functions/isNumberFinite.mdx)
- [`isObject`](functions/isObject.mdx)
- [`isPromise`](functions/isPromise.mdx)
- [`isRegEx`](functions/isRegEx.mdx)
- [`isString`](functions/isString.mdx)
- [`isUndefined`](functions/isUndefined.mdx)
- [`isVariable`](functions/isVariable.mdx)
- [`lastArrayItem`](functions/lastArrayItem.mdx)
- [`lookupVariable`](functions/lookupVariable.mdx)
- [`makeMarkdownSafe`](functions/makeMarkdownSafe.mdx)
- [`map`](functions/map.mdx)
- [`markdownifyToString`](functions/markdownifyToString.mdx)
- [`npgettext`](functions/npgettext.mdx)
- [`pgettext`](functions/pgettext.mdx)
- [`populateSlots`](functions/populateSlots.mdx)
- [`populateVariables`](functions/populateVariables.mdx)
- [`reduce`](functions/reduce.mdx)
- [`scheduleAction`](functions/scheduleAction.mdx)
- [`scheduleAndCancelFrame`](functions/scheduleAndCancelFrame.mdx)
- [`scheduleAndCancelPromise`](functions/scheduleAndCancelPromise.mdx)
- [`scheduleAnimation`](functions/scheduleAnimation.mdx)
- [`scheduleEvent`](functions/scheduleEvent.mdx)
- [`scheduleFrame`](functions/scheduleFrame.mdx)
- [`schedulePromise`](functions/schedulePromise.mdx)
- [`set`](functions/set.mdx)
- [`stringLength`](functions/stringLength.mdx)

## 🗂️ Modules {#modules}
- [`AES`](modules/AES.mdx)
- [`Collection`](modules/Collection/index.mdx)
- [`Components`](modules/Components/index.mdx)
- [`DateTime`](modules/DateTime.mdx)
- [`Environment`](modules/Environment.mdx)
- [`Forms`](modules/Forms/index.mdx)
- [`L10n`](modules/L10n/index.mdx)
- [`Markdown`](modules/Markdown/index.mdx)
- [`Num`](modules/Num.mdx)
- [`SHA2`](modules/SHA2.mdx)
- [`Slots`](modules/Slots/index.mdx)
- [`Str`](modules/Str.mdx)
- [`Tripetto`](modules/Tripetto.mdx)

## ⛓️ Interfaces {#interfaces}
- [`ISlotIdentifier`](interfaces/ISlotIdentifier.mdx)
- [`IVariable`](interfaces/IVariable.mdx)

## 🗿 Constants {#constants}
- [`NAME`](constants/NAME.mdx)
- [`REGEX_IS_URL`](constants/REGEX_IS_URL.mdx)
- [`VERSION`](constants/VERSION.mdx)

## 📁 Source code [![](/img/logo-gitlab.svg)](https://gitlab.com/tripetto/builder) {#source}
The Tripetto Builder package code is on [GitLab](https://gitlab.com/tripetto/builder).
