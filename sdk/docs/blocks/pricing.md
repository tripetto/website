---
title: Pricing - Blocks
sidebar_label: Pricing
sidebar_position: 5
description: Blocks are free of charge!
---

# 💳 Pricing
We can keep it short here: Blocks are free of charge! Have a blast 😎

## Stock blocks
All of Tripetto's [stock blocks](stock/index.mdx) are free, [open-source](https://gitlab.com/tripetto/blocks/) and published under the [MIT license](https://opensource.org/licenses/MIT).

## Custom blocks
Building [custom blocks](custom/introduction.md) is of course free. You don't need an SDK license in order to develop them, and you may distribute them under your terms and conditions.
