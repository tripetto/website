---
title: Tracking - Guides - Stock runners
sidebar_label: Tracking
sidebar_position: 14
description: The stock runners have a special event to implement form tracking to measure the number of times a form and its questions are shown.
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Track form usage
The stock runners have a special event to implement form tracking. Through form tracking, you can measure the number of times a form and its questions are shown. You can connect services like [Google Analytics](#google-analytics), [Google Tag Manager](#gtm), or [Facebook Pixel](#facebook-pixel).

:::tip
Read more about using tracking services in the [Tripetto Studio](../../../applications/studio/introduction.md) or [WordPress plugin](../../../applications/wordpress/introduction.md) in the [Tripetto Help Center](https://tripetto.com/help/articles/how-to-track-form-activity-with-custom-tracking-code/).
:::

## 📈 Basic implementation {#implementation}
To receive tracking events, you use the [`onAction`](#reference) event of the runner. This event is invoked when a certain action takes place. The following actions are supported:

- `start`: A form is started;
- `stage`: A block becomes available. It depends on the runner when this action fires:
  - Autoscroll runner: this action fires when a block gets activated;
  - Chat runner: this action fires when a block becomes answerable;
  - Classic runner: this even fires when the block becomes visible.
- `unstage`: A block becomes unavailable. It depends on the runner when this action fires:
   - Autoscroll runner: this action fires when a block gets deactivated;
   - Chat runner: this action fires when a block becomes unanswerable;
   - Classic runner: this even fires when the block becomes invisible.
- `focus`: An input element gains focus;
- `blur`: An input element loses focus;
- `pause`: A form is paused;
- `complete`: A form is completed.

Besides the type of action, the event supplies information about the form and the block. The block information is only available for the action `stage`, `unstage`, `focus`, and `blur`.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onAction: (type, definition, block) => {
    switch(type) {
      case "start":
      case "pause":
      case "complete":
        console.log(`Form ${definition.name} ${type}`);
        break;
      case "stage":
      case "unstage":
      case "focus":
      case "blur":
        console.log(`Form ${definition.name} ${type} ${block.name}`);
        break;
    }
  }
  //highlight-end
});
```

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
import { run } from "@tripetto/runner-chat";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onAction: (type, definition, block) => {
    switch(type) {
      case "start":
      case "pause":
      case "complete":
        console.log(`Form ${definition.name} ${type}`);
        break;
      case "stage":
      case "unstage":
      case "focus":
      case "blur":
        console.log(`Form ${definition.name} ${type} ${block.name}`);
        break;
    }
  }
  //highlight-end
});
```

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
import { run } from "@tripetto/runner-classic";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onAction: (type, definition, block) => {
    switch(type) {
      case "start":
      case "pause":
      case "complete":
        console.log(`Form ${definition.name} ${type}`);
        break;
      case "stage":
      case "unstage":
      case "focus":
      case "blur":
        console.log(`Form ${definition.name} ${type} ${block.name}`);
        break;
    }
  }
  //highlight-end
});
```

</TabItem>
</Tabs>

## ⚙️ Google Analytics {#google-analytics}
First, make sure to enable [Google Analytics](https://www.google.com/analytics/) in your application. Then you should be able to use the following code for tracking events.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onAction: (type, definition, block) => {
    switch (type) {
      case "start":
        gtag("event", "tripetto_start", {
          description: "Form is started.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "pause":
        gtag("event", "tripetto_pause", {
          description: "Form is paused.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "complete":
        gtag("event", "tripetto_complete", {
          description: "Form is completed.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "stage":
        gtag("event", "tripetto_stage", {
          description: "Form block becomes available.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "unstage":
        gtag("event", "tripetto_unstage", {
          description: "Form block becomes unavailable.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "focus":
        gtag("event", "tripetto_focus", {
          description: "Form input element gained focus.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "blur":
        gtag("event", "tripetto_blur", {
          description: "Form input element lost focus.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
    }
  },
  //highlight-end
});
```

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
import { run } from "@tripetto/runner-chat";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onAction: (type, definition, block) => {
    switch (type) {
      case "start":
        gtag("event", "tripetto_start", {
          description: "Form is started.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "pause":
        gtag("event", "tripetto_pause", {
          description: "Form is paused.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "complete":
        gtag("event", "tripetto_complete", {
          description: "Form is completed.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "stage":
        gtag("event", "tripetto_stage", {
          description: "Form block becomes available.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "unstage":
        gtag("event", "tripetto_unstage", {
          description: "Form block becomes unavailable.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "focus":
        gtag("event", "tripetto_focus", {
          description: "Form input element gained focus.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "blur":
        gtag("event", "tripetto_blur", {
          description: "Form input element lost focus.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
    }
  },
  //highlight-end
});
```

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
import { run } from "@tripetto/runner-classic";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onAction: (type, definition, block) => {
    switch (type) {
      case "start":
        gtag("event", "tripetto_start", {
          description: "Form is started.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "pause":
        gtag("event", "tripetto_pause", {
          description: "Form is paused.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "complete":
        gtag("event", "tripetto_complete", {
          description: "Form is completed.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "stage":
        gtag("event", "tripetto_stage", {
          description: "Form block becomes available.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "unstage":
        gtag("event", "tripetto_unstage", {
          description: "Form block becomes unavailable.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "focus":
        gtag("event", "tripetto_focus", {
          description: "Form input element gained focus.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "blur":
        gtag("event", "tripetto_blur", {
          description: "Form input element lost focus.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
    }
  },
  //highlight-end
});
```

</TabItem>
</Tabs>

## ⚙️ Google Tag Manager {#gtm}
First, make sure to enable [Google Tag Manager](https://tagmanager.google.com/) in your application. Then you should be able to use the following code for tracking events.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onAction: (type, definition, block) => {
    switch (type) {
      case "start":
        dataLayer.push({
          event: "tripetto_start",
          description: "Form is started.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "pause":
        dataLayer.push({
          event: "tripetto_pause",
          description: "Form is paused.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "complete":
        dataLayer.push({
          event: "tripetto_complete",
          description: "Form is completed.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "stage":
        dataLayer.push({
          event: "tripetto_stage",
          description: "Form block becomes available.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "unstage":
        dataLayer.push({
          event: "tripetto_unstage",
          description: "Form block becomes unavailable.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "focus":
        dataLayer.push({
          event: "tripetto_focus",
          description: "Form input element gained focus.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "blur":
        dataLayer.push({
          event: "tripetto_blur",
          description: "Form input element lost focus.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
    }
  },
  //highlight-end
});
```

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
import { run } from "@tripetto/runner-chat";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onAction: (type, definition, block) => {
    switch (type) {
      case "start":
        dataLayer.push({
          event: "tripetto_start",
          description: "Form is started.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "pause":
        dataLayer.push({
          event: "tripetto_pause",
          description: "Form is paused.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "complete":
        dataLayer.push({
          event: "tripetto_complete",
          description: "Form is completed.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "stage":
        dataLayer.push({
          event: "tripetto_stage",
          description: "Form block becomes available.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "unstage":
        dataLayer.push({
          event: "tripetto_unstage",
          description: "Form block becomes unavailable.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "focus":
        dataLayer.push({
          event: "tripetto_focus",
          description: "Form input element gained focus.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "blur":
        dataLayer.push({
          event: "tripetto_blur",
          description: "Form input element lost focus.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
    }
  },
  //highlight-end
});
```

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
import { run } from "@tripetto/runner-classic";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onAction: (type, definition, block) => {
    switch (type) {
      case "start":
        dataLayer.push({
          event: "tripetto_start",
          description: "Form is started.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "pause":
        dataLayer.push({
          event: "tripetto_pause",
          description: "Form is paused.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "complete":
        dataLayer.push({
          event: "tripetto_complete",
          description: "Form is completed.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "stage":
        dataLayer.push({
          event: "tripetto_stage",
          description: "Form block becomes available.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "unstage":
        dataLayer.push({
          event: "tripetto_unstage",
          description: "Form block becomes unavailable.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "focus":
        dataLayer.push({
          event: "tripetto_focus",
          description: "Form input element gained focus.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "blur":
        dataLayer.push({
          event: "tripetto_blur",
          description: "Form input element lost focus.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
    }
  },
  //highlight-end
});
```

</TabItem>
</Tabs>

## ⚙️ Facebook Pixel {#facebook-pixel}
First, make sure to enable [Facebook Pixel](https://developers.facebook.com/docs/facebook-pixel/get-started) in your application. Then you should be able to use the following code for tracking events.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onAction: (type, definition, block) => {
    switch (type) {
      case "start":
        fbq("trackCustom", "TripettoStart", {
          description: "Form is started.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "pause":
        fbq("trackCustom", "TripettoPause", {
          description: "Form is paused.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "complete":
        fbq("trackCustom", "TripettoComplete", {
          description: "Form is completed.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "stage":
        fbq("trackCustom", "TripettoStage", {
          description: "Form block becomes available.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "unstage":
        fbq("trackCustom", "TripettoUnstage", {
          description: "Form block becomes unavailable.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "focus":
        fbq("trackCustom", "TripettoFocus", {
          description: "Form input element gained focus.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "blur":
        fbq("trackCustom", "TripettoBlur", {
          description: "Form input element lost focus.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
    }
  },
  //highlight-end
});
```

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
import { run } from "@tripetto/runner-chat";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onAction: (type, definition, block) => {
    switch (type) {
      case "start":
        fbq("trackCustom", "TripettoStart", {
          description: "Form is started.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "pause":
        fbq("trackCustom", "TripettoPause", {
          description: "Form is paused.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "complete":
        fbq("trackCustom", "TripettoComplete", {
          description: "Form is completed.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "stage":
        fbq("trackCustom", "TripettoStage", {
          description: "Form block becomes available.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "unstage":
        fbq("trackCustom", "TripettoUnstage", {
          description: "Form block becomes unavailable.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "focus":
        fbq("trackCustom", "TripettoFocus", {
          description: "Form input element gained focus.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "blur":
        fbq("trackCustom", "TripettoBlur", {
          description: "Form input element lost focus.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
    }
  },
  //highlight-end
});
```

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
import { run } from "@tripetto/runner-classic";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onAction: (type, definition, block) => {
    switch (type) {
      case "start":
        fbq("trackCustom", "TripettoStart", {
          description: "Form is started.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "pause":
        fbq("trackCustom", "TripettoPause", {
          description: "Form is paused.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "complete":
        fbq("trackCustom", "TripettoComplete", {
          description: "Form is completed.",
          form: definition.name,
          fingerprint: definition.fingerprint,
        });
        break;
      case "stage":
        fbq("trackCustom", "TripettoStage", {
          description: "Form block becomes available.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "unstage":
        fbq("trackCustom", "TripettoUnstage", {
          description: "Form block becomes unavailable.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "focus":
        fbq("trackCustom", "TripettoFocus", {
          description: "Form input element gained focus.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
      case "blur":
        fbq("trackCustom", "TripettoBlur", {
          description: "Form input element lost focus.",
          form: definition.name,
          fingerprint: definition.fingerprint,
          block: block.name,
          key: block.id,
        });
        break;
    }
  },
  //highlight-end
});
```

</TabItem>
</Tabs>

## 📖 Reference {#reference}

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

Have a look at the complete [autoscroll runner API reference](../../api/stock/autoscroll/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`run`](../../api/stock/autoscroll/functions/run.mdx)
- [`onAction`](../../api/stock/autoscroll/interfaces/IAutoscroll.mdx#onAction)

</TabItem>
<TabItem value="chat" label="Chat">

Have a look at the complete [chat runner API reference](../../api/stock/chat/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`run`](../../api/stock/chat/functions/run.mdx)
- [`onAction`](../../api/stock/chat/interfaces/IChat.mdx#onAction)

</TabItem>
<TabItem value="classic" label="Classic">

Have a look at the complete [classic runner API reference](../../api/stock/classic/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`run`](../../api/stock/classic/functions/run.mdx)
- [`onAction`](../../api/stock/classic/interfaces/IClassic.mdx#onAction)

</TabItem>
</Tabs>
