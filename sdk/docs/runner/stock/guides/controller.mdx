---
title: Controlling the runner - Guides - Stock runners
sidebar_label: Controlling the runner
sidebar_position: 15
description: To interact with the runner you need a reference to the runner instance.
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Controlling the runner
To interact with the runner you need a reference to the runner instance. That allows you to perform actions like pause, stop and start the runner. You can also retrieve the current state of the runner. All possible functions and properties are in the [reference](#reference).

:::tip
If you are using the React component of the runner, have a look [here](#using-react).
:::

## 🕹️ Retrieving the runner reference {#retrieving}
The runner reference is retrieved from the [`run`](#reference) function. This is an async function, so you should use [`await`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function) or the promise to retrieve the reference.

### Using await
In a modern browser environment, you can use [`await`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function) to obtain the reference.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";

// Retrieve the runner reference using await
const runner = await run({
  definition: /* Supply your form definition here */,
});

// Now we can interact with the runner
runner.restart();
```

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
import { run } from "@tripetto/runner-chat";

// Retrieve the runner reference using await
const runner = await run({
  definition: /* Supply your form definition here */,
});

// Now we can interact with the runner
runner.restart();
```

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
import { run } from "@tripetto/runner-classic";

// Retrieve the runner reference using await
const runner = await run({
  definition: /* Supply your form definition here */,
});

// Now we can interact with the runner
runner.restart();
```

</TabItem>
</Tabs>

### Using the promise
If you can't use [`await`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function), then use the promise.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";

// Retrieve the runner reference using promise
run({
  definition: /* Supply your form definition here */,
}).then((runner) => {
  // Now we can interact with the runner
  runner.restart();
});
```

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
import { run } from "@tripetto/runner-chat";

// Retrieve the runner reference using promise
run({
  definition: /* Supply your form definition here */,
}).then((runner) => {
  // Now we can interact with the runner
  runner.restart();
});
```

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
import { run } from "@tripetto/runner-classic";

// Retrieve the runner reference using promise
run({
  definition: /* Supply your form definition here */,
}).then((runner) => {
  // Now we can interact with the runner
  runner.restart();
});
```

</TabItem>
</Tabs>

### Using React
If you use the React component of the runner, you can use the controller property to retrieve the reference.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```ts showLineNumbers
import { AutoscrollRunner, IAutoscrollController } from "@tripetto/runner-autoscroll";
import { useRef } from "react";

function ExampleApp() {
  //highlight-start
  const controllerRef = useRef<IAutoscrollController>();
  //highlight-end

  return (
    <AutoscrollRunner
        definition={/* Supply your form definition here */}
        //highlight-start
        controller={controllerRef}
        //highlight-end
      />
  );
}
```

</TabItem>
<TabItem value="chat" label="Chat">

```ts showLineNumbers
import { ChatRunner, IChatController } from "@tripetto/runner-chat";
import { useRef } from "react";

function ExampleApp() {
  //highlight-start
  const controllerRef = useRef<IChatController>();
  //highlight-end

  return (
    <ChatRunner
        definition={/* Supply your form definition here */}
        //highlight-start
        controller={controllerRef}
        //highlight-end
      />
  );
}
```

</TabItem>
<TabItem value="classic" label="Classic">

```ts showLineNumbers
import { ClassicRunner, IClassicController } from "@tripetto/runner-chat";
import { useRef } from "react";

function ExampleApp() {
  //highlight-start
  const controllerRef = useRef<IClassicController>();
  //highlight-end

  return (
    <ClassicRunner
        definition={/* Supply your form definition here */}
        //highlight-start
        controller={controllerRef}
        //highlight-end
      />
  );
}
```

</TabItem>
</Tabs>

## 📖 Reference {#reference}

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

Have a look at the complete [autoscroll runner API reference](../../api/stock/autoscroll/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`run`](../../api/stock/autoscroll/functions/run.mdx)
- [`AutoscrollRunner`](../../api/stock/autoscroll/components/react.mdx)
- [`IAutoscrollController`](../../api/stock/autoscroll/interfaces/IAutoscrollController.mdx)

</TabItem>
<TabItem value="chat" label="Chat">

Have a look at the complete [chat runner API reference](../../api/stock/chat/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`run`](../../api/stock/chat/functions/run.mdx)
- [`ChatRunner`](../../api/stock/chat/components/react.mdx)
- [`IChatController`](../../api/stock/chat/interfaces/IChatController.mdx)

</TabItem>
<TabItem value="classic" label="Classic">

Have a look at the complete [classic runner API reference](../../api/stock/classic/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`run`](../../api/stock/classic/functions/run.mdx)
- [`ClassicRunner`](../../api/stock/classic/components/react.mdx)
- [`IClassicController`](../../api/stock/classic/interfaces/IClassicController.mdx)

</TabItem>
</Tabs>

