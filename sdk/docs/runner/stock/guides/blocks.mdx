---
title: Custom blocks - Guides - Stock runners
sidebar_label: Custom blocks
sidebar_position: 20
description: You can extend Tripetto with custom blocks (question types).
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Custom blocks
You can extend Tripetto with custom blocks (question types). When you develop a custom block, there are always two parts that you need to develop:
- The builder part;
- The runner part.

The builder part allows editors to use your block in the builder. It instructs the builder on how to handle the block and defines the configurable properties and settings for the block.

The runner part, on the other hand, contains the rendering of the block inside the runner. It defines the UI for the block and makes the block visible in the runner. This guide shows how you can create the runner part of a custom block for the stock runners.

:::info
Since the runner part defines the actual UI of the block, you need an implementation for each stock runner you want to use.
:::

:::tip
See the [blocks](../../../blocks/introduction.md) documentation to learn how to create the [builder part](../../../blocks/custom/implement/visual.mdx#builder-part) of a custom block.
:::

## 📃 Basic implementation
To define a new block for a stock runner, you need to register the block to the runner using the [`@tripetto`](../../api/library/decorators/tripetto.mdx) decorator. This allows the runner to find and use your custom block. The code below shows the basic structure of a custom block for a stock runner. The most important part is the `render` method (or the `answer` and `input` method in the chat runner), where you define the UI for your block. The stock runners are built using [React](https://react.dev). So you need to use React when creating custom blocks for the stock runners.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```ts showLineNumbers title="custom-block.tsx"
import { tripetto, NodeBlock } from "@tripetto/runner";
import { namespace, IAutoscrollRenderProps, IAutoscrollRendering } from "@tripetto/runner-autoscroll";
import { ReactNode } from "react";

@tripetto({
    type: "node",

    // Feed the namespace of the stock runner here.
    // This is important!
    namespace,

    // This is the unique identifier for your block
    identifier: "your-custom-block",
})
export class YourCustomBlock extends NodeBlock implements IAutoscrollRendering {
    // This render function is invoked when your block renders in the runner UI.
    render(props: IAutoscrollRenderProps): ReactNode {
        return (
            <>
                Welcome to your custom block!
            </>
        );
    }
}
```

</TabItem>
<TabItem value="chat" label="Chat">

```ts showLineNumbers title="custom-block.tsx"
import { tripetto, NodeBlock } from "@tripetto/runner";
import { namespace, IChatRenderProps, IChatRendering } from "@tripetto/runner-chat";
import { ReactNode } from "react";

@tripetto({
    type: "node",

    // Feed the namespace of the stock runner here.
    // This is important!
    namespace,

    // This is the unique identifier for your block
    identifier: "your-custom-block",
})
export class YourCustomBlock extends NodeBlock implements IChatRendering {
    // This defines the UI of the block within a chat question bubble
    question(props: IChatRenderProps): ReactNode {
        return (
            <>
                This block has a question!
            </>
        );
    }

    // This defines the UI of the block within a chat answer bubble
    answer(props: IChatRenderProps): ReactNode {
        return (
            <>
                This block has an answer!
            </>
        );
    }

    // This defines the UI for the block in the chat input area
    input(props: IChatRenderProps): ReactNode {
        return (
            <>
                This block needs an answer!
            </>
        );
    }
}
```

</TabItem>
<TabItem value="classic" label="Classic">

```ts showLineNumbers title="custom-block.tsx"
import { tripetto, NodeBlock } from "@tripetto/runner";
import { namespace, IClassicRenderProps, IClassicRendering } from "@tripetto/runner-classic";
import { ReactNode } from "react";

@tripetto({
    type: "node",

    // Feed the namespace of the stock runner here.
    // This is important!
    namespace,

    // This is the unique identifier for your block
    identifier: "your-custom-block",
})
export class YourCustomBlock extends NodeBlock implements IClassicRendering {
    // This render function is invoked when your block renders in the runner UI.
    render(props: IClassicRenderProps): ReactNode {
        return (
            <>
                Welcome to your custom block!
            </>
        );
    }
}
```

</TabItem>
</Tabs>

:::info
Tripetto uses [decorators](https://www.typescriptlang.org/docs/handbook/decorators.html) for registering blocks. Therefore, you should always use [TypeScript](https://www.typescriptlang.org/) for developing blocks. Make sure to enable the [`experimentalDecorators`](https://www.typescriptlang.org/tsconfig#experimentalDecorators) feature.
:::

### Using your custom block
Make sure to import your custom block in the file where you use the runner. You only have to import the file. The block will self-register and become available in the runner.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";
//highlight-start
import "custom-block";
//highlight-end

run({
  definition: /* Supply your form definition here */
});
```

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
import { run } from "@tripetto/runner-chat";
//highlight-start
import "custom-block";
//highlight-end

run({
  definition: /* Supply your form definition here */
});
```

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
import { run } from "@tripetto/runner-classic";
//highlight-start
import "custom-block";
//highlight-end

run({
  definition: /* Supply your form definition here */
});
```

</TabItem>
</Tabs>

## 📰 Handling markdown
Tripetto supports markdown to add basic text markup (like bold, italic, underline) and references to other blocks in the form. If your block implements properties that need markdown support, you need to run them through one if the [markdown functions](#reference).

```ts showLineNumbers title="custom-block.tsx"
import { tripetto, NodeBlock } from "@tripetto/runner";
import { namespace, IAutoscrollRenderProps, IAutoscrollRendering } from "@tripetto/runner-autoscroll";
import { ReactNode } from "react";

@tripetto({
  namespace,
  type: "node",
  identifier: "your-custom-block",
})
export class YourCustomBlock extends NodeBlock<{
  customPropWithMarkdown: string;
}> implements IAutoscrollRendering {
  render(props: IAutoscrollRenderProps): ReactNode {
    return (
      <>
        Welcome to your custom block!
        //highlight-start
        {props.markdownifyToJSX(this.props.customPropWithMarkdown)}
        //highlight-end
      </>
    );
  }
}
```

## 💅 Using Styled Components
The stock runners are built using [Styled Components](https://styled-components.com/). You can use Styled Components in your custom blocks rendering, but you need to make sure to import the [`styled`](https://styled-components.com/docs/api#styled) method from the stock runner itself (otherwise, you might end up with two copies of Styled Components in your JS bundle).

```ts showLineNumbers title="custom-block.tsx"
import { tripetto, NodeBlock } from "@tripetto/runner";
import { namespace, IAutoscrollRenderProps, IAutoscrollRendering } from "@tripetto/runner-autoscroll";
import { ReactNode } from "react";
//highlight-start
import { styled } from "@tripetto/runner-autoscroll";
//highlight-end

//highlight-start
const Button = styled.button`
  background: palevioletred;
  border-radius: 3px;
  border: none;
  color: white;
`;
//highlight-end

@tripetto({
  namespace,
  type: "node",
  identifier: "your-custom-block",
})
export class YourCustomBlock extends NodeBlock implements IAutoscrollRendering {
  render(props: IAutoscrollRenderProps): ReactNode {
    return (
      <>
        Welcome to your custom block!
        //highlight-start
        <Button />
        //highlight-end
      </>
    );
  }
}
```

## 👔 Using fabric components
The stock runners use the input components from the [Tripetto Runner Fabric](../../api/fabric/index.md) package. If you want, you can use these components in your custom block. For convenience, you can import any fabric component directly from the stock runner packages. See the [Runner Fabric reference](../../api/fabric/index.md#components) for an overview of all components included.

```ts showLineNumbers title="custom-block.tsx"
import { tripetto, NodeBlock } from "@tripetto/runner";
import { namespace, IAutoscrollRenderProps, IAutoscrollRendering } from "@tripetto/runner-autoscroll";
import { ReactNode } from "react";
//highlight-start
import { CheckboxFabric } from "@tripetto/runner-autoscroll";
//highlight-end

@tripetto({
  namespace,
  type: "node",
  identifier: "your-custom-block",
})
export class YourCustomBlock extends NodeBlock implements IAutoscrollRendering {
  render(props: IAutoscrollRenderProps): ReactNode {
    return (
      <>
        Welcome to your custom block!
        //highlight-start
        <CheckboxFabric
            styles={props.styles.checkboxes}
            tabIndex={props.tabIndex}
            onAutoFocus={props.autoFocus}
            onFocus={props.focus}
            onBlur={props.blur}
        />
        //highlight-end
      </>
    );
  }
}
```

## 📖 Reference {#reference}

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

Have a look at the complete [autoscroll runner API reference](../../api/stock/autoscroll/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`tripetto`](../../api/library/decorators/tripetto.mdx)
- [`NodeBlock`](../../api/library/classes/NodeBlock.mdx)
- [`namespace`](../../api/stock/autoscroll/constants/namespace.mdx)
- [`IAutoscrollRendering`](../../api/stock/autoscroll/interfaces/IAutoscrollRendering.mdx)
- [`IAutoscrollRenderProps`](../../api/stock/autoscroll/interfaces/IAutoscrollRenderProps.mdx)
- [`markdownifyToJSX`](../../api/stock/autoscroll/interfaces/IAutoscrollRenderProps.mdx#markdownifyToJSX)
- [`markdownifyToURL`](../../api/stock/autoscroll/interfaces/IAutoscrollRenderProps.mdx#markdownifyToURL)
- [`markdownifyToImage`](../../api/stock/autoscroll/interfaces/IAutoscrollRenderProps.mdx#markdownifyToImage)
- [`markdownifyToString`](../../api/stock/autoscroll/interfaces/IAutoscrollRenderProps.mdx#markdownifyToString)

</TabItem>
<TabItem value="chat" label="Chat">

Have a look at the complete [chat runner API reference](../../api/stock/chat/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`tripetto`](../../api/library/decorators/tripetto.mdx)
- [`NodeBlock`](../../api/library/classes/NodeBlock.mdx)
- [`namespace`](../../api/stock/chat/constants/namespace.mdx)
- [`IChatRendering`](../../api/stock/chat/interfaces/IChatRendering.mdx)
- [`IChatRenderProps`](../../api/stock/chat/interfaces/IChatRenderProps.mdx)
- [`markdownifyToJSX`](../../api/stock/chat/interfaces/IChatRenderProps.mdx#markdownifyToJSX)
- [`markdownifyToURL`](../../api/stock/chat/interfaces/IChatRenderProps.mdx#markdownifyToURL)
- [`markdownifyToImage`](../../api/stock/chat/interfaces/IChatRenderProps.mdx#markdownifyToImage)
- [`markdownifyToString`](../../api/stock/chat/interfaces/IChatRenderProps.mdx#markdownifyToString)

</TabItem>
<TabItem value="classic" label="Classic">

Have a look at the complete [classic runner API reference](../../api/stock/classic/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`tripetto`](../../api/library/decorators/tripetto.mdx)
- [`NodeBlock`](../../api/library/classes/NodeBlock.mdx)
- [`namespace`](../../api/stock/classic/constants/namespace.mdx)
- [`IClassicRendering`](../../api/stock/classic/interfaces/IClassicRendering.mdx)
- [`IClassicRenderProps`](../../api/stock/classic/interfaces/IClassicRenderProps.mdx)
- [`markdownifyToJSX`](../../api/stock/classic/interfaces/IClassicRenderProps.mdx#markdownifyToJSX)
- [`markdownifyToURL`](../../api/stock/classic/interfaces/IClassicRenderProps.mdx#markdownifyToURL)
- [`markdownifyToImage`](../../api/stock/classic/interfaces/IClassicRenderProps.mdx#markdownifyToImage)
- [`markdownifyToString`](../../api/stock/classic/interfaces/IClassicRenderProps.mdx#markdownifyToString)

</TabItem>
</Tabs>
