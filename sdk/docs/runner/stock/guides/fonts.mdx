---
title: Using fonts - Guides - Stock runners
sidebar_label: Using fonts
sidebar_position: 3
description: You can select the font you want to use for the runner. This works by setting the font property of the styles object.
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Using fonts
You can select the font you want to use for the runner. This works by setting the [`font`](#reference) property of the [`styles`](#reference) object. The font can load from 3 different sources:

- [Regular font (from local client)](#regular);
- [Font from Google Fonts](#google);
- [Custom font from a custom URL](#custom).

## 🗛 Use a regular font {#regular}
Use one of the following regular fonts that are supported by most browsers:
- Arial
- Arial Black
- Comic Sans MS
- Courier New
- Georgia
- Garamond
- Helvetica
- Impact
- Palatino
- Sans-Serif
- Times New Roman
- Trebuchet MS
- Verdana

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  styles: {
    font: {
      family: "Arial"
    }
  }
  //highlight-end
});
```

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
import { run } from "@tripetto/runner-chat";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  styles: {
    font: {
      family: "Arial"
    }
  }
  //highlight-end
});
```

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
import { run } from "@tripetto/runner-classic";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  styles: {
    font: {
      family: "Arial"
    }
  }
  //highlight-end
});
```

</TabItem>
</Tabs>

## 🌍 Use a Google font {#google}
If you use any other font family name than the regular ones listed above, the font tries to load from [Google Fonts](https://fonts.google.com/). So, you can search for a nice font on Google Fonts and use it in the runner. Make sure to copy the exact name of the font you want to use. The font name is case-sensitive.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  styles: {
    font: {
      // This font will load from Google Fonts
      family: "Poppins"
    }
  }
  //highlight-end
});
```

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
import { run } from "@tripetto/runner-chat";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  styles: {
    font: {
      // This font will load from Google Fonts
      family: "Poppins"
    }
  }
  //highlight-end
});
```

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
import { run } from "@tripetto/runner-classic";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  styles: {
    font: {
      // This font will load from Google Fonts
      family: "Poppins"
    }
  }
  //highlight-end
});
```

</TabItem>
</Tabs>

## 👩‍💻 Use a custom font {#custom}
Another option is to host the font on a custom domain. To do this, you need to create a font CSS file that specifies the required fonts for the runner.

### Create the font CSS file
Create the font CSS file and host it together with the font files on a custom domain. An example `my-font.css` might look something like this:

```css showLineNumbers title="my-font.css"
@font-face {
  font-family: 'My Font';
  font-style: normal;
  font-weight: normal; /* or 400 */
  src: url("my_font_regular.woff") format("woff");
}
@font-face {
  font-family: 'My Font';
  font-style: italic;
  font-weight: normal; /* or 400 */
  src: url("my_font_italic.woff") format("woff");
}
@font-face {
  font-family: 'My Font';
  font-style: normal;
  font-weight: bold; /* or 700 */
  src: url("my_font_bold.woff") format("woff");
}
@font-face {
  font-family: 'My Font';
  font-style: italic;
  font-weight: bold; /* or 700 */
  src: url("my_font_bold_italic.woff") format("woff");
}
```

:::info
The runner uses `font-weight` 400 for regular text and `font-weight` 700 for bold text. Also, the runner supports italic text. So that should be defined as well for both font weights.
:::

### Load the font CSS file
When you have created the CSS file, you can configure the runner to use it. To do so, you need to specify the URL to the font CSS file. Append that URL with a hash sign `#`, followed by the name of the font. This allows the font loader to load the font CSS file and then use the font with the given name.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";

run({
  definition: /* Supply your form definition here */,
  styles: {
    font: {
      //highlight-start
      family: "https://yourdomain.com/my-font.css#My%20Font"
      //highlight-end
    }
  }
});
```

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
import { run } from "@tripetto/runner-chat";

run({
  definition: /* Supply your form definition here */,
  styles: {
    font: {
      //highlight-start
      family: "https://yourdomain.com/my-font.css#My%20Font"
      //highlight-end
    }
  }
});
```

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
import { run } from "@tripetto/runner-classic";

run({
  definition: /* Supply your form definition here */,
  styles: {
    font: {
      //highlight-start
      family: "https://yourdomain.com/my-font.css#My%20Font"
      //highlight-end
    }
  }
});
```

</TabItem>
</Tabs>

:::info
You can specify a relative path to the font CSS file instead of an absolute URL as long as you start it with a forward slash `/`. For example, `/my-font.css#My%20Font`.
:::

:::warning
Make sure to apply proper URL encoding for the font name in the URL. Spaces, for example, should be written as `%20`. You can use an online [URL encoder](https://www.urlencoder.org/) to do this. Also, the font name is case sensitive.
:::

:::warning
If you specify an absolute URL it should always use `https://`. Unsecure HTTP requests are only supported when using a relative path to the font CSS file (this requires that the runner and font CSS file are on the same domain).
:::

## 📖 Reference {#reference}

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

Have a look at the complete [autoscroll runner API reference](../../api/stock/autoscroll/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`run`](../../api/stock/autoscroll/functions/run.mdx)
- [`styles`](../../api/stock/autoscroll/interfaces/IAutoscroll.mdx#styles)
- [`font`](../../api/stock/autoscroll/interfaces/IAutoscrollStyles.mdx#font)

</TabItem>
<TabItem value="chat" label="Chat">

Have a look at the complete [chat runner API reference](../../api/stock/chat/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`run`](../../api/stock/chat/functions/run.mdx)
- [`styles`](../../api/stock/chat/interfaces/IChat.mdx#styles)
- [`font`](../../api/stock/chat/interfaces/IChatStyles.mdx#font)

</TabItem>
<TabItem value="classic" label="Classic">

Have a look at the complete [classic runner API reference](../../api/stock/classic/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`run`](../../api/stock/classic/functions/run.mdx)
- [`styles`](../../api/stock/classic/interfaces/IClassic.mdx#styles)
- [`font`](../../api/stock/classic/interfaces/IClassicStyles.mdx#font)

</TabItem>
</Tabs>
