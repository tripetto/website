---
title: Style forms - Guides - Stock runners
sidebar_label: Style forms
sidebar_position: 2
description: The visual appearance of the stock runners can be modified. For example, you can adjust the font face, text size, and colors.
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Style forms
The visual appearance of the stock runners can be modified. For example, you can adjust the font face, text size, and colors. Each stock runner may contain different style settings. Please look at the [styles reference](#reference) of each stock runner to see all the possible settings.

## 🎨 Applying styles {#styles}
To apply styles to the runner, you need to specify a styles object. Supply it to the [`styles`](#reference) property of the [`run`](#reference) function as shown in the following example.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  styles: {
    background: {
      color: "blue",
    },
    font: {
      family: "Arial",
      size: 12,
    },
  },
  //highlight-end
});
```

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
import { run } from "@tripetto/runner-chat";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  styles: {
    background: {
      color: "blue",
    },
    font: {
      family: "Arial",
      size: 12,
    },
  },
  //highlight-end
});
```

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
import { run } from "@tripetto/runner-classic";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  styles: {
    background: {
      color: "blue",
    },
    font: {
      family: "Arial",
      size: 12,
    },
  },
  //highlight-end
});
```

</TabItem>
</Tabs>

:::tip
It is possible to manage the styles of the runner using the builder. See this [guide](../../../builder/integrate/guides/livepreview.mdx#styles) to learn how to implement that.
:::

## 📂 Dynamic loading styles {#dynamic}
Since the style settings are stored as JavaScript objects (JSON strings), you can load them dynamically during runtime. To make it even easier, the [`styles`](#reference) property also accepts a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise). You can use that to implement asynchronous loading of externally stored styles.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  styles: new Promise((resolve) => {
    // This example uses fetch to retrieve a JSON string with styles
    fetch("/styles.json").then((response) => {
      response.json().then((styles) => resolve(styles));
    });
  }),
  //highlight-end
});
```

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
import { run } from "@tripetto/runner-chat";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  styles: new Promise((resolve) => {
    // This example uses fetch to retrieve a JSON string with styles
    fetch("/styles.json").then((response) => {
      response.json().then((styles) => resolve(styles));
    });
  }),
  //highlight-end
});
```

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
import { run } from "@tripetto/runner-classic";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  styles: new Promise((resolve) => {
    // This example uses fetch to retrieve a JSON string with styles
    fetch("/styles.json").then((response) => {
      response.json().then((styles) => resolve(styles));
    });
  }),
  //highlight-end
});
```

</TabItem>
</Tabs>

## 👩‍💻 Custom CSS {#custom}
It is possible to apply custom CSS and styles to the HTML element that hosts the runner and to the HTML elements of the blocks inside the runner.

### Setting a custom CSS class {#custom-class}
To specify a custom CSS class for the runner HTML element, use the [`className`](#reference) property.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  className: "your-custom-class-name"
  //highlight-end
});
```

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
import { run } from "@tripetto/runner-chat";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  className: "your-custom-class-name"
  //highlight-end
});
```

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
import { run } from "@tripetto/runner-classic";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  className: "your-custom-class-name"
  //highlight-end
});
```

</TabItem>
</Tabs>

### Adding custom styles to the runner element {#custom-style}
To specify custom styles for the runner HTML element, use the [`customStyle`](#reference) property. You can use it, for example, to set a fixed height for the runner (which can be useful if you set the [`display`](display.mdx) mode to `inline`).

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  customStyle: {
    height: "100px"
  }
  //highlight-end
});
```

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
import { run } from "@tripetto/runner-chat";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  customStyle: {
    height: "100px"
  }
  //highlight-end
});
```

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
import { run } from "@tripetto/runner-classic";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  customStyle: {
    height: "100px"
  }
  //highlight-end
});
```

</TabItem>
</Tabs>

### Adding custom styles to blocks {#custom-block-styles}
There is also a property to add custom CSS styles to the blocks (input fields) of the runner. The custom CSS is supplied as a string to the [`customCSS`](#reference) property of the runner. Multiple CSS rules can be separated with a new line and nesting within CSS rules is supported. Each block is referenced by its block identifier (for the stock blocks, this identifier is always prefixed with `@tripetto/block-` followed by the lowercase name of the block).

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  customCSS: `
        [data-block="@tripetto/block-text"] { background-color: blue; }
        [data-block="@tripetto/block-checkbox"] {
            input {
                background-color: red;
            }
        }
    `,
  //highlight-end
});
```
[![Run](/img/button-run.svg)](https://eg2bu.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-autoscroll-custom-css-eg2bu?file=/src/index.ts:1205-1382)
:::tip
Have a look [here](../faces/autoscroll.mdx#blocks) to see which blocks are supported by the runner.
:::

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
import { run } from "@tripetto/runner-chat";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  customCSS: `
        [data-block="@tripetto/block-text"] { background-color: blue; }
        [data-block="@tripetto/block-checkbox"] {
            input {
                background-color: red;
            }
        }
    `,
  //highlight-end
});
```
[![Run](/img/button-run.svg)](https://mbme2.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-chat-custom-css-mbme2?file=/src/index.ts:1200-1376)
:::tip
Have a look [here](../faces/chat.mdx#blocks) to see which blocks are supported by the runner.
:::

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
import { run } from "@tripetto/runner-classic";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  customCSS: `
        [data-block="@tripetto/block-text"] { background-color: blue; }
        [data-block="@tripetto/block-checkbox"] {
            input {
                background-color: red;
            }
        }
    `,
  //highlight-end
});
```
[![Run](/img/button-run.svg)](https://zbu74.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-classic-custom-css-zbu74?file=/src/index.ts:1204-1379)
:::tip
Have a look [here](../faces/classic.mdx#blocks) to see which blocks are supported by the runner.
:::

</TabItem>
</Tabs>

## 📖 Reference {#reference}

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

Have a look at the complete [autoscroll runner API reference](../../api/stock/autoscroll/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`run`](../../api/stock/autoscroll/functions/run.mdx)
- [`styles`](../../api/stock/autoscroll/interfaces/IAutoscroll.mdx#styles)
- [`className`](../../api/stock/autoscroll/interfaces/IAutoscroll.mdx#className)
- [`customStyle`](../../api/stock/autoscroll/interfaces/IAutoscroll.mdx#customStyle)
- [`customCSS`](../../api/stock/autoscroll/interfaces/IAutoscroll.mdx#customCSS)
- [`IAutoscrollStyles`](../../api/stock/autoscroll/interfaces/IAutoscrollStyles.mdx)

</TabItem>
<TabItem value="chat" label="Chat">

Have a look at the complete [chat runner API reference](../../api/stock/chat/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`run`](../../api/stock/chat/functions/run.mdx)
- [`styles`](../../api/stock/chat/interfaces/IChat.mdx#styles)
- [`className`](../../api/stock/chat/interfaces/IChat.mdx#className)
- [`customStyle`](../../api/stock/chat/interfaces/IChat.mdx#customStyle)
- [`customCSS`](../../api/stock/chat/interfaces/IChat.mdx#customCSS)
- [`IChatStyles`](../../api/stock/chat/interfaces/IChatStyles.mdx)

</TabItem>
<TabItem value="classic" label="Classic">

Have a look at the complete [classic runner API reference](../../api/stock/classic/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`run`](../../api/stock/classic/functions/run.mdx)
- [`styles`](../../api/stock/classic/interfaces/IClassic.mdx#styles)
- [`className`](../../api/stock/classic/interfaces/IClassic.mdx#className)
- [`customStyle`](../../api/stock/classic/interfaces/IClassic.mdx#customStyle)
- [`customCSS`](../../api/stock/classic/interfaces/IClassic.mdx#customCSS)
- [`IClassicStyles`](../../api/stock/classic/interfaces/IClassicStyles.mdx)

</TabItem>
</Tabs>
