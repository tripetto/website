---
title: Guides - Guides - Stock runners
sidebar_label: Guides
description: This chapter shows you guides to help you master the Tripetto stock runners.
---

# Guides
When you have the [basic implementation](../quickstart/index.md) of the runner up and running, it is time to dive deeper and enable more features of the runner. The following guides are available to help you master the Tripetto stock runners.

- 💾 [Collecting response data](collecting.mdx)
- 🎨 [Style forms](styles.mdx)
- 🗛 [Using fonts](fonts.mdx)
- 🖥️ [Display modes](display.mdx)
- 📝 [Prefilling forms](prefilling.mdx)
- 🔥 [Runtime data usage](runtime-data.mdx)
- ⏯️ [Enable pause and resume](pause-resume.mdx)
- 🧠 [Form data persistency](persistent.mdx)
- 📄 [File uploads](attachments.mdx)
- 🌍 [Loading translations and locale data](l10n.mdx)
- ✅ [Validating response data](validation.mdx)
- 🤖 [Prevent form spamming](spam-protection.mdx)
- 📈 [Track usage](tracking.mdx)
- 🕹️ [Controlling the runner](controller.mdx)
- 💸 [Disable Tripetto branding](branding.mdx)
- 🛡️ [Content Security Policy](csp.mdx)
- 📦 [Custom blocks](blocks.mdx)
- 📂 [Package overview](package.mdx)
