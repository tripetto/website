---
title: Package overview - Guides - Stock runners
sidebar_label: Package overview
sidebar_position: 21
description: The stock runner packages contain a collection of resources for both the runner and the builder part of Tripetto.
---

# Stock runner package overview
The stock runner packages contain a collection of resources for both the runner and the builder part of Tripetto. They all have the same structure, making it easy to swap stock runners.

## 📖 Resources {#resources}
So, what's included in the packages? Here's a list:
- **Runner library**: First and foremost there is the runner UI (included is an ESM, ES5 and UMD version);
- **Runner React component**: React component for the runner ([learn more](../quickstart/react.mdx));
- **Runner Angular component**: Angular component for the runner ([learn more](../quickstart/angular.mdx));
- **Runner locales**: Locale JSON files to load the right locale for the runner ([learn more](l10n.mdx#locale));
- **Runner translations**: Translations of the static labels and texts in the runner ([learn more](l10n.mdx#translations));
- **Builder blocks bundle**: Ready-made bundle for the builder of all blocks used in the runner ([learn more](../../../builder/integrate/guides/blocks.mdx#import-block-bundles));
- **Builder blocks translations bundle**: Ready-made bundle for the builder of all block translations ([learn more](../../../builder/integrate/guides/l10n.mdx#loading-multiple-translations));
- **Builder styles contract**: Contract to instruct the builder how to manage the styles for the runner ([learn more](../../../builder/integrate/guides/livepreview.mdx#styles));
- **Builder l10n contract**: Contract to instruct the builder how to manage the language settings and translations for the runner ([learn more](../../../builder/integrate/guides/livepreview.mdx#translations));
- **TypeScript typings**: Fully supports TypeScript out-of-the-box.

## 📂 Folder index {#index}
| Directory             | Description                                                                                                                                                                                                     |
|:----------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| /runner               | Contains all runner related resources.                                                                                                                                                                          |
| /runner/esm           | Contains the ESM build of the runner.                                                                                                                                                                           |
| /runner/es5           | Contains the ES5 build of the runner.                                                                                                                                                                           |
| /runner/index.js      | Contains the UMD build of the runner (for direct use in browser environments).                                                                                                                                  |
| /runner/types         | Contains the TypeScript typings for the runner.                                                                                                                                                                 |
| /runner/locales       | Contains the runner locale JSON files ([learn more](l10n.mdx#locale)).                                                                                                                                          |
| /runner/translations  | Contains the runner translation JSON files ([learn more](l10n.mdx#translations)).                                                                                                                               |
| /react                | Contains the runner React component.                                                                                                                                                                            |
| /angular              | Contains the runner Angular component.                                                                                                                                                                          |
| /builder              | Contains all builder related resources.                                                                                                                                                                         |
| /builder/esm          | Contains the ESM build of the builder blocks bundle.                                                                                                                                                            |
| /builder/es5          | Contains the ES5 build of the builder blocks bundle.                                                                                                                                                            |
| /builder/index.js     | Contains the UMD build of the builder blocks bundle (for direct use in browser environments).                                                                                                                   |
| /builder/types        | Contains the TypeScript typings for the builder blocks bundle.                                                                                                                                                  |
| /builder/translations | Contains the translation JSON files of the builder blocks bundle ([learn more](../../../builder/integrate/guides/l10n.mdx#loading-multiple-translations)).                                                      |
| /builder/styles       | Contains the styles contract for the runner. This contract instructs the builder which style properties can be managed for the runner ([learn more](../../../builder/integrate/guides/livepreview.mdx#styles)). |
| /builder/l10n         | Contains the l10n contract for the runner. This contract instructs the builder which labels can be translated for the runner ([learn more](../../../builder/integrate/guides/livepreview.mdx#translations)).    |

:::info
The runner packages include an [export map](https://webpack.js.org/guides/package-exports/) to declare the modules. That should make it easy to import the right resources. For example, bundlers like [webpack](https://webpack.js.org/) and [Rollup](https://rollupjs.org/) will use it to automatically select the right resource based on the bundler configuration.
:::
