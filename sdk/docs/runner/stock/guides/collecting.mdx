---
title: Collecting response data - Guides - Stock runners
sidebar_label: Collecting response data
sidebar_position: 1
description: To collect response data from the form runner, you need to use the onSubmit event.
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Collecting response data
To collect response data from the form runner, you need to use the [`onSubmit`](#reference) event. This event fires when the form completes and the response data is ready for further processing. You can also use this event to supply feedback to the runner about whether the data processing was successful or not. This guide shows you how to use the event and set up data retrieval properly.

:::tip
To retrieve response data you need to use one of the export functions. Have a look at the [`Export`](../../api/library/modules/Export.mdx) module for a list of all available export functions and detailed documentation.
:::

## 📥 Basic data collection {#basic-data-collection}
The following example shows how to use the [`onSubmit`](#reference) event. As you can see the supplied function receives a reference to the active form instance. By using one of the supplied [`Export`](../../api/library/modules/Export.mdx) functions from the Runner library you can retrieve the data in a convenient format. The following example shows how to export the data using the [`exportables`](../../api/library/modules/Export.mdx#exportables) or [`CSV`](../../api/library/modules/Export.mdx#CSV) function.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";
//highlight-start
import { Export } from "@tripetto/runner";
//highlight-end

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onSubmit: (instance) => {
    // This exports all exportable data in the form
    const exportables = Export.exportables(instance);

    // Iterate through all the fields
    exportables.fields.forEach((field) => {
      // Output each field name and value to the console
      console.log(`${field.name}: ${field.string}`);
    });

    // This exports the collected data as a CSV object
    const csv = Export.CSV(instance);

    // Output CSV to the console
    console.log(csv.fields);
    console.log(csv.record);
  }
  //highlight-end
});
```
[![Run](/img/button-run.svg)](https://60cti.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-autoscroll-handling-response-data-60cti?file=/src/index.ts:1259-1777)

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
import { run } from "@tripetto/runner-chat";
//highlight-start
import { Export } from "@tripetto/runner";
//highlight-end

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onSubmit: (instance) => {
    // This exports all exportable data in the form
    const exportables = Export.exportables(instance);

    // Iterate through all the fields
    exportables.fields.forEach((field) => {
      // Output each field name and value to the console
      console.log(`${field.name}: ${field.string}`);
    });

    // This exports the collected data as a CSV object
    const csv = Export.CSV(instance);

    // Output CSV to the console
    console.log(csv.fields);
    console.log(csv.record);
  }
  //highlight-end
});
```
[![Run](/img/button-run.svg)](https://ju5rv.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-chat-handling-response-data-ju5rv?file=/src/index.ts:1253-1771)

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
import { run } from "@tripetto/runner-classic";
//highlight-start
import { Export } from "@tripetto/runner";
//highlight-end

run({
definition: /* Supply your form definition here */,
  //highlight-start
  onSubmit: (instance) => {
    // This exports all exportable data in the form
    const exportables = Export.exportables(instance);

    // Iterate through all the fields
    exportables.fields.forEach((field) => {
      // Output each field name and value to the console
      console.log(`${field.name}: ${field.string}`);
    });

    // This exports the collected data as a CSV object
    const csv = Export.CSV(instance);

    // Output CSV to the console
    console.log(csv.fields);
    console.log(csv.record);
  }
  //highlight-end
});
```
[![Run](/img/button-run.svg)](https://k6n5z.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-classic-handling-response-data-k6n5z?file=/src/index.ts:1256-1774)

</TabItem>
</Tabs>

:::tip
See the Runner library documentation for all possible [Export](../../api/library/modules/Export.mdx) functions.
:::

## ⚠️ Error handling {#error-handling}
In most situations, the [`onSubmit`](#reference) event is the best place to transmit data to an endpoint like an API. Usually, this is an asynchronous action that might fail in some situations (like losing the connection during transmission). You can supply feedback about the operation to the runner. That way, the user sees a visual indication while data is transferring. When something goes wrong, an error/retry message pops up. To use this feature, construct a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) as shown in the following example:

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";
import { Export } from "@tripetto/runner";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onSubmit: (instance) =>
    new Promise((resolve, reject) => {
      // This example uses fetch to post data to an endpoint
      fetch("/example-server", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(Export.exportables(instance)),
      })
        .then((response) => {
          if (response.ok) {
            // All good, resolve the promise
            resolve();
          } else {
            // Not so good, reject the promise
            reject("rejected");
          }
        })
        .catch((error) => {
          // Error occurred, reject with error message
          reject(error.message);
        });
    }),
  //highlight-end
});
```

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
import { run } from "@tripetto/runner-chat";
import { Export } from "@tripetto/runner";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onSubmit: (instance) =>
    new Promise((resolve, reject) => {
      // This example uses fetch to post data to an endpoint
      fetch("/example-server", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(Export.exportables(instance)),
      })
        .then((response) => {
          if (response.ok) {
            // All good, resolve the promise
            resolve();
          } else {
            // Not so good, reject the promise
            reject("rejected");
          }
        })
        .catch((error) => {
          // Error occurred, reject with error message
          reject(error.message);
        });
    }),
  //highlight-end
});
```

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
import { run } from "@tripetto/runner-classic";
import { Export } from "@tripetto/runner";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onSubmit: (instance) =>
    new Promise((resolve, reject) => {
      // This example uses fetch to post data to an endpoint
      fetch("/example-server", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(Export.exportables(instance)),
      })
        .then((response) => {
          if (response.ok) {
            // All good, resolve the promise
            resolve();
          } else {
            // Not so good, reject the promise
            reject("rejected");
          }
        })
        .catch((error) => {
          // Error occurred, reject with error message
          reject(error.message);
        });
    }),
  //highlight-end
});
```

</TabItem>
</Tabs>

#### Reject error codes
The reject function supports two particular error codes:
- `outdated`: This indicates to the runner that the current form is an outdated version (this allows the runner to propagate the [`onReload`](#reference) event to try to load the most recent version of the form);
- `rejected`: This indicates that the data is rejected and not saved by the endpoint.

You can feed those error values to the reject function:

```ts showLineNumbers
run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onSubmit: (instance) => new Promise((resolve, reject) => {
    // This example indicates the runner that the form is outdated
    reject("outdated");
  })
  //highlight-end
});
```

:::info
Any other value provided to the reject function is considered a descriptive error message. The runner outputs this error message in the browser console. The respondent will see a common error message without technical details in it.
:::

## ✅ Validating the response data {#validating}
Tripetto contains built-in functions to make it easy to validate the response data. Static validation using the available JSON Schema is also possible. Read the [Validating response data guide](validation.mdx) to learn more.

## ↩️ Feedback to the runner {#feedback}
It is possible to feed an identifier or reference associated with the submitted response data back to the runner. Tripetto allows using this identifier in the closing message of a form. It can be presented to the user or used in the redirect URL. To do so, supply the identifier or reference to the `resolve` function of the promise.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";
import { Export } from "@tripetto/runner";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onSubmit: (instance) => new Promise((resolve, reject) => {
      // This returns the identifier `TEST123` to the runner
      resolve("TEST123");
    })
  //highlight-end
});
```

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
import { run } from "@tripetto/runner-chat";
import { Export } from "@tripetto/runner";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onSubmit: (instance) => new Promise((resolve, reject) => {
      // This returns the identifier `TEST123` to the runner
      resolve("TEST123");
    })
  //highlight-end
});
```

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
import { run } from "@tripetto/runner-classic";
import { Export } from "@tripetto/runner";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onSubmit: (instance) => new Promise((resolve, reject) => {
      // This returns the identifier `TEST123` to the runner
      resolve("TEST123");
    })
  //highlight-end
});
```

</TabItem>
</Tabs>

## ℹ️ Exportable vs. actionable data {#exportable-vs-actionable}
Tripetto supports two types of data collections that are retrieved from a form:

- **Exportable data**: These are all the fields in the form that are marked as exportable by the form creator;
- **Actionable data**: These fields are generated by action blocks that can perform certain actions like sending an email message.

The exportable data is the most important data collection as it includes all the fields that usually need to be stored. There is a special function called [`exportables`](../../api/library/modules/Export.mdx#xportables) to make it easy to retrieve those fields. So, a common setup would be to retrieve this data collection and submit it to an endpoint as this is the response data that should be saved in most cases.

The actionable data, on the other hand, is volatile data required for performing actions when the form completes. It often needs to be transferred to an API or endpoint to allow further processing. One of the most common actions is post-processing the [Mailer block](../../../blocks/stock/mailer.mdx). This block allows form creators to compose email messages in the form. It is a powerful feature, but it needs additional work to get it up and running properly.

Please refer to the [Post-processing actions guide](../../../blocks/custom/guides/post-processing.mdx) for more information and instructions to set it up.

## 👩‍💻 Simple AJAX example {#example}
The actual saving of form response data to a data store is entirely up to you. You could, for example, use an AJAX call to post the data to a server.

<Tabs>
<TabItem value="fetch" label="fetch()">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";
import { Export } from "@tripetto/runner";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onSubmit: (instance) =>
    new Promise((resolve, reject) => {
      fetch("/example-server", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(Export.exportables(instance)),
      })
        .then((response) => {
          if (response.ok) {
            // All good, resolve the promise
            resolve();
          } else {
            // Not so good, reject the promise
            reject();
          }
        })
        .catch((error) => {
          // Error occurred, reject with error message
          reject(error.message);
        });
    }),
  //highlight-end
});
```

</TabItem>
<TabItem value="XMLHttpRequest" label="XMLHttpRequest">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";
import { Export } from "@tripetto/runner";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onSubmit: (instance) =>
    new Promise((resolve, reject) => {
      const httpRequest = new XMLHttpRequest();

      httpRequest.addEventListener("load", () => resolve());
      httpRequest.addEventListener("error", () => reject());
      httpRequest.addEventListener("abort", () => reject());

      httpRequest.open("POST", "/example-server");
      httpRequest.send(JSON.stringify(Export.exportables(instance)));
    }),
  //highlight-end
});
```

</TabItem>
<TabItem value="SuperAgent" label="SuperAgent">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";
import { Export } from "@tripetto/runner";
import { post } from "superagent";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onSubmit: (instance) =>
    new Promise((resolve, reject) => {
      post("/example-server")
        .send(Export.exportables(instance))
        .then((response) => {
          if (response.ok) {
            // All good, resolve the promise
            resolve();
          } else {
            // Not so good, reject the promise
            reject();
          }
        })
        .catch((error) => {
          // Error occurred, reject with error message
          reject(error.message);
        });
    }),
  //highlight-end
});
```

</TabItem>
<TabItem value="Axios" label="Axios">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";
import { Export } from "@tripetto/runner";
import { post } from "axios";

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onSubmit: (instance) =>
    new Promise((resolve, reject) => {
      post("/example-server", Export.exportables(instance))
        .then((response) => {
          if (response.status == 200) {
            // All good, resolve the promise
            resolve();
          } else {
            // Not so good, reject the promise
            reject();
          }
        })
        .catch((error) => {
          // Error occurred, reject with error message
          reject(error.message);
        });
    }),
  //highlight-end
});
```

</TabItem>
</Tabs>

## 📖 Reference {#reference}

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

Have a look at the complete [autoscroll runner API reference](../../api/stock/autoscroll/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`run`](../../api/stock/autoscroll/functions/run.mdx)
- [`onSubmit`](../../api/stock/autoscroll/interfaces/IAutoscroll.mdx#onSubmit)
- [`onReload`](../../api/stock/autoscroll/interfaces/IAutoscroll.mdx#onReload)
- [`Export`](../../api/library/modules/Export.mdx)

</TabItem>
<TabItem value="chat" label="Chat">

Have a look at the complete [chat runner API reference](../../api/stock/chat/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`run`](../../api/stock/chat/functions/run.mdx)
- [`onSubmit`](../../api/stock/chat/interfaces/IChat.mdx#onSubmit)
- [`onReload`](../../api/stock/chat/interfaces/IChat.mdx#onReload)
- [`Export`](../../api/library/modules/Export.mdx)

</TabItem>
<TabItem value="classic" label="Classic">

Have a look at the complete [classic runner API reference](../../api/stock/classic/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`run`](../../api/stock/classic/functions/run.mdx)
- [`onSubmit`](../../api/stock/classic/interfaces/IClassic.mdx#onSubmit)
- [`onReload`](../../api/stock/classic/interfaces/IClassic.mdx#onReload)
- [`Export`](../../api/library/modules/Export.mdx)

</TabItem>
</Tabs>
