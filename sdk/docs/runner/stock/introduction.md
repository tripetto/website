---
title: Stock runners
sidebar_label: Introduction
sidebar_position: 1
toc_min_heading_level: 3
description: Stock runners are full-featured runners built and maintained by the Tripetto team.
---
# Stock runners
Stock runners are full-featured runners built and maintained by the Tripetto team. Stock runners are the easiest and most convenient way to run a Tripetto form. They provide a seamless experience out-of-the-box and are very easy to use and implement. There are currently three different form UIs (we call them [form faces](faces/index.md)) to choose from.

- [Autoscroll runner](faces/autoscroll.mdx): Fluently presents one question at a time;
- [Chat runner](faces/chat.mdx): Presents all questions and answers as a chat;
- [Classic runner](faces/classic.mdx): Presents question fields in a traditional form format.

:::tip
Each runner presents the form using a different UI to the user. Those different form UIs are called [form faces](faces/index.md).
:::

## 🚀 Quickstart {#quickstart}
[![Implement stock runner using plain JS](/img/button-js.svg)](../quickstart/plain-js/)
[![Implement stock runner using React](/img/button-react.svg)](../quickstart/react/)
[![Implement stock runner using Angular](/img/button-angular.svg)](../quickstart/angular/)
[![Implement stock runner using HTML](/img/button-html.svg)](../quickstart/html/)

▶️ [Implement stock runner using **plain JS**](quickstart/plain-js.mdx)

▶️ [Implement stock runner using **React**](quickstart/react.mdx)

▶️ [Implement stock runner using **Angular**](quickstart/angular.mdx)

▶️ [Implement stock runner using **HTML**](quickstart/html.mdx)

## 🎞️ Video {#video}
<div className="youtube-embed"><iframe width="560" height="315" src="https://www.youtube.com/embed/fixp89Yievg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
