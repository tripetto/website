---
title: React - Quickstart - Stock runners
sidebar_label: React
sidebar_position: 2
description: Implement a stock runner using React.
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';
import UpNextTopics from './up-next-topics.include.mdx'

# ![](/img/logo-react.svg) Implement runner using React
The stock runners comes with a built-in React component. This makes it very easy to use the runners in a React application. This quickstart shows how to use it.

## ✅ Add packages to your project {#packages}
First, you need to add the required packages to your project. To do so, run the following command:

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```bash
npm install @tripetto/runner-autoscroll @tripetto/runner
```

</TabItem>
<TabItem value="chat" label="Chat">

```bash
npm install @tripetto/runner-chat @tripetto/runner
```

</TabItem>
<TabItem value="classic" label="Classic">

```bash
npm install @tripetto/runner-classic @tripetto/runner
```

</TabItem>
</Tabs>

:::info
The stock runners all depend on the [Runner library](../../api/library/index.md) and [React](https://react.dev). The Tripetto Runner library is the actual workhorse of the Tripetto runners. It parses the form definition and prepares it for UI rendering. React is used for the actual rendering.
:::

## 📄 Basic implementation {#component}
The next step is to import the [`AutoscrollRunner`](../../api/stock/autoscroll/components/react.mdx) React component. Here's an example:

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```ts showLineNumbers
//highlight-start
import { AutoscrollRunner } from "@tripetto/runner-autoscroll";
//highlight-end

function ExampleApp() {
  return (
    <div>
      <h1>Example app</h1>
      //highlight-start
      <AutoscrollRunner definition={/* Supply your form definition here */} />
      //highlight-end
    </div>
  );
}
```
[![Run](/img/button-run.svg)](https://ovhd8.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-autoscroll-react-basic-implementation-ovhd8?file=/src/index.js)

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
//highlight-start
import { ChatRunner } from "@tripetto/runner-chat";
//highlight-end

function ExampleApp() {
  return (
    <div>
      <h1>Example app</h1>
      //highlight-start
      <ChatRunner definition={/* Supply your form definition here */} />
      //highlight-end
    </div>
  );
}
```
[![Run](/img/button-run.svg)](https://6o6y7.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-chat-react-basic-implementation-6o6y7?file=/src/index.js)

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
//highlight-start
import { ClassicRunner } from "@tripetto/runner-classic";
//highlight-end

function ExampleApp() {
  return (
    <div>
      <h1>Example app</h1>
      //highlight-start
      <ClassicRunner definition={/* Supply your form definition here */} />
      //highlight-end
    </div>
  );
}
```
[![Run](/img/button-run.svg)](https://5u6m2.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-classic-react-basic-implementation-5u6m2?file=/src/index.js)

</TabItem>
</Tabs>

## 📥 Collecting response data {#collecting-data}
The next step is actual data retrieval from the form. To do so, you use the [`onSubmit`](#reference) event. This event fires when the form completes and the response data is ready for further processing. The event receives a reference to the active form instance. Together with one of the [`Export`](../../api/library/modules/Export.mdx) functions from the Runner library, you use it to retrieve data in a convenient format. The following example shows how to export the data using the [`exportables`](../../api/library/modules/Export.mdx#exportables) or [`CSV`](../../api/library/modules/Export.mdx#CSV) function.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```js showLineNumbers
import { AutoscrollRunner } from "@tripetto/runner-autoscroll";
//highlight-start
import { Export } from "@tripetto/runner";
//highlight-end

function ExampleApp() {
  return (
    <div>
      <h1>Example app</h1>
      //highlight-start
      <AutoscrollRunner
        definition={/* Supply your form definition here */}
        //highlight-start
        onSubmit={(instance) => {
          // This exports all exportable data in the form
          const exportables = Export.exportables(instance);

          // Iterate through all the fields
          exportables.fields.forEach((field) => {
            // Output each field name and value to the console
            console.log(`${field.name}: ${field.string}`);
          });

          // This exports the collected data as a CSV object
          const csv = Export.CSV(instance);

          // Output CSV to the console
          console.log(csv.fields);
          console.log(csv.record);
        }}
        //highlight-end
      />
    </div>
  );
}
```
[![Run](/img/button-run.svg)](https://mvj4n.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-autoscroll-react-handling-response-data-mvj4n?file=/src/index.js:1578-2153)

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
import { ChatRunner } from "@tripetto/runner-chat";
//highlight-start
import { Export } from "@tripetto/runner";
//highlight-end

function ExampleApp() {
  return (
    <div>
      <h1>Example app</h1>
      //highlight-start
      <ChatRunner
        definition={/* Supply your form definition here */}
        //highlight-start
        onSubmit={(instance) => {
          // This exports all exportable data in the form
          const exportables = Export.exportables(instance);

          // Iterate through all the fields
          exportables.fields.forEach((field) => {
            // Output each field name and value to the console
            console.log(`${field.name}: ${field.string}`);
          });

          // This exports the collected data as a CSV object
          const csv = Export.CSV(instance);

          // Output CSV to the console
          console.log(csv.fields);
          console.log(csv.record);
        }}
        //highlight-end
      />
    </div>
  );
}
```
[![Run](/img/button-run.svg)](https://1xq9h.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-chat-react-handling-response-data-1xq9h?file=/src/index.js:1560-2135)

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
import { ClassicRunner } from "@tripetto/runner-autclassicoscroll";
//highlight-start
import { Export } from "@tripetto/runner";
//highlight-end

function ExampleApp() {
  return (
    <div>
      <h1>Example app</h1>
      //highlight-start
      <ClassicRunner
        definition={/* Supply your form definition here */}
        //highlight-start
        onSubmit={(instance) => {
          // This exports all exportable data in the form
          const exportables = Export.exportables(instance);

          // Iterate through all the fields
          exportables.fields.forEach((field) => {
            // Output each field name and value to the console
            console.log(`${field.name}: ${field.string}`);
          });

          // This exports the collected data as a CSV object
          const csv = Export.CSV(instance);

          // Output CSV to the console
          console.log(csv.fields);
          console.log(csv.record);
        }}
        //highlight-end
      />
    </div>
  );
}
```
[![Run](/img/button-run.svg)](https://u2zyr.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-classic-react-handling-response-data-u2zyr?file=/src/index.js:1569-2144)

</TabItem>
</Tabs>

:::tip
The `onSubmit` event supports some additional features for error handling. Have a look at the [Collecting response data guide](../guides/collecting.mdx) for more information and guidance.
:::

## 🎞️ Video {#video}
<div className="youtube-embed"><iframe width="560" height="315" src="https://www.youtube.com/embed/fixp89Yievg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>

## 📖 Reference {#reference}

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

Have a look at the complete [autoscroll runner API reference](../../api/stock/autoscroll/index.md) for detailed documentation. In the examples above, the following React components, symbols were used:
- [`AutoscrollRunner`](../../api/stock/autoscroll/components/react.mdx)
- [`onSubmit`](../../api/stock/autoscroll/interfaces/IAutoscroll.mdx#onSubmit)

</TabItem>
<TabItem value="chat" label="Chat">

Have a look at the complete [chat runner API reference](../../api/stock/chat/index.md) for detailed documentation. In the examples above, the following React components and symbols were used:
- [`ChatRunner`](../../api/stock/chat/components/react.mdx)
- [`onSubmit`](../../api/stock/chat/interfaces/IChat.mdx#onSubmit)

</TabItem>
<TabItem value="classic" label="Classic">

Have a look at the complete [classic runner API reference](../../api/stock/classic/index.md) for detailed documentation. In the examples above, the following React components and symbols were used:
- [`ClassicRunner`](../../api/stock/classic/components/react.mdx)
- [`onSubmit`](../../api/stock/classic/interfaces/IClassic.mdx#onSubmit)

</TabItem>
</Tabs>

## ⏭️ Up next {#up-next}
<UpNextTopics />
