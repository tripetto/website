---
title: Plain JS - Quickstart - Stock runners
sidebar_label: Plain JS
sidebar_position: 1
description: Implement a stock runner using plain JS.
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';
import UpNextTopics from './up-next-topics.include.mdx'

# ![](/img/logo-js.svg) Implement runner using plain JS
You can implement a [stock runner](../introduction.md) using plain JS.

## ✅ Add packages to your project {#packages}
First, you need to add the required packages to your project. To do so, run the following command:

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```bash
npm install @tripetto/runner-autoscroll @tripetto/runner
```

</TabItem>
<TabItem value="chat" label="Chat">

```bash
npm install @tripetto/runner-chat @tripetto/runner
```

</TabItem>
<TabItem value="classic" label="Classic">

```bash
npm install @tripetto/runner-classic @tripetto/runner
```

</TabItem>
</Tabs>

:::info
The stock runners all depend on the [Runner library](../../api/library/index.md) and [React](https://react.dev). The Tripetto Runner library is the actual workhorse of the Tripetto runners. It parses the form definition and prepares it for UI rendering. React is used for the actual rendering.
:::

## 📄 Basic implementation {#implementation}
This code example shows the minimal code required to display the runner. It uses the [`run`](#reference) function to bootstrap the runner using a single call. By default, the runner will use the body element of the browser consuming the full-page viewport (have a look [here](#custom-element) if you want to display the runner inline with other content).

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";

// Open the runner
run({
  definition: /* Supply your form definition here */,
  display: "page" // Let the runner know it runs full-page
});
```
[![Run](/img/button-run.svg)](https://eyq4x.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-autoscroll-basic-implementation-eyq4x?file=/src/index.ts)

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
import { run } from "@tripetto/runner-chat";

// Open the runner
run({
  definition: /* Supply your form definition here */,
  display: "page" // Let the runner know it runs full-page
});
```
[![Run](/img/button-run.svg)](https://t6end.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-chat-basic-implementation-t6end?file=/src/index.ts)

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
import { run } from "@tripetto/runner-classic";

// Open the runner
run({
  definition: /* Supply your form definition here */,
  display: "page" // Let the runner know it runs full-page
});
```
[![Run](/img/button-run.svg)](https://nlxbq.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-classic-basic-implementation-nlxbq?file=/src/index.ts)

</TabItem>
</Tabs>

## 👩‍💻 Use a custom HTML element {#custom-element}
If you want to display the runner inline with other content, you can specify a custom HTML element as a host for the runner.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";

// Open the runner
run({
  //highlight-start
  element: document.getElementById("CustomElement"),
  //highlight-end
  definition: /* Supply your form definition here */
});
```
[![Run](/img/button-run.svg)](https://6vcop.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-autoscroll-custom-element-6vcop?file=/src/index.ts)

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
import { run } from "@tripetto/runner-chat";

// Open the runner
run({
  //highlight-start
  element: document.getElementById("CustomElement"),
  //highlight-end
  definition: /* Supply your form definition here */
});
```
[![Run](/img/button-run.svg)](https://tqsyo.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-chat-custom-element-tqsyo?file=/src/index.ts)

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
import { run } from "@tripetto/runner-classic";

// Open the runner
run({
  //highlight-start
  element: document.getElementById("CustomElement"),
  //highlight-end
  definition: /* Supply your form definition here */
});
```
[![Run](/img/button-run.svg)](https://vnxdo.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-classic-custom-element-vnxdo?file=/index.html)

</TabItem>
</Tabs>

:::tip
There is no need to specify the `display` property as we did in the previous example since inline rendering is the default behavior.
:::

## 📥 Collecting response data {#collecting-data}
The next step is actual data retrieval from the form. To do so, you use the [`onSubmit`](#reference) event. This event fires when the form completes and the response data is ready for further processing. The event receives a reference to the active form instance. Together with one of the [`Export`](../../api/library/modules/Export.mdx) functions from the Runner library, you use it to retrieve data in a convenient format. The following example shows how to export the data using the [`exportables`](../../api/library/modules/Export.mdx#exportables) or [`CSV`](../../api/library/modules/Export.mdx#CSV) function.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```js showLineNumbers
import { run } from "@tripetto/runner-autoscroll";
//highlight-start
import { Export } from "@tripetto/runner";
//highlight-end

// Open the runner
run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onSubmit: (instance) => {
    // This exports all exportable data in the form
    const exportables = Export.exportables(instance);

    // Iterate through all the fields
    exportables.fields.forEach((field) => {
      // Output each field name and value to the console
      console.log(`${field.name}: ${field.string}`);
    });

    // This exports the collected data as a CSV object
    const csv = Export.CSV(instance);

    // Output CSV to the console
    console.log(csv.fields);
    console.log(csv.record);
  }
  //highlight-end
});
```
[![Run](/img/button-run.svg)](https://60cti.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-autoscroll-handling-response-data-60cti?file=/src/index.ts:1259-1777)

</TabItem>
<TabItem value="chat" label="Chat">

```js showLineNumbers
import { run } from "@tripetto/runner-chat";
//highlight-start
import { Export } from "@tripetto/runner";
//highlight-end

// Open the runner
run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onSubmit: (instance) => {
    // This exports all exportable data in the form
    const exportables = Export.exportables(instance);

    // Iterate through all the fields
    exportables.fields.forEach((field) => {
      // Output each field name and value to the console
      console.log(`${field.name}: ${field.string}`);
    });

    // This exports the collected data as a CSV object
    const csv = Export.CSV(instance);

    // Output CSV to the console
    console.log(csv.fields);
    console.log(csv.record);
  }
  //highlight-end
});
```
[![Run](/img/button-run.svg)](https://ju5rv.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-chat-handling-response-data-ju5rv?file=/src/index.ts:1253-1771)

</TabItem>
<TabItem value="classic" label="Classic">

```js showLineNumbers
import { run } from "@tripetto/runner-classic";
//highlight-start
import { Export } from "@tripetto/runner";
//highlight-end

// Open the runner
run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onSubmit: (instance) => {
    // This exports all exportable data in the form
    const exportables = Export.exportables(instance);

    // Iterate through all the fields
    exportables.fields.forEach((field) => {
      // Output each field name and value to the console
      console.log(`${field.name}: ${field.string}`);
    });

    // This exports the collected data as a CSV object
    const csv = Export.CSV(instance);

    // Output CSV to the console
    console.log(csv.fields);
    console.log(csv.record);
  }
  //highlight-end
});
```
[![Run](/img/button-run.svg)](https://k6n5z.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-classic-handling-response-data-k6n5z?file=/src/index.ts:1256-1774)

</TabItem>
</Tabs>

:::tip
The `onSubmit` event supports some additional features for error handling. Have a look at the [Collecting response data guide](../guides/collecting.mdx) for more information and guidance.
:::

## 📖 Reference {#reference}

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

Have a look at the complete [autoscroll runner API reference](../../api/stock/autoscroll/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`run`](../../api/stock/autoscroll/functions/run.mdx)
- [`onSubmit`](../../api/stock/autoscroll/interfaces/IAutoscroll.mdx#onSubmit)

</TabItem>
<TabItem value="chat" label="Chat">

Have a look at the complete [chat runner API reference](../../api/stock/chat/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`run`](../../api/stock/chat/functions/run.mdx)
- [`onSubmit`](../../api/stock/chat/interfaces/IChat.mdx#onSubmit)

</TabItem>
<TabItem value="classic" label="Classic">

Have a look at the complete [classic runner API reference](../../api/stock/classic/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`run`](../../api/stock/classic/functions/run.mdx)
- [`onSubmit`](../../api/stock/classic/interfaces/IClassic.mdx#onSubmit)

</TabItem>
</Tabs>

## ⏭️ Up next {#up-next}
<UpNextTopics />
