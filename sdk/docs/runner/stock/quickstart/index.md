---
title: Quickstarts - Stock runners
sidebar_label: Quickstart
description: This chapter shows you different approaches to implement a stock runner.
toc_min_heading_level: 3
---

# Stock runner quickstart
The runner needs a browser environment with [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript/A_re-introduction_to_JavaScript) support to work properly. It can be implemented using different libraries and frameworks. Please select how you want to implement the runner:

[![Implement stock runner using plain JS](/img/button-js.svg)](plain-js/)
[![Implement stock runner using React](/img/button-react.svg)](react/)
[![Implement stock runner using Angular](/img/button-angular.svg)](angular/)
[![Implement stock runner using HTML](/img/button-html.svg)](html/)

▶️ [Implement stock runner using **plain JS**](plain-js.mdx)

▶️ [Implement stock runner using **React**](react.mdx)

▶️ [Implement stock runner using **Angular**](angular.mdx)

▶️ [Implement stock runner using **HTML**](html.mdx)

## 🎞️ Video {#video}
<div className="youtube-embed"><iframe width="560" height="315" src="https://www.youtube.com/embed/fixp89Yievg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
