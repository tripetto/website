---
title: Angular - Quickstart - Stock runners
sidebar_label: Angular
sidebar_position: 3
description: Implement a stock runner using Angular.
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';
import UpNextTopics from './up-next-topics.include.mdx'

# ![](/img/logo-angular.svg) Implement runner using Angular
The Tripetto stock runners come with a built-in [Angular](https://angular.io/) component. This makes it very easy to use a runner in an [Angular](https://angular.io/) application. This quickstart shows how to use it.

## ✅ Add packages to your project {#packages}
First, you need to add the required packages to your project. To do so, run the following command:

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

```bash
npm install @tripetto/runner-autoscroll @tripetto/runner
```

</TabItem>
<TabItem value="chat" label="Chat">

```bash
npm install @tripetto/runner-chat @tripetto/runner
```

</TabItem>
<TabItem value="classic" label="Classic">

```bash
npm install @tripetto/runner-classic @tripetto/runner
```

</TabItem>
</Tabs>

:::info
The stock runners all depend on the [Runner library](../../api/library/index.md). The Tripetto Runner library is the actual workhorse of the Tripetto runners. It parses the form definition and prepares it for UI rendering.
:::

## 📄 Basic implementation {#implementation}
To use the runner, simply import the module from the package and feed it to your application's `@NgModule` imports array. This makes the runner selector available in your application. The Angular component is located in a subfolder named `angular` inside the runner package. Here's an example:

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

<Tabs>
<TabItem value="app-module" label="App module">

```ts showLineNumbers title="app.module.ts"
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
//highlight-start
import { TripettoAutoscrollModule } from "@tripetto/runner-autoscroll/angular";
//highlight-end

@NgModule({
  declarations: [AppComponent],
  //highlight-start
  imports: [BrowserModule, TripettoAutoscrollModule],
  //highlight-end
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
```

</TabItem>
<TabItem value="app-html" label="App HTML">

```html showLineNumbers title="app.component.html"
<tripetto-runner-autoscroll [definition]="definition"></tripetto-runner-autoscroll>
```

</TabItem>
<TabItem value="app-component" label="App component">

```ts showLineNumbers title="app.component.ts"
import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  //highlight-start
  definition = /* Supply your form definition here */;
  //highlight-end
}
```

</TabItem>
</Tabs>

[![Run](/img/button-run.svg)](https://s3o6j.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-autoscroll-angular-basic-implementation-s3o6j?file=/src/app/app.component.html)

</TabItem>
<TabItem value="chat" label="Chat">

<Tabs>
<TabItem value="app-module" label="App module">

```ts showLineNumbers title="app.module.ts"
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
//highlight-start
import { TripettoChatModule } from "@tripetto/runner-chat/angular";
//highlight-end

@NgModule({
  declarations: [AppComponent],
  //highlight-start
  imports: [BrowserModule, TripettoChatModule],
  //highlight-end
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
```

</TabItem>
<TabItem value="app-html" label="App HTML">

```html showLineNumbers title="app.component.html"
<tripetto-runner-chat [definition]="definition"></tripetto-runner-chat>
```

</TabItem>
<TabItem value="app-component" label="App component">

```ts showLineNumbers title="app.component.ts"
import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  //highlight-start
  definition = /* Supply your form definition here */;
  //highlight-end
}
```

</TabItem>
</Tabs>

[![Run](/img/button-run.svg)](https://wtvhr.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-chat-angular-basic-implementation-wtvhr?file=/src/app/app.component.html)

</TabItem>
<TabItem value="classic" label="Classic">

<Tabs>
<TabItem value="app-module" label="App module">

```ts showLineNumbers title="app.module.ts"
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
//highlight-start
import { TripettoClassicModule } from "@tripetto/runner-classic/angular";
//highlight-end

@NgModule({
  declarations: [AppComponent],
  //highlight-start
  imports: [BrowserModule, TripettoClassicModule],
  //highlight-end
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
```

</TabItem>
<TabItem value="app-html" label="App HTML">

```html showLineNumbers title="app.component.html"
<tripetto-runner-classic [definition]="definition"></tripetto-runner-classic>
```

</TabItem>
<TabItem value="app-component" label="App component">

```ts showLineNumbers title="app.component.ts"
import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  //highlight-start
  definition = /* Supply your form definition here */;
  //highlight-end
}
```

</TabItem>
</Tabs>

[![Run](/img/button-run.svg)](https://de1fh.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-classic-angular-basic-implementation-de1fh?file=/src/app/app.component.html)

</TabItem>
</Tabs>

## 📥 Collecting response data {#collecting-data}
The next step is actual data retrieval from the form. To do so, you use the [`onSubmit`](#reference) event. This event fires when the form completes and the response data is ready for further processing. The event receives a reference to the active form instance. Together with one of the [`Export`](../../api/library/modules/Export.mdx) functions from the Runner library, you use it to retrieve data in a convenient format. The following example shows how to export the data using the [`exportables`](../../api/library/modules/Export.mdx#exportables) or [`CSV`](../../api/library/modules/Export.mdx#CSV) function.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

<Tabs>
<TabItem value="app-module" label="App module">

```ts showLineNumbers title="app.module.ts"
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
//highlight-start
import { TripettoAutoscrollModule } from "@tripetto/runner-autoscroll/angular";
//highlight-end

@NgModule({
  declarations: [AppComponent],
  //highlight-start
  imports: [BrowserModule, TripettoAutoscrollModule],
  //highlight-end
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
```

</TabItem>
<TabItem value="app-html" label="App HTML">

```html showLineNumbers title="app.component.html"
<tripetto-runner-autoscroll (onSubmit)="onSubmit($event)"></tripetto-runner-autoscroll>
```

</TabItem>
<TabItem value="app-component" label="App component">

```ts showLineNumbers title="app.component.ts"
import { Component, Output } from "@angular/core";
import { Instance, Export } from "@tripetto/runner";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  //highlight-start
  @Output() onSubmit(event: { instance: Instance }) {
    // This exports all exportable data in the form
    const exportables = Export.exportables(event.instance);

    // Iterate through all the fields
    exportables.fields.forEach((field) => {
      // Output each field name and value to the console
      console.log(`${field.name}: ${field.string}`);
    });

    // This exports the collected data as a CSV object
    const csv = Export.CSV(event.instance);

    // Output CSV to the console
    console.log(csv.fields);
    console.log(csv.record);
  }
  //highlight-end
}
```

</TabItem>
</Tabs>

[![Run](/img/button-run.svg)](https://4p70v.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-autoscroll-angular-collecting-response-data-4p70v?file=/src/app/app.component.html)

</TabItem>
<TabItem value="chat" label="Chat">

<Tabs>
<TabItem value="app-module" label="App module">

```ts showLineNumbers title="app.module.ts"
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
//highlight-start
import { TripettoChatModule } from "@tripetto/runner-chat/angular";
//highlight-end

@NgModule({
  declarations: [AppComponent],
  //highlight-start
  imports: [BrowserModule, TripettoChatModule],
  //highlight-end
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
```

</TabItem>
<TabItem value="app-html" label="App HTML">

```html showLineNumbers title="app.component.html"
<tripetto-runner-chat (onSubmit)="onSubmit($event)"></tripetto-runner-chat>
```

</TabItem>
<TabItem value="app-component" label="App component">

```ts showLineNumbers title="app.component.ts"
import { Component, Output } from "@angular/core";
import { Instance, Export } from "@tripetto/runner";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  //highlight-start
  @Output() onSubmit(event: { instance: Instance }) {
    // This exports all exportable data in the form
    const exportables = Export.exportables(event.instance);

    // Iterate through all the fields
    exportables.fields.forEach((field) => {
      // Output each field name and value to the console
      console.log(`${field.name}: ${field.string}`);
    });

    // This exports the collected data as a CSV object
    const csv = Export.CSV(event.instance);

    // Output CSV to the console
    console.log(csv.fields);
    console.log(csv.record);
  }
  //highlight-end
}
```

</TabItem>
</Tabs>

[![Run](/img/button-run.svg)](https://elxd2.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-chat-angular-collecting-response-data-elxd2?file=/src/app/app.component.html)

</TabItem>
<TabItem value="classic" label="Classic">


<Tabs>
<TabItem value="app-module" label="App module">

```ts showLineNumbers title="app.module.ts"
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
//highlight-start
import { TripettoClassicModule } from "@tripetto/runner-classic/angular";
//highlight-end

@NgModule({
  declarations: [AppComponent],
  //highlight-start
  imports: [BrowserModule, TripettoClassicModule],
  //highlight-end
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
```

</TabItem>
<TabItem value="app-html" label="App HTML">

```html showLineNumbers title="app.component.html"
<tripetto-runner-classic (onSubmit)="onSubmit($event)"></tripetto-runner-classic>
```

</TabItem>
<TabItem value="app-component" label="App component">

```ts showLineNumbers title="app.component.ts"
import { Component, Output } from "@angular/core";
import { Instance, Export } from "@tripetto/runner";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  //highlight-start
  @Output() onSubmit(event: { instance: Instance }) {
    // This exports all exportable data in the form
    const exportables = Export.exportables(event.instance);

    // Iterate through all the fields
    exportables.fields.forEach((field) => {
      // Output each field name and value to the console
      console.log(`${field.name}: ${field.string}`);
    });

    // This exports the collected data as a CSV object
    const csv = Export.CSV(event.instance);

    // Output CSV to the console
    console.log(csv.fields);
    console.log(csv.record);
  }
  //highlight-end
}
```

</TabItem>
</Tabs>

[![Run](/img/button-run.svg)](https://i7eyf.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-classic-angular-collecting-response-data-i7eyf?file=/src/app/app.component.html)

</TabItem>
</Tabs>

:::tip
The `onSubmit` event supports some additional features for error handling. Have a look at the [Collecting response data guide](../guides/collecting.mdx) for more information and guidance.
:::

## 📖 Reference {#reference}

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

Have a look at the complete [autoscroll runner API reference](../../api/stock/autoscroll/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`TripettoAutoscrollModule`](../../api/stock/autoscroll/components/angular.mdx)
- [`onSubmit`](../../api/stock/autoscroll/interfaces/IAutoscroll.mdx#onSubmit)

</TabItem>
<TabItem value="chat" label="Chat">

Have a look at the complete [chat runner API reference](../../api/stock/chat/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`TripettoChatModule`](../../api/stock/chat/components/angular.mdx)
- [`onSubmit`](../../api/stock/chat/interfaces/IChat.mdx#onSubmit)

</TabItem>
<TabItem value="classic" label="Classic">

Have a look at the complete [classic runner API reference](../../api/stock/classic/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`TripettoClassicModule`](../../api/stock/classic/components/angular.mdx)
- [`onSubmit`](../../api/stock/classic/interfaces/IClassic.mdx#onSubmit)

</TabItem>
</Tabs>

## ⏭️ Up next {#up-next}
<UpNextTopics />
