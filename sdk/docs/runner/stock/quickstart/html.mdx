---
title: HTML - Quickstart - Stock runners
sidebar_label: HTML
sidebar_position: 4
description: Implement a stock runner using HTML.
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';
import UpNextTopics from './up-next-topics.include.mdx'

# ![](/img/logo-html.svg) Implement stock runner using HTML
You can use good old HTML to implement a [stock runner](../introduction.md). To do so, you need to load the Runner library from a CDN or [host the library yourself](#self-host). If you want to use a CDN, you could go with [unpkg](https://unpkg.com), or [jsDelivr](https://cdn.jsdelivr.net/).

:::info
The stock runners all depend on the [Runner library](../../api/library/index.md). This library is the actual workhorse of the Tripetto runners. It parses the form definition and prepares it for UI rendering.
:::

## 📄 Basic implementation {#implementation}
This code example shows the minimal code required to run a form using simple HTML. The example assumes you have created a form and saved the [form definition](../../../builder/api/interfaces/IDefinition.mdx), so you can use it in the runner.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

<Tabs groupId="cdn">
<TabItem value="jsdelivr" label="jsDelivr">

```html showLineNumbers
<html>
  <body>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/runner"></script>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/runner-autoscroll"></script>
    <script>
    TripettoAutoscroll.run({
      definition: /* Supply a form definition here. */,
      display: "page" // Let the runner know it runs full-page
    });
    </script>
  </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/1c5d1b2cb4b8ea10219f00b31b4c30cc) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/1c5d1b2cb4b8ea10219f00b31b4c30cc)

</TabItem>
<TabItem value="unpkg" label="unpkg">

```html showLineNumbers
<html>
  <body>
    <script src="https://unpkg.com/@tripetto/runner"></script>
    <script src="https://unpkg.com/@tripetto/runner-autoscroll"></script>
    <script>
    TripettoAutoscroll.run({
      definition: /* Supply a form definition here. */,
      display: "page" // Let the runner know it runs full-page
    });
    </script>
  </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/4ea97880f9777c59ef1c638a79fcff34) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/4ea97880f9777c59ef1c638a79fcff34)

</TabItem>
</Tabs>

</TabItem>
<TabItem value="chat" label="Chat">

<Tabs groupId="cdn">
<TabItem value="jsdelivr" label="jsDelivr">

```html showLineNumbers
<html>
  <body>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/runner"></script>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/runner-chat"></script>
    <script>
    TripettoChat.run({
      definition: /* Supply a form definition here. */,
      display: "page" // Let the runner know it runs full-page
    });
    </script>
  </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/d471ec4f3e208c21c4e2971bc964ef10) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/d471ec4f3e208c21c4e2971bc964ef10)

</TabItem>
<TabItem value="unpkg" label="unpkg">

```html showLineNumbers
<html>
  <body>
    <script src="https://unpkg.com/@tripetto/runner"></script>
    <script src="https://unpkg.com/@tripetto/runner-chat"></script>
    <script>
    TripettoChat.run({
      definition: /* Supply a form definition here. */,
      display: "page" // Let the runner know it runs full-page
    });
    </script>
  </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/8ccf2465b70c431ea81a05f6fc6cb737) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/8ccf2465b70c431ea81a05f6fc6cb737)

</TabItem>
</Tabs>

</TabItem>
<TabItem value="classic" label="Classic">

<Tabs groupId="cdn">
<TabItem value="jsdelivr" label="jsDelivr">

```html showLineNumbers
<html>
  <body>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/runner"></script>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/runner-classic"></script>
    <script>
    TripettoClassic.run({
      definition: /* Supply a form definition here. */,
      display: "page" // Let the runner know it runs full-page
    });
    </script>
  </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/50b06cf0c6bb7c10dd39259dd8b6b540) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/50b06cf0c6bb7c10dd39259dd8b6b540)

</TabItem>
<TabItem value="unpkg" label="unpkg">

```html showLineNumbers
<html>
  <body>
    <script src="https://unpkg.com/@tripetto/runner"></script>
    <script src="https://unpkg.com/@tripetto/runner-classic"></script>
    <script>
    TripettoClassic.run({
      definition: /* Supply a form definition here. */,
      display: "page" // Let the runner know it runs full-page
    });
    </script>
  </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/7d1de497c510a80ac71ae60aa77188d8) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/7d1de497c510a80ac71ae60aa77188d8)

</TabItem>
</Tabs>

</TabItem>
</Tabs>

## 👩‍💻 Use a custom HTML element {#custom-element}
If you want to display the runner inline with other content, you can specify a custom HTML element as a host element for the runner.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

<Tabs groupId="cdn">
<TabItem value="jsdelivr" label="jsDelivr">

```html showLineNumbers
<html>
  <body>
    //highlight-start
    <div id="RunnerElement"></div>
    //highlight-end
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/runner"></script>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/runner-autoscroll"></script>
    <script>
    TripettoAutoscroll.run({
      //highlight-start
      element: document.getElementById("RunnerElement"),
      //highlight-end
      definition: /* Supply a form definition here. */
    });
    </script>
  </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/d21fcf8b6c1055f5f0bf5c1bcb6de122) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/d21fcf8b6c1055f5f0bf5c1bcb6de122)

</TabItem>
<TabItem value="unpkg" label="unpkg">

```html showLineNumbers
<html>
  <body>
    //highlight-start
    <div id="RunnerElement"></div>
    //highlight-end
    <script src="https://unpkg.com/@tripetto/runner"></script>
    <script src="https://unpkg.com/@tripetto/runner-autoscroll"></script>
    <script>
    TripettoAutoscroll.run({
      //highlight-start
      element: document.getElementById("RunnerElement"),
      //highlight-end
      definition: /* Supply a form definition here. */
    });
    </script>
  </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/78c7d2fe15751bd0ee5c99d79bc080b2) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/78c7d2fe15751bd0ee5c99d79bc080b2)

</TabItem>
</Tabs>

</TabItem>
<TabItem value="chat" label="Chat">

<Tabs groupId="cdn">
<TabItem value="jsdelivr" label="jsDelivr">

```html showLineNumbers
<html>
  <body>
    //highlight-start
    <div id="RunnerElement"></div>
    //highlight-end
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/runner"></script>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/runner-chat"></script>
    <script>
    TripettoChat.run({
      //highlight-start
      element: document.getElementById("RunnerElement"),
      //highlight-end
      definition: /* Supply a form definition here. */
    });
    </script>
  </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/9d74be2e1061415977409ac0e45ac019) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/9d74be2e1061415977409ac0e45ac019)

</TabItem>
<TabItem value="unpkg" label="unpkg">

```html showLineNumbers
<html>
  <body>
    //highlight-start
    <div id="RunnerElement"></div>
    //highlight-end
    <script src="https://unpkg.com/@tripetto/runner"></script>
    <script src="https://unpkg.com/@tripetto/runner-chat"></script>
    <script>
    TripettoChat.run({
      //highlight-start
      element: document.getElementById("RunnerElement"),
      //highlight-end
      definition: /* Supply a form definition here. */
    });
    </script>
  </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/95bd0802a09ba4f12899421274178ca7) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/95bd0802a09ba4f12899421274178ca7)

</TabItem>
</Tabs>

</TabItem>
<TabItem value="classic" label="Classic">

<Tabs groupId="cdn">
<TabItem value="jsdelivr" label="jsDelivr">

```html showLineNumbers
<html>
  <body>
    //highlight-start
    <div id="RunnerElement"></div>
    //highlight-end
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/runner"></script>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/runner-classic"></script>
    <script>
    TripettoClassic.run({
      //highlight-start
      element: document.getElementById("RunnerElement"),
      //highlight-end
      definition: /* Supply a form definition here. */
    });
    </script>
  </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/80f7a31ea638936a0c703bbe0f93bb12) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/80f7a31ea638936a0c703bbe0f93bb12)

</TabItem>
<TabItem value="unpkg" label="unpkg">

```html showLineNumbers
<html>
  <body>
    //highlight-start
    <div id="RunnerElement"></div>
    //highlight-end
    <script src="https://unpkg.com/@tripetto/runner"></script>
    <script src="https://unpkg.com/@tripetto/runner-classic"></script>
    <script>
    TripettoClassic.run({
      //highlight-start
      element: document.getElementById("RunnerElement"),
      //highlight-end
      definition: /* Supply a form definition here. */
    });
    </script>
  </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/97d4c1f386d6046ab58d6b5d9bc79f77) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/97d4c1f386d6046ab58d6b5d9bc79f77)

</TabItem>
</Tabs>

</TabItem>
</Tabs>

:::tip
There is no need to specify the `display` property as we did in the previous example since inline rendering is the default behavior.
:::

## 📥 Collecting response data {#collecting-data}
The next step is actual data retrieval from the form. To do so, you use the [`onSubmit`](#reference) event. This event fires when the form completes and the response data is ready for further processing. The event receives a reference to the active form instance. Together with one of the [`Export`](../../api/library/modules/Export.mdx) functions from the Runner library, you use it to retrieve data in a convenient format. The following example shows how to export the data using the [`exportables`](../../api/library/modules/Export.mdx#exportables) or [`CSV`](../../api/library/modules/Export.mdx#CSV) function.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

<Tabs groupId="cdn">
<TabItem value="jsdelivr" label="jsDelivr">

```html showLineNumbers
<html>
  <body>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/runner"></script>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/runner-autoscroll"></script>
    <script>
    TripettoAutoscroll.run({
      definition: /* Supply a form definition here. */,
      //highlight-start
      onSubmit: function(instance) {
        // This exports all exportable data in the form
        const exportables = TripettoRunner.Export.exportables(instance);

        // Iterate through all the fields
        exportables.fields.forEach((field) => {
          // Output each field name and value to the console
          console.log(`${field.name}: ${field.string}`);
        });

        // This exports the collected data as a CSV object
        const csv = TripettoRunner.Export.CSV(instance);

        // Output CSV to the console
        console.log(csv.fields);
        console.log(csv.record);
      }
      //highlight-end
    });
    </script>
  </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/c7f98c3e434562462e700342fd3401c9) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/c7f98c3e434562462e700342fd3401c9)

</TabItem>
<TabItem value="unpkg" label="unpkg">

```html showLineNumbers
<html>
  <body>
    <script src="https://unpkg.com/@tripetto/runner"></script>
    <script src="https://unpkg.com/@tripetto/runner-autoscroll"></script>
    <script>
    TripettoAutoscroll.run({
      definition: /* Supply a form definition here. */,
      //highlight-start
      onSubmit: function(instance) {
        // This exports all exportable data in the form
        const exportables = TripettoRunner.Export.exportables(instance);

        // Iterate through all the fields
        exportables.fields.forEach((field) => {
          // Output each field name and value to the console
          console.log(`${field.name}: ${field.string}`);
        });

        // This exports the collected data as a CSV object
        const csv = TripettoRunner.Export.CSV(instance);

        // Output CSV to the console
        console.log(csv.fields);
        console.log(csv.record);
      }
      //highlight-end
    });
    </script>
  </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/835cfff322900f55cd0ac54b2851100e) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/835cfff322900f55cd0ac54b2851100e)

</TabItem>
</Tabs>

</TabItem>
<TabItem value="chat" label="Chat">

<Tabs groupId="cdn">
<TabItem value="jsdelivr" label="jsDelivr">

```html showLineNumbers
<html>
  <body>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/runner"></script>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/runner-chat"></script>
    <script>
    TripettoChat.run({
      definition: /* Supply a form definition here. */,
      //highlight-start
      onSubmit: function(instance) {
        // This exports all exportable data in the form
        const exportables = TripettoRunner.Export.exportables(instance);

        // Iterate through all the fields
        exportables.fields.forEach((field) => {
          // Output each field name and value to the console
          console.log(`${field.name}: ${field.string}`);
        });

        // This exports the collected data as a CSV object
        const csv = TripettoRunner.Export.CSV(instance);

        // Output CSV to the console
        console.log(csv.fields);
        console.log(csv.record);
      }
      //highlight-end
    });
    </script>
  </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/eb46c1463dd72556c2dd219150dd80fa) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/eb46c1463dd72556c2dd219150dd80fa)

</TabItem>
<TabItem value="unpkg" label="unpkg">

```html showLineNumbers
<html>
  <body>
    <script src="https://unpkg.com/@tripetto/runner"></script>
    <script src="https://unpkg.com/@tripetto/runner-chat"></script>
    <script>
    TripettoChat.run({
      definition: /* Supply a form definition here. */,
      //highlight-start
      onSubmit: function(instance) {
        // This exports all exportable data in the form
        const exportables = TripettoRunner.Export.exportables(instance);

        // Iterate through all the fields
        exportables.fields.forEach((field) => {
          // Output each field name and value to the console
          console.log(`${field.name}: ${field.string}`);
        });

        // This exports the collected data as a CSV object
        const csv = TripettoRunner.Export.CSV(instance);

        // Output CSV to the console
        console.log(csv.fields);
        console.log(csv.record);
      }
      //highlight-end
    });
    </script>
  </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/e26cc00e6d71f8d8e02d8641633ca97c) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/e26cc00e6d71f8d8e02d8641633ca97c)

</TabItem>
</Tabs>
</TabItem>
<TabItem value="classic" label="Classic">

<Tabs groupId="cdn">
<TabItem value="jsdelivr" label="jsDelivr">

```html showLineNumbers
<html>
  <body>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/runner"></script>
    <script src="https://cdn.jsdelivr.net/npm/@tripetto/runner-classic"></script>
    <script>
    TripettoClassic.run({
      definition: /* Supply a form definition here. */,
      //highlight-start
      onSubmit: function(instance) {
        // This exports all exportable data in the form
        const exportables = TripettoRunner.Export.exportables(instance);

        // Iterate through all the fields
        exportables.fields.forEach((field) => {
          // Output each field name and value to the console
          console.log(`${field.name}: ${field.string}`);
        });

        // This exports the collected data as a CSV object
        const csv = TripettoRunner.Export.CSV(instance);

        // Output CSV to the console
        console.log(csv.fields);
        console.log(csv.record);
      }
      //highlight-end
    });
    </script>
  </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/b6765a6f53c42fcef5864fcf78591e52) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/b6765a6f53c42fcef5864fcf78591e52)

</TabItem>
<TabItem value="unpkg" label="unpkg">

```html showLineNumbers
<html>
  <body>
    <script src="https://unpkg.com/@tripetto/runner"></script>
    <script src="https://unpkg.com/@tripetto/runner-classic"></script>
    <script>
    TripettoClassic.run({
      definition: /* Supply a form definition here. */,
      //highlight-start
      onSubmit: function(instance) {
        // This exports all exportable data in the form
        const exportables = TripettoRunner.Export.exportables(instance);

        // Iterate through all the fields
        exportables.fields.forEach((field) => {
          // Output each field name and value to the console
          console.log(`${field.name}: ${field.string}`);
        });

        // This exports the collected data as a CSV object
        const csv = TripettoRunner.Export.CSV(instance);

        // Output CSV to the console
        console.log(csv.fields);
        console.log(csv.record);
      }
      //highlight-end
    });
    </script>
  </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/ffd8dced3674a88ea308c0289cf761d2) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/ffd8dced3674a88ea308c0289cf761d2)

</TabItem>
</Tabs>

</TabItem>
</Tabs>

:::tip
The `onSubmit` event supports some additional features for error handling. Have a look at the [Collecting response data guide](../guides/collecting.mdx) for more information and guidance.
:::

## 🌎 Self-host the library {#self-host}
If you want to host the runner files on a custom domain, you must publish the required JavaScript files to that domain. To do so, download the following files (right-click and select save as) and publish it to the desired domain. Update the `src`-attributes of the `script` tags, so it points to the domain and location of the JavaScript files.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

📥 https://unpkg.com/@tripetto/runner

📥 https://unpkg.com/@tripetto/runner-autoscroll

</TabItem>
<TabItem value="chat" label="Chat">

📥 https://unpkg.com/@tripetto/runner

📥 https://unpkg.com/@tripetto/runner-chat

</TabItem>
<TabItem value="classic" label="Classic">

📥 https://unpkg.com/@tripetto/runner

📥 https://unpkg.com/@tripetto/runner-classic

</TabItem>
</Tabs>

## 📖 Reference {#reference}

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll">

Have a look at the complete [autoscroll runner API reference](../../api/stock/autoscroll/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`run`](../../api/stock/autoscroll/functions/run.mdx)
- [`onSubmit`](../../api/stock/autoscroll/interfaces/IAutoscroll.mdx#onSubmit)

</TabItem>
<TabItem value="chat" label="Chat">

Have a look at the complete [chat runner API reference](../../api/stock/chat/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`run`](../../api/stock/chat/functions/run.mdx)
- [`onSubmit`](../../api/stock/chat/interfaces/IChat.mdx#onSubmit)

</TabItem>
<TabItem value="classic" label="Classic">

Have a look at the complete [classic runner API reference](../../api/stock/classic/index.md) for detailed documentation. In the examples above, the following symbols were used:
- [`run`](../../api/stock/classic/functions/run.mdx)
- [`onSubmit`](../../api/stock/classic/interfaces/IClassic.mdx#onSubmit)

</TabItem>
</Tabs>

## ⏭️ Up next {#up-next}
<UpNextTopics />
