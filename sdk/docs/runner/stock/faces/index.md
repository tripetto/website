---
title: Form faces - Stock runners
sidebar_label: Form faces
description: Each stock runner implements a different form UI. We call those UIs form faces.
---

# Form faces
Each stock runner implements a different form UI. We call those UIs form faces. Currently, the following form faces are available as stock runner:

- [Autoscroll runner](autoscroll.mdx): Fluently presents one question at a time;
- [Chat runner](chat.mdx): Presents all questions and answers as a chat;
- [Classic runner](classic.mdx): Presents question fields in a traditional form format.

:::tip
Click on the form face you wish to use for your project to learn how to implement that stock runner.
:::
