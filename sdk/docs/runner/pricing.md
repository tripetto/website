---
title: Runner Pricing
sidebar_label: Pricing
sidebar_position: 5
description: The runners are free to use as long as the Tripetto branding is enabled and clearly visible.
---

# 💳 Pricing
The runners are free to use as long as the [Tripetto branding](stock/guides/branding.mdx) is enabled and clearly visible. The runners will show a link to the Tripetto website and this helps to spread the word about Tripetto. If you leave the Tripetto branding enabled, you don't need a license to use the runners.

If you want to disable and remove the Tripetto branding you need a license to do so. There are three options to acquire a runner license:

- Option 1: Buy a runner SDK license to unlock the branding for a single runner on a single domain;
- Option 2: Buy a builder SDK license (the builder SDK license comes with the right to use all of the runners without branding);
- Option 3: Use the [Tripetto Studio application](../applications/studio/introduction.md) to build and host a form definition and [unlock the Tripetto branding](https://tripetto.com/help/articles/how-to-purchase-and-activate-form-upgrades-in-the-studio/) for a single form (this option requires to fetch the form definition from the [Studio API](../applications/studio/api/index.mdx) so the terms and conditions of the Tripetto Studio application apply).

Please see the [pricing](https://tripetto.com/sdk/pricing/) page for more information.

[➡️ Go to the pricing page](https://tripetto.com/sdk/pricing/)

[➡️ Learn more about using license codes](stock/guides/license.mdx)
