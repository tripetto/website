---
title: Runner API
description: This section contains the API reference for all the runner-related packages.
---

# Runner API reference
This section contains the API reference for all the runner-related packages.

---
## 🏃 Stock runners
[Stock runners](../stock/introduction.md) are full-features runners built and maintained by the Tripetto team. Each stock runner has its own appearance and user experience.

▶️ [Autoscroll runner API reference](stock/autoscroll/index.md)

▶️ [Chat runner API reference](stock/chat/index.md)

▶️ [Classic runner API reference](stock/classic/index.md)

▶️ [Classic Fluent UI runner API reference](stock/classic-fluentui/index.md)

---
## ⚙️ Runner Library
The Runner library is the workhorse of all Tripetto runners. It turns a form definition into an executable program; a [virtual finite state machine](https://en.wikipedia.org/wiki/Virtual_finite-state_machine) that handles all the complex logic and response collection during the execution of the form. Apply a UI on top of it, and you have a usable runner.

▶️ [Runner Library API reference](library/index.md)

---
## 🧶 Runner Fabric
The Runner Fabric package contains a set of UI components that are used in the [stock runners](../stock/introduction.md).

▶️ [Runner Fabric API reference](fabric/index.md)

---
## 🪝 Runner React Hook
The runner React Hook package wraps the [Runner library](library/index.md) in a convenient [React Hook](https://react.dev/reference/react), making it even easier to build a custom runner using [React](https://react.dev).

▶️ [Runner React Hook API reference](react-hook/index.md)
