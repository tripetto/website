---
title: Runner Fabric API
description: The Runner Fabric package contains a set of UI components that are used in the stock runners.
---

# ![](/img/logo-ts.svg) Runner Fabric API reference
The [Runner Fabric](https://www.npmjs.com/package/@tripetto/runner-fabric) package contains a set of UI components that are used in the [stock runners](../../stock/introduction.md). If you are developing [custom blocks](../../stock/guides/blocks.mdx) for those stock runners, you can use these controls in your custom block if you like.

## ✨ Installation [![](/img/logo-npm-icon.svg)](https://www.npmjs.com/package/@tripetto/runner-fabric) {#installation}

```bash npm2yarn
npm install @tripetto/runner-fabric
```
:::caution
This package needs the following packages: [@tripetto/runner](https://www.npmjs.com/package/@tripetto/runner), [react](https://www.npmjs.com/package/react), [styled-components](https://www.npmjs.com/package/styled-components), and [tslib](https://www.npmjs.com/package/tslib). You have to install those peer dependencies yourself.
:::

:::tip Typescript support
This package contains type declarations and supports [TypeScript](https://www.typescriptlang.org/) out-of-the-box.
:::

## ⚛️ Components {#components}
- [`ButtonFabric`](components/ButtonFabric.mdx)
- [`CheckboxesFabric`](components/CheckboxesFabric.mdx)
- [`CheckboxFabric`](components/CheckboxFabric.mdx)
- [`DateTimeFabric`](components/DateTimeFabric.mdx)
- [`DropdownFabric`](components/DropdownFabric.mdx)
- [`EmailFabric`](components/EmailFabric.mdx)
- [`FileFabric`](components/FileFabric.mdx)
- [`FileThumbnailFabric`](components/FileThumbnailFabric.mdx)
- [`InputFabric`](components/InputFabric.mdx)
- [`MatrixFabric`](components/MatrixFabric.mdx)
- [`MultiSelectFabric`](components/MultiSelectFabric.mdx)
- [`MultipleChoiceFabric`](components/MultipleChoiceFabric.mdx)
- [`NumberFabric`](components/NumberFabric.mdx)
- [`PasswordFabric`](components/PasswordFabric.mdx)
- [`PhoneNumberFabric`](components/PhoneNumberFabric.mdx)
- [`PictureChoiceFabric`](components/PictureChoiceFabric.mdx)
- [`RadiobuttonsFabric`](components/RadiobuttonsFabric.mdx)
- [`RatingFabric`](components/RatingFabric.mdx)
- [`RequiredIndicatorFabric`](components/RequiredIndicatorFabric.mdx)
- [`ScaleFabric`](components/ScaleFabric.mdx)
- [`TextareaFabric`](components/TextareaFabric.mdx)
- [`TextFabric`](components/TextFabric.mdx)
- [`URLFabric`](components/URLFabric.mdx)
- [`YesNoFabric`](components/YesNoFabric.mdx)

## 🪝 Hooks {#hooks}
- [`useFontLoader`](hooks/useFontLoader.mdx)
- [`useOverlay`](hooks/useOverlay.mdx)

## ▶️ Functions {#functions}
- [`avataaars`](functions/avataaars.mdx)
- [`color`](functions/color.mdx)

## ⛓️ Interfaces {#interfaces}
- [`IAvataaars`](interfaces/IAvataaars.mdx)
- [`IColorOperations`](interfaces/IColorOperations.mdx)

## 📁 Source code [![](/img/logo-gitlab.svg)](https://gitlab.com/tripetto/runner-fabric) {#source}
The Tripetto Runner Fabric package is open-source and the code is on [GitLab](https://gitlab.com/tripetto/runner-fabric).
