---
title: MultipleChoiceFabric component - Runner Fabric
sidebar_label: MultipleChoiceFabric
description: Component for rendering a list of selectable buttons.
---

# ![](/img/logo-react.svg) MultipleChoiceFabric component
Component for rendering a list of selectable buttons.
### 📝 Signature {#signature}
```ts
MultipleChoiceFabric(props: {
  styles: {
    color: string;
    outlineSize?: number;
    roundness?: number;
    scale?: number;
    margin?: number;
  };
  buttons: IMultipleChoiceButton[];
  alignment?: "vertical" | "equal" | "full" | "columns" | "horizontal";
  required?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  ariaDescribedBy?: string;
  tabIndex?: number;
  value?: string | Value;
  autoSubmit?: boolean;
  view?: "live" | "test" | "preview";
  onChange?: (value: string) => void;
  onFocus?: (e: FocusEvent) => void;
  onBlur?: (e: FocusEvent) => void;
  onAutoFocus?: (el: HTMLButtonElement | null) => void;
  onSubmit?: () => void;
  onCancel?: () => void;
}): JSX.Element
```
### 📇 Props {#props}
| Name              | Type                                                         | Optional | Description                                                                                                                                                                                                                                                                                                                                                                                    |
|:------------------|:-------------------------------------------------------------|:---------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `styles`          | object                                                       | No       | Specifies the styles for the buttons. Supports the following styles:<br/>- `color`: Base color for the buttons;<br/>- `outlineSize`: Optional outline size in pixels for the buttons;<br/>- `roundness`: Optional roundness in pixels for the buttons;<br/>- `scale`: Optional scale factor for the buttons (defaults to 1);<br/>- `margin`: Optional margin in pixels in between the buttons. |
| `buttons`         | [`IMultipleChoiceButton[]`](#IMultipleChoiceButton)          | No       | Specifies the buttons (see [`IMultipleChoiceButton`](#IMultipleChoiceButton)).                                                                                                                                                                                                                                                                                                                 |
| `alignment`       | "vertical" \| "equal" \| "full" \| "columns" \| "horizontal" | Yes      | Specifies the alignment of the button group (defaults to `vertical`).                                                                                                                                                                                                                                                                                                                          |
| `required`        | boolean                                                      | Yes      | Specifies if the component is required.                                                                                                                                                                                                                                                                                                                                                        |
| `disabled`        | boolean                                                      | Yes      | Specifies if the component is disabled.                                                                                                                                                                                                                                                                                                                                                        |
| `readOnly`        | boolean                                                      | Yes      | Specifies if the component is read-only.                                                                                                                                                                                                                                                                                                                                                       |
| `ariaDescribedBy` | string                                                       | Yes      | Specifies the [aria-describedby](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-describedby) identifier of the element that describes the component.                                                                                                                                                                                                          |
| `tabIndex`        | number                                                       | Yes      | Specifies the [`tabindex`](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex) for the component.                                                                                                                                                                                                                                                                    |
| `value`           | string \| [`Value`](../../library/classes/Value.mdx)      | Yes      | Specifies the value for the component. When a string is specified, this is the initial value for the input control. When a [`Value`](../../library/classes/Value.mdx) is supplied, this initial value is retrieved from the Value instance. Changes made in the input control are automatically reflected to the `Value` instance.                                                           |
| `autoSubmit`      | boolean                                                      | Yes      | Specifies if the component gets submitted when the user selects a button.                                                                                                                                                                                                                                                                                                                     |
| `view`            | "live" \| "test" \| "preview"                                | Yes      | Specifies the view in which the component is shown.                                                                                                                                                                                                                                                                                                                                            |
| `onChange`        | (value: string) => void                                      | Yes      | Invoked when the value of the component is changed.                                                                                                                                                                                                                                                                                                                                            |
| `onFocus`         | (e: FocusEvent) => void                                      | Yes      | Invoked when the component gets focus.                                                                                                                                                                                                                                                                                                                                                         |
| `onBlur`          | (e: FocusEvent) => void                                      | Yes      | Invoked when the component loses focus.                                                                                                                                                                                                                                                                                                                                                        |
| `onAutoFocus`     | (el: HTMLButtonElement \| null) => void                      | Yes      | Invoked when the HTML element of the component is available for auto-focus.                                                                                                                                                                                                                                                                                                                    |
| `onSubmit`        | () => void                                                   | Yes      | Invoked when the user presses the `Shift` + `Enter` key combination or the `Tab` key (this event is often used to go to the next input control in a form).                                                                                                                                                                                                                                     |
| `onCancel`        | () => void                                                   | Yes      | Invoked when the user presses the `Shift` + `Tab` key combination (this event is often used to go to the previous input control in a form).                                                                                                                                                                                                                                                    |
### ↩️ Return value {#return-value}
Returns the `JSX.Element` for the component.
### 👩‍💻 Example {#example}
```ts showLineNumbers
import { MultipleChoiceFabric } from "@tripetto/runner-fabric/components/multiple-choice";

const MultipleChoiceFabricExample = () => (
  <MultipleChoiceFabric
    styles={{
      color: "green"
    }}
    buttons={[
      {
        id: "1",
        label: "Button 1",
        onChange: (selected) => console.log(`Button 1 selected: ${selected}`)
      },
      {
        id: "2",
        label: "Button 2",
        onChange: (selected) => console.log(`Button 2 selected: ${selected}`)
      }
    ]}
  />
);
```

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `IMultipleChoiceButton` {#IMultipleChoiceButton}
Describes the interface for a multiple choice button.
#### Type declaration
```ts
interface IMultipleChoiceButton {
  /* Identifier for the button. */
  id: string;

  /* Name for the button. */
  name: string;

  /* Value for the button. */
  value?: string;

  /* Description for the button. */
  description?: string;

  /* URL for the button. */
  url?: string;

  /* Target for the button URL. */
  target?: "self" | "blank";

  /* Specifies if the button is disabled. */
  disabled?: boolean;

  /* Specifies the button slot. */
  slot: Slots.Boolean;

  /* Tabindex for the button. */
  tabIndex?: number;

  /* Specifies the onChange function for the button. */
  onChange?: (selected: boolean) => void;

```
