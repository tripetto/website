---
title: MultiSelectFabric component - Runner Fabric
sidebar_label: MultiSelectFabric
description: Component for rendering a multi-select dropdown with keyword search.
---

# ![](/img/logo-react.svg) MultiSelectFabric component
Component for rendering a multi-select dropdown with keyword search.
### 📝 Signature {#signature}
```ts
MultiSelectFabric(props: {
  styles: {
    backgroundColor: string;
    borderColor: string;
    borderSize?: number;
    roundness?: number;
    textColor?: string;
    errorColor: string;
    scale?: number;
    pills?: "filled" | "outline";
    overlayWidth?: "auto" | "full";
  };
  overlay: TOverlayContext;
  id: string;
  options: IMultiSelectOption[];
  placeholder?: string;
  required?: boolean;
  error?: boolean;
  tabIndex?: number;
  maxSelected?: number;
  ariaDescribedBy?: string;
  onChange?: (value: string) => void;
  onFocus?: (e: FocusEvent) => void;
  onBlur?: (e: FocusEvent) => void;
  onAutoFocus?: (el: HTMLInputElement | null) => void;
  onSubmit?: () => void;
  onCancel?: () => void;
}): JSX.Element
```
### 📇 Props {#props}
| Name              | Type                                          | Optional | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
|:------------------|:----------------------------------------------|:---------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `styles`          | object                                        | No       | Specifies the styles for the multi-select component. Supports the following styles:<br/>- `backgroundColor`: Background color for the control;<br/>- `borderColor`: Border color for the control;<br/>- `borderSize`: Optional border size in pixels for the control;<br/>- `roundness`: Optional roundness in pixels for the control;<br/>- `textColor`: Text color for the component;<br/>- `errorColor`: Error color for the component;<br/>- `scale`: Optional scale factor for the component (defaults to `1`);<br/>- `pills`: Optional setting to display the pills (defaults to `filled`);<br/>- `overlayWidth`: Optional setting to display the overlay (defaults to `auto`). |
| `overlay`         | TOverlayContext                               | No       | Specifies the overlay context used for rendering the list with options.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| `id`              | string                                        | No       | Specifies the identifier.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| `options`         | [`IMultiSelectOption[]`](#IMultiSelectOption) | No       | Specifies the options (see [`IMultiSelectOption`](#IMultiSelectOption)).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `placeholder`     | string                                        | Yes      | Optional placeholder that is shown when there is no value entered.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| `required`        | boolean                                       | Yes      | Specifies if the component is required.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| `error`           | boolean                                       | Yes      | Specifies if the component has an error.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `tabIndex`        | number                                        | Yes      | Specifies the [`tabindex`](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex) for the component.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| `maxSelected`     | number                                        | Yes      | Specifies the maximum amount of options to select for the component.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| `ariaDescribedBy` | string                                        | Yes      | Specifies the [aria-describedby](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-describedby) identifier of the element that describes the component.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| `onChange`        | (value: string) => void                       | Yes      | Invoked when the value of the component is changed.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| `onFocus`         | (e: FocusEvent) => void                       | Yes      | Invoked when the component gets focus.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| `onBlur`          | (e: FocusEvent) => void                       | Yes      | Invoked when the component loses focus.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| `onAutoFocus`     | (el: HTMLInputElement \| null) => void        | Yes      | Invoked when the HTML element of the component is available for auto-focus.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| `onSubmit`        | () => void                                    | Yes      | Invoked when the user presses the `Shift` + `Enter` key combination or the `Tab` key (this event is often used to go to the next input control in a form).                                                                                                                                                                                                                                     |
| `onCancel`        | () => void                                    | Yes      | Invoked when the user presses the `Shift` + `Tab` key combination (this event is often used to go to the previous input control in a form).                                                                                                                                                                                                                                                    |
### ↩️ Return value {#return-value}
Returns the `JSX.Element` for the component.
### 👩‍💻 Example {#example}
```ts showLineNumbers
import { MultiSelectFabric } from "@tripetto/runner-fabric/components/multi-select";
import { useOverlay } from "@tripetto/runner-fabric/overlay";

const MultiSelectExample = () => {
  const [OverlayProvider, OverlayContext] = useOverlay();

  return (
    <>
      <MultiSelectFabric
        styles={{
          backgroundColor: "white",
          borderColor: "black",
          errorColor: "red"
        }}
        overlay: OverlayContext
        id: "1"
        options={[
          {
            id: "1",
            name: "Option 1"
          },
          {
            id: "2",
            name: "Option 2"
          }
        ]}
      />
      <OverlayProvider />
    </>
  );
}
```

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `IMultiSelectOption` {#IMultiSelectOption}
Describes the interface for a multi-select option.
#### Type declaration
```ts
interface IMultiSelectOption {
  /* Identifier for the option. */
  id: string;

  /* Name for the option. */
  name: string;

  /* Label for the option. */
  label?: string | JSX.Element;

  /* Description for the option. */
  description?: string | JSX.Element;

  /* Specifies if the option is disabled. */
  disabled?: boolean;

  /* Value for the option. */
  value?: Value;
}
```
