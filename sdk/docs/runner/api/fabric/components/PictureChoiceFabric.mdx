---
title: PictureChoiceFabric component - Runner Fabric
sidebar_label: PictureChoiceFabric
description: Component for rendering a list of selectable images or emojis.
---

# ![](/img/logo-react.svg) PictureChoiceFabric component
Component for rendering a list of selectable images or emojis.
### 📝 Signature {#signature}
```ts
PictureChoiceFabric(props: {
  styles: {
    color: string;
    outlineSize?: number;
    roundness?: number;
    scale?: number;
    margin?: number;
  };
  options: IPictureChoiceOption[];
  size?: "small" | "medium" | "large";
  required?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  ariaDescribedBy?: string;
  tabIndex?: number;
  value?: string | Value;
  autoSubmit?: boolean;
  view?: "live" | "test" | "preview";
  onChange?: (value: string) => void;
  onFocus?: (e: FocusEvent) => void;
  onBlur?: (e: FocusEvent) => void;
  onAutoFocus?: (el: HTMLButtonElement | null) => void;
  onSubmit?: () => void;
  onCancel?: () => void;
}): JSX.Element
```
### 📇 Props {#props}
| Name              | Type                                                    | Optional | Description                                                                                                                                                                                                                                                                                                                                                                                    |
|:------------------|:--------------------------------------------------------|:---------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `styles`          | object                                                  | No       | Specifies the styles for the options. Supports the following styles:<br/>- `color`: Base color for the buttons;<br/>- `outlineSize`: Optional outline size in pixels for the buttons;<br/>- `roundness`: Optional roundness in pixels for the buttons;<br/>- `scale`: Optional scale factor for the buttons (defaults to 1);<br/>- `margin`: Optional margin in pixels in between the buttons. |
| `options`         | [`IPictureChoiceOption[]`](#IPictureChoiceOption)       | No       | Specifies the options (see [`IPictureChoiceOption`](#IPictureChoiceOption)).                                                                                                                                                                                                                                                                                                                   |
| `size`            | "small" \| "medium" \| "large"                          | Yes      | Specifies the size of the options.                                                                                                                                                                                                                                                                                                                          |
| `required`        | boolean                                                 | Yes      | Specifies if the component is required.                                                                                                                                                                                                                                                                                                                                                        |
| `disabled`        | boolean                                                 | Yes      | Specifies if the component is disabled.                                                                                                                                                                                                                                                                                                                                                        |
| `readOnly`        | boolean                                                 | Yes      | Specifies if the component is read-only.                                                                                                                                                                                                                                                                                                                                                       |
| `ariaDescribedBy` | string                                                  | Yes      | Specifies the [aria-describedby](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-describedby) identifier of the element that describes the component.                                                                                                                                                                                                          |
| `tabIndex`        | number                                                  | Yes      | Specifies the [`tabindex`](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex) for the component.                                                                                                                                                                                                                                                                    |
| `value`           | string \| [`Value`](../../library/classes/Value.mdx) | Yes      | Specifies the value for the component. When a string is specified, this is the initial value for the input control. When a [`Value`](../../library/classes/Value.mdx) is supplied, this initial value is retrieved from the Value instance. Changes made in the input control are automatically reflected to the `Value` instance.                                                           |
| `autoSubmit`      | boolean                                                 | Yes      | Specifies if the component gets submitted when the user selects a button.                                                                                                                                                                                                                                                                                                                      |
| `view`            | "live" \| "test" \| "preview"                           | Yes      | Specifies the view in which the component is shown.                                                                                                                                                                                                                                                                                                                                            |
| `onChange`        | (value: string) => void                                 | Yes      | Invoked when the value of the component is changed.                                                                                                                                                                                                                                                                                                                                            |
| `onFocus`         | (e: FocusEvent) => void                                 | Yes      | Invoked when the component gets focus.                                                                                                                                                                                                                                                                                                                                                         |
| `onBlur`          | (e: FocusEvent) => void                                 | Yes      | Invoked when the component loses focus.                                                                                                                                                                                                                                                                                                                                                        |
| `onAutoFocus`     | (el: HTMLButtonElement \| null) => void                 | Yes      | Invoked when the HTML element of the component is available for auto-focus.                                                                                                                                                                                                                                                                                                                    |
| `onSubmit`        | () => void                                              | Yes      | Invoked when the user presses the `Shift` + `Enter` key combination or the `Tab` key (this event is often used to go to the next input control in a form).                                                                                                                                                                                                                                     |
| `onCancel`        | () => void                                              | Yes      | Invoked when the user presses the `Shift` + `Tab` key combination (this event is often used to go to the previous input control in a form).                                                                                                                                                                                                                                                    |
### ↩️ Return value {#return-value}
Returns the `JSX.Element` for the component.
### 👩‍💻 Example {#example}
```ts showLineNumbers
import { PictureChoiceFabric } from "@tripetto/runner-fabric/components/picture-choice";

const PictureChoiceFabricExample = () => (
  <PictureChoiceFabric
    styles={{
      color: "green"
    }}
    options={[
      {
        id: "1",
        emoji: "🙂",
        onChange: (selected) => console.log(`Option 1 selected: ${selected}`)
      },
      {
        id: "2",
        label: "😀",
        onChange: (selected) => console.log(`Option 2 selected: ${selected}`)
      }
    ]}
  />
);
```

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `IPictureChoiceOption` {#IPictureChoiceOption}
Describes the interface for a picture choice option.
#### Type declaration
```ts
interface IPictureChoiceOption {
  /* Identifier for the option. */
  id: string;

  /* Image for the option. */
  image?: string;

  /* Emoji for the option. */
  emoji?: string;

  /* Name for the option. */
  name?: string;

  /* Specifies if the name is visible. */
  nameVisible?: boolean;

  /* Value for the option. */
  value?: string;

  /* Description for the option. */
  description?: string;

  /* URL for the option. */
  url?: string;

  /* Target for the option URL. */
  target?: "self" | "blank";

  /* Specifies if the option is disabled. */
  disabled?: boolean;

  /* Specifies the option slot. */
  slot: Slots.Boolean;

  /* Tabindex for the option. */
  tabIndex?: number;

  /* Specifies the onChange function for the option. */
  onChange?: (selected: boolean) => void;
}
```
