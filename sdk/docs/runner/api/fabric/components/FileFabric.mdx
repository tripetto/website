---
title: FileFabric component - Runner Fabric
sidebar_label: FileFabric
description: Component for rendering a file upload control.
---

# ![](/img/logo-react.svg) FileFabric component
Component for rendering a file upload control.
### 📝 Signature {#signature}
```ts
FileFabric(props: {
  styles: {
    backgroundColor: string;
    borderColor: string;
    borderSize?: number;
    roundness?: number;
    textColor?: string;
    errorColor: string;
    scale?: number;
  };
  controller: IFileController;
  labels: (id: "explanation" | "dragging" | "limit" | "extensions" | "retry" | "progress" | "delete" | "invalid-file" | "invalid-amount" | "invalid-extension" | "invalid-size" | "error", message: string) => string;
  service?: IFileService;
  disabled?: boolean;
  readOnly?: boolean;
  error?: boolean;
  tabIndex?: number;
  ariaDescribedBy?: string;
  onFocus?: (e: FocusEvent) => void;
  onBlur?: (e: FocusEvent) => void;
  onAutoFocus?: (el: HTMLDivElement | null) => void;
  onSubmit?: () => void;
  onCancel?: () => void;
}): JSX.Element
```
### 📇 Props {#props}
| Name              | Type                                                                                                                                                                                                                   | Optional | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
|:------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:---------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `styles`          | object                                                                                                                                                                                                                 | No       | Specifies the styles for the file component. Supports the following styles:<br/>- `backgroundColor`: Background color for the file control;<br/>- `borderColor`: Border color for the file control;<br/>- `borderSize`: Optional border size in pixels for the file control;<br/>- `roundness`: Optional roundness in pixels for the file control;<br/>- `textColor`: Optional text color for the file control;<br/>- `errorColor`: Error color for the file control;<br/>- `scale`: Optional scale factor for the file control (defaults to `1`). |
| `controller`      | [`IFileController[]`](#IFileController)                                                                                                                                                                                | No       | Specifies the file controller (see [`IFileController`](#IFileController)).                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| `labels`          | (id: "explanation" \| "dragging" \| "limit" \| "extensions" \| "retry" \| "progress" \| "delete" \| "invalid-file" \| "invalid-amount" \| "invalid-extension" \| "invalid-size" \| "error", message: string) => string | No       | Specifies the labels for the component.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| `service`         | [`IFileService[]`](#IFileService)                                                                                                                                                                                      | Yes      | Specifies the file service (see [`IFileService`](#IFileService)).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| `disabled`        | boolean                                                                                                                                                                                                                | Yes      | Specifies if the component is disabled.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| `readOnly`        | boolean                                                                                                                                                                                                                | Yes      | Specifies if the component is read-only.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| `error`           | boolean                                                                                                                                                                                                                | Yes      | Specifies if the component has an error.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| `tabIndex`        | number                                                                                                                                                                                                                 | Yes      | Specifies the [`tabindex`](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex) for the component.                                                                                                                                                                                                                                                                                                                                                                                                                        |
| `ariaDescribedBy` | string                                                                                                                                                                                                                 | Yes      | Specifies the [aria-describedby](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-describedby) identifier of the element that describes the component.                                                                                                                                                                                                                                                                                                                                                              |
| `onFocus`         | (e: FocusEvent) => void                                                                                                                                                                                                | Yes      | Invoked when the component gets focus.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| `onBlur`          | (e: FocusEvent) => void                                                                                                                                                                                                | Yes      | Invoked when the component loses focus.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| `onAutoFocus`     | (el: HTMLDivElement \| null) => void                                                                                                                                                                                   | Yes      | Invoked when the HTML element of the component is available for auto-focus.                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| `onSubmit`        | () => void                                                                                                                                                                                                             | Yes      | Invoked when the user presses the `Shift` + `Enter` key combination or the `Tab` key (this event is often used to go to the next input control in a form).                                                                                                                                                                                                                                                                                                                                                                                         |
| `onCancel`        | () => void                                                                                                                                                                                                             | Yes      | Invoked when the user presses the `Shift` + `Tab` key combination (this event is often used to go to the previous input control in a form).                                                                                                                                                                                                                                                                                                                                                                                                        |

### ↩️ Return value {#return-value}
Returns the `JSX.Element` for the component.

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `IFileController` {#IFileController}
Describes the interface for a file controller.
#### Type declaration
```ts
interface IFileController {
  /* Specifies if the file is an image. */
  isImage: boolean;

  /* Specifies the limit. */
  limit: number;

  /* Specifies the allowed file extenstions. */
  allowedExtensions?: string[];

  /* Specifies the upload function. */
  upload: (files: FileList, service: IFileService | undefined, onProgress: (percent: number) => void) => Promise<void>;

  /* Specifies the download function. */
  download: (service?: IFileService) => Promise<string>;

  /* Specifies the delete function. */
  delete: (service?: IFileService) => Promise<void>;

  /* Specifies the file slot. */
  fileSlot: Slots.String;
}
```
### 🔗 `IFileService` {#IFileService}
Describes the interface for a file service.
:::tip
Have a look at the [Handling file uploads](../../../stock/guides/attachments.mdx) guide for more information on how to use this interface.
:::
#### Type declaration
```ts
interface IFileService {
  /* Specifies the get function. */
  get: (file: string) => Promise<Blob>;

  /* Specifies the put function. */
  put: (file: File, onProgress?: (percentage: number) => void) => Promise<string>;

  /* Specifies the delete function. */
  delete: (file: string) => Promise<void>;
}
```
