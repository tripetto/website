---
title: ScaleFabric component - Runner Fabric
sidebar_label: ScaleFabric
description: Component for rendering a scale control.
---

# ![](/img/logo-react.svg) ScaleFabric component
Component for rendering a scale control.
### 📝 Signature {#signature}
```ts
ScaleFabric(props: {
  styles: {
    color: string;
    outlineSize?: number;
    roundness?: number;
    scale?: number;
    margin?: number;
    labelColor?: string;
  };
  options: IScaleNumeric | IScaleOption[];
  labelLeft?: ReactNode;
  labelCenter?: ReactNode;
  labelRight?: ReactNode;
  required?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  ariaDescribedBy?: string;
  tabIndex?: number;
  justify?: boolean;
  value?: number | string | Value;
  autoSubmit?: boolean;
  view?: "live" | "test" | "preview";
  onChange?: (value: number | string | undefined) => void;
  onFocus?: (e: FocusEvent) => void;
  onBlur?: (e: FocusEvent) => void;
  onAutoFocus?: (el: HTMLButtonElement | null) => void;
  onSubmit?: () => void;
  onCancel?: () => void;
}): JSX.Element
```
### 📇 Props {#props}
| Name              | Type                                                                     | Optional | Description                                                                                                                                                                                                                                                                                                                                                                                                                                     |
|:------------------|:-------------------------------------------------------------------------|:---------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `styles`          | object                                                                   | No       | Specifies the styles for the scale. Supports the following styles:<br/>- `color`: Base color for the buttons;<br/>- `outlineSize`: Optional outline size in pixels for the buttons;<br/>- `roundness`: Optional roundness in pixels for the buttons;<br/>- `scale`: Optional scale factor for the buttons (defaults to 1);<br/>- `margin`: Optional margin in pixels in between the buttons;<br/>- `labelColor`: Optional color for the labels. |
| `options`         | [`IScaleNumeric`](#IScaleNumeric) \| [`IScaleOption[]`](#IScaleOption[]) | No       | Specifies the scale. Can be a numeric scale (see [`IScaleNumeric`](#IScaleNumeric)) or a scale with options (see [`IScaleOption`](#IScaleOption)).                                                                                                                                                                                                                                                                                              |
| `labelLeft`       | ReactNode                                                                | Yes      | Specifies the label at the left side of the scale.                                                                                                                                                                                                                                                                                                                                                                                              |
| `labelCenter`     | ReactNode                                                                | Yes      | Specifies the label at the center of the scale.                                                                                                                                                                                                                                                                                                                                                                                                 |
| `labelRight`      | ReactNode                                                                | Yes      | Specifies the label at the right side of the scale.                                                                                                                                                                                                                                                                                                                                                                                             |
| `required`        | boolean                                                                  | Yes      | Specifies if the component is required.                                                                                                                                                                                                                                                                                                                                                                                                         |
| `disabled`        | boolean                                                                  | Yes      | Specifies if the component is disabled.                                                                                                                                                                                                                                                                                                                                                                                                         |
| `readOnly`        | boolean                                                                  | Yes      | Specifies if the component is read-only.                                                                                                                                                                                                                                                                                                                                                                                                        |
| `ariaDescribedBy` | string                                                                   | Yes      | Specifies the [aria-describedby](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-describedby) identifier of the element that describes the component.                                                                                                                                                                                                                                                           |
| `tabIndex`        | number                                                                   | Yes      | Specifies the [`tabindex`](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex) for the component.                                                                                                                                                                                                                                                                                                                     |
| `justify`         | boolean                                                                  | Yes      | Specifies if the component uses the full available width.                                                                                                                                                                                                                                                                                                                                                                                       |
| `value`           | number \| string \| [`Value`](../../library/classes/Value.mdx)        | Yes      | Specifies the value for the component. When a number or string is specified, this is the initial value for the input control. When a [`Value`](../../library/classes/Value.mdx) is supplied, this initial value is retrieved from the Value instance. Changes made in the input control are automatically reflected to the `Value` instance.                                                                                                            |
| `autoSubmit`      | boolean                                                                  | Yes      | Specifies if the component gets submitted when the user selects a scale.                                                                                                                                                                                                                                                                                                                                                                        |
| `view`            | "live" \| "test" \| "preview"                                            | Yes      | Specifies the view in which the component is shown.                                                                                                                                                                                                                                                                                                                                                                                             |
| `onChange`        | (value: number \| string \| undefined) => void                             | Yes      | Invoked when the value of the component is changed.                                                                                                                                                                                                                                                                                                                                                                                             |
| `onFocus`         | (e: FocusEvent) => void                                                  | Yes      | Invoked when the component gets focus.                                                                                                                                                                                                                                                                                                                                                                                                          |
| `onBlur`          | (e: FocusEvent) => void                                                  | Yes      | Invoked when the component loses focus.                                                                                                                                                                                                                                                                                                                                                                                                         |
| `onAutoFocus`     | (el: HTMLButtonElement \| null) => void                                  | Yes      | Invoked when the HTML element of the component is available for auto-focus.                                                                                                                                                                                                                                                                                                                                                                     |
| `onSubmit`        | () => void                                                               | Yes      | Invoked when the user presses the `Shift` + `Enter` key combination or the `Tab` key (this event is often used to go to the next input control in a form).                                                                                                                                                                                                                                                                                      |
| `onCancel`        | () => void                                                               | Yes      | Invoked when the user presses the `Shift` + `Tab` key combination (this event is often used to go to the previous input control in a form).                                                                                                                                                                                                                                                                                                     |
### ↩️ Return value {#return-value}
Returns the `JSX.Element` for the component.
### 👩‍💻 Example {#example}
#### Numeric scale
```ts showLineNumbers
import { ScaleFabric } from "@tripetto/runner-fabric/components/scale";

const ScaleFabricExample = () => (
  <ScaleFabric
    styles={{
      color: "black"
    }}
    options={{
      from: 0,
      to: 10
    }}
    onChange={(value) => console.log(`Selected scale: ${value}`)}
  />
);
```
#### Option scale
```ts showLineNumbers
import { ScaleFabric } from "@tripetto/runner-fabric/components/scale";

const ScaleFabricExample = () => (
  <ScaleFabric
    styles={{
      color: "black"
    }}
    options={[
      {
        id: "1",
        label: "Scale option 1"
      },
      {
        id: "2",
        label: "Scale option 2"
      }
    ]}
    onChange={(value) => console.log(`Selected scale: ${value}`)}
  />
);
```

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `IScaleNumeric` {#IScaleNumeric}
Describes the interface for a numeric scale.
#### Type declaration
```ts
interface IScaleNumeric {
  /* Start of the numeric scale. */
  from: number;

  /* End of the numeric scale. */
  to: number;

  /* Step size of the numeric scale (defaults to 1). */
  stepSize?: number;
}
```
### 🔗 `IScaleOption` {#IScaleOption}
Describes the interface for a scale option.
#### Type declaration
```ts
interface IScaleOption {
  /* Identifier for the scale option. */
  id: string;

  /* Name for the scale option. */
  name: string;

  /* Value for the scale option. */
  value?: string;
}
```
