---
title: InputFabric component - Runner Fabric
sidebar_label: InputFabric
description: Base component for rendering an input control.
---

# ![](/img/logo-react.svg) InputFabric component
Base component for rendering an input control.
### 📝 Signature {#signature}
```ts
InputFabric(props: {
  type: "text" | "email" | "url" | "password" | "tel" | "number";
  styles: {
    backgroundColor: string;
    borderColor: string;
    borderSize?: number;
    roundness?: number;
    textColor?: string;
    errorColor: string;
    scale?: number;
  };
  id?: string;
  placeholder?: string;
  required?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  error?: boolean;
  tabIndex?: number;
  maxLength?: number;
  value?: string | Value;
  ariaDescribedBy?: string;
  inputMode?: "text" | "tel" | "url" | "email" | "numeric" | "decimal" | "search";
  autoComplete?: string;
  list?: string;
  onChange?: (value: string) => string | void;
  onFocus?: (e: FocusEvent) => string | void;
  onBlur?: (e: FocusEvent) => string | void;
  onAutoFocus?: (el: HTMLInputElement | null) => void;
  onSubmit?: () => void;
  onCancel?: () => void;
}): JSX.Element
```
### 📇 Props {#props}
| Name              | Type                                                                      | Optional | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
|:------------------|:--------------------------------------------------------------------------|:---------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `type`            | "text" \| "email" \| "url" \| "password" \| "tel" \| "number"             | No       | Specifies the type of the input component.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| `styles`          | object                                                                    | No       | Specifies the styles for the input component. Supports the following styles:<br/>- `backgroundColor`: Background color for the input control;<br/>- `borderColor`: Border color for the input control;<br/>- `borderSize`: Optional border size in pixels for the input control;<br/>- `roundness`: Optional roundness in pixels for the input control;<br/>- `textColor`: Optional text color for the input control;<br/>- `errorColor`: Error color for the input control;<br/>- `scale`: Optional scale factor for the input control (defaults to `1`). |
| `id`              | string                                                                    | Yes      | Optional identifier for the component.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `placeholder`     | string                                                                    | Yes      | Optional placeholder that is shown when there is no value entered.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| `required`        | boolean                                                                   | Yes      | Specifies if the component is required.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| `disabled`        | boolean                                                                   | Yes      | Specifies if the component is disabled.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| `readOnly`        | boolean                                                                   | Yes      | Specifies if the component is read-only.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| `error`           | boolean                                                                   | Yes      | Specifies if the component has an error.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| `tabIndex`        | number                                                                    | Yes      | Specifies the [`tabindex`](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex) for the component.                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| `maxLength`       | number                                                                    | Yes      | Specifies the maximum characters that can be entered in the input control.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| `value`           | string \| [`Value`](../../library/classes/Value.mdx)                   | Yes      | Specifies the value for the component. When a string is specified, this is the initial value for the input control. When a [`Value`](../../library/classes/Value.mdx) is supplied, this initial value is retrieved from the Value instance. Changes made in the input control are automatically reflected to the `Value` instance.                                                                                                                                                                                                                                |
| `ariaDescribedBy` | string                                                                    | Yes      | Specifies the [aria-describedby](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-describedby) identifier of the element that describes the component.                                                                                                                                                                                                                                                                                                                                                                               |
| `inputMode`       | "text" \| "tel" \| "url" \| "email" \| "numeric" \| "decimal" \| "search" | Yes      | Specifies the [inputmode](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/inputmode) of the input control.                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| `autoComplete`    | string                                                                    | Yes      | Specifies the [autocomplete](https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes/autocomplete) attribute of the input control.                                                                                                                                                                                                                                                                                                                                                                                                                             |
| `list`            | string                                                                    | Yes      | Specifies the identifier of a [datalist](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/text#list).                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| `onChange`        | (value: string) => string \| void                                         | Yes      | Invoked when the value of the component is changed.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| `onFocus`         | (e: FocusEvent) => string \| void                                         | Yes      | Invoked when the component gets focus.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `onBlur`          | (e: FocusEvent) => string \| void                                         | Yes      | Invoked when the component loses focus.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| `onAutoFocus`     | (el: HTMLInputElement \| null) => void                                    | Yes      | Invoked when the HTML element of the component is available for auto-focus.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| `onSubmit`        | () => void                                                                | Yes      | Invoked when the user presses the `Shift` + `Enter` key combination or the `Tab` key (this event is often used to go to the next input control in a form).                                                                                                                                                                                                                                                                                                                                                                                                          |
| `onCancel`        | () => void                                                                | Yes      | Invoked when the user presses the `Shift` + `Tab` key combination (this event is often used to go to the previous input control in a form).                                                                                                                                                                                                                                                                                                                                                                                                                         |
### ↩️ Return value {#return-value}
Returns the `JSX.Element` for the component.
### 👩‍💻 Example {#example}
```ts showLineNumbers
import { InputFabric } from "@tripetto/runner-fabric/components/input";

const InputFabricExample = () => (
  <InputFabric
    type="text"
    styles={{
      backgroundColor: "white",
      borderColor: "black",
      errorColor: "red"
    }}
    onChange={(value) => console.log(`Entered value: ${value}`)}
  />
);
```
