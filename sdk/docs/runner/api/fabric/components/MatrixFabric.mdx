---
title: MatrixFabric component - Runner Fabric
sidebar_label: MatrixFabric
description: Component for rendering a matrix with rows and columns.
---

# ![](/img/logo-react.svg) MatrixFabric component
Component for rendering a matrix with rows and columns.
### 📝 Signature {#signature}
```ts
MatrixFabric(props: {
  styles: {
    backgroundColor: string;
    borderColor: string;
    borderSize?: number;
    textColor: string;
    errorColor: string;
    scale?: number;
    hideRequiredIndicator?: boolean;
  };
  columns: IMatrixColumn[];
  rows: IMatrixRow[];
  required?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  allowUnselect?: boolean;
  tabIndex?: number;
  ariaDescribedBy?: string;
  onFocus?: (e: FocusEvent) => void;
  onBlur?: (e: FocusEvent) => void;
  onAutoFocus?: (el: HTMLInputElement | null) => void;
  onSubmit?: () => void;
  onCancel?: () => void;
}): JSX.Element
```
### 📇 Props {#props}
| Name              | Type                                   | Optional | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
|:------------------|:---------------------------------------|:---------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `styles`          | object                                 | No       | Specifies the styles for the matrix control. Supports the following styles:<br/>- `backgroundColor`: Background color for the input control;<br/>- `borderColor`: Border color for the input control;<br/>- `borderSize`: Optional border size in pixels for the input control;<br/>- `textColor`: Text color for the input control;<br/>- `errorColor`: Optional error color for the input control;<br/>- `scale`: Optional scale factor for the input control (defaults to `1`);<br/>- `hideRequiredIndicator`: Specifies if the indicator (asterisk) for a required row is hidden. |
| `columns`         | [`IMatrixColumn[]`](#IMatrixColumn)    | No       | Specifies the columns for the matrix (see [`IMatrixColumn`](#IMatrixColumn)).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| `rows`            | [`IMatrixRow[]`](#IMatrixRow)          | No       | Specifies the rows for the matrix (see [`IMatrixRow`](#IMatrixRow)).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| `required`        | boolean                                | Yes      | Specifies if the component is required.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| `disabled`        | boolean                                | Yes      | Specifies if the component is disabled.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| `readOnly`        | boolean                                | Yes      | Specifies if the component is read-only.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `allowUnselect`   | boolean                                | Yes      | Specifies if unselecting is allowed for the component.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| `tabIndex`        | number                                 | Yes      | Specifies the [`tabindex`](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex) for the component.                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| `ariaDescribedBy` | string                                 | Yes      | Specifies the [aria-describedby](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-describedby) identifier of the element that describes the component.                                                                                                                                                                                                                                                                                                                                                                                                 |
| `onFocus`         | (e: FocusEvent) =>  void               | Yes      | Invoked when the component gets focus.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| `onBlur`          | (e: FocusEvent) =>  void               | Yes      | Invoked when the component loses focus.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| `onAutoFocus`     | (el: HTMLInputElement \| null) => void | Yes      | Invoked when the HTML element of the component is available for auto-focus.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| `onSubmit`        | () => void                             | Yes      | Invoked when the user presses the `Shift` + `Enter` key combination or the `Tab` key (this event is often used to go to the next input control in a form).                                                                                                                                                                                                                                                                                                                                                                                                                            |
| `onCancel`        | () => void                             | Yes      | Invoked when the user presses the `Shift` + `Tab` key combination (this event is often used to go to the previous input control in a form).                                                                                                                                                                                                                                                                                                                                                                                                                                           |
### ↩️ Return value {#return-value}
Returns the `JSX.Element` for the component.
### 👩‍💻 Example {#example}
```ts showLineNumbers
import { MatrixFabric } from "@tripetto/runner-fabric/components/matrix";

const MatrixFabricExample = () => (
  <MatrixFabric
    styles={{
      backgroundColor: "white",
      borderColor: "black",
      textColor: "black",
      errorColor: "red"
    }}
    columns={[
      {
        id: "1",
        label: "Column 1"
      },
      {
        id: "2",
        label: "Column 2"
      }
    ]}
    rows={[
      {
        id: "1",
        label: "Row 1",
        onChange: (id) => console.log(`Row 1 selected: ${id}`)
      },
      {
        id: "2",
        label: "Row 2",
        onChange: (id) => console.log(`Row 2 selected: ${id}`)
      }
    ]}
  />
);
```

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `IMatrixColumn` {#IMatrixColumn}
Describes the interface for a matrix column.
#### Type declaration
```ts
interface IMatrixColumn {
  /* Identifier for the column. */
  id: string;

  /* Name for the column. */
  name: string;

  /* Label for the column. */
  label?: string | JSX.Element;

  /* Value for the column. */
  value?: string;
}
```
### 🔗 `IMatrixRow` {#IMatrixRow}
Describes the interface for a matrix row.
#### Type declaration
```ts
interface IMatrixRow {
  /* Identifier for the row. */
  id: string;

  /* Label for the row. */
  label?: string | JSX.Element;

  /* Alias for the row. */
  alias?: string;

  /* Explanation for the row. */
  explanation?: string | JSX.Element;

  /* Specifies if the row is required. */
  required?: boolean;

  /* Tabindex for the row. */
  tabIndex?: number;

  /* Value for the row. */
  value?: string | Value;

  /* Specifies the onChange function for the row. */
  onChange?: (id: string) => void;
}
```
