---
title: DropdownFabric component - Runner Fabric
sidebar_label: DropdownFabric
description: Component for rendering a dropdown list control.
---

# ![](/img/logo-react.svg) DropdownFabric component
Component for rendering a dropdown list control.
### 📝 Signature {#signature}
```ts
DropdownFabric(props: {
  styles: {
    backgroundColor: string;
    borderColor: string;
    borderSize?: number;
    roundness?: number;
    textColor?: string;
    errorColor: string;
    scale?: number;
  };
  options: IDropdownFabricOption[];
  id?: string;
  placeholder?: string;
  required?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  error?: boolean;
  tabIndex?: number;
  value?: string | Value;
  ariaDescribedBy?: string;
  onChange?: (id: string) => void;
  onFocus?: (e: FocusEvent) => void;
  onBlur?: (e: FocusEvent) => void;
  onAutoFocus?: (el: HTMLSelectElement | null) => void;
  onSubmit?: () => void;
  onCancel?: () => void;
}): JSX.Element
```
### 📇 Props {#props}
| Name              | Type                                                    | Optional | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
|:------------------|:--------------------------------------------------------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `styles`          | object                                                  | No       | Specifies the styles for the dropdown component. Supports the following styles:<br/>- `backgroundColor`: Background color for the component;<br/>- `borderColor`: Border color for the component;<br/>- `borderSize`: Optional border size in pixels for the component;<br/>- `roundness`: Optional roundness in pixels for the component;<br/>- `textColor`: Optional text color for the component;<br/>- `errorColor`: Error color for the component;<br/>- `scale`: Optional scale factor for the component (defaults to `1`). |
| `options`         | [`IDropdownFabricOption[]`](#IDropdownFabricOption)     | No       | Specifies the options for the dropdown (see [`IDropdownFabricOption`](#IDropdownFabricOption)).                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| `id`              | string                                                  | Yes      | Optional identifier for the component.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| `placeholder`     | string                                                  | Yes      | Optional placeholder that is shown when there is no option selected.                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `required`        | boolean                                                 | Yes      | Specifies if the component is required.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| `disabled`        | boolean                                                 | Yes      | Specifies if the component is disabled.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| `readOnly`        | boolean                                                 | Yes      | Specifies if the component is read-only.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| `error`           | boolean                                                 | Yes      | Specifies if the component has an error.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| `tabIndex`        | number                                                  | Yes      | Specifies the [`tabindex`](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex) for the component.                                                                                                                                                                                                                                                                                                                                                                                                                |
| `value`           | string \| [`Value`](../../library/classes/Value.mdx) | Yes      | Specifies the value for the component. When a string is supplied, this should be the identifier of the initial dropdown option that should be selected. When a [`Value`](../../library/classes/Value.mdx) instance is supplied, the initial option is selected according to the current value in that instance. When the selected option is changed, the value in the `Value` instance is automatically updated.                                                                                                                        |
| `ariaDescribedBy` | string                                                  | Yes      | Specifies the [aria-describedby](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-describedby) identifier of the element that describes the component.                                                                                                                                                                                                                                                                                                                                                      |
| `onChange`        | (id: string) => void                                    | Yes      | Invoked when the selected option is changed.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| `onFocus`         | (e: FocusEvent) => void                                 | Yes      | Invoked when the component gets focus.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| `onBlur`          | (e: FocusEvent) => void                                 | Yes      | Invoked when the component loses focus.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| `onAutoFocus`     | (el: HTMLSelectElement \| null) => void                 | Yes      | Invoked when the HTML element of the component is available for auto-focus.                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| `onSubmit`        | () => void                                              | Yes      | Invoked when the user presses the `Shift` + `Enter` key combination or the `Tab` key (this event is often used to go to the next input control in a form).                                                                                                                                                                                                                                                                                                                                                                                 |
| `onCancel`        | () => void                                              | Yes      | Invoked when the user presses the `Shift` + `Tab` key combination (this event is often used to go to the previous input control in a form).                                                                                                                                                                                                                                                                                                                                                                                                |
### ↩️ Return value {#return-value}
Returns the `JSX.Element` for the component.
### 👩‍💻 Example {#example}
```ts showLineNumbers
import { DropdownFabric } from "@tripetto/runner-fabric/components/dropdown";

const DropdownExample = () => (
  <DropdownFabric
    styles={{
      backgroundColor: "white",
      borderColor: "black",
      errorColor: "red"
    }}
    options={[
      {
        id: "1",
        name: "Option 1",
      },
      {
        id: "2",
        name: "Option 2",
      }
    ]}
    onChange={(id) => console.log(`Selected: ${id}`)}
  />
);
```

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `IDropdownFabricOption` {#IDropdownFabricOption}
Describes the interface for a dropdown option.
#### Type declaration
```ts
interface IDropdownFabricOption {
  /* Identifier for the option. */
  id: string;

  /* Name for the option. */
  name: string;

  /* Value for the option. */
  value?: string;
}
```
