---
title: YesNoFabric component - Runner Fabric
sidebar_label: YesNoFabric
description: Component for rendering a yes/no button control.
---

# ![](/img/logo-react.svg) YesNoFabric component
Component for rendering a yes/no button control.
### 📝 Signature {#signature}
```ts
YesNoFabric(props: {
  styles: {
    yesColor: string;
    noColor: string;
    outlineSize?: number;
    roundness?: number;
    scale?: number;
    margin?: number;
    alignment?: "horizontal" | "vertical";
  };
  yes: IYesNo;
  no: IYesNo;
  required?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  ariaDescribedBy?: string;
  tabIndex?: number;
  value?: "" | "yes" | "no" | Value;
  autoSubmit?: boolean;
  onChange?: (value: "" | "yes" | "no") => void;
  onFocus?: (e: FocusEvent) => void;
  onBlur?: (e: FocusEvent) => void;
  onAutoFocus?: (el: HTMLButtonElement | null) => void;
  onSubmit?: () => void;
  onCancel?: () => void;
}): JSX.Element
```
### 📇 Props {#props}
| Name              | Type                                                                 | Optional | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
|:------------------|:---------------------------------------------------------------------|:---------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `styles`          | object                                                               | No       | Specifies the styles for the yes/no component. Supports the following styles:<br/>- `yesColor`: Base color for the yes button;<br/>- `noColor`: Base color for the no button;<br/>- `outlineSize`: Optional outline size in pixels for the buttons;<br/>- `roundness`: Optional roundness in pixels for the buttons;<br/>- `scale`: Optional scale factor for the buttons (defaults to 1);<br/>- `margin`: Optional margin in pixels in between the buttons;<br/>- `alignment`: Optional alignment for the buttons (defaults to `horizontal`). |
| `yes`             | [`IYesNo`](#IYesNo)                                                  | No       | Specifies the yes button (see [`IYesNo`](#IYesNo))                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| `no`              | [`IYesNo`](#IYesNo)                                                  | No       | Specifies the no button (see [`IYesNo`](#IYesNo))                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| `required`        | boolean                                                              | Yes      | Specifies if the component is required.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| `disabled`        | boolean                                                              | Yes      | Specifies if the component is disabled.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| `readOnly`        | boolean                                                              | Yes      | Specifies if the component is read-only.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `ariaDescribedBy` | string                                                               | Yes      | Specifies the [aria-describedby](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-describedby) identifier of the element that describes the component.                                                                                                                                                                                                                                                                                                                                                          |
| `tabIndex`        | number                                                               | Yes      | Specifies the [`tabindex`](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex) for the component.                                                                                                                                                                                                                                                                                                                                                                                                                    |
| `value`           | "" \| "yes" \| "no" \| [`Value`](../../library/classes/Value.mdx) | Yes      | Specifies the value for the component. When `""`, `yes` or `no` is specified, this is the initial value for the input control. When a [`Value`](../../library/classes/Value.mdx) is supplied, this initial value is retrieved from the Value instance. Changes made in the input control are automatically reflected to the `Value` instance.                                                                                                                                                                                                 |
| `autoSubmit`      | boolean                                                              | Yes      | Specifies if the component gets submitted when the user selects a button.                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| `view`            | "live" \| "test" \| "preview"                                        | Yes      | Specifies the view in which the component is shown.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| `onChange`        | (value: "" \| "yes" \| "no") => void                                 | Yes      | Invoked when the value of the component is changed.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| `onFocus`         | (e: FocusEvent) => void                                              | Yes      | Invoked when the component gets focus.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| `onBlur`          | (e: FocusEvent) => void                                              | Yes      | Invoked when the component loses focus.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| `onAutoFocus`     | (el: HTMLButtonElement \| null) => void                              | Yes      | Invoked when the HTML element of the component is available for auto-focus.                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| `onSubmit`        | () => void                                                           | Yes      | Invoked when the user presses the `Shift` + `Enter` key combination or the `Tab` key (this event is often used to go to the next input control in a form).                                                                                                                                                                                                                                                                                                                                                                                     |
| `onCancel`        | () => void                                                           | Yes      | Invoked when the user presses the `Shift` + `Tab` key combination (this event is often used to go to the previous input control in a form).                                                                                                                                                                                                                                                                                                                                                                                                    |
### ↩️ Return value {#return-value}
Returns the `JSX.Element` for the component.
### 👩‍💻 Example {#example}
```ts showLineNumbers
import { YesNoFabric } from "@tripetto/runner-fabric/components/yes-no";

const YesNoFabricExample = () => (
  <YesNoFabric
    styles={{
      yesColor: "green",
      noColor: "red"
    }}
    yes={{
      label: "Agree"
    }}
    no={{
      label: "Disagree"
    }}
    onChange={(value) => console.log(`Selected button: ${value}`)}
  />
);
```
## ⛓️ Interfaces {#interfaces}

---
### 🔗 `IYesNo` {#IYesNo}
Describes the interface for a yes/no button.
#### Type declaration
```ts
interface IYesNo {
  /* Label for the button. */
  name: string;

  /* Icon for the button. */
  icon: "yes" | "no";
}
