---
title: IChatController interface - Chat Runner
sidebar_label: IChatController
description: Interface for controlling the chat runner. This interface is used when using the controller prop of the ChatRunner component.
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# IChatController interface

## 📖 Description {#description}
Interface for controlling the runner. This interface is used when using the controller prop of the [ChatRunner](../components/react.mdx) component.

## 📃 Type declaration {#type-declaration}
<InterfaceDeclaration prefix="#" src={`interface IChatController {
  definition: IDefinition;
  styles: IChatStyles;
  l10n: IL10n;
  view: "live" | "test" | "preview";
  readonly instance: Instance | undefined;
  readonly fingerprint: string;
  readonly snapshot: ISnapshot | undefined;
  readonly isRunning: boolean;
  readonly isFinishing: boolean;
  readonly isPausing: boolean;
  readonly allowStart: boolean;
  readonly allowRestart: boolean;
  readonly allowPause: boolean;
  readonly allowStop: boolean;
  readonly start: () => void;
  readonly restart: () => void;
  readonly pause: () => ISnapshot | Promise<ISnapshot> | undefined;
  readonly stop: () => void;
  readonly doPreview: (data: TRunnerPreviewData) => void;
  readonly openChat: () => void;
  readonly closeChat: () => void;
}`} symbols={{
  "Promise": "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise",
  "IDefinition": "/runner/api/library/interfaces/IDefinition/",
  "ISnapshot": "/runner/api/library/interfaces/ISnapshot/",
  "Instance": "/runner/api/library/classes/Instance/",
  "IChatStyles": "/runner/api/stock/chat/interfaces/IChatStyles/",
  "IL10n": "/runner/api/library/interfaces/IL10n/",
  "TRunnerPreviewData": "https://gitlab.com/tripetto/runner-react-hook/-/blob/master/src/lib/preview.ts#L23"
}} />

## 🗃️ Properties {#properties}

---
### 🏷️ `allowPause` {#allowPause}
Retrieves if the runner allows pausing the form.
#### Type
boolean

---
### 🏷️ `allowRestart` {#allowRestart}
Retrieves if the runner allows restarting the form.
#### Type
boolean

---
### 🏷️ `allowStart` {#allowStart}
Retrieves if the runner allows starting the form.
#### Type
boolean

---
### 🏷️ `allowStop` {#allowStop}
Retrieves if the runner allows stopping the form.
#### Type
boolean

---
### 🏷️ `definition` {#definition}
Sets or retrieves the current form definition of the runner.
#### Type
[`IDefinition`](../../../library/interfaces/IDefinition.mdx)

---
### 🏷️ `fingerprint` {#fingerprint}
Retrieves the unique fingerprint of the current form definition. This is a SHA-256 hash of the form that is stable as long as the structure of the form is not changed. Textual changes in the content of the form definition will not alter the fingerprint hash. But adding, removing or reordering elements does.
#### Type
string

:::tip
Read the [Form fingerprint guide](../../../../../builder/integrate/guides/fingerprint.mdx) for more information about the form fingerprint.
:::

---
### 🏷️ `instance` {#instance}
Reference to the active form instance.
#### Type
[`Instance`](../../../library/classes/Instance.mdx) | undefined

:::warning
This property is `undefined` when no form has started yet.
:::

---
### 🏷️ `isFinishing` {#isFinishing}
Retrieves if an active form instance is currently finishing. This property is `true` when a respondent completes the form and the runner is submitting the response data to an endpoint.
#### Type
boolean

---
### 🏷️ `isPausing` {#isPausing}
Retrieves if an active form instance is currently pausing. This property is `true` when a respondent requests to pause the form and the runner is submitting the snapshot data to an endpoint.
#### Type
boolean

---
### 🏷️ `isRunning` {#isRunning}
Retrieves if an active form instance is running.
#### Type
boolean

---
### 🏷️ `l10n` {#l10n}
Sets or retrieves the current locale and translation settings.
#### Type
[`IL10n`](../../../library/interfaces/IL10n.mdx)

:::tip
Read the [Localization guide](../../../../stock/guides/l10n.mdx#l10n) for more information about locales and translations.
:::

---
### 🏷️ `snapshot` {#snapshot}
Retrieves a snapshot of the active form instance. A snapshot can be used to restore/resume a form later on.
#### Type
[`ISnapshot`](../../../library/interfaces/ISnapshot.mdx)

:::tip
Read the [Pause and resume guide](../../../../stock/guides/pause-resume.mdx#snapshot) for more information about pausing and resuming runners.
:::

---
### 🏷️ `styles` {#styles}
Sets or retrieves the current styles (colors, font, size, etc.) of the runner.
#### Type
[`IChatStyles`](IChatStyles.mdx)

:::tip
Read the [Style forms guide](../../../../stock/guides/styles.mdx) for more information about the runner styles.
:::

---
### 🏷️ `view` {#view}
Sets or retrieves the current view mode of the runner. It can be one of the following values:
- `live`: The form is running in normal (production) mode;
- `test`: The form is running in test mode (no response data is submitted);
- `preview`: The form is running in preview mode, which shows all blocks in a single view.
#### Type
"live" | "test" | "preview"

## ▶️ Functions {#functions}

---
### 🔧 `closeChat` {#closeChat}
Closes the chat window if its opened.
#### Signature
```ts
() => void
```

:::warning
This function is only available when the [`display`](IChatStyles.mdx#display) mode is set to `button`
:::

---
### 🔧 `doPreview` {#doPreview}
Request a preview of the specified element of the form. This function is used in a live preview setup to bring a certain element in the form into view.
#### Signature
```ts
(data: TRunnerPreviewData) => void
```
#### Parameters
| Name   | Type                                                                                                       | Optional | Description                 |
|:-------|:-----------------------------------------------------------------------------------------------------------|:---------|:----------------------------|
| `data` | [`TRunnerPreviewData`](https://gitlab.com/tripetto/runner-react-hook/-/blob/master/src/lib/preview.ts#L23) | No       | Specifies the preview data. |

:::tip
Read the [Live form preview guide](../../../../../builder/integrate/guides/livepreview.mdx) for an example.
:::

---
### 🔧 `openChat` {#openChat}
Opens the chat window. This simulates a click on the chat button in the bottom right corner of the screen and allows the chat window to open programmatically.
#### Signature
```ts
() => void
```

:::warning
This function is only available when the [`display`](IChatStyles.mdx#display) mode is set to `button`
:::

---
### 🔧 `pause` {#pause}
Pauses the active form instance.
#### Signature
```ts
() => ISnapshot | Promise<ISnapshot> | undefined
```
#### Return value
Returns the [`ISnapshot`](../../../library/interfaces/ISnapshot.mdx) object, a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) for that object or `undefined` if the pause operation failed.

---
### 🔧 `restart` {#restart}
Restart with a new empty form instance.
#### Signature
```ts
() => void
```

---
### 🔧 `start` {#start}
Starts a new form instance.
#### Signature
```ts
() => void
```

---
### 🔧 `stop` {#stop}
Stops the active form instance.
#### Signature
```ts
() => void
```
