---
title: IChatStyles interface - Chat Runner
sidebar_label: IChatStyles
description: Interface that specifies the styles for the chat runner. Supply it to the styles property of the runner.
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# IChatStyles interface

## 📖 Description {#description}
Interface that specifies the styles for the runner. Supply it to the [`styles`](IChat.mdx#styles) property of the runner.

## 📃 Type declaration {#type-declaration}
<InterfaceDeclaration prefix="#" src={`interface IChatStyles {
  display?: "inline" | "button" | "page";
  color?: string;
  autoOpen?: boolean;
  autoFocus?: boolean;
  showScrollbar?: boolean;
  fullWidth?: boolean;
  noBranding?: boolean;
  font?: {
    family?: string;
    size?: number;
    sizeSmall?: number;
  };
  background?: {
    color?: string;
    url?: string;
    opacity?: number;
    positioning?: "auto" | "100% auto" | "auto 100%" | "cover" | "contain" | "repeat";
  };
  avatar?: {
    type?: "avataaar" | "image";
    image?: string;
    skin?: "Black" | "Brown" | "DarkBrown" | "Light" | "Pale" | "Tanned" | "Yellow";
    eyes?: "Close" | "Cry" | "Dizzy" | "EyeRoll" | "Happy" | "Hearts" | "Neutral" | "Side" | "Squint" | "Surprised" | "Wink" | "WinkWacky";
    eyeBrow?: "Angry" | "AngryNatural" | "FlatNatural" | "Neutral" | "NeutralNatural" | "RaisedExcited" | "RaisedExcitedNatural" | "SadConcerned" | "SadConcernedNatural" | "UnibrowNatural" | "UpDown" | "UpDownNatural";
    mouth?: "Concerned" | "Disbelief" | "Eating" | "Grimace" | "Neutral" | "Sad" | "ScreamOpen" | "Serious" | "Smile" | "Tongue" | "Twinkle" | "Vomit";
    top?: "NoHair" | "Eyepatch" | "Hat" | "Hijab" | "Turban" | "WinterHat1" | "WinterHat2" | "WinterHat3" | "WinterHat4" | "LongHairBigHair" | "LongHairBob" | "LongHairBun" | "LongHairCurly" | "LongHairCurvy" | "LongHairDreads" | "LongHairFrida" | "LongHairFro" | "LongHairFroBand" | "LongHairNotTooLong" | "LongHairShavedSides" | "LongHairMiaWallace" | "LongHairStraight" | "LongHairStraight2" | "LongHairStraightStrand" | "ShortHairDreads01" | "ShortHairDreads02" | "ShortHairFrizzle" | "ShortHairShaggyMullet" | "ShortHairShortCurly" | "ShortHairShortFlat" | "ShortHairShortRound" | "ShortHairShortWaved" | "ShortHairSides" | "ShortHairTheCaesar" | "ShortHairTheCaesarSidePart";
    hairColor?: "Auburn" | "Black" | "Blonde" | "BlondeGolden" | "Brown" | "BrownDark" | "PastelPink" | "Platinum" | "Red" | "SilverGray";
    hatColor?: "Black" | "Blue01" | "Blue02" | "Blue03" | "Gray01" | "Gray02" | "Heather" | "PastelBlue" | "PastelGreen" | "PastelOrange" | "PastelRed" | "PastelYellow" | "Pink" | "Red" | "White";
    facialHair?: "None" | "BeardMedium" | "BeardLight" | "BeardMagestic" | "MoustacheFancy" | "MoustacheMagnum";
    facialHairColor?: "Auburn" | "Black" | "Blonde" | "BlondeGolden" | "Brown" | "BrownDark" | "Platinum" | "Red";
    clothing?: "BlazerShirt" | "BlazerSweater" | "CollarSweater" | "GraphicShirt" | "Hoodie" | "Overall" | "ShirtCrewNeck" | "ShirtScoopNeck" | "ShirtVNeck";
    clothingFabric?: "Black" | "Blue01" | "Blue02" | "Blue03" | "Gray01" | "Gray02" | "Heather" | "PastelBlue" | "PastelGreen" | "PastelOrange" | "PastelRed" | "PastelYellow" | "Pink" | "Red" | "White";
    clothingGraphic?: "Bat" | "Bear" | "Cumbia" | "Deer" | "Diamond" | "Hola" | "Pizza" | "Resist" | "Selena" | "Skull" | "SkullOutline";
    accessories?: "None" | "Kurt" | "Prescription01" | "Prescription02" | "Round" | "Sunglasses" | "Wayfarers";
  };
  questions?: {
    alignment?: "left" | "right";
    roundness?: number;
    backgroundColor?: string;
    borderColor?: string;
    textColor?: string;
  };
  answers?: {
    alignment?: "left" | "right";
    roundness?: number;
    backgroundColor?: string;
    borderColor?: string;
    textColor?: string;
    avatarImage?: string;
  };
  chatButton?: {
    color?: string;
    image?: string;
  };
  inputs?: {
    backgroundColor?: string;
    borderColor?: string;
    borderSize?: number;
    roundness?: number;
    textColor?: string;
    errorColor?: string;
    agreeColor?: string;
    declineColor?: string;
    selectionColor?: string;
  };
  buttons?: {
    baseColor?: string;
    textColor?: string;
    roundness?: number;
    mode?: "fill" | "outline";
  };
}`} />

## 🗃️ Properties {#properties}

---
### 🏷️ `answers` {#answers}
Specifies the styles for the answer bubbles.
#### Type
```ts
{
  /* Bubble alignment. */
  alignment?: "left" | "right";

  /* Bubble roundness in pixels. */
  roundness?: number;

  /* Bubble background color. */
  backgroundColor?: string;

  /* Bubble border color. */
  borderColor?: string;

  /* Bubble text color. */
  textColor?: string;

  /* Answer bubble avatar image. */
  avatarImage?: string;
}
```

---
### 🏷️ `autoFocus` {#autoFocus}
When set to `true`, the runner tries to gain focus as soon as it is displayed.
#### Type
string
:::warning
If the [`display`](IChat.mdx#display) mode of the runner is set to `page` this option is automatically used and therefore always `true`.
:::

---
### 🏷️ `autoOpen` {#autoOpen}
Specifies if the chat should automatically open when using [`display`](#display) mode `button`.
#### Type
boolean

---
### 🏷️ `avatar` {#avatar}
Specifies the question bubble avatar for the runner. Uses [Avataaars](https://avataaars.com/) (designed by [Pablo Stanley](https://twitter.com/pablostanley)) or a custom image.
#### Type
```ts
avatar?: {
  /* Specifies if an Avataaar is used or an image. */
  type?: "avataaar" | "image";

  /* Specifies the URL of the avatar image. */
  image?: string;

  /* Specifies the skin color/ */
  skin?: "Black" | "Brown" | "DarkBrown" | "Light" | "Pale" | "Tanned" | "Yellow";

  /* Specifies the eye color. */
  eyes?: "Close" | "Cry" | "Dizzy" | "EyeRoll" | "Happy" | "Hearts" | "Neutral" | "Side" | "Squint" | "Surprised" | "Wink" | "WinkWacky";

  /* Specifies the eyebrow style. */
  eyeBrow?: "Angry" | "AngryNatural" | "FlatNatural" | "Neutral" | "NeutralNatural" | "RaisedExcited" | "RaisedExcitedNatural" | "SadConcerned" | "SadConcernedNatural" | "UnibrowNatural" | "UpDown" | "UpDownNatural";

  /* Specifies the mouth style. */
  mouth?: "Concerned" | "Disbelief" | "Eating" | "Grimace" | "Neutral" | "Sad" | "ScreamOpen" | "Serious" | "Smile" | "Tongue" | "Twinkle" | "Vomit";

  /* Specifies the top style. */
  top?: "NoHair" | "Eyepatch" | "Hat" | "Hijab" | "Turban" | "WinterHat1" | "WinterHat2" | "WinterHat3" | "WinterHat4" | "LongHairBigHair" | "LongHairBob" | "LongHairBun" | "LongHairCurly" | "LongHairCurvy" | "LongHairDreads" | "LongHairFrida" | "LongHairFro" | "LongHairFroBand" | "LongHairNotTooLong" | "LongHairShavedSides" | "LongHairMiaWallace" | "LongHairStraight" | "LongHairStraight2" | "LongHairStraightStrand" | "ShortHairDreads01" | "ShortHairDreads02" | "ShortHairFrizzle" | "ShortHairShaggyMullet" | "ShortHairShortCurly" | "ShortHairShortFlat" | "ShortHairShortRound" | "ShortHairShortWaved" | "ShortHairSides" | "ShortHairTheCaesar" | "ShortHairTheCaesarSidePart";

  /* Specifies the hair color. */
  hairColor?: "Auburn" | "Black" | "Blonde" | "BlondeGolden" | "Brown" | "BrownDark" | "PastelPink" | "Platinum" | "Red" | "SilverGray";

  /* Specifies the hat color. */
  hatColor?: "Black" | "Blue01" | "Blue02" | "Blue03" | "Gray01" | "Gray02" | "Heather" | "PastelBlue" | "PastelGreen" | "PastelOrange" | "PastelRed" | "PastelYellow" | "Pink" | "Red" | "White";

  /* Specifies the facial hair style. */
  facialHair?: "None" | "BeardMedium" | "BeardLight" | "BeardMagestic" | "MoustacheFancy" | "MoustacheMagnum";

  /* Specifies the facial hair color. */
  facialHairColor?: "Auburn" | "Black" | "Blonde" | "BlondeGolden" | "Brown" | "BrownDark" | "Platinum" | "Red";

  /* Specifies the clothing. */
  clothing?: "BlazerShirt" | "BlazerSweater" | "CollarSweater" | "GraphicShirt" | "Hoodie" | "Overall" | "ShirtCrewNeck" | "ShirtScoopNeck" | "ShirtVNeck";

  /* Specifies the clothing fabric. */
  clothingFabric?: "Black" | "Blue01" | "Blue02" | "Blue03" | "Gray01" | "Gray02" | "Heather" | "PastelBlue" | "PastelGreen" | "PastelOrange" | "PastelRed" | "PastelYellow" | "Pink" | "Red" | "White";

  /* Specifies the clothing graphic. */
  clothingGraphic?: "Bat" | "Bear" | "Cumbia" | "Deer" | "Diamond" | "Hola" | "Pizza" | "Resist" | "Selena" | "Skull" | "SkullOutline";

  /* Specifies the accessories. */
  accessories?: "None" | "Kurt" | "Prescription01" | "Prescription02" | "Round" | "Sunglasses" | "Wayfarers";
}
```
:::tip
You can use the [Avataaars generator](https://getavataaars.com/) by [Fang-Pen Lin](https://fangpenlin.com/) to create an avatar.
:::

---
### 🏷️ `background` {#background}
Specifies the background settings for the runner.
#### Type
```ts
{
  /* Background color. */
  color?: string;

  /* Background image. */
  url?: string;

  /* Background opacity (0..100%). */
  opacity?: number;

  /* Background positioning. */
  positioning?: "auto" | "100% auto" | "auto 100%" | "cover" | "contain" | "repeat";
}
```

---
### 🏷️ `buttons` {#buttons}
Specifies the styles for the buttons.
#### Type
```ts
{
  /* Button base color. */
  baseColor?: string;

  /* Button text color. */
  textColor?: string;

  /* Button roundness in pixels. */
  roundness?: number;

  /* Button mode. */
  mode?: "fill" | "outline";
}
```

---
### 🏷️ `chatbutton` {#chatbutton}
Specifies the styles for the chat button when using [`display`](#display) mode `button`.
#### Type
```ts
{
  /* Button color. */
  color?: string;

  /* Button image. */
  image?: string;
}
```

---
### 🏷️ `color` {#color}
Specifies the base color for the runner.
#### Type
string

---
### 🏷️ `display` {#display}
Specifies the display mode. It can be one of the following values:
- `inline`: Chat content shown inline with other page content (this is the default mode);
- `button`: Chat window is behind a chat button (displayed in the bottom right corner of the page) and shown when the button is clicked;
- `page`: Chat uses the whole page.

#### Type
"inline" | "button" | "page"

:::warning
This property is only used when the [`display`](IChat.mdx#display) property in the [`run`](../functions/run.mdx) function is omitted or undefined.
:::

---
### 🏷️ `font` {#font}
Specifies the font to use. The font family can be a regular font name or a font from [Google Fonts](https://fonts.google.com/). You can also use a custom font by supplying an URL to a custom font stylesheet appended with a hash-sign `#`, followed by the name of the font (for example, `https://yourdomain.com/custom-font.css#fontname`). See the [Using fonts guide](../../../../stock/guides/fonts.mdx) for more information.
#### Type
```ts
{
  /* Font family to use. */
  family?: string;

  /* Font size in pixels for regular devices (default is 16). */ z
  size?: number;

  /* Font size in pixels for small devices (default is 14). */
  sizeSmall?: number;
}
```

:::tip
Read the [Using fonts guide](../../../../stock/guides/fonts.mdx) for more information on changing the font.
:::

---
### 🏷️ `fullWidth` {#fullWidth}
Specifies if runner should use the full available width (default is `false`). When set to `false` a predefined maximum width is used.
#### Type
boolean

---
### 🏷️ `inputs` {#inputs}
Specifies the styles for the input elements of the form.
#### Type
```ts
{
  /* Input field background color. */
  backgroundColor?: string;

  /* Input field border color. */
  borderColor?: string;

  /* Input field border size in pixels. */
  borderSize?: number;

  /* Input field roundness in pixels. */
  roundness?: number;

  /* Input field text color. */
  textColor?: string;

  /* Input field error color. */
  errorColor?: string;

  /* Input field color for agree buttons. */
  agreeColor?: string;

  /* Input field color for decline buttons. */
  declineColor?: string;

  /* Input field text selection color. */
  selectionColor?: string;
}
```

---
### 🏷️ `noBranding` {#noBranding}
Specifies if the Tripetto branding (a link to the Tripetto website) should be invisible (default is `false`).
#### Type
boolean

:::warning License required!
You are only allowed to remove the Tripetto branding if you have bought a license. See the [pricing](../../../../pricing.md) page for more information about licensing and pricing. Read the [Disable Tripetto branding guide](../../../../stock/guides/branding.mdx) for more information about this feature.
:::

---
### 🏷️ `questions` {#questions}
Specifies the styles for the question bubbles.
#### Type
```ts
{
  /* Bubble alignment. */
  alignment?: "left" | "right";

  /* Bubble roundness in pixels. */
  roundness?: number;

  /* Bubble background color. */
  backgroundColor?: string;

  /* Bubble border color. */
  borderColor?: string;

  /* Bubble text color. */
  textColor?: string;
}
```

---
### 🏷️ `showScrollbar` {#showScrollbar}
Specifies if a visual scrollbar should be shown (default is `false`).
#### Type
boolean
