---
title: Stock runners API
description: This reference covers the stock runner packages. Stock runners are full-features runners built and maintained by the Tripetto team.
---

# ![](/img/logo-ts.svg) Stock runners API reference
This reference covers the stock runner packages. Stock runners are full-features runners built and maintained by the Tripetto team. Each stock runner has its own appearance and user experience.

▶️ [Autoscroll runner API reference](autoscroll/index.md)

▶️ [Chat runner API reference](chat/index.md)

▶️ [Classic runner API reference](classic/index.md)

▶️ [Classic Fluent UI runner API reference](classic-fluentui/index.md)
