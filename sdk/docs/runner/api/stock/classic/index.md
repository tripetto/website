---
title: Classic runner API
description: This reference covers the Tripetto Classic Stock Runner package.
---

# ![](/img/logo-ts.svg) Classic runner API reference
This reference covers the [Tripetto Classic Stock Runner](https://www.npmjs.com/package/@tripetto/runner-classic) package. See the [Classic runner face](../../../stock/faces/classic.mdx) page for more information about how this runner works and operates.

:::info
This package does not only contain the Runner library, but also:
- [Locale](../../../stock/guides/l10n.mdx#locale) data for the runner;
- [Translations](../../../stock/guides/l10n.mdx#translations) for the runner;
- [Builder bundle](../../../../builder/integrate/guides/blocks.mdx#import-block-bundles) with the builder part of all the blocks used in the runner;
- [Translations](../../../../builder/integrate/guides/l10n.mdx#loading-multiple-translations) for the builder part of the blocks.

See the [Package](#package) section for more information.
:::

## 📺 Preview {#preview}
![Classic runner preview](/img/runner-classic.jpg)

## 📽️ Demo {#demo}
This demo shows the builder and classic runner side by side. You can create a form in the builder and see a live preview of the form in the runner. The collected form data is outputted to the browser console (no data is sent).

[![Play](/img/button-play.svg)](https://tripetto.gitlab.io/runners/classic/)

## 🚀 Quickstart {#quickstart}
[![Implement stock runner using plain JS](/img/button-js.svg)](../../../stock/quickstart/plain-js/)
[![Implement stock runner using React](/img/button-react.svg)](../../../stock/quickstart/react/)
[![Implement stock runner using Angular](/img/button-angular.svg)](../../../stock/quickstart/angular/)
[![Implement stock runner using HTML](/img/button-html.svg)](../../../stock/quickstart/html/)

## ✨ Installation [![](/img/logo-npm-icon.svg)](https://www.npmjs.com/package/@tripetto/runner-classic) {#installation}

```bash npm2yarn
npm install @tripetto/runner-classic
```
:::caution
This package needs the packages [@tripetto/runner](https://www.npmjs.com/package/@tripetto/runner) and [react](https://www.npmjs.com/package/react) to run. You have to install those peer dependencies yourself.
:::

:::tip Typescript support
This package contains type declarations and supports [TypeScript](https://www.typescriptlang.org/) out-of-the-box.
:::

## ⚛️ Components {#components}
- [`Angular`](components/angular.mdx)
- [`React`](components/react.mdx)

## ▶️ Functions {#functions}
- [`run`](functions/run.mdx)

## 🗂️ Modules {#modules}
- [`TripettoClassic`](modules/TripettoClassic.mdx)

## ⛓️ Interfaces {#interfaces}
- [`IClassic`](interfaces/IClassic.mdx)
- [`IClassicController`](interfaces/IClassicController.mdx)
- [`IClassicProps`](interfaces/IClassicProps.mdx)
- [`IClassicRendering`](interfaces/IClassicRendering.mdx)
- [`IClassicRenderProps`](interfaces/IClassicRenderProps.mdx)
- [`IClassicRunner`](interfaces/IClassicRunner.mdx)
- [`IClassicStyles`](interfaces/IClassicStyles.mdx)

## 🗿 Constants {#constants}
- [`namespace`](constants/namespace.mdx)

## 📦 Package {#package}
The classic runner package contains resources for both the runner and the builder part of Tripetto. Below is a quick reference of all the resources.

:::tip
Read the [Package overview guide](../../../stock/guides/package.mdx) for more information about the structure of this package.
:::

### 🏃 Runner

#### ESM/ES5 ([learn more](../../../stock/quickstart/index.md))
```ts showLineNumbers
import { run } from "@tripetto/runner-classic";

// The code above imports the ES5 or ESM version based on your project configuration.
// If you want to use the ES5 version, you can do an explicit import:
import { run } from "@tripetto/runner-classic/runner/es5";
```

#### UMD
The UMD version of the Runner library (can run directly in the browser): [`./runner/index.js`](https://unpkg.com/@tripetto/runner-classic).

:::caution
Please be aware that the stock runner needs the [Tripetto Runner library](../../../api/library/index.md) package to run properly. If you use the UMD version of the stock runner, you should therefore also load the UMD version of the Runner library.
:::

#### Locales ([learn more](../../../stock/guides/l10n.mdx#locale))
```ts
import en from "@tripetto/runner-classic/runner/locales/en.json";
```

#### Translations ([learn more](../../../stock/guides/l10n.mdx#translations))
```ts
import runnerTranslations from "@tripetto/runner-classic/runner/translations/nl.json";
```

### 🏗️ Builder

#### Blocks bundle ([learn more](../../../../builder/integrate/guides/blocks.mdx#import-block-bundles))
```ts showLineNumbers
import "@tripetto/runner-classic/builder";

// The code above imports the ES5 or ESM version based on your project configuration.
// If you want to use the ES5 version, you can do an explicit import:
import "@tripetto/runner-classic../builder/es5";
```
#### UMD
The UMD version of the blocks bundle (can run directly in the browser): [`.../builder/index.js`](https://unpkg.com/@tripetto/runner-classic/builder).

#### Translations ([learn more](../../../../builder/integrate/guides/l10n.mdx#loading-multiple-translations))
```ts
import blocksBundleTranslations from "@tripetto/runner-classic../builder/translations/nl.json";
```

#### Styles contract ([learn more](../../../../builder/integrate/guides/livepreview.mdx#styles))
```ts
import stylesContract from "@tripetto/runner-classic../builder/styles";
```

#### L10n contract ([learn more](../../../../builder/integrate/guides/livepreview.mdx#translations))
```ts
import l10nContract from "@tripetto/runner-classic../builder/l10n";
```

## 📁 Source code [![](/img/logo-gitlab.svg)](https://gitlab.com/tripetto/runners/classic) {#source}
The Tripetto Classic Runner package is open-source and the code is on [GitLab](https://gitlab.com/tripetto/runners/classic).
