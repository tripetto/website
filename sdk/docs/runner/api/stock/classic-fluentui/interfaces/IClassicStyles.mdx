---
title: IClassicStyles interface - Classic Fluent UI Runner
sidebar_label: IClassicStyles
description: Interface that specifies the styles for the classic runner (Fluent UI). Supply it to the styles property of the runner.
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# IClassicStyles interface

## 📖 Description {#description}
Interface that specifies the styles for the runner. Supply it to the [`styles`](IClassic.mdx#styles) property of the runner.

## 📃 Type declaration {#type-declaration}
<InterfaceDeclaration prefix="#" src={`interface IClassicStyles {
  mode?: "paginated" | "continuous" | "progressive";
  autoFocus?: boolean;
  showEnumerators?: boolean;
  showPageIndicators?: boolean;
  showProgressbar?: boolean;
  color?: string;
  textColor?: string;
  backgroundColor?: string;
  palette?: {
    themePrimary?: string;
    themeLighterAlt?: string;
    themeLighter?: string;
    themeLight?: string;
    themeTertiary?: string;
    themeSecondary?: string;
    themeDarkAlt?: string;
    themeDark?: string;
    themeDarker?: string;
    neutralLighterAlt?: string;
    neutralLighter?: string;
    neutralLight?: string;
    neutralQuaternaryAlt?: string;
    neutralQuaternary?: string;
    neutralTertiaryAlt?: string;
    neutralTertiary?: string;
    neutralSecondary?: string;
    neutralSecondaryAlt?: string;
    neutralPrimaryAlt?: string;
    neutralPrimary?: string;
    neutralDark?: string;
    black?: string;
    white?: string;
  };
}`} />

## 🗃️ Properties {#properties}

---
### 🏷️ `autoFocus` {#autoFocus}
When set to `true`, the runner tries to gain focus as soon as it is displayed.
#### Type
string
:::warning
If the [`display`](IClassic.mdx#display) mode of the runner is set to `page` this option is automatically used and therefore always `true`.
:::

---
### 🏷️ `backgroundColor` {#backgroundColor}
Specifies the background color for the runner.
#### Type
string

---
### 🏷️ `color` {#color}
Specifies the primary color for the runner.
#### Type
string

---
### 🏷️ `mode` {#mode}
Specifies the operation mode of the runner. It can be one of the following values:
- `progressive`: Presents the whole form at once (this is the default mode);
- `continuous`: Keeps answered blocks in view while working through the form;
- `paginated`: Use pagination for the form and let the user navigate through the form using a next and back button.

#### Type
"progressive" | "continuous" | "paginated"

---
### 🏷️ `noBranding` {#noBranding}
Specifies if the Tripetto branding (a link to the Tripetto website) should be invisible (default is `false`).
#### Type
boolean

:::warning License required!
You are only allowed to remove the Tripetto branding if you have bought a license. See the [pricing](../../../../pricing.md) page for more information about licensing and pricing. Read the [Disable Tripetto branding guide](../../../../stock/guides/branding.mdx) for more information about this feature.
:::

---
### 🏷️ `palette` {#palette}
Specifies the color palette (theme) for the Fluent UI controls. If this property is omitted the runner will generate a color palette from the specified [color](#color), [backgroundColor](#backgroundColor), and [textColor](#textColor).
:::tip
Use the [Fluent UI Theme Designer](https://fluentuipr.z22.web.core.windows.net/heads/master/theming-designer/index.html) to generate a theme for the form.
:::
#### Type
```ts
{
  themePrimary?: string;
  themeLighterAlt?: string;
  themeLighter?: string;
  themeLight?: string;
  themeTertiary?: string;
  themeSecondary?: string;
  themeDarkAlt?: string;
  themeDark?: string;
  themeDarker?: string;
  neutralLighterAlt?: string;
  neutralLighter?: string;
  neutralLight?: string;
  neutralQuaternaryAlt?: string;
  neutralQuaternary?: string;
  neutralTertiaryAlt?: string;
  neutralTertiary?: string;
  neutralSecondary?: string;
  neutralSecondaryAlt?: string;
  neutralPrimaryAlt?: string;
  neutralPrimary?: string;
  neutralDark?: string;
  black?: string;
  white?: string;
}
```

---
### 🏷️ `showEnumerators` {#showEnumerators}
Specifies if enumerators (question numbers) should be shown (default is `false`).
#### Type
boolean

---
### 🏷️ `showPageIndicators` {#showPageIndicators}
Specifies if a page indiciators should be shown when the [`mode`](#mode) is set to `paginated` (default is `false`).
#### Type
boolean

---
### 🏷️ `showProgressbar` {#showProgressbar}
Specifies if a progressbar should be shown when the [`mode`](#mode) is set to `paginated` (default is `false`).
#### Type
boolean

---
### 🏷️ `textColor` {#textColor}
Specifies the text color for the runner.
#### Type
string
