---
title: Angular component - Classic FluentUI Runner
sidebar_label: Angular
description: The Classic FluentUI runner Angular component.
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# ![](/img/logo-angular.svg) Angular component
The runner [Angular](https://angular.io/) component allows to use the runner in Angular applications with ease. To use the component, simply import `TripettoClassicModule` from the package and feed it to your application's `@NgModule` imports array. This makes the `<tripetto-runner-classic-fluentui>` selector available in your application. Below is a list of [inputs](#inputs) and [outputs](#outputs) that can be used on the selector.
:::tip
Read the [Angular implementation guide](../../../../stock/quickstart/angular.mdx) for more information and examples.
:::
:::info
The Angular component is located in a subfolder named `angular` inside the runner package. Make sure to import the module and component from this folder.
:::
#### Module
```ts
import { TripettoClassicModule } from "@tripetto/runner-classic-fluentui/angular";
```
#### Component
```ts
import { TripettoClassicComponent } from "@tripetto/runner-classic-fluentui/angular";
```
#### Selector
```ts
<tripetto-runner-classic-fluentui></tripetto-runner-classic-fluentui>
```

### ⏬ Inputs {#inputs}
| Name             | Type                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     | Optional | Description                                                                                                                                                                                                            |
|:-----------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:---------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `definition`     | [`IDefinition`](../../../library/interfaces/IDefinition.mdx) \| Promise\<[`IDefinition`](../../../library/interfaces/IDefinition.mdx) \| undefined>                                                                                                                                                                                                                                                                                                                                                                                                                      | Yes      | Specifies the definition to run ([more information](../interfaces/IClassic.mdx#definition)).                                                                                                                           |
| `view`           | "live" \| "test" \| "preview"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | Yes      | Specifies the view mode of the runner ([more information](../interfaces/IClassic.mdx#view)).                                                                                                                           |
| `styles`         | [`IClassicStyles`](../interfaces/IClassicStyles.mdx) \| Promise\<[`IClassicStyles`](../interfaces/IClassicStyles.mdx) \| undefined>                                                                                                                                                                                                                                                                                                                                                                                                                                      | Yes      | Specifies the styles (colors, font, size, etc.) for the runner ([more information](../interfaces/IClassic.mdx#styles)).                                                                                                |
| `l10n`           | [`IL10n`](../../../library/interfaces/IL10n.mdx) \| Promise\<[`IL10n`](../../../library/interfaces/IL10n.mdx) \| undefined>                                                                                                                                                                                                                                                                                                                                                                                                                                              | Yes      | Specifies the localization (locale and translation) information ([more information](../interfaces/IClassic.mdx#l10n)).                                                                                                 |
| `display`        | "inline" \| "page"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       | Yes      | Specifies the display mode of the runner ([more information](../interfaces/IClassic.mdx#display)).                                                                                                                     |
| `snapshot`       | [`ISnapshot`](../../../library/interfaces/ISnapshot.mdx) \| Promise\<[`ISnapshot`](../../../library/interfaces/ISnapshot.mdx)>                                                                                                                                                                                                                                                                                                                                                                                                                                           | Yes      | Specifies the snapshot that should be restored ([more information](../interfaces/IClassic.mdx#snapshot)).                                                                                                              |
| `persistent`     | boolean                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | Yes      | Specifies if the runner should try to store sessions in the local store to preserve persistency on navigation between multiple pages that host the runner ([more information](../interfaces/IClassic.mdx#persistent)). |
| `license`        | string \| Promise\<string \| undefined>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | Yes      | Specifies a license code for the runner ([more information](../interfaces/IClassic.mdx#license)).                                                                                                                      |
| `removeBranding` | boolean                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | Yes      | Specifies if all visual Tripetto branding should be removed from the runner (a valid [license](../interfaces/IClassic.mdx#license) is required).                                                                       |
| `attachments`    | [`IRunnerAttachments`](../../../react-hook/interfaces/IRunnerAttachments.mdx)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | Yes      | Specifies the attachments handler used by blocks that support file uploads ([more information](../interfaces/IClassic.mdx#attachments)).                                                                               |
| `className`      | string                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | Yes      | Specifies a custom class name for the HTML element that holds the runner ([more information](../interfaces/IClassic.mdx#className)).                                                                                   |
| `customStyle`    | CSSProperties                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | Yes      | Specifies the inline style for the HTML element that holds the runner ([more information](../interfaces/IClassic.mdx#customStyle)).                                                                                    |
| `customCSS`      | string                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | Yes      | Specifies custom CSS rules for blocks used in the runner ([more information](../interfaces/IClassic.mdx#customCSS)).                                                                                                   |
| `language`       | string                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   | Yes      | Specifies the preferred language to use for the form locale and translation ([more information](../interfaces/IClassic.mdx#language)).                                                                                 |
| `locale`         | [`L10n.ILocale`](../../../library/modules/L10n/index.mdx#ILocale) \| ((locale: string) => [`L10n.ILocale`](../../../library/modules/L10n/index.mdx#ILocale) \| Promise\<[`L10n.ILocale`](../../../library/modules/L10n/index.mdx#ILocale) \| undefined> \| undefined)                                                                                                                                                                                                                                                                                                    | Yes      | Specifies the locale or the locale loader function to use ([more information](../interfaces/IClassic.mdx#locale)).                                                                                                     |
| `translations`   | [`L10n.TTranslation`](../../../library/modules/L10n/index.mdx#TTranslation) \| [`L10n.TTranslation`](../../../library/modules/L10n/index.mdx#TTranslation)[] \| (language: string, name: string, version: string) => [`L10n.TTranslation`](../../../library/modules/L10n/index.mdx#TTranslation) \| [`L10n.TTranslation`](../../../library/modules/L10n/index.mdx#TTranslation)[] \| Promise\<[`L10n.TTranslation`](../../../library/modules/L10n/index.mdx#TTranslation) \| [`L10n.TTranslation`](../../../library/modules/L10n/index.mdx#TTranslation)[] \| undefined> | Yes      | Specifies the translations or the translations loader function to use ([more information](../interfaces/IClassic.mdx#translations)).                                                                                   |
| `builder`        | [`Builder`](../../../../../builder/api/classes/Builder.mdx)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              | Yes      | Specifies the [`Builder`](../../../../../builder/api/classes/Builder.mdx) instance to bind to ([more information](../interfaces/IClassic.mdx#builder)).                                                                |
| `onReload`       | () => [`IDefinition`](../../../library/interfaces/IDefinition.mdx) \| Promise\<[`IDefinition`](../../../library/interfaces/IDefinition.mdx)>                                                                                                                                                                                                                                                                                                                                                                                                                             | Yes      | Specifies a function that is invoked when the runner wants to reload the definition ([more information](../interfaces/IClassic.mdx#onReload)).                                                                         |
| `onPause`        | `{...}`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  | Yes      | Specifies a function or recipe that is invoked when the runner wants to pause ([more information](../interfaces/IClassic.mdx#onPause)).                                                                                |

### 📢 Outputs {#outputs}
| Name         | Type                     | Optional | Description                                                                                                                          |
|:-------------|:-------------------------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------|
| `onReady`    | `EventEmitter<Instance>` | Yes      | Invoked when the runner is ready ([more information](../interfaces/IClassic.mdx#onReady)).                                        |
| `onChange`   | `EventEmitter<Instance>` | Yes      | Invoked when there is a change in the runner ([more information](../interfaces/IClassic.mdx#onChange)).                           |
| `onImport`   | `EventEmitter<Instance>` | Yes      | Invoked when data can be imported into the instance ([more information](../interfaces/IClassic.mdx#onImport)).                    |
| `onData`     | `EventEmitter<Instance>` | Yes      | Invoked when there is a data change ([more information](../interfaces/IClassic.mdx#onData)).                                      |
| `onAction`   | `EventEmitter<{...}>`    | Yes      | Invoked when the user performs an action ([more information](../interfaces/IClassic.mdx#onAction)).                               |
| `onSubmit`   | `EventEmitter<{...}>`    | Yes      | Invoked when the runner submits data ([more information](../interfaces/IClassic.mdx#onSubmit)).                                   |
| `onComplete` | `EventEmitter<{...}>`    | Yes      | Invoked when the runner is completed but after the data is submitted ([more information](../interfaces/IClassic.mdx#onComplete)). |
| `onEdit`     | `EventEmitter<{...}>`    | Yes      | Invoked when an edit action is requested ([more information](../interfaces/IClassic.mdx#onEdit)).                                 |
| `onTouch`    | `EventEmitter<void>`     | Yes      | Invoked when the runner is "touched" by a user ([more information](../interfaces/IClassic.mdx#onTouch)).                          |
| `onDestroy`  | `EventEmitter<void>`     | Yes      | Invoked when the runner is destroyed. ([more information](../interfaces/IClassic.mdx#onDestroy)).                                 |

### 🗃️ Fields {#fields}
| Name         | Type                                                                    | Description                                  |
|:-------------|:------------------------------------------------------------------------|:---------------------------------------------|
| `controller` | [`IClassicRunner`](../interfaces/IClassicRunner.mdx) \| undefined | Contains a reference to the runner instance. |

### 👩‍💻 Example {#example}
<Tabs>
<TabItem value="app-module" label="App module">

```ts showLineNumbers title="app.module.ts"
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
//highlight-start
import { TripettoClassicModule } from "@tripetto/runner-classic-fluentui/angular";
//highlight-end

@NgModule({
  declarations: [AppComponent],
  //highlight-start
  imports: [BrowserModule, TripettoClassicModule],
  //highlight-end
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
```

</TabItem>
<TabItem value="app-html" label="App HTML">

```html showLineNumbers title="app.component.html"
<tripetto-runner-classic-fluentui [definition]="definition"></tripetto-runner-classic-fluentui>
```

</TabItem>
<TabItem value="app-component" label="App component">

```ts showLineNumbers title="app.component.ts"
import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  //highlight-start
  definition = /* Supply your form definition here */;
  //highlight-end
}
```

</TabItem>
</Tabs>

[![Run](/img/button-run.svg)](https://10f9op.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-runner-classic-fluent-ui-angular-basic-implementation-10f9op?file=/src/app/app.component.html)
