---
title: IAutoscrollRenderProps interface - Autoscroll Runner
sidebar_label: IAutoscrollRenderProps
description: Interface that specifies the props for the block renderer in the autoscroll runner. Used when creating custom blocks.
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# IAutoscrollRenderProps interface

## 📖 Description {#interface-description}
Interface that specifies the props for the block renderer. Used when creating [custom blocks](../../../../stock/guides/blocks.mdx).

## 📃 Type declaration {#type-declaration}
<InterfaceDeclaration prefix="#" src={`interface IAutoscrollRenderProps {
  readonly index: number;
  readonly id: string;
  readonly l10n: L10n.Namespace;
  readonly styles: IAutoscrollStyles;
  readonly attachments: IRunnerAttachments | undefined;
  readonly view: "live" | "test" | "preview";
  readonly name: JSX.Element | undefined;
  readonly description: JSX.Element | undefined;
  readonly explanation: JSX.Element | undefined;
  readonly label: JSX.Element | undefined;
  readonly placeholder: string;
  readonly tabIndex: number;
  readonly isActivated: boolean;
  readonly isFailed: boolean;
  readonly isPage: boolean;
  readonly ariaDescribedBy: string | undefined;
  readonly ariaDescription: JSX.Element | undefined;
  readonly focus: (e: FocusEvent) => void;
  readonly blur: (e: FocusEvent) => void;
  readonly autoFocus: (element: HTMLElement | null) => void;
  readonly markdownifyToJSX: (md: string, lineBreaks?: boolean) => JSX.Element;
  readonly markdownifyToURL: (md: string) => string;
  readonly markdownifyToImage: (md: string) => string;
  readonly markdownifyToString: (md: string) => string;
}`} symbols={{
  "HTMLElement": "https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement",
  "FocusEvent": "https://developer.mozilla.org/en-US/docs/Web/API/FocusEvent",
  "JSX.Element": "https://react.dev/learn/writing-markup-with-jsx",
  "IAutoscrollStyles": "/runner/api/stock/autoscroll/interfaces/IAutoscrollStyles/",
  "IRunnerAttachments": "/runner/api/react-hook/interfaces/IRunnerAttachments/",
  "L10n.Namespace": "/runner/api/library/modules/L10n/Namespace/"
}} />

## 🗃️ Properties {#properties}

---
### 🏷️ `ariaDescribedBy` {#ariaDescribedBy}
Contains a [`aria-describedby`](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-describedby) identifier when there is an [`ariaDescription`](#ariaDescription) set for the block.
#### Type
string

---
### 🏷️ `ariaDescription` {#ariaDescription}
Contains an automatic generated [aria description](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/Attributes/aria-describedby) for the elements labeled with the [`ariaDescribedBy`](#ariaDescribedBy) identifier.
#### Type
JSX.Element | undefined

---
### 🏷️ `attachments` {#attachments}
Reference to the attachments handler for processing file uploads.
#### Type
[`IRunnerAttachments`](../../../react-hook/interfaces/IRunnerAttachments.mdx)

:::tip
Have a look at the [Handling file uploads guide](../../../../stock/guides/attachments.mdx) to learn how to use this feature.
:::

---
### 🏷️ `description` {#description}
Contains the description of the block as a [`JSX.Element`](https://react.dev/learn/writing-markup-with-jsx). Any markdown used in the description is already processed. Most blocks implement this property and store the description of the block in it.
#### Type
[`JSX.Element`](https://react.dev/learn/writing-markup-with-jsx) | undefined

---
### 🏷️ `explanation` {#explanation}
Contains the explanation of the block as a [`JSX.Element`](https://react.dev/learn/writing-markup-with-jsx). Any markdown used in the explanation is already processed. Most blocks implement this property and store the explanation of the block in it.
#### Type
[`JSX.Element`](https://react.dev/learn/writing-markup-with-jsx) | undefined

---
### 🏷️ `id` {#id}
Contains a unique id (key) for the block.
#### Type
string

---
### 🏷️ `index` {#index}
Contains the index (or question number) of the block.
#### Type
number

---
### 🏷️ `isActivated` {#isActivated}
Contains if the block is active.
#### Type
boolean

---
### 🏷️ `isFailed` {#isFailed}
Contains if the block validation has failed.
#### Type
boolean

---
### 🏷️ `isPage` {#isPage}
Contains if the runner is in page mode.
#### Type
boolean

---
### 🏷️ `l10n` {#l10n}
Contains a reference to the l10n (translation) namespace. This namespace contains the gettext functions for translating text.
#### Type
[`L10n.Namespace`](../../../library/modules/L10n/Namespace.mdx)
#### Example
```ts showLineNumbers
l10n._("Text to be translated");
l10n._n("1 car", "%1 cars", 10);
```

---
### 🏷️ `label` {#label}
Contains the label of the block as a [`JSX.Element`](https://react.dev/learn/writing-markup-with-jsx). Any markdown used in the label is already processed.
#### Type
[`JSX.Element`](https://react.dev/learn/writing-markup-with-jsx) | undefined

---
### 🏷️ `name` {#name}
Contains the name of the block as a [`JSX.Element`](https://react.dev/learn/writing-markup-with-jsx). Any markdown used in the name is already processed. Most blocks implement this property and store the name or title of the block in it.
#### Type
[`JSX.Element`](https://react.dev/learn/writing-markup-with-jsx) | undefined

---
### 🏷️ `placeholder` {#placeholder}
Contains the placeholder of the block.
#### Type
string

---
### 🏷️ `styles` {#styles}
Reference to the styles of the runner.
#### Type
[`IAutoscrollStyles`](IAutoscrollStyles.mdx)

---
### 🏷️ `tabIndex` {#tabIndex}
Retrieves a [tab index](https://developer.mozilla.org/en-US/docs/Web/HTML/Global_attributes/tabindex) value used for sequential keyboard navigation (usually with the `Tab` key, hence the name). Each time this property is retrieved, it is incremented by 1.
#### Type
number

---
### 🏷️ `view` {#view}
Contains the current view mode of the runner. It can be one of the following values:
- `live`: The form is running in normal (production) mode;
- `test`: The form is running in test mode;
- `preview`: The form is running in preview mode.
#### Type
"live" | "test" | "preview"

## ▶️ Functions {#functions}

---
### 🔧 `autoFocus` {#autoFocus}
This function should be invoked to indicate that an element is able to receive auto focus. A reference to the element should be provided.
#### Signature
```ts
(element: HTMLElement | null) => void
```
#### Parameters
| Name      | Type                                                                                       | Optional | Description                                               |
|:----------|:-------------------------------------------------------------------------------------------|:---------|:----------------------------------------------------------|
| `element` | [`HTMLElement`](https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement) \| undefined | No       | Reference to an element that is able to gain input focus. |

---
### 🔧 `blur` {#blur}
This function should be invoked to indicate that an element in the block lost focus.  This is usually attached to the [`blur`](https://developer.mozilla.org/en-US/docs/Web/API/Element/blur_event) event of input elements used in the block.
#### Signature
```ts
(e: FocusEvent) => void
```
#### Parameters
| Name | Type                                                                        | Optional | Description                       |
|:-----|:----------------------------------------------------------------------------|:---------|:----------------------------------|
| `e`  | [`FocusEvent`](https://developer.mozilla.org/en-US/docs/Web/API/FocusEvent) | No       | Reference to the event interface. |

---
### 🔧 `focus` {#focus}
This function should be invoked to indicate that an element in the block gained focus. This is usually attached to the [`focus`](https://developer.mozilla.org/en-US/docs/Web/API/Element/focus_event) event of input elements used in the block.
#### Signature
```ts
(e: FocusEvent) => void
```
#### Parameters
| Name | Type                                                                        | Optional | Description                       |
|:-----|:----------------------------------------------------------------------------|:---------|:----------------------------------|
| `e`  | [`FocusEvent`](https://developer.mozilla.org/en-US/docs/Web/API/FocusEvent) | No       | Reference to the event interface. |

---
### 🔧 `markdownifyToImage` {#markdownifyToImage}
Parses a markdown string to a valid image URL.
#### Signature
```ts
(md: string) => string
```
#### Parameters
| Name | Type   | Optional | Description                               |
|:-----|:-------|:---------|:------------------------------------------|
| `md` | string | No       | Specifies the markdown string to process. |
#### Return value
Returns the image URL.

---
### 🔧 `markdownifyToJSX` {#markdownifyToJSX}
Parses a markdown string to formatted JSX.
#### Signature
```ts
(md: string, lineBreaks?: boolean) => JSX.Element
```
#### Parameters
| Name         | Type    | Optional | Description                                                        |
|:-------------|:--------|:---------|:-------------------------------------------------------------------|
| `md`         | string  | No       | Specifies the markdown string to process.                          |
| `lineBreaks` | boolean | Yes      | Specifies if line breaks are supported or not (default is `true`). |
#### Return value
Returns the markdown `JSX.Element`.

---
### 🔧 `markdownifyToString` {#markdownifyToString}
Parses a markdown string to a plain string.
#### Signature
```ts
(md: string) => string
```
#### Parameters
| Name | Type   | Optional | Description                               |
|:-----|:-------|:---------|:------------------------------------------|
| `md` | string | No       | Specifies the markdown string to process. |
#### Return value
Returns the parsed string.

---
### 🔧 `markdownifyToURL` {#markdownifyToURL}
Parses a markdown string to a valid URL.
#### Signature
```ts
(md: string) => string
```
#### Parameters
| Name | Type   | Optional | Description                               |
|:-----|:-------|:---------|:------------------------------------------|
| `md` | string | No       | Specifies the markdown string to process. |
#### Return value
Returns the parsed URL.
