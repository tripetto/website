---
title: IAutoscrollStyles interface - Autoscroll Runner
sidebar_label: IAutoscrollStyles
description: Interface that specifies the styles for the autoscroll runner. Supply it to the styles property of the runner.
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# IAutoscrollStyles interface

## 📖 Description {#description}
Interface that specifies the styles for the runner. Supply it to the [`styles`](IAutoscroll.mdx#styles) property of the runner.

## 📃 Type declaration {#type-declaration}
<InterfaceDeclaration prefix="#" src={`interface IAutoscrollStyles {
  direction?: "horizontal" | "vertical";
  verticalAlignment?: "top" | "middle" | "bottom";
  hidePast?: boolean;
  hideUpcoming?: boolean;
  color?: string;
  autoFocus?: boolean;
  showSeparateSubmit?: boolean;
  showPreviousButton?: boolean;
  showNavigation?: "auto" | "always" | "never";
  showProgressbar?: boolean;
  showEnumerators?: boolean;
  hideRequiredIndicator?: boolean;
  showScrollbar?: boolean;
  disableScrolling?: boolean;
  noBranding?: boolean;
  font?: {
    family?: string;
    size?: number;
    sizeSmall?: number;
  };
  background?: {
    color?: string;
    url?: string;
    opacity?: number;
    positioning?: "auto" | "100% auto" | "auto 100%" | "cover" | "contain" | "repeat";
  };
  inputs?: {
    backgroundColor?: string;
    borderColor?: string;
    borderSize?: number;
    roundness?: number;
    textColor?: string;
    errorColor?: string;
    agreeColor?: string;
    declineColor?: string;
    selectionColor?: string;
  };
  buttons?: {
    baseColor?: string;
    textColor?: string;
    roundness?: number;
    mode?: "fill" | "outline";
    finishColor?: string;
  };
  navigation?: {
    backgroundColor?: string;
    textColor?: string;
    progressbarColor?: string;
  };
}`} />

## 🗃️ Properties {#properties}

---
### 🏷️ `autoFocus` {#autoFocus}
When set to `true`, the runner tries to gain focus as soon as it is displayed.
#### Type
string
:::warning
If the [`display`](IAutoscroll.mdx#display) mode of the runner is set to `page` this option is automatically used and therefore always `true`.
:::
---
### 🏷️ `background` {#background}
Specifies the background settings for the runner.
#### Type
```ts
{
  /* Background color. */
  color?: string;

  /* Background image. */
  url?: string;

  /* Background opacity (0..100%). */
  opacity?: number;

  /* Background positioning. */
  positioning?: "auto" | "100% auto" | "auto 100%" | "cover" | "contain" | "repeat";
}
```

---
### 🏷️ `buttons` {#buttons}
Specifies the styles for the buttons.
#### Type
```ts
{
  /* Button base color. */
  baseColor?: string;

  /* Button text color. */
  textColor?: string;

  /* Button roundness in pixels. */
  roundness?: number;

  /* Button mode. */
  mode?: "fill" | "outline";

  /* Finish (or complete) button color. */
  finishColor?: string;
}
```

---
### 🏷️ `color` {#color}
Specifies the base color for the runner.
#### Type
string

---
### 🏷️ `direction` {#direction}
Specifies the scroll direction of the runner (default is `vertical`).
#### Type
"horizontal" | "vertical"

---
### 🏷️ `disableScrolling` {#disableScrolling}
Specifies if scrolling (with the mousewheel or touch input) should be disabled (default is `false`).
#### Type
boolean

---
### 🏷️ `font` {#font}
Specifies the font to use. The font family can be a regular font name or a font from [Google Fonts](https://fonts.google.com/). You can also use a custom font by supplying an URL to a custom font stylesheet appended with a hash-sign `#`, followed by the name of the font (for example, `https://yourdomain.com/custom-font.css#fontname`). See the [Using fonts guide](../../../../stock/guides/fonts.mdx) for more information.
#### Type
```ts
{
  /* Font family to use. */
  family?: string;

  /* Font size in pixels for regular devices (default is 16). */ z
  size?: number;

  /* Font size in pixels for small devices (default is 14). */
  sizeSmall?: number;
}
```

:::tip
Read the [Using fonts guide](../../../../stock/guides/fonts.mdx) for more information on changing the font.
:::

---
### 🏷️ `hidePast` {#hidePast}
When set to `true`, all the blocks before the active block become invisible.
#### Type
boolean

---
### 🏷️ `hideRequiredIndicator` {#hideRequiredIndicator}
Specifies if the required asterisk (*) should be made invisible (default is `false`).
#### Type
boolean

---
### 🏷️ `hideUpcoming` {#hideUpcoming}
When set to `true`, all the blocks after the active block are invisible.
#### Type
boolean

---
### 🏷️ `inputs` {#inputs}
Specifies the styles for the input elements of the form.
#### Type
```ts
{
  /* Input field background color. */
  backgroundColor?: string;

  /* Input field border color. */
  borderColor?: string;

  /* Input field border size in pixels. */
  borderSize?: number;

  /* Input field roundness in pixels. */
  roundness?: number;

  /* Input field text color. */
  textColor?: string;

  /* Input field error color. */
  errorColor?: string;

  /* Input field color for agree buttons. */
  agreeColor?: string;

  /* Input field color for decline buttons. */
  declineColor?: string;

  /* Input field text selection color. */
  selectionColor?: string;
}
```

---
### 🏷️ `navigation` {#navigation}
Specifies the styles for the navigation bar.
#### Type
```ts
{
  /* Background color of the navigation bar. */
  backgroundColor?: string;

  /* Text color of the navigation bar. */
  textColor?: string;

  /* Progressbar color of the navigation bar. */
  progressbarColor?: string;
}
```

---
### 🏷️ `noBranding` {#noBranding}
Specifies if the Tripetto branding (a link to the Tripetto website) should be invisible (default is `false`).
#### Type
boolean

:::warning License required!
You are only allowed to remove the Tripetto branding if you have bought a license. See the [pricing](../../../../pricing.md) page for more information about licensing and pricing. Read the [Disable Tripetto branding guide](../../../../stock/guides/branding.mdx) for more information about this feature.
:::

---
### 🏷️ `showEnumerators` {#showEnumerators}
Specifies if enumerators (question numbers) should be shown (default is `false`).
#### Type
boolean

---
### 🏷️ `showNavigation` {#showNavigation}
Specifies the behavior of the navigation bar. It can be one of the following values:
- `auto`: The navigation bar is only displayed when the [`display`](IAutoscroll.mdx#display) mode is set to `page` (this is the default setting);
- `always`: The navigation bar is always shown;
- `never`: The navigation bar is never shown.
#### Type
"auto" | "always" | "never"

---
### 🏷️ `showPreviousButton` {#showPreviousButton}
Specifies if a back button is shown (default is `true`). When clicked, the respondent returns to the previous block.
#### Type
boolean

---
### 🏷️ `showProgressbar` {#showProgressbar}
Specifies if a progressbar is shown in the navigation bar (default is `true`).
#### Type
boolean

---
### 🏷️ `showScrollbar` {#showScrollbar}
Specifies if a visual scrollbar should be shown (default is `false`).
#### Type
boolean

---
### 🏷️ `showSeparateSubmit` {#showSeparateSubmit}
Specifies if the submit button should be shown as a separate block (that scrolls into view) or that the button is shown in the last active question block (default is `true`).
#### Type
boolean

---
### 🏷️ `verticalAlignment` {#verticalAlignment}
Specifies the vertical alignment of the content in the runner (default is `middle`).
#### Type
"top" | "middle" | "bottom"
