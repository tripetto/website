---
title: IAutoscrollProps interface - Autoscroll Runner
sidebar_label: IAutoscrollProps
description: Properties interface for the AutoscrollRunner React component.
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# IAutoscrollProps interface

## 📖 Description {#description}
Properties interface for the [`AutoscrollRunner`](../components/react.mdx) React component.

## 📃 Type declaration {#type-declaration}
<InterfaceDeclaration prefix="#" src={`interface IAutoscrollProps {
  definition?: IDefinition | Promise<IDefinition | undefined>;
  snapshot?: ISnapshot | Promise<ISnapshot | undefined>;
  styles?: IAutoscrollStyles | Promise<IAutoscrollStyles | undefined>;
  l10n?: IL10n | Promise<IL10n | undefined>;
  license?: string | Promise<string | undefined>;
  view?: "live" | "test" | "preview";
  display?: "inline" | "page";
  removeBranding?: boolean;
  persistent?: boolean;
  className?: string;
  customStyle?: CSSProperties;
  customCSS?: string;
  attachments?: IRunnerAttachments;
  language?: string;
  locale?: L10n.ILocale | ((locale: string) => L10n.ILocale | Promise<L10n.ILocale | undefined> | undefined);
  translations?: L10n.TTranslation | L10n.TTranslation[] | ((language: string, name: string, version: string) => L10n.TTranslation | L10n.TTranslation[] | Promise<L10n.TTranslation | L10n.TTranslation[] | undefined> | undefined);
  loader?: JSX.Element;
  controller?: MutableRefObject<IAutoscrollController | undefined>;
  builder?: MutableRefObject<Builder | undefined>;
  onReady?: (instance: Instance | undefined) => void;
  onTouch?: () => void;
  onAction?: (
    type: "start" | "stage" | "unstage" | "focus" | "blur" | "pause" | "complete",
    definition: {
      fingerprint: string;
      name: string;
    },
    block?: {
      id: string;
      name: string;
      alias?: string;
    }
  ) => void;
  onImport?: (instance: Instance) => void;
  onChange?: (instance: Instance) => void;
  onData?: (instance: Instance) => void;
  onPause?: {
    recipe: "email";
    onPause: (
      emailAddress: string,
      snapshot: ISnapshot,
      language: string,
      locale: string,
      namespace: string
    ) => Promise<void> | boolean | void;
  } | ((snapshot: ISnapshot, language: string, locale: string, namespace: string) => Promise<void> | boolean | void);
  onSubmit?: (instance: Instance, language: string, locale: string, namespace: string) => Promise<string | undefined> | boolean | void;
  onComplete?: (instance: Instance, id?: string) => void;
  onReload?: () => IDefinition | Promise<IDefinition>;
  onEdit?: (type: "prologue" | "epilogue" | "styles" | "l10n" | "block", id?: string) => void;
  onDestroy?: () => void;
}`} symbols={{
  "Promise": "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise",
  "CSSProperties": "https://github.com/DefinitelyTyped/DefinitelyTyped/blob/master/types/react/v16/index.d.ts#L1539",
  "IDefinition": "/runner/api/library/interfaces/IDefinition/",
  "ISnapshot": "/runner/api/library/interfaces/ISnapshot/",
  "Instance": "/runner/api/library/classes/Instance/",
  "IAutoscrollStyles": "/runner/api/stock/autoscroll/interfaces/IAutoscrollStyles/",
  "IAutoscrollController": "/runner/api/stock/autoscroll/interfaces/IAutoscrollController/",
  "IL10n": "/runner/api/library/interfaces/IL10n/",
  "IRunnerAttachments": "/runner/api/react-hook/interfaces/IRunnerAttachments/",
  "L10n.ILocale": "/runner/api/library/modules/L10n/#ILocale",
  "L10n.TTranslation": "/runner/api/library/modules/L10n/#TTranslation",
  "Builder": "/builder/api/classes/Builder/",
}} />

## 🗃️ Properties {#properties}

---
### 🏷️ `attachments` {#attachments}
Specifies the attachments handler used by blocks that support file uploads. If your form does not need file upload support, you can omit this property.
#### Type
[`IRunnerAttachments`](../../../react-hook/interfaces/IRunnerAttachments.mdx)

:::warning
If your form uses file uploads and no attachments handler is defined, the uploaded files are included in the dataset using Base64 encoding.
:::

:::tip
Have a look at the [Handling file uploads guide](../../../../stock/guides/attachments.mdx) to learn how to use this feature.
:::

---
### 🏷️ `builder` {#builder}
Property to receive a reference to a [`Builder`](../../../../../builder/api/classes/Builder.mdx) instance. This will bind the runner with the builder and allows the builder to use the runner as a live form preview.
#### Type
MutableRefObject\<[`Builder`](../../../../../builder/api/classes/Builder.mdx) | undefined>

:::tip
Have a look at the [Live preview guide](../../../../../builder/integrate/guides/livepreview.mdx) to learn more about this feature.
:::

---
### 🏷️ `className` {#className}
Specifies a custom class name for the HTML element that holds the runner.
#### Type
string

:::tip
Have a look at the [Style forms guide](../../../../stock/guides/styles.mdx#custom-class) to learn more about this feature.
:::

---
### 🏷️ `controller` {#controller}
Property to receive a controller reference to the runner. This controller reference can be used to perform actions (like start, stop, pause) and retrieve status information from the runner.
#### Type
MutableRefObject\<[`IAutoscrollController`](IAutoscrollController.mdx) | undefined>

:::tip
Have a look at the [Controlling the runner guide](../../../../stock/guides/controller.mdx) to learn more about this feature.
:::

---
### 🏷️ `customCSS` {#customCSS}
Specifies custom CSS rules for blocks used in the runner. Multiple CSS rules can be separated with a new line and nesting within CSS rules is supported. Each block is referenced by its block identifier (for the stock blocks, this identifier is always prefixed with `@tripetto/block-` followed by the lowercase name of the block).
#### Type
string

:::tip
Have a look at the [Style forms guide](../../../../stock/guides/styles.mdx#custom-block-styles) to learn more about this feature.
:::

---
### 🏷️ `customStyle` {#customStyle}
Specifies the inline style for the HTML element that holds the runner.
#### Type
CSSProperties

:::tip
Have a look at the [Style forms guide](../../../../stock/guides/styles.mdx#custom-style) to learn more about this feature.
:::

---
### 🏷️ `definition` {#definition}
Specifies the definition to run. This property also accepts a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise). That's useful when you want to load the definition dynamically from an external source.
#### Type
[`IDefinition`](../../../library/interfaces/IDefinition.mdx) | Promise\<[`IDefinition`](../../../library/interfaces/IDefinition.mdx) | undefined>

---
### 🏷️ `display` {#display}
Specifies the display mode of the runner. It can be one of the following values:
- `inline`: The runner is shown inline with other content (this is the default value);
- `page`: The runner uses the whole page (viewport).
#### Type
"inline" | "page"

:::tip
Have a look at the [Display guide](../../../../stock/guides/display.mdx) to learn more about the runner display modes.
:::

---
### 🏷️ `l10n` {#l10n}
Specifies the localization (locale and translation) information. This property also accepts a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise). That's useful when you want to load the l10n data dynamically from an external source.
#### Type
[`IL10n`](../../../library/interfaces/IL10n.mdx) | Promise\<[`IL10n`](../../../library/interfaces/IL10n.mdx) | undefined>

:::tip
Have a look at the [Localization guide](../../../../stock/guides/l10n.mdx) for more information about locales and translations.
:::

---
### 🏷️ `language` {#language}
Specifies the preferred language to use for the form locale and translation. This property is only used for form definitions that don't have a specified language. If a language is specified in the form definition, that language is always used.
#### Type
string

:::warning
Make sure to implement dynamic loading of [locales](../../../../stock/guides/l10n.mdx#dynamic-locale) and [translations](../../../../stock/guides/l10n.mdx#dynamic-translation) to let this feature work. Dynamic loading allows the runner to load the appropriate locale and translation based on the form language or the language specified by this property.
:::

---
### 🏷️ `license` {#license}
Specifies a license code for the runner. This property also accepts a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise). That's useful when you want to load the license code dynamically from an external source.
#### Type
string | Promise\<string | undefined>

:::tip
Have a look at the [License guide](../../../../stock/guides/license.mdx) for more information about applying licenses.
:::

---
### 🏷️ `loader` {#loader}
Specifies a loader that is shown when the runner is loading (e.g., awaiting any promises).
#### Type
JSX.Element

---
### 🏷️ `locale` {#locale}
Specifies the locale or the locale loader function to use. The locale data contains the number and date/time formats the runner uses.
#### Type
[`L10n.ILocale`](../../../library/modules/L10n/index.mdx#ILocale) | ((locale: string) => [`L10n.ILocale`](../../../library/modules/L10n/index.mdx#ILocale) | Promise\<[`L10n.ILocale`](../../../library/modules/L10n/index.mdx#ILocale) | undefined> | undefined)

:::tip
Have a look at the [Localization guide](../../../../stock/guides/l10n.mdx#locale) to learn more about this feature.
:::

---
### 🏷️ `persistent` {#persistent}
Specifies if the runner should try to store sessions in the local store to preserve persistency on navigation between multiple pages that host the runner.
#### Type
boolean

:::tip
Have a look at the [Form data persistency guide](../../../../stock/guides/persistent.mdx) to learn more about this feature.
:::

---
### 🏷️ `removeBranding` {#removeBranding}
Removes all visual Tripetto branding from the runner (a valid [license](#license) is required).
#### Type
boolean

:::tip
Have a look at the [Disable Tripetto branding guide](../../../../stock/guides/branding.mdx) to learn more about removing the branding.
:::

---
### 🏷️ `snapshot` {#snapshot}
Specifies the snapshot that should be restored. This property also accepts a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise). That's useful when you want to load the snapshot dynamically from an external source.
#### Type
[`ISnapshot`](../../../library/interfaces/ISnapshot.mdx) | Promise\<[`ISnapshot`](../../../library/interfaces/ISnapshot.mdx) | undefined>

:::tip
Have a look at the [Pause and resume guide](../../../../stock/guides/pause-resume.mdx#resume) to learn more about pausing and resuming.
:::

---
### 🏷️ `styles` {#styles}
Specifies the styles (colors, font, size, etc.) for the runner. This property also accepts a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise). That's useful when you want to load the styles dynamically from an external source.
#### Type
[`IAutoscrollStyles`](IAutoscrollStyles.mdx) | Promise\<[`IAutoscrollStyles`](IAutoscrollStyles.mdx) | undefined>

:::tip
Have a look at the [Style forms guide](../../../../stock/guides/styles.mdx) to learn more about styling the runner.
:::

---
### 🏷️ `translations` {#translations}
Specifies the translations or the translations loader function to use.
#### Type
[`L10n.TTranslation`](../../../library/modules/L10n/index.mdx#TTranslation) | [`L10n.TTranslation[]`](../../../library/modules/L10n/index.mdx#TTranslation) | (language: string, name: string, version: string) => [`L10n.TTranslation`](../../../library/modules/L10n/index.mdx#TTranslation) | [`L10n.TTranslation[]`](../../../library/modules/L10n/index.mdx#TTranslation) | Promise\<[`L10n.TTranslation`](../../../library/modules/L10n/index.mdx#TTranslation) | [`L10n.TTranslation[]`](../../../library/modules/L10n/index.mdx#TTranslation) | undefined> | undefined

:::tip
Have a look at the [Localization guide](../../../../stock/guides/l10n.mdx#translations) to learn more about this feature.
:::

---
### 🏷️ `view` {#view}
Specifies the initial view mode of the runner. It can be one of the following values:
- `live`: The form is running in normal (production) mode (this is the default value);
- `test`: The form is running in test mode (no response data is submitted);
- `preview`: The form is running in preview mode, which shows all blocks in a single view.
#### Type
"live" | "test" | "preview"

## 📢 Events {#events}

---
### 🔔 `onAction` {#onAction}
Specifies a function that is invoked when the user performs an action. This is useful when you need to implement a tracking service like [Google Analytics](../../../../stock/guides/tracking.mdx#google-analytics),  [Google Tag Manager](../../../../stock/guides/tracking.mdx#gtm), or [FaceBook Pixel](../../../../stock/guides/tracking.mdx#facebook-pixel).
:::tip
Have a look at the [Tracking guide](../../../../stock/guides/tracking.mdx) to learn more about this feature.
:::
#### Signature
```ts
(
  type: "start" | "stage" | "unstage" | "focus" | "blur" | "pause" | "complete",
  definition: {
    fingerprint: string;
    name: string;
  },
  block?: {
    id: string;
    name: string;
    alias?: string;
  }
) => void
```
#### Parameters
| Name         | Type                                                                          | Optional | Description                                                                                                                                                                                                                                                                                                                                                  |
|:-------------|:------------------------------------------------------------------------------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `type`       | "start" \| "stage" \| "unstage" \| "focus" \| "blur" \| "pause" \| "complete" | No       | Contains the action type. It can be one of the following values:<br />- `start`: A form is started;<br />- `stage`: A block gets activated;<br />- `unstage`: A block gets deactivated;<br />- `focus`: An input element gains focus;<br />- `blur`: An input element loses focus;<br />- `pause`: A form is paused;<br />- `complete`: A form is completed. |
| `definition` | object                                                                        | No       | Contains information about the form definition.                                                                                                                                                                                                                                                                                                              |
| `block`      | object                                                                        | Yes      | Contains information about the blocks.                                                                                                                                                                                                                                                                                                                       |

---
### 🔔 `onChange` {#onChange}
Specifies a function that is invoked when there is a change in the runner. This change can be anything from a data update to a UI rendering cycle.
:::tip
If you only need updates on data changes, use the [`onData`](#onData) event.
:::
#### Signature
```ts
(instance: Instance) => void
```
#### Parameters
| Name       | Type                                                          | Optional | Description                       |
|:-----------|:--------------------------------------------------------------|:---------|:----------------------------------|
| `instance` | [`Instance`](../../../library/classes/Instance.mdx) | No       | Reference to the runner instance. |

---
### 🔔 `onComplete` {#onComplete}
Specifies a function that is invoked when the runner is completed (after the data is submitted).
:::tip
Have a look at the [Collecting response data guide](../../../../stock/guides/collecting.mdx#feedback) to learn how to feed an identifier or reference associated with the submitted response data back to the runner.
:::
#### Signature
```ts
(instance: Instance, id?: string) => void
```
#### Parameters
| Name       | Type                                                          | Optional | Description                                                          |
|:-----------|:--------------------------------------------------------------|:---------|:---------------------------------------------------------------------|
| `instance` | [`Instance`](../../../library/classes/Instance.mdx) | No       | Reference to the runner instance.                                    |
| `id`       | string                                                        | Yes      | Contains an optional identifier or reference returned by the server. |

---
### 🔔 `onData` {#onData}
Specifies a function that is invoked when there is a data change.
#### Signature
```ts
(instance: Instance) => void
```
#### Parameters
| Name       | Type                                                          | Optional | Description                       |
|:-----------|:--------------------------------------------------------------|:---------|:----------------------------------|
| `instance` | [`Instance`](../../../library/classes/Instance.mdx) | No       | Reference to the runner instance. |

---
### 🔔 `onDestroy` {#onDestroy}
Specifies a function that is invoked when the component unmounts.
#### Signature
```ts
() => void
```

---
### 🔔 `onEdit` {#onEdit}
Specifies a function that is invoked when an edit action is requested. This event is useful in a [live preview setup](../../../../../builder/integrate/guides/livepreview.mdx) where the builder can open the properties panel of the referenced item.
:::tip
Read the [Live form preview guide](../../../../../builder/integrate/guides/livepreview.mdx) for an example.
:::
#### Signature
```ts
(type: "prologue" | "epilogue" | "styles" | "l10n" | "block", id?: string) => void
```
#### Parameters
| Name   | Type                                                      | Optional | Description                              |
|:-------|:----------------------------------------------------------|:---------|:-----------------------------------------|
| `type` | "prologue" \| "epilogue" \| "styles" \| "l10n" \| "block" | No       | Specifies the type of item being edited. |
| `id`   | string                                                    | Yes      | Specifies the item identifier.           |

---
### 🔔 `onImport` {#onImport}
Specifies a function that is invoked when data can be imported into the instance. To import form data, you need to use one of the [`Import`](../../../library/modules/Import.mdx) functions from the Runner library as shown in the [Prefilling forms guide](../../../../stock/guides/prefilling.mdx).
:::tip
Have a look at the [Prefilling forms guide](../../../../stock/guides/prefilling.mdx) to learn more about importing data.
:::
#### Signature
```ts
(instance: Instance) => void
```
#### Parameters
| Name       | Type                                                          | Optional | Description                       |
|:-----------|:--------------------------------------------------------------|:---------|:----------------------------------|
| `instance` | [`Instance`](../../../library/classes/Instance.mdx) | No       | Reference to the runner instance. |

---
### 🔔 `onPause` {#onPause}
Specifies a function or recipe that is invoked when the runner wants to pause. The recipe `email` can be used if you want to collect the email address of the respondent before pausing the form. Read the [Pause and resume guide](../../../../stock/guides/pause-resume.mdx) for more information about this feature.

Once you implement the `onPause` event, a pause button will appear in the runner's UI. When the respondent clicks this button, the pause event fires. When the `email` recipe is used, the runner will first ask for the respondent's email address and then invoke the event.
:::tip
Have a look at the [Pause and resume guide](../../../../stock/guides/pause-resume.mdx) to learn more about this feature.
:::
#### Signature
```ts
{
    recipe: "email";
    onPause: (
      emailAddress: string,
      snapshot: ISnapshot,
      language: string,
      locale: string,
      namespace: string
    ) => Promise<void> | boolean | void;
  } | ((snapshot: ISnapshot, language: string, locale: string, namespace: string) => Promise<void> | boolean | void)
```
#### Parameters
| Name           | Type                                                               | Optional | Description                                                          |
|:---------------|:-------------------------------------------------------------------|:---------|:---------------------------------------------------------------------|
| `emailAddress` | string                                                             | No       | Contains the email address when the `email` recipe is used.          |
| `snapshot`     | [`ISnapshot`](../../../library/interfaces/ISnapshot.mdx)           | No       | Contains the snapshot data.                                          |
| `language`     | string                                                             | No       | Contains the language used in the runner (default is `en`).          |
| `locale`       | string                                                             | No       | Contains the locale identifier used in the runner (default is `en`). |
| `namespace`    | string                                                             | No       | Contains the namespace identifier for the runner.                    |
#### Return value
Returns `true` when the pause succeeded, `false` if it failed or a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) that resolves in case the pause succeeded or rejects in case of an error.

---
### 🔔 `onReady` {#onReady}
Specifies a function that is invoked when the runner is ready.
:::info
The `onReady` event is invoked after all the resources (form definition, styles, locales, and translations) are loaded.
:::
#### Signature
```ts
(instance: Instance | undefined) => void
```
#### Parameters
| Name       | Type                                                                       | Optional | Description                                                             |
|:-----------|:---------------------------------------------------------------------------|:---------|:------------------------------------------------------------------------|
| `instance` | [`Instance`](../../../library/classes/Instance.mdx) \| undefined | No       | Reference to the runner instance or `undefined` if no form has started. |

---
### 🔔 `onReload` {#onReload}
Specifies a function that is invoked when the runner wants to reload the definition. The definition returned by this event handler is loaded, preserving the data already entered by the user.
:::info
This event will be automatically invoked when data is submitted to an endpoint, but the endpoint indicates the form is outdated. To indicate this, the [`onSubmit`](#onSubmit) event can reject with status `outdated`. See the [Collecting response data guide](../../../../stock/guides/collecting.mdx#reject-error-codes) for more information about this feature.
:::
#### Signature
```ts
() => IDefinition | Promise<IDefinition>
```
#### Return value
Returns the reloaded [`IDefinition`](../../../library/interfaces/IDefinition.mdx) or a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) that resolves with the [`IDefinition`](../../../library/interfaces/IDefinition.mdx).

---
### 🔔 `onSubmit` {#onSubmit}
Specifies a function that is invoked when the runner submits data. This event is the one to use to submit the form data to an endpoint. To retrieve the form data, you need to use one of the [`Export`](../../../library/modules/Export.mdx) functions from the Runner library as shown in the [Collecting response data guide](../../../../stock/guides/collecting.mdx).

This event supports different return types. First of all, you can return a boolean value to indicate if the submission was successful or not. That's only useful for synchronous operations. If you need async operation, you can return a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) which resolves in case of a successful operation or rejects when the submission fails.
:::tip
Have a look at the [Collecting response data guide](../../../../stock/guides/collecting.mdx) to learn more about submitting data, async operation, and handling errors. To learn more about validating the response data, read the [Validating response data guide](../../../../stock/guides/validation.mdx). If you need spam protection for your forms, the [Prevent form spamming guide](../../../../stock/guides/spam-protection.mdx) is of interest to you.
:::
#### Signature
```ts
(instance: Instance, language: string, locale: string, namespace: string) =>
  Promise<string | undefined> | boolean | void
```
| Name        | Type                                                          | Optional | Description                                                          |
|:------------|:--------------------------------------------------------------|:---------|:---------------------------------------------------------------------|
| `instance`  | [`Instance`](../../../library/classes/Instance.mdx) | No       | Reference to the runner instance.                                    |
| `language`  | string                                                        | No       | Contains the language used in the runner (default is `en`).          |
| `locale`    | string                                                        | No       | Contains the locale identifier used in the runner (default is `en`). |
| `namespace` | string                                                        | No       | Contains the namespace identifier for the runner.                    |
#### Return value
Returns `true` when the submission succeeded, `false` if it failed or a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) that resolves with an optional reference string (in case it succeeds) or rejects (in case of an error).

---
### 🔔 `onTouch` {#onTouch}
Specifies a function that is invoked when the runner is "touched" by a user. This is an interaction by the user with the runner by either the mouse, touch input, or keyboard.
#### Signature
```ts
() => void
```
