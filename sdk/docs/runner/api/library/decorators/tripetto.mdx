---
title: "@tripetto decorator - Runner"
sidebar_label: "@tripetto"
toc_max_heading_level: 4
description: The @tripetto decorator is used to register the runner part of node and condition blocks (Runner library).
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# @tripetto

The `@tripetto` decorator is used to register the **runner part** of node and condition blocks. There are three types of blocks that can be registered:
- [`NodeBlock`](../classes/NodeBlock.mdx): Visual blocks that interact with the user and collect response data;
- [`ConditionBlock`](../classes/ConditionBlock.mdx): Blocks that are used in branches to define conditions;
- [`HeadlessBlock`](../classes/HeadlessBlock.mdx): Invisible blocks that can perform actions.

:::tip
See the [blocks](../../../../blocks/custom/introduction.md) documentation to learn how to develop custom blocks.
:::

#### Decorator type
Class [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#class-decorators)

#### Applies to
- [`NodeBlock`](../classes/NodeBlock.mdx)
- [`ConditionBlock`](../classes/ConditionBlock.mdx)
- [`HeadlessBlock`](../classes/HeadlessBlock.mdx)

#### Decorator signature
```ts
@tripetto(properties: INodeBlockDecorator | IConditionBlockDecorator | IHeadlessBlockDecorator)
```

#### Decorator parameters
| Name         | Type                                                                                                                                                              | Optional | Description                                                                 |
|:-------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------|:---------|:----------------------------------------------------------------------------|
| `properties` | [`INodeBlockDecorator`](#INodeBlockDecorator) \| [`IConditionBlockDecorator`](#IConditionBlockDecorator) \| [`IHeadlessBlockDecorator`](#IHeadlessBlockDecorator) | No       | Specifies the block properties that implicitly determine the type of block. |

#### Example
```ts showLineNumbers
import { tripetto, NodeBlock } from "@tripetto/runner";

@tripetto({
  type: "node",
  identifier: "example-block",
})
class ExampleBlock extends NodeBlock {
  // Block implementation here
}
```

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `INodeBlockDecorator` {#INodeBlockDecorator}
Describes the interface for registering a [`NodeBlock`](../classes/NodeBlock.mdx).
##### Type declaration
<InterfaceDeclaration prefix="#INodeBlockDecorator-" src={`interface INodeBlockDecorator {
  type: "node";
  identifier: string;
  alias?: string | string[];
  namespace?: string | string[];
  autoRender?: boolean;
  autoValidate?: boolean;
  ref?: {};
}`} />

---
#### 🏷️ `alias` {#INodeBlockDecorator-alias}
Specifies optional type aliases (alternative type identifiers for the block).
##### Type
string | string[]

---
#### 🏷️ `autoRender` {#INodeBlockDecorator-autoRender}
Specifies if the block needs to be rerendered upon value store (disabled by default).
##### Type
boolean

---
#### 🏷️ `autoValidate` {#INodeBlockDecorator-autoValidate}
Specifies if the block checks if all required slots contain a value (enabled by default).
##### Type
boolean

---
#### 🏷️ `identifier` {#INodeBlockDecorator-identifier}
Specifies the unique type identifier for the block.
##### Type
string

---
#### 🏷️ `namespace` {#INodeBlockDecorator-namespace}
Specifies the namespace(s) for the block.
##### Type
string | string[]

---
#### 🏷️ `ref` {#INodeBlockDecorator-ref}
Optional reference to pass static data to the block.
##### Type
`{}`

---
#### 🏷️ `type` {#INodeBlockDecorator-type}
Specifies to register a node block.
##### Type
"node"

---
### 🔗 `IConditionBlockDecorator` {#IConditionBlockDecorator}
Describes the interface for registering a [`ConditionBlock`](../classes/ConditionBlock.mdx).
##### Type declaration
<InterfaceDeclaration prefix="#IConditionBlockDecorator-" src={`interface IConditionBlockDecorator {
  type: "condition";
  identifier: string;
  alias?: string | string[];
  namespace?: string | string[];
}`} />

---
#### 🏷️ `alias` {#IConditionBlockDecorator-alias}
Specifies optional type aliases (alternative type identifiers for the block).
##### Type
string | string[]

---
#### 🏷️ `identifier` {#IConditionBlockDecorator-identifier}
Specifies the unique type identifier for the block.
##### Type
string

---
#### 🏷️ `namespace` {#IConditionBlockDecorator-namespace}
Specifies the namespace(s) for the block.
##### Type
string | string[]

---
#### 🏷️ `type` {#IConditionBlockDecorator-type}
Specifies to register a condition block.
##### Type
"condition"

---
### 🔗 `IHeadlessBlockDecorator` {#IHeadlessBlockDecorator}
Describes the interface for registering a [`HeadlessBlock`](../classes/HeadlessBlock.mdx).
##### Type declaration
<InterfaceDeclaration prefix="#IHeadlessBlockDecorator-" src={`interface IHeadlessBlockDecorator {
  type: "headless";
  identifier: string;
  alias?: string | string[];
  namespace?: string | string[];
}`} />

---
#### 🏷️ `alias` {#IHeadlessBlockDecorator-alias}
Specifies optional type aliases (alternative type identifiers for the block).
##### Type
string | string[]

---
#### 🏷️ `identifier` {#IHeadlessBlockDecorator-identifier}
Specifies the unique type identifier for the block.
##### Type
string

---
#### 🏷️ `namespace` {#IHeadlessBlockDecorator-namespace}
Specifies the namespace(s) for the block.
##### Type
string | string[]

---
#### 🏷️ `type` {#IHeadlessBlockDecorator-type}
Specifies to register a headless block.
##### Type
"headless"
