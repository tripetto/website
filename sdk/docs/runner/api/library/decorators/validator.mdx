---
title: "@validator decorator - Runner"
sidebar_label: "@validator"
description: The @validator decorator is used to mark the validation method in a block.
---

# @validator

The `@validator` decorator is used to mark the validation method in a block. That method is invoked when the block needs to be validated. It allows synchronous and asynchronous validation. When synchronous validation is used the decorator method should return `true` when the validation succeeds or `false` when the validation fails. When asynchronous validation is used, the method should return the `callback` property of the `props` argument. When the validation completes, the result should be passed to [`callback.return`](../classes/Callback.mdx#return) (see example below).

:::info
It can be used to decorate a method in a [`NodeBlock`](../classes/NodeBlock.mdx) or [`HeadlessBlock`](../classes/HeadlessBlock.mdx) derived class.
:::

:::tip
Read more about validation in the [Block validation guide](../../../../blocks/custom/guides/validation.mdx).
:::

#### Decorator type
Method [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#method-decorators)

#### Applies to
- [`NodeBlock`](../classes/NodeBlock.mdx)
- [`HeadlessBlock`](../classes/HeadlessBlock.mdx)

#### Decorator signature
```ts
@validator
```

#### Decorated method signature
```ts
(props?: {
  callback: Callback<boolean>;
  current: "unknown" | "fail" | "pass";
}): boolean | Callback<boolean>;
```

#### Decorated method parameters
| Name    | Type    | Optional | Description                                                                                                                                                                                                                                                                                                                 |
|:--------|:--------|:---------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `props` | object  | Yes      | Specifies the properties for the validator method. Contains the following properties:<br/>- `callback`: Reference to the [`Callback`](../classes/Callback.mdx) instance. Pass this reference as method return value to enable the asynchronous validation callback;<br/>- `current`: Contains the current validation state. |

#### Return value
Returns `true` if the validation passes, `false` when the validation fails, or `props.callback` to indicate asynchronous operation.

#### Example
Synchronous example:
```ts showLineNumbers
import { tripetto, validator, NodeBlock, Callback } from "@tripetto/runner";

@tripetto({
  type: "node",
  identifier: "example-block",
})
class ExampleBlock extends NodeBlock {
  //highlight-start
  @validator
  validate(): boolean {
    // Indicate the validation passes
    return true;
  }
  //highlight-end
}
```

Asynchronous example:
```ts showLineNumbers
import { tripetto, validator, NodeBlock, Callback } from "@tripetto/runner";

@tripetto({
  type: "node",
  identifier: "example-block",
})
class ExampleBlock extends NodeBlock {
  //highlight-start
  @validator
  validate({ callback }: { callback: Callback<boolean> }): Callback<boolean> {
    setTimeout(() => {
      // After a while, indicate the validation fails
      callback.return(false);
    }, 1000);

    return callback;
  }
  //highlight-end
}
```
