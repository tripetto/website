---
title: "@condition decorator - Runner"
sidebar_label: "@condition"
description: The @condition decorator is used to mark the condition method in a condition block.
---

# @condition

The `@condition` decorator is used to mark the condition method in a condition block. That method is invoked when the condition block wants to verify if a condition is valid or not. It allows synchronous and asynchronous operation. When synchronous operation is used the decorator method should return `true` when the condition is valid or `false` when the condition is invalid. When asynchronous operation is used, the method should return the `callback` property of the `props` argument. When the condition operation completes, the result should be passed to [`callback.return`](../classes/Callback.mdx#return) (see example below).

:::info
It can be used to decorate a method in a [`ConditionBlock`](../classes/ConditionBlock.mdx) derived class.
:::

:::tip
Read more about conditions in the [Block conditions guide](../../../../blocks/custom/guides/conditions.mdx).
:::

#### Decorator type
Method [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#method-decorators)

#### Applies to
- [`ConditionBlock`](../classes/ConditionBlock.mdx)

#### Decorator signature
```ts
@condition
```

#### Decorated method signature
```ts
(props?: {
  callback: Callback<boolean>;
}): boolean | Callback<boolean>;
```

#### Decorated method parameters
| Name    | Type    | Optional | Description                                                                                                                                                                                                                                                        |
|:--------|:--------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `props` | object  | Yes      | Specifies the properties for the condition method. Contains the following properties:<br/>- `callback`: Reference to the [`Callback`](../classes/Callback.mdx) instance. Pass this reference as method return value to enable the asynchronous condition callback. |

#### Return value
Returns `true` if the condition is valid, `false` when the condition is invalid, or `props.callback` to indicate asynchronous operation.

#### Example
Synchronous example:
```ts showLineNumbers
import { tripetto, condition, ConditionBlock, Callback } from "@tripetto/runner";

@tripetto({
  type: "condition",
  identifier: "example-condition-block",
})
class ExampleConditionBlock extends ConditionBlock {
  //highlight-start
  @condition
  isValid(): boolean {
    // Indicate the condition is valid
    return true;
  }
  //highlight-end
}
```

Asynchronous example:
```ts showLineNumbers
import { tripetto, condition, ConditionBlock, Callback } from "@tripetto/runner";

@tripetto({
  type: "condition",
  identifier: "example-condition-block",
})
class ExampleConditionBlock extends ConditionBlock {
  //highlight-start
  @condition
  isValid({ callback }: { callback: Callback<boolean> }): Callback<boolean> {
    setTimeout(() => {
      // After a while, indicate the condition is invalid
      callback.return(false);
    }, 1000);

    return callback;
  }
  //highlight-end
}
```
