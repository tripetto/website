---
title: IStoryline interface - Runner
sidebar_label: IStoryline
description: Interface that describes the immutable representation of the storyline (Runner library).
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# IStoryline interface

## 📖 Description {#description}
Interface that describes the [immutable](../classes/Storyline.mdx#immutable) representation of the storyline.

## 📃 Type declaration {#type-declaration}
<InterfaceDeclaration prefix="#" src={`interface IStoryline {
  readonly moments: Moment[];
  readonly pages: {
    number: number;
    active: boolean;
    activate: () => void;
  }[];
  readonly presentMoment: Moment | undefined;
  readonly presentSection: Section | undefined;
  readonly presentNodes: IObservableNode[];
  readonly all: (IObservableNode & {
    bookmark: IBookmark;
  })[];
  readonly nodes: IObservableNode[];
  readonly bookmarks: IBookmark[];
  readonly slides: ISlide[];
  readonly isEvaluating: boolean;
  readonly isEmpty: boolean;
  readonly isAtStart: boolean;
  readonly isAtHead: boolean;
  readonly isAtFinish: boolean;
  readonly isFinishable: boolean;
  readonly isFinishing: boolean;
  readonly isFinished: boolean;
  readonly isPausing: boolean;
  readonly isPaused: boolean;
  readonly hasDataCollected: boolean;
  readonly isFailed: boolean;
  readonly isPassed: boolean;
  readonly mode: "paginated" | "continuous" | "progressive" | "ahead";
  readonly direction: "forward" | "backward";
  readonly count: number;
  readonly enumerators: number;
  readonly percentage: number;
  readonly stepForward: () => boolean;
  readonly stepBackward: () => boolean;
  readonly stepToStart: () => boolean;
  readonly stepToHead: () => boolean;
  readonly stepToPage: (page: number) => boolean;
  readonly finish: () => Promise<void> | false;
  readonly map: <R>(fn: (moment: Moment, index: number) => R) => R[];
}`} symbols={{
  "Moment": "/runner/api/library/classes/Moment/",
  "Section": "/runner/api/library/classes/Section/",
  "IObservableNode": "/runner/api/library/interfaces/IObservableNode/",
  "IBookmark": "/runner/api/library/interfaces/IBookmark/",
  "ISlide": "/runner/api/library/interfaces/ISlide/",
  "Promise": "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise"
}} />

## 🗃️ Properties {#properties}

---
### 🏷️ `all` {#all}
Retrieves all observable nodes of the active moment (so the nodes of all moments in the [`moments`](#moments) collection) with their corresponding bookmark.
#### Type
`(IObservableNode & { bookmark: IBookmark })[]`

---
### 🏷️ `bookmarks` {#bookmarks}
Retrieves the bookmarks in the storyline.
#### Type
[`IBookmark[]`](IBookmark.mdx)

---
### 🏷️ `count` {#count}
Retrieves the total number of observable nodes in the storyline.
#### Type
number

---
### 🏷️ `direction` {#direction}
Retrieves the most recent step direction of the storyline.
#### Type
"forward" | "backward"

---
### 🏷️ `enumerators` {#enumerators}
Retrieves the total number of observable nodes that have an enumerator value.
#### Type
number

---
### 🏷️ `hasDataCollected` {#hasDataCollected}
Retrieves if the storyline has any data collected.
#### Type
boolean

---
### 🏷️ `isAtFinish` {#isAtFinish}
Retrieves if the present moment is at the finish of the storyline.
#### Type
boolean

---
### 🏷️ `isAtHead` {#isAtHead}
Retrieves if the present moment is at the head of the storyline.
#### Type
boolean

---
### 🏷️ `isAtStart` {#isAtStart}
Retrieves if the present moment is at the beginning of the storyline.
#### Type
boolean

---
### 🏷️ `isChanged` {#isChanged}
Retrieves if the storyline has changed since the last call of this prop.
#### Type
boolean

---
### 🏷️ `isEmpty` {#isEmpty}
Retrieves if the storyline is empty (has no observable nodes, basically an empty form).
#### Type
boolean

---
### 🏷️ `isEvaluating` {#isEvaluating}
Retrieves if the storyline is evaluating.
#### Type
boolean

---
### 🏷️ `isFailed` {#isFailed}
Retrieves if the validation of the storyline failed.
#### Type
boolean

---
### 🏷️ `isFinishable` {#isFinishable}
Retrieves if the storyline is finishable using the [`finish`](#finish) method.
#### Type
boolean

---
### 🏷️ `isFinished` {#isFinished}
Retrieves if the storyline is finished.
#### Type
boolean

---
### 🏷️ `isFinishing` {#isFinishing}
Retrieves if the storyline is finishing.
#### Type
boolean

---
### 🏷️ `isPassed` {#isPassed}
Retrieves if the validation of the storyline passed.
#### Type
boolean

---
### 🏷️ `isPaused` {#isPaused}
Retrieves if the storyline is paused.
#### Type
boolean

---
### 🏷️ `isPausing` {#isPausing}
Retrieves if the storyline is pausing.
#### Type
boolean

---
### 🏷️ `mode` {#mode}
Retrieves the mode of operation. It can be one of the following values:
- `paginated`: Render each section on a separate page (this is the default behavior);
- `continuous`: Render all completed (past) sections and the current (present) section on a page;
- `progressive`: Render all completed (past), current (present) and future sections on a page till the point where one of the sections fails validation;
- `ahead`: Render all completed (past), current (present) and future sections on a page, regardless of the section's validation result.
#### Type
"paginated" | "continuous" | "progressive" | "ahead"

---
### 🏷️ `moments` {#moments}
Retrieves the moments that are in the storyline. There are three types of moments:
- Past moments;
- The present moment;
- Future moments.

:::info
The best way to understand these moment types is to visualize a paginated form runner, where the questions (node blocks) are presented on a page. Each section is a page. All pages that are already answered by the respondent are past moments. The current page the respondent is on is the present moment. And all the pages the respondent still needs to answer are future moments.
:::
#### Type
[`Moment[]`](../classes/Moment.mdx)

---
### 🏷️ `nodes` {#nodes}
Retrieves the observable nodes of the active moment (so the nodes of all moments in the [`moments`](#moments) collection).
#### Type
[`IObservableNode[]`](IObservableNode.mdx)

---
### 🏷️ `pages` {#pages}
Retrieves the collection of pages.
:::caution
Only available for runners in [paginated mode](#mode).
:::
#### Type
```ts
{
  /* Contains the page number. */
  number: number;

  /* Contains if the page is active. */
  active: boolean;

  /* Activates the page. */
  activate: () => void;
}[]
```

---
### 🏷️ `percentage` {#percentage}
Retrieves the progress percentage of the storyline.
:::tip
Can be used to show a progressbar that indicates the progress of the form.
:::
#### Type
number

---
### 🏷️ `presentMoment` {#presentMoment}
Retrieves the present moment.
#### Type
[`Moment`](../classes/Moment.mdx) | undefined

---
### 🏷️ `presentNodes` {#presentNodes}
Retrieves the observable nodes of the present moment.
#### Type
[`IObservableNode[]`](IObservableNode.mdx)

---
### 🏷️ `presentSection` {#presentSection}
Retrieves the section of the present moment.
#### Type
[`Section`](../classes/Section.mdx) | undefined

---
### 🏷️ `slides` {#slides}
Retrieves the slides in the storyline.
#### Type
[`ISlide[]`](ISlide.mdx)

## ▶️ Functions {#functions}

---
### 🔧 `finish` {#finish}
Finish the storyline when it is at the end.
:::caution
You can only finish a form when it is [finishable](#isFinishable).
:::
#### Signature
```ts
finish(): Promise<void>
```
#### Return value
Returns a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) that resolves when the instance was finished. The finish operation can be asynchronous. For example, when the form  submits data to an endpoint. During the finish operation you could show a visual indicator to the respondent to indicate the form is submitting. When the promise resolves or rejects, the finish operation ends. When the promise rejects, the form remains active and the respondent should be able to try to finish the form again.

---
### 🔧 `map` {#map}
Maps moments of the storyline to a certain type.
#### Signature
```ts
map<R>(fn: (moment: Moment, index: number) => R): R[]
```
#### Parameters
| Name | Type                                                            | Optional | Description                 |
|:-----|:----------------------------------------------------------------|:---------|:----------------------------|
| `fn` | (moment: [`Moment`](../classes/Moment.mdx), index: number) => R | No       | Specifies the map function. |
#### Return value
Returns the resulting array of the map operation.

---
### 🔧 `stepBackward` {#stepBackward}
Step backward in the storyline.
#### Signature
```ts
stepBackward(): boolean
```
#### Return value
Returns `true` if the step succeeded.

---
### 🔧 `stepForward` {#stepForward}
Step forward in the storyline.
#### Signature
```ts
stepForward(): boolean
```
#### Return value
Returns `true` if the step succeeded.

---
### 🔧 `stepToHead` {#stepToHead}
Step to the head of the storyline.
#### Signature
```ts
stepToHead(): boolean
```
#### Return value
Returns `true` if the step succeeded.

---
### 🔧 `stepToPage` {#stepToPage}
Steps to a page.
:::caution
Only available for runners in [paginated mode](#mode).
:::
#### Signature
```ts
stepToPage(page: number): boolean
```
#### Parameters
| Name   | Type   | Optional | Description                    |
|:-------|:-------|:---------|:-------------------------------|
| `page` | number | No       | Specifies the page to step to. |
#### Return value
Returns `true` if the step succeeded.

---
### 🔧 `stepToStart` {#stepToStart}
Step to the start of the storyline.
#### Signature
```ts
stepToStart(): boolean
```
#### Return value
Returns `true` if the step succeeded.
