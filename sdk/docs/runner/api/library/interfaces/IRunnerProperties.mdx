---
title: IRunnerProperties interface - Runner
sidebar_label: IRunnerProperties
description: Properties interface for constructing new Runner instances (Runner library).
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# IRunnerProperties interface

## 📖 Description {#description}
Properties interface for constructing new [`Runner`](../classes/Runner.mdx) instances.

## 📃 Type declaration {#type-declaration}
<InterfaceDeclaration prefix="#" src={`interface IRunnerProperties {
  definition: IDefinition | string;
  mode: "paginated" | "continuous" | "progressive" | "ahead";
  namespace?: string;
  l10n?: L10n.Namespace;
  start?: boolean;
  preview?: boolean;
  test?: boolean;
  snapshot?: ISnapshot;
  data?: (instance: Instance) => void;
}`} symbols={{
  "IDefinition": "/runner/api/library/interfaces/IDefinition/",
  "ISnapshot": "/runner/api/library/interfaces/ISnapshot/",
  "Instance": "/runner/api/library/classes/Instance/",
  "L10n.Namespace": "/runner/api/library/modules/L10n/Namespace/"
}} />

## 🗃️ Properties {#properties}

---
### 🏷️ `definition` {#definition}
Specifies the form definition to load. You can either supply the [form definition](../../../../builder/api/interfaces/IDefinition.mdx) as an object or as a JSON string.
#### Type
[`IDefinition`](../../../../builder/api/interfaces/IDefinition.mdx) | string

:::info
When a string is supplied, [`JSON.parse`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/parse) will be used to convert to an object.
:::

---
### 🏷️ `l10n` {#l10n}
Specifies the l10n namespace to use. This namespace contains translation and locale information for the runner.
#### Type
[`L10n.Namespace`](../modules/L10n/Namespace.mdx)

---
### 🏷️ `mode` {#mode}
Specifies the mode of operation of the runner. It can be one of the following values:
- `paginated`: Render each section on a separate page (this is the default behavior);
- `continuous`: Render all completed sections and the current section on a page;
- `progressive`: Render all completed (past), current (present) and future sections on a page till the point where one of the sections fails validation;
- `ahead`: Render all completed (past), current (present) and future sections on a page, regardless of the section's validation result.
#### Type
"paginated" | "continuous" | "progressive" | "ahead"

---
### 🏷️ `namespace` {#namespace}
Specifies a namespace identifier for the runner. This is a unique identifier for the runner.
#### Type
string

---
### 🏷️ `preview` {#preview}
Specifies if the runner should run in preview mode. This is a special mode that allows the whole form to be shown by the runner UI (basically ignoring all branch conditions, so all branches are taken and shown). This is useful when building a [live preview](../../../../builder/integrate/guides/livepreview.mdx) panel that is shown along with the builder.
#### Type
boolean

:::info
When `preview` is set to `true` and the [`start`](#start) property is omitted, the runner will start immediately after construction.
:::

---
### 🏷️ `snapshot` {#snapshot}
Specifies the snapshot to restore. A snapshot is a saved state of a runner that can be restored later on. To obtain a snapshot you either [pause](../classes/Runner.mdx#pause) the runner or use [`snapshot`](../classes/Runner.mdx#snapshot).
#### Type
[`ISnapshot`](ISnapshot.mdx)

---
### 🏷️ `start` {#start}
Specifies if the runner should immediately start a from instance after construction or that it should wait until an instance is manually started with the [`start`](../classes/Runner.mdx#start) function.
#### Type
boolean

---
### 🏷️ `test` {#test}
Specifies if the runner should run in test mode. This is a special mode that runs the whole form normally, but without invoking the data submission events when the form completes. This is useful in a [live preview](../../../../builder/integrate/guides/livepreview.mdx) setup where users want to test a form, without generating response data.
#### Type
boolean

:::info
When `preview` is set to `true`, this supersedes the test mode and the preview mode will be active.
:::

## ▶️ Functions {#functions}

---
### 🔧 `data` {#data}
Specifies a function that can supply data to new form instances. This is useful for importing data into forms ([prefilling forms](../../../stock/guides/prefilling.mdx)). This function is invoked automatically when a form instance is started.
#### Signature
```ts
(instance: Instance) => void
```
#### Parameters
| Name       | Type                                  | Optional | Description                                |
|:-----------|:--------------------------------------|:---------|:-------------------------------------------|
| `instance` | [`Instance`](../classes/Instance.mdx) | No       | Contains a reference to the form instance. |
#### Example
```ts showLineNumbers
import { Runner, Import } from "@tripetto/runner";

const runner = new Runner({
  definition: /* Supply your form definition here */,
  mode: "paginated",
  data: (instance) => {
    // This example assumes the form has two text fields with
    // the data identifiers (aliases) `FIRSTNAME` and `LASTNAME`.
    Import.fields(instance, [
      {
        name: "FIRSTNAME",
        value: "John",
      },
      {
        name: "LASTNAME",
        value: "Doe",
      }
    ]);

    // Or use the generated key of each field
    Import.fields(instance, [
      {
        key: "9ef64ab160ef6096ceecb4561f3eecc37007b669337929393a9d3789fa744f3e",
        value: "John",
      },
      {
        key: "e14599867b9fd518459e7ecaf974d33f84f2c3b322f644e838f948c2136eec3a",
        value: "Doe",
      }
    ]);
  }
});
```
