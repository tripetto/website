---
title: IVariable interface - Runner
sidebar_label: IVariable
description: Describes the interface that holds a variable (Runner library).
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# IVariable interface

## 📖 Description {#description}
Describes the interface that holds a variable.

## 📃 Type declaration {#type-declaration}
<InterfaceDeclaration prefix="#" src={`interface IVariable {
  readonly id: string;
  readonly string: string;
  readonly hasValue: boolean;
  readonly value?: any;
  readonly reference?: string;
  readonly content: {
    value: string;
    type: "string" | "text" | "markdown";
    separator: string;
  }[];
  readonly slot?: Slot;
  readonly subscribe: (fnChange: (variable: IVariable) => void, context?: {}) => void;
  readonly unsubscribe: (context: {}) => void;
}`} symbols={{
  "Slot": "/runner/api/library/modules/Slots/Slot/",
  "IVariable": "../IVariable/"
}} />

## 🗃️ Properties {#properties}

---
### 🏷️ `content` {#content}
Contains the content for the variable. This array has at least one item but can contain more if the variable has multiple values in different contexts.
#### Type
```ts
{
  /*
   * Contains the type of value. It can be one of the following values:
   * - `string`: `value` contains the variable value as a string;
   * - `text`: `value` contains text with basic markdown support for referencing variables;
   * - `markdown`: `value` contains markdown content with support for basic formatting, hyperlinks and referencing variables.
   */
  type: "string" | "text" | "markdown";

  /* Contains the value defined by the `type` property. */
  value: string;

  /* Contains the character to use as separator when concatenating multiple value content parts. */
  separator: string;
}[]
```

---
### 🏷️ `hasValue` {#hasValue}
Retrieves if a valid value is present.
#### Type
boolean

---
### 🏷️ `id` {#id}
Contains the identifier of the variable.
#### Type
string

---
### 🏷️ `reference` {#reference}
Retrieves the additional reference that can be set. This reference is used by some blocks to store an additional identifier related to the value. For example, when a value from a dropdown block is set, the reference is used to store the identifier of the selected item.
#### Type
string

---
### 🏷️ `slot` {#slot}
Reference to the slot that defines the data value.
#### Type
[`Slot`](../modules/Slots/Slot.mdx)

---
### 🏷️ `string` {#string}
Contains the value as a string.
#### Type
string

---
### 🏷️ `value` {#value}
Retrieves the value (can be `undefined` when there is no value set).
#### Type
any

## ▶️ Functions {#functions}

---
### 🔧 `subscribe` {#subscribe}
Subscribes a listener to the variable. This listener function is invoked when the variable changes. It is also possible to supply a context. This context is used when you want to [`unsubscribe`](#unsubscribe) all the listener functions attached to the supplied context.
#### Signature
```ts
(onChange: (variable: IVariable) => void, context?: {}) => void
```
#### Parameters
| Name       | Type                                             | Optional | Description                                                      |
|:-----------|:-------------------------------------------------|:---------|:-----------------------------------------------------------------|
| `onChange` | (variable: [`IVariable`](IVariable.mdx)) => void | No       | Specifies the function that is called when a change is detected. |
#### Example
```
const variable: IVariable;
const context = {};

// Subscribes a listener function and attaches the function to the supplied context
variable.subscribe((v) => console.log(`New value: ${v.string}`), context);

// And, when you want to unsubscribe all listeners attached to the context do:
variable.unsubscribe(context);
```

---
### 🔧 `unsubscribe` {#unsubscribe}
Unsubscribe the listener functions attached to a context.
#### Signature
```ts
(context: {}) => void
```
#### Parameters
| Name      | Type | Optional | Description                           |
|:----------|:-----|:---------|:--------------------------------------|
| `context` | `{}` | No       | Specifies the context to unsubscribe. |
