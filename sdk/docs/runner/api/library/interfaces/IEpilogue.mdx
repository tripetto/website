---
title: IEpilogue interface - Runner
sidebar_label: IEpilogue
description: Interface that describes the properties to define a form epilogue within a form definition (Runner library).
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# IEpilogue interface

## 📖 Description {#interface-description}
Interface that describes the properties to define a form epilogue within a [form definition](IDefinition.mdx). An epilogue can be used by runners (that support it) to show a closing message when a form is completed.

## 📃 Type declaration {#type-declaration}
<InterfaceDeclaration prefix="#" src={`interface IEpilogue {
  readonly context: Context | undefined;
  readonly branch: string | undefined;
  readonly title?: string;
  readonly description?: string;
  readonly image?: string;
  readonly video?: string;
  readonly button?: {
    label: string;
    url: string;
    target: "self" | "blank";
  };
  readonly repeatable?: boolean;
  readonly redirectUrl?: string;
  readonly getRedirect?: () => string | undefined;
}`} symbols={{
  "Context": "/runner/api/library/classes/Context/"
}} />

## 🗃️ Properties {#properties}

---
### 🏷️ `branch` {#branch}
Contains the branch identifier of the epilogue. This property is `undefined` if the epilogue is not set for a certain branch. This is the case when the [root epilogue](../../../../builder/api/interfaces/IDefinition.mdx#epilogue) is supplied.
#### Type
string | undefined

---
### 🏷️ `button` {#button}
Contains an optional button with a hyperlink to show.
#### Type
```ts
{
  /* Specifies the label for the button. */
  label: string;

  /* Specifies the URL for the button. */
  url: string;

  /* Specifies if the button should open a new window/tab. */
  target: "self" | "blank";
}
```

---
### 🏷️ `context` {#context}
Contains the [`Context`](../classes/Context.mdx) instance for the epilogue.
#### Type
[`Context`](../classes/Context.mdx) | undefined

---
### 🏷️ `description` {#description}
Contains the description (supports markdown).
#### Type
string

---
### 🏷️ `image` {#image}
Contains the URL of an image to show.
#### Type
string

:::tip
It is possible to embed images using [Base64 encoding](https://www.base64-image.de/).
:::

---
### 🏷️ `redirectUrl` {#redirectUrl}
Contains an URL to redirect to when the form ends.
#### Type
string

:::warning
When this property is used, all other properties are ignored.
:::

---
### 🏷️ `repeatable` {#repeatable}
Contains if the form is repeatable. If set to `true` a button will be shown to start a new form session.
#### Type
boolean

---
### 🏷️ `title` {#title}
Contains the title (supports markdown).
#### Type
string

---
### 🏷️ `video` {#video}
Contains the URL of a video to show. Currently, [YouTube](https://www.youtube.com/) and [Vimeo](https://vimeo.com/) are supported. The shareable URL of the video needs to be supplied.
#### Type
string

## ▶️ Functions {#functions}

---
### 🔧 `getRedirect` {#getRedirect}
Redirect helper function that is useful for retrieving the actual URL to redirect to when a [`redirectUrl`](#redirectUrl) is set. Any variables used in the redirect URL are processed and the function returns a valid URL or `undefined` if the URL is invalid.
#### Signature
```ts
() => string | undefined
```
#### Return value
Returns a valid URL or `undefined` if the URL is invalid.
