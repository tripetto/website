---
title: IObservableNode interface - Runner
sidebar_label: IObservableNode
description: Interface that describes the observable nodes with in Moment (Runner library).
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# IObservableNode interface

## 📖 Description {#description}
Interface that describes the observable nodes with in [`Moment`](../classes/Moment.mdx).

## 📃 Type declaration {#type-declaration}
<InterfaceDeclaration prefix="#" src={`interface IObservableNode {
  readonly id: string;
  readonly key: string;
  readonly node: Node;
  readonly props: INode;
  readonly block: NodeBlock | undefined;
  readonly context: Context;
  readonly collectsData: boolean;
  readonly hasDataCollected: boolean;
  readonly enumerator: number | undefined;
  readonly validation: "unknown" | "pass" | "fail";
  readonly isFailed: boolean;
  readonly isPassed: boolean;
  readonly observer: Await | undefined;
  readonly hasChanged: (bReset?: boolean) => boolean;
}`} symbols={{
  "NodeBlock": "/runner/api/library/classes/NodeBlock/",
  "Context": "/runner/api/library/classes/Context/",
  "Node": "/runner/api/library/classes/Node/",
  "Await": "/runner/api/library/classes/Await/",
  "INode": "/builder/api/interfaces/INode/"
}} />

## 🗃️ Properties {#properties}

---
### 🏷️ `block` {#block}
Reference to the node block instance.
:::info
When no block is attached to the node, this property is `undefined`.
:::
#### Type
[`NodeBlock`](../classes/NodeBlock.mdx) | undefined

---
### 🏷️ `collectsData` {#collectsData}
Specifies if the node collects data.
#### Type
boolean

---
### 🏷️ `context` {#context}
Reference to the context.
#### Type
[`Context`](../classes/Context.mdx)

---
### 🏷️ `enumerator` {#enumerator}
Contains the enumerator for the node. Enumerators are only granted to nodes that have a block that collects data.
#### Type
number | undefined

---
### 🏷️ `hasDataCollected` {#hasDataCollected}
Specifies if the node has collected data.
#### Type
boolean

---
### 🏷️ `id` {#id}
Contains a unique identifier for the node.
#### Type
string

---
### 🏷️ `isFailed` {#isFailed}
Retrieves if the validation of the node failed.
#### Type
boolean

---
### 🏷️ `isPassed` {#isPassed}
Retrieves if the validation of the node passed.
#### Type
boolean

---
### 🏷️ `key` {#key}
Contains a unique key for the node within the current context (the key is prefixed with an underscore).
#### Type
string

---
### 🏷️ `node` {#node}
Reference to the node.
#### Type
[`Node`](../classes/Node.mdx)

---
### 🏷️ `observer` {#observer}
Reference to the node observer. Can be used to trigger a validation cycle for node.
#### Type
[`Await`](../classes/Await.mdx)

---
### 🏷️ `props` {#props}
Reference to the node properties.
#### Type
[`INode`](../../../../builder/api/interfaces/INode.mdx)

---
### 🏷️ `validation` {#validation}
Retrieves the validation state of the node. It can be one of the following values:
- `unknown`: There is no validation state available yet;
- `pass`: Validation succeeded;
- `fail`: Validation failed.
#### Type
"unknown" | "pass" | "fail"

## ▶️ Functions {#functions}

---
### 🔧 `hasChanged` {#hasChanged}
Function to retrieve if the node has changed and needs to be rerendered. This reads out a flag that is set to `true` whenever a node changes. The function accepts an optional `reset` argument. When `true` is supplied to that argument, the flag will reset to `false`. Any subsequent call to this function will then return `false` until another change is made to the form.
#### Signature
```ts
(reset?: boolean) => boolean
```
#### Parameters
| Name    | Type    | Optional | Description                                                     |
|:--------|:--------|:---------|:----------------------------------------------------------------|
| `reset` | boolean | Yes      | Specifies if the change flag should reset (default is `false`). |
#### Return value
Returns `true` if the node has changed.
