---
title: AES module - Runner
sidebar_label: AES
sidebar_position: 1
description: The AES module contains AES-based encryption and decryption functionality (Runner library).
---

# AES module

## 📖 Description {#description}
The `AES` module contains [AES](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard)-based encryption and decryption functionality.

:::info Rationale
The functions in this module are primitive and probably widely available in many other open-source packages on [npm](https://npmjs.com). So why are they included in the Tripetto Runner library? That's because the Tripetto Runner library doesn't have any dependency on other npm packages. So all the code needed to run a Tripetto form is included and these functions are simply required. Since they are part of the package, they are exported so you can use them too. For example when building custom blocks.
:::

## ▶️ Functions {#functions}

---
### 🔧 `decrypt` {#decrypt}
Decrypt [Base64-encoded](https://developer.mozilla.org/en-US/docs/Glossary/Base64) AES-encrypted data using [AES](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard) in [counter mode](https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation#Counter_(CTR)).
#### Signature
```ts
decrypt(
  data: string,
  password: string,
  bits: 128 | 192 | 256,
  atob: (input: string) => string
): string
```
#### Parameters
| Name       | Type                      | Optional | Description                                                                                                                                         |
|:-----------|:--------------------------|:---------|:----------------------------------------------------------------------------------------------------------------------------------------------------|
| `data`     | string                    | No       | Specifies the data to decrypt.                                                                                                                      |
| `password` | string                    | No       | Specifies the password used to generate the decryption key.                                                                                         |
| `bits`     | 128 \| 192 \| 256         | No       | Specifies the number of bits for the decryption key.                                                                                                |
| `atob`     | (input: string) => string | No       | Specifies the function used to decode the Base64-encoded input data (for example, [`atob`](https://developer.mozilla.org/en-US/docs/Web/API/atob)). |
#### Return value
Returns the decrypted string data.
#### Example
```ts showLineNumbers
import { AES } from "@tripetto/runner";

// This example uses the standard `atob` function available in browsers (see https://developer.mozilla.org/en-US/docs/Web/API/atob)
const decryptedData = AES.decrypt("cgGot0H7OmKO2Wh741MKBbTnJg==", "secret password", 256, atob);

// Outputs `secret data` to the console
console.log(decryptedData);
```

---
### 🔧 `encrypt` {#encrypt}
Encrypt string data using [AES](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard) encryption in [counter mode](https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation#Counter_(CTR)) and returns the enrypted data as a string in [Base64-encoded ASCII](https://developer.mozilla.org/en-US/docs/Glossary/Base64) format.
#### Signature
```ts
encrypt(
  data: string,
  password: string,
  bits: 128 | 192 | 256,
  btoa: (input: string) => string
): string
```
#### Parameters
| Name       | Type                      | Optional | Description                                                                                                                                                                     |
|:-----------|:--------------------------|:---------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `data`     | string                    | No       | Specifies the data to encrypt.                                                                                                                                                  |
| `password` | string                    | No       | Specifies the password used to generate the encryption key.                                                                                                                     |
| `bits`     | 128 \| 192 \| 256         | No       | Specifies the number of bits for the encryption key.                                                                                                                            |
| `btoa`     | (input: string) => string | No       | Specifies the function used to generate the Base64-encoded ASCII string with the encrypted data (for example, [`btoa`](https://developer.mozilla.org/en-US/docs/Web/API/btoa)). |
#### Return value
Returns the encrypted data as a string in [Base64-encoded ASCII](https://developer.mozilla.org/en-US/docs/Glossary/Base64) format.
#### Example
```ts showLineNumbers
import { AES } from "@tripetto/runner";

// This example uses the standard `btoa` function available in browsers (see https://developer.mozilla.org/en-US/docs/Web/API/btoa)
const encryptedData = AES.encrypt("secret data", "secret password", 256, btoa);

// Outputs `cgGot0H7OmKO2Wh741MKBbTnJg==` to the console
console.log(encryptedData);
```
