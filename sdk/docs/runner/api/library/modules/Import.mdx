---
title: Import module - Runner
sidebar_label: Import
sidebar_position: 5
toc_max_heading_level: 4
description: The Import module contains helper functions for importing data into forms (Runner library).
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# Import module

## 📖 Description {#description}
The `Import` module contains helper functions for importing data into forms. This is useful for prefilling forms.

:::tip
Read the [Prefilling forms guide](../../../stock/guides/prefilling.mdx) for more information.
:::

## 📇 Functions overview {#overview}
The following table shows the available data import functions and a description of what they do:

| Function            | Description                                                                                          |
|:--------------------|:-----------------------------------------------------------------------------------------------------|
| [`fields`](#fields) | Imports data into the form fields. **This is the de facto function for importing data into a form.** |
| [`CSV`](#CSV)       | Import data into a form using CSV input.                                                             |
| [`values`](#values) | Import raw data values into the form.                                                                |

## 👩‍💻 Example {#example}
```ts showLineNumbers
import { run } from "@tripetto/runner-autoscroll";
//highlight-start
import { Import } from "@tripetto/runner";
//highlight-end

run({
  definition: /* Supply your form definition here */,
  //highlight-start
  onImport: (instance) => {
    // This example assumes the form has two text fields with
    // the data identifiers (aliases) `FIRSTNAME` and `LASTNAME`.
    Import.fields(instance, [
      {
        name: "FIRSTNAME",
        value: "John",
      },
      {
        name: "LASTNAME",
        value: "Doe",
      }
    ]);

    // Or use the generated key of each field
    Import.fields(instance, [
      {
        key: "9ef64ab160ef6096ceecb4561f3eecc37007b669337929393a9d3789fa744f3e",
        value: "John",
      },
      {
        key: "e14599867b9fd518459e7ecaf974d33f84f2c3b322f644e838f948c2136eec3a",
        value: "Doe",
      }
    ]);
  }
  //highlight-end
});
```

## ▶️ Functions {#functions}

---
### 🔧 `CSV` {#CSV}
Imports data values using a CSV record. The number of columns in the CSV record and their order should exactly match the number of fields and their order in the instance.
##### Signature
```ts
CSV(
  instanceOrContext: Instance | Context,
  record: string,
  selection?: string[],
  delimiter?: string
): boolean
```
##### Parameters
| Name                | Type                                                                         | Optional | Description                                                     |
|:--------------------|:-----------------------------------------------------------------------------|:---------|:----------------------------------------------------------------|
| `instanceOrContext` | [`Instance`](../classes/Instance.mdx) \| [`Context`](../classes/Context.mdx) | No       | Reference to the instance or context.                           |
| `record`            | string                                                                       | No       | Specifies the CSV record string to import.                      |
| `selection`         | string[]                                                                     | Yes      | Specifies the identifiers of the slots that should be imported. |
| `delimiter`         | string                                                                       | Yes      | Specifies the delimiter for the CSV input (default is `,`).    |
##### Return value
Returns `true` if the import succeeded or `false` when the import failed (probably because the column and field count are not equal).
:::tip
You can use the [`CSV`](Export.mdx#CSV) export function to retrieve a sample CSV record for a form definition.
:::

---
### 🔧 `fields` {#fields}
Import data fields to the supplied form instance. You can import the fields by referencing the unique key of the field or by specifying the name of the field.
##### Signature
```ts
fields(
  instanceOrContext: Instance | Context,
  fields: (IFieldByKey | IFieldByName)[],
  selection?: string[],
  separator?: string
): boolean
```
##### Parameters
| Name                | Type                                                                         | Optional | Description                                                                                                                        |
|:--------------------|:-----------------------------------------------------------------------------|:---------|:-----------------------------------------------------------------------------------------------------------------------------------|
| `instanceOrContext` | [`Instance`](../classes/Instance.mdx) \| [`Context`](../classes/Context.mdx) | No       | Reference to the instance or context.                                                                                              |
| `fields`            | ([`IFieldByKey`](#IFieldByKey) \| [`IFieldByName`](#IFieldByName))[]         | No       | Contains the fields to import. You can reference a field by its key or field name.                                                 |
| `selection`         | string[]                                                                     | Yes      | Specifies the identifiers of the slots that should be imported.                                                                    |
| `separator`         | string                                                                       | Yes      | Specifies the string that is used to separate subject and field names for slots that are part of an iteration (default is ` / `). |
##### Return value
Returns `true` if the import succeeded or `false` when there was an error and one or more of the specified fields could not be imported.
##### Example
```ts showLineNumbers
import { run } from "@tripetto/runner-autoscroll";
//highlight-start
import { Import } from "@tripetto/runner";
//highlight-end

run({
  definition: /* Supply your form definition here */,
  onImport: (instance) => {
  //highlight-start
    // This example assumes the form has two text fields with
    // the data identifiers (aliases) `FIRSTNAME` and `LASTNAME`.
    Import.fields(instance, [
      {
        name: "FIRSTNAME",
        value: "John",
      },
      {
        name: "LASTNAME",
        value: "Doe",
      }
    ]);

    // Or use the generated key of each field
    Import.fields(instance, [
      {
        key: "9ef64ab160ef6096ceecb4561f3eecc37007b669337929393a9d3789fa744f3e",
        value: "John",
      },
      {
        key: "e14599867b9fd518459e7ecaf974d33f84f2c3b322f644e838f948c2136eec3a",
        value: "Doe",
      }
    ]);
  //highlight-end
  }
});
```

---
### 🔧 `values` {#values}
Import data values to the instance.
##### Signature
```ts
values(instanceOrContext: Instance | Context, values: IValues, selection?: string[]): boolean
```
##### Parameters
| Name                | Type                                                                         | Optional | Description                                                     |
|:--------------------|:-----------------------------------------------------------------------------|:---------|:----------------------------------------------------------------|
| `instanceOrContext` | [`Instance`](../classes/Instance.mdx) \| [`Context`](../classes/Context.mdx) | No       | Reference to the instance or context.                           |
| `values`            | [`IValues`](#IValues)                                                        | No       | Specifies the values to import.                                 |
| `selection`         | string[]                                                                     | Yes      | Specifies the identifiers of the slots that should be imported. |
##### Return value
Returns `true` if the import succeeded or `false` when there was an error and one or more of the specified fields could not be imported.

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `IFieldByKey` {#IFieldByKey}
Describes the interface to import data using the [`fields`](#fields) function where the fields are referenced by their keys (unique identifiers).
##### Type declaration
<InterfaceDeclaration prefix="#IFieldByKey-" src={`interface IFieldByKey {
  key: string;
  value: any;
  reference?: string;
  time?: number;
}`} />

---
#### 🏷️ `key` {#IFieldByKey-key}
Key of the field.
##### Type
string

---
#### 🏷️ `reference` {#IFieldByKey-reference}
Contains the data reference.
##### Type
string

---
#### 🏷️ `time` {#IFieldByKey-time}
Contains the UTC set time of the data.
##### Type
number

---
#### 🏷️ `value` {#IFieldByKey-value}
Contains the data value.
##### Type
any

---
### 🔗 `IFieldByName` {#IFieldByName}
Describes the interface to import data using the [`fields`](#fields) function where the fields are referenced by their names.
##### Type declaration
<InterfaceDeclaration prefix="#IFieldByName-" src={`interface IFieldByName {
  name: string;
  value: any;
  reference?: string;
  time?: number;
}`} />

---
#### 🏷️ `name` {#IFieldByName-name}
Contains the name of the field.
##### Type
string

---
#### 🏷️ `reference` {#IFieldByName-reference}
Contains the data reference.
##### Type
string

---
#### 🏷️ `time` {#IFieldByName-time}
Contains the UTC set time of the data.
##### Type
number

---
#### 🏷️ `value` {#IFieldByName-value}
Contains the data value.
##### Type
any

---
### 🔗 `IValues` {#IValues}
Describes the interface to import data using the [`values`](#values) function.
##### Type declaration
```ts
interface IValues {
  /* Specifies the slot identifier. */
  [slot: string]: {
    values: {
      /*
       * The keys for the contextual values are composed using the condition hashes of the
       * context. If the context is global the key of that value is `*`. If the
       * context is defined by a single condition, the key is the computed
       * hash of the stringified condition data (where the `id` property is set to
       * an empty `string`). If the context is defined by 2 or more conditions, the
       * computed hashes of these conditions are used to calculate a new hash. The
       * computed condition hashes are concatenated in chronological order and then a
       * `SHA2_256` hash is generated for this concatenated string. This hash is
       * used as key.
       */
      [context: string]: {
        /* Contains the value to import. */
        value: any;

        /* Contains the optional reference. */
        reference?: string;

        /* Contains the optional UTC set time of the value. */
        time?: number;
      };
    };
  };
}
```

:::info
This interface is basically a partial implementation of the [`IValues`](Export.mdx#IValues) interface in the [export](Export.mdx) module. This means you can use the object returned by the [`values`](Export.mdx#values) export function as input for the [`values`](#values) import function.
:::
