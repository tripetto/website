---
title: L10n Namespace class - Runner
sidebar_label: Namespace
description: The Namespace class holds the translations and locale information for a single namespace identifier (Runner library).
---

# Namespace class

## 📖 Description {#description}
The `Namespace` class holds the translations and locale information for a single namespace identifier. The stock runners all create a namespace with an unique identifier to hold the translations and locale information for each runner. It allows the use of multiple runners in the same application with different localization settings.

## 📌 Statics {#statics}

---
### 🏷️ `global` {#static-global}
Retrieves the global namespace instance. This namespace is always available and can be used if it is not necessary to create separate namespaces.
:::info
The [global translation functions](index.mdx#functions) use this global namespace.
:::
#### Type
`Namespace`
#### Example
```ts showLineNumbers
import { L10n } from "@tripetto/runner";

// Import the dutch locale and translations
import locale from "@tripetto/runner-autoscroll/runner/locales/nl.json";
import translations from "@tripetto/runner-autoscroll/runner/translations/nl.json";

// Load the locale and translation into the global namespace
L10n.Namespace.global.locale.load(locale);
L10n.Namespace.global.load(translations);
```

---
### 🔧 `create` {#static-create}
Creates a new `Namespace` instance with the specified identifier.
#### Signature
```ts
create(identifier: string): Namespace
```
#### Parameters
| Name         | Type   | Optional | Description                         |
|:-------------|:-------|:---------|:------------------------------------|
| `identifier` | string | No       | Specifies the namespace identifier. |
#### Return value
Returns the `Namespace`instance for the specified identifier.

---
### 🔧 `translation` {#static-translation}
Retrieves the translation `Namespace` instance for the specified identifier.
#### Signature
```ts
translation(identifier: string): Namespace
```
#### Parameters
| Name         | Type   | Optional | Description                         |
|:-------------|:-------|:---------|:------------------------------------|
| `identifier` | string | No       | Specifies the namespace identifier. |
#### Return value
Returns the `Namespace` instance for the specified identifier.

## 🗃️ Fields {#fields}

---
### 🏷️ `current` {#current}
Retrieves or sets the current translation domain (this is the ISO 639-1 language code of the domain).
#### Type
string

---
### 🏷️ `default` {#default}
Retrieves or sets the default translation domain (defaults to `en`).
#### Type
string

---
### 🏷️ `domains` {#domains}
Retrieves a list of all available translation domains.
#### Type
string[]

---
### 🏷️ `locale` {#locale}
Retrieves a reference to the [`Locales`](Locales.mdx) instance that holds the locale information.
#### Type
[`Locales`](Locales.mdx)

## ▶️ Methods {#methods}

---
### 🔧 `_` {#_}
Translates a message (short for [`gettext`](#gettext)).
#### Signature
```ts
_(message: string, ...arguments: string[]): string
```
#### Parameters
| Name        | Type     | Optional | Description                                                                                                                                                              |
|:------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `message`   | string   | No       | Specifies the message to translate.                                                                                                                                      |
| `arguments` | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The first argument is referenced with `%1`. |
#### Return value
Returns the translated message.

---
### 🔧 `_n` {#_n}
Translates a plural message (short for [`ngettext`](#ngettext)).
#### Signature
```ts
_n(message: string, messagePlural: string, count: number, ...arguments: string[]): string
```
#### Parameters
| Name            | Type     | Optional | Description                                                                                                                                                                                            |
|:----------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `message`       | string   | No       | Specifies the message to translate.                                                                                                                                                                    |
| `messagePlural` | string   | No       | Specifies the plural message to translate.                                                                                                                                                             |
| `count`         | number   | No       | Specifies the count for the plural (can be reference in the message with `%1`).                                                                                                                        |
| `arguments`     | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The `count` value is automatically included as the first argument (`%1`). |
#### Return value
Returns the translated message.

---
### 🔧 `dgettext` {#dgettext}
Translates a message using the specified translation domain.
#### Signature
```ts
dgettext(domain: string, message: string, ...arguments: string[]): string
```
#### Parameters
| Name        | Type     | Optional | Description                                                                                                                                                              |
|:------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `domain`    | string   | No       | Specifies the translation domain to use.                                                                                                                                 |
| `message`   | string   | No       | Specifies the message to translate.                                                                                                                                      |
| `arguments` | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The first argument is referenced with `%1`. |
#### Return value
Returns the translated message.

---
### 🔧 `dngettext` {#dngettext}
Translates a plural message using the specified translation domain.
#### Signature
```ts
dngettext(
  domain: string,
  message: string,
  messagePlural: string,
  count: number,
  ...arguments: string[]
): string
```
#### Parameters
| Name            | Type     | Optional | Description                                                                                                                                                                                            |
|:----------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `domain`        | string   | No       | Specifies the translation domain to use.                                                                                                                                                               |
| `message`       | string   | No       | Specifies the message to translate.                                                                                                                                                                    |
| `messagePlural` | string   | No       | Specifies the plural message to translate.                                                                                                                                                             |
| `count`         | number   | No       | Specifies the count for the plural (can be reference in the message with `%1`).                                                                                                                        |
| `arguments`     | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The `count` value is automatically included as the first argument (`%1`). |
#### Return value
Returns the translated message.

---
### 🔧 `dnpgettext` {#dnpgettext}
Translates a plural message with the specified context using the specified translation domain.
#### Signature
```ts
dnpgettext(
  domain: string,
  context: string,
  message: string,
  messagePlural: string,
  count: number,
  ...arguments: string[]
): string
```
#### Parameters
| Name            | Type     | Optional | Description                                                                                                                                                                                            |
|:----------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `domain`        | string   | No       | Specifies the translation domain to use.                                                                                                                                                               |
| `context`       | string   | No       | Specifies the translation context.                                                                                                                                                                     |
| `message`       | string   | No       | Specifies the message to translate.                                                                                                                                                                    |
| `messagePlural` | string   | No       | Specifies the plural message to translate.                                                                                                                                                             |
| `count`         | number   | No       | Specifies the count for the plural (can be reference in the message with `%1`).                                                                                                                        |
| `arguments`     | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The `count` value is automatically included as the first argument (`%1`). |
#### Return value
Returns the translated message.

---
### 🔧 `dpgettext` {#dpgettext}
Translates a message with the specified context using the specified translation domain.
#### Signature
```ts
dpgettext(domain: string, context: string, message: string, ...arguments: string[]): string
```
#### Parameters
| Name        | Type     | Optional | Description                                                                                                                                                              |
|:------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `domain`    | string   | No       | Specifies the translation domain to use.                                                                                                                                 |
| `context`   | string   | No       | Specifies the translation context.                                                                                                                                       |
| `message`   | string   | No       | Specifies the message to translate.                                                                                                                                      |
| `arguments` | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The first argument is referenced with `%1`. |
#### Return value
Returns the translated message.

---
### 🔧 `gettext` {#gettext}
Translates a message.
#### Signature
```ts
gettext(message: string, ...arguments: string[]): string
```
#### Parameters
| Name        | Type     | Optional | Description                                                                                                                                                              |
|:------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `message`   | string   | No       | Specifies the message to translate.                                                                                                                                      |
| `arguments` | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The first argument is referenced with `%1`. |
#### Return value
Returns the translated message.

---
### 🔧 `isLoaded` {#isLoaded}
Verifies if the specified domain is loaded.
#### Signature
```ts
isLoaded(domain: string): void
```
#### Parameters
| Name     | Type   | Optional | Description                     |
|:---------|:-------|:---------|:--------------------------------|
| `domain` | string | No       | Specifies the domain to verify. |
#### Return value
Returns `true` if the translation domain is loaded.

---
### 🔧 `load` {#load}
Loads translation domains.
#### Signature
```ts
load(
  translation: TTranslation | TTranslation[],
  makeCurrent?: boolean | "only-when-no-domain",
  mode?: "enrich" | "overwrite" | "replace"
): boolean
```
#### Parameters
| Name          | Type                                                                                                                     | Optional | Description                                                                                   |
|:--------------|:-------------------------------------------------------------------------------------------------------------------------|:---------|:----------------------------------------------------------------------------------------------|
| `translation` | [`TTranslation`](../../modules/L10n/index.mdx#TTranslation) \| [`TTranslation[]`](../../modules/L10n/index.mdx#TTranslation) | No       | Specifies the translation to load.                                                            |
| `makeCurrent` | boolean \| "only-when-no-domain"                                                                                         | Yes      | Specifies if the translation domain should be selected as current domain.                     |
| `mode`        | "enrich" \| "overwrite" \| "replace"                                                                                     | Yes      | Specifies if the translation domain needs to be enriched, overwritten or completely replaced. |
#### Return value
Returns `true` if the translation domain(s) were successfully loaded.

---
### 🔧 `ngettext` {#ngettext}
Translates a plural message.
#### Signature
```ts
ngettext(
  message: string,
  messagePlural: string,
  count: number,
  ...arguments: string[]
): string
```
#### Parameters
| Name            | Type     | Optional | Description                                                                                                                                                                                            |
|:----------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `message`       | string   | No       | Specifies the message to translate.                                                                                                                                                                    |
| `messagePlural` | string   | No       | Specifies the plural message to translate.                                                                                                                                                             |
| `count`         | number   | No       | Specifies the count for the plural (can be reference in the message with `%1`).                                                                                                                        |
| `arguments`     | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The `count` value is automatically included as the first argument (`%1`). |
#### Return value
Returns the translated message.

---
### 🔧 `npgettext` {#npgettext}
Translates a plural message with the specified context.
#### Signature
```ts
npgettext(
  context: string,
  message: string,
  messagePlural: string,
  count: number,
  ...arguments: string[]
): string
```
#### Parameters
| Name            | Type     | Optional | Description                                                                                                                                                                                            |
|:----------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `context`       | string   | No       | Specifies the translation context.                                                                                                                                                                     |
| `message`       | string   | No       | Specifies the message to translate.                                                                                                                                                                    |
| `messagePlural` | string   | No       | Specifies the plural message to translate.                                                                                                                                                             |
| `count`         | number   | No       | Specifies the count for the plural (can be reference in the message with `%1`).                                                                                                                        |
| `arguments`     | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The `count` value is automatically included as the first argument (`%1`). |
#### Return value
Returns the translated message.

---
### 🔧 `pgettext` {#pgettext}
Translates a message with the specified context.
#### Signature
```ts
pgettext(context: string, message: string, ...arguments: string[]): string
```
#### Parameters
| Name        | Type     | Optional | Description                                                                                                                                                              |
|:------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `context`   | string   | No       | Specifies the translation context.                                                                                                                                       |
| `message`   | string   | No       | Specifies the message to translate.                                                                                                                                      |
| `arguments` | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The first argument is referenced with `%1`. |
#### Return value
Returns the translated message.

---
### 🔧 `reset` {#reset}
Resets all translations in the current domain and activates the default domain (or the supplied domain).
#### Signature
```ts
reset(domain?: string): void
```
#### Parameters
| Name     | Type   | Optional | Description                       |
|:---------|:-------|:---------|:----------------------------------|
| `domain` | string | Yes      | Specifies the domain to activate. |

---
### 🔧 `unload` {#unload}
Unloads translation domain(s).
#### Signature
```ts
unload(domain?: string): void
```
#### Parameters
| Name     | Type   | Optional | Description                                                          |
|:---------|:-------|:---------|:---------------------------------------------------------------------|
| `domain` | string | Yes      | Specifies the domain to unload. If omitted all domains are unloaded. |
