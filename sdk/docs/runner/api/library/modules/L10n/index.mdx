---
title: L10n module - Runner
sidebar_label: L10n
toc_max_heading_level: 4
description: The L10n module contains functions for localization (Runner library).
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# L10n module

## 📖 Description {#description}
The `L10n` module contains functions for localization. Tripetto uses the GNU [gettext](https://en.wikipedia.org/wiki/Gettext) system.

## 👩‍💻 Example {#example}
```ts showLineNumbers
import { L10n } from "@tripetto/runner";

// Translate message
L10n.gettext("Lorem ipsum dolor sit amet");

// Translate message with shorthand
L10n._("Lorem ipsum dolor sit amet");

// Translate message with arguments
L10n.gettext("Hello %1", "there");
// Shorthand
L10n._("Hello %1", "there");

// Translate plural message
L10n.ngettext("1 user", "%1 users", 2);
// Shorthand
L10n._n("1 user", "%1 users", 2);
```

## 💎 Classes {#classes}
- [`Locales`](Locales.mdx)
- [`Namespace`](Namespace.mdx)

## ▶️ Functions {#functions}
---
### 🔧 `_` {#_}
Translates a message (short for [`gettext`](#gettext)).
:::info
This function uses the global translation namespace ([`L10n.Namespace.global`](Namespace.mdx#static-global)) and is a shorthand for [`L10n.Namespace.global._`](Namespace.mdx#_).
:::
##### Signature
```ts
_(message: string, ...arguments: string[]): string
```
##### Parameters
| Name        | Type     | Optional | Description                                                                                                                                                              |
|:------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `message`   | string   | No       | Specifies the message to translate.                                                                                                                                      |
| `arguments` | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The first argument is referenced with `%1`. |
##### Return value
Returns the translated message.

---
### 🔧 `_n` {#_n}
Translates a plural message (short for [`ngettext`](#ngettext)).
:::info
This function uses the global translation namespace ([`L10n.Namespace.global`](Namespace.mdx#static-global)) and is a shorthand for [`L10n.Namespace.global._n`](Namespace.mdx#_n).
:::
##### Signature
```ts
_n(message: string, messagePlural: string, count: number, ...arguments: string[]): string
```
##### Parameters
| Name            | Type     | Optional | Description                                                                                                                                                                                            |
|:----------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `message`       | string   | No       | Specifies the message to translate.                                                                                                                                                                    |
| `messagePlural` | string   | No       | Specifies the plural message to translate.                                                                                                                                                             |
| `count`         | number   | No       | Specifies the count for the plural (can be reference in the message with `%1`).                                                                                                                        |
| `arguments`     | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The `count` value is automatically included as the first argument (`%1`). |
##### Return value
Returns the translated message.
##### Example
```ts showLineNumbers
import { L10n } from "@tripetto/runner";

// Outputs `1 car` to the console.
console.log(L10n._n("1 car", "%1 cars", 1));

// Outputs `2 cars` to the console.
console.log(L10n._n("1 car", "%1 cars", 2));
```

---
### 🔧 `dgettext` {#dgettext}
Translates a message using the specified translation domain.
:::info
This function uses the global translation namespace ([`L10n.Namespace.global`](Namespace.mdx#static-global)) and is a shorthand for [`L10n.Namespace.global.dgettext`](Namespace.mdx#dgettext).
:::
##### Signature
```ts
dgettext(domain: string, message: string, ...arguments: string[]): string
```
##### Parameters
| Name        | Type     | Optional | Description                                                                                                                                                              |
|:------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `domain`    | string   | No       | Specifies the translation domain to use.                                                                                                                                 |
| `message`   | string   | No       | Specifies the message to translate.                                                                                                                                      |
| `arguments` | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The first argument is referenced with `%1`. |
##### Return value
Returns the translated message.

---
### 🔧 `dngettext` {#dngettext}
Translates a plural message using the specified translation domain.
:::info
This function uses the global translation namespace ([`L10n.Namespace.global`](Namespace.mdx#static-global)) and is a shorthand for [`L10n.Namespace.global.dngettext`](Namespace.mdx#dngettext).
:::
##### Signature
```ts
dngettext(
  domain: string,
  message: string,
  messagePlural: string,
  count: number,
  ...arguments: string[]
): string
```
##### Parameters
| Name            | Type     | Optional | Description                                                                                                                                                                                            |
|:----------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `domain`        | string   | No       | Specifies the translation domain to use.                                                                                                                                                               |
| `message`       | string   | No       | Specifies the message to translate.                                                                                                                                                                    |
| `messagePlural` | string   | No       | Specifies the plural message to translate.                                                                                                                                                             |
| `count`         | number   | No       | Specifies the count for the plural (can be reference in the message with `%1`).                                                                                                                        |
| `arguments`     | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The `count` value is automatically included as the first argument (`%1`). |
##### Return value
Returns the translated message.

---
### 🔧 `dnpgettext` {#dnpgettext}
Translates a plural message with the specified context using the specified translation domain.
:::info
This function uses the global translation namespace ([`L10n.Namespace.global`](Namespace.mdx#static-global)) and is a shorthand for [`L10n.Namespace.global.dnpgettext`](Namespace.mdx#dnpgettext).
:::
##### Signature
```ts
dnpgettext(
  domain: string,
  context: string,
  message: string,
  messagePlural: string,
  count: number,
  ...arguments: string[]
): string
```
##### Parameters
| Name            | Type     | Optional | Description                                                                                                                                                                                            |
|:----------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `domain`        | string   | No       | Specifies the translation domain to use.                                                                                                                                                               |
| `context`       | string   | No       | Specifies the translation context.                                                                                                                                                                     |
| `message`       | string   | No       | Specifies the message to translate.                                                                                                                                                                    |
| `messagePlural` | string   | No       | Specifies the plural message to translate.                                                                                                                                                             |
| `count`         | number   | No       | Specifies the count for the plural (can be reference in the message with `%1`).                                                                                                                        |
| `arguments`     | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The `count` value is automatically included as the first argument (`%1`). |
##### Return value
Returns the translated message.

---
### 🔧 `dpgettext` {#dpgettext}
Translates a message with the specified context using the specified translation domain.
:::info
This function uses the global translation namespace ([`L10n.Namespace.global`](Namespace.mdx#static-global)) and is a shorthand for [`L10n.Namespace.global.dpgettext`](Namespace.mdx#dpgettext).
:::
##### Signature
```ts
dpgettext(domain: string, context: string, message: string, ...arguments: string[]): string
```
##### Parameters
| Name        | Type     | Optional | Description                                                                                                                                                              |
|:------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `domain`    | string   | No       | Specifies the translation domain to use.                                                                                                                                 |
| `context`   | string   | No       | Specifies the translation context.                                                                                                                                       |
| `message`   | string   | No       | Specifies the message to translate.                                                                                                                                      |
| `arguments` | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The first argument is referenced with `%1`. |
##### Return value
Returns the translated message.

---
### 🔧 `gettext` {#gettext}
Translates a message.
:::info
This function uses the global translation namespace ([`L10n.Namespace.global`](Namespace.mdx#static-global)) and is a shorthand for [`L10n.Namespace.global.gettext`](Namespace.mdx#gettext).
:::
##### Signature
```ts
gettext(message: string, ...arguments: string[]): string
```
##### Parameters
| Name        | Type     | Optional | Description                                                                                                                                                              |
|:------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `message`   | string   | No       | Specifies the message to translate.                                                                                                                                      |
| `arguments` | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The first argument is referenced with `%1`. |
##### Return value
Returns the translated message.

---
### 🔧 `ngettext` {#ngettext}
Translates a plural message.
:::info
This function uses the global translation namespace ([`L10n.Namespace.global`](Namespace.mdx#static-global)) and is a shorthand for [`L10n.Namespace.global.ngettext`](Namespace.mdx#ngettext).
:::
##### Signature
```ts
ngettext(
  message: string,
  messagePlural: string,
  count: number,
  ...arguments: string[]
): string
```
##### Parameters
| Name            | Type     | Optional | Description                                                                                                                                                                                            |
|:----------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `message`       | string   | No       | Specifies the message to translate.                                                                                                                                                                    |
| `messagePlural` | string   | No       | Specifies the plural message to translate.                                                                                                                                                             |
| `count`         | number   | No       | Specifies the count for the plural (can be reference in the message with `%1`).                                                                                                                        |
| `arguments`     | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The `count` value is automatically included as the first argument (`%1`). |
##### Return value
Returns the translated message.
##### Example
```ts showLineNumbers
import { L10n } from "@tripetto/runner";

// Outputs `1 car` to the console.
console.log(L10n.ngettext("1 car", "%1 cars", 1));

// Outputs `2 cars` to the console.
console.log(L10n.ngettext("1 car", "%1 cars", 2));
```

---
### 🔧 `npgettext` {#npgettext}
Translates a plural message with the specified context.
:::info
This function uses the global translation namespace ([`L10n.Namespace.global`](Namespace.mdx#static-global)) and is a shorthand for [`L10n.Namespace.global.npgettext`](Namespace.mdx#npgettext).
:::
##### Signature
```ts
npgettext(
  context: string,
  message: string,
  messagePlural: string,
  count: number,
  ...arguments: string[]
): string
```
##### Parameters
| Name            | Type     | Optional | Description                                                                                                                                                                                            |
|:----------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `context`       | string   | No       | Specifies the translation context.                                                                                                                                                                     |
| `message`       | string   | No       | Specifies the message to translate.                                                                                                                                                                    |
| `messagePlural` | string   | No       | Specifies the plural message to translate.                                                                                                                                                             |
| `count`         | number   | No       | Specifies the count for the plural (can be reference in the message with `%1`).                                                                                                                        |
| `arguments`     | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The `count` value is automatically included as the first argument (`%1`). |
##### Return value
Returns the translated message.

---
### 🔧 `pgettext` {#pgettext}
Translates a message with the specified context.
:::info
This function uses the global translation namespace ([`L10n.Namespace.global`](Namespace.mdx#static-global)) and is a shorthand for [`L10n.Namespace.global.pgettext`](Namespace.mdx#pgettext).
:::
##### Signature
```ts
pgettext(context: string, message: string, ...arguments: string[]): string
```
##### Parameters
| Name        | Type     | Optional | Description                                                                                                                                                              |
|:------------|:---------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `context`   | string   | No       | Specifies the translation context.                                                                                                                                       |
| `message`   | string   | No       | Specifies the message to translate.                                                                                                                                      |
| `arguments` | string[] | Yes      | Optional string arguments which can be referenced in the message using the percent sign followed by the argument index `%n`. The first argument is referenced with `%1`. |
##### Return value
Returns the translated message.

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `IDomain` {#IDomain}
Describes the interface for the domain object.
##### Type declaration
<InterfaceDeclaration prefix="#IDomain-" src={`interface IDomain {
  readonly locale: string;
  readonly language: string;
  readonly native: string;
}`} />

---
#### 🏷️ `locale` {#IDomain-locale}
Specifies the locale (`language[_territory]`).
##### Type
string

---
#### 🏷️ `language` {#IDomain-language}
Specifies the language in english.
##### Type
string

---
#### 🏷️ `native` {#IDomain-native}
Specifies the language in the native language.
##### Type
string

---
### 🔗 `ILocale` {#ILocale}
Describes the interface for the locale object. Locale information is stored in a JSON file per locale and are stored in the [runner/locales](https://unpkg.com/browse/@tripetto/runner-autoscroll/runner/locales/) folder of each stock runner package.
##### Type declaration
```ts
interface ILocale {
  locale: string;
  domain: string;
  direction: "ltr" | "rtl";
  countryCode: string;
  country: string;
  countryNative: string;
  language: string;
  languageNative: string;
  translations: {
    months: {
      formatted: {
        abbreviated: string[];
        narrow: string[];
        wide: string[];
      };
      nominative: {
        abbreviated: string[];
        narrow: string[];
        wide: string[];
      };
    };
    days: {
      formatted: {
        abbreviated: string[];
        narrow: string[];
        short: string[];
        wide: string[];
      };
      nominative: {
        abbreviated: string[];
        narrow: string[];
        short: string[];
        wide: string[];
      };
    };
    time: {
      AM: string;
      PM: string;
    };
  };
  formats: {
    date: {
      full: string;
      long: string;
      medium: string;
      short: string;
    };
    time: {
      full: string;
      long: string;
      medium: string;
      short: string;
    };
    dateTime: {
      full: string;
      long: string;
      medium: string;
      short: string;
    };
    numbers: {
      decimals: string;
      grouping: string;
      minus: string;
    };
  };
}
```

## 📜 Types {#types}

---
### 📃 `TTranslation` {#TTranslation}
Defines a translation.
##### Type
```ts
{
  "": {
    language?: string;
    "plural-forms"?: string;
    "plural-family"?: string;
  };
} & {
  [id: string]: [null | string, ...string[]] | [null | string, [string]];
}
```
