---
title: Namespaces module - Runner
sidebar_label: Namespaces
sidebar_position: 7
description: The Namespaces class is used to load runner block bundles (Runner library).
---

# Namespaces module

## 📖 Description {#description}
The `Namespaces` class is used to load runner block bundles.

## ▶️ Functions {#functions}

---
### 🔧 `get` {#get}
Retrieves the namespace for the specified identifier (or the default namespace if the identifier is omitted).
#### Signature
```ts
get(identifier?: string): INamespace
```
#### Parameters
| Name         | Type   | Optional | Description                                            |
|:-------------|:-------|:---------|:-------------------------------------------------------|
| `identifier` | string | No       | Specifies the identifier of the namespace to retrieve. |
#### Return value
Returns the [`INamespace`](../interfaces/INamespace.mdx) object that holds the blocks for the namespace.

---
### 🔧 `isAvailable` {#isAvailable}
Retrieves if a namespace exists for the specified identifier.
#### Signature
```ts
isAvailable(identifier?: string): boolean
```
#### Parameters
| Name         | Type   | Optional | Description                                         |
|:-------------|:-------|:---------|:----------------------------------------------------|
| `identifier` | string | No       | Specifies the identifier of the namespace to check. |
#### Return value
Returns `true` if the namespace exists.

---
### 🔧 `loadUMD` {#loadUMD}
Loads a block bundle using UMD code.
:::danger
Loading UMD code from a string is unsafe! Only use when the source of the UMD code is trusted. When you are using a [Content Security Policy (CSP)](https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP) configuration, make sure `script-src 'unsafe-eval'` is allowed.
:::
#### Signature
```ts
loadUMD(umd: string): boolean
```
#### Parameters
| Name  | Type   | Optional | Description                     |
|:------|:-------|:---------|:--------------------------------|
| `umd` | string | No       | Specifies the UMD code to load. |
#### Return value
Returns `true` if the UMD code was successfully loaded.

---
### 🔧 `loadURL` {#loadURL}
Loads a block bundle from an URL.
:::info
The UMD bundle is loaded using [trusted-types](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/trusted-types) with a policy named `tripetto#runner`. Please read the [Content Security Policy guide](../../../../builder/integrate/guides/csp.mdx) for more information.
:::
#### Signature
```ts
loadURL(url: string, done?: (succeeded: boolean) => void): void
```
#### Parameters
| Name   | Type                         | Optional | Description                                                      |
|:-------|:-----------------------------|:---------|:-----------------------------------------------------------------|
| `url`  | string                       | No       | Specifies the URL of the UMD block bundle to load.               |
| `done` | (succeeded: boolean) => void | Yes      | Callback function that is invoked when the loading is completed. |

---
### 🔧 `mount` {#mount}
Mounts (activates) a namespace context. All blocks that self-register when this namespace is mounted will become part of the namespace.
:::tip
You can also use the shorthand [`mountNamespace`](../functions/mountNamespace.mdx) function.
:::
#### Signature
```ts
mount(identifier?: string): void
```
#### Parameters
| Name         | Type   | Optional | Description                                         |
|:-------------|:-------|:---------|:----------------------------------------------------|
| `identifier` | string | No       | Specifies the identifier of the namespace to mount. |

---
### 🔧 `unload` {#unload}
Unloads the specified namespace.
#### Signature
```ts
unload(identifier?: string): boolean
```
#### Parameters
| Name         | Type   | Optional | Description                                          |
|:-------------|:-------|:---------|:-----------------------------------------------------|
| `identifier` | string | No       | Specifies the identifier of the namespace to unload. |
#### Return value
Returns `true` when the namespace is unloaded.

---
### 🔧 `unmount` {#unmount}
Unmounts (deactivates) the active namespace context.
:::tip
You can also use the shorthand [`unmountNamespace`](../functions/unmountNamespace.mdx) function.
:::
#### Signature
```ts
unmount(): void
```
