---
title: Slots module - Runner
sidebar_label: Slots
toc_max_heading_level: 4
description: Slots are an elementary part of Tripetto's data collection mechanism (Runner library).
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# Slots module

## 📖 Description {#description}
Slots are an elementary part of Tripetto's data collection mechanism. They are defined by the [form definition](../../../../../builder/api/interfaces/IDefinition.mdx) and denote the data that a form can collect. By default, there are slot types available to collect primitive data types like [strings](String.mdx), [numbers](Number.mdx), [booleans](Boolean.mdx), and [more](#built-in-slots). But it is also possible to define your own slot types for advanced data collection using the [`Slot`](Slot.mdx) base class and the [`@slot`](#slot-decorator) decorator (see [example](#example) below).

There are four different kinds of slots:
- [`static`](SlotsClass.mdx#static): Static slots are fixed slots that are required for the functional operation of a block (for example, a string slot that holds the text value of a text input block);
- [`dynamic`](SlotsClass.mdx#dynamic): Dynamic slots are used for dynamic elements in a block (for example, slots that are created for each checkbox of a block that supports a customizable list of checkboxes);
- [`feature`](SlotsClass.mdx#feature): Feature slots are used for optional features of a block (for example, a slot that holds the score value of a block that supports an optional score feature);
- [`meta`](SlotsClass.mdx#meta): Meta slots are used to store metadata (for example, the time taken to answer a block).

:::tip
See the [Slots guide](../../../../../blocks/custom/guides/slots.mdx) for more information about slots.
:::

## 📦 Built-in slot types {#built-in-slots}
The following slot types are always available:
- [`String`](String.mdx)
- [`Number`](Number.mdx)
- [`Boolean`](Boolean.mdx)
- [`Date`](Date.mdx)
- [`Text`](Text.mdx)
- [`Numeric`](Numeric.mdx)

## 👩‍💻 Example {#example}
You can define your own slot types for advanced data collection using this [`Slot`](Slot.mdx) base class and the [`@slot`](#slot-decorator) decorator:
```ts showLineNumbers
import { Slots, castToNumber } from "@tripetto/runner";

// Let's define a new slot type to store a square metre unit that automatically appends the unit symbol m².
@Slots.slot("square-metre")
class SquareMetre extends Slots.Slot<number> {
  // Converts the pristine data to the correct type (in this case number).
  public toValue(data): number {
    return castToNumber(data);
  }

  // Retrieves the string representation of the value (in this case with the unit symbol m² added).
  public toString(data): string {
    return `${this.toValue(data)} m²`;
  }
}
```

## 💎 Classes {#classes}
- [`Boolean`](Boolean.mdx)
- [`Date`](Date.mdx)
- [`Number`](Number.mdx)
- [`Numeric`](Numeric.mdx)
- [`Slot`](Slot.mdx)
- [`Slots`](SlotsClass.mdx)
- [`String`](String.mdx)
- [`Text`](Text.mdx)

## 🎀 Decorators {#decorators}

---
### 🎀 `@deserialize` {#deserialize-decorator}
Marks a class property as a deserializable property. This means the property can be deserialized automatically from the [form definition](../../../../../builder/api/interfaces/IDefinition.mdx).
##### Decorator type
Property [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#property-decorators)
##### Applies to
- [`Slot`](Slot.mdx)
##### Decorator signature
```ts
@serialize
```
:::info
If you don't want automatic deserialization, you need to override the [`deserialize`](Slot.mdx#deserialize) method in your slot class.
:::

---
### 🎀 `@property` {#property-decorator}
Marks a class property as a slot property. Slot properties are stored in the [form definition](../../../../../builder/api/interfaces/IDefinition.mdx) and are monitored by the builder. Whenever the value of a slot property changes, it kicks off a [form definition](../../../../../builder/api/interfaces/IDefinition.mdx) change event in the builder. Use the [`@serialize`](#serialize-decorator) and [`@deserialize`](#deserialize-decorator) decorators to enable automatic serialization to and deserialization from the [form definition](../../../../../builder/api/interfaces/IDefinition.mdx).
##### Decorator type
Property [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#property-decorators)
##### Applies to
- [`Slot`](Slot.mdx)
##### Decorator signature
```ts
@property
```
##### Example
```ts showLineNumbers
import { Slots, castToNumber } from "@tripetto/runner";

// Let's define a new slot type to store a square metre unit that automatically appends the unit symbol m².
@Slots.slot("square-metre")
class SquareMetre extends Slots.Slot<number> {
  //highlight-start
  // Let's add a property to set a maximum number of square meters.
  @property
  @serialize // Enable automatic serialization to the form definition.
  @deserialize // Enable automatic deserialization from the form definition.
  public maximum?: number;
  //highlight-end

  // Converts the pristine data to the correct type (in this case number).
  public toValue(data): number {
    //highlight-start
    const metres = castToNumber(data);

    if (this.maximum && metres > this.maximum) {
        return this.maximum;
    }

    return metres;
    //highlight-end
  }

  // Retrieves the string representation of the value (in this case with the unit symbol m² added).
  public toString(data): string {
    return `${this.toValue(data)} m²`;
  }
}
```

---
### 🎀 `@serialize` {#serialize-decorator}
Marks a class property as a serializable property. This means the property can be serialized automatically to the [form definition](../../../../../builder/api/interfaces/IDefinition.mdx).
##### Decorator type
Property [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#property-decorators)
##### Applies to
- [`Slot`](Slot.mdx)
##### Decorator signature
```ts
@serialize
```
:::info
If you don't want automatic serialization, you need to override the [`serialize`](Slot.mdx#deserialize) method in your slot class.
:::

---
### 🎀 `@slot` {#slot-decorator}
Registers a new slot type.
##### Decorator type
Class [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#class-decorators)
##### Applies to
- [`Slot`](Slot.mdx)
##### Decorator signature
```ts
@slot(typeIdentifier: string)
```
##### Parameters
| Name             | Type   | Optional | Description                                |
|:-----------------|:-------|:---------|:-------------------------------------------|
| `typeIdentifier` | string | No       | Specifies the unique slot type identifier. |
:::caution
The supplied type identifier must be unique; otherwise, an error is thrown. The following identifiers are reserved by the built-in slot types and therefore not allowed: `string`, `number`, `boolean`, `date`, `text`, and `numeric`.
:::
##### Example
```ts showLineNumbers
import { Slots, castToNumber } from "@tripetto/runner";

// Let's define a new slot type to store a square metre unit that automatically appends the unit symbol m².
@Slots.slot("square-metre")
class SquareMetre extends Slots.Slot<number> {
  // Converts the pristine data to the correct type (in this case number).
  public toValue(data): number {
    return castToNumber(data);
  }

  // Retrieves the string representation of the value (in this case with the unit symbol m² added).
  public toString(data): string {
    return `${this.toValue(data)} m²`;
  }
}
```

## ⛓️ Interfaces {#interfaces}

---
### 🔗 `ISlotProperties` {#ISlotProperties}
Describes the interface for declaring new slots using one of the slot create methods ([`static`](SlotsClass.mdx#static), [`dynamic`](SlotsClass.mdx#dynamic), [`feature`](SlotsClass.mdx#feature), or [`meta`](SlotsClass.mdx#meta)).
##### Type declaration
<InterfaceDeclaration prefix="#ISlotProperties-" src={`interface ISlotProperties {
  type: typeof (String | Number | Boolean | Date | Text | Numeric);
  reference: string;
  label: string;
  sequence?: number;
  name?: string;
  placeholder?: string;
  alias?: string;
  required?: boolean;
  default?: any;
  exportable?: boolean;
  actionable?: boolean;
  protected?: boolean;
  pipeable?: boolean | {
    pipe?: string;
    label?: string;
    alias?: string;
    content?: "value" | "label" | "name" | {
      string: string;
      text?: string;
      markdown?: string;
    };
    legacy?: string;
  };
  exchange?: string[];
}`} symbols={{
  "String": "/runner/api/library/modules/Slots/String/",
  "Number": "/runner/api/library/modules/Slots/Number/",
  "Boolean": "/runner/api/library/modules/Slots/Boolean/",
  "Date": "/runner/api/library/modules/Slots/Date/",
  "Text": "/runner/api/library/modules/Slots/Text/",
  "Numeric": "/runner/api/library/modules/Slots/Numeric/",
}} />

---
#### 🏷️ `actionable` {#ISlotProperties-actionable}
Specifies if the slot is actionable. When set to `true` the slot will be included in the [`actionables`](../Export.mdx#actionables) export function.
:::info
When set to `true` the default value for the [`exportable`](#ISlotProperties-exportable) property will be `false`. If you want to make a slot both actionable and exportable, you should set both properties to `true`.
:::
:::tip
More information about actionable data can be found [here](../Export.mdx#exportable-vs-actionable).
:::
##### Type
boolean

---
#### 🏷️ `alias` {#ISlotProperties-alias}
Specifies the slot alias. This alias is used to identify the slot in the dataset of a form. The alias is, for example, used in the [`NVPs`](../Export.mdx#NVPs) and [`CSV`](../Export.mdx#CSV) export functions.
:::tip
When [prefilling](../../../../stock/guides/prefilling.mdx) (or importing data into) forms the alias can be used to easily specify the right data for the right slot.
:::
##### Type
string

---
#### 🏷️ `default` {#ISlotProperties-default}
Specifies the default value for a slot. This default value will be used when there is no explicit value set for a slot or when the slot value is cleared.
##### Type
any

---
#### 🏷️ `exchange` {#ISlotProperties-exchange}
The exchange property specifies the properties of the slot that are exchangeble with properties of another slot with the same kind and reference. This happens when a block is replaced with another (compatible) block. For example, the [text](../../../../../blocks/stock/text.mdx) and [textarea](../../../../../blocks/stock/textarea.mdx) blocks have the same slot structure. Both denote a slot with `value` as the [`reference`](#ISlotProperties-reference). When a text block is replaced with a textarea block, the slots are interchangeable. The `exchange` property specifies which slot properties are transferred to the slot of the newly selected block.
:::info
When a slot is entirely exchangeble, compatible blocks can replace eachother without leading to a change in the [data stencil](../../../../../builder/integrate/guides/stencil.mdx) hash. This means the dataset of the form remains the same.
:::
##### Type
(keyof T)[]
##### Example {#ISlotProperties-type-example}
```ts showLineNumbers
import { Slots, L10n } from "@tripetto/runner";

// Let's assume there is a variable slots
declare const slots: Slots.Slots;

// Create a new static slot
slots.static({
  type: Slots.Text,
  reference: "value",
  label: L10n.gettext("Text"),
  //highlight-start
  // All these properties can be transferred.
  // Only the properties that are used in both the old and the new slot are transferred.
  exchange: [
    "required",
    "alias",
    "exportable",
    "maxLength",
    "transformation",
  ],
  //highlight-end
});
```

---
#### 🏷️ `exportable` {#ISlotProperties-exportable}
Specifies if the slot is exportable. When set to `true`, the slot will be included in the [`exportables`](../Export.mdx#exportables) export function.
:::info
This property defaults to `true` when the `exportable` property is omitted and the [`actionable`](#ISlotProperties-actionable) property is either set to `false` or omitted as well.
:::
:::tip
More information about exportable data can be found [here](../Export.mdx#exportable-vs-actionable).
:::
##### Type
boolean

---
#### 🏷️ `label` {#ISlotProperties-label}
Specifies the (localized) label for the slot. This label is often a description of the kind of data the slot holds. For example, `Text` for a text input block. When a block has multiple slots, the label is used to distinguish the different slots. See an example [here](#ISlotProperties-type-example).
##### Type
string

---
#### 🏷️ `name` {#ISlotProperties-name}
Specifies the slot name.
:::tip
For [`dynamic`](SlotsClass.mdx#dynamic) slots, this name is often the same as the name of the item for which the slot is intended.
:::
##### Type
string

---
#### 🏷️ `pipeable` {#ISlotProperties-pipeable}
Specifies if the slot is pipeable. Piping is the process of recalling slot values in (markdown) text in the form. There are three possible values:
- `true`: Slot can be used as piping value (this is the default behavior);
- `false`: Slot cannot be used as piping value;
- Or a custom configuration to instruct Tripetto how to recall the slot.

:::info
To simply enable or disable piping for the slot (based on the slot value), supply a boolean value. If you need more control over the pipe, you can supply an object with a more specific configuration.
:::
##### Type
```ts
boolean | {
  /* Optional name for the pipe. This is used to group slot values that have the same pipe name. */
  pipe?: string;

  /* Optional localized label for the pipe. */
  label?: string;

  /* Optional alias for the pipe. */
  alias?: string;

  /*
   * Specifies the field or content that should be used as the data that goes
   * into the pipe. It can be one of the following values:
   * - `value`: Use the current string value of the slot (this is the default behavior);
   * - `label`: Use the slot label;
   * - `name`: Use the name of the slot;
   * - Custom configuration to supply the data that goes into the pipe.
   */
  content?: "value" | "label" | "name" | {
    /* Contains the content as a string without any markup or variables. */
    string: string;

    /* Contains the content as text with support for variables. */
    text?: string;

    /* Contains markdown content with support for basic formatting, hyperlinks, and variables. */
    markdown?: string;
  };

  /*
   * Specifies the name of a legacy pipe. Only here for backward compatibility. Do not use.
   * @deprecated
   */
  legacy?: string; 🗑️
}
```

---
#### 🏷️ `placeholder` {#ISlotProperties-placeholder}
Specifies the slot placeholder that can be used when a slot doesn't hold a value.
##### Type
string

---
#### 🏷️ `protected` {#ISlotProperties-protected}
Specifies whether the slot is write-protected and can only be changed by the block that created the slot. Other blocks in the form (like the [Setter block](../../../../../blocks/stock/setter.mdx)) cannot change the data of the slot.
##### Type
boolean

---
#### 🏷️ `reference` {#ISlotProperties-reference}
Specifies the slot reference. This is a unique reference to the slot within a block. You use the reference to retrieve a certain slot in the runner part of a block. See an example [here](#ISlotProperties-type-example).
##### Type
string

---
#### 🏷️ `required` {#ISlotProperties-required}
Specifies if the slot is required. When set to `true`, the block validation will only pass when a slot has a valid value.
##### Type
boolean
:::caution
The Runner library will automatically validate if all required slots have a valid value.
:::

---
#### 🏷️ `sequence` {#ISlotProperties-sequence}
Specifies the slot sequence number. Used to sort the slot order.
##### Type
number

---
#### 🏷️ `type` {#ISlotProperties-type}
Specifies the slot type. Can be one of the built-in slot types or a custom slot type (registered with the [`@slot`](#slot-decorator) decorator).
##### Type
[`typeof String`](String.mdx) | [`typeof Number`](Number.mdx) | [`typeof Boolean`](Boolean.mdx) | [`typeof Date`](Date.mdx) | [`typeof Text`](Text.mdx) | [`typeof Numeric`](Numeric.mdx)
##### Example {#ISlotProperties-type-example}
```ts showLineNumbers
import { Slots, L10n } from "@tripetto/runner";

// Let's assume there is a variable slots
declare const slots: Slots.Slots;

//highlight-start
// Create a new static slot
slots.static({
  type: Slots.String,
  reference: "text-value",
  label: L10n.gettext("Text value")
});
//highlight-end
```
