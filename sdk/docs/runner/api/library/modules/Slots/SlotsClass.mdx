---
id: Slots
title: Slots Slots class - Runner
sidebar_label: Slots
description: The Slots class defines a collection that holds all the slots for a block (Runner library).
---

# Slots class

## 📖 Description {#description}
The `Slots` class defines a collection that holds all the slots for a block. It is used in the builder part of a block implementation to denote the data slots for that block. There are four kinds of slots supported by the `Slots` class:
- [`static`](#static): Static slots are fixed slots that are required for the functional operation of a block (for example, a string slot that holds the text value of a text input block);
- [`dynamic`](#dynamic): Dynamic slots are used for dynamic elements in a block (for example, slots that are created for each checkbox of a block that supports a customizable list of checkboxes);
- [`feature`](#feature): Feature slots are used for optional features of a block (for example, a slot that holds the score value of a block that supports an optional score feature);
- [`meta`](#meta): Meta slots are used to store metadata (for example, the time taken to answer a block).

## 🗃️ Fields {#fields}

---
### 🏷️ `all` {#all}
Retrieves all the slots that are in the collection.
#### Type
[`Slot[]`](Slot.mdx)

---
### 🏷️ `count` {#count}
Retrieves the number of slots in the collection.
#### Type
number

---
### 🏷️ `firstItem` {#firstItem}
Retrieves the first slot in the collection.
#### Type
[`Slot`](Slot.mdx) | undefined

---
### 🏷️ `lastItem` {#lastItem}
Retrieves the last slot in the collection.
#### Type
[`Slot`](Slot.mdx) | undefined

## ▶️ Methods {#methods}

---
### 🔧 `clean` {#clean}
Removes all the slots from the slots collection.
#### Signature
```ts
clean(kind?: "static" | "dynamic" | "feature" | "meta", ...exclude: Slot[]): void
```
#### Parameters
| Name         | Type                                         | Optional | Description                                               |
|:-------------|:---------------------------------------------|:---------|:----------------------------------------------------------|
| `kind`       | "static" \| "dynamic" \| "feature" \| "meta" | Yes      | Optional kind that specifies the kind of slots to remove. |
| `...exclude` | [`Slot[]`](Slot.mdx)                         | Yes      | Specifies the slots that you want to keep.                |

---
### 🔧 `delete` {#delete}
Deletes a slot from the slots collection.
#### Signature
```ts
delete<T extends Slot>(
  referenceOrId: string,
  kind?: "static" | "dynamic" | "feature" | "meta"
): T | undefined
```
#### Parameters
| Name            | Type                                         | Optional | Description                                                                                 |
|:----------------|:---------------------------------------------|:---------|:--------------------------------------------------------------------------------------------|
| `referenceOrId` | string                                       | No       | Specifies the [reference](Slot.mdx#reference) or the [identifier](Slot.mdx#id) of the slot. |
| `kind`          | "static" \| "dynamic" \| "feature" \| "meta" | Yes      | Optional kind to narrow the scope of the operation.                                         |
#### Return value
Returns the deleted [`Slot`](Slot.mdx) instance or `undefined` if the slot was not found.

---
### 🔧 `deprecate` {#deprecate}
Deprecates a slot. This removes a slot from the slots collection and is used when a new version of a block needs to remove slots that were created by an earlier version of the block.
#### Signature
```ts
deprecate<T extends Slot>(
  referenceOrId: string,
  kind?: "static" | "dynamic" | "feature" | "meta"
): T | undefined
```
#### Parameters
| Name            | Type                                         | Optional | Description                                                                                 |
|:----------------|:---------------------------------------------|:---------|:--------------------------------------------------------------------------------------------|
| `referenceOrId` | string                                       | No       | Specifies the [reference](Slot.mdx#reference) or the [identifier](Slot.mdx#id) of the slot. |
| `kind`          | "static" \| "dynamic" \| "feature" \| "meta" | Yes      | Optional kind to narrow the scope of the operation.                                         |
#### Return value
Returns the deprecated [`Slot`](Slot.mdx) instance or `undefined` if the slot was not found.

---
### 🔧 `dynamic` {#dynamic}
Creates a new dynamic slot. Dynamic slots are used for dynamic elements in a block (for example, slots that are created for each checkbox of a block that supports a customizable list of checkboxes).
#### Signature
```ts
dynamic<T extends Slot>(properties: ISlotProperties): T
```
#### Parameters
| Name         | Type                                           | Optional | Description                            |
|:-------------|:-----------------------------------------------|:---------|:---------------------------------------|
| `properties` | [`ISlotProperties`](index.mdx#ISlotProperties) | No       | Specifies the properties for the slot. |
#### Return value
Returns a reference to the new [`Slot`](Slot.mdx) instance.

---
### 🔧 `each` {#each}
Iterates through all slots in the collection.
#### Signature
```ts
each(fn: (slot: Slot) => boolean | void): boolean
```
#### Parameters
| Name | Type                                          | Optional | Description                                                                                                                                    |
|:-----|:----------------------------------------------|:---------|:-----------------------------------------------------------------------------------------------------------------------------------------------|
| `fn` | (slot: [`Slot`](Slot.mdx)) => boolean \| void | No       | Function that is called for each slot in the collection. If you want to stop the iteration, you can return `true` from this function to do so. |
#### Return value
Returns `true` if the iteration was stopped (break).

---
### 🔧 `feature` {#feature}
Creates a new feature slot. Feature slots are used for optional features of a block (for example, a slot that holds the score value of a block that supports an optional score feature).
#### Signature
```ts
feature<T extends Slot>(properties: ISlotProperties): T
```
#### Parameters
| Name         | Type                                           | Optional | Description                            |
|:-------------|:-----------------------------------------------|:---------|:---------------------------------------|
| `properties` | [`ISlotProperties`](index.mdx#ISlotProperties) | No       | Specifies the properties for the slot. |
#### Return value
Returns a reference to the new [`Slot`](Slot.mdx) instance.

---
### 🔧 `meta` {#meta}
Creates a new meta slot. Meta slots are used to store metadata (for example, the time taken to answer a block).
#### Signature
```ts
meta<T extends Slot>(properties: ISlotProperties): T
```
#### Parameters
| Name         | Type                                           | Optional | Description                            |
|:-------------|:-----------------------------------------------|:---------|:---------------------------------------|
| `properties` | [`ISlotProperties`](index.mdx#ISlotProperties) | No       | Specifies the properties for the slot. |
#### Return value
Returns a reference to the new [`Slot`](Slot.mdx) instance.

---
### 🔧 `reverseEach` {#reverseEach}
Iterates through all slots in the collection in reverse direction (starting with the last slot).
#### Signature
```ts
reverseEach(fn: (slot: Slot) => boolean | void): boolean
```
#### Parameters
| Name | Type                                          | Optional | Description                                                                                                                                    |
|:-----|:----------------------------------------------|:---------|:-----------------------------------------------------------------------------------------------------------------------------------------------|
| `fn` | (slot: [`Slot`](Slot.mdx)) => boolean \| void | No       | Function that is called for each slot in the collection. If you want to stop the iteration, you can return `true` from this function to do so. |
#### Return value
Returns `true` if the iteration was stopped (break).

---
### 🔧 `select` {#select}
Selects a slot with the supplied [reference](Slot.mdx#reference) or [identifier](Slot.mdx#id).
#### Signature
```ts
select<T extends Slot>(
  referenceOrId: string,
  kind?: "static" | "dynamic" | "feature" | "meta"
): T | undefined
```
#### Parameters
| Name            | Type                                         | Optional | Description                                                                                 |
|:----------------|:---------------------------------------------|:---------|:--------------------------------------------------------------------------------------------|
| `referenceOrId` | string                                       | No       | Specifies the [reference](Slot.mdx#reference) or the [identifier](Slot.mdx#id) of the slot. |
| `kind`          | "static" \| "dynamic" \| "feature" \| "meta" | Yes      | Optional kind to narrow the scope of the select operation.                                  |
#### Return value
Returns the [`Slot`](Slot.mdx) instance or `undefined` if the slot was not found.

---
### 🔧 `selectByIdentifier` {#selectByIdentifier}
Selects a slot with the supplied [identifier](Slot.mdx#id).
#### Signature
```ts
selectByIdentifier<T extends Slot>(id: string): T | undefined
```
#### Parameters
| Name | Type   | Optional | Description                                                                                 |
|:-----|:-------|:---------|:--------------------------------------------------------------------------------------------|
| `id` | string | No       | Specifies the [reference](Slot.mdx#reference) or the [identifier](Slot.mdx#id) of the slot. |
#### Return value
Returns the [`Slot`](Slot.mdx) instance or `undefined` if the slot was not found.

---
### 🔧 `sort` {#sort}
Sorts the slots based on the [`sequence`](Slot.mdx#sequence) number of the slots.
#### Signature
```ts
sort(): boolean
```
#### Return value
Returns `true` if the order of the slots has changed.

---
### 🔧 `static` {#static}
Creates a new static slot. Static slots are fixed slots that are required for the functional operation of a block (for example, a string slot that holds the text value of a text input block).
#### Signature
```ts
static<T extends Slot>(properties: ISlotProperties): T
```
#### Parameters
| Name         | Type                                           | Optional | Description                            |
|:-------------|:-----------------------------------------------|:---------|:---------------------------------------|
| `properties` | [`ISlotProperties`](index.mdx#ISlotProperties) | No       | Specifies the properties for the slot. |
#### Return value
Returns a reference to the new [`Slot`](Slot.mdx) instance.
#### Example
```ts showLineNumbers
import { Slots, L10n } from "@tripetto/runner";

// Let's assume there is a variable slots
declare const slots: Slots.Slots;

//highlight-start
// Create a static slot to hold a text
const textSlot: Slots.Text = slots.static({
  type: Slots.Text,
  reference: "value",
  label: L10n.gettext("Text")
});
//highlight-end
```
