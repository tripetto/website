---
title: Runner Library API
description: The Tripetto Runner library is the workhorse of all Tripetto runners.
---

# ![](/img/logo-ts.svg) Runner Library API reference
The [Tripetto Runner library](https://www.npmjs.com/package/@tripetto/runner) is the workhorse of all Tripetto runners. It turns a [form definition](../../../builder/api/interfaces/IDefinition.mdx) (the output of the [builder](../../../builder/introduction.md)) into an executable program; a [virtual finite state machine](https://en.wikipedia.org/wiki/Virtual_finite-state_machine) that handles all the complex logic and response collection during the execution of the form. Apply a UI on top of it, and you have a usable runner!

## ✨ Installation [![](/img/logo-npm-icon.svg)](https://www.npmjs.com/package/@tripetto/runner) {#installation}

```bash npm2yarn
npm install @tripetto/runner
```

:::tip Typescript support
This package contains type declarations and supports [TypeScript](https://www.typescriptlang.org/) out-of-the-box.
:::

## 💎 Classes {#classes}
- [`Await`](classes/Await.mdx)
- [`Branch`](classes/Branch.mdx)
- [`Branches`](classes/Branches.mdx)
- [`Callback`](classes/Callback.mdx)
- [`Section`](classes/Section.mdx)
- [`Sections`](classes/Sections.mdx)
- [`Condition`](classes/Condition.mdx)
- [`ConditionBlock`](classes/ConditionBlock.mdx)
- [`ConditionBlocksNamespace`](classes/ConditionBlocksNamespace.mdx)
- [`Conditions`](classes/Conditions.mdx)
- [`Context`](classes/Context.mdx)
- [`Data`](classes/Data.mdx)
- [`Enumerator`](classes/Enumerator.mdx)
- [`HeadlessBlock`](classes/HeadlessBlock.mdx)
- [`ImmutableValue`](classes/ImmutableValue.mdx)
- [`Instance`](classes/Instance.mdx)
- [`MarkdownParser`](classes/MarkdownParser.mdx)
- [`Moment`](classes/Moment.mdx)
- [`Node`](classes/Node.mdx)
- [`NodeBlock`](classes/NodeBlock.mdx)
- [`NodeBlocksNamespace`](classes/NodeBlocksNamespace.mdx)
- [`Nodes`](classes/Nodes.mdx)
- [`Runner`](classes/Runner.mdx)
- [`Storyline`](classes/Storyline.mdx)
- [`Value`](classes/Value.mdx)
- [`Values`](classes/Values.mdx)

## 🎀 Decorators {#decorators}
#### Class decorators [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#class-decorators)
- [`@tripetto`](decorators/tripetto.mdx)
#### Method decorators [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#method-decorators)
- [`@condition`](decorators/condition.mdx)
- [`@destroy`](decorators/destroy.mdx)
- [`@validator`](decorators/validator.mdx)

## ▶️ Functions {#functions}
- [`arrayItem`](functions/arrayItem.mdx)
- [`arraySize`](functions/arraySize.mdx)
- [`assert`](functions/assert.mdx)
- [`calculateFingerprintAndStencil`](functions/calculateFingerprintAndStencil.mdx)
- [`call`](functions/call.mdx)
- [`callAsync`](functions/callAsync.mdx)
- [`callBind`](functions/callBind.mdx)
- [`callPromise`](functions/callPromise.mdx)
- [`cancelFrame`](functions/cancelFrame.mdx)
- [`cancelPromise`](functions/cancelPromise.mdx)
- [`cast`](functions/cast.mdx)
- [`castToBoolean`](functions/castToBoolean.mdx)
- [`castToFloat`](functions/castToFloat.mdx)
- [`castToNumber`](functions/castToNumber.mdx)
- [`castToString`](functions/castToString.mdx)
- [`checksum`](functions/checksum.mdx)
- [`clone`](functions/clone.mdx)
- [`compare`](functions/compare.mdx)
- [`count`](functions/count.mdx)
- [`destroy`](functions/destroy.mdx)
- [`each`](functions/each.mdx)
- [`eachReverse`](functions/eachReverse.mdx)
- [`extend`](functions/extend.mdx)
- [`extendImmutable`](functions/extendImmutable.mdx)
- [`filter`](functions/filter.mdx)
- [`find`](functions/find.mdx)
- [`findFirst`](functions/findFirst.mdx)
- [`findLast`](functions/findLast.mdx)
- [`fingerprint`](functions/fingerprint.mdx)
- [`firstArrayItem`](functions/firstArrayItem.mdx)
- [`forEach`](functions/forEach.mdx)
- [`get`](functions/get.mdx)
- [`getBoolean`](functions/getBoolean.mdx)
- [`getFloat`](functions/getFloat.mdx)
- [`getNumber`](functions/getNumber.mdx)
- [`getString`](functions/getString.mdx)
- [`hasOneOrMore`](functions/hasOneOrMore.mdx)
- [`hasOnly`](functions/hasOnly.mdx)
- [`isArray`](functions/isArray.mdx)
- [`isBoolean`](functions/isBoolean.mdx)
- [`isCollection`](functions/isCollection.mdx)
- [`isDate`](functions/isDate.mdx)
- [`isDefined`](functions/isDefined.mdx)
- [`isError`](functions/isError.mdx)
- [`isFilledString`](functions/isFilledString.mdx)
- [`isFloat`](functions/isFloat.mdx)
- [`isFunction`](functions/isFunction.mdx)
- [`isNull`](functions/isNull.mdx)
- [`isNumber`](functions/isNumber.mdx)
- [`isNumberFinite`](functions/isNumberFinite.mdx)
- [`isObject`](functions/isObject.mdx)
- [`isPromise`](functions/isPromise.mdx)
- [`isRegEx`](functions/isRegEx.mdx)
- [`isString`](functions/isString.mdx)
- [`isUndefined`](functions/isUndefined.mdx)
- [`isVariable`](functions/isVariable.mdx)
- [`lastArrayItem`](functions/lastArrayItem.mdx)
- [`map`](functions/map.mdx)
- [`markdownify`](functions/markdownify.mdx)
- [`markdownifyTo`](functions/markdownifyTo.mdx)
- [`markdownifyToPlainText`](functions/markdownifyToPlainText.mdx)
- [`markdownifyToString`](functions/markdownifyToString.mdx)
- [`markdownifyToURL`](functions/markdownifyToURL.mdx)
- [`mountNamespace`](functions/mountNamespace.mdx)
- [`powDuration`](functions/powDuration.mdx)
- [`powHashRate`](functions/powHashRate.mdx)
- [`powSolve`](functions/powSolve.mdx)
- [`powSpentTime`](functions/powSpentTime.mdx)
- [`powVerify`](functions/powVerify.mdx)
- [`reduce`](functions/reduce.mdx)
- [`scheduleAction`](functions/scheduleAction.mdx)
- [`scheduleAndCancelFrame`](functions/scheduleAndCancelFrame.mdx)
- [`scheduleAndCancelPromise`](functions/scheduleAndCancelPromise.mdx)
- [`scheduleAnimation`](functions/scheduleAnimation.mdx)
- [`scheduleEvent`](functions/scheduleEvent.mdx)
- [`scheduleFrame`](functions/scheduleFrame.mdx)
- [`schedulePromise`](functions/schedulePromise.mdx)
- [`set`](functions/set.mdx)
- [`stencil`](functions/stencil.mdx)
- [`stringLength`](functions/stringLength.mdx)
- [`unmountNamespace`](functions/unmountNamespace.mdx)

## 🗂️ Modules {#modules}
- [`AES`](modules/AES.mdx)
- [`DateTime`](modules/DateTime.mdx)
- [`Environment`](modules/Environment.mdx)
- [`Export`](modules/Export.mdx)
- [`Import`](modules/Import.mdx)
- [`L10n`](modules/L10n/index.mdx)
- [`Namespaces`](modules/Namespaces.mdx)
- [`Num`](modules/Num.mdx)
- [`SHA2`](modules/SHA2.mdx)
- [`Slots`](modules/Slots/index.mdx)
- [`Str`](modules/Str.mdx)
- [`TripettoRunner`](modules/TripettoRunner.mdx)

## ⛓️ Interfaces {#interfaces}
- [`IDataChangeEvent`](interfaces/IDataChangeEvent.mdx)
- [`IDefinition`](interfaces/IDefinition.mdx)
- [`IEpilogue`](interfaces/IEpilogue.mdx)
- [`IInstanceSnapshot`](interfaces/IInstanceSnapshot.mdx)
- [`IL10n`](interfaces/IL10n.mdx)
- [`INamespace`](interfaces/INamespace.mdx)
- [`IObservableNode`](interfaces/IObservableNode.mdx)
- [`IPrologue`](interfaces/IPrologue.mdx)
- [`IRunnerChangeEvent`](interfaces/IRunnerChangeEvent.mdx)
- [`IRunnerProperties`](interfaces/IRunnerProperties.mdx)
- [`ISnapshot`](interfaces/ISnapshot.mdx)
- [`IStoryline`](interfaces/IStoryline.mdx)
- [`IVariable`](interfaces/IVariable.mdx)

## 📇 Enums {#enums}
- [`Errors`](enums/Errors.mdx)
- [`MarkdownFeatures`](enums/MarkdownFeatures.mdx)

## 🗿 Constants {#constants}
- [`NAME`](constants/NAME.mdx)
- [`VERSION`](constants/VERSION.mdx)

## 📁 Source code [![](/img/logo-gitlab.svg)](https://gitlab.com/tripetto/runner) {#source}
The Tripetto Runner library code is on [GitLab](https://gitlab.com/tripetto/runner).
