---
title: ConditionBlock class - Runner
sidebar_label: ConditionBlock
description: The ConditionBlock class is what gives a Condition its actual functionality (Runner library).
---

# ConditionBlock class

## 📖 Description {#description}
The `ConditionBlock` class is what gives a [`Condition`](Condition.mdx) its actual functionality.

:::tip
You can create [custom condition blocks](../../../../blocks/custom/implement/condition.mdx) for Tripetto.
:::

## 🎀 Applicable decorators {#decorators}
The following decorators can be applied in this class:
#### Class decorators [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#class-decorators)
- [`@tripetto`](../decorators/tripetto.mdx)
#### Method decorators [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#method-decorators)
- [`@condition`](../decorators/condition.mdx)

## 🗃️ Fields {#fields}

---
### 🏷️ `branch` {#branch}
Retrieves the branch props from the [form definition](../../../../builder/api/interfaces/IDefinition.mdx) (only when there is a branch attached to the condition).
#### Type {#signature}
[`IBranch`](../../../../builder/api/interfaces/IBranch.mdx) | undefined

---
### 🏷️ `branchRef` {#branchRef}
Retrieves a reference to an attached branch (if any).
#### Type {#signature}
[`Branch`](Branch.mdx) | undefined

---
### 🏷️ `section` {#section}
Retrieves the section props from the [form definition](../../../../builder/api/interfaces/IDefinition.mdx) (only when there is a section attached to the condition).
#### Type {#signature}
[`ISection`](../../../../builder/api/interfaces/ISection.mdx) | undefined

---
### 🏷️ `sectionRef` {#sectionRef}
Retrieves a reference to an attached section (if any).
#### Type {#signature}
[`Section`](Section.mdx) | undefined

---
### 🏷️ `condition` {#condition}
Retrieves the condition props from the [form definition](../../../../builder/api/interfaces/IDefinition.mdx).
#### Type {#signature}
[`ICondition`](../../../../builder/api/interfaces/ICondition.mdx)

---
### 🏷️ `context` {#context}
Reference to the context of the block.
#### Type {#signature}
[`Context`](Context.mdx)

---
### 🏷️ `node` {#node}
Retrieves the node props from the [form definition](../../../../builder/api/interfaces/IDefinition.mdx) (only when there is a node attached to the condition).
#### Type {#signature}
[`INode`](../../../../builder/api/interfaces/INode.mdx) | undefined

---
### 🏷️ `nodeRef` {#nodeRef}
Retrieves a reference to an attached node (if any).
#### Type {#signature}
[`Node`](Node.mdx) | undefined

---
### 🏷️ `props` {#props}
Retrieves the block properties from the [form definition](../../../../builder/api/interfaces/IDefinition.mdx).
#### Type {#signature}
```ts
{
  /* Contains the condition block type identifier. */
  type: string;

  /* Contains the version of the block. */
  version: string;

  /* Contains the id of a related branch. */
  branch?: string;

  /* Contains the id of a related section. */
  section?: string;

  /* Contains the id of a related node. */
  node?: string;

  /* Contains the id of a related slot. */
  slot?: string;
}
```

:::info
Besides the listed properties, condition blocks may store additional properties in the `props` object.
:::

---
### 🏷️ `slots` {#slots}
Retrieves a reference to the slots collection of an attached node (only when condition is attached to a node).
#### Type {#signature}
[`Slots`](../modules/Slots/SlotsClass.mdx)

---
### 🏷️ `type` {#type}
Retrieves the block type object.
#### Type {#signature}
```ts
{
  /* Contains the block type identifier. */
  identifier: string;
}
```

---
### 🏷️ `view` {#view}
Retrieves the current view mode of the runner. It can be one of the following values:
- `live`: Normal run mode of the form;
- `test`: Runs the form like a real one without being able to submit data;
- `preview`: Shows all elements in the form by skipping all logic.
#### Type {#signature}
"live" | "test" | "preview"

## ▶️ Methods {#methods}

---
### 🔧 `immutableValueOf` {#immutableValueOf}
Retrieves the immutable (write-protected) value for the supplied slot instance or identifier.
#### Signature
```ts
immutableValueOf(slot?: Slot | string): ImmutableValue | undefined
```
#### Parameters
| Name   | Type                                          | Optional | Description                                                                                                                       |
|:-------|:----------------------------------------------|:---------|:----------------------------------------------------------------------------------------------------------------------------------|
| `slot` | [`Slot`](../modules/Slots/Slot.mdx) \| string | Yes      | Specifies the slot of the value. If omitted the slot specified in the `slot` property of the block [`props`](#props) is returned. |
#### Return value
Returns the [`ImmutableValue`](ImmutableValue.mdx) instance for the slot or `undefined` if the supplied slot is invalid.

---
### 🔧 `parseVariables` {#parseVariables}
Parses the supplied markdown string, replacing all variables with their current values.
:::info
This method is used to parse markdown to plain text.
:::
#### Signature
```ts
parseVariables(markdown: string, placeholder?: string, lineBreaks?: boolean): string
```
#### Parameters
| Name          | Type    | Optional | Description                                                   |
|:--------------|:--------|:---------|:--------------------------------------------------------------|
| `markdown`    | string  | No       | Specifies the markdown string to parse.                       |
| `placeholder` | string  | Yes      | Specifies a string for empty variables (defaults to `""`).    |
| `lineBreaks`  | boolean | Yes      | Specifies if line breaks are supported (defaults to `false`). |
#### Return value
Returns the parsed string.

---
### 🔧 `slotOf` {#slotOf}
Retrieves the slot attached to the condition (this is the slot specified in the `slot` property of the block [`props`](#props)) or the slot with the specified identifier.
#### Signature
```ts
slotOf(id?: string): Slot | undefined
```
#### Parameters
| Name | Type   | Optional | Description                                                                                                                                        |
|:-----|:-------|:---------|:---------------------------------------------------------------------------------------------------------------------------------------------------|
| `id` | string | No       | Specifies the identifier of the slot to retrieve. If omitted the slot specified in the `slot` property of the block [`props`](#props) is returned. |
#### Return value
Returns the [`Slot`](../modules/Slots/Slot.mdx) instance or `undefined` if no slot is attached or the slot is not found.

---
### 🔧 `valueOf` {#valueOf}
Retrieves an immutable (write-protected) value. You can only retrieve values for the [slots](#slots) of an attached [node](#nodeRef). If you want to retrieve values from other nodes or for conditions without an attached node, use [`immutableValueOf`](#immutableValueOf).
#### Signature
```ts
valueOf(slot?: Slot | string, kind?: "static" | "dynamic" | "feature" | "meta"): ImmutableValue | undefined
```
#### Parameters
| Name   | Type                                          | Optional | Description                                                                                                                       |
|:-------|:----------------------------------------------|:---------|:----------------------------------------------------------------------------------------------------------------------------------|
| `slot` | [`Slot`](../modules/Slots/Slot.mdx) \| string | Yes      | Specifies the slot of the value. If omitted the slot specified in the `slot` property of the block [`props`](#props) is returned. |
| `kind` | "static" \| "dynamic" \| "feature" \| "meta"  | Yes      | Optional kind to narrow the scope of the slot select operation.                                                                   |
#### Return value
Returns the [`ImmutableValue`](ImmutableValue.mdx) instance for the slot or `undefined` if the supplied slot is invalid.

---
### 🔧 `variableFor` {#variableFor}
Retrieves the variable for the supplied slot instance or (pipe) identifier. A variable is an immutable representation of a slot value. This method allows to retrieve any slot value in the form within the right context. It can be used to retrieve values from other blocks as well. For example, the [calculator block](../../../../blocks/stock/calculator.mdx) uses this method to retrieve input values from other blocks for the calculator.
#### Signature
```ts
variableFor(slot: Slot | { slot: Slot } | string): IVariable | undefined
```
#### Parameters
| Name   | Type                                                                                            | Optional | Description                         |
|:-------|:------------------------------------------------------------------------------------------------|:---------|:------------------------------------|
| `slot` | [`Slot`](../modules/Slots/Slot.mdx) \| \{ slot: [`Slot`](../modules/Slots/Slot.mdx) } \| string | No       | Specifies the slot of the variable. |
#### Return value
Returns the [`IVariable`](../interfaces/IVariable.mdx) instance for the slot or `undefined` if the supplied slot is invalid.
