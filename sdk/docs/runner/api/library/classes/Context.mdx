---
title: Context class - Runner
sidebar_label: Context
description: The Context class defines the context for the data values in a form (Runner library).
---

# Context class

## 📖 Description {#description}
The `Context` class defines the context for the data values in a form. For simple forms, there is just one context. But Tripetto allows the use of iterations to ask the same set of questions multiple times for multiple subjects. In those cases, there is a context generated for each subject. When iterations are used, the same block may appear several times in a form (defined by the number of subjects). It then stores multiple values for a single block, where each value is stored in its own context.

:::info
`Context` instances are automatically generated. There is no need to construct them from your own code.
:::

## 🗃️ Fields {#fields}

---
### 🏷️ `index` {#index}
Retrieves the index of the context.
#### Type
number

---
### 🏷️ `instance` {#instance}
Retrieves the instance of the context.
#### Type
[`Instance`](Instance.mdx)

---
### 🏷️ `key` {#key}
Retrieves the context key.
#### Type
string

---
### 🏷️ `l10n` {#l10n}
Retrieves the localization namespace with helper functions for working with translations and locales.
#### Type
[`L10n.Namespace`](../modules/L10n/Namespace.mdx)

## ▶️ Methods {#methods}

---
### 🔧 `contextualValueOf` {#contextualValueOf}
Retrieves the mutable value for the supplied slot instance or identifier. But instead of retrieving the [`Value`](Value.mdx) instance from the current `Context` instance, it first tries to retrieve the value from a parent `Context` instance tree. If the value is already available higher up in the context tree, that [`Value`](Value.mdx) instance is returned. If the value was not found, the [`mutableValueOf`](#mutableValueOf) method is called instead and that [`Value`](Value.mdx) instance is returned.
:::caution
Only values for slots defined by the block that called this method can be retrieved. If you need to retrieve values from other blocks, use the [`variableFor`](#variableFor) method.
:::
#### Signature
```ts
contextualValueOf(slot: Slot | { slot: Slot } | string): Value | undefined
```
#### Parameters
| Name   | Type                                                                                            | Optional | Description                      |
|:-------|:------------------------------------------------------------------------------------------------|:---------|:---------------------------------|
| `slot` | [`Slot`](../modules/Slots/Slot.mdx) \| \{ slot: [`Slot`](../modules/Slots/Slot.mdx) } \| string | No       | Specifies the slot of the value. |
#### Return value
Returns the [`Value`](Value.mdx) instance for the slot or `undefined` if the supplied slot is invalid.

---
### 🔧 `immutableValueOf` {#immutableValueOf}
Retrieves the immutable (write-protected) value for the supplied slot instance or identifier.
:::caution
Only values for slots defined by the block that called this method can be retrieved. If you need to retrieve values from other blocks, use the [`variableFor`](#variableFor) method.
:::
#### Signature
```ts
immutableValueOf(slot: Slot | { slot: Slot } | string): ImmutableValue | undefined
```
#### Parameters
| Name   | Type                                                                                            | Optional | Description                      |
|:-------|:------------------------------------------------------------------------------------------------|:---------|:---------------------------------|
| `slot` | [`Slot`](../modules/Slots/Slot.mdx) \| \{ slot: [`Slot`](../modules/Slots/Slot.mdx) } \| string | No       | Specifies the slot of the value. |
#### Return value
Returns the [`ImmutableValue`](ImmutableValue.mdx) instance for the slot or `undefined` if the supplied slot is invalid.

---
### 🔧 `mutableValueOf` {#mutableValueOf}
Retrieves the mutable value for the supplied slot instance or identifier.
:::caution
Only values for slots defined by the block that called this method can be retrieved. If you need to retrieve values from other blocks, use the [`variableFor`](#variableFor) method.

A mutable value always exists in the `Context` instance where this method is called from. If you want to retrieve a value that already exists in a parent `Context`, use the [`contextualValueOf`](#contextualValueOf) method.
:::
#### Signature
```ts
mutableValueOf(slot: Slot | { slot: Slot } | string): Value | undefined
```
#### Parameters
| Name   | Type                                                                                            | Optional | Description                      |
|:-------|:------------------------------------------------------------------------------------------------|:---------|:---------------------------------|
| `slot` | [`Slot`](../modules/Slots/Slot.mdx) \| \{ slot: [`Slot`](../modules/Slots/Slot.mdx) } \| string | No       | Specifies the slot of the value. |
#### Return value
Returns the [`Value`](Value.mdx) instance for the slot or `undefined` if the supplied slot is invalid.

---
### 🔧 `validationOf` {#validationOf}
Retrieves the validation state of the supplied node.
#### Signature
```ts
validationOf(node: Node): "unknown" | "pass" | "fail"
```
#### Parameters
| Name   | Type               | Optional | Description                                              |
|:-------|:-------------------|:---------|:---------------------------------------------------------|
| `node` | [`Node`](Node.mdx) | No       | Specifies the node to retrieve the validation state for. |
#### Return value
Returns the validation state of the node. It can be one of the following values:
- `unknown`: There is no validation state available yet;
- `pass`: Validation succeeded;
- `fail`: Validation failed.

---
### 🔧 `variableFor` {#variableFor}
Retrieves the variable for the supplied slot instance or (pipe) identifier. A variable is an immutable representation of a slot value. This method allows to retrieve any slot value in the form within the right context. It can be used to retrieve values from other blocks as well. For example, the [calculator block](../../../../blocks/stock/calculator.mdx) uses this method to retrieve input values from other blocks for the calculator.
:::tip
This method can also retrieve (write-protected) values from blocks other than the block from which it was called.
:::
#### Signature
```ts
variableFor(slot: Slot | { slot: Slot } | string): IVariable | undefined
```
#### Parameters
| Name   | Type                                                                                            | Optional | Description                         |
|:-------|:------------------------------------------------------------------------------------------------|:---------|:------------------------------------|
| `slot` | [`Slot`](../modules/Slots/Slot.mdx) \| \{ slot: [`Slot`](../modules/Slots/Slot.mdx) } \| string | No       | Specifies the slot of the variable. |
#### Return value
Returns the [`IVariable`](../interfaces/IVariable.mdx) instance for the slot or `undefined` if the supplied slot is invalid.
