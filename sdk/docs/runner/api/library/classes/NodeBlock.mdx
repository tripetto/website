---
title: NodeBlock class - Runner
sidebar_label: NodeBlock
description: The NodeBlock class is what gives a Node its actual functionality (Runner library).
---

# NodeBlock class

## 📖 Description {#description}
The `NodeBlock` class is what gives a [`Node`](Node.mdx) its actual functionality. These blocks are visual blocks that interact with the user and collect the response data for the slots that are denoted by the block.

:::tip
You can create [custom blocks](../../../../blocks/custom/implement/visual.mdx) for Tripetto. More information about custom blocks for the stock runners can be found in this [Custom blocks guide](../../../stock/guides/blocks.mdx).
:::

## 🎀 Applicable decorators {#decorators}
The following decorators can be applied in this class:
#### Class decorators [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#class-decorators)
- [`@tripetto`](../decorators/tripetto.mdx)
#### Method decorators [ℹ️](https://www.typescriptlang.org/docs/handbook/decorators.html#method-decorators)
- [`@destroy`](../decorators/destroy.mdx)
- [`@validator`](../decorators/validator.mdx)

## 🗃️ Fields {#fields}

---
### 🏷️ `context` {#context}
Reference to the context of the block.
#### Type {#signature}
[`Context`](Context.mdx)

---
### 🏷️ `isFailed` {#isFailed}
Retrieves if the validation of the block failed.
#### Type {#signature}
boolean

---
### 🏷️ `isPassed` {#isPassed}
Retrieves if the validation of the block passed.
#### Type {#signature}
boolean

---
### 🏷️ `node` {#condition}
Retrieves the node props from the [form definition](../../../../builder/api/interfaces/IDefinition.mdx).
#### Type {#signature}
[`INode`](../../../../builder/api/interfaces/INode.mdx)

---
### 🏷️ `props` {#props}
Retrieves the block properties from the [form definition](../../../../builder/api/interfaces/IDefinition.mdx).
#### Type {#signature}
```ts
{
  /* Contains the node block type identifier. */
  type: string;

  /* Contains the version of the block. */
  version: string;
}
```

:::info
Besides the listed properties, node blocks may store additional properties in the `props` object.
:::

---
### 🏷️ `shouldAutoRender` {#shouldAutoRender}
Specifies if an automatic render is needed upon value store. This triggers a rerendering when a value is changed.
#### Type {#signature}
boolean

---
### 🏷️ `shouldAutoValidate` {#shouldAutoValidate}
Specifies if automatic validation is applied which checks if all required slots contain a valid value.
#### Type {#signature}
boolean

---
### 🏷️ `type` {#type}
Retrieves the block type object.
#### Type {#signature}
```ts
{
  /* Contains the block type identifier. */
  identifier: string;

  /* Specifies if the block needs an automatic render upon value store. */
  autoRender?: boolean;

  /* Specifies if the block needs automatic validation. */
  autoValidate?: boolean;

  /* Reference to optional static data for the block. */
  ref?: {};
}
```

---
### 🏷️ `validation` {#validation}
Retrieves the validation state of the block. It can be one of the following values:
- `unknown`: There is no validation state available yet;
- `pass`: Validation succeeded;
- `fail`: Validation failed.
#### Type {#signature}
"unknown" | "pass" | "fail"

---
### 🏷️ `view` {#view}
Retrieves the current view mode of the runner. It can be one of the following values:
- `live`: Normal run mode of the form;
- `test`: Runs the form like a real one without being able to submit data;
- `preview`: Shows all elements in the form by skipping all logic.
#### Type {#signature}
"live" | "test" | "preview"

## ▶️ Methods {#methods}

---
### 🔧 `clear` {#clear}
Clears all data values of a block.
#### Signature
```ts
clear(): void
```

---
### 🔧 `immutableValueOf` {#immutableValueOf}
Retrieves the immutable (write-protected) value for the supplied slot instance or identifier.
#### Signature
```ts
immutableValueOf(slot: Slot | string): ImmutableValue | undefined
```
#### Parameters
| Name   | Type                                          | Optional | Description                      |
|:-------|:----------------------------------------------|:---------|:---------------------------------|
| `slot` | [`Slot`](../modules/Slots/Slot.mdx) \| string | No       | Specifies the slot of the value. |
#### Return value
Returns the [`ImmutableValue`](ImmutableValue.mdx) instance for the slot or `undefined` if the supplied slot is invalid.

---
### 🔧 `key` {#key}
Retrieves a unique key for the block within the current context for the specified label (the key is prefixed with an underscore).
:::tip
Use this function if you need stable unique keys (for example to use as element IDs).
:::
#### Signature
```ts
key(label?: string): string
```
#### Parameters
| Name          | Type    | Optional | Description                                                   |
|:--------------|:--------|:---------|:--------------------------------------------------------------|
| `label` | string  | Yes      | Optional label for the key (if omitted the default key will be returned).    |
#### Return value
Returns the key.

---
### 🔧 `lock` {#lock}
Locks the data values of a block (makes the whole block write-protected).
#### Signature
```ts
lock(): void
```

---
### 🔧 `parseVariables` {#parseVariables}
Parses the supplied markdown string, replacing all variables with their current values.
:::info
This method is used to parse markdown to plain text.
:::
#### Signature
```ts
parseVariables(markdown: string, placeholder?: string, lineBreaks?: boolean): string
```
#### Parameters
| Name          | Type    | Optional | Description                                                   |
|:--------------|:--------|:---------|:--------------------------------------------------------------|
| `markdown`    | string  | No       | Specifies the markdown string to parse.                       |
| `placeholder` | string  | Yes      | Specifies a string for empty variables (defaults to `""`).    |
| `lineBreaks`  | boolean | Yes      | Specifies if line breaks are supported (defaults to `false`). |
#### Return value
Returns the parsed string.

---
### 🔧 `rerender` {#rerender}
Invokes a block rerendering.
#### Signature
```ts
rerender(): void
```

---
### 🔧 `slotOf` {#slotOf}
Retrieves the slot with the specified reference.
#### Signature
```ts
slotOf(reference: string, kind?: "static" | "dynamic" | "feature" | "meta"): Slot | undefined
```
#### Parameters
| Name        | Type                                         | Optional | Description                                                     |
|:------------|:---------------------------------------------|:---------|:----------------------------------------------------------------|
| `reference` | string                                       | No       | Specifies the slot reference.                                   |
| `kind`      | "static" \| "dynamic" \| "feature" \| "meta" | Yes      | Optional kind to narrow the scope of the slot select operation. |
#### Return value
Returns the [`Slot`](../modules/Slots/Slot.mdx) instance or `undefined` if no slot is found.

---
### 🔧 `unlock` {#unlock}
Unlocks the data values of a block (removes the write-protected flag from the whole block).
#### Signature
```ts
unlock(): void
```

---
### 🔧 `valueOf` {#valueOf}
Retrieves a mutable value of a slot. Use this method to set/retrieve the values of slots that belong to the block.
:::info
You can only retrieve mutable values for the slots managed by the block. If you want values from other nodes, use the [`immutableValueOf`](#immutableValueOf) method.
:::
#### Signature
```ts
valueOf(
  reference: string,
  kind?: "static" | "dynamic" | "feature" | "meta",
  options?: {
    confirm?: boolean;
    prefill?: {
      value: any;
      reference?: string;
    };
    modifier?: (value: Value) => {
      value: any;
      reference?: string;
    } | undefined;
    onChange?: (value: Value) => void;
    onContext?: (value: Value, context: Context) => void;
  }
): Value | undefined
```
#### Parameters
| Name        | Type                                         | Optional | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
|:------------|:---------------------------------------------|:---------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `reference` | string                                       | No       | Specifies the slot reference.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| `kind`      | "static" \| "dynamic" \| "feature" \| "meta" | Yes      | Optional kind to narrow the scope of the slot select operation.                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `options`   | object                                       | Yes      | Specifies additional options. Supports the following options:<br/>- `confirm`: Specifies if the slot always has a confirmed value;<br/>- `prefill`: Specifies a prefill value for the slot;<br/>- `modifier`: Specifies a modifier function that is invoked when the value of the slot changes and can be used to modify the value;<br/>- `onChange`: Specifies a function that is invoked when the value of the slot changes;<br/>- `onContext`: Specifies a function that is invoked when the value is used within another context. |
#### Return value
Returns the [`Value`](Value.mdx) instance for the slot or `undefined` if the supplied slot is invalid.

---
### 🔧 `variableFor` {#variableFor}
Retrieves the variable for the supplied slot instance or (pipe) identifier. A variable is an immutable representation of a slot value. This method allows to retrieve any slot value in the form within the right context. It can be used to retrieve values from other blocks as well. For example, the [calculator block](../../../../blocks/stock/calculator.mdx) uses this method to retrieve input values from other blocks for the calculator.
#### Signature
```ts
variableFor(slot: Slot | { slot: Slot } | string): IVariable | undefined
```
#### Parameters
| Name   | Type                                                                                            | Optional | Description                         |
|:-------|:------------------------------------------------------------------------------------------------|:---------|:------------------------------------|
| `slot` | [`Slot`](../modules/Slots/Slot.mdx) \| \{ slot: [`Slot`](../modules/Slots/Slot.mdx) } \| string | No       | Specifies the slot of the variable. |
#### Return value
Returns the [`IVariable`](../interfaces/IVariable.mdx) instance for the slot or `undefined` if the supplied slot is invalid.
