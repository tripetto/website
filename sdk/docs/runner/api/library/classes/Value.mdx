---
title: Value class - Runner
sidebar_label: Value
description: The Value class defines a mutable value for a certain Slot within a certain Context (Runner library).
---

# Value class

## 📖 Description {#description}
The `Value` class defines a mutable value for a certain [`Slot`](../modules/Slots/Slot.mdx) within a certain [`Context`](Context.mdx).

:::tip
For each `Value` instance you can retrieve an [immutable version](#immutable). This immutable representation of the value defined by the [`ImmutableValue`](ImmutableValue.mdx) class is a write-protected carbon copy of the value.
:::

## 🗃️ Fields {#fields}

---
### 🏷️ `allowPrefill` {#allowPrefill}
Specifies if the value can be prefilled.
#### Type
boolean

---
### 🏷️ `context` {#context}
Retrieves the context identifier for the value where `*` is the global (root) context.
#### Type
"*" | string

---
### 🏷️ `hasValue` {#hasValue}
Retrieves if there is a value set.
#### Type
boolean

---
### 🏷️ `immutable` {#immutable}
Retrieves an immutable (write-protected) value instance.
#### Type
[`ImmutableValue`](ImmutableValue.mdx)

---
### 🏷️ `isAwaiting` {#isAwaiting}
Retrieves if the instance is awaiting a value update.
#### Type
boolean

---
### 🏷️ `isDirty` {#isDirty}
Retrieves if the value is dirty. This is the case when there is not value set or the value is not sealed.
#### Type
boolean

---
### 🏷️ `isLocked` {#isLocked}
Retrieves if the value is locked (write-protected).
#### Type
boolean


---
### 🏷️ `isModified` {#isModified}
Retrieves if the value is modified.
#### Type
boolean

---
### 🏷️ `isPreliminarily` {#isPreliminarily}
Retrieves if the value is preliminarily. This means the value is already considered [`sealed`](#isSealed) because the form respondent is answering the block that denotes this value.
#### Type
boolean

---
### 🏷️ `isSealed` {#isSealed}
Retrieves if the value is sealed. A value is considered sealed when the form respondent has seen/answered the block and is past the section that contains the block that denotes this value. Only sealed values can be considered as confirmed by the form respondent. If a value is not sealed, it is not exportable.
#### Type
boolean

---
### 🏷️ `key` {#key}
Retrieves a unique key for the value within its context (the key is prefixed with an underscore).
#### Type
string

---
### 🏷️ `pristine` {#pristine}
Sets or retrieves the pristine value.
#### Type
any

---
### 🏷️ `reference` {#reference}
Retrieves the optional reference that can be set along with the value using the [`set`](#set) method. This reference is used by some blocks to store an additional identifier related to the value. For example, when a value from a dropdown block is set, the reference is used to store the identifier of the selected item.
#### Type
string | undefined

---
### 🏷️ `slot` {#slot}
Retrieves a reference to the slot that defines the value.
#### Type
[`Slot`](../modules/Slots/Slot.mdx)

---
### 🏷️ `string` {#string}
Retrieves the value as a string.
:::caution
This will always return a string with a value regardless of a pristine value was set or not. So, for a value with slot type [`Number`](../modules/Slots/Number.mdx) this will return `"0"` when no (pristine) value was set.
:::
#### Type
string

---
### 🏷️ `time` {#time}
Retrieves the timestamp of the last value update as the number of milliseconds since the [ECMAScript epoch](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date#the_ecmascript_epoch_and_timestamps).
#### Type
number | undefined

---
### 🏷️ `value` {#value}
Sets or retrieves the typed value.
:::info
The type `T` is the value type denoted by the [`Slot`](../modules/Slots/Slot.mdx) type of the value.
:::
:::caution
This will always return a valid value regardless of a pristine value was set or not. So, for a value with slot type [`Number`](../modules/Slots/Number.mdx) this will return `0` when no (pristine) value was set.
:::
#### Type
T

## ▶️ Methods {#methods}

---
### 🔧 `await` {#await}
Puts the `Value` instance in await mode. This indicates an asynchronous operation is executed and a value is pending. When a value is [set](#set) or [`cancelAwait`](#cancelAwait) is called, the await state ends.
#### Signature
```ts
await(): this
```
#### Return value
Returns a reference to the `Value` instance.

---
### 🔧 `cancelAwait` {#cancelAwait}
Cancels a value await state.
#### Signature
```ts
cancelAwait(): this
```
#### Return value
Returns a reference to the `Value` instance.

---
### 🔧 `clear` {#clear}
Clears a value.
:::info
The type `T` in the signature is the value type denoted by the [`Slot`](../modules/Slots/Slot.mdx) type of the value.
:::
#### Signature
```ts
clear(): T
```
#### Return value
Returns the value after the pristine value is cleared.

---
### 🔧 `concatenate` {#concatenate}
Concatenates a value to the current value.
:::caution
Concatenation is only possible with string values.
:::
#### Signature
```ts
concatenate(value: string, context?: {} | string, separator?: string): string
```
#### Parameters
| Name        | Type   | Optional | Description                         |
|:------------|:-------|:---------|:------------------------------------|
| `value`     | string | No       | Specifies the value to concatenate. |
| `context`   | string | Yes      | Specifies the context of the value. |
| `separator` | string | Yes      | Specifies a separator to use.       |
#### Return value
Returns the concatenated string.

---
### 🔧 `confirm` {#confirm}
Marks the value as confirmed. In this case it is not necessary to explicit [set](#set) a value to make the value valid.
#### Signature
```ts
confirm(): this
```
#### Return value
Returns a reference to the `Value` instance.

---
### 🔧 `default` {#default}
Sets a default value.
:::info
The type `T` in the signature is the value type denoted by the [`Slot`](../modules/Slots/Slot.mdx) type of the value.
:::
#### Signature
```ts
default(value: any, reference?: string): T
```
#### Parameters
| Name        | Type   | Optional | Description                                                     |
|:------------|:-------|:---------|:----------------------------------------------------------------|
| `value`     | any    | No       | Specifies the pristine value to set.                            |
| `reference` | string | Yes      | Specifies the optional reference to store along with the value. |
#### Return value
Returns the value that was set.

---
### 🔧 `dirty` {#dirty}
Marks the value as dirty. An explicit value should be [set](#set) to mark the value as valid or [`confirm`](#confirm) should be invoked.
#### Signature
```ts
dirty(): this
```
#### Return value
Returns a reference to the `Value` instance.

---
### 🔧 `lock` {#lock}
Locks a value so it becomes write-protected.
#### Signature
```ts
lock(): void
```

---
### 🔧 `operation` {#operation}
Sets the output of an operation as value. The difference with the [`set`](#set) method is that operations are grouped and emitted to the runner as a single value change instead of separate events for each value change. This is useful for blocks that change the value multiple times within a single tick.
#### Signature
```ts
operation(value: any, context?: {} | string): this
```
#### Parameters
| Name        | Type   | Optional | Description                             |
|:------------|:-------|:---------|:----------------------------------------|
| `value`     | string | No       | Specifies the value to set.             |
| `context`   | string | Yes      | Specifies the context of the operation. |
#### Return value
Returns a reference to the `Value` instance.

---
### 🔧 `reset` {#reset}
Resets a value to the initial value or the optional supplied prefill value.
:::info
The type `T` in the signature is the value type denoted by the [`Slot`](../modules/Slots/Slot.mdx) type of the value.
:::
#### Signature
```ts
reset(prefillValue: any, prefillReference?: string): T
```
#### Parameters
| Name               | Type   | Optional | Description                                                     |
|:-------------------|:-------|:---------|:----------------------------------------------------------------|
| `prefillValue`     | any    | Yes      | Specifies the prefill value to set.                             |
| `prefillReference` | string | Yes      | Specifies the optional reference to store along with the value. |
#### Return value
Returns the value that was set.

---
### 🔧 `set` {#set}
Sets the pristine value and optional reference.
:::info
The type `T` in the signature is the value type denoted by the [`Slot`](../modules/Slots/Slot.mdx) type of the value.
:::
#### Signature
```ts
set(value: any, reference?: string): T
```
#### Parameters
| Name        | Type   | Optional | Description                                                     |
|:------------|:-------|:---------|:----------------------------------------------------------------|
| `value`     | any    | No       | Specifies the pristine value to set.                            |
| `reference` | string | Yes      | Specifies the optional reference to store along with the value. |
#### Return value
Returns the value that was set.

---
### 🔧 `subscribe` {#subscribe}
Subscribe to value changes using the given context.
#### Signature
```ts
subscribe(
    fnChange: (value: Value) => void,
    context?: {},
    mode?: "asynchronous" | "synchronous"
): this
```
#### Parameters
| Name       | Type                            | Optional | Description                                                       |
|:-----------|:--------------------------------|:---------|:------------------------------------------------------------------|
| `fnChange` | (value: `Value`) => void        | No       | Specifies the function that is invoked when the value is changed. |
| `context`  | `{}`                            | Yes      | Specifies the context.                                            |
| `mode`     | "asynchronous" \| "synchronous" | Yes      | Sepcifies the mode to use (defaults to `asynchronous`).           |
#### Return value
Returns a reference to the `Value` instance.

---
### 🔧 `unlock` {#unlock}
Unlocks a value that is locked.
#### Signature
```ts
unlock(): void
```

---
### 🔧 `unsubscribe` {#unsubscribe}
Unsubscribe the given context from the `Value` istance.
#### Signature
```ts
unsubscribe(context?: {}): this
```
#### Parameters
| Name      | Type | Optional | Description                                                         |
|:----------|:-----|:---------|:--------------------------------------------------------------------|
| `context` | `{}` | Yes      | Specifies the context. If omitted all subscribers are unsubscribed. |
#### Return value
Returns a reference to the `Value` instance.
