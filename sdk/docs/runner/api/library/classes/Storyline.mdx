---
title: Storyline class - Runner
sidebar_label: Storyline
description: The Storyline class helps to render runners by supplying all the required properties and methods for rendering the form (Runner library).
---

# Storyline class

## 📖 Description {#description}
The `Storyline` class helps to render runners by supplying all the required [properties](#properties) and [methods](#methods) for rendering the form.

## 🗃️ Fields {#fields}

---
### 🏷️ `all` {#all}
Retrieves all observable nodes of the active moment (so the nodes of all moments in the [`moments`](#moments) collection) with their corresponding bookmark.
#### Type
`(IObservableNode & { bookmark: IBookmark })[]`

---
### 🏷️ `activePage` {#activePage}
Retrieves the active page.
:::caution
Only available for runners in [paginated mode](#mode).
:::
#### Type
```ts
{
  /* Contains the page number. */
  number: number;
} | undefined
```

---
### 🏷️ `bookmarks` {#bookmarks}
Retrieves the bookmarks in the storyline.
#### Type
[`IBookmark[]`](../interfaces/IBookmark.mdx)

---
### 🏷️ `checksum` {#checksum}
Retrieves the checksum of the storyline.
#### Type
string | undefined

---
### 🏷️ `count` {#count}
Retrieves the total number of observable nodes in the storyline.
#### Type
number

---
### 🏷️ `direction` {#direction}
Retrieves the most recent step direction of the storyline.
#### Type
"forward" | "backward"

---
### 🏷️ `enumerators` {#enumerators}
Retrieves the total number of observable nodes that have an enumerator value.
#### Type
number

---
### 🏷️ `epilogue` {#epilogue}
Retrieves the optional epilogue for the form. This epilogue can be used by runners (that support it) to show a closing message when the form is completed.
#### Type
[`IEpilogue`](../interfaces/IEpilogue.mdx)

---
### 🏷️ `hasDataCollected` {#hasDataCollected}
Retrieves if the storyline has any data collected.
#### Type
boolean

---
### 🏷️ `immutable` {#immutable}
Retrieves an immutable representation of the storyline.
#### Type
[`IStoryline`](../interfaces/IStoryline.mdx)

---
### 🏷️ `instance` {#instance}
Retrieves the instance of the storyline.
#### Type
[`Instance`](Instance.mdx)

---
### 🏷️ `isAtFinish` {#isAtFinish}
Retrieves if the present moment is at the finish of the storyline.
#### Type
boolean

---
### 🏷️ `isAtHead` {#isAtHead}
Retrieves if the present moment is at the head of the storyline.
#### Type
boolean

---
### 🏷️ `isAtStart` {#isAtStart}
Retrieves if the present moment is at the beginning of the storyline.
#### Type
boolean

---
### 🏷️ `isChanged` {#isChanged}
Retrieves if the storyline has changed since the last call of this prop.
#### Type
boolean

---
### 🏷️ `isEmpty` {#isEmpty}
Retrieves if the storyline is empty (has no observable nodes, basically an empty form).
#### Type
boolean

---
### 🏷️ `isEvaluating` {#isEvaluating}
Retrieves if the storyline is evaluating.
#### Type
boolean

---
### 🏷️ `isFailed` {#isFailed}
Retrieves if the validation of the storyline failed.
#### Type
boolean

---
### 🏷️ `isFinishable` {#isFinishable}
Retrieves if the storyline is finishable using the [`finish`](#finish) method.
#### Type
boolean

---
### 🏷️ `isFinished` {#isFinished}
Retrieves if the storyline is finished.
#### Type
boolean

---
### 🏷️ `isFinishing` {#isFinishing}
Retrieves if the storyline is finishing.
#### Type
boolean

---
### 🏷️ `isPassed` {#isPassed}
Retrieves if the validation of the storyline passed.
#### Type
boolean

---
### 🏷️ `isPaused` {#isPaused}
Retrieves if the storyline is paused.
#### Type
boolean

---
### 🏷️ `isPausing` {#isPausing}
Retrieves if the storyline is pausing.
#### Type
boolean

---
### 🏷️ `mode` {#mode}
Retrieves the mode of operation. It can be one of the following values:
- `paginated`: Render each section on a separate page (this is the default behavior);
- `continuous`: Render all completed (past) sections and the current (present) section on a page;
- `progressive`: Render all completed (past), current (present) and future sections on a page till the point where one of the sections fails validation;
- `ahead`: Render all completed (past), current (present) and future sections on a page, regardless of the section's validation result.
#### Type
"paginated" | "continuous" | "progressive" | "ahead"

---
### 🏷️ `moments` {#moments}
Retrieves the moments that are in the storyline. There are three types of moments:
- Past moments;
- The present moment;
- Future moments.

:::info
The best way to understand these moment types is to visualize a paginated form runner, where the questions (node blocks) are presented on a page. Each section is a page. All pages that are already answered by the respondent are past moments. The current page the respondent is on is the present moment. And all the pages the respondent still needs to answer are future moments.
:::
#### Type
[`Moment[]`](Moment.mdx)

---
### 🏷️ `nodes` {#nodes}
Retrieves the observable nodes of the active moments (so the nodes of all moments in the [`moments`](#moments) collection).
#### Type
[`IObservableNode[]`](../interfaces/IObservableNode.mdx)

---
### 🏷️ `pages` {#pages}
Retrieves the collection of pages.
:::caution
Only available for runners in [paginated mode](#mode).
:::
#### Type
```ts
{
  /* Contains the page number. */
  number: number;

  /* Contains if the page is active. */
  active: boolean;

  /* Activates the page. */
  activate: () => void;
}[]
```

---
### 🏷️ `percentage` {#percentage}
Retrieves the progress percentage of the storyline.
:::tip
Can be used to show a progressbar that indicates the progress of the form.
:::
#### Type
number

---
### 🏷️ `presentMoment` {#presentMoment}
Retrieves the present moment.
#### Type
[`Moment`](Moment.mdx) | undefined

---
### 🏷️ `presentNodes` {#presentNodes}
Retrieves the observable nodes of the present moment.
#### Type
[`IObservableNode[]`](../interfaces/IObservableNode.mdx)

---
### 🏷️ `presentSection` {#presentSection}
Retrieves the section of the present moment.
#### Type
[`Section`](Section.mdx) | undefined

---
### 🏷️ `prologue` {#prologue}
Retrieves the optional prologue for the form. This prologue can be used by runners (that support it) to show a welcome message before starting the actual form.
#### Type
[`IPrologue`](../interfaces/IPrologue.mdx)

---
### 🏷️ `slides` {#slides}
Retrieves the slides in the storyline.
#### Type
[`ISlide[]`](../interfaces/ISlide.mdx)

## ▶️ Methods {#methods}

---
### 🔧 `finish` {#finish}
Finish the storyline when it is at the end.
:::caution
You can only finish a form when it is [finishable](#isFinishable).
:::
#### Signature
```ts
finish(): Promise<void>
```
#### Return value
Returns a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) that resolves when the instance was finished. The finish operation can be asynchronous. For example, when the form  submits data to an endpoint. During the finish operation you could show a visual indicator to the respondent to indicate the form is submitting. When the promise resolves or rejects, the finish operation ends. When the promise rejects, the form remains active and the respondent should be able to try to finish the form again.

---
### 🔧 `getKeyByNodeId` {#getKeyByNodeId}
Find the node that belongs to the supplied node [identifier](../interfaces/IObservableNode.mdx#id).
#### Signature
```ts
getKeyByNodeId(id: string): IObservableNode | undefined
```
#### Parameters
| Name | Type   | Optional | Description                                                            |
|:-----|:-------|:---------|:-----------------------------------------------------------------------|
| `id` | string | No       | Specifies the node [identifier](../interfaces/IObservableNode.mdx#id). |
#### Return value
Returns the [`IObservableNode`](../interfaces/IObservableNode.mdx) object or `undefined` if the node was not found.

---
### 🔧 `getNodeByKey` {#getNodeByKey}
Find the node that belongs to the supplied [key](../interfaces/IObservableNode.mdx#key).
#### Signature
```ts
getNodeByKey(key: string): IObservableNode | undefined
```
#### Parameters
| Name  | Type   | Optional | Description                                                      |
|:------|:-------|:---------|:-----------------------------------------------------------------|
| `key` | string | No       | Specifies the node [key](../interfaces/IObservableNode.mdx#key). |
#### Return value
Returns the [`IObservableNode`](../interfaces/IObservableNode.mdx) object or `undefined` if the node was not found.

---
### 🔧 `map` {#map}
Maps moments of the storyline to a certain type.
#### Signature
```ts
map<R>(fn: (moment: Moment, index: number) => R): R[]
```
#### Parameters
| Name | Type                                                 | Optional | Description                 |
|:-----|:-----------------------------------------------------|:---------|:----------------------------|
| `fn` | (moment: [`Moment`](Moment.mdx), index: number) => R | No       | Specifies the map function. |
#### Return value
Returns the resulting array of the map operation.

---
### 🔧 `stepBackward` {#stepBackward}
Step backward in the storyline.
#### Signature
```ts
stepBackward(): boolean
```
#### Return value
Returns `true` if the step succeeded.

---
### 🔧 `stepForward` {#stepForward}
Step forward in the storyline.
#### Signature
```ts
stepForward(): boolean
```
#### Return value
Returns `true` if the step succeeded.

---
### 🔧 `stepToHead` {#stepToHead}
Step to the head of the storyline.
#### Signature
```ts
stepToHead(): boolean
```
#### Return value
Returns `true` if the step succeeded.

---
### 🔧 `stepToPage` {#stepToPage}
Steps to a page.
:::caution
Only available for runners in [paginated mode](#mode).
:::
#### Signature
```ts
stepToPage(page: number): boolean
```
#### Parameters
| Name   | Type   | Optional | Description                    |
|:-------|:-------|:---------|:-------------------------------|
| `page` | number | No       | Specifies the page to step to. |
#### Return value
Returns `true` if the step succeeded.

---
### 🔧 `stepToStart` {#stepToStart}
Step to the start of the storyline.
#### Signature
```ts
stepToStart(): boolean
```
#### Return value
Returns `true` if the step succeeded.
