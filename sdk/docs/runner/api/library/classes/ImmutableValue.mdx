---
title: ImmutableValue class - Runner
sidebar_label: ImmutableValue
description: The ImmutableValue class defines an immutable version of a Value instance (Runner library).
---

# ImmutableValue class

## 📖 Description {#description}
The `ImmutableValue` class defines an immutable version of a [`Value`](Value.mdx) instance. An `ImmutableValue` instance is obtained using the [`immutableValueOf`](Context.mdx#immutableValueOf) method of a [`Context`](Context.mdx) or using the [`immutable`](Value.mdx#immutable) property of a [`Value`](Value.mdx).

## 🗃️ Fields {#fields}

---
### 🏷️ `context` {#context}
Retrieves the context identifier for the value where `*` is the global (root) context.
#### Type
"*" | string

---
### 🏷️ `hasValue` {#hasValue}
Retrieves if there is a value set.
#### Type
boolean

---
### 🏷️ `isLocked` {#isLocked}
Retrieves if the value is locked (write-protected).
#### Type
boolean

---
### 🏷️ `isModified` {#isModified}
Retrieves if the value is modified.
#### Type
boolean

---
### 🏷️ `isSealed` {#isSealed}
Retrieves if the value is sealed. A value is considered sealed when the form respondent has seen/answered the block and is past the section that contains the block that denotes this value. Only sealed values can be considered as confirmed by the form respondent. If a value is not sealed, it is not exportable.
#### Type
boolean

---
### 🏷️ `key` {#key}
Retrieves a unique key for the value within its context (the key is prefixed with an underscore).
#### Type
string

---
### 🏷️ `reference` {#reference}
Retrieves the optional reference. This reference is used by some blocks to store an additional identifier related to the value. For example, when a value from a dropdown block is set, the reference is used to store the identifier of the selected item.
#### Type
string | undefined

---
### 🏷️ `slot` {#slot}
Retrieves a reference to the slot that defines the value.
#### Type
[`Slot`](../modules/Slots/Slot.mdx)

---
### 🏷️ `string` {#string}
Retrieves the value as a string.
#### Type
string

---
### 🏷️ `time` {#time}
Retrieves the timestamp of the last value update as the number of milliseconds since the [ECMAScript epoch](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date#the_ecmascript_epoch_and_timestamps).
#### Type
number | undefined

---
### 🏷️ `value` {#value}
Retrieves the typed value.
:::info
The type `T` is the value type denoted by the [`Slot`](../modules/Slots/Slot.mdx) type of the value.
:::
#### Type
T

## ▶️ Methods {#methods}

---
### 🔧 `subscribe` {#subscribe}
Subscribe to value changes using the given context.
#### Signature
```ts
subscribe(fnChange: (value: Value) => void, context?: {}): this
```
#### Parameters
| Name       | Type                                  | Optional | Description                                                       |
|:-----------|:--------------------------------------|:---------|:------------------------------------------------------------------|
| `fnChange` | (value: [`Value`](Value.mdx)) => void | No       | Specifies the function that is invoked when the value is changed. |
| `context`  | `{}`                                  | Yes      | Specifies the context.                                            |
#### Return value
Returns a reference to the `ImmutableValue` instance.

---
### 🔧 `unsubscribe` {#unsubscribe}
Unsubscribe the given context from the `ImmutableValue` instance.
#### Signature
```ts
unsubscribe(context?: {}): this
```
#### Parameters
| Name      | Type | Optional | Description                                                         |
|:----------|:-----|:---------|:--------------------------------------------------------------------|
| `context` | `{}` | Yes      | Specifies the context. If omitted all subscribers are unsubscribed. |
#### Return value
Returns a reference to the `ImmutableValue` instance.
