---
title: Runner React Hook API
description: The Runner React Hook package wraps the functionality of the Runner library into an easy to use React Hook.
---

# ![](/img/logo-ts.svg) Runner React Hook API reference
The [Runner React Hook](https://www.npmjs.com/package/@tripetto/runner-react-hook) package wraps the functionality of the [Runner library](../library/index.md) into an easy to use [React Hook](https://react.dev/reference/react). It greatly simplifies the process of building a runner for Tripetto using [React](https://react.dev). All of Tripetto's [stock runners](../../stock/introduction.md) are built using this hook.

## ✨ Installation [![](/img/logo-npm-icon.svg)](https://www.npmjs.com/package/@tripetto/runner-react-hook) {#installation}

```bash npm2yarn
npm install @tripetto/runner-react-hook
```
:::caution
This package needs the following packages: [@tripetto/runner](https://www.npmjs.com/package/@tripetto/runner), [react](https://www.npmjs.com/package/react), and [tslib](https://www.npmjs.com/package/tslib). You have to install those peer dependencies yourself.
:::

:::tip Typescript support
This package contains type declarations and supports [TypeScript](https://www.typescriptlang.org/) out-of-the-box.
:::

## 🪝 Hooks {#hooks}
- [`useL10n`](hooks/useL10n.mdx)
- [`useRunner`](hooks/useRunner.mdx)

## ▶️ Functions {#functions}
- [`markdownifyToJSX`](functions/markdownifyToJSX.mdx)

## ⛓️ Interfaces {#interfaces}
- [`IRunnerAttachments`](interfaces/IRunnerAttachments.mdx)
- [`IRunner`](interfaces/IRunner.mdx)
- [`IRunnerProps`](interfaces/IRunnerProps.mdx)
- [`IRunnerSequenceItem`](interfaces/IRunnerSequenceItem.mdx)
- [`IRunnerSequenceOptions`](interfaces/IRunnerSequenceOptions.mdx)
