---
title: IRunnerAttachments interface - Runner React Hook
sidebar_label: IRunnerAttachments
description: Interface for defining a store for attachments.
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# IRunnerAttachments interface

## 📖 Description {#description}
Interface for defining a store for attachments.

:::info
Read the [Handling file uploads](../../../stock/guides/attachments.mdx) guide for more information about attachments.
:::

## 📃 Type declaration {#type-declaration}
<InterfaceDeclaration prefix="#" src={`interface IRunnerAttachments {
  get: (file: string) => Promise<Blob>;
  put: (file: File, onProgress?: (percentage: number) => void) => Promise<string>;
  delete: (file: string) => Promise<void>;
}`} symbols={{
  "Promise": "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise",
  "Blob": "https://developer.mozilla.org/en-US/docs/Web/API/Blob",
  "File": "https://developer.mozilla.org/en-US/docs/Web/API/File"
}} />

## ▶️ Functions {#functions}

---
### 🔧 `delete` {#delete}
The `delete` function is used to delete a file from a store.
:::info
Runners typically invoke this function when an uploaded file in the form is removed or replaced with another file.
:::
#### Signature
```ts
(file: string) => Promise<void>
```
#### Parameters
| Name   | Type   | Optional | Description                          |
|:-------|:-------|:---------|:-------------------------------------|
| `file` | string | No       | Identifier of the file in the store. |
#### Return value
Returns a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) that resolves when the file is deleted or rejects when an error occurred.
#### Example
```ts showLineNumbers
// This example `delete` function uses Superagent to delete a file
(file: string) =>
  new Promise<void>((resolve: () => void, reject: () => void) => {
    Superagent.delete("https://url-to-filestore/delete")
      .send({ file })
      .then((response: Superagent.Response) => {
        if (response.ok) {
          return resolve();
        } else {
          return reject();
        }
      })
      .catch(() => reject());
  })
```

---
### 🔧 `get` {#get}
The `get` function is used to retrieve a file from a store.
:::info
Runners typically invoke this function when they want to retrieve an uploaded file for generating a preview (thumbnail) in the form.
:::
#### Signature
```ts
(file: string) => Promise<Blob>
```
#### Parameters
| Name   | Type   | Optional | Description                          |
|:-------|:-------|:---------|:-------------------------------------|
| `file` | string | No       | Identifier of the file in the store. |
#### Return value
Returns a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) that resolves to a [`Blob`](https://developer.mozilla.org/en-US/docs/Web/API/Blob) that contains the file data. If the retrieval of the file fails, the promise should reject.
#### Example
```ts showLineNumbers
// This example `get` function uses Superagent to download a file
(id: string) =>
  new Promise<Blob>((resolve: (data: Blob) => void, reject: () => void) => {
    Superagent.get(`https://url-to-filestore/get/${id}`)
      .responseType("blob")
      .then((response: Superagent.Response) => {
        if (response.ok) {
          resolve(response.body);
        } else {
          reject();
        }
      })
      .catch(() => reject());
  })
```

---
### 🔧 `put` {#put}
The `put` function is used to upload a file to a store.
:::info
Runners typically invoke this function when they want to upload a file for a form.
:::
#### Signature
```ts
(file: File, onProgress?: (percentage: number) => void) => Promise<string>
```
#### Parameters
| Name         | Type                                                            | Optional | Description                                                                                   |
|:-------------|:----------------------------------------------------------------|:---------|:----------------------------------------------------------------------------------------------|
| `file`       | [`File`](https://developer.mozilla.org/en-US/docs/Web/API/File) | No       | Contains the file to upload.                                                                  |
| `onProgress` | (percentage: number) => void                                    | Yes      | Contains the progress function (when supported) to report the progress of the upload process. |
#### Example
```ts showLineNumbers
// This example `put` function uses Superagent to upload a file and report progress
(file: File, onProgress?: (percentage: number) => void) =>
  new Promise<string>(
    (resolve: (id: string) => void, reject: (reason?: string) => void) => {
      const formData = new FormData();

      formData.append("file", file);

      Superagent.post("https://url-to-filestore/upload")
        .send(formData)
        .then((response: Superagent.Response) => {
          if (response.ok && res.body?.id) {
            // Report the file identifier back to the runner.
            // This identifier is what's stored in the form dataset
            return resolve(response.body.id);
          } else {
            return reject(
              response.status === 413 ? "File is too large." : undefined
            );
          }
        })
        // Report progress back to the runner
        .on("progress", (event: Superagent.ProgressEvent) => {
          if (onProgress && event.direction === "upload") {
            onProgress(event.percent || 0);
          }
        })
        .catch(() => reject());
    }
  )
```
