---
title: IRunnerSequenceOptions interface - Runner React Hook
sidebar_label: IRunnerSequenceOptions
description: Options interface for enabling the sequence mode in the useRunner hook.
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# IRunnerSequenceOptions interface

## 📖 Description {#description}
Options interface for enabling the `sequence` mode in the [`useRunner`](../hooks/useRunner.mdx) hook. In sequence mode, all blocks (questions as you will) of a form are placed in a sequence array. Only one item in the sequence can be activated at once. This makes it easier to make visual (animable) transitions when moving through the blocks. This is useful for certain types of runners, such as the [chat stock runner](../../../stock/faces/chat.mdx).

## 📃 Type declaration {#type-declaration}
<InterfaceDeclaration prefix="#" src={`interface IRunnerSequenceOptions {
  mode: "sequence";
  namespace?: string;
  autoStart?: boolean;
  onInteraction?: () => void;
  onPreview?: (
    action: "start" | "end",
    type: "prologue" | "block" | "epilogue",
    blockId?: string
  ) => void;
  onRestart?: () => void;
  onDestroy?: () => void;
  canActivate?: (item: IRunnerSequenceItem) => boolean;
  canInteract?: (item: IRunnerSequenceItem) => boolean;
  canSkip?: (item: IRunnerSequenceItem) => boolean;
  preActiveDuration?: (item: IRunnerSequenceItem) => number | undefined;
  beforeActiveDuration?: (item: IRunnerSequenceItem) => number | undefined;
  afterActiveDuration?: (item: IRunnerSequenceItem) => number | undefined;
  postActiveDuration?: (item: IRunnerSequenceItem) => number | undefined;
  banner?: (l10n: L10n.Namespace) => IEpilogue;
}`} symbols={{
  "L10n.Namespace": "/runner/api/library/modules/L10n/Namespace/",
  "IRunnerSequenceItem": "/runner/api/react-hook/interfaces/IRunnerSequenceItem/",
  "IEpilogue": "/runner/api/library/interfaces/IEpilogue/"
}} />

## 🗃️ Properties {#properties}

---
### 🏷️ `autoStart` {#autoStart}
Specifies if the runner should automatically start.
#### Type
boolean

---
### 🏷️ `mode` {#mode}
Specifies the runner is in `sequence` mode.
#### Type
"sequence"

---
### 🏷️ `namespace` {#namespace}
Specifies the identifier of the namespace to use for the runner.
#### Type
string

## ▶️ Functions {#functions}

---
### 🔧 `afterActiveDuration` {#afterActiveDuration}
Invoked when the runner wants to know if there should be a certain time duration (in milliseconds) for the after-active stage of a [sequence item](IRunnerSequenceItem.mdx).
:::info
This is used to control the timing of animations for the transition between items.
:::
#### Signature
```ts
(item: IRunnerSequenceItem) => number | undefined
```
#### Parameters
| Name   | Type                                             | Optional | Description            |
|:-------|:-------------------------------------------------|:---------|:-----------------------|
| `item` | [`IRunnerSequenceItem`](IRunnerSequenceItem.mdx) | No       | Reference to the item. |
#### Return value
Specifies the duration in milliseconds or `undefined` if this stage should be skipped.

---
### 🔧 `banner` {#banner}
Specifies a banner to show in the runner.
#### Signature
```ts
(l10n: L10n.Namespace) => IEpilogue
```
#### Parameters
| Name   | Type                                                            | Optional | Description                             |
|:-------|:----------------------------------------------------------------|:---------|:----------------------------------------|
| `l10n` | [`L10n.Namespace`](../../library/modules/L10n/Namespace.mdx) | No       | Reference to the translation namespace. |
#### Return value
Specifies the banner as an [`IEpilogue`](../../../../builder/api/interfaces/IEpilogue.mdx) object.

---
### 🔧 `beforeActiveDuration` {#beforeActiveDuration}
Invoked when the runner wants to know if there should be a certain time duration (in milliseconds) for the before-active stage of a [sequence item](IRunnerSequenceItem.mdx).
:::info
This is used to control the timing of animations for the transition between items.
:::
#### Signature
```ts
(item: IRunnerSequenceItem) => number | undefined
```
#### Parameters
| Name   | Type                                             | Optional | Description            |
|:-------|:-------------------------------------------------|:---------|:-----------------------|
| `item` | [`IRunnerSequenceItem`](IRunnerSequenceItem.mdx) | No       | Reference to the item. |
#### Return value
Specifies the duration in milliseconds or `undefined` if this stage should be skipped.

---
### 🔧 `canActivate` {#canActivate}
Invoked when the runner wants to know if a certain [sequence item](IRunnerSequenceItem.mdx) can be activated.
#### Signature
```ts
(item: IRunnerSequenceItem) => boolean
```
#### Parameters
| Name   | Type                                             | Optional | Description            |
|:-------|:-------------------------------------------------|:---------|:-----------------------|
| `item` | [`IRunnerSequenceItem`](IRunnerSequenceItem.mdx) | No       | Reference to the item. |
#### Return value
Should return `true` when the item can be activated.

---
### 🔧 `canInteract` {#canInteract}
Invoked when the runner wants to know if the user can interact with a certain [sequence item](IRunnerSequenceItem.mdx).
#### Signature
```ts
(item: IRunnerSequenceItem) => boolean
```
#### Parameters
| Name   | Type                                             | Optional | Description            |
|:-------|:-------------------------------------------------|:---------|:-----------------------|
| `item` | [`IRunnerSequenceItem`](IRunnerSequenceItem.mdx) | No       | Reference to the item. |
#### Return value
Should return `true` when the item can interact.

---
### 🔧 `canSkip` {#canSkip}
Invoked when the runner wants to know if a certain [sequence item](IRunnerSequenceItem.mdx) can be skipped.
#### Signature
```ts
(item: IRunnerSequenceItem) => boolean
```
#### Parameters
| Name   | Type                                             | Optional | Description            |
|:-------|:-------------------------------------------------|:---------|:-----------------------|
| `item` | [`IRunnerSequenceItem`](IRunnerSequenceItem.mdx) | No       | Reference to the item. |
#### Return value
Should return `true` when the item can be skipped.

---
### 🔧 `postActiveDuration` {#postActiveDuration}
Invoked when the runner wants to know if there should be a certain time duration (in milliseconds) for the post-active stage of a [sequence item](IRunnerSequenceItem.mdx).
:::info
This is used to control the timing of animations for the transition between items.
:::
#### Signature
```ts
(item: IRunnerSequenceItem) => number | undefined
```
#### Parameters
| Name   | Type                                             | Optional | Description            |
|:-------|:-------------------------------------------------|:---------|:-----------------------|
| `item` | [`IRunnerSequenceItem`](IRunnerSequenceItem.mdx) | No       | Reference to the item. |
#### Return value
Specifies the duration in milliseconds or `undefined` if this stage should be skipped.

---
### 🔧 `preActiveDuration` {#preActiveDuration}
Invoked when the runner wants to know if there should be a certain time duration (in milliseconds) for the pre-active stage of a [sequence item](IRunnerSequenceItem.mdx).
:::info
This is used to control the timing of animations for the transition between items.
:::
#### Signature
```ts
(item: IRunnerSequenceItem) => number | undefined
```
#### Parameters
| Name   | Type                                             | Optional | Description            |
|:-------|:-------------------------------------------------|:---------|:-----------------------|
| `item` | [`IRunnerSequenceItem`](IRunnerSequenceItem.mdx) | No       | Reference to the item. |
#### Return value
Specifies the duration in milliseconds or `undefined` if this stage should be skipped.

## 📢 Events {#events}

---
### 🔔 `onDestroy` {#onDestroy}
Invoked when the runner is destroyed.
#### Signature
```ts
() => void
```

---
### 🔔 `onInteraction` {#onInteraction}
Invoked when there is user interaction with the runner.
#### Signature
```ts
() => void
```

---
### 🔔 `onPreview` {#onPreview}
Invoked when the builder requests a preview.
#### Signature
```ts
(
  action: "start" | "end",
  type: "prologue" | "block" | "epilogue",
  blockId?: string
) => void
```
#### Parameters
| Name      | Type                                | Optional | Description                                            |
|:----------|:------------------------------------|:---------|:-------------------------------------------------------|
| `action`  | "start" \| "end"                    | No       | Specifies the preview action.                          |
| `type`    | "prologue" \| "block" \| "epilogue" | No       | Specifies the preview type.                            |
| `blockId` | string                              | Yes      | Specifies the block identifier when `type` is `block`. |

---
### 🔔 `onRestart` {#onRestart}
Invoked when the runner is restarted.
#### Signature
```ts
() => void
```
