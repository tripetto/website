---
title: IRunnerProps interface - Runner React Hook
sidebar_label: IRunnerProps
description: Properties interface for configuring the useRunner hook.
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# IRunnerProps interface

## 📖 Description {#description}
Properties interface for configuring the [`useRunner`](../hooks/useRunner.mdx) hook.

## 📃 Type declaration {#type-declaration}
<InterfaceDeclaration prefix="#" src={`interface IRunnerProps {
  definition: IDefinition;
  snapshot?: ISnapshot;
  l10nNamespace?: L10n.Namespace;
  view?: "live" | "test" | "preview";
  attachments?: IRunnerAttachments;
  onReady?: (instance?: Instance) => void;
  onChange?: (instance: Instance) => void;
  onImport?: (instance: Instance) => void;
  onData?: (instance: Instance) => void;
  onAction?: (
    type: "start" | "stage" | "unstage" | "focus" | "blur" | "pause" | "complete",
    definition: {
        fingerprint: string;
        name: string;
    },
    block?: {
        id: string;
        name: string;
        alias?: string;
    }
  ) => void;
  onSubmit?: (
    instance: Instance,
    language: string,
    locale: string,
    namespace: string | undefined
  ) => Promise<string | undefined> | boolean | void;
  onComplete?: (instance: Instance, id?: string) => void;
  onDestroy?: () => void;
}`} symbols={{
  "Promise": "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise",
  "L10n.Namespace": "/runner/api/library/modules/L10n/Namespace/",
  "Instance": "/runner/api/library/classes/Instance/",
  "IRunnerAttachments": "/runner/api/react-hook/interfaces/IRunnerAttachments/",
  "IDefinition": "/runner/api/library/interfaces/IDefinition/",
  "ISnapshot": "/runner/api/library/interfaces/ISnapshot/",
}} />

## 🗃️ Properties {#properties}

### 🏷️ `attachments` {#attachments}
Specifies the attachments handler for processing file uploads.
#### Type
[`IRunnerAttachments`](IRunnerAttachments.mdx)

---
### 🏷️ `definition` {#definition}
Specifies the [form definition](../../../../builder/api/interfaces/IDefinition.mdx) to use.
#### Type
[`IDefinition`](../../../../builder/api/interfaces/IDefinition.mdx)

---
### 🏷️ `l10nNamespace` {#l10nNamespace}
Specifies the l10n namespace to use for translations and locale data.
#### Type
[`L10n.Namespace`](../../library/modules/L10n/Namespace.mdx)

---
### 🏷️ `snapshot` {#snapshot}
Specifies the snapshot that should be restored.
#### Type
[`ISnapshot`](../../library/interfaces/ISnapshot.mdx)

---
### 🏷️ `view` {#view}
Sets the initial view mode of the runner. It can be one of the following values:
- `live`: The form is running in normal (production) mode;
- `test`: The form is running in test mode (no response data is submitted);
- `preview`: The form is running in preview mode, which shows all blocks in a single view.
#### Type
"live" | "test" | "preview"

## 📢 Events {#events}

---
### 🔔 `onAction` {#onAction}
Specifies a function that is invoked when the user performs an action. This is useful when you need to implement a tracking service like [Google Analytics](../../../stock/guides/tracking.mdx#google-analytics),  [Google Tag Manager](../../../stock/guides/tracking.mdx#gtm), or [FaceBook Pixel](../../../stock/guides/tracking.mdx#facebook-pixel).
:::tip
Have a look at the [Tracking guide](../../../stock/guides/tracking.mdx) to learn more about this feature.
:::
#### Signature
```ts
(
  type: "start" | "stage" | "unstage" | "focus" | "blur" | "pause" | "complete",
  definition: {
    fingerprint: string;
    name: string;
  },
  block?: {
    id: string;
    name: string;
    alias?: string;
  }
) => void
```
#### Parameters
| Name         | Type                                                                          | Optional | Description                                                                                                                                                                                                                                                                                                                                               |
|:-------------|:------------------------------------------------------------------------------|:---------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `type`       | "start" \| "stage" \| "unstage" \| "focus" \| "blur" \| "pause" \| "complete" | No       | Contains the action type. It can be one of the following values:<br />- `start`: A form is started;<br />- `stage`: A block gets activated;<br />- `unstage`: A block gets deactivated;<br />- `focus`: An input element gains focus;<br />- `blur`: An input element loses focus;<br />- `pause`: A form is paused;<br />- `complete`: A form is completed. |
| `definition` | object                                                                        | No       | Contains information about the form definition.                                                                                                                                                                                                                                                                                                           |
| `block`      | object  \| undefined                                                          | Yes       | Contains information about the blocks.                                                                                                                                                                                                                                                                                                                    |

---
### 🔔 `onChange` {#onChange}
Specifies a function that is invoked when there is a change in the runner. This change can be anything from a data update to a UI rendering cycle.
:::tip
If you only need updates on data changes, use the [`onData`](#onData) event.
:::
#### Signature
```ts
(instance: Instance) => void
```
#### Parameters
| Name       | Type                                                | Optional | Description                       |
|:-----------|:----------------------------------------------------|:---------|:----------------------------------|
| `instance` | [`Instance`](../../library/classes/Instance.mdx) | No       | Reference to the runner instance. |

---
### 🔔 `onComplete` {#onComplete}
Specifies a function that is invoked when the runner is completed (after the data is submitted).
#### Signature
```ts
(instance: Instance, id?: string) => void
```
#### Parameters
| Name       | Type                                                | Optional | Description                                                          |
|:-----------|:----------------------------------------------------|:---------|:---------------------------------------------------------------------|
| `instance` | [`Instance`](../../library/classes/Instance.mdx) | No       | Reference to the runner instance.                                    |
| `id`       | string \| undefined                                 | Yes      | Contains an optional identifier or reference returned by the server. |

---
### 🔔 `onData` {#onData}
Specifies a function that is invoked when there is a data change.
#### Signature
```ts
(instance: Instance) => void
```
#### Parameters
| Name       | Type                                                | Optional | Description                       |
|:-----------|:----------------------------------------------------|:---------|:----------------------------------|
| `instance` | [`Instance`](../../library/classes/Instance.mdx) | No       | Reference to the runner instance. |

---
### 🔔 `onDestroy` {#onDestroy}
Specifies a function that is invoked when the component unmounts.
#### Signature
```ts
() => void
```

---
### 🔔 `onImport` {#onImport}
Specifies a function that is invoked when data can be imported into the instance. To import form data, you need to use one of the [`Import`](../../library/modules/Import.mdx) functions from the Runner library as shown in the [Prefilling forms guide](../../../stock/guides/prefilling.mdx).
:::tip
Have a look at the [Prefilling forms guide](../../../stock/guides/prefilling.mdx) to learn more about importing data.
:::
#### Signature
```ts
(instance: Instance) => void
```
#### Parameters
| Name       | Type                                                | Optional | Description                       |
|:-----------|:----------------------------------------------------|:---------|:----------------------------------|
| `instance` | [`Instance`](../../library/classes/Instance.mdx) | No       | Reference to the runner instance. |

---
### 🔔 `onReady` {#onReady}
Specifies a function that is invoked when the runner is ready.
#### Signature
```ts
(instance: Instance | undefined) => void
```
#### Parameters
| Name       | Type                                                             | Optional | Description                                                             |
|:-----------|:-----------------------------------------------------------------|:---------|:------------------------------------------------------------------------|
| `instance` | [`Instance`](../../library/classes/Instance.mdx) \| undefined | No       | Reference to the runner instance or `undefined` if no form has started. |

---
### 🔔 `onSubmit` {#onSubmit}
Specifies a function that is invoked when the runner submits data. This event is the one to use to submit the form data to an endpoint. To retrieve the form data, you need to use one of the [`Export`](../../library/modules/Export.mdx) functions from the Runner library.

This event supports different return types. First of all, you can return a boolean value to indicate if the submission was successful or not. That's only useful for synchronous operations. If you need async operation, you can return a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) which resolves in case of a successful operation or rejects when the submission fails. The [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) can resolve with an optional string reference.
#### Signature
```ts
(instance: Instance, language: string, locale: string, namespace: string) =>
  Promise<string | undefined> | boolean | void
```
#### Parameters
| Name        | Type                                                | Optional | Description                                                          |
|:------------|:----------------------------------------------------|:---------|:---------------------------------------------------------------------|
| `instance`  | [`Instance`](../../library/classes/Instance.mdx) | No       | Reference to the runner instance.                                    |
| `language`  | string                                              | No       | Contains the language used in the runner (default is `en`).          |
| `locale`    | string                                              | No       | Contains the locale identifier used in the runner (default is `en`). |
| `namespace` | string                                              | No       | Contains the namespace identifier for the runner.                    |
#### Return value
Returns `true` when the submission succeeded, `false` if it failed or a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) that resolves with an optional reference string (in case it succeeds) or rejects (in case of an error).
