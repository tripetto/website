---
title: IRunner interface - Runner React Hook
sidebar_label: IRunner
description: Interface that describes the return type of the useRunner hook.
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# IRunner interface

## 📖 Description {#description}
Interface that describes the return type of the [`useRunner`](../hooks/useRunner.mdx) hook.

## 📃 Type declaration {#type-declaration}
<InterfaceDeclaration prefix="#" src={`interface IRunner {
  definition: IDefinition;
  view: "live" | "test" | "preview";
  mode: "paginated" | "continuous" | "progressive" | "ahead" | undefined;
  readonly l10n: L10n.Namespace;
  readonly instance: Instance | undefined;
  readonly fingerprint: string;
  readonly stencil: string;
  readonly actionables: string;
  readonly storyline: Storyline | IRunnerSequenceItem[] | undefined;
  readonly prologue: IPrologue | undefined;
  readonly epilogue: IEpilogue | undefined;
  readonly preview: "prologue" | "blocks" | "epilogue" | undefined;
  readonly status: "empty" | "idle" | "running" | "preview" | "evaluating" | "stopped" | "finishing" | "finished" | "pausing" | "paused" | "error" | "error-outdated" | "error-rejected" | "error-paused";
  readonly isRunning: boolean;
  readonly isFinishing: boolean;
  readonly isEvaluating: boolean;
  readonly isPausing: boolean;
  readonly allowStart: boolean;
  readonly allowRestart: boolean;
  readonly allowPause: boolean;
  readonly allowStop: boolean;
  readonly kickOff: () => void;
  readonly start: () => Instance | undefined;
  readonly restart: () => void;
  readonly reload: (definition: IDefinition) => void;
  readonly pause: ( //@function
    data?: {},
    pausing?: (
      pSnapshot: ISnapshot,
      done: (result: "succeeded" | "failed" | "canceled") => void,
      item: IRunnerSequenceItem | undefined
    ) => void
  ) => Promise<ISnapshot> | ISnapshot | undefined;
  readonly snapshot: (data?: {}) => ISnapshot | undefined;
  readonly stop: () => void;
  readonly update: () => void;
  readonly discard: () => void;
  readonly doAction: (type: "stage" | "unstage" | "focus" | "blur", node?: IObservableNode) => void;
  readonly doPreview: (data: TRunnerPreviewData) => void;
  readonly resetPreview: () => void;
}`} symbols={{
  "Promise": "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise",
  "L10n.Namespace": "/runner/api/library/modules/L10n/Namespace/",
  "Instance": "/runner/api/library/classes/Instance/",
  "Storyline": "/runner/api/library/classes/Storyline/",
  "IRunnerSequenceItem": "/runner/api/react-hook/interfaces/IRunnerSequenceItem/",
  "IDefinition": "/runner/api/library/interfaces/IDefinition/",
  "ISnapshot": "/runner/api/library/interfaces/ISnapshot/",
  "IObservableNode": "/runner/api/library/interfaces/IObservableNode/",
  "IPrologue": "/runner/api/library/interfaces/IEpilogue/",
  "IEpilogue": "/runner/api/library/interfaces/IEpilogue/",
  "TRunnerPreviewData": "https://gitlab.com/tripetto/runner-react-hook/-/blob/master/src/lib/preview.ts#L23"
}} />

## 🗃️ Properties {#properties}

---
### 🏷️ `actionables` {#actionables}
Retrieves the form stencil hash for the actionable data.
#### Type
string

:::tip
More information about the stencil hashes in the [Form data stencil guide](../../../../builder/integrate/guides/stencil.mdx). More information about exportable vs. actionable data can be found [here](../../../stock/guides/collecting.mdx#exportable-vs-actionable).
:::

---
### 🏷️ `allowPause` {#allowPause}
Retrieves if the runner can be paused.
#### Type
boolean

---
### 🏷️ `allowRestart` {#allowRestart}
Retrieves if the runner can be restarted.
#### Type
boolean

---
### 🏷️ `allowStart` {#allowStart}
Retrieves if the runner can be started.
#### Type
boolean

---
### 🏷️ `allowStop` {#allowStop}
Retrieves if the runner can be stopped.
#### Type
boolean

---
### 🏷️ `definition` {#definition}
Sets or retrieves the current [form definition](../../../../builder/api/interfaces/IDefinition.mdx) that is running.
#### Type
[`IDefinition`](../../../../builder/api/interfaces/IDefinition.mdx)

---
### 🏷️ `epilogue` {#epilogue}
Retrieves the optional epilogue for the form. This epilogue can be used by runners (that support it) to show a closing message when the form is completed.
#### Type
[`IEpilogue`](../../library/interfaces/IEpilogue.mdx)

---
### 🏷️ `fingerprint` {#fingerprint}
Retrieves the fingerprint of the form.
#### Type
string

:::tip
More information in the [Form fingerprint guide](../../../../builder/integrate/guides/fingerprint.mdx).
:::

---
### 🏷️ `instance` {#instance}
Retrieves the instance of the current running form.
#### Type
[`Instance`](../../library/classes/Instance.mdx) | undefined

---
### 🏷️ `isEvaluating` {#isEvaluating}
Retrieves if the runner is evaluating a block or condition.
#### Type
boolean

---
### 🏷️ `isFinishing` {#isFinishing}
Retrieves if the runner is finishing (response data is being transmitted).
#### Type
boolean

---
### 🏷️ `isPausing` {#isPausing}
Retrieves if the runner is pausing (pause data is being transmitted).
#### Type
boolean

---
### 🏷️ `isRunning` {#isRunning}
Retrieves if the runner is running.
#### Type
boolean

---
### 🏷️ `l10n` {#l10n}
Retrieves the localization namespace with helper functions for working with translations and locales.
#### Type
[`L10n.Namespace`](../../library/modules/L10n/Namespace.mdx)

---
### 🏷️ `mode` {#mode}
Sets or retrieves the current mode of operation for the runner. It can be one of the following values:
- `paginated`: Render each section on a separate page (this is the default behavior);
- `continuous`: Render all completed (past) sections and the current (present) section on a page;
- `progressive`: Render all completed (past), current (present) and future sections on a page till the point where one of the sections fails validation;
- `ahead`: Render all completed (past), current (present) and future sections on a page, regardless of the section's validation result.

:::info
When the runner is in `sequence` mode, this property returns `undefined`.
:::
#### Type
"paginated" | "continuous" | "progressive" | "ahead" | undefined

---
### 🏷️ `preview` {#preview}
Retrieves the current preview mode of the runner. It can be one of the following values:
- `prologue`: Prologue preview is active;
- `blocks`: Blocks preview is active;
- `epilogue`: Epilogue preview is active.
#### Type
"prologue" | "blocks" | "epilogue" | undefined

---
### 🏷️ `prologue` {#prologue}
Retrieves the optional prologue for the form. This prologue can be used by runners (that support it) to show a welcome message before starting the actual form.
#### Type
[`IPrologue`](../../library/interfaces/IPrologue.mdx)

---
### 🏷️ `status` {#status}
Retrieves the runner status. It can be one of the following values:
- `empty`: The form is empty (has no blocks);
- `idle`: The runner is idle (ready to start);
- `running`: The runner is running;
- `preview`: The runner is in preview mode;
- `evaluating`: The runner is evaluating a block or condition;
- `stopped`: The runner is stopped;
- `finishing`: The runner is finishing (response data is being transmitted);
- `finished`: The runner has finished;
- `pausing`: The running is pausing (pause data is being transmitted);
- `paused`: The runner has paused;
- `error`: An unknown error occurred when finishing the form;
- `error-outdated`: The form definition is outdated;
- `error-rejected`: The server rejected the response data;
- `error-paused`: An error occurred when pausing the form.
#### Type
"empty" | "idle" | "running" | "preview" | "evaluating" | "stopped" | "finishing" | "finished" | "pausing" | "paused" | "error" | "error-outdated" | "error-rejected" | "error-paused"

---
### 🏷️ `stencil` {#stencil}
Retrieves the form stencil hash for the exportable data.
#### Type
string

:::tip
More information about the stencil hashes in the [Form data stencil guide](../../../../builder/integrate/guides/stencil.mdx). More information about exportable vs. actionable data can be found [here](../../../stock/guides/collecting.mdx#exportable-vs-actionable).
:::

---
### 🏷️ `storyline` {#storyline}
Retrieves the [`Storyline`](../../library/classes/Storyline.mdx) instance when the runner is not in `sequence` mode. Otherwise, it returns an array of [sequence items](IRunnerSequenceItem.mdx).
:::caution
This property may be `undefined` if there is no form running.
:::
#### Type
[`storyline`](../../library/classes/Storyline.mdx) | [`IRunnerSequenceItem[]`](IRunnerSequenceItem.mdx) | undefined

---
### 🏷️ `view` {#view}
Sets or retrieves the current view mode of the runner. It can be one of the following values:
- `live`: The form is running in normal (production) mode;
- `test`: The form is running in test mode (no response data is submitted);
- `preview`: The form is running in preview mode, which shows all blocks in a single view.
#### Type
"live" | "test" | "preview"

## ▶️ Functions {#functions}

---
### 🔧 `discard` {#discard}
Discards the error state of a runner when the runner [`status`](#status) is either `error`, `error-outdated`, `error-rejected`, or `error-rejected`. It sets the runner status back to `running`.
:::info
The [`status`](#status) property can be used to generate an error message in the UI when an error occurs. The `discard` function can then be used to remove/close this error message.
:::
#### Signature
```ts
() => void
```

---
### 🔧 `doAction` {#doAction}
Emits an action to the runner.
#### Signature
```ts
doAction: (
  type: "stage" | "unstage" | "focus" | "blur",
  node?: IObservableNode
) => void;
```
#### Parameters
| Name   | Type                                                                 | Optional | Description                   |
|:-------|:---------------------------------------------------------------------|:---------|:------------------------------|
| `type` | "stage" \| "unstage" \| "focus" \| "blur"                            | No       | Specifies the type of action. |
| `node` | [`IObservableNode`](../../library/interfaces/IObservableNode.mdx) | Yes      | Reference to the node.        |

---
### 🔧 `doPreview` {#doPreview}
Request a preview of the specified element of the form. This function is used in a live preview setup to bring a certain element in the form into view.
#### Signature
```ts
(data: TRunnerPreviewData) => void
```
#### Parameters
| Name   | Type                                                                                                       | Optional | Description                 |
|:-------|:-----------------------------------------------------------------------------------------------------------|:---------|:----------------------------|
| `data` | [`TRunnerPreviewData`](https://gitlab.com/tripetto/runner-react-hook/-/blob/master/src/lib/preview.ts#L23) | No       | Specifies the preview data. |

---
### 🔧 `kickOff` {#kickOff}
Starts a runner in `sequence` mode.
#### Signature
```ts
() => void
```

---
### 🔧 `pause` {#pause}
Pauses a form.
#### Signature
```ts
(
  data?: {},
  pausing?: (
    snapshot: ISnapshot,
    done: (result: "succeeded" | "failed" | "canceled") => void,
    item: IRunnerSequenceItem | undefined
  ) => void
) => Promise<ISnapshot> | ISnapshot | undefined;
```
#### Parameters
| Name      | Type                                                                                                                                                                                                             | Optional | Description                                                                                                                                                                                                                         |
|:----------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:---------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `data`    | `{}`                                                                                                                                                                                                             | No       | Optional data to store in the snapshot.                                                                                                                                                                                             |
| `pausing` | (snapshot: [`ISnapshot`](../../library/interfaces/ISnapshot.mdx), done: (result: "succeeded" \| "failed" \| "canceled") => void, item: [`IRunnerSequenceItem`](IRunnerSequenceItem.mdx) \| undefined) => void | Yes      | Optional function that can be used to process the pause data (for example, send it to an endpoint). When this function is supplied, the runner waits until the `done` function (with the result of the pause operation) is invoked. |
#### Return value
Returns the [`ISnapshot`](../../library/interfaces/ISnapshot.mdx) object, a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) for that object or `undefined` if the pause operation failed.

---
### 🔧 `reload` {#reload}
Reloads a [form definition](../../../../builder/api/interfaces/IDefinition.mdx) while trying to keep the data already collected.
#### Signature
```ts
(definition: IDefinition) => void
```
#### Parameters
| Name         | Type                                                                | Optional | Description                            |
|:-------------|:--------------------------------------------------------------------|:---------|:---------------------------------------|
| `definition` | [`IDefinition`](../../../../builder/api/interfaces/IDefinition.mdx) | No       | Specifies the form definition to load. |

---
### 🔧 `resetPreview` {#resetPreview}
Resets the preview element.
#### Signature
```ts
() => void
```

---
### 🔧 `restart` {#restart}
Restarts a runner.
#### Signature
```ts
() => void
```

---
### 🔧 `snapshot` {#snapshot}
Creates a snapshot of the form (without pausing it).
#### Signature
```ts
(data?: {}) => ISnapshot | undefined
```
#### Parameters
| Name      | Type | Optional | Description                             |
|:----------|:-----|:---------|:----------------------------------------|
| `data`    | `{}` | Yes      | Optional data to store in the snapshot. |
#### Return value
Returns the [`ISnapshot`](../../library/interfaces/ISnapshot.mdx) object.

---
### 🔧 `start` {#start}
Starts a runner.
#### Signature
```ts
() => Instance | undefined
```
#### Return value
Returns the [`Instance`](../../library/classes/Instance.mdx) of the form or `undefined` if the form cannot start.

---
### 🔧 `stop` {#stop}
Stops a runner.
#### Signature
```ts
() => void
```

---
### 🔧 `update` {#update}
Invokes a runner update.
#### Signature
```ts
() => void
```
