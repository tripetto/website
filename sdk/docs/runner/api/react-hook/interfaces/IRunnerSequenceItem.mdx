---
title: IRunnerSequenceItem interface - Runner React Hook
sidebar_label: IRunnerSequenceItem
description: Interface that describes a sequence item for useRunner hooks that run in sequence mode.
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# IRunnerSequenceItem interface

## 📖 Description {#description}
Interface that describes a sequence item for [`useRunner`](../hooks/useRunner.mdx) hooks that run in `sequence` mode. The items are returned by the [`storyline`](IRunner.mdx#storyline) property of the [`IRunner`](IRunner.mdx) object.

## 📃 Type declaration {#type-declaration}
<InterfaceDeclaration prefix="#" src={`interface IRunnerSequenceItem {
  readonly type: "node" | "prologue" | "epilogue" | "finishing" | "pausing" | "paused" | "error";
  readonly id: string;
  readonly key: string;
  readonly index: number;
  readonly timeStamp?: number;
  readonly node?: IObservableNode;
  readonly prologue?: IPrologue;
  readonly epilogue?: IEpilogue;
  readonly error?: "unknown" | "outdated" | "rejected" | "paused";
  readonly state: "history" | "past" | "pre-active" | "before-active" | "active" | "after-active" | "post-active" | "upcoming";
  readonly isHealthy: boolean;
  readonly isHistory: boolean;
  readonly isPast: boolean;
  readonly isPreActive: boolean;
  readonly isBeforeActive: boolean;
  readonly isActive: boolean;
  readonly isAfterActive: boolean;
  readonly isPostActive: boolean;
  readonly isUpcoming: boolean;
  readonly isFirst: boolean;
  readonly isLast: boolean;
  readonly isPaused: boolean;
  readonly allowActivate: boolean;
  readonly allowNext: boolean;
  readonly allowUndo: boolean;
  readonly allowSkip: boolean;
  readonly activate: (useTransition?: boolean) => boolean;
  readonly deactivate: () => boolean;
  readonly next: () => boolean;
  readonly undo: () => boolean;
  readonly skip: () => boolean;
  readonly wait: () => void;
  readonly continue: () => void;
  readonly repeat?: () => void;
  readonly kickOff?: () => void;
  readonly retry?: () => void;
  readonly cancel?: () => void;
}`} symbols={{
  "IObservableNode": "/runner/api/library/interfaces/IObservableNode/",
  "IPrologue": "/runner/api/library/interfaces/IPrologue/",
  "IEpilogue": "/runner/api/library/interfaces/IEpilogue/"
}} />

## 🗃️ Properties {#properties}

---
### 🏷️ `allowActivate` {#allowActivate}
Specifies if the runner is allowed to activate the item.
#### Type {#signature}
boolean

---
### 🏷️ `allowNext` {#allowNext}
Specifies if the runner is allowed to continue to the next item.
#### Type {#signature}
boolean

---
### 🏷️ `allowSkip` {#allowSkip}
Specifies if the runner is allowed to skip the item.
#### Type {#signature}
boolean

---
### 🏷️ `allowUndo` {#allowUndo}
Specifies if the runner is allowed to undo (go to the previous item).
#### Type {#signature}
boolean

---
### 🏷️ `epilogue` {#epilogue}
Retrieves the epilogue when the item [`type`](#type) is `epilogue`. This epilogue can be used by runners (that support it) to show a closing message when the form is completed.
#### Type {#signature}
[`IEpilogue`](../../library/interfaces/IEpilogue.mdx)

---
### 🏷️ `error` {#error}
Retrieves the error code when the item [`type`](#type) is `error`. It can be one of the following values:
- `unknown`: An unknown error occurred when finishing the form;
- `outdated`: The form definition is outdated;
- `rejected`: The server rejected the response data;
- `paused`: An error occurred when pausing the form.
#### Type {#signature}
"unknown" | "outdated" | "rejected" | "paused"

---
### 🏷️ `id` {#id}
Specifies the unique identifier of the item.
#### Type {#signature}
string

---
### 🏷️ `index` {#index}
Retrieves the zero-based index of the item in the [`storyline`](IRunner.mdx#storyline) sequence array.
#### Type {#signature}
string

---
### 🏷️ `isActive` {#isActive}
Specifies if the item is active.
#### Type {#signature}
boolean

---
### 🏷️ `isAfterActive` {#isAfterActive}
Specifies if the item is becoming inactive.
#### Type {#signature}
boolean

---
### 🏷️ `isBeforeActive` {#isBeforeActive}
Specifies if the item is becoming active.
#### Type {#signature}
boolean

---
### 🏷️ `isFirst` {#isFirst}
Specifies if the item is the first item in the [`storyline`](IRunner.mdx#storyline) sequence array.
#### Type {#signature}
boolean

---
### 🏷️ `isHealthy` {#isHealthy}
Retrieves if the item is still in the [`storyline`](IRunner.mdx#storyline) sequence array.
#### Type {#signature}
boolean

---
### 🏷️ `isHistory` {#isHistory}
Specifies if the item is history and cannot be changed anymore (write-protected).
#### Type {#signature}
boolean

---
### 🏷️ `isLast` {#isLast}
Specifies if the item is the last item in the [`storyline`](IRunner.mdx#storyline) sequence array.
#### Type {#signature}
boolean

---
### 🏷️ `isPast` {#isPast}
Specifies if the item is in the past.
#### Type {#signature}
boolean

---
### 🏷️ `isPaused` {#isPaused}
Specifies if the runner is paused.
#### Type {#signature}
boolean

---
### 🏷️ `isPostActive` {#isPostActive}
Specifies if the item is about to become inactive.
#### Type {#signature}
boolean

---
### 🏷️ `isPreActive` {#isPreActive}
Specifies if the item is about to become active.
#### Type {#signature}
boolean

---
### 🏷️ `isUpcoming` {#isUpcoming}
Specifies if the item is upcoming (is in the future).
#### Type {#signature}
boolean

---
### 🏷️ `key` {#key}
Retrieves a unique key for the item within its context (the key is prefixed with an underscore).
#### Type {#signature}
string

---
### 🏷️ `node` {#node}
Retrieves the node when the item [`type`](#type) is `node`.
#### Type {#signature}
[`IObservableNode`](../../library/interfaces/IObservableNode.mdx)

---
### 🏷️ `prologue` {#prologue}
Retrieves the prologue when the item [`type`](#type) is `prologue`. This prologue can be used by runners (that support it) to show a welcome message before starting the actual form.
#### Type {#signature}
[`IPrologue`](../../library/interfaces/IPrologue.mdx)

---
### 🏷️ `state` {#erstateror}
Retrieves the item state. It can be one of the following values:
- `history`: Specifies the item is history and cannot be changed anymore (write-protected);
- `past`: Specifies the item is in the past;
- `pre-active`: Specifies the item is about to become active (stage 1 out of 5);
- `before-active`: Specifies the item is becoming active (stage 2 out of 5);
- `active`: Specifies the item is active (stage 3 out of 5);
- `after-active`: Specifies the item is becoming inactive (stage 4 out of 5);
- `post-active`: Specifies the item is about to become inactive (stage 5 out of 5);
- `upcoming`: Specifies the item is upcoming (is in the future).
#### Type {#signature}
"history" | "past" | "pre-active" | "before-active" | "active" | "after-active" | "post-active" | "upcoming"

---
### 🏷️ `timeStamp` {#timeStamp}
Retrieves a last update timestamp for the item.
#### Type {#signature}
string

---
### 🏷️ `type` {#type}
Specifies the item type. It can be one of the following values:
- `node`: The item is a [`node`](#node);
- `prologue`: The item is a [`prologue`](#prologue);
- `epilogue`: The item is a [`epilogue`](#epilogue);
- `finishing`: The item is a finishing message that indicates the runner is submitting a form;
- `pausing`: The item is a pausing message that indicates the runner is pausing a form;
- `paused`: The item is a paused message that indicates the runner has paused;
- `error`: The item is an error message ([`error`](#error) indicates the error type).
#### Type {#signature}
"node" | "prologue" | "epilogue" | "finishing" | "pausing" | "paused" | "error"

## ▶️ Functions {#functions}

---
### 🔧 `activate` {#activate}
Activates the item.
#### Signature
```ts
(useTransition?: boolean) => boolean
```
#### Parameters
| Name            | Type    | Optional | Description                                  |
|:----------------|:--------|:---------|:---------------------------------------------|
| `useTransition` | boolean | Yes      | Specifies if a transition should be applied. |
#### Return value
Returns `true` if the item is activated.

---
### 🔧 `cancel` {#cancel}
Retries an operation after it failed.
#### Signature
```ts
() => void
```

---
### 🔧 `continue` {#continue}
Continues an item that was in a waiting state.
#### Signature
```ts
() => void
```

---
### 🔧 `deactivate` {#deactivate}
Deactivates the item.
#### Signature
```ts
() => boolean
```
#### Return value
Returns `true` if the item is deactivated.

---
### 🔧 `kickOff` {#kickOff}
Kick off a form.
#### Signature
```ts
() => void
```

---
### 🔧 `next` {#next}
Activates to the next item.
#### Signature
```ts
() => boolean
```
#### Return value
Returns `true` if the next item is activated.

---
### 🔧 `repeat` {#repeat}
Repeats the form after it is completed.
#### Signature
```ts
() => void
```

---
### 🔧 `retry` {#retry}
Retries an operation after it failed.
#### Signature
```ts
() => void
```

---
### 🔧 `skip` {#skip}
Skips the item.
#### Signature
```ts
() => boolean
```
#### Return value
Returns `true` if the item is skipped.

---
### 🔧 `undo` {#undo}
Undos the item (and activates the previous item).
#### Signature
```ts
() => boolean
```
#### Return value
Returns `true` if the undo operation was successful.

---
### 🔧 `wait` {#wait}
Puts the item in a waiting state that ends when [`continue`](#continue) is invoked.
#### Signature
```ts
() => void
```
