---
title: Runner
sidebar_label: Introduction
sidebar_position: 1
description: To run a Tripetto form (and collect responses) a runner is used.
---

# Running forms

🎓 To run a Tripetto form (and collect responses) a **runner** is used.

✨ Runners turn the output of the [form builder](../builder/introduction.md) (the JSON form definition) into a **visible and usable form**.

👩‍💻 Implementing a runner is **easy** and can be done in minutes!

## 👩‍🏫 Introduction
So you have created this beautiful, fancy, and smart form with the [form builder](../builder/introduction.md). Now you want to run it and collect responses. Well, you've come to the right place!

To run a Tripetto form, you need a so-called **runner**. A runner turns a [form definition](../builder/api/interfaces/IDefinition.mdx) (the output of the [builder](../builder/introduction.md)) into a fully working form (more technical details [here](custom/concepts.md)). It performs the actual rendering of the form.

## 📺 Preview {#preview}
![Runner preview](/img/runner-autoscroll.jpg)

## 🆎 Choosing a runner
For the runner there are two directions to choose from:

---
### 🅰️ Use a stock runner
Stock runners are the easiest and most convenient way to run a Tripetto form. Stock runners are full-featured runners built and maintained by the Tripetto team. They provide a seamless experience out-of-the-box and are very easy to use and implement. There are currently three different form UIs (we call them [form faces](stock/faces/index.md)) to choose from.

##### Pros and cons
- ✅ Very easy to implement
- ✅ Full-featured
- ✅ Maintained by the Tripetto team
- ❌ Limited to fixed form UIs ([autoscroll](stock/faces/autoscroll.mdx), [chat](stock/faces/chat.mdx), or [classic](stock/faces/classic.mdx) UI)
- ❌ Requires a [license](pricing.md) for commercial use/branding removal

##### Hands-on instructions available for:
[![Implement stock runner using plain JS](/img/logo-js.svg)](../stock/quickstart/plain-js/)
[![Implement stock runner using React](/img/logo-react.svg)](../stock/quickstart/react/)
[![Implement stock runner using Angular](/img/logo-angular.svg)](../stock/quickstart/angular/)
[![Implement stock runner using HTML](/img/logo-html.svg)](../stock/quickstart/html/)

##### Documentation
▶️ [Start with a stock runner](stock/introduction.md)

---
### 🅱️ Build a custom runner
Go commando and build your own custom runner on top of our [Runner library](api/library/index.md). You get full control over the form UI, but it involves coding and is more suitable for experienced developers.

##### Pros and cons
- ✅ Complete UI freedom
- ✅ Does not require a license
- ❌ Requires significant coding

##### Instructions available for:
[![Implement custom runner using plain JS](/img/logo-js.svg)](../custom/implement/plain-js/)
[![Implement custom runner using React](/img/logo-react.svg)](../custom/implement/react/)
[![Implement custom runner using Angular](/img/logo-angular.svg)](../custom/implement/angular/)

##### Documentation
▶️ [Start building a custom runner](custom/introduction.md)

---
## 🖥️ Try it​ {#try-it}
See the runners in action. This demo implements the [builder](../builder/integrate/introduction.md) and the [stock runners](stock/introduction.md) in a side-by-side view. Play around and have fun!

[![Play](/img/button-play.svg)](https://jdmt3.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-plain-js-live-preview-with-multiple-runners-jdmt3?file=/src/index.ts)

---
## 🎞️ Video {#video}
<div className="youtube-embed"><iframe width="560" height="315" src="https://www.youtube.com/embed/fixp89Yievg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
