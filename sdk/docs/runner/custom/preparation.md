---
title: Preparation - Custom runner
sidebar_label: Preparation
sidebar_position: 4
description: To use the Runner library in your project you should install the Runner library as a dependency.
---

# Preparation
To use the Runner library in your project you should install it as a dependency using the following command:

```bash npm2yarn
npm install @tripetto/runner
```

:::tip Typescript support
This package contains type declarations and supports [TypeScript](https://www.typescriptlang.org/) out-of-the-box.
:::
