---
title: Prerequisites - Custom runner
sidebar_label: Prerequisites
sidebar_position: 3
description: Overview of prerequisites to develop a custom runner.
---

# Prerequisites
To develop a custom runner for Tripetto you need programming skills. [TypeScript](https://www.typescriptlang.org/) is required when building runners, because we rely on the use of [decorators](https://www.typescriptlang.org/docs/handbook/decorators.html). Also, you need a bundler like [webpack](https://webpack.js.org/) to bundle all the assets.

:::info
Tripetto uses [decorators](https://www.typescriptlang.org/docs/handbook/decorators.html) for building runners. Therefore, you should always use [TypeScript](https://www.typescriptlang.org/) and enable the [`experimentalDecorators`](https://www.typescriptlang.org/tsconfig#experimentalDecorators) feature of it.
:::
