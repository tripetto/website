---
title: Plain JS - Custom runner
sidebar_label: Plain JS
sidebar_position: 3
description: This guide shows how to build a custom runner using plain JS.
---

# ![](/img/logo-js.svg) Custom runner using plain JS
The [Runner library](../../api/library/index.md) is the workhorse of each Tripetto runner. It turns a [form definition](../../../builder/api/interfaces/IDefinition.mdx) into a state machine (more specifically a [virtual finite state machine](https://en.wikipedia.org/wiki/Virtual_finite-state_machine)) that holds the state of the form. It handles all the complex logic and the response collection during the execution of the form. It is a headless library, so it has no UI (it can even run at server-side). The idea is to separate concerns, in this case, the actual state and logic of the form, and the visual representation of the form (the UI). You could even attach other means of interaction to the runner, like speech recognition for the input and a speech synthesizer for the output.

:::tip
If you want to render a visual form, we advise you to use a library like [React](https://react.dev) or a framework like [Angular](https://angular.io/) (see the implementation guide for [React](./react.mdx) or [Angular](./angular.mdx) to learn how). Rendering and managing a UI using plain JS can be quite challenging (though it is possible if you want).
:::

## Basic implementation
The following code shows the basic implementation of a runner. It constructs a new [`Runner`](../../api/library/classes/Runner.mdx) instance. A function handler is specified for the [`onChange`](../../api/library/classes/Runner.mdx#onChange) event. Whenever there is a change in the state of the form, this event is invoked. When the form is finished, the [`onFinish`](../../api/library/classes/Runner.mdx#onFinish) event is invoked. One of the [export functions](../../api/library/modules/Export.mdx) can be used to extract the collected data from the instance.

```ts showLineNumbers
import { Runner, IRunnerChangeEvent, Instance, Export } from "@tripetto/runner";

const runner = new Runner({
  definition: /** Supply your form definition here */,
  mode: "paginated"
});

// React to changes
runner.onChange = (event: IRunnerChangeEvent) => {
  // Do stuff here
};

// Do something with the output when the runner is finished
runner.onFinish = (instance: Instance) => {
  // We're done, export the collected data as CSV
  const csv = Export.CSV(instance);

  console.dir(csv);
};
```

### Storyline
The [`onChange`](../../api/library/classes/Runner.mdx#onChange) event contains the [storyline](../../api/library/classes/Storyline.mdx) of the runner. This storyline contains all the sections, nodes, and blocks that should render. The [mode of operation](../../api/library/classes/Runner.mdx#mode) of the runner affect how the storyline is generated:
- `paginated`: Blocks are presented page for page and the user navigates through the pages using the next and back buttons;
- `continuous`: This will keep all past blocks in view as the user navigates using the next and back buttons;
- `progressive`: In this mode, all possible blocks are presented to the user (till the point where one of the blocks fails validation). The user does not need to navigate using the next and back buttons (so we can hide those buttons);
- `ahead`: Present all blocks to the user, regardless of the section's validation result.

The following example shows how to retrieve all the (active) nodes that should render.

```ts showLineNumbers
import { Runner, IRunnerChangeEvent } from "@tripetto/runner";

const runner = new Runner({
  definition: /** Supply your form definition here */,
  mode: "paginated"
});

runner.onChange = (event: IRunnerChangeEvent) => {
  //highlight-start
  // Render all the nodes
  event.storyline.nodes.each((node) => {
    // Render each node
  });
  //highlight-end
};
```

## Implementation using overrides
If you want a more [OOP](https://en.wikipedia.org/wiki/Object-oriented_programming) approach, you can also derive a class from the [`Runner`](../../api/library/classes/Runner.mdx) class and extend it with custom overrides to tap in the functionality of the runner. Let's start with an example.


```ts showLineNumbers
import { Runner } from "@tripetto/runner";

class CustomRunner extends Runner {
  protected onInstanceStart(instance: Instance): void {
    // Form is started
  }

  protected onInstanceFinish(instance: Instance): void {
    // Form is finished
  }
}
```

Have a look at the [API documentation](../../api/library/classes/Runner.mdx#methods) to see all the methods that you can override.

## Creating blocks
Tripetto uses [dependency injection](https://en.wikipedia.org/wiki/Dependency_injection) to make blocks available to the runner. Blocks are registered using a special [class decorator](https://www.typescriptlang.org/docs/handbook/decorators.html#class-decorators). The following code shows the basic structure of a block:

```ts showLineNumbers
import { tripetto, NodeBlock } from "@tripetto/runner";

@tripetto({
  type: "node",
  identifier: "hello-world",
})
export class HelloWorldBlock extends NodeBlock {}
```

Here we define a block named `HelloWorldBlock`. It is registered using the [`@tripetto`](../../api/library/decorators/tripetto.mdx) decorator using the identifier `hello-world`. This identifier is used in the [form definition](../../api/library/interfaces/IRunnerProperties.mdx#definition) to select the right block for a node.

:::tip
See the [block implementation guide](../../../blocks/custom/implement/index.md) to learn how to develop blocks for Tripetto.
:::

## Render blocks
The block example above does not render anything. We need to add functionality to do so. Let's add an interface we can use as a contract between the runner and the blocks. That allows us to call a render function in the runner for each block.

```ts showLineNumbers
import { tripetto, NodeBlock, Runner, IRunnerChangeEvent } from "@tripetto/runner";

//highlight-start
interface IBlockRenderer extends NodeBlock {
    blockToHTML: () => HTMLElement;
}
//highlight-end

@tripetto({
  type: "node",
  identifier: "hello-world",
})
//highlight-start
class HelloWorldBlock extends NodeBlock implements IBlockRenderer {
    blockToHTML(): HTMLElement {
        const element = document.createElement("div");

        element.textContent = "Hello world!";

        return element;
    }
//highlight-end
}

//highlight-start
const runner = new Runner<IBlockRenderer>({
  definition: {
    name: "Example form",
    sections: [
      {
        "id": "0abbc4d4aacecbb8f1aa13e77c6ab7a58c416aefcc42f4fd77d6a1a46a4e3afa",
        "nodes": [
        {
            "id": "362221f5ed388ec503a186f66be7829b542cb307b4f43e91473838a5f8c5ac75",
            "block": { "type": "hello-world", "version": "1.0.0" }
        }
        ]
      }
    ]
  },
//highlight-end
  mode: "paginated"
});

runner.onChange = (event: IRunnerChangeEvent) => {
  // Render all the nodes
  event.storyline.nodes.each((node) => {
  //highlight-start
    if (node.block) {
        document.body.appendChild(node.block.blockToHTML());
    }
  //highlight-end
  });
};
```

In this example, each block should have a `blockToHTML` method that generates an HTML element for the block. The runner can then call this method for each block that should render.
