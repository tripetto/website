---
title: Implementation - Custom runner
sidebar_label: Implementation
description: This chapter shows you different approaches to implement a custom runner.
---

# Implement a custom runner

The Tripetto Runner library does not depend on a specific library or framework. So it should be able to implement it in any environment that runs [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript/A_re-introduction_to_JavaScript). This chapter shows you different approaches to implement a custom runner. Please select what you want to use for your runner:

[![Implement custom runner using React](/img/button-react.svg)](react/)
[![Implement custom runner using Angular](/img/button-angular.svg)](angular/)
[![Implement custom runner using plain JS](/img/button-js.svg)](plain-js/)

▶️ [Implement custom runner using **React**](react.mdx)

▶️ [Implement custom runner using **Angular**](angular.mdx)

▶️ [Implement custom runner using **plain JS**](plain-js.mdx)
