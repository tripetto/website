---
title: Custom runner
sidebar_label: Introduction
sidebar_position: 1
description: If you don't want to use one of the stock runners, you can also build your own runner using the Tripetto Runner library.
---

# Custom runner
If you don't want to use one of the [stock runners](../stock/introduction.md), you can also build your own runner using the [Tripetto Runner library](https://www.npmjs.com/package/@tripetto/runner). This package is what drives the stock runners and does all the heavy weight lifting of parsing the form definition into a [virtual finite state machine](https://en.wikipedia.org/wiki/Virtual_finite-state_machine) that handles all the complex logic and response collection during the execution of the form. Basically you can apply any UI framework you like on top of it and build a runner with a custom UI.

🎓 This chapter contains all the information you need to **start developing a custom runner**.

✍️ Building a custom runner involves coding and **JavaScript programming skills**.

📦 You will use the Runner library published on **[npm](https://www.npmjs.com/package/@tripetto/runner)**.

:::tip
By building a custom runner, you get full control over the form UI. The downside is that it involves more coding, and therefore it's more suitable for experienced developers. Use one of Tripetto's [stock runners](../stock/introduction.md) to run forms without the need of building a custom runner. The stock runners are built and maintained by the Tripetto team and provide a seamless experience out-of-the-box.
:::
