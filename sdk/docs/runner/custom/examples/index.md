---
title: Examples - Custom runner
sidebar_label: Examples
description: Various custom runner examples with React and Angular.
---

# Custom runner examples

---
## React + Bootstrap
Custom runner example that uses [React](https://react.dev) and [Bootstrap](https://getbootstrap.com/).

[![Demo](/img/button-demo.svg)](https://tripetto.gitlab.io/examples/react-bootstrap/)
[![Source code](/img/button-code.svg)](https://gitlab.com/tripetto/examples/react-bootstrap)

---
## React + Material UI
Custom runner example that uses [React](https://react.dev) and [Material UI](https://mui.com/).

[![Demo](/img/button-demo.svg)](https://tripetto.gitlab.io/examples/react-mui/)
[![Source code](/img/button-code.svg)](https://gitlab.com/tripetto/examples/react-mui)

---
## Angular + Bootstrap
Custom runner example that uses [Angular](https://angular.io/) and [Bootstrap](https://getbootstrap.com/).

[![Demo](/img/button-demo.svg)](https://tripetto.gitlab.io/examples/angular-bootstrap/)
[![Source code](/img/button-code.svg)](https://gitlab.com/tripetto/examples/angular-bootstrap)

---
## Angular + Angular Material
Custom runner example that uses [Angular](https://angular.io/) and [Angular Material](https://material.angular.io/).

[![Demo](/img/button-demo.svg)](https://tripetto.gitlab.io/examples/angular-material/)
[![Source code](/img/button-code.svg)](https://gitlab.com/tripetto/examples/angular-material)

---
## Stock runners
All of Tripetto's [stock runners](../../stock/introduction.md) can also serve as good examples and inspiration for a custom runner. The stock runners are open-source. They all use [React](https://react.dev) and the [Runner React Hook](../../api/react-hook/index.md) for rendering the UI.

- **Autoscroll stock runner** [▶️ Demo](https://tripetto.gitlab.io/runners/autoscroll/) [📖 Info](../../stock/faces/autoscroll.mdx) [📁 Source code](https://gitlab.com/tripetto/runners/autoscroll)
- **Chat stock runner** [▶️ Demo](https://tripetto.gitlab.io/runners/chat/) [📖 Info](../../stock/faces/chat.mdx) [📁 Source code](https://gitlab.com/tripetto/runners/classic)
- **Classic stock runner** [▶️ Demo](https://tripetto.gitlab.io/runners/classic/) [📖 Info](../../stock/faces/classic.mdx) [📁 Source code](https://gitlab.com/tripetto/runners/chat)
- **Classic Fluent UI stock runner** [▶️ Demo](https://tripetto.gitlab.io/runners/classic-fluentui/) [📁 Source code](https://gitlab.com/tripetto/runners/classic-fluentui)
