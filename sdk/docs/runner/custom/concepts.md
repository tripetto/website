---
title: Concepts - Custom runner
sidebar_label: Concepts
sidebar_position: 2
description: The concepts of a typical Tripetto runner.
---

# Concepts
Let's talk about the concepts of a typical Tripetto runner. Each runner consists out of two layers:
- The runner core layer;
- The runner UI layer.

The runner core layer is a library published on [npm](https://www.npmjs.com/package/@tripetto/runner). It drives all the runners and does the heavy weight lifting of parsing the [form definition](../../builder/api/interfaces/IDefinition.mdx) into a [virtual finite state machine](https://en.wikipedia.org/wiki/Virtual_finite-state_machine) that handles all the complex logic and response collection during the execution of the form. You can apply any UI framework you like on top of it (which is the second layer). Combined they are a full-featured form runner capable of running complex forms.

The following diagram shows how the layers relate to each other:

![Runner overview](/diagrams/runner.svg)

The runner solves a couple of things at once:

- **Form parsing**: The runner takes on the task of parsing the [form definition](../../builder/api/interfaces/IDefinition.mdx) (the output of the [form builder](../../builder/introduction.md)) into an executable program. This altogether eliminates the typically complex programming of logic and flows within forms.

- **UI freedom**: For the UI layer you can implement anything you want. We don’t impose any particular UI framework or library. You decide how it looks and operates. Just wire it up to any UI you like by using the standard DOM-methods of the browser, or by using a library like [React](https://react.dev) or framework like [Angular](https://angular.io/).

## ♻️ Form lifecycle
Whenever a form session in a runner starts, a form instance is created. So, an instance represents a single input/user session. It holds the current state of the form and the collected data. A form instance always follows the following lifecycle:

:::tip
A description of the entities used in the diagram can be found [here](../../builder/api/interfaces/IDefinition.mdx#entities).
:::

![Form lifecycle](/diagrams/definition.svg)

When a form starts, the sections in the root of that form are shown. Sections can have branches with conditions. The branches that have matching conditions are taken. Branches, in turn, can have more sections, and those sections can have nested branches where this process repeats itself (recursive model).

:::tip
Form instances can also [pause](../api/library/classes/Runner.mdx#pause) and [resume](../api/library/classes/Runner.mdx#restore) later on. In a typical UI-oriented application only one instance at a time is active.
:::

:::info
We tend to call the forms you build with Tripetto smart because they can contain this advanced logic and conditional flows, allowing for jumps from one part of the form to another or the skipping of certain parts altogether; all depending on the respondent’s input.
:::
