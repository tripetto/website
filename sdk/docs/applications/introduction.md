---
title: Applications
sidebar_label: Introduction
sidebar_position: 1
description: The Tripetto Studio and Tripetto WordPress plugin are both applications built by the Tripetto team using the Tripetto SDK.
---

# Applications

🎓 Allows **anyone** to create Tripetto forms without coding.

💡 Applications built and maintained by the **Tripetto team**.

🆓 **Free** to use!

## 👩‍🏫 Introduction
The [Tripetto Studio](https://tripetto.app) and [Tripetto WordPress plugin](https://wordpress.org/plugins/tripetto/) are both applications built by the Tripetto team using the Tripetto SDK. The Tripetto Studio is an easy to use web app (SaaS) that allows anyone to create and publish forms and surveys (no-code). The Tripetto WordPress plugin can do the same but runs entirely inside a WordPress website (on-premise). Both applications also have an API, which is why they are included in this documentation.

▶️ [Tripetto Studio](studio/introduction.md)

▶️ [Tripetto WordPress plugin](wordpress/introduction.md)
