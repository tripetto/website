---
title: Studio
sidebar_label: Introduction
sidebar_position: 1
description: The Studio is a free online service (SaaS) to build and publish forms and surveys.
---

# ![](/img/logo-studio.svg) Tripetto Studio web app

:::info Not a developer!?
If you are not a developer and just want to build a form or survey, go to the [product page](https://tripetto.com/studio/) for end-users.
:::

## 🎓 What is it?
The Studio is a free online service (SaaS) to build and publish forms and surveys.

## 👤 For who is it?
Anyone who needs a form or survey. Whether you need a form for a website (embed) or want to create a survey that you want to share (using a direct link). It's all possible with the Studio!

## 🚀 Quickstart
You can start creating a form right away! Just go to [tripetto.app](https://tripetto.app) and start building your first form. You can try the application without creating an account. But if you want to save (and publish) your form and store form results, make sure to create an account by clicking on the user icon in the top right corner of the application.

▶️ [Start creating a form with the Studio](https://tripetto.app)

## 👩‍💻 For developers
The Studio is an online service that allows to store forms and results (responses) in the Tripetto Studio cloud. The API of the Studio is accessible through a dedicated [npm](https://www.npmjs.com/) package and can be used to interact with the Studio back-end. Read the Studio API documentation for more information.

▶️ [Studio API documentation](api/index.mdx)

📁 [Studio source code](https://gitlab.com/tripetto/studio/)
