---
title: Services class - Studio
sidebar_label: Services
description: The services class implements the connection between a client and the Studio back-end.
---

# Services class

## 📖 Description {#description}
The services class implements the connection between a client and the Studio back-end. To create a new instance, use the static [`form`](#static-form), [`get`](#static-get), or [`init`](#static-init) function.

---
## 🆕 `constructor` {#constructor}
Creates a new services instance with the specified properties.
:::info
The token of a form can be obtained from the share panel of the Studio application. Open your form in the Studio and then click the `Share` button.
:::
#### Signature
```ts
constructor(
  properties: {
    token: string;
    signature?: string;
    url?: string;
    filestoreUrl?: string;
  }
): Services
```
#### Properties
| Name           | Type   | Optional | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
|:---------------|:-------|:---------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `token`        | string | No       | Specifies the Studio API token for the form.                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| `signature`    | string | Yes      | Specifies a signature for the form. This signature is a SHA256 hash of the email address of the account that holds the form and the token of the form, separated by a colon: `SHA256("<email address>:<token>")`. The Studio server will verify the supplied signature and will only return the form if it is from the account specified. The account needs to be specified by the full email adress of the account or `*@<domain>` to allow all accounts from a certain domain. |
| `url`          | string | Yes      | Specifies the URL of the server. If omitted the default URL will be used.                                                                                                                                                                                                                                                                                                                                                                                                        |
| `filestoreUrl` | string | Yes      | Specifies the URL of the filestore service (used for file uploads). If omitted the default URL will be used.                                                                                                                                                                                                                                                                                                                                                                     |
#### Return value
Returns a reference to the new services instance.


## 📌 Statics {#statics}

---
### 🔧 `form` {#static-form}
Initializes a new [stock runner](../../../../runner/stock/introduction.md). This function retrieves a form from the Studio API and creates a new [stock runner](../../../../runner/stock/introduction.md) (of the supplied runner type). The goal of this function is to make it easy to fetch a form from the Studio, run it, and post the form data back to the Studio.
:::info
The token of a form can be obtained from the share panel of the Studio application. Open your form in the Studio and then click the `Share` button.
:::
:::tip
All properties available in the runner you are using can be provided to this function. Those properties are then passed on to the runner.
:::
:::caution
This function always sends the form data to the studio regardless of the use of the `onSubmit` event. Use the [`init`](#static-init) function if you do not want form data to be sent to the Studio.
:::
#### Signature
```ts
form(
  properties: {
    runner: {};
    token: string;
    signature?: string;
    element?: string | HTMLElement;
    persistent?: boolean;
    pausable?: boolean;
    trackers?: (
        type: "start" | "stage" | "unstage" | "focus" | "blur" | "pause" | "complete",
        definition: {
          fingerprint: string;
          name: string;
        },
        block?: {
          id: string;
          name: string;
        }
      ) => void;
    onSubmit?: (
        instance: Instance,
        id: string,
        language: string,
        locale: string,
        namespace: string
      ) => Promise<void> | void;
    url?: string;
    filestoreUrl?: string;
  }
): Services
```
#### Properties
| Name           | Type                             | Optional | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
|:---------------|:---------------------------------|:---------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `runner`       | `{...}`                          | No       | Specifies the runner to use.                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| `token`        | string                           | No       | Specifies the Studio API token for the form.                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| `signature`    | string                           | Yes      | Specifies a signature for the form. This signature is a SHA256 hash of the email address of the account that holds the form and the token of the form, separated by a colon: `SHA256("<email address>:<token>")`. The Studio server will verify the supplied signature and will only return the form if it is from the account specified. The account needs to be specified by the full email adress of the account or `*@<domain>` to allow all accounts from a certain domain. |
| `element`      | string \| HTMLElement            | Yes      | Specifies the element to use for the form.                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `persistent`   | boolean                          | Yes      | Specifies to store sessions in the local store to preserve persistency when navigating between multiple pages that host the runner.                                                                                                                                                                                                                                                                                                                                              |
| `pausable`     | boolean                          | Yes      | Specifies if the form can be [paused and resumed](../../../../runner/stock/guides/pause-resume.mdx).                                                                                                                                                                                                                                                                                                                                                                             |
| `trackers`     | `(...) => void`                  | Yes      | Specifies a [tracking function](../../../../runner/stock/guides/tracking.mdx) that is invoked when the user performs an action.                                                                                                                                                                                                                                                                                                                                                  |
| `onSubmit`     | `(...) => Promise<void>` \| void | Yes      | Specifies a function that is invoked when the form is [submitted](../../../../runner/stock/guides/collecting.mdx).<br/><br/>⚠️ **Please note that the form data is also sent to the Studio. Use the [`init`](#static-init) function if you want to avoid that.**                                                                                                                                                                                                                 |
| `url`          | string                           | Yes      | Specifies the URL of the server. If omitted the default URL will be used.                                                                                                                                                                                                                                                                                                                                                                                                        |
| `filestoreUrl` | string                           | Yes      | Specifies the URL of the filestore service (used for file uploads). If omitted the default URL will be used.                                                                                                                                                                                                                                                                                                                                                                     |
#### Return value
Returns an object that can be used to initiate a new runner.
#### Example
```js showLineNumbers
import * as Autoscroll from "@tripetto/runner-autoscroll";
import Services from "@tripetto/studio";

Services.form({
  runner: Autoscroll,

  // You can obtain the token of a form in the share panel of the Studio
  token: "a-valid-tripetto-token",
});
```

---
### 🔧 `get` {#static-get}
Initializes a new services instance with the specified properties and returns an object that can be supplied directly to one of the Tripetto [stock runners](../../../../runner/stock/introduction.md).
:::info
The token of a form can be obtained from the share panel of the Studio application. Open your form in the Studio and then click the `Share` button.
:::
:::tip
All properties available in the runner you are using can be provided to this function. Those properties will be included in the return object of this function.
:::
:::caution
This function always sends the form data to the studio regardless of the use of the `onSubmit` event. Use the [`init`](#static-init) function if you do not want form data to be sent to the Studio.
:::
#### Signature
```ts
get(
  properties: {
    token: string;
    signature?: string;
    element?: string | HTMLElement;
    persistent?: boolean;
    pausable?: boolean;
    trackers?: (
        type: "start" | "stage" | "unstage" | "focus" | "blur" | "pause" | "complete",
        definition: {
          fingerprint: string;
          name: string;
        },
        block?: {
          id: string;
          name: string;
        }
      ) => void;
    onSubmit?: (
        instance: Instance,
        id: string,
        language: string,
        locale: string,
        namespace: string
      ) => Promise<void> | void;
    url?: string;
    filestoreUrl?: string;
  }
): Services
```
#### Properties
| Name           | Type                             | Optional | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
|:---------------|:---------------------------------|:---------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `token`        | string                           | No       | Specifies the Studio API token for the form.                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| `signature`    | string                           | Yes      | Specifies a signature for the form. This signature is a SHA256 hash of the email address of the account that holds the form and the token of the form, separated by a colon: `SHA256("<email address>:<token>")`. The Studio server will verify the supplied signature and will only return the form if it is from the account specified. The account needs to be specified by the full email adress of the account or `*@<domain>` to allow all accounts from a certain domain. |
| `element`      | string \| HTMLElement            | Yes      | Specifies the element to use for the form.                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `persistent`   | boolean                          | Yes      | Specifies to store sessions in the local store to preserve persistency when navigating between multiple pages that host the runner.                                                                                                                                                                                                                                                                                                                                              |
| `pausable`     | boolean                          | Yes      | Specifies if the form can be [paused and resumed](../../../../runner/stock/guides/pause-resume.mdx).                                                                                                                                                                                                                                                                                                                                                                             |
| `trackers`     | `(...) => void`                  | Yes      | Specifies a [tracking function](../../../../runner/stock/guides/tracking.mdx) that is invoked when the user performs an action.                                                                                                                                                                                                                                                                                                                                                  |
| `onSubmit`     | `(...) => Promise<void>` \| void | Yes      | Specifies a function that is invoked when the form is [submitted](../../../../runner/stock/guides/collecting.mdx).<br/><br/>⚠️ **Please note that the form data is also sent to the Studio. Use the [`init`](#static-init) function if you want to avoid that.**                                                                                                                                                                                                                 |
| `url`          | string                           | Yes      | Specifies the URL of the server. If omitted the default URL will be used.                                                                                                                                                                                                                                                                                                                                                                                                        |
| `filestoreUrl` | string                           | Yes      | Specifies the URL of the filestore service (used for file uploads). If omitted the default URL will be used.                                                                                                                                                                                                                                                                                                                                                                     |
#### Return value
Returns an object that can be used to initiate a new runner.
#### Example
```ts showLineNumbers
import { run } from "@tripetto/runner-autoscroll";
import { Export } from "@tripetto/runner";
import Services from "@tripetto/studio";

run(Services.get({
    // You can obtain the token of a form in the share panel of the Studio
    token: "a-valid-tripetto-token",

    // If you want you can use the submit event to also send the response data
    // to a custom endpoint. Please note that when using this function, the
    // response data is always first stored in the Studio.
    onSubmit: (instance, id) => {
      const data = Export.dataset(instance);

      // Do something with the data here
    }
}));
```

---
### 🔧 `init` {#static-init}
Initializes a new services instance with the specified properties.
:::info
The token of a form can be obtained from the share panel of the Studio application. Open your form in the Studio and then click the `Share` button.
:::
#### Signature
```ts
init(
  properties: {
    token: string;
    signature?: string;
    url?: string;
    filestoreUrl?: string;
  }
): Services
```
#### Properties
| Name           | Type   | Optional | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
|:---------------|:-------|:---------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `token`        | string | No       | Specifies the Studio API token for the form.                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| `signature`    | string | Yes      | Specifies a signature for the form. This signature is a SHA256 hash of the email address of the account that holds the form and the token of the form, separated by a colon: `SHA256("<email address>:<token>")`. The Studio server will verify the supplied signature and will only return the form if it is from the account specified. The account needs to be specified by the full email adress of the account or `*@<domain>` to allow all accounts from a certain domain. |
| `url`          | string | Yes      | Specifies the URL of the server. If omitted the default URL will be used.                                                                                                                                                                                                                                                                                                                                                                                                        |
| `filestoreUrl` | string | Yes      | Specifies the URL of the filestore service (used for file uploads). If omitted the default URL will be used.                                                                                                                                                                                                                                                                                                                                                                     |
#### Return value
Returns a reference to the new services instance.
#### Example
```ts showLineNumbers
import { run } from "@tripetto/runner-autoscroll";
import Services from "@tripetto/studio";

const { definition, onSubmit } = Services.init({
    // You can obtain the token of a form in the share panel of the Studio
    token: "a-valid-tripetto-token"
});

run({
    definition,
    onSubmit
});
```

## 🗃️ Fields {#fields}

---
### 🏷️ `attachments` {#attachments}
Retrieves the attachments object that holds the endpoints for file upload store.
:::tip
For more information how this field is used in a runner, take a look at the [`attachments`](../../../../runner/api/stock/autoscroll/interfaces/IAutoscroll.mdx#attachments) property of the [Autoscroll stock runner](../../../../runner/stock/faces/autoscroll.mdx).
:::
#### Type
[`IRunnerAttachments`](../../../../runner/api/react-hook/interfaces/IRunnerAttachments.mdx)

---
### 🏷️ `definition` {#definition}
Fetches the [form definition](../../../../builder/api/interfaces/IDefinition.mdx) from the server. Returns a promise that resolves with the [form definition](../../../../builder/api/interfaces/IDefinition.mdx) (or `undefined` if the definition was not found).
#### Type
[`Promise<IDefinition | undefined>`](../../../../builder/api/interfaces/IDefinition.mdx)

---
### 🏷️ `l10n` {#l10n}
Fetches the form localization and translation data from the server. Returns a promise that resolves with the data (or `undefined` if no data was found).
:::tip
For more information how this field is used in a runner, take a look at the [`l10n`](../../../../runner/api/stock/autoscroll/interfaces/IAutoscroll.mdx#l10n) property of the [Autoscroll stock runner](../../../../runner/stock/faces/autoscroll.mdx).
:::
#### Type
[`Promise<IL10n | undefined>`](../../../../runner/api/library/interfaces/IL10n.mdx)

---
### 🏷️ `license` {#license}
Retrieves the license code for the form.
:::tip
For more information how this field is used in a runner, take a look at the [`license`](../../../../runner/api/stock/autoscroll/interfaces/IAutoscroll.mdx#license) property of the [Autoscroll stock runner](../../../../runner/stock/faces/autoscroll.mdx).
:::
#### Type
`() => Promise<string | undefined>`

---
### 🏷️ `locale` {#locale}
Retrieves the locale function used to retrieve locale data.
:::tip
For more information how this field is used in a runner, take a look at the [`locale`](../../../../runner/api/stock/autoscroll/interfaces/IAutoscroll.mdx#locale) property of the [Autoscroll stock runner](../../../../runner/stock/faces/autoscroll.mdx).
:::
#### Type
`(locale: "auto" | string) => Promise<L10n.ILocale | undefined>`

---
### 🏷️ `snapshot` {#snapshot}
Fetches the [form snapshot](../../../../runner/api/library/interfaces/ISnapshot.mdx) from the server. Returns a promise that resolves with the [form snapshot](../../../../runner/api/library/interfaces/ISnapshot.mdx) (or `undefined` if no snapshot was found).
#### Type
[`Promise<ISnapshot | undefined>`](../../../../runner/api/library/interfaces/ISnapshot.mdx)

---
### 🏷️ `styles` {#styles}
Fetches the form styles from the server. Returns a promise that resolves with the form styles (or `undefined` if no styles were found).
:::info
The form styles object differs per runner. For example, when the Autoscroll stock runner is used, this property returns an [`IAutoscrollStyles`](../../../../runner/api/stock/autoscroll/interfaces/IAutoscrollStyles.mdx) object.
:::
#### Type
`Promise<TStyles | undefined>`

---
### 🏷️ `translations` {#translations}
Retrieves the translation function used to retrieve translation data.
:::tip
For more information how this field is used in a runner, take a look at the [`translations`](../../../../runner/api/stock/autoscroll/interfaces/IAutoscroll.mdx#translations) property of the [Autoscroll stock runner](../../../../runner/stock/faces/autoscroll.mdx).
:::
#### Type
`(language: "auto" | string, context: string) => Promise<L10n.TTranslation | L10n.TTranslation[] | undefined>`


## 📢 Events {#events}

---
### 🏷️ `onPause` {#onPause}
Retrieves the pause function that is invoked when a form is paused.
#### Signature
```ts
{
  recipe: "email";
  onPause: (
    emailAddress: string,
    snapshot: ISnapshot,
    language: string,
    locale: string,
    namespace: string
  ) => Promise<void>;
}
```
#### Parameters
| Name           | Type                                                                      | Optional | Description                                                          |
|:---------------|:--------------------------------------------------------------------------|:---------|:---------------------------------------------------------------------|
| `emailAddress` | string                                                                    | No       | Contains the email address.                                          |
| `snapshot`     | [`ISnapshot`](../../../../runner/api/library/interfaces/ISnapshot.mdx) | No       | Contains the snapshot data.                                          |
| `language`     | string                                                                    | No       | Contains the language used in the runner (default is `en`).          |
| `locale`       | string                                                                    | No       | Contains the locale identifier used in the runner (default is `en`). |
| `namespace`    | string                                                                    | No       | Contains the namespace identifier for the runner.                    |
#### Return value
Returns a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) that resolves in case the pause succeeded or rejects in case of an error.

---
### 🏷️ `onReload` {#onReload}
Retrieves the reload function that is invoked when a form definition should reload from the server.
#### Signature
```ts
() => Promise<IDefinition>
```
#### Return value
Returns a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) that resolves with the [`IDefinition`](../../../../builder/api/interfaces/IDefinition.mdx).

---
### 🏷️ `onSubmit` {#onSubmit}
Retrieves the submit function that is invoked when a form response is submitted to the server.
#### Signature
```ts
(instance: Instance, language: string, locale: string, namespace: string) =>
  Promise<string | undefined>
```
#### Parameters
| Name        | Type                                                                 | Optional | Description                                                          |
|:------------|:---------------------------------------------------------------------|:---------|:---------------------------------------------------------------------|
| `instance`  | [`Instance`](../../../../runner/api/library/classes/Instance.mdx) | No       | Reference to the runner instance.                                    |
| `language`  | string                                                               | No       | Contains the language used in the runner (default is `en`).          |
| `locale`    | string                                                               | No       | Contains the locale identifier used in the runner (default is `en`). |
| `namespace` | string                                                               | No       | Contains the namespace identifier for the runner.                    |
#### Return value
Returns a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) that resolves with an optional reference string (in case it succeeds) or rejects (in case of an error).
