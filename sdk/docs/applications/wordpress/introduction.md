---
title: WordPress plugin
sidebar_label: Introduction
sidebar_position: 1
description: The Tripetto WordPress plugin is a complete and powerful plugin to create forms and surveys directly in your WordPress website.
---

# ![](/img/logo-wordpress.svg) Tripetto WordPress plugin

:::info Not a developer!?
If you are not a developer and just want to use Tripetto in your WordPress website, go to the [product page](https://tripetto.com/wordpress/) for end-users.
:::

## 🎓 What is it?
The Tripetto WordPress plugin is a complete and powerful plugin to create forms and surveys directly in your WordPress website. The plugin runs a stand-alone, full-blown form builder and stores all data right inside your WordPress environment without any dependencies on outside infrastructure. [GDPR friendly!](https://tripetto.com/blog/dont-trust-someone-else-with-your-form-data/)

## 👤 For who is it?
Anyone who owns a WordPress website and needs a form or survey solution.

## 🚀 Quickstart
Tripetto is listed in the [WordPress plugin directory](https://wordpress.org/plugins/). You can find and install the plugin directly from your WP Admin (go to the plugins page in your WP Admin, search for `Tripetto`, and hit the install button). Or, download the plugin manually from the plugin page on the WordPress website:

📥 [Download the Tripetto WordPress plugin](https://wordpress.org/plugins/tripetto/)

## 👩‍💻 For developers
The plugin supports hooks and filters to interact with Tripetto. For example, you can use a hook to trigger certain actions when a form response is submitted. Read the plugin API documentation for more information.

▶️ [Tripetto WordPress plugin API documentation](api/index.md)

📁 [WordPress plugin source code](https://gitlab.com/tripetto/wordpress/)
