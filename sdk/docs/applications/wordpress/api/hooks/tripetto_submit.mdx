---
title: tripetto_submit hook - WordPress plugin
sidebar_label: tripetto_submit
description: This hook is invoked whenever a form response is submitted.
---

# tripetto_submit hook

## 📖 Description {#description}
This hook is invoked whenever a form response is submitted. It's a perfect place to implement any server-side code you might want to run when a form is completed. In most cases, you can include this hook in the `functions.php` file of your WordPress theme using the [`add_action`](https://developer.wordpress.org/reference/functions/add_action/) WordPress function.

:::danger
Make sure to specify the correct number of arguments for the 4th parameter [`accepted_args`](https://developer.wordpress.org/reference/functions/add_action/#parameters) of the [`add_action`](https://developer.wordpress.org/reference/functions/add_action/) function. The number given there must match the number of parameters your function has!
:::

#### Syntax
```php showLineNumbers
add_action("tripetto_submit", function($data, $form) {
  // Do stuff with the data here
}, 10, 2);
```

#### Parameters
| Name    | Type   | Optional | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
|:--------|:-------|:---------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `$data` | object | No       | PHP JSON object that holds the form data and contains the following fields: <br/>- `id`: Contains the id of the response *(number)*;<br/>- `index`: Contains the index number of the response *(number)*;<br/>- `created`: Date and time the response was received *(string)*;<br/>- `fingerprint`: Contains the [fingerprint](../../../../builder/integrate/guides/fingerprint.mdx) of the form *(string)*;<br/>- `fields`: Contains an array with all fields that are collected in the response. Each item in this array is of type [`IExportableField`](../../../../runner/api/library/modules/Export.mdx#IExportableField). |
| `$form` | object | Yes      | PHP JSON object with form information. Contains the following fields:<br/>- `id`: Contains the id of the form *(number)*;<br/>- `reference`: Contains the reference of the form *(string)*;<br/>- `name`: Contains the name of the form *(string)*;<br/>- `fingerprint`: Contains the [fingerprint](../../../../builder/integrate/guides/fingerprint.mdx) of the form *(string)*;<br/>- `runner`: Contains the name of the runner used for the form *(string)*;<br/>- `created`: Contains the date the form was created *(string)*;<br/>- `modified`: Contains the last modified date of the form *(string)*.                      |

#### Example
```php showLineNumbers
add_action("tripetto_submit", function($data, $form) {
  // Let's send an email whenever a form is submitted
  $subject = "New form submission for " . $form->name;
  $message = $subject . "\n\n";

  // Add all the collected fields to the message
  foreach ($data->fields as $field) {
    $message .= $field->name . ": " . $field->string . "\n";
  }

  // Send it!
  mail("example@domain", $subject, $message);
}, 10, 2);
```

## 📁 Handling file uploads
If your form contains a [file upload block](../../../../blocks/stock/file-upload.mdx) or a [signature block](../../../../blocks/stock/signature.mdx) it is possible to use the uploaded file(s) in the `tripetto_submit` hook. Files are uploaded to a special (protected) folder on your WordPress server. The meta information for each file (file name, path, etc.) is stored in the `tripetto_attachments` SQL table. To fetch the uploaded file, you should use the [`reference`](../../../../runner/api/library/modules/Export.mdx#IExportableField-reference) property of the field dataset. This reference can then be used to retrieve the file information from the `tripetto_attachments` table.

#### Example
```php showLineNumbers
add_action("tripetto_submit", function($data, $form) {
  global $wpdb;

  // Let's assume the first field in the form dataset is a file upload
  // Retrieve the reference so we can use it to find the file
  $reference = $data->fields[0]->reference;

  $file = $wpdb->get_row(
    $wpdb->prepare("SELECT name,path from {$wpdb->prefix}tripetto_attachments where reference=%s", $reference)
  );

  if (!empty($file)) {
    // Now the path to the file is available through $file->path
    // The name of the file is available through $data->fields[0]->string or $file->name
  }
}, 10, 2);
```
