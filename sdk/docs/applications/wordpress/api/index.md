---
title: WordPress plugin API
sidebar_label: API
sidebar_position: 2
description: This API reference covers the available hooks and filters for the Tripetto WordPress plugin.
---

# ![](/img/logo-wordpress.svg) Tripetto WordPress plugin API
This API reference covers the available hooks and filters for the Tripetto WordPress plugin.

## ✨ Installation {#installation}
Tripetto is listed in the [WordPress plugin directory](https://wordpress.org/plugins/). You can find and install the plugin directly from your WP Admin. Or, download the plugin manually from the plugin page on the WordPress website:

📥 [Download the Tripetto WordPress plugin](https://wordpress.org/plugins/tripetto/)

## 👩‍💻 Example {#example}
Here is a basic example of the `tripetto_submit` hook that is invoked when a form is completed:
```php showLineNumbers
add_action("tripetto_submit", function($data, $form) {
  // Let's send an email whenever a form is submitted
  $subject = "New form submission for " . $form->name;
  $message = $subject . "\n\n";

  // Add all the collected fields to the message
  foreach ($data->fields as $field) {
    $message .= $field->name . ": " . $field->string . "\n";
  }

  // And send it!
  mail("example@domain", $subject, $message);
}, 10, 2);
```

## 🪝 Hooks {#hooks}
- [`tripetto_pause`](hooks/tripetto_pause.mdx)
- [`tripetto_submit`](hooks/tripetto_submit.mdx)

## 🩺 Filters {#filters}
- [`tripetto_prefill`](filters/tripetto_prefill.mdx)
- [`tripetto_styles`](filters/tripetto_styles.mdx)
- [`tripetto_webhook`](filters/tripetto_webhook.mdx)
