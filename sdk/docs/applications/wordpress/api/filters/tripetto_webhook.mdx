---
title: tripetto_webhook filter - WordPress plugin
sidebar_label: tripetto_webhook
description: This filter is invoked right before form data is pushed to a configured webhook.
---

# tripetto_webhook filter

## 📖 Description {#description}
This filter is invoked right before form data is pushed to a configured webhook (more information about configuring webhooks in Tripetto [here](https://tripetto.com/help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/)). It can be used to modify or enrich the data that is sent to the webhook endpoint. In most cases, you can include this hook in the `functions.php` file of your WordPress theme using the [`add_filter`](https://developer.wordpress.org/reference/functions/add_filter/) WordPress function.

:::danger
Make sure to specify the correct number of arguments for the 4th parameter [`accepted_args`](https://developer.wordpress.org/reference/functions/add_filter/#parameters) of the [`add_filter`](https://developer.wordpress.org/reference/functions/add_filter/) function. The number given there must match the number of parameters your filter function has!
:::

#### Syntax
```php showLineNumbers
add_filter("tripetto_webhook", function($data, $form, $dataset) {
  // Maybe modify $data in some way.

  // What you return here, is what's sent to the webhook endpoint.
  return $data;
}, 10, 3);
```

#### Parameters
| Name       | Type   | Optional | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
|:-----------|:-------|:---------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `$data`    | object | No       | PHP JSON object that holds the form data in a [name-value pair](https://en.wikipedia.org/wiki/Name%E2%80%93value_pair) format. The object also contains the following information about the submitted response data and form:<br/>- `tripettoId`: Identifier of the response data (can be used to retrieve the data from the `tripetto_entries` table) *(number)*;<br/>- `tripettoIndex`: Chronological index number of the response *(number)*;<br/>- `tripettoCreateDate`: Date and time of the response submission *(string)*;<br/>- `tripettoFingerprint`: [Fingerprint](../../../../builder/integrate/guides/fingerprint.mdx) hash of the form *(string)*;<br/>- `tripettoFormReference`: Reference of the form (can be used to retrieve the form from the `tripetto_forms` table) *(string)*;<br/>- `tripettoFormName`: Name of the form *(string)*. |
| `$form`    | object | Yes      | PHP JSON object with form information. Contains the following fields:<br/>- `id`: Contains the id of the form *(number)*;<br/>- `reference`: Contains the reference of the form *(string)*;<br/>- `name`: Contains the name of the form *(string)*;<br/>- `fingerprint`: Contains the [fingerprint](../../../../builder/integrate/guides/fingerprint.mdx) of the form *(string)*;<br/>- `runner`: Contains the name of the runner used for the form *(string)*;<br/>- `created`: Contains the date the form was created *(string)*;<br/>- `modified`: Contains the last modified date of the form *(string)*.                                                                                                                                                                                                                                          |
| `$dataset` | object | Yes      | PHP JSON object that holds the form data and contains the following fields: <br/>- `id`: Contains the id of the response *(number)*;<br/>- `index`: Contains the index number of the response *(number)*;<br/>- `created`: Date and time the response was received *(string)*;<br/>- `fingerprint`: Contains the [fingerprint](../../../../builder/integrate/guides/fingerprint.mdx) of the form *(string)*;<br/>- `fields`: Contains an array with all fields that are collected in the response. Each item in this array is of type [`IExportableField`](../../../../runner/api/library/modules/Export.mdx#IExportableField).                                                                                                                                                                                                                     |

:::caution
If you have enabled the setting `Send raw response data to webhook` for a form, the `$data` parameter will be identical to the `$dataset` parameter. Since this is a setting that can be changed by the form editor, we recommend to always use the `$dataset` parameter for retrieving form data as this parameter always uses the same data format.
:::

#### Example
```php showLineNumbers
add_filter("tripetto_webhook", function($data, $form, $dataset) {
  // Add something to the data
  $data->customField = "Test";

  return $data;
}, 10, 3);
```
You don't have to return the `$data` argument. You can return something else, like a text string:
```php showLineNumbers
add_filter("tripetto_webhook", function($data, $form) {
  return "This text string is now sent to the webhook endpoint instead of the data!";
}, 10, 2);
```

## 📁 Handling file uploads
If your form contains a [file upload block](../../../../blocks/stock/file-upload.mdx) or a [signature block](../../../../blocks/stock/signature.mdx) it is possible to use the uploaded file(s) in the `tripetto_webhook` filter. Files are uploaded to a special (protected) folder on your WordPress server. The meta information for each file (file name, path, etc.) is stored in the `tripetto_attachments` SQL table. To fetch the uploaded file, you should use the [`reference`](../../../../runner/api/library/modules/Export.mdx#IExportableField-reference) property of the field dataset. This reference can then be used to retrieve the file information from the `tripetto_attachments` table.

#### Example
```php showLineNumbers
add_filter("tripetto_webhook", function($data, $form, $dataset) {
  global $wpdb;

  // Let's assume the first field in the form dataset is a file upload
  // Retrieve the reference so we can use it to find the file
  $reference = $dataset->fields[0]->reference;

  $file = $wpdb->get_row(
    $wpdb->prepare("SELECT name,path from {$wpdb->prefix}tripetto_attachments where reference=%s", $reference)
  );

  if (!empty($file)) {
    // Now the path to the file is available through $file->path
    // The name of the file is available through $dataset->fields[0]->string or $file->name
  }

  return $data;
}, 10, 3);
```
