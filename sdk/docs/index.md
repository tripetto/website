---
title: Docs
sidebar_label: Introduction
sidebar_position: 1
description: The Tripetto SDK helps building powerful and deeply customizable forms for your application, web app, or website.
---

# Welcome to the Tripetto SDK

🙋‍♂️ This SDK helps building **powerful and deeply customizable forms for your application, web app, or website**.

👩‍💻 Create and run forms and surveys **without depending on external services**.

💸 Developing a custom form solution is tedious and expensive. Instead, use Tripetto and **save time and money!**

🌎 Trusted and used by organizations **around the globe**, including [**Fortune 500 companies**](https://en.wikipedia.org/wiki/Fortune_500).

## 📄 In a nutshell {#nutshell}
Tripetto is a full-fledged form kit. It contains a powerful graphical form builder (form designer) that lets you build smart flowing forms and surveys like flowcharts. You can use this builder as a [standalone tool](builder/cli/introduction.md) or [implement](builder/integrate/introduction.md) it in your web apps or websites. To run forms, you can use one of Tripetto's [stock runners](runner/stock/introduction.md) (out-of-the-box form UIs) or build your [custom form UI](runner/custom/introduction.md). Tripetto supports advanced logic features in its core. It is also extendible with [custom blocks](blocks/introduction.md) (question types).

## ✔️ Features {#features}
- Unique visual [form builder](builder/introduction.md) (form designer) lets you sketch forms and surveys like flowcharts
- Form builder [fully integrates](builder/integrate/introduction.md) into custom web apps without external dependencies
- Ready-to-go [runners](runner/introduction.md) to show forms in a modern [autoscroll UI](runner/stock/faces/autoscroll.mdx), a more [classic form UI](runner/stock/faces/classic.mdx), or a fancy [chat UI](runner/stock/faces/chat.mdx)
- Form runners also [fully integrate](runner/stock/quickstart/index.md) into custom web apps or websites without external dependencies
- Ready to use components for [React](https://react.dev) and [Angular](https://angular.io/) (but you can use plain JS as well)
- Packed with all the common [question types](blocks/stock/index.mdx) (we call them blocks)
- Extendable with [custom blocks](blocks/custom/introduction.md), so you can build your own question types
- Forms are defined by a [JSON form definition](builder/api/interfaces/IDefinition.mdx) (the builder generates this JSON and the runners consume it)
- Supports the use of [subforms](builder/integrate/guides/subforms.mdx) (forms within another form)
- Advanced [calculator block](blocks/stock/calculator.mdx) available to create quizzes, order forms, exams, and more ([learn more](https://tripetto.com/calculator/))
- [GDPR friendly](https://tripetto.com/blog/dont-trust-someone-else-with-your-form-data/), no external services required

## 🔎 How it works {#how-it-works}
To get a quick feeling with how the SDK works, have a look at our [How It Works](https://tripetto.com/sdk/how-it-works/) demo. It lets you play with the form builder which outputs a form definition in JSON format. You use that JSON form definition in a form runner which handles form rendering, complex logic and response collection. The runner captures collected data in a JSON format for delivery directly to your endpoint. From there on you can take charge of data storage without any dependencies on unwanted infrastructure.

## 🖱️ Try it {#try-it}
[![Play](/img/button-play.svg)](https://jdmt3.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-plain-js-live-preview-with-multiple-runners-jdmt3?file=/src/index.ts)

:::tip Tripetto Studio
Another great example of the Tripetto SDK in an end-user application is the [Tripetto Studio web app](https://tripetto.app). A free online tool for anyone who wants to create a form or survey ([learn more](applications/studio/introduction.md)).
:::

## 🚀 Quickstart {#quickstart}
### 🏗️ Form builder

<div className="youtube-embed"><iframe width="560" height="315" src="https://www.youtube.com/embed/78v901HZbD0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>

Integrate Tripetto's form builder and equip your web app with powerful form-building capabilities. The builder generates a ready-to-use [JSON form definition](builder/api/interfaces/IDefinition.mdx) that can be supplied to a runner (which turns it into a usable form).

[![Implement builder using plain JS](/img/button-js.svg)](builder/integrate/quickstart/plain-js/)
[![Implement builder using React](/img/button-react.svg)](builder/integrate/quickstart/react/)
[![Implement builder using Angular](/img/button-angular.svg)](builder/integrate/quickstart/angular/)
[![Implement builder using HTML](/img/button-html.svg)](builder/integrate/quickstart/html/)

:::tip
If you don't need a builder integration in your application, you can also use the [Tripetto Studio](https://tripetto.app) or the [CLI tool](builder/cli/introduction.md) to create a form and then embed that form using one of the [stock runners](runner/stock/introduction.md).
:::

### 🏃 Form runner

<div className="youtube-embed"><iframe width="560" height="315" src="https://www.youtube.com/embed/fixp89Yievg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>

Embed a Tripetto form in your website or application and start collecting responses. Simply add a runner and supply it with a [JSON form definition](builder/api/interfaces/IDefinition.mdx) generated by the builder.

[![Implement builder using plain JS](/img/button-js.svg)](runner/stock/quickstart/plain-js/)
[![Implement builder using React](/img/button-react.svg)](runner/stock/quickstart/react/)
[![Implement builder using Angular](/img/button-angular.svg)](runner/stock/quickstart/angular/)
[![Implement builder using HTML](/img/button-html.svg)](runner/stock/quickstart/html/)

## 🎓 Assumptions {#assumptions}
These docs assume that you are already familiar with [HTML](https://developer.mozilla.org/docs/Learn/HTML/Introduction_to_HTML), [CSS](https://developer.mozilla.org/docs/Learn/CSS/First_steps), [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript/A_re-introduction_to_JavaScript), and some of the tools from the [latest standards](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Language_Resources), such as [classes](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes) and [modules](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import). The code samples are written using [TypeScript](https://www.typescriptlang.org/).

## 📂 Open source {#open-source}
Tripetto's source code is hosted on [GitLab](https://gitlab.com/tripetto).

## ®️ Trademark {#trademark}
Tripetto is a [registered trademark](https://euipo.europa.eu/eSearch/#details/trademarks/015322051).
