---
title: Prerequisites - Guides - Builder
sidebar_label: Prerequisites
sidebar_position: 2
description: Overview of prerequisites to implement the Tripetto builder.
---

# Prerequisites
To be able to implement the Tripetto builder you need programming skills. For implementing the builder you use [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript/A_re-introduction_to_JavaScript) or [TypeScript](https://www.typescriptlang.org/). The latter has some advantages since it includes static type checking, which significantly reduces the risk of errors. All Tripetto packages include TypeScript typings (type declarations), providing excellent TypeScript support out-of-the-box.
