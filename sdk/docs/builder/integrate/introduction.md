---
title: Builder integration
sidebar_label: Introduction
sidebar_position: 1
description: You can completely integrate the Tripetto builder into your own web app or website.
toc_min_heading_level: 3
---

# 👩‍💻 Builder integration
You can completely integrate the Tripetto builder into your own web app or website. Integration allows you to enable powerful form and survey creation right inside your application. And since the builder is a pure client-side component, you can do so without becoming dependent on any external service not owned by you. The whole thing runs inside the context of your project, giving you full control.

🎓 This chapter contains all the information you need to **start implementing the builder**.

✍️ Implementing the builder is relatively easy, but you need some **JavaScript programming skills**.

📦 You will use the builder package published on **[npm](https://www.npmjs.com/package/@tripetto/builder)**.

💳 You need a **[license](../pricing.md)** to integrate the builder in commercial projects.

## 🚀 Quickstart {#quickstart}
[![Implement builder using plain JS](/img/button-js.svg)](../quickstart/plain-js/)
[![Implement builder using React](/img/button-react.svg)](../quickstart/react/)
[![Implement builder using Angular](/img/button-angular.svg)](../quickstart/angular/)
[![Implement builder using HTML](/img/button-html.svg)](../quickstart/html/)

▶️ [Implement builder using **plain JS**](quickstart/plain-js.mdx)

▶️ [Implement builder using **React**](quickstart/react.mdx)

▶️ [Implement builder using **Angular**](quickstart/angular.mdx)

▶️ [Implement builder using **HTML**](quickstart/html.mdx)

## 🎞️ Video {#video}
<div className="youtube-embed"><iframe width="560" height="315" src="https://www.youtube.com/embed/78v901HZbD0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
