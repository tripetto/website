---
title: HTML - Quickstart - Builder
sidebar_label: HTML
sidebar_position: 4
description: Implement builder using HTML.
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';
import UpNextTopics from './up-next-topics.include.mdx'

# ![](/img/logo-html.svg) Implement builder using HTML
You can use good old HTML to implement the builder. To do so, you need to load the builder library from a CDN or [host the library yourself](#self-host). If you want to use a CDN, you could go with [jsDelivr](https://cdn.jsdelivr.net/) or [unpkg](https://unpkg.com).

## 📄 Basic implementation {#implementation}
This code example shows the minimal code required to show the builder using simple HTML.

<Tabs groupId="cdn">
<TabItem value="jsdelivr" label="jsDelivr">

```html showLineNumbers
<html>
    <body>
        <!-- Load the Tripetto builder library -->
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/builder"></script>

        <!-- Fire it up! -->
        <script>
            Tripetto.Builder.open();
        </script>
    </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/29a188ee6c49b3568eeab94794639e16) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/29a188ee6c49b3568eeab94794639e16)

</TabItem>
<TabItem value="unpkg" label="unpkg">

```html showLineNumbers
<html>
    <body>
        <!-- Load the Tripetto builder library -->
        <script src="https://unpkg.com/@tripetto/builder"></script>

        <!-- Fire it up! -->
        <script>
            Tripetto.Builder.open();
        </script>
    </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/79c4785b567ce98a0042dae8d144be6a) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/79c4785b567ce98a0042dae8d144be6a)

</TabItem>
</Tabs>

## 👩‍💻 Use a custom HTML element {#custom-element}
By default, the builder opens in the body element of the HTML document. If you want to use a custom HTML element for the builder, you should supply the desired element to the [`element`](../../api/interfaces/IBuilderProperties.mdx#element) property as shown in the example below:

<Tabs groupId="cdn">
<TabItem value="jsdelivr" label="jsDelivr">

```html showLineNumbers
<html>
    <body>
        <!-- Load the Tripetto builder library -->
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/builder"></script>

        // highlight-start
        <!-- This div will host the builder -->
        <div id="CustomElement"></div>
        // highlight-end

        <!-- Fire it up! -->
        <script>
            // Create a new builder instance
            const builder = new Tripetto.Builder({
                // highlight-next-line
                element: document.getElementById("CustomElement")
            });

            // Open the builder
            builder.open();
        </script>
    </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/3812767695fd214a45983e43f0f579fa) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/3812767695fd214a45983e43f0f579fa)

</TabItem>
<TabItem value="unpkg" label="unpkg">

```html showLineNumbers
<html>
    <body>
        <!-- Load the Tripetto builder library -->
        <script src="https://unpkg.com/@tripetto/builder"></script>

        // highlight-start
        <!-- This div will host the builder -->
        <div id="CustomElement"></div>
        // highlight-end

        <!-- Fire it up! -->
        <script>
            // Create a new builder instance
            const builder = new Tripetto.Builder({
                // highlight-next-line
                element: document.getElementById("CustomElement")
            });

            // Open the builder
            builder.open();
        </script>
    </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/2838291a9e431e331a2a0002339978b1) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/2838291a9e431e331a2a0002339978b1)

</TabItem>
</Tabs>

:::warning
When you specify a custom HTML element for the builder, you need to make sure to set the `position` style attribute of that element to `absolute`, `relative`, `fixed` or `sticky`. Otherwise it is possible that the builder is displayed outside of the element boundaries.
:::

## 📦 Loading blocks {#blocks}
By default, the builder does not contain any blocks (question types). So, you need to load the desired blocks (more about making blocks available to the builder [here](../guides/blocks.mdx)). The following example shows how to load a set of [stock blocks](../../../blocks/stock/index.mdx) (blocks built and maintained by the Tripetto team):

<Tabs groupId="cdn">
<TabItem value="jsdelivr" label="jsDelivr">

```html showLineNumbers
<html>
    <body>
        <!-- Load the Tripetto builder library -->
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/builder"></script>

        // highlight-start
        <!-- Load blocks -->
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-calculator"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-checkbox"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-checkboxes"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-date"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-device"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-dropdown"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-email"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-error"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-evaluate"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-file-upload"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-hidden-field"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-mailer"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-matrix"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-multiple-choice"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-number"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-paragraph"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-password"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-phone-number"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-picture-choice"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-radiobuttons"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-rating"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-regex"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-scale"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-setter"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-signature"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-statement"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-stop"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-text"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-textarea"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-url"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-variable"></script>
        <script src="https://cdn.jsdelivr.net/npm/@tripetto/block-yes-no"></script>
        // highlight-end

        <!-- Fire it up! -->
        <script>
            Tripetto.Builder.open();
        </script>
    </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/2ff6822853f92be911c62605e68955b7) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/2ff6822853f92be911c62605e68955b7)

</TabItem>
<TabItem value="unpkg" label="unpkg">

```html showLineNumbers
<html>
    <body>
        <!-- Load the Tripetto builder library -->
        <script src="https://unpkg.com/@tripetto/builder"></script>

        // highlight-start
        <!-- Load blocks -->
        <script src="https://unpkg.com/@tripetto/block-calculator"></script>
        <script src="https://unpkg.com/@tripetto/block-checkbox"></script>
        <script src="https://unpkg.com/@tripetto/block-checkboxes"></script>
        <script src="https://unpkg.com/@tripetto/block-date"></script>
        <script src="https://unpkg.com/@tripetto/block-device"></script>
        <script src="https://unpkg.com/@tripetto/block-dropdown"></script>
        <script src="https://unpkg.com/@tripetto/block-email"></script>
        <script src="https://unpkg.com/@tripetto/block-error"></script>
        <script src="https://unpkg.com/@tripetto/block-evaluate"></script>
        <script src="https://unpkg.com/@tripetto/block-file-upload"></script>
        <script src="https://unpkg.com/@tripetto/block-hidden-field"></script>
        <script src="https://unpkg.com/@tripetto/block-mailer"></script>
        <script src="https://unpkg.com/@tripetto/block-matrix"></script>
        <script src="https://unpkg.com/@tripetto/block-multiple-choice"></script>
        <script src="https://unpkg.com/@tripetto/block-number"></script>
        <script src="https://unpkg.com/@tripetto/block-paragraph"></script>
        <script src="https://unpkg.com/@tripetto/block-password"></script>
        <script src="https://unpkg.com/@tripetto/block-phone-number"></script>
        <script src="https://unpkg.com/@tripetto/block-picture-choice"></script>
        <script src="https://unpkg.com/@tripetto/block-radiobuttons"></script>
        <script src="https://unpkg.com/@tripetto/block-rating"></script>
        <script src="https://unpkg.com/@tripetto/block-regex"></script>
        <script src="https://unpkg.com/@tripetto/block-scale"></script>
        <script src="https://unpkg.com/@tripetto/block-setter"></script>
        <script src="https://unpkg.com/@tripetto/block-signature"></script>
        <script src="https://unpkg.com/@tripetto/block-statement"></script>
        <script src="https://unpkg.com/@tripetto/block-stop"></script>
        <script src="https://unpkg.com/@tripetto/block-text"></script>
        <script src="https://unpkg.com/@tripetto/block-textarea"></script>
        <script src="https://unpkg.com/@tripetto/block-url"></script>
        <script src="https://unpkg.com/@tripetto/block-variable"></script>
        <script src="https://unpkg.com/@tripetto/block-yes-no"></script>
        // highlight-end

        <!-- Fire it up! -->
        <script>
            Tripetto.Builder.open();
        </script>
    </body>
</html>
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/4de58f7369cace52dd68137aac0f7523) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/4de58f7369cace52dd68137aac0f7523)

</TabItem>
</Tabs>

## 🌎 Self-host the library {#self-host}
If you want to host the builder library on a custom domain, you must publish the required JavaScript file to that domain. To do so, download the following file (right-click and select save as) and publish it to the desired domain. Update the `src`-attributes of the `script` tags, so it points to the domain and location of the JavaScript file.

📥 https://unpkg.com/@tripetto/builder

## ⏭️ Up next {#up-next}
<UpNextTopics />
