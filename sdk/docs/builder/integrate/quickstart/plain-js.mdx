---
title: Plain JS - Quickstart - Builder
sidebar_label: Plain JS
sidebar_position: 1
description: Implement builder using plain JS.
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';
import UpNextTopics from './up-next-topics.include.mdx'

# ![](/img/logo-js.svg) Implement builder using plain JS
You can implement the builder using plain JS.

## ✅ Add package to your project {#packages}
First of all, you need to add the [Tripetto builder package](https://www.npmjs.com/package/@tripetto/builder) to your project. This package is published on [npm](https://www.npmjs.com). To do so, run the following command:

```bash npm2yarn
npm install @tripetto/builder
```

## 📄 Basic implementation {#implementation}
This code example shows the minimal code required to show the builder. It uses the static [`open`](../../api/classes/Builder.mdx#static-open) method to open the builder in a single call. This will open the builder in the body element of the browser.

```js showLineNumbers
import { Builder } from "@tripetto/builder";

// Open the builder
Builder.open();
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/f7884595bf8178137fab2b4894f9fc20) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/f7884595bf8178137fab2b4894f9fc20)

## 👩‍💻 Use a custom HTML element {#custom-element}
By default, the builder opens in the body element of the HTML document. If you want to use a custom HTML element for the builder, you should supply the desired element to the [`element`](../../api/interfaces/IBuilderProperties.mdx#element) property as shown in the example below:

```js showLineNumbers
import { Builder } from "@tripetto/builder";

// Create a new builder instance
const builder = new Builder({
    // highlight-next-line
    element: document.getElementById("CustomElement")
});

// Open the builder
builder.open();
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/45555fd8551d7a7e25b0444ffacaff59) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/45555fd8551d7a7e25b0444ffacaff59)

:::caution
When you specify a custom HTML element for the builder, you need to make sure to set the `position` style attribute of that element to `absolute`, `relative`, `fixed` or `sticky`. Otherwise it is possible that the builder is displayed outside of the element boundaries.
:::

## 📦 Loading blocks {#blocks}
By default, the builder does not contain any blocks (question types). So, you need to load the desired blocks (more about making blocks available to the builder [here](../guides/blocks.mdx)). The following example shows how to load a set of [stock blocks](../../../blocks/stock/index.mdx) (blocks built and maintained by the Tripetto team). The first step is to add these packages to your project:

```bash npm2yarn
npm install @tripetto/block-calculator @tripetto/block-checkbox @tripetto/block-checkboxes @tripetto/block-date @tripetto/block-device @tripetto/block-dropdown @tripetto/block-email @tripetto/block-error @tripetto/block-evaluate @tripetto/block-file-upload @tripetto/block-hidden-field @tripetto/block-mailer @tripetto/block-matrix @tripetto/block-multi-select @tripetto/block-multiple-choice @tripetto/block-number @tripetto/block-paragraph @tripetto/block-password @tripetto/block-phone-number @tripetto/block-picture-choice @tripetto/block-radiobuttons @tripetto/block-ranking @tripetto/block-rating @tripetto/block-regex @tripetto/block-scale @tripetto/block-setter @tripetto/block-signature @tripetto/block-statement @tripetto/block-stop @tripetto/block-text @tripetto/block-textarea @tripetto/block-url @tripetto/block-variable @tripetto/block-yes-no
```

Next, add imports to your code to load the blocks (the blocks self-register and become available to the builder):

```js showLineNumbers
import { Builder } from "@tripetto/builder";

// highlight-start
// Load the stock blocks
import "@tripetto/block-calculator";
import "@tripetto/block-checkbox";
import "@tripetto/block-checkboxes";
import "@tripetto/block-date";
import "@tripetto/block-device";
import "@tripetto/block-dropdown";
import "@tripetto/block-email";
import "@tripetto/block-error";
import "@tripetto/block-evaluate";
import "@tripetto/block-file-upload";
import "@tripetto/block-hidden-field";
import "@tripetto/block-mailer";
import "@tripetto/block-matrix";
import "@tripetto/block-multi-select";
import "@tripetto/block-multiple-choice";
import "@tripetto/block-number";
import "@tripetto/block-paragraph";
import "@tripetto/block-password";
import "@tripetto/block-phone-number";
import "@tripetto/block-picture-choice";
import "@tripetto/block-radiobuttons";
import "@tripetto/block-ranking";
import "@tripetto/block-rating";
import "@tripetto/block-regex";
import "@tripetto/block-scale";
import "@tripetto/block-setter";
import "@tripetto/block-signature";
import "@tripetto/block-statement";
import "@tripetto/block-stop";
import "@tripetto/block-text";
import "@tripetto/block-textarea";
import "@tripetto/block-url";
import "@tripetto/block-variable";
import "@tripetto/block-yes-no";
// highlight-end

// Open the builder
Builder.open();
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/8b40f4c95c105914df766aa523a0b5eb) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/8b40f4c95c105914df766aa523a0b5eb)

:::tip
The example above uses static imports for adding the stock block packages. It results in a code bundle that includes all the imported blocks. There is also an option to dynamically load blocks. That preserves JS bundle bloat. Please read the [Block loading guide](../guides/blocks.mdx) for more information about loading blocks.
:::

## ⏭️ Up next {#up-next}
<UpNextTopics />
