---
title: Quickstarts - Builder
sidebar_label: Quickstart
description: This chapter shows you different approaches to implement the builder.
toc_min_heading_level: 3
---

# Builder quickstart
The Tripetto builder does not depend on a specific library or framework. So it should be able to implement it in any environment that runs [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript/A_re-introduction_to_JavaScript). This chapter shows you different approaches to implement the builder. Please select how you want to use the builder:

[![Implement builder using plain JS](/img/button-js.svg)](plain-js/)
[![Implement builder using React](/img/button-react.svg)](react/)
[![Implement builder using Angular](/img/button-angular.svg)](angular/)
[![Implement builder using HTML](/img/button-html.svg)](html/)

▶️ [Implement builder using **plain JS**](plain-js.mdx)

▶️ [Implement builder using **React**](react.mdx)

▶️ [Implement builder using **Angular**](angular.mdx)

▶️ [Implement builder using **HTML**](html.mdx)

## 🎞️ Video {#video}
<div className="youtube-embed"><iframe width="560" height="315" src="https://www.youtube.com/embed/78v901HZbD0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
