---
title: Angular - Quickstart - Builder
sidebar_label: Angular
sidebar_position: 3
description: Implement builder using Angular.
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';
import UpNextTopics from './up-next-topics.include.mdx'

# ![](/img/logo-angular.svg) Implement builder using Angular
The Tripetto builder package comes with a built-in [Angular](https://angular.io/) component. This makes it very easy to use the builder in an [Angular](https://angular.io/) application. This quickstart shows how to use it.

## ✅ Add package to your project {#packages}
First of all, you need to add the [Tripetto builder package](https://www.npmjs.com/package/@tripetto/builder) to your project. This package is published on [npm](https://www.npmjs.com). To do so, run the following command:

```bash npm2yarn
npm install @tripetto/builder
```

## 📄 Basic implementation {#implementation}
To use the component, simply import `TripettoBuilderModule` from the package and feed it to your application's `@NgModule` imports array. This makes the [`<tripetto-builder>`](../../api/components/angular.mdx) selector available in your application. The Angular component is located in a subfolder named `angular` inside the builder package. Here's an example:

<Tabs>
<TabItem value="app-module" label="App module">

```ts showLineNumbers title="app.module.ts"
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
//highlight-start
import { TripettoBuilderModule } from "@tripetto/builder/angular";
//highlight-end

@NgModule({
  declarations: [AppComponent],
  //highlight-start
  imports: [BrowserModule, TripettoBuilderModule],
  //highlight-end
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
```

</TabItem>
<TabItem value="app-html" label="App HTML">

```html showLineNumbers title="app.component.html"
<tripetto-builder (onSave)="formSaved($event)"></tripetto-builder>
```

</TabItem>
<TabItem value="app-component" label="App component">

```ts showLineNumbers title="app.component.ts"
import { Component } from "@angular/core";
//highlight-start
import { IDefinition } from "@tripetto/builder";
//highlight-end

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  //highlight-start
  formSaved(definition: IDefinition) {
    // Do something with the form definition
    console.log(definition);
  }
  //highlight-end
}
```

</TabItem>
</Tabs>

[![Run](/img/button-run.svg)](https://6lc5e.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-angular-basic-implementation-6lc5e)

## 📦 Loading blocks {#blocks}
By default, the builder does not contain any blocks (question types). So, you must load the desired blocks. There is a specific [guide](../guides/blocks.mdx) about the different options for loading blocks. But as an example, you can load a set of [stock blocks](../../../blocks/stock/index.mdx) (blocks built and maintained by the Tripetto team) simply by importing them. The first step is to add these packages to your project:

```bash npm2yarn
npm install @tripetto/block-calculator @tripetto/block-checkbox @tripetto/block-checkboxes @tripetto/block-date @tripetto/block-device @tripetto/block-dropdown @tripetto/block-email @tripetto/block-error @tripetto/block-evaluate @tripetto/block-file-upload @tripetto/block-hidden-field @tripetto/block-mailer @tripetto/block-matrix @tripetto/block-multi-select @tripetto/block-multiple-choice @tripetto/block-number @tripetto/block-paragraph @tripetto/block-password @tripetto/block-phone-number @tripetto/block-picture-choice @tripetto/block-radiobuttons @tripetto/block-ranking @tripetto/block-rating @tripetto/block-regex @tripetto/block-scale @tripetto/block-setter @tripetto/block-signature @tripetto/block-statement @tripetto/block-stop @tripetto/block-text @tripetto/block-textarea @tripetto/block-url @tripetto/block-variable @tripetto/block-yes-no
```

Next, add imports to your code to load the appropriate blocks (the blocks will self-register and become available to the builder):

```ts showLineNumbers title="app.module.ts"
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
import { TripettoBuilderModule } from "@tripetto/builder/angular";

// highlight-start
// Load the blocks
import "@tripetto/block-calculator";
import "@tripetto/block-checkbox";
import "@tripetto/block-checkboxes";
import "@tripetto/block-date";
import "@tripetto/block-device";
import "@tripetto/block-dropdown";
import "@tripetto/block-email";
import "@tripetto/block-error";
import "@tripetto/block-evaluate";
import "@tripetto/block-file-upload";
import "@tripetto/block-hidden-field";
import "@tripetto/block-mailer";
import "@tripetto/block-matrix";
import "@tripetto/block-multi-select";
import "@tripetto/block-multiple-choice";
import "@tripetto/block-number";
import "@tripetto/block-paragraph";
import "@tripetto/block-password";
import "@tripetto/block-phone-number";
import "@tripetto/block-picture-choice";
import "@tripetto/block-radiobuttons";
import "@tripetto/block-ranking";
import "@tripetto/block-rating";
import "@tripetto/block-regex";
import "@tripetto/block-scale";
import "@tripetto/block-setter";
import "@tripetto/block-signature";
import "@tripetto/block-statement";
import "@tripetto/block-stop";
import "@tripetto/block-text";
import "@tripetto/block-textarea";
import "@tripetto/block-url";
import "@tripetto/block-variable";
import "@tripetto/block-yes-no";
// highlight-end

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, TripettoBuilderModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
```
[![Run](/img/button-run.svg)](https://y1ws5.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-angular-loading-blocks-y1ws5)

:::tip
The example above uses static imports for adding the stock block packages. It results in a code bundle that includes all the imported blocks. There is also an option to dynamically load blocks. That preserves JS bundle bloat. Please read the [Block loading guide](../guides/blocks.mdx) for more information about loading blocks.
:::

## ⏭️ Up next {#up-next}
<UpNextTopics />
