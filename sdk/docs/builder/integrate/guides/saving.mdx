---
title: Saving forms - Guides - Builder
sidebar_label: Saving forms
sidebar_position: 2
description: This guide shows you different approaches you could use for saving form definitions.
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Saving form definitions
The builder stores the complete form structure in a single JavaScript object called the [form definition](../../api/interfaces/IDefinition.mdx). It includes all the properties and settings of the elements used in the form. From there on, it is up to you what you want to do with the form definition (probably you want to store it somewhere). This guide shows you different approaches you could use for saving form definitions.

:::tip
If you want to validate form definitions before saving them, read the [Validating forms guide](validation.mdx).
:::

## 🆗 Use a save button {#save-button}
When you want to use a save button, you need to listen to the [`onSave`](../../api/classes/Builder.mdx#onSave) event of the builder. This event emits when the [`save`](../../api/classes/Builder.mdx#save) method is invoked. If you use the standard builder configuration, a toolbar is shown inside the builder. This toolbar contains a save button by default. When the user clicks the save button, it calls the [`save`](../../api/classes/Builder.mdx#save) method.

```ts showLineNumbers
import { Builder } from "@tripetto/builder";

// Use the `onSave` event
const builder = new Builder({
    onSave: (definition) => {
        // This gets called when a save is initiated
    }
});

// You can also set the `onSave` property later on
builder.onSave = (definition) => {
    // This gets called when a save is initiated
};

// Or use a hook to listen to the event
builder.hook("OnSave", "synchronous", (event: IBuilderSaveEvent) => {
    // This gets called when a save is initiated
    // `event.definition` contains the form definition
});
```

## ✏️ Use save on change {#save-on-change}
If you want to save the form definition on each change, you need to listen to the [`onChange`](../../api/classes/Builder.mdx#onChange) event of the builder. This event emits on each change of the form. If you use this approach, you could [disable the save button](../../api/interfaces/IBuilderProperties.mdx#disableSaveButton) in the toolbar as it is no longer needed. Also, there is no need to invoke the [`save`](../../api/classes/Builder.mdx#save) method.

```ts showLineNumbers
import { Builder } from "@tripetto/builder";

// Use the `onChange` event
const builder = new Builder({
    onChange: (definition) => {
        // This gets called on each change
    }
});

// You can also set the `onChange` property later on
builder.onChange = (definition) => {
    // This gets called on each change
};

// Or use a hook to listen to the event
builder.hook("OnChange", "synchronous", (event: IBuilderChangeEvent) => {
    // This gets called on each change
    // `event.definition` contains the form definition
});
```

### Removing the save button from the toolbar
To remove the save button from the toolbar, you should use the [`disableSaveButton`](../../api/interfaces/IBuilderProperties.mdx#disableSaveButton) property when creating a new builder instance.

```ts showLineNumbers
const builder = new Builder({
    disableSaveButton: true
});
```

You can also remove the entire toolbar if you want. This [guide](toolbar.mdx) shows you how to do that.

## 👩‍💻 Simple AJAX example {#example}
The actual saving of a form definition to a data store is entirely up to you. You could, for example, use an AJAX call to post a form to a server.

<Tabs>
<TabItem value="fetch" label="fetch()">

```ts showLineNumbers
import { Builder } from "@tripetto/builder";

const builder = new Builder({
    onSave: (definition) => {
        fetch("/example-server", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(definition)
        });
    }
});

// Save the form
builder.save();
```

</TabItem>
<TabItem value="XMLHttpRequest" label="XMLHttpRequest">

```ts showLineNumbers
import { Builder } from "@tripetto/builder";

const builder = new Builder({
    onSave: (definition) => {
        const httpRequest = new XMLHttpRequest();

        httpRequest.open("POST", "/example-server");
        httpRequest.send(JSON.stringify(definition));
    }
});

// Save the form
builder.save();

```

</TabItem>
<TabItem value="SuperAgent" label="SuperAgent">

```ts showLineNumbers
import { Builder } from "@tripetto/builder";
import { post } from "superagent";

const builder = new Builder({
    onSave: (definition) => {
        post("/example-server")
            .send(definition);
    }
});

// Save the form
builder.save();
```

</TabItem>
<TabItem value="Axios" label="Axios">

```ts showLineNumbers
import { Builder } from "@tripetto/builder";
import { post } from "axios";

const builder = new Builder({
    onSave: (definition) => {
        post("/example-server", definition);
    }
});

// Save the form
builder.save();
```

</TabItem>
</Tabs>
