---
title: Resizing - Guides - Builder
sidebar_label: Resizing
sidebar_position: 13
description: The builder automatically monitors changes in the dimensions of the parent element used for rendering the builder.
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Handling viewport/screen resizing
The builder automatically monitors changes in the dimensions of the parent element used for rendering the builder. This allows the builder to adapt to changes in the viewport dimensions. If you don't want the builder to monitor this automatically, you can disable that and manually handle the process.

:::tip
You only need to use this feature when you want to disable automatic detection of viewport changes.
:::

## Disable automatic resizing
To disable monitoring viewport changes, use the [`disableResizing`](../../api/interfaces/IBuilderProperties.mdx#disableResizing) property:
```ts showLineNumbers
import { Builder } from "@tripetto/builder";

const builder = new Builder({
  element:  document.getElementById("CustomElement"),
  //highlight-start
  disableResizing: true
  //highlight-end
});

builder.open();
```

## Detecting viewport resize
The most common scenario is a browser window resize or screen orientation change. In both cases, you should monitor this and then invoke the [`resize`](../../api/classes/Builder.mdx#resize) method of your builder instance. You can also use the [`ResizeObserver`](#ResizeObserver) API for this when you target modern browsers.

:::info
There is no need to debounce calls to the [`resize`](../../api/classes/Builder.mdx#resize) method. This is already done inside the builder.
:::

### Example using global resize events
This example shows how to use the [`resize`](https://developer.mozilla.org/en-US/docs/Web/API/Window/resize_event) and [`orientationchange`](https://developer.mozilla.org/en-US/docs/Web/API/Window/orientationchange_event) events of the browser.
```ts showLineNumbers
import { Builder } from "@tripetto/builder";

const builder = new Builder({
  element:  document.getElementById("CustomElement"),
  //highlight-start
  disableResizing: true
  //highlight-end
});

builder.open();

//highlight-start
// Upon window resize notify the builder
window.addEventListener("resize", () => builder.resize());
window.addEventListener("orientationchange", () => builder.resize());
//highlight-end
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/ef9f7d26de2821c9af7979ec2685df28) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/ef9f7d26de2821c9af7979ec2685df28)

### Example using `ResizeObserver` {#ResizeObserver}
If you target modern browsers, you can use the [`ResizeObserver`](https://developer.mozilla.org/en-US/docs/Web/API/ResizeObserver) API to monitor changes in the dimensions of the host element. This can be more efficient than listening to the global resize event of the browser window. Especially in a component-based approach like when using [React](../quickstart/react.mdx).

<Tabs>
<TabItem value="plain" label="Plain JS">

```ts showLineNumbers
import { Builder } from "@tripetto/builder";

// Let's assume you have a host element with the id `CustomElement`
const element = document.getElementById("CustomElement");

const builder = new Builder({
  element,
  //highlight-start
  disableResizing: true,
  onClose: () => {
    resizeObserver.disconnect();
  }
  //highlight-end
});

//highlight-start
const resizeObserver = new ResizeObserver(() => {
  builder.resize();
});

resizeObserver.observe(element);
//highlight-end
builder.open();

```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/39b61abe617742f00b63522b3395eb44) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/39b61abe617742f00b63522b3395eb44)

</TabItem>
<TabItem value="react" label="React">

```tsx showLineNumbers
import React, { useEffect, useRef } from "react";
import { Builder, IDefinition } from "@tripetto/builder";

function TripettoBuilder() {
  const elementRef = useRef<HTMLDivElement>(null);
  const builderRef = useRef<Builder>();

  useEffect(() => {
    if (!builderRef.current) {
      builderRef.current = new Builder({
        element: elementRef.current,
        //highlight-start
        disableResizing: true
        // highlight-end
      });
    }
  }, []);

  // highlight-start
  useEffect(() => {
    const resizeObserver = new ResizeObserver(() => {
      if (builderRef.current) {
        builderRef.current.resize();
      }
    });

    resizeObserver.observe(elementRef.current);

    return () => {
      resizeObserver.disconnect();
    };
  });
  // highlight-end

  return (<div ref={elementRef}></div>);
}
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/4b8d317efdaa40ee46b59eae92b3b0d4) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/4b8d317efdaa40ee46b59eae92b3b0d4)

</TabItem>
<TabItem value="angular" label="Angular">

```ts showLineNumbers
import { Component, Input, Output, ElementRef, NgZone, EventEmitter, OnInit, OnDestroy, ChangeDetectionStrategy } from "@angular/core";
import { Builder, IDefinition } from "@tripetto/builder";

@Component({
  selector: "tripetto-builder",
  template: "",
  styleUrls: ["./builder.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BuilderComponent implements OnInit, OnDestroy {
  private builder?: Builder;
  private initialDefinition?: IDefinition;
  // highlight-start
  private resizeObserver?: ResizeObserver;
  // highlight-end

  /** Specifies the form definition. */
  @Input() set definition(definition: IDefinition | undefined) {
    if (this.builder) {
      this.zone.runOutsideAngular(() => {
        this.builder.definition = definition;
      });

      return;
    }

    this.initialDefinition = definition;
  }

  /** Retrieves the form definition. */
  get definition(): IDefinition | undefined {
    return (this.builder && this.builder.definition) || this.initialDefinition;
  }

  /**
   * Invoked when the form definition is saved.
   * @event
   */
  @Output() saved = new EventEmitter<IDefinition>();

  constructor(private element: ElementRef, private zone: NgZone) {}

  ngOnInit() {
    // Leave the builder outside of Angular to avoid unnecessary and costly change detection.
    this.zone.runOutsideAngular(() => {
      this.builder = Builder.open(this.definition, {
        element: this.element.nativeElement,
        //highlight-start
        disableResizing: true,
        // highlight-end
        onSave: (definition) => {
          this.saved.emit(definition);
        }
      });

      // highlight-start
      // Monitor element resizing
      this.resizeObserver = new ResizeObserver(() => {
        this.builder?.resize();
      });

      this.resizeObserver.observe(this.element.nativeElement);
      // highlight-end
    });
  }

  ngOnDestroy() {
    // highlight-start
    this.resizeObserver.disconnect();
    this.resizeObserver = undefined;
    // highlight-end

    this.builder.destroy();
    this.builder = undefined;
  }
}
```
[![Run](/img/button-run.svg)](https://gyjx9.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-angular-monitor-host-element-resizing-gyjx9?file=/src/app/builder.component.ts)

</TabItem>
</Tabs>
