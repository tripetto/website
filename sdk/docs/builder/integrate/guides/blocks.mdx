---
title: Loading blocks - Guides - Builder
sidebar_label: Loading blocks
sidebar_position: 4
description: Guide for loading blocks and namespaces.
---

# Loading blocks and namespaces
By default, the builder does not contain any blocks (question types). So, you need to load the desired blocks yourself. There are two options to do this:
- Using static imports (compile-time loading)
- Using dynamic loading (run-time or lazy loading)

Loading using static imports is the most easy way to load blocks. It has one disadvantage, the loaded blocks become part of your JS bundle. If you need/want to avoid that, use dynamic loading to load the blocks at runtime.

## 🧱 Static loading using imports {#static-loading}
Static loading of blocks is very simple. Just import the block, using the [`import`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/import) statement. Tripetto blocks will self-register and become available to the builder.

### Importing stock blocks
[Stock blocks](../../../blocks/stock/index.mdx) are blocks that are built and maintained by the Tripetto team. To use them, you first need to add them to your project. The stock blocks are distributed as packages using npm. To add all the available stock blocks run the following command:

```bash npm2yarn
npm install @tripetto/block-calculator @tripetto/block-checkbox @tripetto/block-checkboxes @tripetto/block-date @tripetto/block-device @tripetto/block-dropdown @tripetto/block-email @tripetto/block-error @tripetto/block-evaluate @tripetto/block-file-upload @tripetto/block-hidden-field @tripetto/block-mailer @tripetto/block-matrix @tripetto/block-multi-select @tripetto/block-multiple-choice @tripetto/block-number @tripetto/block-paragraph @tripetto/block-password @tripetto/block-phone-number @tripetto/block-picture-choice @tripetto/block-radiobuttons @tripetto/block-ranking @tripetto/block-rating @tripetto/block-regex @tripetto/block-scale @tripetto/block-setter @tripetto/block-signature @tripetto/block-statement @tripetto/block-stop @tripetto/block-text @tripetto/block-textarea @tripetto/block-url @tripetto/block-variable @tripetto/block-yes-no
```

Next, add imports to your code to load the blocks:

```js showLineNumbers
import "@tripetto/block-calculator";
import "@tripetto/block-checkbox";
import "@tripetto/block-checkboxes";
import "@tripetto/block-date";
import "@tripetto/block-device";
import "@tripetto/block-dropdown";
import "@tripetto/block-email";
import "@tripetto/block-error";
import "@tripetto/block-evaluate";
import "@tripetto/block-file-upload";
import "@tripetto/block-hidden-field";
import "@tripetto/block-mailer";
import "@tripetto/block-matrix";
import "@tripetto/block-multi-select";
import "@tripetto/block-multiple-choice";
import "@tripetto/block-number";
import "@tripetto/block-paragraph";
import "@tripetto/block-password";
import "@tripetto/block-phone-number";
import "@tripetto/block-picture-choice";
import "@tripetto/block-radiobuttons";
import "@tripetto/block-ranking";
import "@tripetto/block-rating";
import "@tripetto/block-regex";
import "@tripetto/block-scale";
import "@tripetto/block-setter";
import "@tripetto/block-signature";
import "@tripetto/block-statement";
import "@tripetto/block-stop";
import "@tripetto/block-text";
import "@tripetto/block-textarea";
import "@tripetto/block-url";
import "@tripetto/block-variable";
import "@tripetto/block-yes-no";
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/8b40f4c95c105914df766aa523a0b5eb) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/8b40f4c95c105914df766aa523a0b5eb)

### Import block bundles
If you use one of the stock runners (runners built and maintained by the Tripetto team), you can also load the blocks bundle that is included in the stock runner packages. This bundle contains all the blocks that are used by the stock runner. This makes it easy to load the correct blocks and eliminates the need to add separate block packages to your project.

The block bundles are stored in the `./builder` folder of the stock runner packages. Loading is very easy and can be done with a single line of code:

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll runner">

```ts showLineNumbers
import "@tripetto/runner-autoscroll/builder";

// The code above imports the ES5 or ESM version based on your project configuration.
// If you want to use the ES5 version, you can do an explicit import:
import "@tripetto/runner-autoscroll/builder/es5";
```

</TabItem>
<TabItem value="chat" label="Chat runner">

```ts showLineNumbers
import "@tripetto/runner-chat/builder";

// The code above imports the ES5 or ESM version based on your project configuration.
// If you want to use the ES5 version, you can do an explicit import:
import "@tripetto/runner-chat/builder/es5";
```

</TabItem>
<TabItem value="classic" label="Classic runner">

```ts showLineNumbers
import "@tripetto/runner-classic/builder";

// The code above imports the ES5 or ESM version based on your project configuration.
// If you want to use the ES5 version, you can do an explicit import:
import "@tripetto/runner-classic/builder/es5";
```

</TabItem>
</Tabs>

### Importing custom blocks
If you are [building custom blocks](../../../blocks/custom/introduction.md), you can just import the file that implements the custom block.
```ts
import "./custom-block";
```

### Loading blocks in namespaces
It is possible to load blocks in different namespaces. This is mainly helpful if you are implementing a [live preview with multiple runners](livepreview.mdx#preview-multiple-runners). In that case you can load the required blocks (or blocks bundle) for each runner. This allows you to easily switch the builder from one namespace to another using the [`useNamespace`](../../api/classes/Builder.mdx#useNamespace) method.

If you use static imports, you need the help of two functions from the tripetto builder package to define the namespace and load it with the right blocks: [`mountNamespace`](../../api/functions/mountNamespace.mdx) and [`unmountNamespace`](../../api/functions/unmountNamespace.mdx).

```ts showLineNumbers
import { mountNamespace, unmountNamespace } from "@tripetto/builder";

// Creates a namespace with the name `custom-namespace`
mountNamespace("custom-namespace");

// Import some blocks
import "@tripetto/blocks-dropdown";
import "@tripetto/blocks-text";

// Import a block bundle
import "@tripetto/runner-autoscroll/builder";

// Call this when you are done importing blocks
unmountNamespace();

// The blocks `@tripetto/blocks-dropdown`, `@tripetto/blocks-text`, and
// all the blocks in the autoscroll bundle are now part of the namespace `custom-namespace`.

// Now, you can switch to this namespace in any builder instance.
const builder = new Builder();

builder.useNamespace("custom-namespace");
```

## ⌛ Dynamic loading or lazy loading {#dynamic-loading}
Dynamic loading allows block loading during run-time. Using this method you can create separate JS bundles with the blocks you need and then load those block bundles whenever they are required in the application. This way you can keep your application JS bundle as compact as possible. This only makes sense for applications that don't show the builder to the user immediately. For applications that show the builder immediately to the user, it is better to use the static approach for loading blocks as described above.

:::warning
If your application implements a [Content Security Policy (CSP)](https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP) you have to make sure to allow the Trusted Type policy named `tripetto#loader`. More on CSP and Trusted Types can be found in this [guide](csp.mdx).
:::

### Loading a block bundle during instance construction
If you want to load blocks when constructing a new builder instance, you can use the [`namespace`](../../api/interfaces/IBuilderProperties.mdx#namespace) property in the builder constructor.

```ts showLineNumbers
import { Builder } from "@tripetto/builder";

const builder = new Builder({
    namespace: {
        identifier: "custom-namespace",
        url: "https://unpkg.com/@tripetto/runner-autoscroll/builder"
    }
});
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/9a8ab5deb2199461648590730224b6b7) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/9a8ab5deb2199461648590730224b6b7)

### Loading a block bundle after instance construction
You can load block bundles after instance construction using the [`useNamespace`](../../api/classes/Builder.mdx#useNamespace) method.

```ts showLineNumbers
import { Builder } from "@tripetto/builder";

const builder = new Builder();

// Load the builder blocks for the autoscroll runner
builder.useNamespace(
    "custom-namespace",
    "https://unpkg.com/@tripetto/runner-autoscroll/builder",
    "url"
);
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/b66a5c15bfb3ca279daf097d3f25086b) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/b66a5c15bfb3ca279daf097d3f25086b)

### Loading blocks using UMD code
The best way to dynamically load block bundles is using the URL of a bundle. But when this is not possible, you can handle the actual loading of the code of the bundle yourself and then supply this code as a string to the loader. To do that, you need to specify that you want to load UMD code instead of an URL.

```ts showLineNumbers
import { Builder } from "@tripetto/builder";

const builder = new Builder({
    namespace: {
        identifier: "custom-namespace",
        //highlight-start
        umd: "/* UMD code here */"
        //highlight-end
    }
});

// or

builder.useNamespace(
    "custom-namespace",
    //highlight-start
    "/* UMD code here */",
    "umd"
    //highlight-end
);
```

:::warning
If your application implements a [Content Security Policy (CSP)](https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP) and prohibits the use of `eval()`, you should always load namespace block bundles using the `url` property and never using direct UMD code loading.
:::

## 🎚️ Excluding or including blocks {#include-exclude}
It is possible to globally exclude certain blocks from the builder or only include specific blocks. That is useful when you are loading a [block bundle](#import-block-bundles), but want to exclude one or more blocks from the bundle or only use a selected set of blocks of that bundle. To do this, you use the [`exclude`](../../api/modules/Namespaces.mdx#exclude) or [`include`](../../api/modules/Namespaces.mdx#include) functions of the [`Namespaces`](../../api/modules/Namespaces.mdx) module.

### Excluding blocks
You can specify the blocks you wish to omit from the builder using the [`exclude`](../../api/modules/Namespaces.mdx#exclude) function. Those blocks are not loaded and not available in the builder.

```ts showLineNumbers
import { Namespaces } from "@tripetto/builder";

// Let's exclude the mailer block and the file upload block
Namespaces.exclude("@tripetto/block-mailer", "@tripetto/block-file-upload");
```

### Including blocks
When you specify blocks to include, only those blocks are useable in the builder. So you can use the [`include`](../../api/modules/Namespaces.mdx#include) function to specify the blocks available in the builder. The builder will ignore all other blocks.

```ts showLineNumbers
import { Namespaces } from "@tripetto/builder";

// Only allow the text input blocks in the builder
Namespaces.include("@tripetto/block-text", "@tripetto/block-textarea");
```

## ⚙️ Using multiple namespaces {#namespaces}
It is possible to load different block bundles in separate namespaces. This makes it easy to switch the builder to another blocks namespace.

```ts showLineNumbers
const builder = new Builder();

// Load the builder blocks for the autoscroll runner
builder.useNamespace(
    "autoscroll",
    "https://unpkg.com/@tripetto/runner-autoscroll/builder",
    "url"
);

// Load the builder blocks for the chat runner
builder.useNamespace(
    "chat",
    "https://unpkg.com/@tripetto/runner-chat/builder",
    "url"
);

// Load the builder blocks for the classic runner
builder.useNamespace(
    "classic",
    "https://unpkg.com/@tripetto/runner-classic/builder",
    "url"
);

// Now switch the builder to the chat runner namespace
builder.useNamespace("chat");
```

:::tip
You can always switch between blocks namespaces. The current loaded form definition will be validated against the available blocks in the selected namespace. If a block is not available, that node is marked and made read-only.
:::
