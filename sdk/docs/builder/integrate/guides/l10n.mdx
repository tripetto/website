---
title: Localization - Guides - Builder
sidebar_label: Localization
sidebar_position: 5
description: The localization process for the builder involves loading the appropriate locale and translation based on the user's language.
---

# Localization
The localization process for the builder involves loading the appropriate locale and translation based on the user's language.

## 🌐 Builder locale {#locale}
The locale instructs the builder which locale data to use. This locale data contains the number and date/time formats. The builder package comes with all the locales included. Locale information is stored in a JSON file per locale. The locale JSON files are in the [`locales`](https://unpkg.com/browse/tripetto/locales/) folder of the package. You need to load the appropriate locale JSON and feed it to the [`locale`](../../api/interfaces/IBuilderProperties.mdx#locale) property. This property is part of the [`IBuilderProperties`](../../api/interfaces/IBuilderProperties.mdx) interface used when [constructing](../../api/classes/Builder.mdx#constructor) a new [builder instance](../../api/classes/Builder.mdx). If omitted, the default locale [`en`](https://unpkg.com/browse/tripetto/locales/en.json) will be used.

### Using a static locale (compile-time)
If you use the builder in a predefined single language, you could use a static import of the locale data for that language and supply it to the builder:
```ts showLineNumbers
import { Builder } from "@tripetto/builder";

// Import the dutch locale data
import locale from "@tripetto/builder/locales/nl.json";

const builder = new Builder({
  locale
});
```
:::warning
Make sure to enable the [`--resolveJsonModule`](https://www.typescriptlang.org/tsconfig#resolveJsonModule) compiler flag in your TypeScript configuration to allow importing JSON files.
:::

### Using a dynamic locale (run-time)
If you want to load the locale dynamically (which is the preferred approach), you have multiple options to arrange this depending on the possibilities within your application. If your server or application back-end is locale-aware (e.g. knows the user language), you could load the locale data at server-side and supply it to the builder. Another approach is to make all the JSON locale files available on your application domain and then use an AJAX call to load the locale data, as shown in the example below:
```ts showLineNumbers
import { Builder } from "@tripetto/builder";

const language = (navigator.languages && navigator.languages[0]) || navigator.language || navigator.browserLanguage || "en";
const response = await fetch(`/locales/${language}.json`);
const locale = await response.json();

const builder = new Builder({
  locale
});
```
:::warning
To keep things simple, the example above does not contain any error handling. If you are implementing this example code, make sure to check the server response and handle errors appropriately.
:::

:::tip
If you use webpack and want to copy all the locale files to a static distribution folder of your application, you could use [`copy-webpack-plugin`](https://www.npmjs.com/package/copy-webpack-plugin) to copy the files as part of the build process:

```js showLineNumbers title="webpack.config.js"
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
  plugins: [
    new CopyPlugin({
      patterns: [
        { from: "node_modules/@tripetto/builder/locales/", to: "build/static/locales/" },
      ],
    }),
  ],
};
```
:::

## 🔤 Builder translations {#translations}
The builder comes bundled with a set of translations (see a list [here](https://gitlab.com/tripetto/translations/-/blob/master/README.md#components-languages-and-translation-progress)). The translation files are in the [`translations`](https://unpkg.com/browse/tripetto/translations/) folder of the package. You need to load the appropriate translation JSON and feed it to the [`translations`](../../api/interfaces/IBuilderProperties.mdx#translations) property. This property is part of the [`IBuilderProperties`](../../api/interfaces/IBuilderProperties.mdx) interface used when [constructing](../../api/classes/Builder.mdx#constructor) a new [builder instance](../../api/classes/Builder.mdx). If omitted, the default language is English.

:::info
The [translations](../../api/interfaces/IBuilderProperties.mdx#translations) property not only accepts a builder translation, it can also be used to supply translations for the [blocks](#blocks) used by the builder.
:::

### Using a fixed translation (compile-time) {#static-import}
If you use the builder in a predefined single language, you could use a static import of the translation for that language and supply it to the builder:
```ts showLineNumbers
import { Builder } from "@tripetto/builder";

//highlight-start
// Import the dutch translation
import dutchTranslation from "@tripetto/builder/translations/nl.json";
//highlight-end

const builder = new Builder({
  //highlight-start
  translations: dutchTranslation
  //highlight-end
});
```
:::warning
Make sure to enable the [`--resolveJsonModule`](https://www.typescriptlang.org/tsconfig#resolveJsonModule) compiler flag in your TypeScript configuration to allow importing JSON files.
:::

### Using a dynamic translation (run-time) {#dynamic}
If you want to load the translation dynamically (which is the preferred approach), you have multiple options to arrange this depending on the possibilities within your application. If your server or application back-end is locale-aware (e.g., knows the user language), you could load the desired translation at server-side and supply it to the builder. Another approach is to make all the translations files available on your application domain and then use an AJAX call to load the translation, as shown in the example below:
```ts showLineNumbers
import { Builder } from "@tripetto/builder";

//highlight-start
const language = (navigator.languages && navigator.languages[0]) || navigator.language || navigator.browserLanguage || "en";
const response = await fetch(`/translations/${language}.json`);
const translation = await response.json();
//highlight-end

const builder = new Builder({
  //highlight-start
  translations: translation
  //highlight-end
});
```
:::warning
To keep things simple, the example above does not contain any error handling. If you are implementing this example code, make sure to check the server response and handle errors appropriately.
:::

:::tip
If you use webpack and want to copy all the translation files to a static distribution folder of your application, you could use [`copy-webpack-plugin`](https://www.npmjs.com/package/copy-webpack-plugin) to copy the files as part of the build process:

```js showLineNumbers title="webpack.config.js"
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
  plugins: [
    new CopyPlugin({
      patterns: [
        { from: "node_modules/@tripetto/builder/translations/", to: "build/static/translations/" },
      ],
    }),
  ],
};
```
:::

### Loading multiple translations
You can supply multiple translations to the builder at once. That allows, for example, to load the translations of the blocks (or builder block bundles).
```ts showLineNumbers
// Import the dutch translation
import { Builder } from "@tripetto/builder";

//highlight-start
import builderTranslations from "@tripetto/builder/translations/nl.json";
import blockTranslationsBundle from "@tripetto/runner-autoscroll/builder/translations/nl.json";
import blockTranslations from "@tripetto/block-checkbox/translations/nl.json";
//highlight-end

const builder = new Builder({
  //highlight-start
  translations: [
    builderTranslations,
    blockTranslationsBundle,
    blockTranslations
  ]
  //highlight-end
});
```

### Contribute
If you want to help translate the Tripetto builder to other languages, you're very welcome to do so! Go to our [translations repository](https://gitlab.com/tripetto/translations) for instructions. Download the builder [POT file](https://unpkg.com/@tripetto/builder/translations/template.pot) and start translating it. We use [POEdit](https://poedit.net/) for this job, but you can use any other tool or service you like. Please send us the translated PO file, and we are more than happy to include it.

## 📦 Block translations {#blocks}
Besides the translation for the builder, you also need to load translations for the blocks you are using.

:::tip
The actual process of loading block translation files is similar to that of the builder translation. You can use [static imports](#static-import) or [dynamic loading](#dynamic).
:::

### Loading a translation for a stock block
If you use one of the stock blocks (blocks built and maintained by the Tripetto team), you will find the translations for each block in the `translations` folder of the block packages. Just like with the builder translation, you need to load the appropriate translation JSON and feed it to the [`translations`](../../api/interfaces/IBuilderProperties.mdx#translations) property. This property can be used to supply a set of translations, as shown in the following example:
```ts showLineNumbers
import { Builder } from "@tripetto/builder";

// Import the dutch translation for the builder and some blocks
import builderTranslation from "@tripetto/builder/translations/nl.json";
//highlight-start
import checkboxTranslation from "@tripetto/block-checkbox/translations/nl.json";
import dropdownTranslation from "@tripetto/block-dropdown/translations/nl.json";
//highlight-end

const builder = new Builder({
  translations: [
    builderTranslation,
    //highlight-start
    checkboxTranslation,
    dropdownTranslation
    //highlight-end
  ]
});
```
:::warning
Make sure to enable the [`--resolveJsonModule`](https://www.typescriptlang.org/tsconfig#resolveJsonModule) compiler flag in your TypeScript configuration to allow importing JSON files.
:::

### Loading translation bundles from a stock runner
If you use a Tripetto [stock runner](../../../runner/stock/introduction.md) you can use the builder translation bundles included in the stock runner packages. A translation bundle is a single translation file that contains the translations for all the blocks used in the runner. So if you use a stock runner and want to implement a builder, it is sufficient to load the builder translation and the builder bundle from the runner. This saves the hassle of separately loading the translations for all the blocks.

The translation bundles are stored in the `builder/translations` folder of the stock runner packages. Just like with the builder translation, you need to load the appropriate translation JSON bundle and feed it to the [`translations`](../../api/interfaces/IBuilderProperties.mdx#translations) property. This property can be used to supply a set of translations, as shown in the following example:
```ts showLineNumbers
import { Builder } from "@tripetto/builder";

// Import the dutch builder translation
import builderTranslation from "@tripetto/builder/translations/nl.json";

//highlight-start
// Import the bundle with block translations for the blocks used in the autoscroll runner
import bundle from "@tripetto/runner-autoscroll/builder/translations/nl.json";
//highlight-end

const builder = new Builder({
  translations: [
    builderTranslation,
    //highlight-start
    bundle
    //highlight-end
  ]
});
```
:::warning
Make sure to enable the [`--resolveJsonModule`](https://www.typescriptlang.org/tsconfig#resolveJsonModule) compiler flag in your TypeScript configuration to allow importing JSON files.
:::
