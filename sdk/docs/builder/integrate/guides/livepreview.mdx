---
title: Live form preview - Guides - Builder
sidebar_label: Live form preview
sidebar_position: 6
description: For an optimal user experience, it is recommended to show a live preview of the form along with the builder.
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Live form preview
For an optimal user experience, it is recommended to show a live preview of the form along with the builder. To do so, you could display a runner in a side-by-side view with the builder (or use a popup or overlay to display the runner). Besides showing the live preview and updating the form when a change is made in the builder, you can also enable interactions between the two. That allows to bring the element that's being edited into view in the live preview. It also makes it possible to open the properties of an element in the builder by clicking on it in the live preview. These two features can greatly improve the user experience.

## Live preview using stock runners
The [stock runners](../../../runner/stock/introduction.md) (runners built and maintained by the Tripetto team) already contain all the required functions to get an optimal live preview experience. The only thing you need to do is feed the builder controller to the runners. The code below shows how to set up this up.

<Tabs groupId="framework">
<TabItem value="plainjs" label="Plain JS">

```ts showLineNumbers
import { Builder } from "@tripetto/builder";
import { run } from "@tripetto/runner-autoscroll";

// Create a new builder instance
Builder.open(undefined, {
  // This example assumes there is a HTML element with id `Builder` for the builder
  element: document.getElementById("Builder"),

  // When the builder is ready, create the runner for the live preview
  onReady: (builder) => run({
      // This example assumes there is a HTML element with id `Runner` for the runner live preview
      element: document.getElementById("Runner"),
      definition: builder.definition,
      view: "preview",
      //highlight-start
      // Feed the builder instance to the runner to enable live preview
      builder,
      //highlight-end
    }),
});
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/239faa4c7d858da7d30fc67c0b4a4bd2) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/239faa4c7d858da7d30fc67c0b4a4bd2)

</TabItem>
<TabItem value="react" label="React">

```tsx showLineNumbers title="app.tsx"
import { useRef } from "react";
import { TripettoBuilder } from "@tripetto/builder/react";
import { AutoscrollRunner } from "@tripetto/runner-autoscroll";

function App() {
  //highlight-start
  const controllerRef = useRef();
  //highlight-end

  return (
    <>
      //highlight-start
      <TripettoBuilder controller={controllerRef} />
      <AutoscrollRunner builder={controllerRef} view="preview" />
      //highlight-end
    </>
  );
}
```
[![Run](/img/button-run.svg)](https://gvrpng.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-react-live-preview-gvrpng?file=/src/index.js)

</TabItem>
<TabItem value="angular" label="Angular">

```ts showLineNumbers title="app.component.ts"
import { Component, ViewChild, AfterViewInit } from "@angular/core";
//highlight-start
import { TripettoBuilderComponent } from "@tripetto/builder/angular";
import { TripettoAutoscrollComponent } from "@tripetto/runner-autoscroll/angular";
//highlight-end

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements AfterViewInit {
  //highlight-start
  @ViewChild(TripettoBuilderComponent) builder!: TripettoBuilderComponent;
  @ViewChild(TripettoAutoscrollComponent) runner!: TripettoAutoscrollComponent;

  ngAfterViewInit() {
    // Pass the builder instance to the runner
    this.runner.builder = this.builder;
  }
  //highlight-end
}
```
[![Run](/img/button-run.svg)](https://78soc.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-angular-live-preview-78soc?file=/src/app/app.component.html)

</TabItem>
</Tabs>

## Preview vs. test mode
The stock runners have two view modes related to the live preview functionality:
- **Preview mode**: Shows all elements in the form by skipping all logic;
- **Test mode**: Runs the form like a real one without being able to submit data.

The preview mode is ideal for editing since it can show each element in the form as the logic is not applied. In the test mode, the forms run like a real form. Both modes are useful when creating forms. The preview mode allows viewing each element in the form without filling in the form. The test mode is ideal for actual testing the form and all the logic in it.

If you implement a live preview in your application, we suggest adding a toggle to allow the user to switch between preview and test mode.

<Tabs groupId="framework">
<TabItem value="plainjs" label="Plain JS">

```ts showLineNumbers
import { Builder } from "@tripetto/builder";
import { run } from "@tripetto/runner-autoscroll";

// Create a new builder instance
Builder.open(undefined, {
  // This example assumes there is a HTML element with id `builder` for the builder
  element: document.getElementById("builder"),

  // When the builder is ready, create the runner for the live preview
  onReady: async (builder) => {
    const runner = await run({
      // This example assumes there is a HTML element with id `runner` for the runner live preview
      element: document.getElementById("runner"),
      definition: builder.definition,
      view: "preview",
      builder,
    });

    //highlight-start
    // This example assumes there are two toggle buttons
    const togglePreview = document.getElementById("toggle-preview");
    const toggleTest = document.getElementById("toggle-test");

    togglePreview.addEventListener("click", () => {
      runner.view = "preview";

      togglePreview.classList.add("selected");
      toggleTest.classList.remove("selected");
    });

    toggleTest.addEventListener("click", () => {
      runner.view = "test";

      toggleTest.classList.add("selected");
      togglePreview.classList.remove("selected");
    });
    //highlight-end
  }
});

```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/d609b174bc40c404b2d4ad8f6b1dbc0d) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/d609b174bc40c404b2d4ad8f6b1dbc0d)

</TabItem>
<TabItem value="react" label="React">

```tsx showLineNumbers title="app.tsx"
import { useState, useRef } from "react";
import { TripettoBuilder } from "@tripetto/builder/react";
import { AutoscrollRunner } from "@tripetto/runner-autoscroll";

function App() {
  //highlight-start
  const [view, setView] = useState("preview");
  //highlight-end
  const controllerRef = useRef();

  return (
    <>
      <TripettoBuilder controller={controllerRef} />
      //highlight-start
      <AutoscrollRunner view={view} builder={controllerRef} />
      <div onClick={() => setView("preview")}>Preview mode</div>
      <div onClick={() => setView("test")}>Test mode</div>
      //highlight-end
    </>
  );
}
```
[![Run](/img/button-run.svg)](https://z2wjix.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-react-live-preview-with-mode-toggle-z2wjix?file=/src/index.js)

</TabItem>
<TabItem value="angular" label="Angular">

```ts showLineNumbers title="app.component.ts"
import { Component, ViewChild, AfterViewInit } from "@angular/core";
import { IDefinition } from "@tripetto/builder";
import { TripettoBuilderComponent } from "@tripetto/builder/angular";
import { TripettoAutoscrollComponent } from "@tripetto/runner-autoscroll/angular";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements AfterViewInit {
  @ViewChild(TripettoBuilderComponent) builder!: TripettoBuilderComponent;
  @ViewChild(TripettoAutoscrollComponent) runner!: TripettoAutoscrollComponent;

  //highlight-start
  get isPreview(): boolean {
    return (this.runner?.view || "preview") === "preview";
  }
  //highlight-end

  ngAfterViewInit() {
    this.ref.builder = this.builder;
  }

  //highlight-start
  onPreview() {
    this.runner.view = "preview";
  }

  onTest() {
    this.runner.view = "test";
  }
  //highlight-end
}
```
[![Run](/img/button-run.svg)](https://0hvxh.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-angular-live-preview-with-mode-toggle-0hvxh?file=/src/app/app.component.ts)

</TabItem>
</Tabs>

## Preview runner styles {#styles}
The stock runners support customizing the styles of the form. Things like the font face, text size, and colors can be changed. The builder contains a special editor panel for managing the styles of a runner. To let this work, each stock runner contains a specific file that contains all the available styles for the runner, called the styles contract. The builder is able to use this styles contract and populate an editor panel with it that allows the user to configure the styles. If you combine this with a live preview, the user can change the style settings and immediately see the result in the live preview panel.

### Importing the styles contract
The styles contract is a function that can be imported from the stock runner packages. It is located in the `/builder/styles` folder.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll runner">

```ts showLineNumbers
import stylesContract from "@tripetto/runner-autoscroll/builder/styles";
```

</TabItem>
<TabItem value="chat" label="Chat runner">

```ts showLineNumbers
import stylesContract from "@tripetto/runner-chat/builder/styles";
```

</TabItem>
<TabItem value="classic" label="Classic runner">

```ts showLineNumbers
import stylesContract from "@tripetto/runner-classic/builder/styles";
```

</TabItem>
</Tabs>

### Generating the styles editor panel
To generate the styles editor panel the [`stylesEditor`](../../api/classes/Builder.mdx#stylesEditor) method of a builder instance is used. The styles contract is generated using the `stylesContract` function imported from the runner package as shown above.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll runner">

```ts showLineNumbers
import stylesContract from "@tripetto/runner-autoscroll/builder/styles";

// Open styles editor using an existing builder instance
builder.stylesEditor(stylesContract);
```
[![Run](/img/button-run.svg)](https://5ts0k.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-styles-editor-autoscroll-runner-5ts0k?file=/src/index.ts)

</TabItem>
<TabItem value="chat" label="Chat runner">

```ts showLineNumbers
import stylesContract from "@tripetto/runner-chat/builder/styles";

// Open styles editor using an existing builder instance
builder.stylesEditor(stylesContract);
```
[![Run](/img/button-run.svg)](https://bs2jr.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-styles-editor-chat-runner-bs2jr?file=/src/index.ts)

</TabItem>
<TabItem value="classic" label="Classic runner">

```ts showLineNumbers
import stylesContract from "@tripetto/runner-classic/builder/styles";

// Open styles editor using an existing builder instance
builder.stylesEditor(stylesContract);
```
[![Run](/img/button-run.svg)](https://ij6l1.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-styles-editor-classic-runner-ij6l1?file=/src/index.ts)

</TabItem>
</Tabs>

### Update styles in live preview
The [`stylesEditor`](../../api/classes/Builder.mdx#stylesEditor) has a special argument that allows specifying a callback function that is invoked when the styles change. This callback function can update the live preview. The following example adds a button to open the styles editor panel. When styles are changed, the live preview is updated accordingly.

<Tabs groupId="framework">
<TabItem value="plainjs" label="Plain JS">

```ts showLineNumbers
import { Builder } from "@tripetto/builder";
import { run } from "@tripetto/runner-autoscroll";
//highlight-start
import stylesContract from "@tripetto/runner-autoscroll/builder/styles";
//highlight-end

// Create a new builder instance
Builder.open(undefined, {
  // This example assumes there is a HTML element with id `builder` for the builder
  element: document.getElementById("builder"),

  // When the builder is ready, create the runner for the live preview
  onReady: async (builder) => {
    const runner = await run({
      // This example assumes there is a HTML element with id `runner` for the runner live preview
      element: document.getElementById("runner"),
      definition: builder.definition,
      view: "preview",
      builder,
    });

    // This example assumes there are two toggle buttons
    const togglePreview = document.getElementById("toggle-preview");
    const toggleTest = document.getElementById("toggle-test");

    togglePreview.addEventListener("click", () => {
      runner.view = "preview";
    });

    toggleTest.addEventListener("click", () => {
      runner.view = "test";
    });

    //highlight-start
    // This example assumes there is an edit button for the styles
    const editButton = document.getElementById("edit-styles");

    editButton.addEventListener("click", () => {
      // Open the styles editor
      builder.stylesEditor(
        stylesContract,
        () => runner,
        "unlicensed"
      );
    });
    //highlight-end
  }
});

```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/7c4bdc5b46940d6ad5de8d33fcf4f81c) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/7c4bdc5b46940d6ad5de8d33fcf4f81c)

</TabItem>
<TabItem value="react" label="React">

```tsx showLineNumbers title="app.tsx"
import { useState, useRef } from "react";
import { TripettoBuilder } from "@tripetto/builder/react";
import { AutoscrollRunner } from "@tripetto/runner-autoscroll";
//highlight-start
import stylesContract from "@tripetto/runner-autoscroll/builder/styles";
//highlight-end

function App() {
  const [view, setView] = useState("preview");
  const builderControllerRef = useRef();
  //highlight-start
  const runnerControllerRef = useRef();
  //highlight-end

  return (
    <>
      <TripettoBuilder controller={builderControllerRef} />
      //highlight-start
      <AutoscrollRunner
        controller={runnerControllerRef}
        view={view}
        builder={builderControllerRef}
      />
      //highlight-end
      //highlight-start
      <div onClick={() => {
          // Open the styles editor
          builderControllerRef.current.stylesEditor(
            stylesContract,
            () => runnerControllerRef.current,
            "standard"
          );
        }}
      >Edit styles</div>
      //highlight-end
    </>
  );
}
```
[![Run](/img/button-run.svg)](https://oe1duh.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-react-live-preview-with-styles-editor-oe1duh?file=/src/index.js)

</TabItem>
<TabItem value="angular" label="Angular">

```ts showLineNumbers title="app.component.ts"
import { Component, ViewChild, AfterViewInit } from "@angular/core";
import { IDefinition } from "@tripetto/builder";
import { TripettoBuilderComponent } from "@tripetto/builder/angular";
import { TripettoAutoscrollComponent } from "@tripetto/runner-autoscroll/angular";
//highlight-start
import stylesContract from "@tripetto/runner-autoscroll/builder/styles";
//highlight-end

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements AfterViewInit {
  @ViewChild(TripettoBuilderComponent) builder!: TripettoBuilderComponent;
  @ViewChild(TripettoAutoscrollComponent) runner!: TripettoAutoscrollComponent;

  ngAfterViewInit() {
    this.runner.builder = this.builder;
  }

  //highlight-start
  onEditStyles() {
    // Open the styles editor
    this.builder.stylesEditor(
      stylesContract,
      () => this.runner,
      "unlicensed"
    );
  }
  //highlight-end
}
```
[![Run](/img/button-run.svg)](https://x1lih.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-angular-live-preview-with-styles-editor-x1lih?file=/src/app/app.component.ts)

</TabItem>
</Tabs>

## Preview runner translations {#translations}
The stock runners support translations for different languages. The builder has a special translation editor panel that allows users to create custom translations for the stock runners. To let this work, each stock runner contains a specific file that contains translation information, called the localization (l10n) contract. The builder is able to use this l10n contract and populate an editor panel with it that allows the user to translate the text labels. If you combine this with a live preview, the user can translate the runner and immediately see the result in the live preview panel.

:::info
Currently, this panel only allows translating the static text labels of the runner and not the text labels within the form definition.
:::

### Importing the l10n contract
The l10n contract is a function that can be imported from the stock runner packages. It is located in the `/builder/l10n` folder.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll runner">

```ts
import l10nContract from "@tripetto/runner-autoscroll/builder/l10n";
```

</TabItem>
<TabItem value="chat" label="Chat runner">

```ts
import l10nContract from "@tripetto/runner-chat/builder/l10n";
```

</TabItem>
<TabItem value="classic" label="Classic runner">

```ts
import l10nContract from "@tripetto/runner-classic/builder/l10n";
```

</TabItem>
</Tabs>

### Generating the translations editor panel
To generate the translations editor panel the [`l10nEditor`](../../api/classes/Builder.mdx#l10nEditor) method of a builder instance is used. The l10n contract is generated using the `l10nContract` function imported from the runner package as shown above.

<Tabs groupId="runner">
<TabItem value="autoscroll" label="Autoscroll runner">

```ts showLineNumbers
import l10nContract from "@tripetto/runner-autoscroll/builder/l10n";

// Open translations editor using an existing builder instance
builder.l10nEditor(L10nContract);
```
[![Run](/img/button-run.svg)](https://1zwwc.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-translations-editor-autoscroll-runner-1zwwc?file=/src/index.ts)

</TabItem>
<TabItem value="chat" label="Chat runner">

```ts showLineNumbers
import l10nContract from "@tripetto/runner-chat/builder/l10n";

// Open translations editor using an existing builder instance
builder.l10nEditor(L10nContract);
```
[![Run](/img/button-run.svg)](https://702cc.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-translations-editor-chat-runner-702cc?file=/src/index.ts)

</TabItem>
<TabItem value="classic" label="Classic runner">

```ts showLineNumbers
import l10nContract from "@tripetto/runner-classic/builder/l10n";

// Open translations editor using an existing builder instance
builder.l10nEditor(L10nContract);
```
[![Run](/img/button-run.svg)](https://vx7rb.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-translations-editor-classic-runner-vx7rb?file=/src/index.ts)

</TabItem>
</Tabs>

### Update translations in live preview
The [`l10nEditor`](../../api/classes/Builder.mdx#l10nEditor) has a special argument that allows specifying a callback function that is invoked when the translations change. This callback function can update the live preview. The following example adds a button to open the l10n editor panel. When translations are changed, the live preview is updated accordingly.

<Tabs groupId="framework">
<TabItem value="plainjs" label="Plain JS">

```ts showLineNumbers
import { Builder } from "@tripetto/builder";
import { run } from "@tripetto/runner-autoscroll";
//highlight-start
import l10nContract from "@tripetto/runner-autoscroll/builder/l10n";
//highlight-end

// Create a new builder instance
Builder.open(undefined, {
  // This example assumes there is a HTML element with id `builder` for the builder
  element: document.getElementById("builder"),

  // When the builder is ready, create the runner for the live preview
  onReady: async (builder) => {
    const runner = await run({
      // This example assumes there is a HTML element with id `runner` for the runner live preview
      element: document.getElementById("runner"),
      definition: builder.definition,
      view: "preview",
      builder
    });

    // This example assumes there are two toggle buttons
    const togglePreview = document.getElementById("toggle-preview");
    const toggleTest = document.getElementById("toggle-test");

    togglePreview.addEventListener("click", () => {
      runner.view = "preview";
    });

    toggleTest.addEventListener("click", () => {
      runner.view = "test";
    });

    //highlight-start
    // This example assumes there is an edit button for the translations
    const editButton = document.getElementById("edit-translations");

    editButton.addEventListener("click", () => {
      // Open the translations editor
      builder.l10nEditor(
        l10nContract,
        () => runner
      );
    });
    //highlight-end
  }
});

```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/0596f886d701bd292f9f068845832ce9) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/0596f886d701bd292f9f068845832ce9)

</TabItem>
<TabItem value="react" label="React">

```tsx showLineNumbers title="app.tsx"
import { useState, useRef } from "react";
import { TripettoBuilder } from "@tripetto/builder/react";
import { AutoscrollRunner } from "@tripetto/runner-autoscroll";
//highlight-start
import l10nContract from "@tripetto/runner-autoscroll/builder/l10n";
//highlight-end

function App() {
  const [view, setView] = useState("preview");
  const builderControllerRef = useRef();
  //highlight-start
  const runnerControllerRef = useRef();
  //highlight-end

  return (
    <>
      <TripettoBuilder controller={builderControllerRef} />
      //highlight-start
      <AutoscrollRunner
        controller={runnerControllerRef}
        view={view}
        builder={builderControllerRef}
      />
      //highlight-end
      //highlight-start
      <div onClick={() => {
          // Open the translations editor
          builderControllerRef.current.l10nEditor(
            l10nContract,
            () => runnerControllerRef.current
          );
        }}
      >Edit translations</div>
      //highlight-end
    </>
  );
}
```
[![Run](/img/button-run.svg)](https://iiiqin.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-react-live-preview-with-translations-editor-iiiqin?file=/src/index.js)

</TabItem>
<TabItem value="angular" label="Angular">

```ts showLineNumbers title="app.component.ts"
import { Component, ViewChild, AfterViewInit } from "@angular/core";
import { IDefinition } from "@tripetto/builder";
import { TripettoBuilderComponent } from "@tripetto/builder/angular";
import { TripettoAutoscrollComponent } from "@tripetto/runner-autoscroll/angular";
//highlight-start
import l10nContract from "@tripetto/runner-autoscroll/builder/l10n";
//highlight-end

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements AfterViewInit {
  @ViewChild(TripettoBuilderComponent) builder!: TripettoBuilderComponent;
  @ViewChild(TripettoAutoscrollComponent) runner!: TripettoAutoscrollComponent;

  ngAfterViewInit() {
    this.runner.builder = this.builder;
  }

  //highlight-start
  onEditTranslations() {
    // Open the trsnslations editor
    this.builder.l10nEditor(
      l10nContract,
      () => this.runner
    );
  }
  //highlight-end
}
```
[![Run](/img/button-run.svg)](https://c76c0.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-angular-live-preview-with-translations-editor-c76c0?file=/src/app/app.component.ts)

</TabItem>
</Tabs>

## Preview multiple runners
It is possible to use multiple runners so that the user can preview the form in different runners with a simple switch. To do so, it is necessary to specify different namespaces for each runner. The builder blocks of those runners need to be [loaded](blocks.mdx) in those namespaces. When the user switches to another runner, the appropriate namespace is selected using the [`useNamespace`](../../api/classes/Builder.mdx#useNamespace) method of the builder instance.

The following example implements a dropdown control with a list of runners. The user can switch to another runner by selecting one in the dropdown. The live preview then changes to the selected runner.

:::info
In this example the runners and block bundles are loaded using a static import. See the [Loading blocks guide](blocks.mdx) if you want to load bundles dynamically (lazy load).
:::

<Tabs groupId="framework">
<TabItem value="plainjs" label="Plain JS">

```ts showLineNumbers
import { Builder, IBuilderChangeEvent, IBuilderEditEvent, mountNamespace, unmountNamespace } from "@tripetto/builder";
//highlight-start
import { run as runAutoscroll } from "@tripetto/runner-autoscroll";
import { run as runChat } from "@tripetto/runner-chat";
import { run as runClassic } from "@tripetto/runner-classic";

// This demo implements multiple runners than run simultaneously. Each runner
// comes with a bundle with the implemented builder blocks for that runner.
// Here we load those bundles in different namespaces. By activating the
// appropriate namespace during runtime in the builder we can switch between
// builder block bundles. This allows for differences between the blocks used
// in the runners.
mountNamespace("autoscroll");
import "@tripetto/runner-autoscroll/builder";
unmountNamespace();

mountNamespace("chat");
import "@tripetto/runner-chat/builder";
unmountNamespace();

mountNamespace("classic");
import "@tripetto/runner-classic/builder";
unmountNamespace();
//highlight-end

// Create a new builder instance
Builder.open(undefined, {
  element: document.getElementById("builder"),
  onReady: async (builder) => {
    //highlight-start
    let activeRunner = "autoscroll";

    const updateRunner = () => {
      builder.useNamespace(activeRunner);

      document
        .getElementById("runner-autoscroll")
        ?.classList.toggle("visible", activeRunner === "autoscroll");
      document
        .getElementById("runner-chat")
        ?.classList.toggle("visible", activeRunner === "chat");
      document
        .getElementById("runner-classic")
        ?.classList.toggle("visible", activeRunner === "classic");
    };

    const autoscrollRunner = await runAutoscroll({
      element: document.getElementById("runner-autoscroll"),
      view: "preview",
      builder,
      onReady: () => updateRunner()
    });

    const chatRunner = await runChat({
      element: document.getElementById("runner-chat"),
      view: "preview",
      builder,
      onReady: () => updateRunner()
    });

    const classicRunner = await runClassic({
      element: document.getElementById("runner-classic"),
      view: "preview",
      builder,
      onReady: () => updateRunner()
    });

    document
      .getElementById("runner-switch")
      ?.addEventListener("change", (e) => {
        activeRunner = (e.target as HTMLSelectElement)?.value;

        updateRunner();
      });
    //highlight-end

    const togglePreview = document.getElementById("toggle-preview");
    const toggleTest = document.getElementById("toggle-test");

    togglePreview?.addEventListener("click", () => {
      //highlight-start
      autoscrollRunner.view = "preview";
      chatRunner.view = "preview";
      classicRunner.view = "preview";
      //highlight-end

      togglePreview.classList.add("selected");
      toggleTest?.classList.remove("selected");
    });

    toggleTest?.addEventListener("click", () => {
      //highlight-start
      autoscrollRunner.view = "test";
      chatRunner.view = "test";
      classicRunner.view = "test";
      //highlight-end

      toggleTest.classList.add("selected");
      togglePreview?.classList.remove("selected");
    });
  },
  disableSaveButton: true,
  disableCloseButton: true,
  controls: "left"
});
```
[![Run](/img/button-run.svg)](https://jdmt3.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-plain-js-live-preview-with-multiple-runners-jdmt3?file=/src/index.ts)

</TabItem>
<TabItem value="react" label="React">

```tsx showLineNumbers title="app.tsx"
import { useRef, useState, useEffect } from "react";
//highlight-start
import { Builder, mountNamespace, unmountNamespace } from "@tripetto/builder";
import { run as autoscrollRun } from "@tripetto/runner-autoscroll";
import { run as chatRun } from "@tripetto/runner-chat";
import { run as classicRun } from "@tripetto/runner-classic";

// This demo implements multiple runners than run simultaneously. Each runner
// comes with a bundle with the implemented builder blocks for that runner.
// Here we load those bundles in different namespaces. By activating the
// appropriate namespace during runtime in the builder we can switch between
// builder block bundles. This allows for differences between the blocks used
// in the runners.
mountNamespace("autoscroll");
import "@tripetto/runner-autoscroll/builder";
unmountNamespace();

mountNamespace("chat");
import "@tripetto/runner-chat/builder";
unmountNamespace();

mountNamespace("classic");
import "@tripetto/runner-classic/builder";
unmountNamespace();
//highlight-end

function App() {
  //highlight-start
  const [view, setView] = useState<"preview" | "test">("preview");
  const [runner, setRunner] = useState("autoscroll");
  const builderRef = useRef<Builder>();

  if (builderRef.current) {
    builderRef.current.useNamespace(runner);
  }

  return (
    <>
      <TripettoBuilder controller={builderRef} />
      {runner === "autoscroll" && <AutoscrollRunner view={view} builder={builderRef} />}
      {runner === "chat" && <ChatRunner view={view} builder={builderRef} />}
      {runner === "classic" && <ClassicRunner view={view} builder={builderRef} />}
      <select className="runner-switch" onChange={(e) => setRunner(e.target.value)}>
        <option value="autoscroll">Autoscroll</option>
        <option value="chat">Chat</option>
        <option value="classic">Classic</option>
      </select>
      <div
        className={"toggle-preview" + (view === "preview" ? " selected" : "")}
        onClick={() => setView("preview")}
      >
        Preview mode
      </div>
      <div
        className={"toggle-test" + (view === "test" ? " selected" : "")}
        onClick={() => setView("test")}
      >
        Test mode
      </div>
    </>
  );
  //highlight-end
}
```
[![Run](/img/button-run.svg)](https://c771g.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-react-live-preview-with-multiple-runners-c771g?file=/src/builder.js)

</TabItem>
<TabItem value="angular" label="Angular">

```ts showLineNumbers title="app.component.ts"
import { Component, ViewChild, AfterViewInit } from "@angular/core";
//highlight-start
import { IDefinition, mountNamespace, unmountNamespace } from "@tripetto/builder";
import { TripettoBuilderComponent } from "@tripetto/builder/angular";
import { TripettoAutoscrollComponent } from "@tripetto/runner-autoscroll/angular";
import { TripettoChatComponent } from "@tripetto/runner-chat/angular";
import { TripettoClassicComponent } from "@tripetto/runner-classic/angular";

// This demo implements multiple runners than run simultaneously. Each runner
// comes with a bundle with the implemented builder blocks for that runner.
// Here we load those bundles in different namespaces. By activating the
// appropriate namespace during runtime in the builder we can switch between
// builder block bundles. This allows for differences between the blocks used
// in the runners.
mountNamespace("autoscroll");
import "@tripetto/runner-autoscroll/builder";
unmountNamespace();

mountNamespace("chat");
import "@tripetto/runner-chat/builder";
unmountNamespace();

mountNamespace("classic");
import "@tripetto/runner-classic/builder";
unmountNamespace();
//highlight-end

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements AfterViewInit {
  //highlight-start
  @ViewChild(TripettoBuilderComponent) builder!: TripettoBuilderComponent;
  @ViewChild("autoscroll")
  autoscroll!: TripettoAutoscrollComponent;
  @ViewChild("chat") chat!: TripettoChatComponent;
  @ViewChild("classic") classic!: TripettoClassicComponent;
  activeRunner: "autoscroll" | "chat" | "classic" = "autoscroll";

  get isPreview(): boolean {
    return (this.autoscroll?.view || "preview") === "preview";
  }

  ngAfterViewInit() {
    this.autoscroll.builder = this.chat.builder = this.classic.builder = this.builder;

    this.builder.controller.useNamespace(this.activeRunner);
  }

  onRunnerChange(runner: "autoscroll" | "classic" | "chat") {
    this.builder.controller.useNamespace((this.activeRunner = runner));
  }

  onPreview() {
    this.autoscroll.view = this.chat.view = this.classic.view = "preview";
  }

  onTest() {
    this.autoscroll.view = this.chat.view = this.classic.view = "test";
  }
  //highlight-end
}
```
[![Run](/img/button-run.svg)](https://nllov.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-angular-live-preview-with-multiple-runners-nllov?file=/src/app/app.component.ts)

</TabItem>
</Tabs>

## Listening for builder edit events
If you want to listen for edit events sent by the builder, use the following code. It implements the [`hook`](../../api/classes/Builder.mdx#hook) function to attach a listener that receives an object of type [`IBuilderEditEvent`](../../api/interfaces/IBuilderEditEvent.mdx) when the event occurs.
```ts showLineNumbers
import { Builder } from "@tripetto/builder";

const builder = new Builder();

//highlight-start
builder.hook("OnEdit", "framed", (event) => {
  // `event.data` contains information about the element being edited.
  // See `IBuilderEditEvent` docs for more information.
});
//highlight-end
```

