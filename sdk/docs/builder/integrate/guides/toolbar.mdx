---
title: Toolbars - Guides - Builder
sidebar_label: Toolbars
sidebar_position: 12
description: This guide shows the different options available for the toolbars and shows how to disable them entirely if you like.
---

# Customize the toolbars
In the default configuration, the builder shows a navigation and zoombar (the latter is only visible when the form structure is larger than the current viewport). The navigation bar is primarily intended to be the default place for the Save button if there isn't room for it elsewhere in the UI (or when the builder is used as a standalone tool). If you integrate the builder into a custom application, you probably don't need it. For example, you could implement a custom save button in your application UI instead of the navigation bar (or use automatic saving on each change). This chapter shows the different options available for the toolbars and shows how to disable them entirely if you like.

## Toolbar options {#options}
This table shows the available options and the default value of each option. These options are part of the [`IBuilderProperties`](../../api/interfaces/IBuilderProperties.mdx) interface used when [constructing](../../api/classes/Builder.mdx#constructor) a new [builder instance](../../api/classes/Builder.mdx).

| Name                                                                                         | Type              | Description                                                                                                                                                                                              | Default                        |
|:---------------------------------------------------------------------------------------------|:------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:-------------------------------|
| [`disableLogo`](../../api/interfaces/IBuilderProperties.mdx#disableLogo)                     | boolean           | Disables the Tripetto logo in the navigation bar.                                                                                                                                                        | false                          |
| [`disableSaveButton`](../../api/interfaces/IBuilderProperties.mdx#disableSaveButton)         | boolean           | Disables the save button in the navigation bar.                                                                                                                                                          | false                          |
| [`disableRestoreButton`](../../api/interfaces/IBuilderProperties.mdx#disableRestoreButton)   | boolean           | Disables the restore button in the navigation bar. The restore button restores a form to the previous saved state. So this will undo any changes made to the form since the last save action was called. | true                           |
| [`disableClearButton`](../../api/interfaces/IBuilderProperties.mdx#disableClearButton)       | boolean           | Disables the clear button in the navigation bar. When the clear button is pressed, the whole form is removed and a new empty one is created.                                                             | true                           |
| [`disableEditButton`](../../api/interfaces/IBuilderProperties.mdx#disableEditButton)         | boolean           | Disables the edit button in the navigation bar. The edit button opens the form properties editor where the name, language, description and keywords of the form are managed.                             | false                          |
| [`disableCloseButton`](../../api/interfaces/IBuilderProperties.mdx#disableCloseButton)       | boolean           | Disables the close button in the navigation bar. The close button closes the builder and invokes the [`onClose`](../../api/interfaces/IBuilderProperties.mdx#onClose) event event.                       | false                          |
| [`disableTutorialButton`](../../api/interfaces/IBuilderProperties.mdx#disableTutorialButton) | boolean           | Disables the tutorial button in the navigation bar.                                                                                                                                                      | false                          |
| [`previewURL`](../../api/interfaces/IBuilderProperties.mdx#previewURL)                       | string            | Specifies the URL of a preview page. When set, a preview button will be shown in the navigation bar. When the user clicks that button a new tab/window with the supplied URL is loaded.                  |                                |
| [`supportURL`](../../api/interfaces/IBuilderProperties.mdx#supportURL)                       | string \| false   | Specifies an URL to a custom help or support page or supply `false` to disable this button.                                                                                                              | https://tripetto.com/sdk/docs/ |
| [`disableZoombar`](../../api/interfaces/IBuilderProperties.mdx#disableZoombar)               | boolean           | Disables the visual zoombar that is shown when the form exceeds the size of the builder viewport.                                                                                                        | false                          |
| [`controls`](../../api/interfaces/IBuilderProperties.mdx#controls)                           | "left" \| "right" | Specifies the location of the navigation and zoombar.                                                                                                                                                    | "right"                        |

## Hiding the navigation bar
Use the following configuration to remove the navigation bar:
```ts showLineNumbers
import { Builder } from "@tripetto/builder";

const builder = new Builder({
  disableLogo: true,
  disableSaveButton: true,
  disableEditButton: true,
  disableCloseButton: true,
  disableTutorialButton: true,
  disableOpenCloseAnimation: true,
  supportURL: false,
});
```

## Hiding the zoombar
Use the following configuration to remove the zoombar:
```ts showLineNumbers
import { Builder } from "@tripetto/builder";

const builder = new Builder({
  disableZoombar: true,
});
```

## Hiding the subforms navigation bar
When a subform is being opened, a floating navigation bar appears with the name of the subform, a button to edit the name of the form, and a button to close the subform (and return to the parent form). If you don't want to show/use that navigation bar, you can implement the [`onBreadcrumb`](../../api/interfaces/IBuilderProperties.mdx#onBreadcrumb) event of the builder. Use this event to provide an interactive breadcrumb to the user with controls to edit the name of the subform and to close the subform (so it returns to the parent form).
