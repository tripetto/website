---
title: Guides - Guides - Builder
sidebar_label: Guides
description: This chapter shows you guides to help you master the Tripetto builder.
---

# Guides
When you have the [basic implementation](../quickstart/index.md) of the builder up and running, it is time to dive deeper and enable more features of the builder. The following guides are available to help you master the Tripetto builder.

## Loading and saving
- 📂 [Loading forms](loading.mdx)
- 💾 [Saving forms](saving.mdx)
- ✅ [Validating forms](validation.mdx)
- 📦 [Loading blocks and namespaces](blocks.mdx)
- 🌍 [Loading translations and locale data](l10n.mdx)

## Advanced
- 🎭 [Live form preview](livepreview.mdx)
- 🖥️ [Form fingerprint](fingerprint.mdx)
- 🗃️ [Form data stencil](stencil.mdx)
- ↪️ [Subforms (nested forms)](../guides/subforms.mdx)
- 🗛 [Bundle builder fonts](fonts.mdx)
- 🛡️ [Content Security Policy](csp.mdx)
- 🖥️ [Handle screen resizing](resize.mdx)

## Customization
- ⚒️ [Customize toolbars](toolbar.mdx)
- 💰 [Define a builder tier](tiers.mdx)
- 💳 [Add a license](license.mdx)
- 💡 [Add help links](help.mdx)
