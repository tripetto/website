---
title: Subforms - Guides - Builder
sidebar_label: Subforms
sidebar_position: 9
description: Tripetto supports the use of subforms (also called nested forms). Simply put, these are forms within another form.
---

import Preview from '@site/src/components/preview.js';

# Using subforms
Tripetto supports the use of subforms (also called nested forms). Simply put, these are forms within another form. They greatly help to structure large forms and simplify manageability. It is also possible to load subforms from external sources. This allows, for example, to create a library of commonly used forms that can then be imported by the builder and used in other forms. In order to let that work, you need to implement some additional events to instruct the builder where to find the forms that can be imported. This guide explains how subforms in Tripetto work and how external form sources can be used.

## 👩‍🏫 How subforms work {#how}
By default, the builder supports the use of subforms (this means you need to opt-out if you want to disable this feature, using the [`disableNesting`](../../api/interfaces/IBuilderProperties.mdx#disableNesting) property of the builder configuration). The builder user creates new subforms by holding (or right-clicking) the button to create a new section. So a regular tap (or left-click) on that button creates a new section as usual. But holding the button (or right-clicking it) shows a menu with the option to create a new (empty) subform.

Subforms are always stored inside the [form definition](../../api/interfaces/IDefinition.mdx) of the main form. This is a careful design decision to minimize the impact on the complexity of loading a form definition in a runner. Since a single form definition also contains all the used subforms, there is no need for (asynchronous) loading of additional form definitions while the runner is running. That assures the form is valid, and it also stabilizes the form [fingerprint](fingerprint.mdx), which helps process the response data that comes out of the runner.

So, how does this work? Surprisingly simple! A subform is defined by a branch inside a section in the form definition. This section has a special [`type`](../../api/interfaces/ISection.mdx#type), which is `nest`. Whenever the builder "sees" a section with type `nest`, it knows that's a subform, and it will render that (nested) branch with everything in it into a separate builder view.

The builder has various functions to convert existing sections and structures into subforms and vice versa. You can also move structures to subforms or parent forms. Just open the menu for a section to see all the possible options.

## 🗂️ Using external sources {#external-subforms}
When you want to enable the builder to load subforms from external sources, there are two possible options that can be implemented. First of all, you can supply a list of available forms. This list is shown by the builder when a user wants to create a new subform. The other option is to implement a custom UI component to let the user browse through the available forms. And of course, you can combine both options.

### 📇 List forms {#list}
The list of forms is shown when the builder user wants to add a new subform. It allows the use of submenus and dividers to create a multilayer menu, as shown in the following example:
<Preview src="subforms-list.png" spacer={true} />

The forms list is implemented using the [`onListForms`](../../api/interfaces/IBuilderProperties.mdx#onListForms) property of the [`Builder`](../../api/classes/Builder.mdx) class. This property accepts the list of forms as an array, a function that returns the array, or a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) that resolves with an array. The latter is useful when you want to retrieve the list of forms from an API or other asynchronous process.

The forms list array accepts three types of items (click the items below to learn how to define them):
- [Forms](../../api/interfaces/IBuilderProperties.mdx#onListForms-definition) (can be supplied by either their [definition](../../../api/interfaces/IBuilderProperties#onListForms-definition) or [reference](../../../api/interfaces/IBuilderProperties#onListForms-reference));
- [Categories](../../api/interfaces/IBuilderProperties.mdx#onListForms-categories) (used to define submenus as the `Examples` category in the screenshot above);
- [Dividers](../../api/interfaces/IBuilderProperties.mdx#onListForms-dividers) (used to separate items in the menu as the `Recent forms` and `Your library` sections in the screenshot above).

:::info
The form itself can be supplied by either the [form definition](../../api/interfaces/IBuilderProperties.mdx#onListForms-definition) or a [reference string](../../api/interfaces/IBuilderProperties.mdx#onListForms-reference) of the form. When a reference string is used, the [`onLoadForm`](../../api/interfaces/IBuilderProperties.mdx#onLoadForm) event is required to load the actual form definition for the given reference. See [loading subforms](#loading) for more information. When the form definition is supplied, you may also return a promise that resolves to the form definition. That allows to asynchronous load form definitions from the list, without the need of the [`onLoadForm`](../../api/interfaces/IBuilderProperties.mdx#onLoadForm) event.
:::

#### Example
```ts showLineNumbers
import { Builder } from "@tripetto/builder";

// Let's assume we have two form definitions in global variables so
// we can use these variables in the forms list.
declare CONTACT_FORM: IDefinition;
declare FEEDBACK_FORM: IDefinition;

const builder = new Builder({
  //highlight-start
  onListForms: [
    {
      divider: "Recent forms",
    },
    {
      name: "Contact form",
      definition: CONTACT_FORM,
    },
    {
      name: "Feedback form",
      // We can also supply a function that returns a promise
      definition: () =>
        new Promise((resolve, reject) => {
          // Let's simulate an asynchronous action
          setTimeout(() => {
            resolve(FEEDBACK_FORM);
          }, 1000);
        }),
    },
    {
      divider: "Your library",
    },
    {
      category: "Examples",
      forms: [
        {
          name: "Contact form",
          definition: CONTACT_FORM,
        },
        {
          name: "Feedback form",
          definition: FEEDBACK_FORM,
        },
      ],
    },
  ],
  //highlight-end
});
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/JjpOGeb/d1a76edf9d9ff6e7f330807de5ff06d4) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/JjpOGeb/d1a76edf9d9ff6e7f330807de5ff06d4)

See the [`onListForms`](../../api/interfaces/IBuilderProperties.mdx#onListForms) API reference for more information about this event and the possible options for listing items.

### 📂 Browse forms {#browse}
In some cases, supplying forms through a list is not ideal. For example, when you have a lot of forms. Or when you want to enable the user to search for forms based on certain criteria. In that case, Tripetto allows to supply a custom UI for selecting an external form. This is implemented using the [`onBrowseForms`](../../api/interfaces/IBuilderProperties.mdx#onBrowseForms) property of the [`Builder`](../../api/classes/Builder.mdx) class. The process is quite simple. When the user clicks the option to browse for a form (the label of this option can be configured using the [`browseFormsLabel`](../../api/interfaces/IBuilderProperties.mdx#browseFormsLabel) property), the builder creates a blank canvas for you to construct the UI on to. A reference to the element of that canvas is supplied. In return, you should give a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) back that resolves with the form definition (or the form reference) when a form is selected. You may also reject the promise to indicate that the user wants to cancel the action.

#### Example
The following example uses [React](https://react.dev) to render a custom UI to browse for a form. In this case, a simple list of 2 forms that can be clicked and a cancel button to close the UI without selecting a form.
```ts showLineNumbers
import { Builder } from "@tripetto/builder";

// Let's assume we have two form definitions in global variables so
// we can use these variables for this demo.
declare CONTACT_FORM: IDefinition;
declare FEEDBACK_FORM: IDefinition;

const builder = new Builder({
  //highlight-start
  onBrowseForms: (element) =>
    new Promise((resolve, reject) => {
      // We use the styles property of the element to set a background
      element.styles.backgroundColor = "rgba(0,0,0,0.2)";
      element.styles.backdropFilter = "blur(5px)";

      // Create a React root
      const root = ReactDOM.createRoot(element.HTMLElement);

      // Render the UI
      root.render(
        <div>
          <h1>Browse forms</h1>
          <ul>
            <li onClick={() => resolve(CONTACT_FORM)}>Contact form</li>
            <li onClick={() => resolve(FEEDBACK_FORM)}>Feedback form</li>
          </ul>
          <button onClick={() => reject()}>Close</button>
        </div>
      );
    }),
  //highlight-end
});
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/zYREPbV/0b8f076a350267b5240285322fd1def1) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/zYREPbV/0b8f076a350267b5240285322fd1def1)

See the [`onBrowseForms`](../../api/interfaces/IBuilderProperties.mdx#onBrowseForms) API reference for more information about this event.

## 📤 Loading subforms {#loading}
When the [`onListForms`](../../api/interfaces/IBuilderProperties.mdx#onListForms) or [`onBrowseForms`](../../api/interfaces/IBuilderProperties.mdx#onBrowseForms) events return a reference string of a form, the actual form definition for that form still needs to be loaded. In that case, the [`onLoadForm`](../../api/interfaces/IBuilderProperties.mdx#onLoadForm) event of the [`Builder`](../../api/classes/Builder.mdx) class comes into play. This event receives the reference of the form and should return the form definition (or a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) that resolves with the form definition).

:::info
This event is also invoked when a user tries to update (reload) a subform. Only subforms that have a [reference](../../api/interfaces/INestedDefinition.mdx#reference) can be updated. You can implement the [`onUpdateForm`](../../api/interfaces/IBuilderProperties.mdx#onUpdateForm) event to indicate if there is an update available for a form.
:::

#### Example
In most scenarios, this event calls an API to fetch the form definition. Here is an example using the [Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API):
```ts showLineNumbers
import { Builder } from "@tripetto/builder";

const builder = new Builder({
  //highlight-start
  onLoadForm: (reference) =>
    new Promise((resolve, reject) => {
      fetch("/api/form/get/" + reference)
        .then((response) => response.json())
        .then((data) => resolve(data))
        .catch(() => reject());
    })
  //highlight-end
});
```

See the [`onLoadForm`](../../api/interfaces/IBuilderProperties.mdx#onLoadForm) API reference for more information about this event.

## 🔃 Updating subforms {#updating}
Subforms that have a [reference](../../api/interfaces/INestedDefinition.mdx#reference) set can be reloaded when the [`onLoadForm`](../../api/interfaces/IBuilderProperties.mdx#onLoadForm) event is implemented. This action is initiated by the builder user from the subform menu. If you implement the [`onUpdateForm`](../../api/interfaces/IBuilderProperties.mdx#onUpdateForm) event, that update menu option is only available when there is an updated (newer) version of the form available. So, the event indicates to the builder if the current subform is the latest version or not. The event receives the [reference](../../api/interfaces/INestedDefinition.mdx#reference) of the subform, and the current loaded [version](../../api/interfaces/INestedDefinition.mdx#version) (if that property is set) as input. The event should return `true` (or a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) that resolves to `true`) when there is an update available for the form.

#### Example
In most scenarios, this event calls an API to fetch the update state. Here is an example using the [Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API). In this example, the server would return a JSON object with a `needsUpdate` property that indicates if the form needs an update or not.
```ts showLineNumbers
import { Builder } from "@tripetto/builder";

const builder = new Builder({
  //highlight-start
  onUpdateForm: (reference, version) =>
    new Promise((resolve, reject) => {
      fetch("/api/form/update/" + reference)
        .then((response) => response.json())
        .then((data) => resolve(data.needsUpdate))
        .catch(() => reject());
    })
  //highlight-end
});
```

See the [`onUpdateForm`](../../api/interfaces/IBuilderProperties.mdx#onUpdateForm) API reference for more information about this event.

## 💾 Saving subforms {#saving}
If you want to enable the builder user to save subforms, you should implement the [`onSaveForm`](../../api/interfaces/IBuilderProperties.mdx#onSaveForm) event. This event receives the [form definition](../../api/interfaces/INestedDefinition.mdx) of the subform as input and should return a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) with the result of the save operation.

The event also receives an element that can be used to render a UI with, for example, a save dialog.

:::tip
You can change the default label for the save function using the [`saveFormLabel`](../../api/interfaces/IBuilderProperties.mdx#saveFormLabel) property.
:::

#### Example
The following example uses [React](https://react.dev) to render a custom UI to save a form. In a real application, the UI could ask the user for a location to store the form. When the user confirms, the form is stored, and a reference to the form is returned.
```ts showLineNumbers
import { Builder } from "@tripetto/builder";

const builder = new Builder({
  //highlight-start
  onSaveForm: (definition, element) =>
    new Promise((resolve, reject) => {
      // We use the styles property of the element to set a background
      element.styles.backgroundColor = "rgba(0,0,0,0.2)";
      element.styles.backdropFilter = "blur(5px)";

      // Create a React root
      const root = ReactDOM.createRoot(element.HTMLElement);

      // Render the UI
      root.render(
        <div>
          <h1>Save form</h1>
          <button
            onClick={() => {
              fetch("/api/form/save", {
                method: "POST",
                headers: {
                  "Content-Type": "application/json",
                },
                body: JSON.stringify(definition),
              })
                .then((response) => response.json())
                .then((data) => resolve(data))
                .catch(() => reject());
            }}
          >
            Save
          </button>
          <button onClick={() => reject()}>Cancel</button>
        </div>
      );
    }),
  //highlight-end
});
```

See the [`onSaveForm`](../../api/interfaces/IBuilderProperties.mdx#onSaveForm) API reference for more information about this event.

:::info
Rendering a UI for saving a form is optional. You can also use this event to save the form directly to an endpoint and return the form reference for headless operation.
:::

## 🧭 Navigation and breadcrumb {#breadcrumb}
Whenever the builder opens a subform, a new builder panel is generated for it. This panel shifts into view, and by default, a floating navigation bar is shown for each subform. This navigation bar contains the title of the subform and contains buttons to edit the form title or return to the parent form. If you want, you can implement a custom navigation bar (or breadcrumb) to assist the user while navigating through subforms. To do so, you need to implement the [`onBreadcrumb`](../../api/interfaces/IBuilderProperties.mdx#onBreadcrumb) event.

:::caution
As soon as you implement this event, the default navigation bar to close a subform (this is a floating bar in the builder) will not be shown anymore. So now you are in control and should present navigation buttons to the user.
:::

#### Example
Here is an example of a custom navigation bar using [React](https://react.dev):
```tsx showLineNumbers
import React, { useEffect, useRef, useState } from "react";
import { Builder, IDefinition } from "@tripetto/builder";

function TripettoBuilder(props: {
  definition?: IDefinition;
  onSave?: (definition: IDefinition) => void;
}) {
  const elementRef = useRef<HTMLDivElement>(null);
  const builderRef = useRef<Builder>();
  //highlight-start
  const [breadcrumb, setBreadcrumb] = useState([]);
  const backRef = useRef();
  //highlight-end

  useEffect(() => {
    if (!builderRef.current) {
      builderRef.current = Builder.open(props.definition, {
        element: elementRef.current,
        onSave: props.onSave,
        //highlight-start
        onBreadcrumb: (forms, back) => {
          backRef.current = back;
          setBreadcrumb(forms);
        }
        //highlight-end
      });
    }
  }, []);

  return (
    <>
      //highlight-start
      <div className="navigation">
        <button
          onClick={() => backRef.current && backRef.current()}
          disabled={!backRef.current}
        >
          Back
        </button>
        {breadcrumb.map((form, i) => (
          <>
            {i > 0 ? " > " : ""}
            <span onClick={() => form.edit()}>
              {form.name || "Unnamed form"}
            </span>
          </>
        ))}
      </div>
      //highlight-end
      <div ref={elementRef} className="builder"></div>
    </>
  );
}
```
[![Run](/img/button-run.svg)](https://codepen.io/tripetto/live/ExQbZgp/ab2d8059815ca4f8ca30d2fa007c3f86) [![Try on CodePen](/img/button-codepen.svg)](https://codepen.io/tripetto/pen/ExQbZgp/ab2d8059815ca4f8ca30d2fa007c3f86)

See the [`onBreadcrumb`](../../api/interfaces/IBuilderProperties.mdx#onBreadcrumb) API reference for more information about this event.
