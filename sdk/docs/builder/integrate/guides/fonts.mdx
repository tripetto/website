---
title: Bundling fonts - Guides - Builder
sidebar_label: Bundling fonts
sidebar_position: 10
description: The builder needs some specific font files to work properly. These font files are included in the Tripetto Builder package.
---

# Bundling builder fonts
The builder needs some specific font files to work properly. These font files are included in the [Tripetto Builder](https://unpkg.com/browse/tripetto/fonts/) package. But, without the proper configuration, the builder will use the [unpkg](https://unpkg.com/) CDN for loading the font files. And, since unpkg is a free CDN, that's not always the best option. To avoid this, bundle the font files with your application and then instruct the builder where to load the files.

## ⚙️ Setting it up
You must copy the required font files to a custom folder inside your project. You can do this manually or configure a  build step to automate this (see a [webpack example](#example) below). The fonts are in the `fonts` folder of the builder package. Make sure the font files become available on the domain of your application. Next, configure the builder to load the fonts from that domain. Use the [fonts](../../api/interfaces/IBuilderProperties.mdx#fonts) property to do this and supply a relative path or an absolute URL to your font folder. This property is part of the [`IBuilderProperties`](../../api/interfaces/IBuilderProperties.mdx) interface used when [constructing](../../api/classes/Builder.mdx#constructor) a new [builder instance](../../api/classes/Builder.mdx).

```ts showLineNumbers
import { Builder } from "@tripetto/builder";

// Use an absolute URL
const builder = new Builder({
    fonts: "https://yourdomain/static/fonts/"
});

// Or a relative path
const builder = new Builder({
    fonts: "static/fonts/"
});
```

:::tip
Use the developer tools of your browser to verify the fonts are loading correctly from the application domain.
:::

## 📃 Example webpack configuration {#example}
If you use webpack you could use [`copy-webpack-plugin`](https://www.npmjs.com/package/copy-webpack-plugin) to copy the font files as part of the build process:

```js showLineNumbers title="webpack.config.js"
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
  plugins: [
    new CopyPlugin({
      patterns: [
        { from: "node_modules/@tripetto/builder/fonts/", to: "static/fonts/" },
      ],
    }),
  ],
};
```
