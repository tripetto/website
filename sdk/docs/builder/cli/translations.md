---
title: Translations - Builder CLI tool
sidebar_label: Translations
sidebar_position: 7
toc_min_heading_level: 3
description: The Tripetto builder comes bundled with a set of translations.
---

# Translations
The Tripetto builder comes bundled with a set of translations (see a list [here](https://gitlab.com/tripetto/translations/-/blob/master/README.md#components-languages-and-translation-progress)). To load a translation, use the `--language` option to specify the ISO 639-1 language code of the translation you want to use. For example, the following command instructs the builder to use the Dutch translation:

```bash
tripetto --language nl ./example.json
```

If you are working on a custom translation for the builder (or for a block you are building), you can also specify a path to the JSON translation file:

```bash
tripetto --language custom-translation.json ./example.json
```

This allows you to test translation files.

:::info
Instructions on how to generate JSON translations files are [here](https://gitlab.com/tripetto/translations/-/blob/master/README.md#test-builder-translation
).
:::

## Contribute
If you want to help translate the Tripetto builder to other languages, you're very welcome to do so! Go to our [translations repository](https://gitlab.com/tripetto/translations) for instructions. Download the builder [POT file](https://unpkg.com/@tripetto/builder/translations/template.pot) and start translating it. We use [POEdit](https://poedit.net/) for this job, but you can use any other tool or service you like. Please send us the translated PO file, and we are more than happy to include it.
