---
title: Builder CLI tool
sidebar_label: Introduction
sidebar_position: 1
description: The CLI tool allows you to run the Tripetto builder locally on your machine.
---

# ⌨️ Builder CLI tool
The CLI tool allows you to run the Tripetto builder locally on your machine. It is an excellent option for developers who want to use Tripetto to create forms inside a project. By using the CLI tool, the form definition is saved locally in your filesystem. Useful if the form needs to be part of a code repository or when developing new blocks for Tripetto.

🎓 This chapter contains all the information you need to start using the builder from the command line.

🆓 You can use the CLI tool for free.
