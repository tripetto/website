---
title: Prerequisites - Builder CLI tool
sidebar_label: Prerequisites
sidebar_position: 2
description: Overview of prerequisites to install the builder CLI tool.
---

# Prerequisites
Tripetto needs [Node.js](https://nodejs.org) to install and run it, and version 10.12 or higher is required. Node.js is available for Windows, macOS, and Linux.

For the installation of the builder you need [npm](https://www.npmjs.com/package/@tripetto/builder) (included with Node.js) or [Yarn](https://yarnpkg.com/package/tripetto).
