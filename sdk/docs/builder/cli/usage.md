---
title: Usage - Builder CLI tool
sidebar_label: Usage
sidebar_position: 4
description: After global installation, you can start the builder from your command line terminal. It runs in any modern browser.
---

# Usage

## Global usage
After global installation, you can start the builder from your command line terminal. It runs in any modern browser. You need to specify the filename of the form definition you want to create or edit. It can be any filename you like. The [form definition](../api/interfaces/IDefinition.mdx) file contains the complete structure of your form.

Let's say you want to create a new form definition with the filename `demo.json`. Run the following command to do so:

```bash
tripetto demo.json
```

This command creates the file in the current working directory (if it does not already exist there) or loads the file if it is available. A server instance at `localhost` port `3333` starts. Your default browser should automatically open with `http://localhost:3333`. If not, open the browser of your choice and navigate to this URL.

There are some additional [command line options](options.md) you can use.

:::caution
You may notice there are no blocks available in the builder. That's because the builder itself does not contain any default blocks. You need to install them yourself. See the [configuration](configuration.md) chapter on how to load the desired blocks.
:::

## Use in a project
When you add Tripetto to a project, you can use [npm scripts](https://docs.npmjs.com/cli/v7/using-npm/scripts) to run Tripetto. First, make sure to add Tripetto to your project as shown in the [installation](installation.md#local-installation) chapter. Then define a task like in the following example:

```json showLineNumbers title="package.json"
{
  "name": "tripetto-example",
  "scripts": {
    "edit-example-form": "tripetto ./example.json"
  },
  "dependencies": {
    "@tripetto/builder": "^5.0.30",
  }
}
```

Now you can start editing the example form by running:

```bash npm2yarn
npm run edit-example-form
```
