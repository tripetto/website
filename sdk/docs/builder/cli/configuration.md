---
title: Configuration - Builder CLI tool
sidebar_label: Configuration
sidebar_position: 5
description: By default, the builder loads no blocks, so you need to configure the builder to load the blocks you want.
---

# Configuring the builder
By default, the builder loads no blocks. So you need to configure the builder to load the blocks you want. There are three options for configuring the available blocks:
- [Locally in your `package.json` file](#using-packagejson)
- [Locally in a separate configuration file](#using-separate-configuration-file)
- [Global configuration](#global-configuration)

In all cases, you need to specify an array that contains the package names or the paths of the desired blocks.

:::tip
To enable blocks in the builder, you need to install the blocks first. If you want to globally install all the available [stock blocks](../../blocks/stock/index.mdx) (blocks developed and maintained by the Tripetto team), run the following command:

```bash npm2yarn
npm install @tripetto/block-calculator @tripetto/block-checkbox @tripetto/block-checkboxes @tripetto/block-date @tripetto/block-device @tripetto/block-dropdown @tripetto/block-email @tripetto/block-error @tripetto/block-evaluate @tripetto/block-file-upload @tripetto/block-hidden-field @tripetto/block-mailer @tripetto/block-matrix @tripetto/block-multi-select @tripetto/block-multiple-choice @tripetto/block-number @tripetto/block-paragraph @tripetto/block-password @tripetto/block-phone-number @tripetto/block-picture-choice @tripetto/block-radiobuttons @tripetto/block-ranking @tripetto/block-rating @tripetto/block-regex @tripetto/block-scale @tripetto/block-setter @tripetto/block-signature @tripetto/block-statement @tripetto/block-stop @tripetto/block-text @tripetto/block-textarea @tripetto/block-url @tripetto/block-variable @tripetto/block-yes-no -g
```
:::

## Local configuration

### Using `package.json`
If you start the builder from a folder containing a `package.json` file, you can use this file to embed your configuration:

```json showLineNumbers title="package.json"
{
  "name": "your-package-name",
  "tripetto": {
    "blocks": [
      "block-package-a",
      "block-package-b",
      "./path-to-block-c"
    ]
  }
}
```

You should list all the desired blocks in the `blocks` array. You can specify the name of the block (in case you use block packages) or the path to the folder that contains a block.

When building a package, you can also include the builder and block packages as package dependencies. This way, you can distribute the builder and the blocks you use along with your package. The builder parses the configuration found in `package.json` and loads the appropriate block packages.

:::tip
To load all [stock blocks](../../blocks/stock/index.mdx), add the block packages to your project:

```bash npm2yarn
npm install @tripetto/block-calculator @tripetto/block-checkbox @tripetto/block-checkboxes @tripetto/block-date @tripetto/block-device @tripetto/block-dropdown @tripetto/block-email @tripetto/block-error @tripetto/block-evaluate @tripetto/block-file-upload @tripetto/block-hidden-field @tripetto/block-mailer @tripetto/block-matrix @tripetto/block-multi-select @tripetto/block-multiple-choice @tripetto/block-number @tripetto/block-paragraph @tripetto/block-password @tripetto/block-phone-number @tripetto/block-picture-choice @tripetto/block-radiobuttons @tripetto/block-ranking @tripetto/block-rating @tripetto/block-regex @tripetto/block-scale @tripetto/block-setter @tripetto/block-signature @tripetto/block-statement @tripetto/block-stop @tripetto/block-text @tripetto/block-textarea @tripetto/block-url @tripetto/block-variable @tripetto/block-yes-no
```

And use the following configuration:

```json showLineNumbers title="package.json"
{
  "name": "your-package-name",
  "tripetto": {
    "blocks": [
      "@tripetto/block-calculator",
      "@tripetto/block-checkbox",
      "@tripetto/block-checkboxes",
      "@tripetto/block-date",
      "@tripetto/block-device",
      "@tripetto/block-dropdown",
      "@tripetto/block-email",
      "@tripetto/block-error",
      "@tripetto/block-evaluate",
      "@tripetto/block-file-upload",
      "@tripetto/block-hidden-field",
      "@tripetto/block-mailer",
      "@tripetto/block-matrix",
      "@tripetto/block-multi-select",
      "@tripetto/block-multiple-choice",
      "@tripetto/block-number",
      "@tripetto/block-paragraph",
      "@tripetto/block-password",
      "@tripetto/block-phone-number",
      "@tripetto/block-picture-choice",
      "@tripetto/block-radiobuttons",
      "@tripetto/block-ranking",
      "@tripetto/block-rating",
      "@tripetto/block-regex",
      "@tripetto/block-scale",
      "@tripetto/block-setter",
      "@tripetto/block-signature",
      "@tripetto/block-statement",
      "@tripetto/block-stop",
      "@tripetto/block-text",
      "@tripetto/block-textarea",
      "@tripetto/block-url",
      "@tripetto/block-variable",
      "@tripetto/block-yes-no"
    ]
  }
}
```
:::

### Using separate configuration file
If you don't want to use your `package.json` file or you are not using the builder from within a package, you can create a separate configuration file named `tripetto.json` with the following structure:

```json showLineNumbers title="tripetto.json"
{
  "blocks": [
    "block-package-a",
    "block-package-b",
    "./path-to-block-c"
  ]
}
```

You should list all the desired blocks in the `blocks` array. You can specify the name of the block (in case you use block packages) or the path to the folder that contains a block.

When the builder starts from a folder containing a `tripetto.json` file, this configuration file is parsed and used.

:::info
You can choose to make the specified block packages available in the same local folder as the configuration or install the block packages globally.
:::


:::tip
To load all [stock blocks](../../blocks/stock/index.mdx), add the block packages to your project:

```bash npm2yarn
npm install @tripetto/block-calculator @tripetto/block-checkbox @tripetto/block-checkboxes @tripetto/block-date @tripetto/block-device @tripetto/block-dropdown @tripetto/block-email @tripetto/block-error @tripetto/block-evaluate @tripetto/block-file-upload @tripetto/block-hidden-field @tripetto/block-mailer @tripetto/block-matrix @tripetto/block-multi-select @tripetto/block-multiple-choice @tripetto/block-number @tripetto/block-paragraph @tripetto/block-password @tripetto/block-phone-number @tripetto/block-picture-choice @tripetto/block-radiobuttons @tripetto/block-ranking @tripetto/block-rating @tripetto/block-regex @tripetto/block-scale @tripetto/block-setter @tripetto/block-signature @tripetto/block-statement @tripetto/block-stop @tripetto/block-text @tripetto/block-textarea @tripetto/block-url @tripetto/block-variable @tripetto/block-yes-no
```

And use the following configuration:

```json showLineNumbers title="tripetto.json"
{
  "blocks": [
    "@tripetto/block-calculator",
    "@tripetto/block-checkbox",
    "@tripetto/block-checkboxes",
    "@tripetto/block-date",
    "@tripetto/block-device",
    "@tripetto/block-dropdown",
    "@tripetto/block-email",
    "@tripetto/block-error",
    "@tripetto/block-evaluate",
    "@tripetto/block-file-upload",
    "@tripetto/block-hidden-field",
    "@tripetto/block-mailer",
    "@tripetto/block-matrix",
    "@tripetto/block-multi-select",
    "@tripetto/block-multiple-choice",
    "@tripetto/block-number",
    "@tripetto/block-paragraph",
    "@tripetto/block-password",
    "@tripetto/block-phone-number",
    "@tripetto/block-picture-choice",
    "@tripetto/block-radiobuttons",
    "@tripetto/block-ranking",
    "@tripetto/block-rating",
    "@tripetto/block-regex",
    "@tripetto/block-scale",
    "@tripetto/block-setter",
    "@tripetto/block-signature",
    "@tripetto/block-statement",
    "@tripetto/block-stop",
    "@tripetto/block-text",
    "@tripetto/block-textarea",
    "@tripetto/block-url",
    "@tripetto/block-variable",
    "@tripetto/block-yes-no"
  ]
}
```
:::

## Global configuration
If you want to specify blocks globally, create a file in the user/home folder named `tripetto.json` with the following structure:

```json showLineNumbers title="~/tripetto.json"
{
  "blocks": [
    "block-package-a",
    "block-package-b",
    "./path-to-block-c"
  ]
}
```

You should list all the desired blocks in the `blocks` array. You can specify the name of the block (in case you use block packages) or the path to the folder that contains a block.

:::caution
Since the configuration is global, the block packages should also be globally installed. Otherwise, the builder is not able to find the supplied block packages.
:::

:::tip
To load all [stock blocks](../../blocks/stock/index.mdx), install the block packages globally:

```bash npm2yarn
npm install @tripetto/block-calculator @tripetto/block-checkbox @tripetto/block-checkboxes @tripetto/block-date @tripetto/block-device @tripetto/block-dropdown @tripetto/block-email @tripetto/block-error @tripetto/block-evaluate @tripetto/block-file-upload @tripetto/block-hidden-field @tripetto/block-mailer @tripetto/block-matrix @tripetto/block-multi-select @tripetto/block-multiple-choice @tripetto/block-number @tripetto/block-paragraph @tripetto/block-password @tripetto/block-phone-number @tripetto/block-picture-choice @tripetto/block-radiobuttons @tripetto/block-ranking @tripetto/block-rating @tripetto/block-regex @tripetto/block-scale @tripetto/block-setter @tripetto/block-signature @tripetto/block-statement @tripetto/block-stop @tripetto/block-text @tripetto/block-textarea @tripetto/block-url @tripetto/block-variable @tripetto/block-yes-no -g
```

And use the following configuration:

```json showLineNumbers title="~/tripetto.json"
{
  "blocks": [
    "@tripetto/block-calculator",
    "@tripetto/block-checkbox",
    "@tripetto/block-checkboxes",
    "@tripetto/block-date",
    "@tripetto/block-device",
    "@tripetto/block-dropdown",
    "@tripetto/block-email",
    "@tripetto/block-error",
    "@tripetto/block-evaluate",
    "@tripetto/block-file-upload",
    "@tripetto/block-hidden-field",
    "@tripetto/block-mailer",
    "@tripetto/block-matrix",
    "@tripetto/block-multi-select",
    "@tripetto/block-multiple-choice",
    "@tripetto/block-number",
    "@tripetto/block-paragraph",
    "@tripetto/block-password",
    "@tripetto/block-phone-number",
    "@tripetto/block-picture-choice",
    "@tripetto/block-radiobuttons",
    "@tripetto/block-ranking",
    "@tripetto/block-rating",
    "@tripetto/block-regex",
    "@tripetto/block-scale",
    "@tripetto/block-setter",
    "@tripetto/block-signature",
    "@tripetto/block-statement",
    "@tripetto/block-stop",
    "@tripetto/block-text",
    "@tripetto/block-textarea",
    "@tripetto/block-url",
    "@tripetto/block-variable",
    "@tripetto/block-yes-no"
  ]
}
```
:::

## Configuration loading order
When Tripetto starts, it tries to load any available configuration files in the following order:

1. `tripetto.json` in the current working directory
2. `package.json` in the current working directory
3. `~/tripetto.json` in the user/home folder

## Stop configuration loading
Tripetto can use multiple configuration files. So it can load both a local configuration and a global configuration at once, following the loading order specified above. To stop Tripetto from doing this, add the property `noFurtherLoading` to the configuration file. As soon as the builder hits a configuration file that has this property set to `true`, the loading stops right after processing that file.

```json showLineNumbers title="package.json"
{
  "name": "your-package-name",
  "tripetto": {
    "blocks": [
      "block-a",
      "block-b"
    ],
    "noFurtherLoading": true
  }
}
```

## Using an entry point
When you are developing [custom blocks](../../blocks/introduction.md) it can be useful to directly point the builder to a certain block bundle file. This is possible using the `entry` property of the configuration. You can supply a path to a JS bundle that contains the blocks that needs to be loaded.

```json showLineNumbers title="package.json"
{
  "name": "your-package-name",
  "tripetto": {
    "entry": "./dist/blocks.bundle.js"
  }
}
```
