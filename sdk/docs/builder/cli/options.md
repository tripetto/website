---
title: Options - Builder CLI tool
sidebar_label: Options
sidebar_position: 6
description: List of available command line options.
---

# Options
The following command line options are available:

| Parameter    | Explanation                                                   | Default   |
|--------------|---------------------------------------------------------------|-----------|
| `--host`     | Specifies a custom hostname.                                  | localhost |
| `--port`     | Specifies a custom server port.                               | 3333      |
| `--preview`  | Specifies the preview URL.                                    |           |
| `--silent`   | Do not automatically open the builder in the default browser. |           |
| `--verbose`  | Enables verbose logging (logs the loaded blocks).             |           |
| `--language` | Specifies the language locale/translation to use.             | en        |
| `--version`  | Shows the version.                                            |           |
| `--tutorial` | Shows the tutorial dialog in the builder at startup.          |           |
| `--help`     | Shows help information.                                       |           |
