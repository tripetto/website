---
title: Installation - Builder CLI tool
sidebar_label: Installation
sidebar_position: 3
description: When you install Tripetto globally, you can run it from anywhere in your command line terminal. But you can also install it locally (per project).
---

# Installation
There are two options for the installation:

- Global installation
- Local installation

When you install Tripetto globally, you can run it from anywhere in your command line terminal. But you can also install it locally (per project). Global and local installations (even different versions) can coexist.

## Global installation
Enter the following command in your command line terminal:

```bash npm2yarn
npm install @tripetto/builder -g
```

This command installs the latest stable version. If you want to validate the installation, run the command `tripetto --version` after the installation. It outputs the current Tripetto version number.

:::info
If you want to try the latest bleeding edge version, you can do so by adding the `@next` tag as follows: `npm i @tripetto/builder@next -g`. Use at your own risk. Could be unstable 🤪
:::

## Local installation
For a local installation, open your command line terminal and change to the desired working directory. Then run the following command:

```bash npm2yarn
npm install @tripetto/builder
```

:::info
As of npm v5.0.0, installed modules are added as a dependency by default. So the `--save` option is no longer needed. The other save options still exist and are listed in the documentation for [npm install](https://docs.npmjs.com/cli/v7/commands/npm-install).
:::

:::tip
If you want to install Tripetto in `devDependencies` add `--save-dev` to the install command.
:::
