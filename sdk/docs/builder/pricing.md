---
title: Builder Pricing
sidebar_label: Pricing
sidebar_position: 6
description: When you want to integrate the builder into your own web app or website, you need a license.
---

# 💳 Pricing
The [CLI version](cli/introduction.md) of the builder can be used free of charge. We consider this a developer tool. Have a blast 😎

When you want to [integrate](integrate/introduction.md) the builder into your own web app or website, you do need a license. Pricing depends on the use-case and number of users. Please see the [pricing](https://tripetto.com/sdk/pricing/) page for more information.

[➡️ Go to the pricing page](https://tripetto.com/sdk/pricing/)

[➡️ Learn more about using license codes](integrate/guides/license.mdx)
