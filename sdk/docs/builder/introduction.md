---
title: Builder
sidebar_label: Introduction
sidebar_position: 1
description: To create a Tripetto form the form builder is used.
---

# Building forms

🎓 To create a Tripetto form the **form builder** is used.

✨ The output of the builder is a [**JSON form definition**](api/interfaces/IDefinition.mdx) that can be supplied to a [form runner](../runner/introduction.md).

💡 Use the **[Studio web app](../applications/studio/introduction.md)** or **[CLI tool](cli/introduction.md)** if you just want to create a form and [run](../runner/introduction.md) it.

👩‍💻 Or, integrate the builder into **your own web app or website** in minutes!

## 👩‍🏫 Introduction

So you want to create this beautiful, fancy, and smart form. And, you want to use Tripetto for that. Well, you've come to the right place! But how to start? The first question to ask yourself is:
**Do I need to run the Tripetto form builder (so the thing you use to create forms) locally on my computer, or do I want to integrate the builder into my own web app?**

👍 If the answer is **yes**, continue reading this chapter!

👎 If the answer is **no**, you probably just want to create and run a form. There is no need to integrate the builder nor run it locally. Instead, use our [Tripetto Studio web app](../applications/studio/introduction.md). It's a free online service that allows you to create forms, save them and export their form definitions for use in the runner so you can embed those forms anywhere you like. If you happen to have a WordPress website, make sure to check out the [Tripetto WordPress plugin](../applications/wordpress/introduction.md). It can do the same as the Studio but then all from inside your own WordPress environment.

:::tip TL;DR
If you need an awesome form builder you've come to the right place. Like to run it locally on your machine? Use the [CLI tool](cli/introduction.md). Want to integrate it in a custom web app or website? Head over to [integration](integrate/introduction.md) documentation. If you just want to build an embeddable form, try the [Studio web app](../applications/studio/introduction.md) or [Tripetto WordPress plugin](../applications/wordpress/introduction.md).
:::

## 📺 Preview {#preview}
![Builder preview](/img/builder.png)

## 🧐 What you should know
The Tripetto form builder is a powerful graphical form builder that lets you build smart flowing forms and surveys. It works using a 2D self-organizing storyboard that lets you sketch forms like flowcharts. It runs in any modern browser; with mouse, touch, or pen. It has excellent support for touch-only enabled devices like tablets. Worth mentioning, as many other form builders don't work well on these type of devices.

### Concepts
The builder incorporates a number of solutions and principles to allow you to smoothly create and edit forms. The most important visual ones are the [storyboard](concepts.md#storyboard), [add-and-arrange](concepts.md#add-and-arrange), [layout guidance](concepts.md#layout-guidance), [active allineation](concepts.md#active-allineation), [smart zoom](concepts.md#smart-zoom), and [feature cards](concepts.md#feature-cards).

### Blocks
The builder uses blocks as a paradigm to question types (for example, a text input field). You can configure which blocks you want to use. You can also [develop your own blocks](../blocks/introduction.md) for the builder, making Tripetto extensible with custom question types.

### Form definition
The builder stores the complete structure of a form in a single JavaScript object (that can be saved as a JSON string) called the [form definition](api/interfaces/IDefinition.mdx). It includes all the properties and settings of the blocks used in a form. Loading a form is as simple as feeding a form definition to the builder. Saving a form is handled by listening to either the `save` or `change` event of the builder and then store the supplied form definition. How and where you store the form definition is up to you.

## 🎬 Usage scenarios
You can use the builder as a standalone tool ([CLI tool](cli/introduction.md)) or implement it in your web apps or websites ([integration](integrate/introduction.md)).

---
### ⌨️ CLI tool​ {#cli-tool}
Allows use of the builder as a CLI tool and run it locally on your machine. It is a great option for developers who want to use Tripetto to create forms inside a project. By using the CLI tool, the form definition is saved locally in your filesystem. Useful if the form needs to be part of a code repository.

▶️ [Start with the CLI tool](cli/introduction.md)

---
### 👩‍💻 Integration​ (in your own website or web app) {#integration}
Let's you fully integrate the builder into your own web app or website. And since the builder is a pure client-side component, you can do so without becoming dependent on any external service not owned by you. The whole thing runs inside the context of your project, giving you full control. There are hands-on instructions available for implementing the builder using:

[![Implement using plain JS](/img/logo-js.svg)](../integrate/quickstart/plain-js/)
[![Implement using React](/img/logo-react.svg)](../integrate/quickstart/react/)
[![Implement using Angular](/img/logo-angular.svg)](../integrate/quickstart/angular/)
[![Implement using HTML](/img/logo-html.svg)](../integrate/quickstart/html/)

▶️ [Start with an integration](integrate/introduction.md)

---
## 🖥️ Try it​ {#try-it}
See the builder in action. This demo implements the [builder](integrate/introduction.md) and the [stock runners](../runner/stock/introduction.md) in a side-by-side view. Play around and have fun!

[![Play](/img/button-play.svg)](https://jdmt3.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-plain-js-live-preview-with-multiple-runners-jdmt3?file=/src/index.ts)

---
## 🎞️ Video {#video}
<div className="youtube-embed"><iframe width="560" height="315" src="https://www.youtube.com/embed/78v901HZbD0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>
