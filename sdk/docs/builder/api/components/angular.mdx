---
title: Angular component - Builder
sidebar_label: Angular
description: The builder Angular component allows to use the builder in Angular applications with ease.
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# ![](/img/logo-angular.svg) Angular component
The builder [Angular](https://angular.io/) component allows to use the builder in Angular applications with ease. To use the component, simply import `TripettoBuilderModule` from the package and feed it to your application's `@NgModule` imports array. This makes the `<tripetto-builder>` selector available in your application. Below is a list of [inputs](#inputs) and [outputs](#outputs) that can be used on the selector.
:::tip
Read the [Angular implementation guide](../../integrate/quickstart/angular.mdx) for more information and examples.
:::
:::info
The Angular component is located in a subfolder named `angular` inside the builder package. Make sure to import the module and component from this folder.
:::
#### Module
```ts
import { TripettoBuilderModule } from "@tripetto/builder/angular";
```
#### Component
```ts
import { TripettoBuilderComponent } from "@tripetto/builder/angular";
```
#### Selector
```ts
<tripetto-builder></tripetto-builder>
```

### ⏬ Inputs {#inputs}
| Name                        | Type                                                                                                                                                               | Optional | Description                                                                                                                                                                                  |
|:----------------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------------|:---------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `definition`                | [`IDefinition`](../interfaces/IDefinition.mdx)                                                                                                                     | Yes      | Specifies the form definition to load (when omitted an empty form is loaded).                                                                                                                |
| `fonts`                     | string \| "inherit"                                                                                                                                                | Yes      | Specifies the path to the folder with the font files ([more information](../interfaces/IBuilderProperties.mdx#fonts)).                                                                       |
| `locale`                    | [`L10n.ILocale`](../../../blocks/api/modules/L10n/index.mdx#ILocale)                                                                                               | Yes      | Specifies the locale data for the builder ([more information](../interfaces/IBuilderProperties.mdx#locale)).                                                                                 |
| `translations`              | [`L10n.TTranslation`](../../../blocks/api/modules/L10n/index.mdx#TTranslation) \| [`L10n.TTranslation[]`](../../../blocks/api/modules/L10n/index.mdx#TTranslation) | Yes      | Specifies the translations for the builder and blocks ([more information](../interfaces/IBuilderProperties.mdx#translations)).                                                               |
| `namespace`                 | `{...}`                                                                                                                                                            | Yes      | Specifies a block namespace object to use or load ([more information](../interfaces/IBuilderProperties.mdx#namespace)).                                                                      |
| `tier`                      | [`IBuilderTier`](../interfaces/IBuilderTier.mdx)                                                                                                                   | Yes      | Specifies a tier for the builder ([more information](../interfaces/IBuilderProperties.mdx#tier)).                                                                                            |
| `license`                   | string                                                                                                                                                             | Yes      | Specifies a license code for the builder ([more information](../interfaces/IBuilderProperties.mdx#license)).                                                                                 |
| `zoom`                      | "1:1" \| "fit" \| "fit-horizontal" \| "fit-vertical"                                                                                                               | Yes      | Specifies the initial zoom state for the builder ([more information](../interfaces/IBuilderProperties.mdx#zoom)).                                                                            |
| `centering`                 | boolean                                                                                                                                                            | Yes      | Specifies if the content in the builder is centered instead of left-aligned (default is `true`).                                                                                             |
| `controls`                  | boolean                                                                                                                                                            | Yes      | Specifies the location of the navigation and zoombar (default is `right`).                                                                                                                   |
| `rubberBandEffect`          | boolean                                                                                                                                                            | Yes      | Specifies if the rubber band effect (also known as the elastic overscrolling effect) is enabled for the builder ([more information](../interfaces/IBuilderProperties.mdx#rubberBandEffect)). |
| `disableResizing`           | boolean                                                                                                                                                            | Yes      | Disables automatic resizing when the dimensions of the HTML element that holds the builder are changed ([more information](../interfaces/IBuilderProperties.mdx#disableResizing)).           |
| `disablePrologue`           | boolean                                                                                                                                                            | Yes      | Specifies if the prologue feature should be disabled ([more information](../interfaces/IBuilderProperties.mdx#disablePrologue)).                                                             |
| `disableEpilogue`           | boolean                                                                                                                                                            | Yes      | Specifies if the epilogue feature should be disabled ([more information](../interfaces/IBuilderProperties.mdx#disableEpilogue)).                                                             |
| `disableNesting`            | boolean                                                                                                                                                            | Yes      | Specifies if creating new nested branches (subforms) should be disabled.                                                                                                                     |
| `disableZoombar`            | boolean                                                                                                                                                            | Yes      | Disables the visual zoombar that is shown when the form exceeds the size of the builder viewport.                                                                                            |
| `disableLogo`               | boolean                                                                                                                                                            | Yes      | Disables the Tripetto logo in the navigation bar.                                                                                                                                            |
| `disableSaveButton`         | boolean                                                                                                                                                            | Yes      | Disables the save button in the navigation bar.                                                                                                                                              |
| `disableRestoreButton`      | boolean                                                                                                                                                            | Yes      | Disables the restore button in the navigation bar ([more information](../interfaces/IBuilderProperties.mdx#disableRestoreButton)).                                                           |
| `disableClearButton`        | boolean                                                                                                                                                            | Yes      | Disables the clear button in the navigation bar ([more information](../interfaces/IBuilderProperties.mdx#disableClearButton)).                                                               |
| `disableEditButton`         | boolean                                                                                                                                                            | Yes      | Disables the edit button in the navigation bar ([more information](../interfaces/IBuilderProperties.mdx#disableEditButton)).                                                                 |
| `disableCloseButton`        | boolean                                                                                                                                                            | Yes      | Disables the close button in the navigation bar ([more information](../interfaces/IBuilderProperties.mdx#disableCloseButton)).                                                               |
| `disableTutorialButton`     | boolean                                                                                                                                                            | Yes      | Disables the tutorial button in the navigation bar.                                                                                                                                          |
| `disableOpenCloseAnimation` | boolean                                                                                                                                                            | Yes      | Disables the open and close animation of the builder.                                                                                                                                        |
| `showTutorial`              | boolean                                                                                                                                                            | Yes      | Shows the tutorial dialog on startup.                                                                                                                                                        |
| `previewURL`                | string                                                                                                                                                             | Yes      | Specifies the URL of a preview page ([more information](../interfaces/IBuilderProperties.mdx#previewURL)).                                                                                   |
| `supportURL`                | string \| `false`                                                                                                                                                  | Yes      | Specifies an URL to a custom help or support page or supply `false` to disable this button.                                                                                                  |
| `helpTopics`                | `{...}`                                                                                                                                                            | Yes      | Specifies URLs to help topics and enables assistive buttons/links in the builder ([more information](../interfaces/IBuilderProperties.mdx#helpTopics)).                                      |
| `nestedFormMode`            | boolean                                                                                                                                                            | Yes      | Specifies if the builder should run in nested form mode ([more information](../interfaces/IBuilderProperties.mdx#nestedFormMode)).                                                           |
| `disableFormAbort`          | boolean                                                                                                                                                            | Yes      | Disables the possibility to abort the whole form in nested forms ([more information](../interfaces/IBuilderProperties.mdx#disableFormAbort)).                                                |
| `disableCreateEmptyForm`    | boolean                                                                                                                                                            | Yes      | Disables the possibility to create new (empty) nested forms.                                                                                                                                 |
| `disableConvertToForm`      | boolean                                                                                                                                                            | Yes      | Disables the possibility to convert sections into nested forms or vice-versa.                                                                                                                |
| `disableMoveToForm`         | boolean                                                                                                                                                            | Yes      | Disables the possibility to move sections to existing nested forms.                                                                                                                          |
| `onMenu`                    | Function                                                                                                                                                           | Yes      | Invoked when the context menu of a node, section, branch, or condition is requested ([more information](../interfaces/IBuilderProperties.mdx#onMenu)).                                       |
| `onOpenForm`                | Function                                                                                                                                                           | Yes      | Invoked when the builder wants to open a subform for editing ([more information](../interfaces/IBuilderProperties.mdx#onOpenForm)).                                                          |
| `onListForms`               | Function                                                                                                                                                           | Yes      | Invoked when the builder needs the list of forms that can be used as subform ([more information](../interfaces/IBuilderProperties.mdx#onListForms)).                                         |
| `onBrowseForms`             | Function                                                                                                                                                           | Yes      | Invoked when the builder wants to browse for a subform ([more information](../interfaces/IBuilderProperties.mdx#onBrowseForms)).                                                             |
| `browseFormsLabel`          | string                                                                                                                                                             | Yes      | Specifies an alternative label for the browse forms feature ([more information](../interfaces/IBuilderProperties.mdx#browseFormsLabel)).                                                     |
| `onLoadForm`                | Function                                                                                                                                                           | Yes      | Invoked when the builder wants to load a certain subform ([more information](../interfaces/IBuilderProperties.mdx#onLoadForm)).                                                              |
| `onUpdateForm`              | Function                                                                                                                                                           | Yes      | Invoked when the builder wants to know if there is an update available for a certain form ([more information](../interfaces/IBuilderProperties.mdx#onUpdateForm)).                           |
| `onSaveForm`                | Function                                                                                                                                                           | Yes      | Invoked when the builder wants to save a subform ([more information](../interfaces/IBuilderProperties.mdx#onSaveForm)).                                                                      |
| `saveFormLabel`             | string                                                                                                                                                             | Yes      | Specifies an alternative label for the save form feature ([more information](../interfaces/IBuilderProperties.mdx#saveFormLabel)).                                                           |
| `onBreadcrumb`              | Function                                                                                                                                                           | Yes      | Invoked everytime when a nested subform is opened or closed in the builder ([more information](../interfaces/IBuilderProperties.mdx#onBreadcrumb)).                                          |

### 📢 Outputs {#outputs}
| Name        | Type                    | Optional | Description                                                                                                        |
|:------------|:------------------------|:---------|:-------------------------------------------------------------------------------------------------------------------|
| `onLoad`    | `EventEmitter<Builder>` | Yes      | Invoked when the builder is loaded ([more information](../interfaces/IBuilderProperties.mdx#onLoad)).              |
| `onError`   | `EventEmitter<{...}>`   | Yes      | Invoked when the builder is loaded ([more information](../interfaces/IBuilderProperties.mdx#onError)).             |
| `onReady`   | `EventEmitter<Builder>` | Yes      | Invoked when the builder is ready to use ([more information](../interfaces/IBuilderProperties.mdx#onReady)).       |
| `onOpen`    | `EventEmitter<Builder>` | Yes      | Invoked when the builder is opened ([more information](../interfaces/IBuilderProperties.mdx#onOpen)).              |
| `onSave`    | `EventEmitter<Builder>` | Yes      | Invoked when the form definition is saved ([more information](../interfaces/IBuilderProperties.mdx#onSave)).       |
| `onChange`  | `EventEmitter<Builder>` | Yes      | Invoked when the form definition is changed ([more information](../interfaces/IBuilderProperties.mdx#onChange)).   |
| `onPreview` | `EventEmitter<Builder>` | Yes      | Invoked when a definition can be previewed ([more information](../interfaces/IBuilderProperties.mdx#onPreview)).   |
| `onEdit`    | `EventEmitter<Builder>` | Yes      | Invoked when the edit panel is opened or closed ([more information](../interfaces/IBuilderProperties.mdx#onEdit)). |
| `onClose`   | `EventEmitter<Builder>` | Yes      | Invoked when the builder is closed ([more information](../interfaces/IBuilderProperties.mdx#onClose)).             |

### 🗃️ Fields {#fields}
| Name         | Type                                | Description                                                               |
|:-------------|:------------------------------------|:--------------------------------------------------------------------------|
| `controller` | [`Builder`](../classes/Builder.mdx) | Contains a reference to the [`Builder`](../classes/Builder.mdx) instance. |

### ▶️ Methods {#methods}
| Name           | Description                                                                                                          |
|:---------------|:---------------------------------------------------------------------------------------------------------------------|
| `l10nEditor`   | Invokes the l10n editor for the supplied l10n contract ([more information](../classes/Builder.mdx#l10nEditor)).      |
| `stylesEditor` | Invokes the styles editor for the supplied styles contract([more information](../classes/Builder.mdx#stylesEditor)). |

### 👩‍💻 Example {#example}
<Tabs>
<TabItem value="app-module" label="App module">

```ts showLineNumbers title="app.module.ts"
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppComponent } from "./app.component";
//highlight-start
import { TripettoBuilderModule } from "@tripetto/builder/angular";
//highlight-end

@NgModule({
  declarations: [AppComponent],
  //highlight-start
  imports: [BrowserModule, TripettoBuilderModule],
  //highlight-end
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
```

</TabItem>
<TabItem value="app-html" label="App HTML">

```html showLineNumbers title="app.component.html"
<tripetto-builder (onSave)="formSaved($event)"></tripetto-builder>
```

</TabItem>
<TabItem value="app-component" label="App component">

```ts showLineNumbers title="app.component.ts"
import { Component } from "@angular/core";
//highlight-start
import { IDefinition } from "@tripetto/builder";
//highlight-end

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  //highlight-start
  formSaved(definition: IDefinition) {
    // Do something with the form definition
    console.log(definition);
  }
  //highlight-end
}
```

</TabItem>
</Tabs>

[![Run](/img/button-run.svg)](https://6lc5e.csb.app/) [![Try on CodeSandbox](/img/button-codesandbox.svg)](https://codesandbox.io/s/tripetto-sdk-builder-angular-basic-implementation-6lc5e)
