---
title: ISection interface - Builder
sidebar_label: ISection
description: Interface that describes a section within the form definition.
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# ISection interface

## 📖 Description {#description}
Interface that describes a section within the [form definition](IDefinition.mdx).

:::tip
See the [form definition interface](IDefinition.mdx) for more information about sections.
:::

## 📃 Type declaration {#type-declaration}
<InterfaceDeclaration prefix="#" src={`interface ISection {
  readonly id: string;
  readonly type?: "nest" | "branch";
  readonly name?: string;
  readonly nodes?: INode[];
  readonly branches?: IBranch[];
  readonly reference?: string;
  readonly version?: string;
  readonly alias?: string;
  readonly readonly?: true;
  readonly randomize?: boolean;
  readonly bookmark?: boolean;
  readonly bookmarkName?: string;
  readonly bookmarkDescription?: string;
  readonly bookmarkLevel?: number;
}`} symbols={{
  "INode": "/builder/api/interfaces/INode/",
  "IBranch": "/builder/api/interfaces/IBranch/"
}} />

## 🗃️ Properties {#properties}

---
### 🏷️ `alias` {#alias}
Contains an alias for the section or subform.
#### Type {#signature}
string

---
### 🏷️ `bookmark` {#bookmark}
Marks a section as a bookmark (see the [Bookmark guide](../../integrate/guides/bookmarks.mdx) for more information about using bookmarks in Tripetto).
#### Type {#signature}
boolean

---
### 🏷️ `bookmarkDescription` {#bookmarkDescription}
Contains a description for the bookmark (see the [Bookmark guide](../../integrate/guides/bookmarks.mdx) for more information about using bookmarks in Tripetto).
#### Type {#signature}
string

---
### 🏷️ `bookmarkLevel` {#bookmarkLevel}
Contains the indentation level of the bookmark (see the [Bookmark guide](../../integrate/guides/bookmarks.mdx) for more information about using bookmarks in Tripetto).
#### Type {#signature}
number

---
### 🏷️ `bookmarkName` {#bookmarkName}
Contains the name for the bookmark (see the [Bookmark guide](../../integrate/guides/bookmarks.mdx) for more information about using bookmarks in Tripetto).
#### Type {#signature}
boolean

---
### 🏷️ `branches` {#branches}
Contains the branches of the section.
#### Type {#signature}
[`IBranch[]`](IBranch.mdx)

---
### 🏷️ `id` {#id}
Specifies the unique identifier of the section.
#### Type {#signature}
string

---
### 🏷️ `name` {#name}
Specifies the name of the section.
#### Type {#signature}
string

---
### 🏷️ `nodes` {#nodes}
Contains the nodes of the section.
#### Type {#signature}
[`INode[]`](INode.mdx)

---
### 🏷️ `randomize` {#randomize}
Specifies if the nodes of this section needs to be randomized (using [Fisher-Yates shuffle](https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle)).
#### Type {#signature}
boolean

---
### 🏷️ `readonly` {#readonly}
Specifies if the nested branch (subform) is read-only. In that case, the subform cannot be edited in the builder.
#### Type {#signature}
`true`

:::caution
Only define this property when the nested branch is read-only. If it's not read-only, omit the property.
:::

---
### 🏷️ `reference` {#reference}
Specifies a reference for a section that is a nested branch (subform). This property can be used to track the origin of a subform when it was loaded from an external source. For example, the identifier of a subform can be stored in this property.
#### Type {#signature}
string

:::tip
See the [Subforms guide](../../integrate/guides/subforms.mdx) for more information.
:::

---
### 🏷️ `type` {#type}
Specifies the section type. Can be one of the following values:
- `nest`: This section forms a nested branch (subform);
- `branch`: This section is used to construct a branch (the section cannot contain any nodes).
#### Type {#signature}
"nest" | "branch"
:::caution
For regular sections, this property should be omitted. It is only there if a section forms a nest or is a branch.
:::

---
### 🏷️ `version` {#version}
Specifies a version indicator for a section that is a nested branch (subform). This property can be used to track the origin of a subform when it was loaded from an external source. For example, the version number of a subform can be stored in this property.
#### Type {#signature}
string

:::tip
See the [Subforms guide](../../integrate/guides/subforms.mdx) for more information.
:::
