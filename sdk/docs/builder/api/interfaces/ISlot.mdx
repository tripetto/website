---
title: ISlot interface - Builder
sidebar_label: ISlot
description: Interface that describes a slot within the form definition.
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# ISlot interface

## 📖 Description {#description}
Interface that describes a slot within the [form definition](IDefinition.mdx). Slots serve as the data transporters for the form. Each node block defines the slots required for the data collection of that block.

## 📃 Type declaration {#type-declaration}
:::info
Besides the listed properties, slots may store additional properties in the slot object.
:::
<InterfaceDeclaration prefix="#" src={`interface ISlot<T> {
  readonly id: string;
  readonly type: string;
  readonly kind: "static" | "dynamic" | "feature" | "meta";
  readonly reference: string;
  readonly sequence?: number;
  readonly label?: string;
  readonly name?: string;
  readonly placeholder?: string;
  readonly alias?: string;
  readonly required?: boolean;
  readonly pipeable?: boolean | {
    pipe?: string;
    label?: string;
    alias?: string;
    content?: "value" "label" | "name" | {
      string: string;
      text?: string;
      markdown?: string;
    };
  };
  readonly default?: T;
  readonly exportable?: boolean;
  readonly actionable?: boolean;
  readonly protected?: boolean;
}`} />

## 🗃️ Properties {#properties}

---
### 🏷️ `actionable` {#actionable}
Specifies if the slot is actionable. Actionable slots are special slots used to perform certain actions.
#### Type {#signature}
boolean

:::tip
More information about [post-processing actions](../../../blocks/custom/guides/post-processing.mdx) and the difference between [exportable and actionable data](../../../runner/stock/guides/collecting.mdx#exportable-vs-actionable).
:::

---
### 🏷️ `alias` {#alias}
Specifies the alias for the slot. Aliases are used in the dataset to identify a certain value.
#### Type {#signature}
string

---
### 🏷️ `default` {#default}
Specifies the default value for the slot.
#### Type {#signature}
`T` (depends on the slot type)

---
### 🏷️ `exportable` {#exportable}
Specifies if the slot is exportable. Exportable slots are part of the dataset that contains all the collected values of a form.
#### Type {#signature}
boolean

:::tip
More information about [exportable data](../../../runner/stock/guides/collecting.mdx#exportable-vs-actionable).
:::

---
### 🏷️ `id` {#id}
Specifies the unique identifier of the slot.
#### Type {#signature}
string

---
### 🏷️ `kind` {#kind}
Specifies the kind of slot. It can be one of the following values:
- `static`: Static slots defined by the node block (for example, a slot to hold the value of a text input block);
- `dynamic`: Slots defined for dynamic items of a node block (for example, slots for multiple-choice options);
- `feature`: Slots defined for certain features of a node block (for example, a slot to hold a score when the score feature of a node block is activated);
- `meta`: Slots that hold meta-data for a node block (for example, a slot that holds the time taken for answering a node block).
#### Type {#signature}
"static" | "dynamic" | "feature" | "meta"

---
### 🏷️ `label` {#label}
Specifies a human readable label for the slot.
#### Type {#signature}
string

---
### 🏷️ `name` {#name}
Specifies the slot name.
#### Type {#signature}
string

---
### 🏷️ `pipeable` {#pipeable}
Specifies if the slot is pipeable. Pipeable slots can be used/referenced by other node blocks. It is used to pass information of a block to other blocks, for example, when asking the form respondent for its name and then use that name elsewhere in the form.

:::info
To simply enable or disable piping for the slot (based on the slot value), supply a boolean value. If you need more control over the pipe, you can supply an object with a more specific configuration.
:::
#### Type {#signature}
```ts
boolean | {
  /* Optional name for the pipe. This is used to group slot values that have the same pipe name. */
  pipe?: string;

  /* Optional localized label for the pipe. */
  label?: string;

  /* Optional alias for the pipe. */
  alias?: string;

  /*
   * Specifies the field or content that should be used as the data that goes
   * into the pipe. It can be one of the following values:
   * - `value`: Use the current string value of the slot (this is the default behavior);
   * - `label`: Use the slot label;
   * - `name`: Use the name of the slot;
   * - Custom configuration to supply the data that goes into the pipe.
   */
  content?: "value" | "label" | "name" | {
    /* Contains the content as a string without any markup or variables. */
    string: string;

    /* Contains the content as text with support for variables. */
    text?: string;

    /* Contains markdown content with support for basic formatting, hyperlinks, and variables. */
    markdown?: string;
  };

  /*
   * Specifies the name of a legacy pipe. Only here for backward compatibility. Do not use.
   * @deprecated
   */
  legacy?: string; 🗑️
}
```

---
### 🏷️ `placeholder` {#placeholder}
Specifies the slot placeholder.
#### Type {#signature}
string

---
### 🏷️ `protected` {#protected}
Specifies whether the slot is write-protected and can only be changed by the block that created the slot. Other blocks in the form (like the [Setter block](../../../blocks/stock/setter.mdx)) cannot change the data of the slot.
#### Type {#signature}
boolean

---
### 🏷️ `reference` {#reference}
Contains the slot reference, which is a unique identifier for the slot within the node block. It is used by node blocks to retrieve slots based on the unique reference.
#### Type {#signature}
string

---
### 🏷️ `required` {#required}
Specifies if the slot is required and needs a value before a form can complete.
#### Type {#signature}
boolean
:::caution
The Runner library will automatically validate if all required slots have a valid value.
:::

---
### 🏷️ `sequence` {#sequence}
Specifies the sequence of a slot (used to order the slots).
#### Type {#signature}
number

---
### 🏷️ `type` {#type}
Specifies the slot type identifier.
#### Type {#signature}
string
