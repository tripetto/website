---
title: IBranch interface - Builder
sidebar_label: IBranch
description: Interface that describes a branch within the form definition.
---

import InterfaceDeclaration from '@site/src/components/interface.js';

# IBranch interface

## 📖 Description {#description}
Interface that describes a branch within the [form definition](IDefinition.mdx).

:::tip
See the [form definition interface](IDefinition.mdx) for more information about branches.
:::

## 📃 Type declaration {#type-declaration}
<InterfaceDeclaration prefix="#" src={`interface IBranch {
  readonly id: string;
  readonly name?: string;
  readonly conditions?: ICondition[];
  readonly sections?: ISection[];
  readonly culling?: "first" | "each" | "all" | "none";
  readonly jump?: "abort" | string;
  readonly epilogue?: IEpilogue;
}`} symbols={{
  "ICondition": "/builder/api/interfaces/ICondition/",
  "ISection": "/builder/api/interfaces/ISection/",
  "IEpilogue": "/builder/api/interfaces/IEpilogue/"
}} />

## 🗃️ Properties {#properties}

---
### 🏷️ `sections` ♾️ {#sections}
Contains the sections of the branch.
#### Type
[`ISection[]`](ISection.mdx)

:::info
This property is where the form definition gets its recursive behavior.
:::

---
### 🏷️ `conditions` {#conditions}
Contains the conditions of the branch.
#### Type
[`ICondition[]`](ICondition.mdx)

---
### 🏷️ `culling` {#culling}
Specifies the culling mode of the branch. This culling mode defines when the branch is taken. It can be one of the following values:
- `first`: Branch is taken when one or more of the conditions match;
- `each`: Branch is taken for each condition that matches (this allows a single branch to be taken multiple times);
- `all`: Branch is taken when all of the conditions match;
- `none`: Branch is taken when none of the conditions match.
#### Type
"first" | "each" | "all" | "none"

:::info
When a branch doesn't contain any conditions, it will only be taken when the culling mode is `first` or `all`.
:::

---
### 🏷️ `epilogue` {#epilogue}
Contains the optional epilogue for the branch. This epilogue can be used by runners (that support it) to show a closing message when the branch ends.
#### Type
[`IEpilogue`](IEpilogue.mdx)

:::info
The branch epilogue is only available when the [`jump`](#jump) is set to `abort`.
:::

---
### 🏷️ `id` {#id}
Specifies the unique identifier of the branch.
#### Type
string

---
### 🏷️ `jump` {#jump}
Specifies if a branch should jump to another section or abort (terminate) the whole form. When a jump to a section is desired, this property should contain the [`id`](ISection.mdx#id) of the section to jump to. When the branch ends, the system jumps to the specified section and continues its operation from there. When this property is set to `abort`, the form will (completes) as soon as the branch ends. It is possible to define a specific epilogue for a branch that aborts using the [`epilogue`](#epilogue) property.
#### Type
"abort" | string;

:::info
Branches can only jump forward (in time). Any jump to a section that lies before the branch (back in time) will be ignored.
:::

---
### 🏷️ `name` {#name}
Specifies the name of the branch.
#### Type
string
