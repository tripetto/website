---
title: Builder class - Builder
sidebar_label: Builder
description: The builder class implements the Tripetto form builder.
---

# Builder class

## 📖 Description {#description}
The builder class implements the Tripetto form builder. You can create a new builder instance using the static [`open`](#static-open) function or by using the [`new`](#constructor) keyword.

---
## 🆕 `constructor` {#constructor}
Creates a new builder instance with the specified builder properties.
#### Signature
```ts
constructor(properties?: IBuilderProperties): Builder
```
#### Parameters
| Name         | Type                                                         | Optional | Description                                                                   |
|:-------------|:-------------------------------------------------------------|:---------|:------------------------------------------------------------------------------|
| `properties` | [`IBuilderProperties`](../interfaces/IBuilderProperties.mdx) | Yes      | Specifies the [builder properties](../interfaces/IBuilderProperties.mdx).     |
#### Return value
Returns a reference to the new builder instance.
#### Example
```ts showLineNumbers
import { Builder } from "@tripetto/builder";

const builder = new Builder();
```

## 📌 Statics {#statics}

---
### 🔧 `open` {#static-open}
Creates a new builder instance for the specified [form definition](../interfaces/IDefinition.mdx) and with the specified properties.
#### Signature
```ts
open(definition?: IDefinition, properties?: IBuilderProperties): Builder
```
#### Parameters
| Name         | Type                                                         | Optional | Description                                                                   |
|:-------------|:-------------------------------------------------------------|:---------|:------------------------------------------------------------------------------|
| `definition` | [`IDefinition`](../interfaces/IDefinition.mdx)               | Yes      | Specifies the form definition to load (when omitted an empty form is loaded.) |
| `properties` | [`IBuilderProperties`](../interfaces/IBuilderProperties.mdx) | Yes      | Specifies the [builder properties](../interfaces/IBuilderProperties.mdx).     |
#### Return value
Returns a reference to the builder instance.
#### Example
```ts showLineNumbers
Builder.open();
```

## 🗃️ Fields {#fields}

---
### 🏷️ `definition` {#definition}
Sets or retrieves the form definition.
#### Type
[`IDefinition`](../interfaces/IDefinition.mdx)

---
### 🏷️ `language` {#language}
Sets or retrieves the language (ISO code) of the form definition.
#### Type
string | undefined

---
### 🏷️ `name` {#name}
Sets or retrieves the name of the form definition.
#### Type
string

## ▶️ Methods {#methods}

---
### 🔧 `clear` {#clear}
Clears the current form (effectively creating a new empty one).
#### Signature
```ts
clear(): Builder
```
#### Return value
Returns a reference to the builder instance.

---
### 🔧 `close` {#close}
Closes the builder.
#### Signature
```ts
close(): Builder
```
#### Return value
Returns a reference to the builder instance.

---
### 🔧 `edit` {#edit}
Opens the editor panel for the specified form element, allowing to open certain elements in the builder from outside of the builder. A typical use case for this method is clicking on a block in a live preview to edit that block in the builder.
#### Signature
```ts
edit(
  type: "prologue" | "properties" | "branch" | "section" | "node" | "condition" | "epilogue",
  id: string
): this;
```
#### Parameters
| Name   | Type                                                                                       | Optional | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
|:-------|:-------------------------------------------------------------------------------------------|:---------|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `type` | "prologue" \| "properties" \| "branch" \| "section" \| "node" \| "condition" \| "epilogue" | No       | Specifies the type of element to open. It can be one of the following values:<br/>- `prologue`: Opens the prologue editor panel (the prologue is the welcome message that is shown before a form is filled in);<br/>- `properties`: Opens the form properties editor panel to edit the name, description, language, and keywords of the form;<br/>- `branch`: Opens the properties editor panel for the branch with the specified identifier;<br/>- `section`: Opens the properties editor panel for the section with the specified identifier;<br/>- `node`: Opens the properties editor panel for the node with the specified identifier;<br/>- `condition`: Opens the properties editor panel for the condition with the specified identifier;<br/>- `epilogue`: Opens the epilogue editor panel for a certain branch (when the identifier of the branch is specified) or the root branch (when the identifier is left empty). The epilogue message is shown when a form completes. |
| `id`   | string                                                                                     | Yes      | Specifies the identifier of the element to open (required when type is `branch`, `section`, `node`, `condition`, and optionally for `epilogue`).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
#### Return value
Returns a reference to the builder instance.

---
### 🔧 `hook` {#hook}
Registers a hook for listening to a certain event. You can create multiple hooks for the same event.
#### Signature
```ts
hook(
  event: "OnLoad" | "OnError" | "OnReady" | "OnOpen" | "OnSave" | "OnChange" | "OnPreview" | "OnRename" | "OnEdit" | "OnClose",
  type: "synchronous" | "asynchronous" | "framed",
  callback: (payload) => void,
  context?: Function | {}
): Builder
```
#### Parameters
| Name       | Type                                                                                                                           | Optional | Description                                                                                                                                                                                                                                                           |
|:-----------|:-------------------------------------------------------------------------------------------------------------------------------|:---------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `event`    | "OnLoad" \| "OnError" \| "OnReady" \| "OnOpen" \| "OnSave" \| "OnChange" \| "OnPreview" \| "OnRename" \| "OnEdit" \| "OnClose" | No       | Specifies the [event](#events) to hook on to.                                                                                                                                                                                                                                    |
| `type`     | "synchronous" \| "asynchronous" \| "framed"                                                                                    | No       | Specifies the callback type. It can be one of the following values:<br/>- `synchronous`: Call the callback in a synchronous call;<br/>- `asynchronous`: Call the callback using a asynchronous call;<br/>- `framed`: Call the callback in the next browser animation frame. |
| `callback` | (payload) => void                                                                                                              | No       | Specifies the callback function to invoke. The supplied function receives the hook payload as its first argument. The structure of this payload depends on the event hooked to.                                                                                       |
| `context`  | Function \| `{}`                                                                                                               | Yes      | Specifies a context to bind to. This context can be used to unhook specific hooks later on.                                                                                                                                                                           |
##### Events {#hook-events}
| Name        | Description                                | Payload                                                        |
|:------------|:-------------------------------------------|:---------------------------------------------------------------|
| `OnLoad`    | Builder is loading.                        | [IBuilderLoadEvent](../interfaces/IBuilderLoadEvent.mdx)       |
| `OnError`   | Builder generated an error during loading. | [IBuilderErrorEvent](../interfaces/IBuilderErrorEvent.mdx)     |
| `OnReady`   | Builder is ready loading.                  | [IBuilderReadyEvent](../interfaces/IBuilderReadyEvent.mdx)     |
| `OnOpen`    | Builder is opened.                         | [IBuilderOpenEvent](../interfaces/IBuilderOpenEvent.mdx)       |
| `OnSave`    | Form definition is saved.                  | [IBuilderSaveEvent](../interfaces/IBuilderSaveEvent.mdx)       |
| `OnChange`  | Form definition has changed.               | [IBuilderChangeEvent](../interfaces/IBuilderChangeEvent.mdx)   |
| `OnPreview` | Form definition preview should update.     | [IBuilderPreviewEvent](../interfaces/IBuilderPreviewEvent.mdx) |
| `OnRename`  | Form is renamed.                           | [IBuilderRenameEvent](../interfaces/IBuilderRenameEvent.mdx)   |
| `OnEdit`    | Editor panel is opened or closed.          | [IBuilderEditEvent](../interfaces/IBuilderEditEvent.mdx)       |
| `OnClose`   | Builder is closed.                         | [IBuilderCloseEvent](../interfaces/IBuilderCloseEvent.mdx)     |
#### Return value
Returns a reference to the builder instance.
#### Example
```ts showLineNumbers
import { Builder } from "@tripetto/builder";

const builder = new Builder();

builder.hook("OnLoad", "synchronous", (event: IBuilderLoadEvent) => {
  console.log("This builder is loaded!");
});
```
:::info
The `OnLoad` event is always invoked before the `OnReady` and `OnOpen` events. The `OnReady` event is invoked as soon as the loading completes (builder is ready to show). After that, the `onOpen` event is emitted when the builder is opened and shown to the user.
:::

---
### 🔧 `l10nEditor` {#l10nEditor}
Invokes the l10n editor for the supplied l10n contract. The l10n editor is used to manage l10n related settings and translations.
#### Signature
```ts
l10nEditor(
  contract: TL10nContract,
  l10n?: IL10n | (() => { l10n: IL10n }),
  onChange?: (l10n: IL10n, currentL10n: IL10n) => void,
  onTranslation?: (language: string) => TTranslation | TTranslation[] | Promise<TTranslation | TTranslation[] | undefined> | undefined
): L10nEditor
```
#### Parameters
| Name            | Type                                                                                                                                                        | Optional | Description                                                                                                                                                                                                                                                      |
|:----------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------|:---------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `contract`      | `TL10nContract`                                                                                                                                             | No       | Specifies the l10n contract to use.                                                                                                                                                                                                                              |
| `l10n`          | [`IL10n \| (() => { l10n: IL10n })`](../../../runner/api/library/interfaces/IL10n.mdx)                                                                   | Yes      | Reference to the l10n object to edit (if omitted a new object is created). You can also supply a function that returns an object that has a `l10n` property. This `l10n` property is then used as input object, and automatically updated when changes are made. |
| `onChange`      | (l10n: [`IL10n`](../../../runner/api/library/interfaces/IL10n.mdx), currentL10n: [`IL10n`](../../../runner/api/library/interfaces/IL10n.mdx)) => void | Yes      | Specifies a function that is invoked on each change. It receives two arguments:<br/>- `l10n`: Reference to the l10n object;<br/>- `currentL10n`: Copy of the l10n object where the current edit language is set as the default language.                         |
| `onTranslation` | (language: string) => [`TTranslation`](../../../blocks/api/modules/L10n/index.mdx#TTranslation)                                                             | Yes      | Specifies a function that should deliver translations for the given language.                                                                                                                                                                                    |
#### Return value
Returns a reference to the `L10nEditor` instance.

---
### 🔧 `load` {#load}
Loads a form definition.
#### Signature
```ts
load(definition: IDefinition): Builder
```
#### Parameters
| Name         | Type                                           | Optional | Description                            |
|:-------------|:-----------------------------------------------|:---------|:---------------------------------------|
| `definition` | [`IDefinition`](../interfaces/IDefinition.mdx) | No       | Specifies the form definition to load. |
#### Return value
Returns a reference to the builder instance.

---
### 🔧 `open` {#open}
Opens the builder with the optional supplied form definition.
#### Signature
```ts
open(definition?: IDefinition): Builder
```
#### Parameters
| Name         | Type                                           | Optional | Description                            |
|:-------------|:-----------------------------------------------|:---------|:---------------------------------------|
| `definition` | [`IDefinition`](../interfaces/IDefinition.mdx) | Yes      | Specifies the form definition to load. |
#### Return value
Returns a reference to the builder instance.
#### Example
```ts showLineNumbers
import { Builder } from "@tripetto/builder";

const builder = new Builder();

builder.open();
```

---
### 🔧 `reload` {#reload}
Reloads the definition, resulting in a complete re-rendering of the form structure in the builder.
#### Signature
```ts
reload(): Builder
```
#### Return value
Returns a reference to the builder instance.

---
### 🔧 `resize` {#resize}
Notifies the builder about a parent element resize. It is important to notify the builder when the dimensions of the viewport of the builder have changed. The builder is not able to detect this by itself. So it needs your help. See the example code below.
:::tip
Read the [Handling viewport/screen resizing guide](../../integrate/guides/resize.mdx) for more information.
:::
#### Signature
```ts
resize(): Builder
```
#### Return value
Returns a reference to the builder instance.
#### Example
```ts showLineNumbers
import { Builder } from "@tripetto/builder";

const builder = new Builder();

// Upon window resize notify the builder
window.addEventListener("resize", () => builder.resize());
window.addEventListener("orientationchange", () => builder.resize());
```
:::info
There is no need to debounce calls to the `resize` method. This is already done inside the builder.
:::

---
### 🔧 `restore` {#restore}
Restores the last loaded or saved definition. When a definition is saved, that version becomes the last loaded one.
#### Signature
```ts
restore(): Builder
```
#### Return value
Returns a reference to the builder instance.

---
### 🔧 `save` {#save}
Saves the [form definition](../interfaces/IDefinition.mdx) and invokes the `onSave` event and emits the `OnSave` [hook](#hook).
:::tip
Read the [Saving form definitions guide](../../integrate/guides/saving.mdx#save-button) for more information.
:::
#### Signature
```ts
save(): IDefinition | undefined
```
#### Return value
Returns the current form definition.
:::warning
This value can be `undefined` when calling the `save` method before the builder is ready loading.
:::

---
### 🔧 `stylesEditor` {#stylesEditor}
Invokes the styles editor for the supplied styles contract. The styles editor is used to manage the styles described by the styles contract.
#### Signature
```ts
stylesEditor(
  contract: TStylesContract,
  styles?: TStyles | (() => { styles: TStyles }),
  tier?: "unlicensed" | "licensed",
  onChange?: (styles: TStyles) => void,
  hideInaccessibleOptions: boolean,
  allowReset: boolean,
  onReset?: (fnDone: (bReset: boolean) => void) => void
): StylesEditor
```
#### Parameters
| Name                      | Type                                     | Optional | Description                                                                                                                                                                                                                                                            |
|:--------------------------|:-----------------------------------------|:---------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `contract`                | `TStylesContract`                        | No       | Specifies the styles contract to use.                                                                                                                                                                                                                                  |
| `styles`                  | `TStyles \| (() => { styles: TStyles })` | Yes      | Reference to the styles object to edit (if omitted a new object is created). You can also supply a function that returns an object that has a `styles` property. This `styles` property is then used as input object, and automatically updated when changes are made. |
| `tier`                    | "unlicensed" \| "licensed"               | Yes      | Specifies the features tier.                                                                                                                                                                                                                                           |
| `onChange`                | (styles: TStyles) => void`               | Yes      | Specifies a function that is invoked on each style change.                                                                                                                                                                                                             |
| `hideInaccessibleOptions` | boolean                                  | Yes      | Specifies if options that aren't available should be invisible instead of disabled (default is `false`).                                                                                                                                                               |
| `allowReset`              | boolean                                  | Yes      | Allows a reset button to reset all styles (default is `true`).                                                                                                                                                                                                         |
| `onReset`                 | (done: (reset: boolean) => void) => void | Yes      | Specifies a function that is invoked when the styles are about to reset. This function can be used to show a confirmation dialog to the user.                                                                                                                          |
#### Return value
Returns a reference to the `StylesEditor` instance.

---
### 🔧 `tutorial` {#tutorial}
Shows the tutorial dialog with information about the available gestures for the builder.
#### Signature
```ts
tutorial(): Builder
```
#### Return value
Returns a reference to the builder instance.

---
### 🔧 `unhook` {#unhook}
Removes a hook for a certain event.
#### Signature
```ts
unhook(
  event: "OnLoad" | "OnError" | "OnReady" | "OnOpen" | "OnSave" | "OnChange" | "OnPreview" | "OnRename" | "OnEdit" | "OnClose",
  context?: Function | {}
): boolean
```
#### Parameters
| Name      | Type                                                                                                                           | Optional | Description                                                                                    |
|:----------|:-------------------------------------------------------------------------------------------------------------------------------|:---------|:-----------------------------------------------------------------------------------------------|
| `event`   | "OnLoad" \| "OnError" \| "OnReady" \| "OnOpen" \| "OnSave" \| "OnChange" \| "OnPreview" \| "OnRename" \| "OnEdit" \| "OnClose" | No       | Specifies the event to unhook.                                                                 |
| `context` | Function \| `{}`                                                                                                               | Yes      | Specifies a context to unhook. If not specified all hooks for the supplied event are unhooked. |
#### Return value
Returns `true` if a hook for the event was found and thus removed.

---
### 🔧 `useNamespace` {#useNamespace}
Switches to the blocks namespace with the specified identifier and optionally load it if it is not available. Namespaces allow the use of different sets of blocks within in a single builder instance. This is useful when a single builder instance is used for editing form definitions that can be used in different runners and when the user can toggle between those runners. With the help of this method it is possible to load different sets of blocks for different runners and then switch between those block sets at runtime.
:::tip
You can also load a namespace during builder instance construction using the [`namespace`](../interfaces/IBuilderProperties.mdx#namespace) property.
:::
#### Signature
```ts
useNamespace(
  identifier: string,
  src?: string,
  type?: "umd" | "url",
  alwaysLoad: boolean,
  unloadFirst: boolean
): Promise
```
#### Parameters
| Name          | Type           | Optional | Description                                                                                                                                     |
|:--------------|:---------------|:---------|:------------------------------------------------------------------------------------------------------------------------------------------------|
| `identifier`  | string         | No       | Specifies the namespace identifier.                                                                                                             |
| `src`         | string         | Yes      | Specifies the UMD code or URL to load.                                                                                                          |
| `type`        | "umd" \| "url" | Yes      | Specifies what to load (UMD code or an URL).                                                                                                    |
| `alwaysLoad`  | boolean        | Yes      | Specifies if the bundle should always load, even when there is already a namespace available with the specified identifier (default is `true`). |
| `unloadFirst` | boolean        | Yes      | Specifies if the namespace should unload if it is already available (default is `false`).                                                       |
#### Return value
Returns a promise that resolves when the namespace loading has succeeded or rejects when it failed.
#### Example
```ts showLineNumbers
import { Builder } from "@tripetto/builder";

const builder = new Builder();

// Load the builder blocks for the autoscroll runner
builder
  .useNamespace("autoscroll", "https://unpkg.com/@tripetto/runner-autoscroll/builder", "url")
  .then(() => {
    console.log("Loading succeeded!");
  })
  .catch(() => {
    console.log("Loading failed!");
  });
```
:::warning
If your application implements a [Content Security Policy (CSP)](https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP) and prohibits the use of `eval()`, you should always load namespace block bundles using the `url` type. This method uses a Trusted Type policy named `tripetto#loader` for loading the bundle, so make sure to whitelist that Trusted Type. More on CSP and Trusted Types can be found in this [guide](../../integrate/guides/csp.mdx).
:::

## 📢 Events {#events}

---
### 🔔 `onChange` {#onChange}
Invoked when the definition is changed.
:::tip
If you need live (or real-time) saving of form definitions, you should use this event. Read the [Saving form definitions guide](../../integrate/guides/saving.mdx#save-on-change) for more information.
:::
#### Signature
```ts
(definition: IDefinition, builder: Builder) => void | Promise<void>
```
#### Parameters
| Name         | Type                                           | Optional | Description                           |
|:-------------|:-----------------------------------------------|:---------|:--------------------------------------|
| `definition` | [`IDefinition`](../interfaces/IDefinition.mdx) | No       | Contains the changed form definition. |
| `builder`    | `Builder`                                      | No       | Reference to the builder instance.    |
#### Return value
When a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) is returned the builder goes into a loading state until the promise resolves or rejects.
#### Example
```ts showLineNumbers
import { Builder } from "@tripetto/builder";

const builder = new Builder();

builder.onChange = (definition) => {
  console.log("Form definition has changed!");
};
```
:::tip
You can also use the [`hook`](#hook) method to hook into this event.
:::

---
### 🔔 `onClose` {#onClose}
Invoked when the builder is closed.
#### Signature
```ts
(builder: Builder) => void
```
#### Parameters
| Name      | Type      | Optional | Description                        |
|:----------|:----------|:---------|:-----------------------------------|
| `builder` | `Builder` | No       | Reference to the builder instance. |
:::tip
You can also use the [`hook`](#hook) method to hook into this event.
:::

---
### 🔔 `onError` {#onError}
Invoked when there was an error during loading a form definition. This event not only indicates that there was an error, it also gives some details about the number of errors found (that's the number of blocks that could not load) and a list of blocks types that are missing (and the reason for the error).
#### Signature
```ts
(erroneousBlocks: number, missingBlockTypes: string[], builder: Builder) => void
```
#### Parameters
| Name                | Type      | Optional | Description                                                   |
|:--------------------|:----------|:---------|:--------------------------------------------------------------|
| `erroneousBlocks`   | number    | No       | Contains the number of blocks that could not load.            |
| `missingBlockTypes` | string[]  | No       | Contains an array of block type identifiers that are missing. |
| `builder`           | `Builder` | No       | Reference to the builder instance.                            |
:::tip
You can also use the [`hook`](#hook) method to hook into this event.
:::

---
### 🔔 `onLoad` {#onLoad}
Invoked when the builder is loading.
#### Signature
```ts
(builder: Builder) => void
```
#### Parameters
| Name      | Type      | Optional | Description                        |
|:----------|:----------|:---------|:-----------------------------------|
| `builder` | `Builder` | No       | Reference to the builder instance. |
:::info
The `onLoad` event is invoked before the `onReady` and `onOpen` events.
:::
:::tip
You can also use the [`hook`](#hook) method to hook into this event.
:::

---
### 🔔 `onOpen` {#onOpen}
Invoked when the builder is opened and shown to the user.
#### Signature
```ts
(builder: Builder) => void
```
#### Parameters
| Name      | Type      | Optional | Description                        |
|:----------|:----------|:---------|:-----------------------------------|
| `builder` | `Builder` | No       | Reference to the builder instance. |
:::info
The `onOpen` event is always invoked after the `onLoad` and `onReady` events.
:::
:::tip
You can also use the [`hook`](#hook) method to hook into this event.
:::

---
### 🔔 `onReady` {#onReady}
Invoked when the builder is ready loading.
#### Signature
```ts
(builder: Builder) => void
```
#### Parameters
| Name      | Type      | Optional | Description                        |
|:----------|:----------|:---------|:-----------------------------------|
| `builder` | `Builder` | No       | Reference to the builder instance. |
:::info
The `onReady` event is invoked after the `onLoad` event, but before the `onOpen` event.
:::
:::tip
You can also use the [`hook`](#hook) method to hook into this event.
:::

---
### 🔔 `onSave` {#onSave}
Invoked when the definition is saved. This happens when the user clicks the save button in the Tripetto toolbar.
:::tip
It is possible to [disable the save button](../interfaces/IBuilderProperties.mdx#disableSaveButton) (or the whole toolbar). Then you can use the ['save'](#save) method to initiate a save action or use the [`onChange`](#onChange) event for real-time monitoring changes in the form definition.

Read the [Saving form definitions guide](../../integrate/guides/saving.mdx#save-button) for more information.
:::
#### Signature
```ts
(definition: IDefinition, builder: Builder) => void | Promise<void>
```
#### Parameters
| Name         | Type                                           | Optional | Description                           |
|:-------------|:-----------------------------------------------|:---------|:--------------------------------------|
| `definition` | [`IDefinition`](../interfaces/IDefinition.mdx) | No       | Contains the form definition to save. |
| `builder`    | `Builder`                                      | No       | Reference to the builder instance.    |
#### Return value
When a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise) is returned the builder goes into a loading state until the promise resolves or rejects.
#### Example
```ts showLineNumbers
import { Builder } from "@tripetto/builder";

const builder = new Builder();

builder.onSave = (definition) => {
  console.log("Save the form definition!");
};
```
:::tip
You can also use the [`hook`](#hook) method to hook into this event.
:::
