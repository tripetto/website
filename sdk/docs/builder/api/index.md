---
title: Builder API
description: This API reference contains all the builder-related exports of the Tripetto Builder package.
---

# ![](/img/logo-ts.svg) Builder API reference
This API reference contains all the builder-related exports of the [Tripetto Builder](https://www.npmjs.com/package/@tripetto/builder) package. These exports are used when integrating the builder into custom projects.

:::info
This package is also used when developing [custom blocks](../../blocks/introduction.md) for the builder. The API reference documentation for all exports related to developing custom blocks is located in the [blocks documentation](../../blocks/api/index.md).
:::

## 🚀 Quickstart {#quickstart}
[![Implement builder using plain JS](/img/button-js.svg)](../integrate/quickstart/plain-js/)
[![Implement builder using React](/img/button-react.svg)](../integrate/quickstart/react/)
[![Implement builder using Angular](/img/button-angular.svg)](../integrate/quickstart/angular/)
[![Implement builder using HTML](/img/button-html.svg)](../integrate/quickstart/html/)

## ✨ Installation [![](/img/logo-npm-icon.svg)](https://www.npmjs.com/package/@tripetto/builder) {#installation}

```bash npm2yarn
npm install @tripetto/builder
```

:::tip Typescript support
This package contains type declarations and supports [TypeScript](https://www.typescriptlang.org/) out-of-the-box.
:::

## ⚛️ Components {#components}
- [`Angular`](components/angular.mdx)
- [`React`](components/react.mdx)

## 💎 Classes {#classes}
- [`Builder`](classes/Builder.mdx)

## ▶️ Functions {#functions}
- [`listNestedDefinitions`](functions/listNestedDefinitions.mdx)
- [`mountNamespace`](functions/mountNamespace.mdx)
- [`unmountNamespace`](functions/unmountNamespace.mdx)
- [`updateDefinition`](functions/updateDefinition.mdx)

## 🗂️ Modules {#modules}
- [`Namespaces`](modules/Namespaces.mdx)
- [`Tripetto`](modules/Tripetto.mdx)

## ⛓️ Interfaces {#interfaces}
- [`IBranch`](interfaces/IBranch.mdx)
- [`IBuilderChangeEvent`](interfaces/IBuilderChangeEvent.mdx)
- [`IBuilderCloseEvent`](interfaces/IBuilderCloseEvent.mdx)
- [`IBuilderEditEvent`](interfaces/IBuilderEditEvent.mdx)
- [`IBuilderErrorEvent`](interfaces/IBuilderErrorEvent.mdx)
- [`IBuilderLoadEvent`](interfaces/IBuilderLoadEvent.mdx)
- [`IBuilderOpenEvent`](interfaces/IBuilderOpenEvent.mdx)
- [`IBuilderPreviewEvent`](interfaces/IBuilderPreviewEvent.mdx)
- [`IBuilderProperties`](interfaces/IBuilderProperties.mdx)
- [`IBuilderReadyEvent`](interfaces/IBuilderReadyEvent.mdx)
- [`IBuilderRenameEvent`](interfaces/IBuilderRenameEvent.mdx)
- [`IBuilderSaveEvent`](interfaces/IBuilderSaveEvent.mdx)
- [`IBuilderTier`](interfaces/IBuilderTier.mdx)
- [`ISection`](interfaces/ISection.mdx)
- [`ICondition`](interfaces/ICondition.mdx)
- [`IDefinition`](interfaces/IDefinition.mdx)
- [`IEpilogue`](interfaces/IEpilogue.mdx)
- [`INamespace`](interfaces/INamespace.mdx)
- [`INode`](interfaces/INode.mdx)
- [`IPrologue`](interfaces/IPrologue.mdx)
- [`ISlot`](interfaces/ISlot.mdx)

## 🗿 Constants {#constants}
- [`NAME`](constants/NAME.mdx)
- [`VERSION`](constants/VERSION.mdx)

## 📁 Source code [![](/img/logo-gitlab.svg)](https://gitlab.com/tripetto/builder) {#source}
The Tripetto Builder package code is on [GitLab](https://gitlab.com/tripetto/builder).
