---
title: Namespaces module - Builder
sidebar_label: Namespaces
description: The Namespaces module contains functions to load block bundles for the builder and functions to include or exclude certain blocks.
---

# Namespaces module

## 📖 Description {#description}
The `Namespaces` module contains functions to load block bundles for the builder and functions to [include or exclude certain blocks](../../integrate/guides/blocks.mdx#include-exclude). Tripetto allows the use of multiple namespaces for loading block bundles. This is useful when the builder is used to create forms that run in different runners, where each runner has its own set of blocks.

:::tip
To learn more about using namespaces, read the [Loading blocks guide](../../integrate/guides/blocks.mdx#loading-blocks-in-namespaces). And have a look at this [guide](../../integrate/guides/livepreview.mdx#preview-multiple-runners) to see how namespaces are used to create a live preview setup with multiple runners.
:::

:::caution
The preferred way to load block bundles is by using the [`namespace`](../interfaces/IBuilderProperties.mdx#namespace) property of the builder constructor or the [`useNamespace`](../classes/Builder.mdx#useNamespace) method of a builder instance.
:::

## 🗃️ Properties {#properties}

---
### 🏷️ `active` {#active}
Retrieves the active builder block namespace.
#### Type
[`INamespace`](../interfaces/INamespace.mdx)

---
### 🏷️ `erroneousBlocks` {#erroneousBlocks}
Retrieves the total number of erroneous blocks due to missing block types.
#### Type
number

---
### 🏷️ `missingBlockTypes` {#missingBlockTypes}
Retrieves the array of missing block types.
#### Type
string[]

## ▶️ Functions {#functions}

---
### 🔧 `activate` {#activate}
Activates the namespace with the specified identifier.
#### Signature
```ts
activate(identifier?: string): INamespace
```
#### Parameters
| Name         | Type   | Optional | Description                                            |
|:-------------|:-------|:---------|:-------------------------------------------------------|
| `identifier` | string | No       | Specifies the identifier of the namespace to activate. |
#### Return value
Returns a reference to the active [`INamespace`](../interfaces/INamespace.mdx) object.

---
### 🔧 `activateDefault` {#activateDefault}
Activates the default (built-in) namespace.
:::info
This default namespace is used when blocks are registered without mounting a specific namespace for them.
:::
#### Signature
```ts
activateDefault(): INamespace
```
#### Return value
Returns a reference to the active [`INamespace`](../interfaces/INamespace.mdx) object.

---
### 🔧 `activateOrLoadUMD` {#activateOrLoadUMD}
Activate a namespace for the specified identifier (or tries to load it if it is not available).
:::danger
Loading UMD code from a string is unsafe! Only use when the source of the UMD code is trusted. When you are using a [Content Security Policy (CSP)](https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP) configuration, make sure `script-src 'unsafe-eval'` is allowed.
:::
:::caution
The preferred way to load block bundles is by using the [`namespace`](../interfaces/IBuilderProperties.mdx#namespace) property of the builder constructor or the [`useNamespace`](../classes/Builder.mdx#useNamespace) method of a builder instance.
:::
#### Signature
```ts
activateOrLoadUMD(
  identifier: string,
  context: {},
  name: string,
  umd: string,
  alwaysLoad?: boolean,
  unload?: boolean
): INamespace | undefined
```
#### Parameters
| Name         | Type    | Optional | Description                                                                                                                                     |
|:-------------|:--------|:---------|:------------------------------------------------------------------------------------------------------------------------------------------------|
| `identifier` | string  | No       | Specifies the namespace identifier.                                                                                                             |
| `context`    | `{}`    | No       | Reference to a context to use (this context is made available under the supplied name to the UMD bundle that is loaded).                        |
| `name`       | string  | No       | Specifies the (to the UMD exposed) symbol name of the supplied context.                                                                         |
| `umd`        | string  | No       | Specifies the UMD code to load.                                                                                                                 |
| `alwaysLoad` | boolean | Yes      | Specifies if the bundle should always load, even when there is already a namespace available with the specified identifier (default is `true`). |
| `unload`     | boolean | Yes      | Specifies if the namespace should unload if it is already available (default is `false`).                                                       |
#### Return value
Returns a reference to the [`INamespace`](../interfaces/INamespace.mdx) object or `undefined` if the namespace was not loaded.

---
### 🔧 `activateOrLoadURL` {#activateOrLoadURL}
Activate a namespace for the specified identifier (or tries to load it if it is not available).
:::info
The UMD bundle is loaded using [trusted-types](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/trusted-types) with a policy name defined by the `policy` parameter.
:::
:::caution
The preferred way to load block bundles is by using the [`namespace`](../interfaces/IBuilderProperties.mdx#namespace) property of the builder constructor or the [`useNamespace`](../classes/Builder.mdx#useNamespace) method of a builder instance.
:::
#### Signature
```ts
activateOrLoadURL(
  identifier: string,
  context: {},
  name: string,
  policy: string,
  url: string,
  done?: (namespace: INamespace) => void,
  alwaysLoad?: boolean,
  unload?: boolean
): void
```
#### Parameters
| Name   | Type                                                                    | Optional | Description                                                                                                                                        |
|:-------|:------------------------------------------------------------------------|:---------|:---------------------------------------------------------------------------------------------------------------------------------------------------|
| `identifier` | string                                                            | No       | Specifies the namespace identifier.                                                                                                                |
| `context`    | `{}`                                                              | No       | Reference to a context to use (this context is made available under the supplied name to the UMD bundle that is loaded).                           |
| `name`       | string                                                            | No       | Specifies the (to the UMD exposed) symbol name of the supplied context.                                                                            |
| `policy`     | string                                                            | No       | Specifies the [trusted-types](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/trusted-types) policy name to use. |
| `url`        | string                                                            | No       | Specifies the URL of the UMD block bundle to load.                                                                                                 |
| `done`       | (namespace: [`INamespace`](../interfaces/INamespace.mdx)) => void | Yes      | Callback function that is invoked when the loading is completed.                                                                                   |
| `alwaysLoad` | boolean                                                           | Yes      | Specifies if the bundle should always load, even when there is already a namespace available with the specified identifier (default is `true`).    |
| `unload`     | boolean                                                           | Yes      | Specifies if the namespace should unload if it is already available (default is `false`).                                                           |

---
### 🔧 `exclude` {#exclude}
Specifies the blocks to exclude from the builder.
#### Signature
```ts
exclude(...blocks: string[]): void
```
#### Parameters
| Name     | Type     | Optional | Description                                               |
|:---------|:---------|:---------|:----------------------------------------------------------|
| `blocks` | string[] | No       | Specifies the block identifiers of the blocks to exclude. |
#### Example
```ts showLineNumbers
import { Namespaces } from "@tripetto/builder";

// Let's exclude the mailer block and the file upload block
Namespaces.exclude("@tripetto/block-mailer", "@tripetto/block-file-upload");
```

---
### 🔧 `flag` {#flag}
Applies a certain flag to the specified blocks.
:::tip
Flags can be verified using the [`flag`](../../../blocks/api/classes/NodeBlock.mdx#flag) method of a [`NodeBlock`](../../../blocks/api/classes/NodeBlock.mdx#flag) or [`ConditionBlock`](../../../blocks/api/classes/ConditionBlock.mdx#flag).
:::
#### Signature
```ts
flag(flag: string, ...types: string[]): void
```
#### Parameters
| Name    | Type     | Optional | Description                                                                    |
|:--------|:---------|:---------|:-------------------------------------------------------------------------------|
| `flag`  | string   | No       | Specifies the flag identifier to set.                                          |
| `types` | string[] | No       | Specifies the type identifiers of the blocks where the flag should be applied. |

---
### 🔧 `get` {#get}
Retrieves the namespace for the specified identifier (or the active namespace if the identifier is omitted).
:::info
A new namespace is created when there is no namespace found for the supplied identifier.
:::
#### Signature
```ts
get(identifier?: string): INamespace
```
#### Parameters
| Name         | Type   | Optional | Description                                            |
|:-------------|:-------|:---------|:-------------------------------------------------------|
| `identifier` | string | No       | Specifies the identifier of the namespace to retrieve. |
#### Return value
Returns the [`INamespace`](../interfaces/INamespace.mdx) object that holds the blocks for the namespace.

---
### 🔧 `hasFlag` {#hasFlag}
Verifies if the block has the specified flag enabled.
#### Signature
```ts
flag(flag: string, type: string): boolean
```
#### Parameters
| Name   | Type   | Optional | Description                                           |
|:-------|:-------|:---------|:------------------------------------------------------|
| `id`   | string | No       | Specifies the flag identifier to verify.              |
| `type` | string | No       | Specifies the type identifier of the block to verify. |
#### Return value
Returns `true` if the specified flag was enabled.

---
### 🔧 `include` {#include}
Specifies the blocks to use in the builder. Only the blocks specified are usable in the builder. All other blocks are unavailable.
#### Signature
```ts
include(...blocks: string[]): void
```
#### Parameters
| Name     | Type     | Optional | Description                                               |
|:---------|:---------|:---------|:----------------------------------------------------------|
| `blocks` | string[] | No       | Specifies the block identifiers of the blocks to include. |
#### Example
```ts showLineNumbers
import { Namespaces } from "@tripetto/builder";

// Only allow the text input blocks in the builder
Namespaces.include("@tripetto/block-text", "@tripetto/block-textarea");
```

---
### 🔧 `isAllowed` {#isAllowed}
Retrieves if a certain block is available in the builder.
#### Signature
```ts
isAllowed(identifier: string): boolean
```
#### Parameters
| Name         | Type   | Optional | Description                             |
|:-------------|:-------|:---------|:----------------------------------------|
| `identifier` | string | No       | Specifies the block identifier to test. |
#### Return value
Returns `true` if the block is available in the builder.
#### Example
```ts showLineNumbers
import { Namespaces } from "@tripetto/builder";

// Let's check if the mailer block is loaded and available
if (Namespaces.isAllowed("@tripetto/block-mailer")) {
    // Yes, it is!
}
```

---
### 🔧 `isAvailable` {#isAvailable}
Check is the specified namespace is available.
#### Signature
```ts
isAvailable(identifier: string): boolean
```
#### Parameters
| Name         | Type   | Optional | Description                                  |
|:-------------|:-------|:---------|:---------------------------------------------|
| `identifier` | string | No       | Specifies the namespace identifier to check. |
#### Return value
Returns `true` if the namespace is available in the builder.
#### Example
```ts showLineNumbers
import { Namespaces } from "@tripetto/builder";

// Load the block bundle of the Autoscroll stock runner
mountNamespace("autoscroll");
import "@tripetto/runner-autoscroll/builder";
unmountNamespace();

// Let's check if the block namespace for the autoscroll stock runner is loaded
if (Namespaces.isAvailable("autoscroll")) {
    // Yes, it is!
}
```

---
### 🔧 `loadUMD` {#loadUMD}
Loads a block bundle using UMD code.
:::danger
Loading UMD code from a string is unsafe! Only use when the source of the UMD code is trusted. When you are using a [Content Security Policy (CSP)](https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP) configuration, make sure `script-src 'unsafe-eval'` is allowed.
:::
:::caution
The preferred way to load block bundles is by using the [`namespace`](../interfaces/IBuilderProperties.mdx#namespace) property of the builder constructor or the [`useNamespace`](../classes/Builder.mdx#useNamespace) method of a builder instance.
:::
#### Signature
```ts
loadUMD(
  identifier: string,
  context: {},
  name: string,
  umd: string,
  unload?: boolean
): INamespace | undefined
```
#### Parameters
| Name         | Type    | Optional | Description                                                                                                              |
|:-------------|:--------|:---------|:-------------------------------------------------------------------------------------------------------------------------|
| `identifier` | string  | No       | Specifies the namespace identifier.                                                                                      |
| `context`    | `{}`    | No       | Reference to a context to use (this context is made available under the supplied name to the UMD bundle that is loaded). |
| `name`       | string  | No       | Specifies the (to the UMD exposed) symbol name of the supplied context.                                                  |
| `umd`        | string  | No       | Specifies the UMD code to load.                                                                                          |
| `unload`     | boolean | Yes      | Specifies if the namespace should unload if it is already available (default is `true`).                                 |
#### Return value
Returns a reference to the [`INamespace`](../interfaces/INamespace.mdx) object or `undefined` if the namespace was not loaded.

---
### 🔧 `loadURL` {#loadURL}
Loads a block bundle from an URL.
:::info
The UMD bundle is loaded using [trusted-types](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/trusted-types) with a policy name defined by the `policy` parameter.
:::
:::caution
The preferred way to load block bundles is by using the [`namespace`](../interfaces/IBuilderProperties.mdx#namespace) property of the builder constructor or the [`useNamespace`](../classes/Builder.mdx#useNamespace) method of a builder instance.
:::
#### Signature
```ts
loadURL(
  identifier: string,
  context: {},
  name: string,
  policy: string,
  url: string,
  done?: (namespace: INamespace) => void,
  unload?: boolean
): void
```
#### Parameters
| Name   | Type                                                                    | Optional | Description                                                                                                                                        |
|:-------|:------------------------------------------------------------------------|:---------|:---------------------------------------------------------------------------------------------------------------------------------------------------|
| `identifier` | string                                                            | No       | Specifies the namespace identifier.                                                                                                                |
| `context`    | `{}`                                                              | No       | Reference to a context to use (this context is made available under the supplied name to the UMD bundle that is loaded).                           |
| `name`       | string                                                            | No       | Specifies the (to the UMD exposed) symbol name of the supplied context.                                                                            |
| `policy`     | string                                                            | No       | Specifies the [trusted-types](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy/trusted-types) policy name to use. |
| `url`        | string                                                            | No       | Specifies the URL of the UMD block bundle to load.                                                                                                 |
| `done`       | (namespace: [`INamespace`](../interfaces/INamespace.mdx)) => void | Yes      | Callback function that is invoked when the loading is completed.                                                                                   |
| `unload`     | boolean                                                           | Yes      | Specifies if the namespace should unload if it is already available (default is `true`).                                                           |

---
### 🔧 `mount` {#mount}
Mounts (activates) a namespace context. All blocks that self-register when this namespace is mounted will become part of the namespace.
:::tip
You can also use the shorthand [`mountNamespace`](../functions/mountNamespace.mdx) function.
:::
#### Signature
```ts
mount(identifier?: string): void
```
#### Parameters
| Name         | Type   | Optional | Description                                         |
|:-------------|:-------|:---------|:----------------------------------------------------|
| `identifier` | string | No       | Specifies the identifier of the namespace to mount. |

---
### 🔧 `unload` {#unload}
Unloads the specified namespace.
#### Signature
```ts
unload(identifier?: string): boolean
```
#### Parameters
| Name         | Type   | Optional | Description                                          |
|:-------------|:-------|:---------|:-----------------------------------------------------|
| `identifier` | string | No       | Specifies the identifier of the namespace to unload. |
#### Return value
Returns `true` when the namespace is unloaded.

---
### 🔧 `unmount` {#unmount}
Unmounts (deactivates) the active namespace context.
:::tip
You can also use the shorthand [`unmountNamespace`](../functions/unmountNamespace.mdx) function.
:::
#### Signature
```ts
unmount(): void
```
