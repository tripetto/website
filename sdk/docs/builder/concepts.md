---
title: Builder Concepts
sidebar_label: Concepts
sidebar_position: 2
description: We’ll give you a high-level overview of the builder concepts here and provide some tips and tricks.
---

# Concepts
The Tripetto builder is one of a kind. It incorporates several solutions and principles to allow you to create and edit smart forms in 2D smoothly. Among many highly technical others, the most important visual ones are the **storyboard**, **add-and-arrange**, **layout guidance**, **active allineation**, **smart zoom**, and **feature cards**. We’ll give you a high-level overview of these concepts here and provide some tips and tricks.

:::tip TL;DR
Add and arrange building blocks on the storyboard to create your form. Layout guidance shows where to move and correctly drop building blocks, while active allineation continually handles the proper alignment of your arrangement. Use smart zoom to quickly go into and out of particular areas on the storyboard.
:::

<div className="youtube-embed"><iframe width="560" height="315" src="https://www.youtube.com/embed/DWdLEmpt4Os" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>

## Storyboard
Forms are created by placing and arranging building blocks on the 2D storyboard in the builder. As the structure of the form expands beyond the edges of the screen, the storyboard can be shifted in any direction to view the desired part of the structure by simply dragging it with your mouse, finger, or pen.

:::tip
When the form expands beyond the edges of the screen, you can also use the **smart zoom** to quickly zoom in and out.
:::

<div className="youtube-embed"><iframe width="560" height="315" src="https://www.youtube.com/embed/n4ahGuFtuDg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>

## Add-and-arrange
As you add more building blocks to the form, you may also want to rearrange certain parts. You can move individual blocks or larger sub-structures at once. To do this, press-and-hold the relevant item for a brief moment to unlock and then move it. Release to drop and lock it again in the desired location.

:::tip
Move items to an area out of view by dragging them up to the screen edge to shift the view to the desired point in the form structure.
:::

<div className="youtube-embed"><iframe width="560" height="315" src="https://www.youtube.com/embed/zhaHhlAzKNU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>

## Layout guidance
When you’re moving parts around, the builder visually guides you. It shows you where around your moving structure it could be correctly dropped and locked by temporarily drawing a guide with a duplicate structure in place and re-aligning all relevant structures.

:::info
Layout guides are doubly sticky. They both exactly follow the situational alignments and pull your moving structure precisely in there when you drop it. If you release something without a layout guide in sight, it will fall right back to where you picked it up.
:::

## Active allineation
As you expand your smart form in the builder, tidiness becomes key. We didn’t want you to have to worry about this, and so we taught the builder to understand what you’re doing. It actively aligns your form structure as you alter its arrangement. Nice and neat.

## Smart zoom
This feature is also absolutely essential for the usability of the builder. It offers the following solutions to streamline navigation across the storyboard.

### Zoom slider
Using the zoom slider to the side of the screen you can zoom in on and out of the area in the center of the screen by either moving the slider itself up and down or pressing the + and - buttons. On touchscreens, pinch to zoom also works.

### Autosize buttons
Press the upper and lower ends of the button on the slider to instantly zoom all the way in on and out of the area in the center of the screen respectively. When your form structure exceeds the screen width, hit the middle of the button to fit it to the screen width.

### Focus
Contrary to the zoom slider, this feature lets you swiftly zoom in on and out of a specific point on the screen without having to use the slider at all. Just double-press on the item you want to fully zoom in on. Repeat to zoom all the way out again.

## Feature cards
You use feature cards inside the builder to manage the properties and settings of building blocks on the storyboard. Feature cards are designed specifically to not show what you don’t need in average scenarios and let you add only the desired additional settings options dynamically as you go. They make editing a breeze.

## Blocks
Because we don’t impose any particular UI library or framework for the runner, we also don’t know which form element types (e.g. text input, checkbox, dropdown etc.) – the building blocks of a form – are available in your runner implementation. For this reason, Tripetto takes a flexible approach to the available building blocks in the builder.

In other words, you decide which building blocks you want to use in the builder and runner. We definitely offer a default set to choose from. But, you could of course decide to take a couple of them out. And you may also develop your own additional building blocks.
