const fs = require("fs");
const sitemapSource = fs.readFileSync("./build/sitemap.xml", "utf8");
const from = sitemapSource.indexOf("<url>");
const to = sitemapSource.lastIndexOf("</url>");

console.log("Merging sitemaps...");

if (from > 0 && to > from) {
    const urls = sitemapSource.substring(from, to + 6);
    const destination = fs.readFileSync("../public/sitemap.xml", "utf8");

    console.log(`Read ${urls.length} bytes from './build/sitemap.xml'`);
    console.log(
        `Read ${destination.length} bytes from '../public/sitemap.xml'`
    );

    const sitemap = destination
        .replace("</urlset>", `${urls}</urlset>`)
        .replace(new RegExp("<changefreq>weekly</changefreq>", "g"), "")
        .replace(new RegExp("<priority>0.5</priority>", "g"), "");

    console.log(`Write ${sitemap.length} bytes to '../public/sitemap.xml'`);

    fs.writeFileSync("../public/sitemap.xml", sitemap, "utf8");
} else {
    throw new Error("Invalid sitemap!");
}
