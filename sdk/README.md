# Tripetto SDK Documentation

## Local development

```bash
$ npm start
```

## Build

```bash
$ npm run build
```

This command generates static content into the `build` directory and can be served using any static contents hosting service.
