const prism = require("prism-react-renderer");
const npm2yarn = require("@docusaurus/remark-plugin-npm2yarn");

// With JSDoc @type annotations, IDEs can provide config autocompletion
/** @type {import('@docusaurus/types').DocusaurusConfig} */
(
    module.exports = {
        title: "Tripetto FormBuilder SDK",
        titleDelimiter: "-",
        tagline:
            "Fully customizable components for equipping websites and apps with a comprehensive solution for building and running conversational forms.",
        url: "https://tripetto.com",
        baseUrl: "/sdk/",
        onBrokenLinks: "throw",
        onBrokenMarkdownLinks: "throw",
        noIndex: false,
        trailingSlash: true,
        favicon: "img/favicon.ico",
        presets: [
            [
                "@docusaurus/preset-classic",
                /** @type {import('@docusaurus/preset-classic').Options} */
                ({
                    docs: {
                        sidebarPath: require.resolve("./sidebars.js"),
                        exclude: ["**/*.include.mdx"],
                        editUrl: ({ docPath }) => {
                            return `https://gitlab.com/tripetto/website/-/tree/main/sdk/docs/${docPath}`;
                        },
                        showLastUpdateAuthor: true,
                        showLastUpdateTime: true,
                        remarkPlugins: [[npm2yarn, { sync: true }]],
                    },
                    blog: {
                        showReadingTime: true,
                        editUrl:
                            "https://gitlab.com/tripetto/website/-/tree/main/sdk/blog/",
                    },
                    theme: {
                        customCss: require.resolve("./src/css/custom.css"),
                    },
                    sitemap: {
                        ignorePatterns: ["/tags/**"],
                        filename: "sitemap.xml",
                    },
                }),
            ],
        ],

        themeConfig:
            /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
            ({
                docs: {
                    sidebar: {
                        hideable: true,
                        autoCollapseCategories: true,
                    },
                },
                navbar: {
                    hideOnScroll: true,
                    title: "FormBuilder SDK",
                    logo: {
                        src: "img/logo.svg",
                        href: "https://tripetto.com/sdk/",
                        target: "_self",
                    },
                    items: [
                        {
                            href: "https://tripetto.com/sdk/how-it-works/",
                            label: "How It Works",
                            position: "left",
                        },
                        {
                            href: "https://tripetto.com/sdk/components/",
                            label: "Components",
                            position: "left",
                        },
                        {
                            href: "https://tripetto.com/sdk/solutions/",
                            label: "Solutions",
                            position: "left",
                        },
                        {
                            type: "doc",
                            docId: "index",
                            label: "Docs",
                            position: "left",
                        },
                        {
                            href: "https://tripetto.com/sdk/pricing/",
                            label: "Pricing",
                            position: "left",
                        },
                        {
                            href: "https://tripetto.com/sdk/support/",
                            label: "Support",
                            position: "right",
                        },
                        {
                            href: "https://gitlab.com/tripetto",
                            position: "right",
                            className: "header-gitlab-link",
                            "aria-label": "Tripetto source code on GitLab",
                        },
                    ],
                },
                prism: {
                    theme: prism.themes.palenight,
                    darkTheme: prism.themes.palenight,
                    additionalLanguages: ["php"],
                },
                colorMode: {
                    defaultMode: "light",
                    disableSwitch: false,
                    respectPrefersColorScheme: true,
                },
                algolia: {
                    appId: "80VVB9EZ1M",
                    apiKey: "64115a50dc6e9e90490cc1b438359498",
                    indexName: "tripetto-sdk-docs-staging",
                },
            }),

        plugins: [
            [
                require.resolve("docusaurus-gtm-plugin"),
                {
                    id: "GTM-KPS9JZK",
                },
            ],
        ],
        headTags: [
            {
                tagName: "script",
                attributes: {
                    src: "/sdk/js/redirect.js",
                },
            },
        ],
    }
);
