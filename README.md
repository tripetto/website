[![pipeline status](https://gitlab.com/tripetto/website/badges/main/pipeline.svg)](https://gitlab.com/tripetto/website/commits/main)

This repository contains the Tripetto website which is published at [tripetto.com](https://tripetto.com). The website consists of two parts. All pages except those behind the path `/sdk/docs/` are generated using [Jekyll](https://jekyllrb.com/). The SDK documentation pages (located in `/sdk/docs/`) are generated using [Docusaurus](https://docusaurus.io/).

## 🧪 For the part generated with Jekyll

### Prerequisites
Make sure [Ruby](https://www.ruby-lang.org/en/) is installed. Run the following command to install the required gems.

```bash
gem install jekyll bundler
```

### Development build
To test the website on your local machine, run the following command inside the root folder of the repository:

```bash
bundle install
bundle exec jekyll serve
```

Then browse to `http://localhost:4000/` to test the website.

### Production build
To generate a production build run:

```bash
bundle install
bundle exec jekyll build
```

Make sure you set the following environment variable: `JEKYLL_ENV=production`

The website is generated in the folder `./public`.

## 🦖 For the part generated with Docusaurus

### Prerequisites
Make sure you have [Node.js](https://nodejs.org/) running. Then run the following command to install the required packages:

```bash
cd ./sdk
npm i
```

### Development build
To test the docs pages on your local machine, run the following command in the `./sdk` folder:

```bash
npm start
```

Then browse to `http://localhost:3000/sdk/docs/` to test the docs pages.

### Production build
To generate a production build run:

```bash
cd ./sdk
npm run build
```

The docs pages are generated in the folder `./sdk/build`.

## 🚀 Deployment
During deployment, both parts of the website are merged into the root `./public` folder and uploaded to the hosting environment.
