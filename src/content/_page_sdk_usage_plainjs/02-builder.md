---
base: ../../
---

<section class="sdk-usage-builder" id="builder">
  <div class="container">
    <div class="row content">
      <div class="col">
        <h2 class="palette-sdk-builder">Add the plain JS package <span>to build forms in your project.</span></h2>
      </div>
    </div>
    <div class="row content">
      <div class="col-md-11 col-lg-12">
        <p>Optionally add powerful form building capabilities to your plain JS app or website by neatly integrating the visual form builder in minutes. <strong>The builder will run entirely inside your JavaScript project</strong> - with custom extensions you develop, and without any dependencies on external infrastructure.</p>
      </div>
    </div>
    <div class="row">
      <div class="col sdk-usage-builder-visual">
        <div class="sdk-device-code sdk-device-code-left">
          <img src="{{ page.base }}images/sdk-scenes/builder.webp" width="2000" height="1441" alt="Screenshot of the form builder." class="sdk-device-code-block sdk-device-code-device" loading="lazy" />
          <div class="sdk-code-snippet sdk-device-code-block sdk-device-code-snippet">
            <ul class="nav nav-tabs nav-fill sdk-code-snippet-nav sdk-code-snippet-nav-single" role="tablist">
              <li class="nav-item"><a class="nav-link active" id="nav-builder-javascript" data-toggle="tab" href="#builder-javascript" role="tab" aria-controls="builder-javascript" aria-selected="true" title="Implement the builder with plain JavaCcript">{% include icons/javascript.html %}</a></li>
            </ul>
            <div class="tab-content sdk-code-snippet-content">
              <div class="tab-pane fade show active" id="builder-javascript" role="tabpanel" aria-labelledby="nav-builder-javascript">
              {% include sdk-code-snippet-pane.html active=true code="builder-javascript.html" language="javascript" palette="builder" url-run="https://codepen.io/tripetto/pen/ZEJrQxB/8b40f4c95c105914df766aa523a0b5eb" url-docs="https://tripetto.com/sdk/docs/builder/integrate/quickstart/plain-js/" %}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {% include sdk-features-builder.html %}
  </div>
</section>
