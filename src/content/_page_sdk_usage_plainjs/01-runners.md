---
base: ../../
---

<section class="sdk-usage-runners">
  <div class="container">
    <div class="row content">
      <div class="col">
        <h2 class="palette-sdk-runners">Use the plain JS package <span>to run forms in your project.</span></h2>
      </div>
    </div>
    <div class="row content">
      <div class="col-md-11 col-lg-12">
        <p>Create a stunning form in the free online <a href="{{ site.url_app }}" target="_blank">Tripetto studio</a> or <a href="#builder" class="anchor">integrated form builder</a> and implement it in your plain JS app or website in no time. <strong>The form will run entirely inside your JavaScript project</strong> - with the style and UX of your choice, and without any dependencies on external infrastructure.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-12 sdk-carousel-runners-buttons">
        <ul class="carousel-buttons">
          <li class="palette-autoscroll active" data-target="#carouselSDKRunners" data-slide-to="0">
            <div>
              {% include icon-face.html face='autoscroll' size='small' name='Autoscroll Form Face' template='sdk' radius='medium' %}
            </div>
            <div>
              <h3>Autoscroll<span> Face</span></h3>
              <p>Fluently presents <strong>one question at a time</strong>.</p>
            </div>
          </li>
          <li class="palette-chat" data-target="#carouselSDKRunners" data-slide-to="1">
            <div>
              {% include icon-face.html face='chat' size='small' name='Chat Form Face' template='sdk' radius='medium' %}
            </div>
            <div>
              <h3>Chat<span> Face</span></h3>
              <p>Presents all <strong>questions and answers as a chat</strong>.</p>
            </div>
          </li>
          <li class="palette-classic" data-target="#carouselSDKRunners" data-slide-to="2">
            <div>
              {% include icon-face.html face='classic' size='small' name='Classic Form Face' template='sdk' radius='medium' %}
            </div>
            <div>
              <h3>Classic<span> Face</span></h3>
              <p>Presents question fields in a <strong>traditional format</strong>.</p>
            </div>
          </li>
        </ul>
      </div>
    </div>
    <div class="row carousel-slides">
      <div class="col">
        <div id="carouselSDKRunners" class="carousel slide">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="row">
                <div class="col-12">
                  <div class="sdk-device-code sdk-device-code-right">
                    <img src="{{ page.base }}images/sdk-scenes/runner-autoscroll.webp" width="2220" height="1435" alt="Screenshot of a fitness registration form in the autoscroll form face, shown on a tablet and a mobile phone." class="sdk-device-code-block sdk-device-code-device" loading="lazy" />
                    <div class="sdk-code-snippet sdk-device-code-block sdk-device-code-snippet">
                      <ul class="nav nav-tabs nav-fill sdk-code-snippet-nav sdk-code-snippet-nav-single" role="tablist">
                        <li class="nav-item"><a class="nav-link active" id="nav-runner-autoscroll-javascript" data-toggle="tab" href="#runner-autoscroll-javascript" role="tab" aria-controls="runner-autoscroll-javascript" aria-selected="true" title="Implement the autoscroll runner with plain JavaScript">{% include icons/javascript.html %}</a></li>
                      </ul>
                      <div class="tab-content sdk-code-snippet-content">
                        <div class="tab-pane fade show active" id="runner-autoscroll-javascript" role="tabpanel" aria-labelledby="nav-runner-autoscroll-javascript">
                        {% include sdk-code-snippet-pane.html active=true code="runners-javascript.html" face="autoscroll" face-name="Autoscroll" language="javascript" palette="runners" url-run="https://codesandbox.io/s/tripetto-sdk-runner-autoscroll-handling-response-data-60cti?file=/src/index.ts" url-docs="https://tripetto.com/sdk/docs/runner/stock/quickstart/plain-js/" %}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="row">
                <div class="col-12">
                  <div class="sdk-device-code sdk-device-code-right">
                    <img src="{{ page.base }}images/sdk-scenes/runner-chat.webp" width="2220" height="1435" alt="Screenshot of a fitness registration form in the chat form face, shown on a tablet and a mobile phone." class="sdk-device-code-block sdk-device-code-device" loading="lazy" />
                    <div class="sdk-code-snippet sdk-device-code-block sdk-device-code-snippet">
                      <ul class="nav nav-tabs nav-fill sdk-code-snippet-nav sdk-code-snippet-nav-single" role="tablist">
                        <li class="nav-item"><a class="nav-link active" id="nav-runner-chat-javascript" data-toggle="tab" href="#runner-chat-javascript" role="tab" aria-controls="runner-chat-javascript" aria-selected="true" title="Implement the chat runner with plain JavaScript">{% include icons/javascript.html %}</a></li>
                      </ul>
                      <div class="tab-content sdk-code-snippet-content">
                        <div class="tab-pane fade show active" id="runner-chat-javascript" role="tabpanel" aria-labelledby="nav-runner-chat-javascript">
                        {% include sdk-code-snippet-pane.html active=false code="runners-javascript.html" face="chat" face-name="Chat" language="javascript" palette="runners" url-run="https://codesandbox.io/s/tripetto-sdk-runner-chat-handling-response-data-ju5rv?file=/src/index.ts" url-docs="https://tripetto.com/sdk/docs/runner/stock/quickstart/plain-js/" %}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="row">
                <div class="col-12">
                  <div class="sdk-device-code sdk-device-code-right">
                    <img src="{{ page.base }}images/sdk-scenes/runner-classic.webp" width="2220" height="1435" alt="Screenshot of a fitness registration form in the classic form face, shown on a tablet and a mobile phone." class="sdk-device-code-block sdk-device-code-device" loading="lazy" />
                    <div class="sdk-code-snippet sdk-device-code-block sdk-device-code-snippet">
                      <ul class="nav nav-tabs nav-fill sdk-code-snippet-nav sdk-code-snippet-nav-single" role="tablist">
                        <li class="nav-item"><a class="nav-link active" id="nav-runner-classic-javascript" data-toggle="tab" href="#runner-classic-javascript" role="tab" aria-controls="runner-classic-javascript" aria-selected="true" title="Implement the classic runner with plain JavaScript">{% include icons/javascript.html %}</a></li>
                      </ul>
                      <div class="tab-content sdk-code-snippet-content">
                        <div class="tab-pane fade show active" id="runner-classic-javascript" role="tabpanel" aria-labelledby="nav-runner-classic-javascript">
                        {% include sdk-code-snippet-pane.html active=false code="runners-javascript.html" face="classic" face-name="Classic" language="javascript" palette="runners" url-run="https://codesandbox.io/s/tripetto-sdk-runner-classic-handling-response-data-k6n5z?file=/src/index.ts" url-docs="https://tripetto.com/sdk/docs/runner/stock/quickstart/plain-js/" %}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {% include sdk-features-runners.html %}
  </div>
</section>
