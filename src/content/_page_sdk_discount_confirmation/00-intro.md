---
base: ../../../
---

<section class="sdk-discount-confirmation-intro block-first sdk-intro">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-md-12 col-lg-10 col-xl-7">
        <h1>We have sent your coupon.</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-11 col-md-8 col-lg-7 col-xl-6">
        <p>We have sent your discount coupon to your email address. You may activate it up to 3 months from now if and when you purchase a new SDK license. <strong>Thank you for helping us.</strong></p>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
