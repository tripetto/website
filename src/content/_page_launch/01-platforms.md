---
base: ../
---

<section class="versions">
  <div class="container">
    <div class="row versions-images">
      <div class="col-11 col-xl-10">
        <img src="{{ page.base }}images/versions-cta/studio.svg" width="188" heigt="320" alt="Illustration representing the studio" />
        <img src="{{ page.base }}images/versions-cta/wordpress.svg" width="242" heigt="320" alt="Illustration representing the WordPress plugin" />
      </div>
    </div>
    <div class="row versions-blocks">
      <div class="col-xl-11">
        <div class="row">
          <div class="col-12 col-md-6 versions-version">
            <div class="versions-version-studio">
              <div>
                <h2>Studio</h2>
                <p>Tripetto in the cloud</p>
                <ul>
                  <li>Available at tripetto.app</li>
                  <li>Store forms and data at Tripetto</li>
                  <li>Free and pay-once per form</li>
                </ul>
              </div>
              <div>
                <a href="{{ site.url_app }}" target="_blank" class="button button-full">Start For Free</a>
              </div>
            </div>
            <a href="{{ page.base }}studio/" class="hyperlink hyperlink-small"><span>About the studio</span><i class="fas fa-arrow-right"></i></a>
          </div>
          <div class="col-12 col-md-6 versions-version">
            <div class="versions-version-wordpress versions-hint">
              <div>
                <h2>Plugin</h2>
                <p>Tripetto inside WordPress</p>
                <ul>
                  <li>Runs completely inside WordPress</li>
                  <li>Store data inside the plugin only</li>
                  <li>Free and paid subscriptions</li>
                </ul>
              </div>
                <div>
                <a href="{{ site.url_wordpress_plugin }}" target="_blank" class="button button-full">Download Free Plugin</a>
              </div>
            </div>
            <a href="{{ page.base }}wordpress/" class="hyperlink hyperlink-small"><span>About the plugin</span><i class="fas fa-arrow-right"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
