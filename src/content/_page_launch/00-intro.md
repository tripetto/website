---
base: ../
---

<section class="launch-intro block-first intro">
  <div class="container">
    <div class="row launch-row-intro">
      <div class="col-md-9 shape-before shape-after">
        <h1>Get Started</h1>
      </div>
      <div class="col-md-9 col-lg-8">
        <p><strong>Tripetto comes in various flavors.</strong> All are full-blown versions with practically identical feature sets. But made for different users and purposes.</p>
        <a href="{{ page.base }}versions/" class="hyperlink"><span>Which version is for me?</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
