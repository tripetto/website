---
base: ../../
---

<section class="sdk-pricing-overview">
  <div class="container">
    <div class="row">
      <div class="col sdk-overview">
        <div class="sdk-overview-pricing-builder">
          <div>
            <div>
              <h3>Builder licenses and pricing.</h3>
              <p>You may embed and integrate the builder in websites and apps for free for evaluation purposes. <strong>A paid license is required for select builder usages in business applications and/or production environments.</strong> Licenses start at €325/month.</p>
            </div>
            <a href="#determine-license" class="button button-full button-small anchor"><span>Calculate License</span><i class="fas fa-arrow-right"></i></a>
          </div>
        </div>
        <div class="sdk-overview-pricing-runners">
          <div>
            <div>
              <h3>Runner licenses and pricing.</h3>
              <p>You may embed and integrate runners in websites and apps for free as long as the default Tripetto branding is enabled. <strong>A paid license is required for hiding Tripetto branding.</strong> Licenses start at €75/month.</p>
            </div>
            <a href="#determine-license" class="button button-full button-small anchor"><span>Calculate License</span><i class="fas fa-arrow-right"></i></a>
          </div>
        </div>
        <div class="sdk-overview-pricing-blocks">
          <div>
            <div>
              <h3>Blocks licenses and pricing.</h3>
              <p><strong>All building blocks and SDK resources are free</strong>, with the exception of the builder and runners in select use cases.</p>
            </div>
            <a href="#determine-license" class="button button-full button-small anchor"><span>Calculate License</span><i class="fas fa-arrow-right"></i></a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
