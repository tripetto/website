---
base: ../../
---

<section class="sdk-pricing-intro block-first sdk-intro">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-xl-6">
        <h1>Licenses and SDK pricing.</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 col-lg-7 col-xl-6">
        <p>Even though much of the FormBuilder SDK is open source, and using it is free in numerous cases, <strong>a paid license is required for select implementations.</strong> Find out more below.</p>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
