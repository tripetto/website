---
base: ../../
---
<section class="sdk-pricing-form" id="determine-license">
  <div class="container">
    <div class="row">
      <div class="col-md-11 col-lg-8 col-xl-7">
        <h2>Determine License</h2>
        <p>Whether you need a paid license, and which license is required, depends on multiple factors. Run through the form below to find out what you need, and <strong>get a quote for your FormBuilder SDK license.</strong></p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-7 order-last order-md-first">
        <div id="TripettoRunner" class="runner-autoscroll-horizontal"></div>
      </div>
      <div class="col-md-5 order-first order-md-last">
        <small>A paid license is required for select SDK implementations. Use of the SDK is <strong>free for evaluation purposes.</strong></small>
        <div class="sdk-pricing-form-outcome">
          <div class="sdk-pricing-form-price pricing-price">
            <div class="pricing-price-prefix">
              <div class="valuta">€</div>
              <div class="amount" id="TripettoPrice">0</div>
              <div class="conditions">
                <span class="conditions-title">Per year</span>
                <span class="conditions-subtitle">Per domain</span>
              </div>
            </div>
          </div>
          <div class="sdk-pricing-form-license" id="TripettoLicense"></div>
          <div class="sdk-pricing-form-license-type" id="TripettoLicenseType"></div>
          <div class="sdk-pricing-form-users" id="TripettoUsers"></div>
        </div>
      </div>
    </div>
  </div>
</section>
<script src="{{ page.base }}vendors/tripetto-runner.js?v={{ site.cache_version }}"></script>
<script src="{{ page.base }}vendors/tripetto-runner-autoscroll.js?v={{ site.cache_version }}"></script>
<script src="{{ page.base }}vendors/tripetto-studio.js?v={{ site.cache_version }}"></script>
<script>
TripettoStudio.form({
  runner: TripettoAutoscroll,
  token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoibFV6Q2ROWXVWYnEvcmZSRkxObERPQ25JcGtEV0VLbkRLUWlRUVNZSzRSMD0iLCJkZWZpbml0aW9uIjoiVmxvUCtrMHY1QmJZdG54MzNNY1pZYlZEQ3pFb3NTRUU4dStlRzJFcU81UT0iLCJ0eXBlIjoiY29sbGVjdCJ9.sVzHuQS2Ik1PLquDwAOBFb0gIoaizmVhyw8Z9VsVMaM",
  element: "TripettoRunner",
  customCSS: "[data-block='@tripetto/block-multiple-choice'] { h2 { letter-spacing: -1px; } } [data-block='@tripetto/block-yes-no'] { h2 { letter-spacing: -1px; } } [data-block='@tripetto/block-radiobuttons'] { h2 { letter-spacing: -1px; } } [data-block='@tripetto/block-text'] { h2 { letter-spacing: -1px; } input { min-height: 56px; } } [data-block='@tripetto/block-email'] { h2 { letter-spacing: -1px; } input { min-height: 56px; } } [data-block='@tripetto/block-url'] { h2 { letter-spacing: -1px; } input { min-height: 56px; } } [data-block='@tripetto/block-textarea'] { h2 { letter-spacing: -1px; } } [data-block='@tripetto/block-stop'] { h2 { letter-spacing: -1px; } } [data-block='@tripetto/block-paragraph'] { h2 { letter-spacing: -1px; } } [data-block='@tripetto/block-number'] { h2 { letter-spacing: -1px; } input { min-height: 56px; } }",
  onData: function(instance) {
      var data = TripettoRunner.Export.NVPs(instance, "strings");
      var users = TripettoRunner.castToNumber(data["OUTPUT_USERS"]);
      var price = data["OUTPUT_PRICE"];
      document.getElementById("TripettoPrice").innerHTML = (price == "-1" ? "?" : price) || "0";
      document.getElementById("TripettoLicense").innerHTML = data["OUTPUT_LICENSE"] || "";
      document.getElementById("TripettoLicenseType").innerHTML = data["OUTPUT_LICENSE_TYPE"] || "";
      document.getElementById("TripettoUsers").innerHTML = users != 0 ? (users === -1 ? "Custom amount of users" : ("Up to " + users + " users")) : "";
    }
});
</script>
