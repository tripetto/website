---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-style-your-forms/
title: Style your forms - Tripetto Help Center
description: You can style all your forms completely to your needs, for example to meet your company's brand.
article_title: How to style your forms
article_id: styling
article_folder: styling
article_video: customization-style
author: jurgen
time: 5
time_video: 2
category_id: customization
subcategory: customization_styling
areas: [studio, wordpress]
---
<p>You can style all your forms completely to your needs, for example to meet your company's brand. You can start simple with just one base color, or modify each element's appearance in your form.</p>

<h2 id="about">About styling</h2>
<p>Before we start styling, let's explain a few things that could be handy while styling your forms.</p>

<h3 id="coloring">Color selecting</h3>
<p>There are a few ways to enter your desired colors in Tripetto:</p>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/color-control.png" width="358" height="432" alt="Screenshot of the color control in Tripetto" loading="lazy" />
  <figcaption>The color control in Tripetto makes it easy to select and use your colors.</figcaption>
</figure>
<ul>
  <li>Select via the <strong>color control</strong> - This is the easiest way to select a color in Tripetto. Just click the color field to open the color control and select your desired color from the color pane, RGB(A) input or HEX input. The color control also keeps a swatch panel of previously used colors, so you can easily select corresponding colors that are already used in your template;</li>
  <li>Enter a <strong>color name</strong> - Just type the name of one of the 140 predefined colors that work in all modern browsers (<a href="https://htmlcolorcodes.com/color-names/" target="_blank" rel="noopener noreferrer">see full list of available color names</a>);</li>
  <li>Enter a <strong>HEX color code</strong> - Need a custom color that's not on the color names list? Just enter any HEX color code, prefixed with a <code>#</code>;</li>
  <li>Enter a <strong>RGB(A) color code</strong> - Rather use the RGB(A) notation instead of HEX? No problem, just enter your RGB(A) color in the right format: <code>rgb()</code> or <code>rgba()</code> (with transparency level);</li>
  <li><strong>Transparent</strong> - You can also enter <code>transparent</code> as a color, for example to make the background color of your form transparent.</li>
</ul>

<h3 id="contrast">Color contrast</h3>
<p>Always make sure your form stays well readable while applying colors to it. The form will automatically try to keep the right amount of contrast, but you can always overrule that with your own color settings.</p>
<hr />

<h2 id="how-to-use">How to style forms</h2>
<p>At the top menu bar of the form builder click <code><i class="fas fa-magic"></i> Customize</code><i class="fas fa-arrow-right"></i><code>Styles</code>.</p>
<blockquote>
  <h4>💡 Tip: Use live form preview</h4>
  <p>The live preview in the form builder is your biggest friend while styling. It always updates to what you're doing, so also everything you do to style your forms, will immediately be visible in the live preview. More information:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-let-the-live-preview-work-for-you/">How to let the live preview work for you</a></li>
  </ul>
</blockquote>

<h3 id="basic-styling">Base color</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/color.png" width="363" height="103" alt="Screenshot of the styling in Tripetto" loading="lazy" />
  <figcaption>Example of a base color styling.</figcaption>
</figure>
<p>You can start with selecting just one simple base color. That color already will have a major effect on the styling of your form, as you can see in the live preview.</p>
<p>It depends on the selected <a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/">form face</a> what the effect of this base color is.</p>

<h3 id="font">Font</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/font.png" width="326" height="458" alt="Screenshot of the styling in Tripetto" loading="lazy" />
  <figcaption>Example of a Google Font styling.</figcaption>
</figure>
<p>Next step that has a major impact on your styling is the font. To enable font styling, activate the <code>Font</code> option in the Styles pane. You can now set the font family and font size.</p>
<p>You can simply select one of the predefined fonts that work in all browsers, but if you really want to show off you also can use any Google Font or even your own font.</p>
<p>For more detailed instructions about fonts, please have a look at this article:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-fonts/">How to use fonts</a></li>
</ul>

<h3 id="background">Background</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/background.png" width="327" height="336" alt="Screenshot of the styling in Tripetto" loading="lazy" />
  <figcaption>Example of a background image styling.</figcaption>
</figure>
<p>To style the background with a color and/or a background image, activate the <code>Background</code> option in the Styles pane.</p>
<p>You can now supply a background color as a color name, HEX code or RGB code.</p>
<h4>Background image</h4>
<p>You can also add an image as the background of your form. You can <a href="{{ page.base }}help/articles/how-to-use-images-and-videos-in-your-form/">enter the URL to an image</a> and then determine how the image must be positioned over the screen size.</p>

<h3 id="inputs">Inputs</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/inputs.png" width="328" height="705" alt="Screenshot of the styling in Tripetto" loading="lazy" />
  <figcaption>Example of input styling.</figcaption>
</figure>
<p>Input fields of course are an important part of your conversational forms. To enable input styling, activate the <code>Inputs</code> option in the Styles pane.</p>

<h4>Input fields</h4>
<p>Input fields are common form elements, like text fields, checkboxes and radio buttons. Those are all stylable by multiple colors, border size and roundness. Just play with them and see the result in the live preview.</p>

<h4>Input buttons</h4>
<p>Some question types, like Multiple choice, Rating and yes/no are presented with input buttons. You can style the following states of those buttons:</p>
<ul>
  <li><strong>Selection color</strong> - The color of buttons used in Multiple choice and Rating blocks;</li>
  <li><strong>Agree color</strong> - The color of <code>Yes</code> buttons used in yes/no blocks;</li>
  <li><strong>Decline color</strong> - The color of <code>No</code> buttons used in yes/no blocks;</li>
</ul>

<h3 id="buttons">Buttons</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/buttons.png" width="328" height="415" alt="Screenshot of the styling in Tripetto" loading="lazy" />
  <figcaption>Example of button styling.</figcaption>
</figure>
<p>Buttons are used in several places, like start buttons, navigation buttons and complete buttons. To enable button styling, activate the <code>Buttons</code> option in the Styles pane.</p>
<p>Buttons are stylable by color and style. With the button style you can determine if the buttons should be filled with the desired color, or shown as a button with only border coloring. The roundness sets the border radius of your buttons.</p>

<h4>Submit button</h4>
<p>The <code>Submit</code> button at the end of your form is of course an important button, so you can set a custom color for that button.</p>
<hr />

<h2 id="form-faces">Style form faces</h2>
<p>The above describes how you can style your forms in general. On top of that Tripetto offers three different <a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/">form faces</a>, completely changing the form layout.</p>
<div>
  <a href="https://tripetto.com/form-layouts/" class="blocklink">
    <div>
      <span class="title">Perfect form layouts with convertible form faces</span>
      <span class="description">Easily switch between totally different form experiences. Choose a Typeform-like, a chat, or a classic form face, and fully customize it for the optimal fit with your audience.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" />
    </div>
  </a>
</div>
<p>In regards to customization, each form face comes with its own additional settings and styling options to improve the finishing touch. Click the help articles below to see these options for each form face:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/discover-additional-options-for-autoscroll-form-face/">Additional options for the <strong>autoscroll form face</strong></a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/discover-additional-options-for-chat-form-face/">Additional options for the <strong>chat form face</strong></a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/discover-additional-options-for-classic-form-face/">Additional options for the <strong>classic form face</strong></a></li>
</ul>
