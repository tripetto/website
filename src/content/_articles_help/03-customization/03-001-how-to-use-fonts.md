---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-fonts/
title: Use fonts - Tripetto Help Center
description: One of the options to style your form is using the right font. That can be a basic font, but also any Google Font or even a custom font.
article_title: How to use fonts
article_folder: styling
article_video: customization-style
author: jurgen
time: 3
time_video: 2
category_id: customization
subcategory: customization_styling
areas: [studio, wordpress]
scripts: [prism]
stylesheets: [prism-dark]
---
<p>One of the options to style your form is using the right font. That can be a basic font, but also any Google Font or even a custom font.</p>

<h2 id="about">About styling</h2>
<p>The font setting is part of the styling options in Tripetto. To play with the fonts, open the Style pane. At the top menu bar of the form builder click <code><i class="fas fa-magic"></i> Customize</code><i class="fas fa-arrow-right"></i><code>Styles</code>. To enable font styling, activate the <code>Font</code> option in the Styling pane. You can now set the font family and font size.</p>
<blockquote>
  <h4>📌 Also see: Styling</h4>
  <p>The font setting is part of the styling options in Tripetto. For global instructions about all styling options, please have a look at this article:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-style-your-forms/">How to style your forms</a></li>
  </ul>
</blockquote>
<blockquote>
  <h4>💡 Tip: Use live form preview</h4>
  <p>The live preview in the form builder is your biggest friend while styling. It always updates to what you're doing, so if you update the font, this will immediately be visible in the live preview. More information:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-let-the-live-preview-work-for-you/">How to let the live preview work for you</a></li>
  </ul>
</blockquote>
<hr/>

<h2 id="font-family">Font family</h2>
<p>By default the font family is set to Arial. Tripetto offers various types of fonts that you can choose from:</p>
<ul>
  <li>Basic fonts;</li>
  <li>Google Fonts;</li>
  <li>Custom fonts.</li>
</ul>

<h3 id="basic-fonts">Basic fonts</h3>
<p>You can simply select one of the predefined basic fonts that work in all browsers. Just choose from the list of fonts and see which works best for your form.</p>

<h3 id="google-fonts">Google Fonts</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/font-family.png" width="328" height="232" alt="Screenshot of the styling in Tripetto" loading="lazy" />
  <figcaption>Example of a Google Font styling.</figcaption>
</figure>
<p>Google offers lots of free to use fonts. All those <a href="https://fonts.google.com/" target="_blank" rel="noopener noreferrer">Google Fonts</a> are directly available to use in your Tripetto form. To use a Google Font, select <code>Google Fonts or URL</code> in the Font selection in Tripetto.</p>
<p>Now open <a href="https://fonts.google.com/" target="_blank" rel="noopener noreferrer">Google Fonts</a> and search for a font that you'd like to use in your form. Then switch back to the form builder and enter the name of the Google Font of your choosing. Keep in mind Google Fonts are case sensitive.</p>
<div>
  <a href="https://fonts.google.com/" target="_blank" rel="noopener noreferrer" class="blocklink">
    <div>
      <span class="title">Google Fonts<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Making the web more beautiful, fast, and open through great typography.</span>
      <span class="url">fonts.google.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-google-fonts.png" width="160" height="160" alt="Google Fonts logo" loading="lazy" />
    </div>
  </a>
</div>

<h3 id="custom-fonts">Custom fonts</h3>
<p>Want to use your own custom font? That's also possible if you have a CSS file that loads your font file(s). Please follow these steps:</p>
<h4>1. Prepare CSS file</h4>
<p>First make sure Tripetto can use your custom font. To do so, create a CSS file in your website and address your fonts with <code>@font-face</code>. A basic example would be something like this (if your font is a <code>woff2</code> type called <code>MyFont</code> and the font file is located in the same directory as your CSS file):</p>
<pre class="line-numbers"><code class="language-css">@font-face {
  font-family: "MyFont";
  src: url("my-font.woff2") format("woff2");
}</code></pre>
<h4>2. Use CSS file</h4>
<p>Now you can use the URL to your CSS file in Tripetto. In the font selection, choose <code>Google Fonts or URL</code> and enter the URL to your CSS file.</p>
<h4>3. Add font-family to URL</h4>
<p>To make your font usable for Tripetto you need to add <code>#</code> + <code>font-family</code> to the URL that you entered in Tripetto. This is the name you entered as <code>font-family</code> in your CSS file.</p>
<p>In our example the final custom font URL will be like this: <code>https://yourwebsite.com/fonts/font.css#MyFont</code>.</p>
<blockquote>
  <h4>💡 Tip: URL encoding</h4>
  <p>If your font-family name contains spaces, make sure you replace each space with <code>%20</code> to ensure the right encoding. For example: <code>https://yourwebsite.com/fonts/font.css#MyFont%20Italic</code>.</p>
</blockquote>
<hr/>

<h2 id="font-size">Font size</h2>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/font-size.png" width="325" height="221" alt="Screenshot of the styling in Tripetto" loading="lazy" />
  <figcaption>Example of font size settings.</figcaption>
</figure>
<p>After you selected the right font, you can set the font size. Tripetto will use the entered font size as a base to determine the different font sizes in your forms. The font size will also have its effect on other form elements, like the size of input fieds, buttons and icons.</p>
<h3 id="font-size-small">Small screens</h3>
<p>To make sure your forms are displayed in the right proportion on small screen devices (like mobile phones), you can set a separate font size that will be used on screens smaller than 500 pixels. Tripetto will use that font size as a base to determine the different font sizes in your forms on small screens.</p>
<blockquote>
  <h4>💡 Tip: Set live preview to mobile</h4>
  <p>Switch the live preview to the mobile device preview to test this small screen font size. Click the <code><i class="fas fa-mobile-alt"></i></code> icon at the top of the preview pane to do so. More information:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-let-the-live-preview-work-for-you/">How to let the live preview work for you</a></li>
  </ul>
</blockquote>

