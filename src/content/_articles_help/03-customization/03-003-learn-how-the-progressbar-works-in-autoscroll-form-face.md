---
layout: help-article
base: ../../../
permalink: /help/articles/learn-how-the-progressbar-works-in-autoscroll-form-face/
title: The progressbar in autoscroll form face - Tripetto Help Center
description: The progressbar in autoscroll forms gives your respondents an indication of what to expect from your form and the progress they made filling out the form.
article_title: Learn how the progressbar works in autoscroll form face
article_folder: progressbar
author: jurgen
time: 4
category_id: customization
subcategory: customization_faces
areas: [studio, wordpress]
---
<p>The progressbar is shown inside your autoscroll forms to give your respondents an indication of what to expect from your form and the progress they made filling out the form. This article explains how the progressbar behaves to achieve this.</p>

<blockquote>
  <h4>📣 Info: Improved progressbar behavior</h4>
  <p>With the <a href="{{ page.base }}changelog/">October 2023 update</a> of Tripetto, the working of the progressbar in the autoscroll layout has been improved. Where the progressbar used to look only as far as the current section with required questions, it now always takes the whole form into account. This way the progressbar gives a more accurate indication of what to expect for the respondent.</p>
  <p>In case you want your form and progressbar to behave as before the October 2023 update, you can enable the styling option <code><a href="#hide-upcoming-sections" class="anchor">Hide upcoming sections</a></code>.</p>
</blockquote>

<h2 id="concept">How it works</h2>
<p>The concept of the progressbar is quite simple: by default it shows the amount of passed questions (answered or skipped) in relation to the total amount of questions in the whole form.</p>
<p>In a static form that would be it, but Tripetto forms aren't static and can alter based on the given answers by the respondent. As a result there are a few scenarios in which the total amount of questions (and thus the progress you made) can alter while filling out the form. These situations all have to do with logic being executed in the form, namely:</p>
<ul>
  <li>Branch logic;</li>
  <li>Iterating branch logic;</li>
  <li>Skip logic.</li>
</ul>

<h3 id="ahead-branch-logic">Branch logic</h3>
<p>With <a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/">branch logic</a> in your Tripetto forms, you can show a part of your form when certain conditions are met. For example to show a group of questions only when a respondent selected a specific option in a multiple choice question.</p>
<p>This influences the progressbar at the moment the branch conditions are met and thus that branch is shown to the respondent. Up to that moment the questions inside that branch did not count for the total amount of questions in the progressbar. At the moment the branch becomes available, the amount of questions inside that branch will be added up to the total questions in the progressbar.</p>

<h3 id="ahead-iterating-branch-logic">Iterating branch logic</h3>
<p>A more advanced type of logic is <a href="{{ page.base }}help/articles/how-to-repeat-follow-up-for-multiple-selected-options/">iterating branch logic</a>, which loops a certain branch for multiple branch conditions. For example to ask follow-up questions for each selected option in a multi-select dropdown question.</p>
<p>This influences the total amount of questions in the progressbar, depending on the amount of questions inside the iterating branch and the times that the branch gets executed. Each time a branch condition is met, the amount of questions inside that branch will be added up to the total questions in the progressbar.</p>

<h3 id="ahead-skip-logic">Skip logic</h3>
<p>It's also possible to <a href="{{ page.base }}help/articles/how-to-skip-questions-or-parts-of-your-form/">skip questions or parts of your form</a>. For example when based on a given answer certain questions are not relevant for that respondent.</p>
<p>This influences the total amount of questions in the progressbar. At first sight these questions were part of the total amount of questions. Once they are jumped though, these questions are no longer part of the form and thus get subtracted from the total questions in the progressbar.</p>
<hr />

<h2 id="hide-upcoming-sections">Hide upcoming sections</h2>
<p>Since the <a href="{{ page.base }}changelog/">October 2023 update</a> of Tripetto, the above described behavior is the default behavior of autoscroll forms and thus the progressbar. There are situations though in which the old behavior is preferred. In that case you can easily revert to that behavior.</p>
<p>At the top menu bar of the form builder click <code><i class="fas fa-magic"></i> Customize</code><i class="fas fa-arrow-right"></i><code>Styles</code>. Now enable the visibility setting <code>Hide upcoming sections when a (required) block fails validation</code>.</p>
<p>Now, instead of displaying the whole form at once, the form will only look as far ahead as it can. There are a few elements in your Tripetto form that can have effect on that, namely:</p>
<ul>
  <li>Sections;</li>
  <li>Required questions;</li>
  <li>Branch logic.</li>
</ul>

<h3 id="progressive-sections">Sections</h3>
<p>Sections help you to group your questions. The progressbar can always count all questions that are grouped in one section.</p>
<p>For example, if you only have one section with 3 questions inside that section, the progressbar will count all 3 questions right away.</p>

<h3 id="progressive-required-questions">Required questions</h3>
<p>Required questions work together with sections. If a section has one or more required questions, the progressbar waits for the respondent to fill out those required questions inside that section, before the form and the progressbar can continue.</p>
<p>For example, if you have a first section with 2 questions of which 1 question is required to fill out, the progressbar can only count the questions inside that first section. If the respondent has answered that required question, the form can continue and the progressbar will go further with counting the upcoming questions.</p>

<h3 id="progressive-branch-logic">Branch logic</h3>
<p>If you use branch logic in your form, the progressbar waits for the answers to the questions that determine your branch conditions. Only after those questions are answered the form and the progressbar know if a certain branch needs to be shown or not. And that will affect your progressbar of course.</p>
<p>Inside branches it also depends on the usage of sections and required questions how far the progressbar can look ahead.</p>
<p>For example, if you have an iterating branch based on multiple selected options, the progressbar will count the number of questions inside that branch. If there are required questions inside that iterating branch, the progressbar will add the amount of questions per iteration and only look further if the required questions are answered.</p>
<hr />

<h2 id="hide-progressbar">Hide progressbar</h2>
<p>If you don't want to show the progressbar, you can simply hide it. To open the options of your form, go to the top menu bar of the form builder and click <code><i class="fas fa-magic"></i> Customize</code><i class="fas fa-arrow-right"></i><code>Styles</code>.</p>
<p>At the bottom of the Style pane you will see a group of options. You have two options to hide the progressbar:</p>
<ul>
  <li><code>Display progressbar</code> - You can simply enable or disable this setting to show or hide the progressbar in your form.</li>
  <li><code>Show navigation bar</code> - You can determine when the whole navigation bar may be shown. The progressbar is part of that navigation bar, so if you don't show the navigation bar, the progressbar also will not be visible.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/04-styling.png" width="376" height="176" alt="Screenshot of styling in Tripetto" loading="lazy" />
  <figcaption>The options in the Style screen to show or hide the progressbar.</figcaption>
</figure>
