---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-remove-branding-in-studio-forms/
title: Remove Tripetto branding in studio forms - Tripetto Help Center
description: By default Tripetto shows some Tripetto branding in your forms, but you can hide that by purchasing the studio form upgrades.
article_title: How to remove Tripetto branding in studio forms
article_folder: branding
author: martijn
time: 3
category_id: customization
subcategory: customization_styling
areas: [studio]
---
<p>By default Tripetto shows some Tripetto branding in your forms, but you can hide that by purchasing the studio form upgrades.</p>

<h2 id="about" data-anchor="About Tripetto branding">Why we show our branding</h2>
<p>Most features in the <a href="{{ site.url_app }}" target="_blank" rel="noopener noreferrer">studio at tripetto.app</a> are free for now, as we focus on growing our user base. In exchange for the free usage, we show our name at some subtle places inside each form.</p>
<p>Showing our Tripetto branding inside forms has a high value for us in our growing ambitions. It's simply the best commercial spot we can have. Hopefully people that see your Tripetto forms want to know more about it by simply clicking on our name.</p>
<p>But of course we also understand and respect it when you don't want to show our branding to your users, for example when you use a form with a professional purpose. In that case you can simply hide it by purchasing a studio upgrade.</p>
<hr/>

<h2 id="upgrades">Form upgrades</h2>
<p>In the Tripetto studio we offer optional form upgrades that unlock the following features:</p>
<ul>
  <li><strong>Removable Tripetto branding</strong> in the form and emails that are sent from the form<sup>1</sup>;</li>
  <li><strong>Multiple webhook connections</strong> to 1.000+ services using Make, Zapier, Pabbly Connect and custom webhooks;</li>
  <li><strong>Form activity tracking</strong> with Google Analytics, Google Tag Manager, Facebook Pixel and custom tracking codes.</li>
</ul>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}studio/pricing/">Click here to discover the studio form upgrades</a></li>
</ul>
<blockquote>
  <h4>🔖 Ad 1: About shareable link, sender name and address</h4>
  <p>Please note the following:</p>
  <ul>
    <li>The shareable link of your form (starting with <code>tripetto.app/run/</code>) will always remain like that. It's not possible to customize that. If you want to distribute the form via your own domain/site, you can always use the <a href="{{ page.base }}help/articles/how-to-embed-a-form-from-the-studio-into-your-website/">embed code</a>.</li>
    <li>Emails will always be sent using <code>Tripetto <no-reply@tripetto.com></code> as the sender. It's not possible to customize that, because the emails are sent from the tripetto.com infrastructure, which is not allowed to send emails on behalf of external domains.</li>
  </ul>
</blockquote>

<h3 id="pricing">Pricing</h3>
<p>We offer 2 options to purchase the upgrades:</p>
<ul>
  <li><strong>Unlock 1</strong> - Unlock the upgrades for <u>a single designated form</u>. This upgrade is pay-once (${{ site.pricing_studio_unlock }}) and per form. You can <a href="{{ page.base }}help/articles/how-to-purchase-and-activate-form-upgrades-in-the-studio/">purchase it</a> directly from the studio.</li>
  <li><strong>Unlock All</strong> - Unlock the upgrades for <u>any and all of your forms</u>. This upgrade is a total unlock at the account level. <a href="{{ page.base }}studio/unlock-all/">Contact us</a> to get it.</li>
</ul>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}studio/pricing/">Click here to learn more about pricing</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-purchase-and-activate-form-upgrades-in-the-studio/">Click here to learn how to purchase form upgrades</a></li>
</ul>
<hr/>

<h2 id="upgrade_use">Hide Tripetto branding</h2>
<p>After your form upgrades are activated, you can hide the Tripetto branding in your form: Open the designated form in the form builder<i class="fas fa-arrow-right"></i>Click <code>Customize</code> at the top menu bar<i class="fas fa-arrow-right"></i>Click <code>Styles</code><i class="fas fa-arrow-right"></i>Enable the option <code>Hide all the Tripetto branding</code>.</p>

<blockquote>
  <h4>🧯 Troubleshooting: Branding still showing</h4>
  <p>It could be your browser uses a cached version of your form. In that case please perform a 'hard refresh' to the page your form is embedded in:</p>
  <p><i class="fab fa-windows fa-fw"></i> Windows: hold <code>CTRL</code> key + Click <code>Refresh</code> button in browser<br/><i class="fab fa-apple fa-fw"></i> MacOS: hold <code>Apple</code> key or <code>CMD</code> key + Click <code>Refresh</code> button in browser</p>
  <p>This should load the latest version of the form, without the branding.</p>
</blockquote>
