---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-switch-between-form-faces/
title: Switch between form faces - Tripetto Help Center
description: One of the coolest things in Tripetto is you can switch each form between three different form layouts, giving your form a whole different look and feel.
article_title: How to switch between form faces
article_id: form_faces
article_folder: customize
article_video: customization-form-faces
author: mark
time: 3
time_video: 2
category_id: customization
subcategory: customization_faces
areas: [studio, wordpress]
---
<p>One of the coolest things in Tripetto is you can switch each form between three different <strong>form faces</strong>, giving your form a whole different look and feel. You can determine which form face helps you best to reach your goals.</p>

<h2 id="about">About form faces</h2>
<p>Form faces stand for different layouts of your forms. They totally change the way your form gets presented to your audience. It affects the whole experience you give to your respondents while filling out your form.</p>
<div>
  <a href="https://tripetto.com/form-layouts/" class="blocklink">
    <div>
      <span class="title">Perfect form layouts with convertible form faces</span>
      <span class="description">Easily switch between totally different form experiences. Choose a Typeform-like, a chat, or a classic form face, and fully customize it for the optimal fit with your audience.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" />
    </div>
  </a>
</div>
<p>But why? Maybe for one use case it works best to show your form like it's a real chat, but for another use case it might be better to show your form like a more traditional form. That's why you're totally free to determine what works best for each form that you build. And we give you the choice out of three form faces:</p>
<ul>
  <li>Autoscroll;</li>
  <li>Chat;</li>
  <li>Classic.</li>
</ul>
<p>Let's have a quick look at each form face.</p>
<h3 id="autoscroll">Autoscroll form face</h3>
<p>The autoscroll form face presents your questions one-by-one, giving it a modern feeling and maximized attention to each individual question. You really feel that your form is smart!<br/><i>Perfect to create surveys, like customer satisfaction surveys.</i></p>
<figure>
  <img src="{{ page.base }}images/scenes/autoscroll-customer-satisfaction.webp" width="2220" height="1435" alt="Screenshot of a autoscroll form face in Tripetto" class="no-shadow" loading="lazy" />
  <figcaption>Example of a customer satisfaction survey in the autoscroll form face.</figcaption>
</figure>

<h3 id="chat">Chat form face</h3>
<p>The chat form face presents your questions in a real chat interface, making it feel like a real online conversation.<br/><i>Perfect to create chatbots, like support interactions.</i></p>
<figure>
  <img src="{{ page.base }}images/scenes/chat-wedding-rsvp.webp" width="2220" height="1435" alt="Screenshot of a chat form face in Tripetto" class="no-shadow" loading="lazy" />
  <figcaption>Example of a wedding RSBP form in the chat form face.</figcaption>
</figure>

<h3 id="classic">Classic form face</h3>
<p>The classic form face presents your form in a more traditional way, with multiple questions at one time, but of course with all benefits of smart logic and a good way of styling.<br/><i>Perfect to create website forms, like contact forms.</i></p>
<figure>
  <img src="{{ page.base }}images/scenes/classic-restaurant-reservation.webp" width="2220" height="1435" alt="Screenshot of a classic form face in Tripetto" class="no-shadow" loading="lazy" />
  <figcaption>Example of a restaurant reservation form in the classic form face.</figcaption>
</figure>
<hr />

<h2 id="switch" data-anchor="Switch form faces">How to switch form faces</h2>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-switch.gif" width="286" height="212" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Switch your form face.</figcaption>
</figure>
<p>At the top menu bar of the form builder click <code><i class="fas fa-magic"></i> Customize</code>, or use the dropdown at the top of the preview. You can now choose between the different form faces. Just try them out, because you can always simply switch between them.</p>
<p>The form face you select in the builder is the form face that your respondents get to see and use.</p>
<blockquote>
  <h4>💡 Tip: Use live form preview</h4>
  <p>The live preview in the form builder is your biggest friend while styling. It always updates to what you're doing, so also if you switch the form face, this will immediately be visible in the live preview. More information:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-let-the-live-preview-work-for-you/">How to let the live preview work for you</a></li>
  </ul>
</blockquote>
<p>You don't have to worry about the structure of your form: everything you build in the form builder is available in all form faces. Just experiment what works best for your case. Even your styling and translations are preserved while switching the form faces.</p>

<h3 id="options">Additional options</h3>
<p>Each form face has its own additional options to improve the finishing touch, like extra settings, extra styling options and extra translatable messages. Read the help articles below to see these additional options for each form face:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/discover-additional-options-for-autoscroll-form-face/">Additional options for the <strong>autoscroll form face</strong></a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/discover-additional-options-for-chat-form-face/">Additional options for the <strong>chat form face</strong></a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/discover-additional-options-for-classic-form-face/">Additional options for the <strong>classic form face</strong></a></li>
</ul>

