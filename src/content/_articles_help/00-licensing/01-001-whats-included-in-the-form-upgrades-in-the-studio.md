---
layout: help-article
base: ../../../
permalink: /help/articles/whats-included-in-the-form-upgrades-in-the-studio/
title: Included in the form upgrades in the studio - Tripetto Help Center
description: See what features get unlocked when you purchase form upgrades in the studio. You can purchase form upgrades per form or per account.
article_title: What's included in the form upgrades in the studio
author: martijn
time: 2
category_id: studio-licensing
subcategory: licensing_upgrade
areas: [studio]
---
<p>See what features get unlocked when you purchase form upgrades in the studio. You can purchase form upgrades per form or per account.</p>

<h2 id="upgrades">Form upgrades</h2>
<p>In the Tripetto studio we offer optional upgrades that unlock the following features:</p>
<ul>
  <li><strong>Removable Tripetto branding</strong> in the form and emails that are sent from the form<sup>1</sup>;</li>
  <li><strong>Multiple webhook connections</strong> to 1.000+ services using Make, Zapier, Pabbly Connect and custom webhooks;</li>
  <li><strong>Form activity tracking</strong> with Google Analytics, Google Tag Manager, Facebook Pixel and custom tracking codes.</li>
</ul>
<blockquote>
  <h4>🔖 Ad 1: About shareable link, sender name and address</h4>
  <p>Please note the following before purchasing a form upgrade:</p>
  <ul>
    <li>The shareable link of your form (starting with <code>tripetto.app/run/</code>) will always remain like that. It's not possible to customize that. If you want to distribute the form via your own domain/site, you can always use the <a href="{{ page.base }}help/articles/how-to-embed-a-form-from-the-studio-into-your-website/">embed code</a>.</li>
    <li>Emails will always be sent using <code>Tripetto <no-reply@tripetto.com></code> as the sender. It's not possible to customize that, because the emails are sent from the tripetto.com infrastructure, which is not allowed to send emails on behalf of external domains.</li>
  </ul>
</blockquote>

<h3 id="pricing">Pricing</h3>
<p>We offer 2 options to purchase the upgrades:</p>
<ul>
  <li><strong>Unlock 1</strong> - Unlock the upgrades for <u>a single designated form</u>. This upgrade is pay-once (${{ site.pricing_studio_unlock }}) and per form. You can <a href="{{ page.base }}help/articles/how-to-purchase-and-activate-form-upgrades-in-the-studio/">purchase it</a> directly from the studio.</li>
  <li><strong>Unlock All</strong> - Unlock the upgrades for <u>any and all of your forms</u>. This upgrade is a total unlock at the account level. <a href="{{ page.base }}studio/unlock-all/">Contact us</a> to get it.</li>
</ul>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}studio/pricing/">Click here to learn more about pricing</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-purchase-and-activate-form-upgrades-in-the-studio/">Click here to learn how to purchase form upgrades</a></li>
</ul>
