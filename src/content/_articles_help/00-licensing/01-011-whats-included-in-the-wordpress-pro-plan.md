---
layout: help-article
base: ../../../
permalink: /help/articles/whats-included-in-the-wordpress-pro-plan/
title: Included in the WordPress Pro plan - Tripetto Help Center
description: See what features get unlocked when you purchase a Pro license for the WordPress plugin.
article_title: What's included in the WordPress Pro plan
author: martijn
time: 2
category_id: wordpress-licensing
subcategory: licensing_pro
areas: [wordpress]
---
<p>See what features get unlocked when you purchase a Pro license for the WordPress plugin.</p>

<h2 id="pro_plan">About Tripetto Pro</h2>
<p>The Pro plan for the Tripetto WordPress plugin includes the following features:</p>
<ul>
  <li><strong>Advanced blocks</strong> to use unlimited subforms (for improved organization and structuring of large forms) and the signature block in your forms;</li>
  <li><strong>All action blocks</strong> to perform advanced actions in your forms, including calculator, custom variable, hidden field, send email and set value blocks;</li>
  <li><strong>Removable Tripetto branding</strong> in your forms and emails that are sent from your forms;</li>
  <li><strong>Notifications</strong> to email and Slack, including all form data;</li>
  <li><strong>Connections</strong> to 1.000+ services using Make, Zapier, Pabbly Connect and custom webhooks;</li>
  <li><strong>Form activity tracking</strong> with Google Analytics, Google Tag Manager, Facebook Pixel and custom tracking codes;</li>
  <li><strong>WordPress roles management</strong> to control user role access and capabilities;</li>
  <li><strong>Priority support + updates</strong>.</li>
</ul>

<h3 id="pro_pricing">Pricing</h3>
<p>Tripetto's <a href="{{ page.base }}wordpress/pricing/">Pro plan</a> always includes all features and priority support. Its pricing only varies depending on the number of sites you want to equip with Tripetto:</p>
<ul>
  <li><strong>Single-Site</strong> - Lets you equip a maximum of 1 WordPress project with Tripetto;</li>
  <li><strong>5-Sites</strong> - Lets you equip a maximum of 5 WordPress projects with Tripetto;</li>
  <li><strong>Unlimited</strong> - Lets you equip unlimited WordPress projects with Tripetto.</li>
</ul>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}wordpress/pricing/">Discover and purchase your WordPress Pro license</a></li>
</ul>
<hr/>

<h2 id="purchase" data-anchor="Purchase">Purchase Tripetto Pro</h2>
<p>You can purchase Tripetto Pro for WordPress via our website or via your WP Admin directly. More information:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}wordpress/pricing/">WordPress Pro pricing</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-upgrade-the-wordpress-plugin-to-the-pro-plan/">How to upgrade the WordPress plugin to the Pro plan</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-manage-your-wordpress-pro-license/">How to manage your WordPress Pro license</a></li>
</ul>
