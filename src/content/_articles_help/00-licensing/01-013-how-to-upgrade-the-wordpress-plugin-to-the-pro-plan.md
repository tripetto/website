---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-upgrade-the-wordpress-plugin-to-the-pro-plan/
title: Upgrade the WordPress plugin to the Pro plan - Tripetto Help Center
description: Learn how to purchase a Pro license and unlock all pro features in the WordPress plugin.
article_title: How to upgrade the WordPress plugin to the Pro plan
article_folder: wordpress-upgrade
author: jurgen
time: 3
category_id: wordpress-licensing
subcategory: licensing_pro
areas: [wordpress]
redirect_from:
- /help/articles/how-to-upgrade-the-wordpress-plugin-to-premium/
---
<p>The Tripetto WordPress plugin always helps you to build awesome conversational forms, but also offers additional pro features that can be unlocked by <a href="{{ page.base }}wordpress/pricing/">upgrading to the Pro plan</a>. Let's see show how you can upgrade to the Pro plan!</p>

<h2 id="pro-plan">About Tripetto Pro</h2>
<p>Tripetto's <a href="{{ page.base }}wordpress/pricing/">Pro plan</a> always includes all features and priority support. Its pricing only varies depending on the number of sites you want to equip with Tripetto:</p>
<ul>
  <li><strong>Single-Site</strong> - Lets you equip a maximum of 1 WordPress project with Tripetto;</li>
  <li><strong>5-Sites</strong> - Lets you equip a maximum of 5 WordPress projects with Tripetto;</li>
  <li><strong>Unlimited</strong> - Lets you equip unlimited WordPress projects with Tripetto.</li>
</ul>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}wordpress/pricing/">Click here to discover and purchase the Pro plan</a></li>
</ul>
<hr/>

<h2 id="purchase" data-anchor="Purchase">Purchase Tripetto Pro</h2>
<p>The Pro plan can be purchased in two ways:</p>
<ul>
  <li>Via our website;</li>
  <li>Via your WP Admin.</li>
</ul>

<h3 id="website" data-anchor="Via our website">Purchase via our website</h3>
<p>On <a href="{{ page.base }}wordpress/pricing/">our pricing page</a> select your preferred amount of sites and then follow the steps to checkout.</p>

<h3 id="wp_admin" data-anchor="Via your WP Admin">Purchase via your WP Admin</h3>
<p>Inside your WP Admin navigate to the Tripetto plugin menu<i class="fas fa-arrow-right"></i>Click <code>Upgrade</code><i class="fas fa-arrow-right"></i>Select your preferred amount of sites<i class="fas fa-arrow-right"></i>Follow the steps to checkout.</p>
<hr/>

<h2 id="install" data-anchor="Install">Install Tripetto Pro</h2>
<p>After checkout you will receive everything you need to proceed, namely:</p>
<ul>
  <li>A .zip file with the Pro version of the plugin, which you can download after the checkout. Save this file to your computer. You don't have to unpack the .zip file;</li>
  <li>Your personal license key, which will be sent by email.</li>
</ul>
<p>To install the Pro plugin, open your WP Admin<i class="fas fa-arrow-right"></i>Navigate to <code>Plugins</code><i class="fas fa-arrow-right"></i>Click <code>Add New</code><i class="fas fa-arrow-right"></i>Click <code>Upload Plugin</code><i class="fas fa-arrow-right"></i>Select the .zip file from your computer<i class="fas fa-arrow-right"></i>Click <code>Install Now</code><i class="fas fa-arrow-right"></i>Click <code>Activate</code><i class="fas fa-arrow-right"></i>Enter your personal license key.</p>
<p>Now the Pro version of Tripetto is installed and ready to use right away!</p>
<blockquote>
  <h4>💡 What happens with your data?</h4>
  <p>If you already had Tripetto installed, for example the free version, that plugin will be deactivated automatically during the upgrade process. Any existing forms and entries will be availabe in the Pro version right away, so you won't lose any data.</p>
</blockquote>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-upload-pro.png" width="1200" height="760" alt="Screenshot of WordPress plugin upload" loading="lazy" />
  <figcaption>Upload Pro version of the plugin.</figcaption>
</figure>
<hr/>

<h2 id="manage" data-anchor="Manage license">Manage Tripetto Pro license</h2>
<p>After you have purchased a Pro license for the Tripetto WordPress plugin, you can manage your license activation, account information and billing information via <strong>our licensing partner Freemius</strong>. They take care of distribution, licenses, subscriptions, payments, billing, etc. Basically everything related to your Tripetto Pro license. More information:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-manage-your-wordpress-pro-license/">How to manage your WordPress Pro license</a></li>
</ul>
