---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-your-wordpress-pro-license-in-test-environments/
title: Use WordPress Pro license in test environments - Tripetto Help Center
description: You can use your WordPress Pro license in test environments without affecting your license's maximum allowed sites.
article_title: How to use your WordPress Pro license in test environments
author: mark
time: 3
category_id: wordpress-licensing
subcategory: licensing_management
areas: [wordpress]
---
<p>You can use your WordPress Pro license in test environments without affecting your license's maximum allowed sites. That enables you to test and configure everything in Tripetto in your test environment before deploying to your live environment.</p>

<h2 id="test-environments">About test environments</h2>
<p>It's commonly used to develop your WordPress site in a separate environment than the live environment of your site. That way you can develop and test everything before you deploy it to your visitors.</p>
<p>In most cases your test environment is totally separated from your live environment, resulting in separate installations of plugins, like the Tripetto WordPress plugin. And thus also the need of multiple Pro licenses for each of your plugin installations.</p>
<p>Luckily our license partner <a href="https://freemius.com/" target="_blank" rel="noopener noreferrer"><strong>Freemius</strong></a> has got you covered for this: all Tripetto Pro licenses can be activated in the most used test environments without affecting your license's maximum allowed sites.</p>
<p>Freemius has a list of test domains that they allow for this purpose. Please see the list below or <a href="https://freemius.com/help/documentation/selling-with-freemius/license-utilization/" target="_blank" rel="noopener noreferrer">have a look at the latest version of this list at the website of Freemius</a>.</p>
<div>
  <a href="https://freemius.com/help/documentation/selling-with-freemius/license-utilization/" target="_blank" rel="noopener noreferrer" class="blocklink">
    <div>
      <span class="title">License Utilization - Freemius<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Using One License for Live / Production, Staging, Dev and Localhost sites</span>
      <span class="url">freemius.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-freemius.svg" width="160" height="160" alt="Freemius logo" loading="lazy" />
    </div>
  </a>
</div>
<hr/>

<h2 id="allowlist">Allowed test domains</h2>
<p>The following top level domains and subdomains are handled as test environments and thus can be used without affecting your license’s maximum allowed sites.</p>

<h3 id="tld">Top Level Domains (TLD)</h3>
<p>The following TLD's are considered as test environments:</p>
<ul>
<li><code>*.dev</code></li>
<li><code>*.dev.cc</code> (DesktopServer)</li>
<li><code>*.test</code></li>
<li><code>*.local</code></li>
<li><code>*.staging</code></li>
<li><code>*.example</code></li>
<li><code>*.invalid</code></li>
<li><code>*.myftpupload.com</code> (GoDaddy)</li>
<li><code>*.cloudwaysapps.com</code> (Cloudways)</li>
<li><code>*.wpsandbox.pro</code> (WPSandbox)</li>
<li><code>*.ngrok.io</code> (tunneling)</li>
</ul>

<h3 id="subdomains">Subdomains</h3>
<p>The following subdomains are considered as test environments:</p>
<ul>
<li><code>local.*</code></li>
<li><code>dev.*</code></li>
<li><code>test.*</code></li>
<li><code>stage.*</code></li>
<li><code>staging.*</code></li>
<li><code>stagingN.*</code> (SiteGround; <code>N</code> is an unsigned int)</li>
<li><code>*.wpengine.com</code> (WP Engine)</li>
<li><code>dev-*.pantheonsite.io</code> (Pantheon)</li>
<li><code>test-*.pantheonsite.io</code> (Pantheon)</li>
<li><code>staging-*.kinsta.com</code> (Kinsta)</li>
<li><code>staging-*.kinsta.cloud</code> (Kinsta)</li>
<li><code>*.mystagingwebsite.com</code> (Pressable)</li>
</ul>

<h3 id="localhost">localhost</h3>
<p>Additionally, <code>localhost</code> (with any port), will also be treated as a test domain.</p>

<h3 id="custom">Custom domain(s)</h3>
<p>Using a test domain that's not on the list? We can add custom test domains for you. Please <a href="{{ page.base }}support/">contact us</a> and let us know the test domain(s) you'd like to use.</p>
