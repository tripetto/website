---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-purchase-and-activate-form-upgrades-in-the-studio/
title: Purchase and activate form upgrades in studio - Tripetto Help Center
description: Learn how to purchase and activate form upgrades in the studio to remove Tripetto branding, connect your data to multiple webhooks and track form activity.
article_title: How to purchase and activate form upgrades in the studio
article_folder: studio-upgrades
author: martijn
time: 3
category_id: studio-licensing
subcategory: licensing_upgrade
areas: [studio]
redirect_from:
- /help/articles/how-to-activate-form-upgrades-in-the-studio/
---
<p>Learn how to purchase and activate form upgrades in the studio to remove Tripetto branding, connect your data to multiple webhooks and smartly track form activity.</p>

<h2 id="upgrades">Form upgrades</h2>
<p>In the Tripetto studio we offer optional form upgrades that unlock the following features:</p>
<ul>
  <li><strong>Removable Tripetto branding</strong> in the form and emails that are sent from the form<sup>1</sup>;</li>
  <li><strong>Multiple webhook connections</strong> to 1.000+ services using Make, Zapier, Pabbly Connect and custom webhooks;</li>
  <li><strong>Form activity tracking</strong> with Google Analytics, Google Tag Manager, Facebook Pixel and custom tracking codes.</li>
</ul>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}studio/pricing/">Click here to discover the studio form upgrades</a></li>
</ul>
<blockquote>
  <h4>🔖 Ad 1: About shareable link, sender name and address</h4>
  <p>Please note the following before purchasing a form upgrade:</p>
  <ul>
    <li>The shareable link of your form (starting with <code>tripetto.app/run/</code>) will always remain like that. It's not possible to customize that. If you want to distribute the form via your own domain/site, you can always use the <a href="{{ page.base }}help/articles/how-to-embed-a-form-from-the-studio-into-your-website/">embed code</a>.</li>
    <li>Emails will always be sent using <code>Tripetto <no-reply@tripetto.com></code> as the sender. It's not possible to customize that, because the emails are sent from the tripetto.com infrastructure, which is not allowed to send emails on behalf of external domains.</li>
  </ul>
</blockquote>
<hr/>

<h2 id="purchase">Purchase form upgrades</h2>
<p>We offer 2 options to purchase the upgrades:</p>
<ul>
  <li><strong>Unlock 1</strong> - Unlock the upgrades for <u>a single designated form</u>. This upgrade is pay-once (${{ site.pricing_studio_unlock }}) and per form. You can <a href="#purchase_unlock_1" class="anchor">purchase it</a> directly from the studio.</li>
  <li><strong>Unlock All</strong> - Unlock the upgrades for <u>any and all of your forms</u>. This upgrade is a total unlock at the account level. <a href="#purchase_unlock_all" class="anchor">Contact us</a> to get it.</li>
</ul>

<h3 id="purchase_unlock_1" data-anchor="Unlock 1">Purchase Unlock 1</h3>
<p>To unlock the upgrades for <u>a single designated form</u> the price is ${{ site.pricing_studio_unlock }} per form (pay-once). Please follow these steps:</p>
<ol>
  <li><strong>Open form</strong> - Open the form that you want to upgrade in the form builder at <a href="{{ site.url_app }}" target="_blank" rel="noopener noreferrer">tripetto.app</a><i class="fas fa-arrow-right"></i>Click the <code><i class="fas fa-bars"></i></code> icon at the top menu bar<i class="fas fa-arrow-right"></i>Click <code>Unlock all features</code>. The purchase form will open at the right of your screen.</li>
  <li><strong>Purchase</strong> - Check your purchase and complete the checkout. You can pay with any credit card or via PayPal. After completing the checkout, you will receive a confirmation mail, including your invoice.</li>
  <li><strong>Activate</strong> - After payment, we will manually unlock the upgrades for your form and let you know when that's done. This is a manual process for us. We always do our best to do this as soon as possible after your payment.</li>
  <li><strong>Use</strong> - After activation you can <a href="#upgrades_use" class="anchor">use the upgrades</a> in your form.</li>
</ol>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/upgrade-request.png" width="1200" height="760" alt="Screenshot of the studio of Tripetto" loading="lazy" />
  <figcaption>Purchase your upgrades for one form directly in the studio.</figcaption>
</figure>

<h3 id="purchase_unlock_all" data-anchor="Unlock All">Purchase Unlock All</h3>
<p>To unlock the upgrades for <u>any and all of your forms</u> you can contact us to upgrade your whole account. Please follow these steps:</p>
<ol>
  <li><strong>Contact us</strong> - Please fill out the <a href="{{ page.base }}studio/unlock-all/">request form</a> for the Unlock All upgrade. We'll contact you as soon as possible with a personal upgrade offer.</li>
  <li><strong>Purchase</strong> - After your agreement on our offer, we can proceed with the purchase. We'll guide you how to do this.</li>
  <li><strong>Activate</strong> - After payment, we will manually unlock the upgrades for your account and let you know when that's done. This is a manual process for us. We always do our best to do this as soon as possible after your payment.</li>
  <li><strong>Use</strong> - After activation you can <a href="#upgrades_use" class="anchor">use the upgrades</a> in all forms in your account.</li>
</ol>
<hr/>

<h2 id="upgrades_use">Use form upgrades</h2>
<p>After your form upgrades are activated, you can use them as follows:</p>
<ul>
  <li><strong>Removable Tripetto branding</strong>: Click <code>Customize</code> at the top menu bar<i class="fas fa-arrow-right"></i>Click <code>Styles</code><i class="fas fa-arrow-right"></i>Enable the option <code>Hide all the Tripetto branding</code>;</li>
  <li><strong>Multiple webhook connections</strong>: Click <code>Automate</code> at the top menu bar<i class="fas fa-arrow-right"></i>Click <code>Connections</code><i class="fas fa-arrow-right"></i>Enable and configure the desired connection(s);</li>
  <li><strong>Form activity tracking</strong>: Click <code>Automate</code> at the top menu bar<i class="fas fa-arrow-right"></i>Click <code>Tracking</code><i class="fas fa-arrow-right"></i>Enable and configure the desired tracking service(s).</li>
</ul>
