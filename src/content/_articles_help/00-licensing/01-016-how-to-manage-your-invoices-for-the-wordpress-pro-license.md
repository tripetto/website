---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-manage-your-invoices-for-the-wordpress-pro-license/
title: Manage invoices for the WordPress Pro license - Tripetto Help Center
description: For each payment of the annual Pro license you can download an invoice via our licensing partner Freemius.
article_title: How to manage your invoices for the WordPress Pro license
author: martijn
time: 1
category_id: wordpress-licensing
subcategory: licensing_invoicing
areas: [wordpress]
---
<p>For each payment of the annual Pro license you can download an invoice via our licensing partner Freemius. You can determine the billing information that gets used for those invoices.</p>

<h2 id="download">Download invoice</h2>
<p>After you purchased a Pro license for the WordPress plugin, you will receive a confirmation mail via our license partner Freemius. That confirmation mail contains a download link to your invoice, which will download a PDF version of your invoice to your computer.</p>
<hr/>

<h2 id="update">Update billing information</h2>
<p>In the confirmation mail you receive after your purchase, you will find the instructions to create an account at the <a href="https://users.freemius.com/" target="_blank" rel="noopener noreferrer"><strong>Freemius User Dashboard</strong></a>. That's the central place to manage everything related to your purchase.</p>
<div>
  <a href="https://users.freemius.com/" target="_blank" rel="noopener noreferrer" class="blocklink">
    <div>
      <span class="title">Freemius User Dashboard<i class="fas fa-external-link-alt"></i></span>
      <span class="description">A central place to manage everything related to your Tripetto purchases.</span>
      <span class="url">users.freemius.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-freemius.svg" width="160" height="160" alt="Freemius logo" loading="lazy" />
    </div>
  </a>
</div>
<p>To update the billing information that gets used in your invoices, login to the Freemius User Dashboard. Then go to <code>My profile</code> and simply update the billing information. Now, if you go to <code>Orders History</code>, you can download the invoices with your updated business details.</p>
