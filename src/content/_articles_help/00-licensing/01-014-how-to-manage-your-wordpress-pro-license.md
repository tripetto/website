---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-manage-your-wordpress-pro-license/
title: Manage WordPress Pro license - Tripetto Help Center
description: Learn how to manage your Pro license(s) for the WordPress plugin, including license activation, account information and billing information.
article_title: How to manage your WordPress Pro license
article_folder: wordpress-manage-licensing
author: jurgen
time: 3
time_video: 0
category_id: wordpress-licensing
subcategory: licensing_management
areas: [wordpress]
redirect_from:
- /help/articles/how-to-manage-wordpress-plugin-licensing/
- /help/articles/how-to-manage-pro-licenses-for-the-wordpress-plugin/
---
<p>Once you have purchased a <a href="{{ page.base }}wordpress/pricing/">Pro plan for the Tripetto WordPress plugin</a>, you can manage your license activation, account information and billing information via our licensing partner Freemius.</p>

<h2 id="freemius">About Freemius</h2>
<p>For everything that has to do with your Pro licenses for Tripetto, we use <a href="https://freemius.com/" target="_blank" rel="noopener noreferrer"><strong>Freemius</strong></a> as our partner. They take care of distribution, licenses, subscriptions, payments, billing, etc. Basically everything related to your Tripetto Pro license.</p>

<h3 id="purchase">Purchase license</h3>
<p>If you purchase a Pro plan for the Tripetto WordPress plugin, you will see Freemius in a few stages:</p>
<ul>
  <li>The checkout of your license, including the payment, is handled by Freemius;</li>
  <li>In your bank account/credit card, you will see Freemius as the recipient of the payment;</li>
  <li>You will receive a confirmation email from Freemius, including the download link for the Pro version of the plugin, your license key and your invoice.</li>
</ul>

<h3 id="manage">Manage license</h3>
<p>After you have purchased a Pro license, you can manage everything related to that license in the Freemius User Dashboard.</p>
<hr/>

<h2 id="user-dashboard">Freemius User Dashboard</h2>
<p>In the confirmation mail you receive after your purchase, you will find the instructions to create an account at the <a href="https://users.freemius.com/" target="_blank" rel="noopener noreferrer"><strong>Freemius User Dashboard</strong></a>. That's the central place to manage everything related to your license(s), for example:</p>
<ul>
  <li>Move your license to a different site;</li>
  <li>Upgrade your license to a different license;</li>
  <li>Update your payment method;</li>
  <li>Update your business details and download updated invoices;</li>
  <li>Cancel your subscription.</li>
</ul>
<div>
  <a href="https://users.freemius.com/" target="_blank" rel="noopener noreferrer" class="blocklink">
    <div>
      <span class="title">Freemius User Dashboard<i class="fas fa-external-link-alt"></i></span>
      <span class="description">A central place to manage everything related to your Tripetto Pro license for WordPress.</span>
      <span class="url">users.freemius.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-freemius.svg" width="160" height="160" alt="Freemius logo" loading="lazy" />
    </div>
  </a>
</div>

<h3 id="move-license" data-anchor="Move license">Move license to different site</h3>
<p>To use your license on a different site than it is installed now, you can deactivate the license.</p>
<p>In the Freemius User Dashboard, go to <code>Websites</code> and select the website you want to deactivate the plugin from. Then click <code>Deactivate</code>. After the license is deactivated, you’ll be able to reuse it on any other WordPress site.</p>

<h3 id="upgrade-license">Upgrade license</h3>
<p>Let's say you want to upgrade your current single-site license to a 5-sites license.</p>
<p>In the Freemius User Dashboard, go to <code>Renewals & Billing</code> and select the license you want to upgrade. Then select the license you want to upgrade to from the <code>Upgrade to...</code> dropdown. The last payment will be automatically prorated.</p>

<h3 id="update-payment">Update payment method</h3>
<p>Let's say your initial payment is done with your PayPal account, but you rather use your credit card from now on.</p>
<p>In the Freemius User Dashboard, go to <code>Renewals & Billing</code> and select the license you want to update the payment method for. Then click <code>Update</code> and checkout with your new payment method. The last payment will be automatically prorated based on the next renewal date.</p>

<h3 id="update-details">Update business details</h3>
<p>During the checkout you can already enter your VAT/Tax ID, but you can also add that afterwards.</p>
<p>In the Freemius User Dashboard, go to <code>My profile</code> and simply update the billing information. Now, if you go to <code>Orders History</code>, you can download the invoices with your updated business details.</p>

<h3 id="cancel-subscription">Cancel subscription</h3>
<p>Of course we don't hope so, but if you want to cancel your subscription you can do that easily.</p>
<p>In the Freemius User Dashboard, go to <code>Renewals & Billing</code> and select the license you want to cancel. Then click <code>Cancel Auto-Renew</code>.</p>





