---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-custom-css-in-embedded-forms/
title: Use custom CSS in embedded forms - Tripetto Help Center
description: In your embedded forms you can add custom CSS to overrule the styling for parts of your form.
article_title: How to use custom CSS in embedded forms
author: jurgen
time: 3
category_id: sharing
subcategory: sharing_embed
areas: [studio, wordpress]
scripts: [prism]
stylesheets: [prism-dark]
---
<p>In your embedded forms you can add custom CSS to overrule the styling for parts of your form.</p>

<h2 id="when-to-use">When to use</h2>
<p>Before using custom CSS, please first have a look at the <a href="{{ page.base }}help/articles/how-to-style-your-forms/">built-in advanced styling options</a> to see if you can achieve your desired styling with that.</p>
<p>If the built-in styling options are not sufficient for your case, you can add custom CSS to parts of your form. That will overrule the built-in styling of your form. Still, Tripetto is not aimed for heavy CSS customization, so not all parts of your Tripetto forms can be customized.</p>
<p>CSS customization is only possible for embedded forms. Forms shared via the shareable link can not contain custom CSS.</p>
<blockquote>
  <h4>🚧 Warning: Disclaimer for using custom CSS</h4>
  <p>The usage of custom CSS is at own risk. We don't offer support on the usage of custom CSS or forms that include custom CSS. We can't guarantee the working of your custom CSS in future updates of Tripetto.</p>
</blockquote>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>Customizing CSS from outside the Tripetto embed is not possible, so you cannot add CSS to your own stylesheet to overrule Tripetto's CSS. You have to make sure the custom CSS for your Tripetto form is done inside the actual embed of your form. See the specific help article about your embed method to see how you can add the custom CSS to your embed.</p>
<h3 id="develop-custom-css">Develop custom CSS</h3>
<p>Custom CSS in Tripetto embeds can only manipulate CSS inside question blocks of your form. Elements in the form that are not part of a question block (like the welcome/closing messages and form footers) are not customizable.</p>
<p>To add custom CSS to a certain question block, you can address that question block by using the <code>data-block</code> selector and referring to the desired block name by its 'package name'. For the full list of package names that you can use as <code>data-block</code> selector, please have a look at <a href="https://www.npmjs.com/search?q=%40tripetto%20block" target="_blank" rel="noopener noreferrer">this overview of all our packages</a>.</p>
<blockquote>
  <h4>🧯 Troubleshooting: Package names changed with May 2023 update</h4>
  <p>With the May 2023 update of Tripetto, the package names have changed. It's advised to update your custom CSS implementations. Whereas before all package names started with <code>tripetto-</code>, all package names now start with <code>@tripetto/</code>. For example, up to May 2023 the text block package name was <code>tripetto-block-text</code>, but now is <code>@tripetto/block-text</code>.</p>
</blockquote>
<p>Now you can add the custom CSS for that block as a string value. To do so, use the HTML element selector in CSS to address the desired elements inside the referred question block. Don't address elements with the class names, because these are flexible class names which can change overtime.</p>

<h3 id="add-to-embed">Add to embedded form</h3>
<p>After setting up your custom CSS you can add it to your embedded form.</p>

<div class="help_article_studio_only">
  <p>In the embed code that you got from the studio, you can add a parameter named <code>customCSS</code> and insert your custom CSS. For example:</p>
  <pre class="line-numbers"><code class="language-html">&lt;div id="tripetto-div-id">&lt;/div>
&lt;script src="https://cdn.jsdelivr.net/npm/@tripetto/runner">&lt;/script>
&lt;script src="https://cdn.jsdelivr.net/npm/@tripetto/runner-autoscroll">&lt;/script>
&lt;script src="https://cdn.jsdelivr.net/npm/@tripetto/studio">&lt;/script>
&lt;script>
TripettoStudio.form({
    runner: TripettoAutoscroll,
    token: "STUDIO_FORM_TOKEN",
    element: "tripetto-div-id",
    customCSS: "[data-block='@tripetto/block-text'] { YOUR_CUSTOM_CSS }"
});
&lt;/script></code></pre>
  <p>Of course don't forget to publish this new embed code to your website's code.</p>
</div>
<div class="help_article_wp_only">
  <p>If you use the WordPress shortcode to embed your form, you can generate a new shortcode in the Share pane in the Tripetto plugin. You can simply enable the <code>Custom CSS</code> option and insert your custom CSS. After that copy your new shortcode and update it on the place(s) in your website where you use this shortcode.</p>
  <p>If you use the Gutenberg block or Elementor widget you can insert the custom CSS right inside the embed. Navigate to the block/widget of the Tripetto form in your website and edit this. You can now enable the <code>Custom CSS</code> option and insert your custom CSS.</p>
  <p>Of course don't forget to publish this update to your live website.</p>
</div>
<hr/>

<h2 id="examples">Examples</h2>
<p>Let us show you some examples of how to use custom CSS in your embedded forms.</p>
<h3 id="example-one">Customize one question block</h3>
<p>Let's see a reference to add custom CSS to the question type <a href="{{ page.base }}help/articles/how-to-use-the-text-single-line-block/">Text (single line)</a>. The package name of that question type is <code>@tripetto/block-text</code> (<a href="https://www.npmjs.com/package/@tripetto/block-text" target="_blank" rel="noopener noreferrer">see package</a>). The basic custom CSS code for that will look like this:</p>
<pre class="line-numbers"><code class="language-css">[data-block='@tripetto/block-text'] { YOUR CSS }</code></pre>

<p>Now you can add the custom CSS for that block as a string value. To do so, use the HTML element selector in CSS to address the desired element(s) inside that question block. For example to customize the colors of the title and description of the question type <a href="{{ page.base }}help/articles/how-to-use-the-text-single-line-block/">Text (single line)</a>:</p>
<pre class="line-numbers"><code class="language-css">[data-block='@tripetto/block-text'] { h2 { color: blue; } p { color: red; } }</code></pre>

<h3 id="example-multiple">Customize multiple question blocks</h3>
<p>As you can see you have to add the custom CSS to each question type apart. So let's say you want to customize elements for the following question types in your form:</p>
<ul>
<li><a href="{{ page.base }}help/articles/how-to-use-the-rating-block/">Rating</a> - Package name <code>@tripetto/block-rating</code> (<a href="https://www.npmjs.com/package/@tripetto/block-rating" target="_blank" rel="noopener noreferrer">see package</a>);</li>
<li><a href="{{ page.base }}help/articles/how-to-use-the-multiple-choice-block/">Multipe choice</a> - Package name <code>@tripetto/block-multiple-choice</code> (<a href="https://www.npmjs.com/package/@tripetto/block-multiple-choice" target="_blank" rel="noopener noreferrer">see package</a>);</li>
<li><a href="{{ page.base }}help/articles/how-to-use-the-yes-no-block/">Yes/no</a> - Package name <code>@tripetto/block-yes-no</code> (<a href="https://www.npmjs.com/package/@tripetto/block-yes-no" target="_blank" rel="noopener noreferrer">see package</a>).</li>
</ul>
<p>Your custom CSS code for these question types will look like this:</p>
<pre class="line-numbers"><code class="language-css">[data-block='@tripetto/block-rating'] { h2 { color: blue; } p { color: red; } }
[data-block='@tripetto/block-multiple-choice'] { h2 { color: blue; } p { color: red; } }
[data-block='@tripetto/block-yes-no'] { h2 { color: blue; } p { color: red; } }</code></pre>
