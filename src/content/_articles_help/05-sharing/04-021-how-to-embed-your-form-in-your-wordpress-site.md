---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-embed-your-form-in-your-wordpress-site/
title: Embed form in WordPress site - Tripetto Help Center
description: Your Tripetto forms can easily be embedded into any place in your WordPress site, using the WordPress shortcode, Gutenberg block or Elementor widget.
article_title: How to embed your form in your WordPress site
author: jurgen
time: 2
category_id: sharing
subcategory: sharing_embed
areas: [wordpress]
---
<p>Your Tripetto forms can easily be embedded into any place in your WordPress site, using the WordPress shortcode, Gutenberg block or Elementor widget.</p>

<h2 id="when-to-use">When to use</h2>
<p>By embedding your form in your site you can place the form at the exact location where it blends perfectly with your content. All data will be stored inside your own WordPress instance.</p>
<hr/>

<h2 id="embed">Embed in your WordPress site</h2>
<p>We offer several ways to embed your forms from the WordPress plugin into your WordPress site. It largely depends on the WordPress builder you use which method is best and easiest for you:</p>
<ul>
  <li><strong>WordPress shortcode</strong>, if you use a traditional editor;</li>
  <li><strong>Gutenberg block</strong>, if you use the Gutenberg editor;</li>
  <li><strong>Elementor widget</strong>, if you use the Elementor builder.</li>
</ul>

<h3 id="shortcode">WordPress shortcode</h3>
<p>This is the basic implementation method in WordPress. Most page builders will offer a way to use shortcodes to embed an element in your site; in our case that's a Tripetto form.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-shortcode/">How to embed your form in your WordPress site using the shortcode</a></li>
</ul>

<h3 id="gutenberg">Gutenberg block</h3>
<p>If you use the Gutenberg editor in WordPress, we advise to use Tripetto's Gutenberg block. This makes it easy to add and build forms right where you want to publish them.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-gutenberg-block/">How to embed your form in your WordPress site using the Gutenberg block</a></li>
</ul>

<h3 id="elementor">Elementor widget</h3>
<p>If you use the Elementor builder in WordPress, we advise to use Tripetto's Elementor widget. This makes it easy to add forms in your Elementor templates, even within Elementor popups.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-elementor-widget/">How to embed your form in your WordPress site using the Elementor widget</a></li>
</ul>
<hr/>

<h2 id="data">Data storage</h2>
<p>Both the form and collected data are always hosted and stored in your own WordPress instance. Not a single connection related to Tripetto is ever made with an external host other than yours. It's all in your own instance, under your control.</p>
