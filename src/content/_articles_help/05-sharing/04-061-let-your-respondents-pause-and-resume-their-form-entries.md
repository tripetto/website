---
layout: help-article
base: ../../../
permalink: /help/articles/let-your-respondents-pause-and-resume-their-form-entries/
title: Pause and resume form entries - Tripetto Help Center
description: Respondents to your Tripetto forms can pause and resume their form entries, so they can continue later on. Especially handy with large forms.
article_title: Let your respondents pause and resume their form entries
author: mark
time: 2
category_id: sharing
subcategory: sharing_uncompleted
areas: [studio, wordpress]
---
<p>Respondents to your Tripetto forms can pause and resume their form entries, so they can continue later on. Especially handy with large forms.</p>

<blockquote>
  <h4>📣 Info: No data from uncompleted entries</h4>
  <p>This feature does NOT store form data from uncompleted entries to Tripetto. Tripetto forms only store form data when the respondent actively submits the form. As the owner of the form you will NOT be able to see form data from uncompleted entries.</p>
</blockquote>
<h2 id="how-it-works">How it works</h2>
<p>This is the process to pause and resume a form entry:</p>
<ol>
  <li>
    <p>If the pause and resume functionality is enabled, a <code>Pause</code> button will be shown inside the form. If the respondent clicks that button, an email address can be entered. This email address will be used to send the resume link to.</p>
    <p>The pause form will even try to fill out the email address automatically, based on an already answered question in your form with question type 'Email address'.</p>
  </li>
  <li>
    <p>After submitting the pause form, the resume link will be sent to the entered email address.</p>
  </li>
  <li>
    <p>By clicking the resume link in the email, the respondent gets back to the position where the form was paused, including all given answers until that point.</p>
  </li>
</ol>
<hr/>

<h2 id="availability">Availability</h2>
<p>Depending on how you use your form, the availability of the pause and resume function can differ:</p>
<ul>
  <li>
    <h3 id="shareable-links">In a shareable link</h3>
    <p>If your respondents fill out your form from the shareable link, the pause and resume functionality works automatically. In that case this function is always enabled and can not be disabled.</p>
  </li>
  <li>
    <h3 id="embed">In an embedded form</h3>
    <p>If your respondents use an embedded version of your form, they can also use the pause and resume functionality. In that case you have to manually activate this function inside your embed code.</p>
  </li>
</ul>
