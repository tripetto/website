---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-share-a-link-to-your-form-in-your-wordpress-site/
title: Share a link to your form in WordPress site - Tripetto Help Center
description: Learn how you can share your forms in your WordPress site with a simple, shareable link.
article_title: How to share a link to your form in your WordPress site
article_id: shareable-wordpress
article_video: sharing-wordpress-link
author: jurgen
time: 2
time_video: 1
category_id: sharing
subcategory: sharing_link
areas: [wordpress]
---
<p>When your form structure is ready, you can use the shareable link to share your form/survey on social media, via email, or whatever way you want to reach your potential respondents.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the direct link to your Tripetto form if you simply want to collect responses to your form. The direct link is served via your own WordPress website domain. Your collected data is stored in your own WordPress instance.</p>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>At the top menu bar of the form builder click <code><i class="fab fa-chromecast"></i> Share</code>. The Share pane will show up on the right side of the form builder.</p>
<p>Over there you see the first method <code>Shareable link</code> and you will see the URL of the shareable link. You can open the link immediately for yourself, or copy the URL to your clipboard to paste it somewhere else.</p>
<figure>
  <img src="{{ page.base }}images/help/wordpress-share/share.png" width="1200" height="760" alt="Screenshot of sharing in Tripetto" loading="lazy" />
  <figcaption>Open or copy the shareable link.</figcaption>
</figure>

<h3 id="url">URL</h3>
<p>The shareable link is always served from your own website's domain with a query string parameter (<code>?tripetto=FORMID</code>). It's not possible to customize this direct link.</p>
<p>If you don't want to show the <code>tripetto</code> query string parameter to your respondents, you can use a URL shortener service (for example Bitly), or <a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site/">embed the form in your website</a> to serve it from a URL that you control.</p>

<h3 id="optimize" data-anchor="Optimize sharing preview">Optimize your sharing preview</h3>
<p>You can optimize the direct link to your form for better sharing it across the web by adding a title, description and keywords to it. That information will be used inside previews when you share the link to your form.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-optimize-your-form-for-better-sharing/">How to optimize your form for better sharing</a></li>
</ul>

<h3 id="pause-resume">Pause and resume</h3>
<p>Tripetto offers a <a href="{{ page.base }}help/articles/let-your-respondents-pause-and-resume-their-form-entries/">pause and resume functionality</a>, so your respondents can continue filling out your form later on.</p>
<p>In the shareable link this function is always enabled and can not be disabled.</p>
<hr/>

<h2 id="data">Data storage</h2>
<p>Both the form and collected data are always hosted and stored in your own WordPress instance. Not a single connection related to Tripetto is ever made with an external host other than yours. It's all in your own instance, under your control.</p>
