---
layout: help-article
base: ../../../
permalink: /help/articles/troubleshooting-file-uploads-from-form-not-saved/
title: File uploads from form not saved - Tripetto Help Center
description: If you use a file upload block in your form, it can happen the files aren't saved properly. This article contains a checklist to troubleshoot such issues.
article_title: File uploads from form not saved
author: jurgen
time: 2
category_id: sharing
subcategory: sharing_troubleshooting
areas: [wordpress]
---
<p>If you use a file upload block in your form, it can happen the files aren't saved properly. This article contains a checklist to troubleshoot such issues.</p>

<h2 id="checklist">Checklist</h2>
<p>Please follow this checklist to investigate why your file uploads are not stored correctly:</p>

<h3 id="plugin">1. Check Tripetto plugin version</h3>
<p>First of all, check which version of the Tripetto plugin you have installed via the Plugins section in your WP Admin. Make sure you have installed the latest version to stay up-to-date with features and bugfixes.</p>
<p>To update the Tripetto plugin to the latest version, please see our <a href="{{ page.base }}help/articles/how-to-update-the-wordpress-plugin-to-the-latest-release/">article about updating the plugin</a>.</p>

<h3 id="file-upload-block">2. Check file upload block</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/editor-block-file-upload/00-file-upload.png" width="1200" height="760" alt="Screenshot of file upload block in Tripetto" loading="lazy" />
  <figcaption>Advanced options of the file upload block.</figcaption>
</figure>
<p>Next, check the settings of your file upload block. It offers multiple settings to let you control what people can upload. Maybe the maximum file size you entered is not sufficient, or a certain file type is not on your list of allowed file types. For more information on these options, please have a look at this article:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-file-upload-block/">How to use the file upload block</a></li>
</ul>

<h3 id="settings">3. Check WordPress upload settings</h3>
<p>Your WordPress installation also has a few settings for uploading. Some of these can influence what's allowed to upload to your WordPress instance, for example the maximum upload size.</p>
<p>How to access and edit those settings differs per WordPress installation, so you'd have to see how you can do that for your case.</p>

<h3 id="conflicts">4. Check conflicting plugins</h3>
<p>Lastly, check if any other WordPress plugins might cause conflicts. WordPress offers lots of plugins, so we can't prevent all conflicts with other plugins, as everybody uses a different set of plugins. Based on some user feedback we are aware of conflicts with the following (type of) plugins:</p>
<ul>
  <li><strong>Caching plugins</strong><br/>Caching plugins can cause issues when a cached version of your page and/or form is shown. Examples of such plugins are W3 Total Cache and WP Rocket.<br/>To solve conflicts with such plugins, you often can exclude the Tripetto scripts and the page that the form is embedded at from those plugins, preventing it to show cached versions of your page/form. How to do this, depends on each particular plugin.</li>
  <li><strong>Optimize, minify and bundle plugins</strong><br/>These types of plugins might mess up some scripts of the Tripetto plugin. Examples of such plugins are Autoptimizer and WP-Optimize.<br/>To solve conflicts with such plugins, you often can exclude the Tripetto scripts from those plugins, preventing it to influence the Tripetto scripts. How to do this, depends on each particular plugin.</li>
  <li><strong>Firewall plugins</strong><br/>These types of plugins might block certain data to be uploaded.<br/>Try to see if it makes a difference if you disable that plugin. Possibly you can make an exception for Tripetto uploads, or you can edit the settings of the plugin to allow uploads from Tripetto.</li>
  <li><strong>Wordfence</strong><br/>This plugin might block the Tripetto plugin to submit data.<br/>To solve conflicts with Wordfence, enable the <code>Learn mode</code> inside the Wordfence plugin and then complete a Tripetto form in your WP site. Wordfence then learns that Tripetto wants to send data. After that you can disable the <code>Learn mode</code> again.</li>
</ul>
<hr />

<h2 id="support">Still having issues?</h2>
<p>Despite the above checklist, you're still experiencing issues? Don't hesitate to <a href="{{ page.base }}support/">submit a support ticket</a>. We're happy to help you!</p>
