---
layout: help-article
base: ../../../
permalink: /help/articles/troubleshooting-results-not-saved-to-wordpress-admin/
title: Results not saved to WordPress Admin - Tripetto Help Center
description: This article contains a checklist to troubleshoot data storage issues.
article_title: Results not saved to WordPress Admin
author: jurgen
time: 4
category_id: sharing
subcategory: sharing_troubleshooting
areas: [wordpress]
---
<p>In some situations your form results might not be saved to your WordPress Admin, resulting in missing data. This article contains a checklist to troubleshoot such issues.</p>

<h2 id="checklist">Checklist</h2>
<p>Please follow this checklist to investigate why your form submissions are not stored correctly:</p>

<h3 id="plugin">1. Check Tripetto plugin version</h3>
<p>First of all, check which version of the Tripetto plugin you have installed via the Plugins section in your WP Admin. Make sure you have installed the latest version to stay up-to-date with features and bugfixes.</p>
<p>To update the Tripetto plugin to the latest version, please see our <a href="{{ page.base }}help/articles/how-to-update-the-wordpress-plugin-to-the-latest-release/">article about updating the plugin</a>.</p>

<h3 id="errors">2. Check form errors</h3>
<p>Next, check if your form shows an error message when you complete the form yourself. If it shows an error message, please check the corresponding error message from the following list:</p>
<ul>
  <li><strong>Something went wrong while submitting your conversation.</strong><br/>If you see this error message, there's probably something wrong with the connection or the permissions to access the <code>admin-ajax.php</code> file in your WordPress instance. The error console of your browser might contain more technical information about what went wrong.</li>
  <li><strong>Your data is rejected.</strong><br/>If you see this error message, the submitted data is marked as invalid and therefore not stored.</li>
  <li><strong>The form is outdated.</strong><br/>If you see this error message, the posted data does not correspond with the latest version of your form structure. That can happen because the form structure is changed while filling out the form, or because a cached version of the form is shown. If you haven't changed the form structure, check the following:
    <ul>
      <li>Check if you have enabled the embed option <code>Disable asynchronous loading</code>. If that's the case, caching plugins have more influence to show a cached version of your form. Please disable that embed option and try again.</li>
      <li>Check if there are any caching plugins installed to your site that can interfere with the Tripetto form or page that the form is embedded at. Make sure you remove any cached versions of the form/page from your caching plugin and possibly configure the caching plugin to exclude the form/page from caching.</li>
    </ul>
   </li>
</ul>
<p>If you can't resolve a form error using the above instructions, please <a href="{{ page.base }}support/">submit a support ticket</a> so we can assist you on this.</p>

<h3 id="database">3. Check databases</h3>
<p>Next, check if the required databases are present and configured correctly inside your WordPress instance (it depends on your WordPress configuration how to access your database admin):</p>
<ul>
  <li><strong>Database tables</strong><br/>Check if the following tables exist: <code>tripetto_forms</code>, <code>tripetto_entries</code> and <code>tripetto_attachments</code>.</li>
  <li><strong>Database column</strong><br/>Check if the table column <code>entry</code> in the table <code>tripetto_entries</code> is set to type <code>LONGTEXT</code>.</li>
</ul>
<p>If any of the above does not match your database structure, please <a href="{{ page.base }}support/">submit a support ticket</a> so we can help you on this.</p>

<h3 id="conflicts">4. Check conflicting plugins</h3>
<p>Lastly, check if any other WordPress plugins might cause conflicts. WordPress offers lots of plugins, so we can't prevent all conflicts with other plugins, as everybody uses a different set of plugins. Based on some user feedback we are aware of conflicts with the following (type of) plugins:</p>
<ul>
  <li><strong>Caching plugins</strong><br/>Caching plugins can cause issues when a cached version of your page and/or form is shown. Examples of such plugins are W3 Total Cache and WP Rocket.<br/>To solve conflicts with such plugins, you often can exclude the Tripetto scripts and the page that the form is embedded at from those plugins, preventing it to show cached versions of your page/form. How to do this, depends on each particular plugin.</li>
  <li><strong>Optimize, minify and bundle plugins</strong><br/>These types of plugins might mess up some scripts of the Tripetto plugin. Examples of such plugins are Autoptimizer and WP-Optimize.<br/>To solve conflicts with such plugins, you often can exclude the Tripetto scripts from those plugins, preventing it to influence the Tripetto scripts. How to do this, depends on each particular plugin.</li>
  <li><strong>Wordfence</strong><br/>This plugin might block the Tripetto plugin to submit data.<br/>To solve conflicts with Wordfence, enable the <code>Learn mode</code> inside the Wordfence plugin and then complete a Tripetto form in your WP site. Wordfence then learns that Tripetto wants to send data. After that you can disable the <code>Learn mode</code> again.</li>
</ul>
<hr />

<h2 id="support">Still having issues?</h2>
<p>Despite the above checklist, you're still experiencing issues? Don't hesitate to <a href="{{ page.base }}support/">submit a support ticket</a>. We're happy to help you!</p>
