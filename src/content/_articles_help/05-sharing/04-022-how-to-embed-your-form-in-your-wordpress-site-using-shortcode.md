---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-shortcode/
title: Embed form in WordPress with shortcode - Tripetto Help Center
description: If you use a traditional editor in WordPress, you can embed your Tripetto forms inside your WordPress site using Tripetto's shortcode.
article_title: How to embed your form in your WordPress site using the shortcode
article_id: embed-wordpress
article_folder: wordpress-embed
article_video: sharing-wordpress-shortcode
author: mark
time: 4
time_video: 2
category_id: sharing
subcategory: sharing_embed
areas: [wordpress]
scripts: [prism]
stylesheets: [prism-dark]
---
<p>If you use a traditional editor in WordPress, you can embed your Tripetto forms inside your WordPress site using Tripetto's shortcode.</p>

<h2 id="when-to-use">When to use</h2>
<p>By embedding your form in your site you can place your form inside your site's content. If you don't use a modern page builder, you can always use the shortcode to achieve this. All data will be stored inside your own WordPress instance.</p>
<blockquote>
  <h4>📌 Also see: Tripetto's other embed options</h4>
  <p>We offer several ways to embed your forms in your WordPress site. It largely depends on the WordPress builder you use which method is best and easiest for you:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-shortcode/">Embed with WordPress shortcode</a>, if you use a traditional editor<span class="related-current">Current article</span></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-gutenberg-block/">Embed with Gutenberg block</a>, if you use the Gutenberg editor</li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-elementor-widget/">Embed with Elementor widget</a>, if you use the Elementor builder</li>
  </ul>
</blockquote>

<h3 id="shortcodes">About shortcodes</h3>
<p>Shortcodes are a commonly used technique in WordPress to add snippets to your WordPress page. <a href="https://wordpress.org/support/article/shortcode-block/" target="_blank" rel="noopener noreferrer">More information about shortcodes can be found at the website of WordPress</a>.</p>
<p>The Tripetto WordPress plugin offers a shortcode for each of your forms to easily embed Tripetto forms in your content.</p>
<div>
  <a href="https://wordpress.org/support/article/shortcode-block/" target="_blank" class="blocklink" rel="noopener noreferrer">
    <div>
      <span class="title">WordPress Shortcode Block<i class="fas fa-external-link-alt"></i></span>
      <span class="description">A shortcode is a WordPress-specific code that lets you do nifty things with very little effort.</span>
      <span class="url">wordpress.org</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-wordpress.svg" width="160" height="160" alt="WordPress logo" loading="lazy" />
    </div>
  </a>
</div>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>At the top menu bar of the form builder plugin, click <code><i class="fab fa-chromecast"></i> Share</code>. The Share pane will show up on the right side of the form builder.</p>
<p>Over there you see the second method <code>WordPress shortcode</code> and you will see the basic syntax of the shortcode for WordPress sites.</p>
<figure>
  <img src="{{ page.base }}images/help/wordpress-share/share.png" width="1200" height="760" alt="Screenshot of embedding in Tripetto" loading="lazy" />
  <figcaption>Open the Share screen.</figcaption>
</figure>

<h3 id="basic-shortcode">Basic shortcode</h3>
<p>The basic syntax is: <code>[tripetto id="#"]</code>, with the <code>id</code> parameter representing the id (integer) of the Tripetto form.</p>

<h3 id="options">Options</h3>
<p>Inside the Share screen you can customize the shortcode to configure the implementation of the form into your WordPress site. You can just toggle the desired options and the shortcode will update automatically.</p>
<p>The following options are available:</p>
<ul>
  <li><h4>Allow pausing and resuming</h4>Setting to determine if the form can be <a href="{{ page.base }}help/articles/let-your-respondents-pause-and-resume-their-form-entries/">paused and resumed</a> by your respondents.</li>
  <li><h4>Save and restore uncompleted forms</h4>Setting to determine if <a href="{{ page.base }}help/articles/let-your-form-save-and-restore-uncompleted-entries/">data persists in the browser</a> if a user enters the form later on again.</li>
  <li><h4>Display form as full page overlay</h4>Setting to show the form as a full page overlay.</li>
  <li><h4>Width</h4>Setting to set the width of form container.</li>
  <li><h4>Height</h4>Setting to set the height of form container.</li>
  <li><h4>Placeholder</h4>Option to show a message that is shown while the form is loading. You can specify text or HTML.</li>
  <li><h4>Disable asynchronous loading</h4>Option to disable asynchronous loading of your form to enhance the form loading time (please be aware that caching plugins may have effect on your Tripetto forms if you use this option).</li>
  <li><h4>Custom CSS</h4>Option to add custom CSS to parts of your form (<a href="{{ page.base }}help/articles/how-to-use-custom-css-in-embedded-forms/">see instructions</a>).</li>
</ul>
<p>When you're done customizing the shortcode, you can copy the shortcode to your clipboard to use it in your WordPress site.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/shortcode.png" width="1200" height="760" alt="Screenshot of the shortcode in Tripetto" loading="lazy" />
  <figcaption>Customize the shortcode with the right settings.</figcaption>
</figure>

<h3 id="embed-form">Embed form</h3>
<p>Now that you've got your shortcode, navigate to the position inside your WP Admin where you want to embed the Tripetto form. In that position add a shortcode block: Click the <code><i class="fas fa-plus"></i></code> icon<i class="fas fa-arrow-right"></i>Click <code>Shortcode</code>. A shortcode block will be inserted. In that block you can now paste the shortcode of the Tripetto form. You can now save the draft and test it.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/editor.png" width="1200" height="760" alt="Screenshot of the shortcode in WordPress" loading="lazy" />
  <figcaption>Insert the shortcode in a page.</figcaption>
</figure>

<h3 id="responsiveness">Responsiveness</h3>
<p>Tripetto forms are of course designed to work properly on all screen sizes. This is called 'responsiveness'.</p>
<p>If you embed a Tripetto form in your own page, the responsiveness of the form will depend on the responsiveness settings of your own page. In most cases that is controlled by the viewport settings in the <code>head</code> section of your website. We advise to configure that like this:</p>
<pre class="line-numbers"><code class="language-html">&lt;meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"></code></pre>
<hr/>

<h2 id="data">Data storage</h2>
<p>Both the form and collected data are always hosted and stored in your own WordPress instance. Not a single connection related to Tripetto is ever made with an external host other than yours. It's all in your own instance, under your control.</p>
