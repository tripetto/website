---
layout: help-article
base: ../../../
permalink: /help/articles/troubleshooting-form-not-showing-in-wordpress-site/
title: Form not showing in WordPress site - Tripetto Help Center
description: In some situations your form might not show the right way in your WordPress site. This article contains a checklist to troubleshoot such issues.
article_title: Form not showing in WordPress site
author: jurgen
time: 3
category_id: sharing
subcategory: sharing_troubleshooting
areas: [wordpress]
---
<p>In some situations your form might not show the right way in your WordPress site. This article contains a checklist to troubleshoot such issues.</p>

<h2 id="checklist">Checklist</h2>
<p>Please follow this checklist to investigate why your embedded form is not showing correctly in your WordPress site:</p>

<h3 id="plugin">1. Check Tripetto plugin version</h3>
<p>First of all, check which version of the Tripetto plugin you have installed via the Plugins section in your WP Admin. Make sure you have installed the latest version to stay up-to-date with features and bugfixes.</p>
<p>To update the Tripetto plugin to the latest version, please see our <a href="{{ page.base }}help/articles/how-to-update-the-wordpress-plugin-to-the-latest-release/">article about updating the plugin</a>.</p>

<h3 id="shortcode">2. Check embed options</h3>
<p>Next, check the embed options of your form. When you embed the form in your WordPress site there are some options you can use to influence the display of the form. You can try to see if these options make any difference to display your form correctly, for example by prodiving a <code>width</code> and/or <code>height</code> to your embedded form.</p>
<p>We offer several ways to embed your form in your WordPress site, so please take a look at the detailed articles per embed option how to control the options:</p>
<ul>
  <li>Using <a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-shortcode/">WordPress shortcode</a> to embed in the traditional editor;</li>
  <li>Using <a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-gutenberg-block/">Gutenberg block</a> to embed in the modern Gutenberg editor;</li>
  <li>Using <a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-elementor-widget/">Elementor widget</a> to embed in the Elementor builder.</li>
</ul>

<h3 id="conflicts">3. Check conflicting plugins</h3>
<p>Lastly, check if any other WordPress plugins might cause conflicts. WordPress offers lots of plugins, so we can't prevent all conflicts with other plugins, as everybody uses a different set of plugins. Based on some user feedback we are aware of conflicts with the following (type of) plugins.</p>
<ul>
  <li><strong>Caching plugins</strong><br/>Caching plugins can cause issues when a cached version of your page and/or form is shown. Examples of such plugins are W3 Total Cache and WP Rocket.<br/>To solve conflicts with such plugins, you often can exclude the Tripetto scripts and the page that the form is embedded at from those plugins, preventing it to show cached versions of your page/form. How to do this, depends on each particular plugin.</li>
  <li><strong>Optimize, minify and bundle plugins</strong><br/>These types of plugins might mess up some scripts of the Tripetto plugin. Examples of such plugins are Autoptimizer and WP-Optimize.<br/>To solve conflicts with such plugins, you often can exclude the Tripetto scripts from those plugins, preventing it to influence the Tripetto scripts. How to do this, depends on each particular plugin.</li>
</ul>
<hr />

<h2 id="support">Still having issues?</h2>
<p>Despite the above checklist, you're still experiencing issues? Don't hesitate to <a href="{{ page.base }}support/">submit a support ticket</a>. We're happy to help you!</p>
