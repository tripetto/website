---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-embed-a-form-from-the-studio-into-your-website/
title: Embed a form from the studio into your website - Tripetto Help Center
description: Learn how to embed a form from the studio into your own website.
article_title: How to embed a form from the studio into your website
article_id: embed-studio
article_folder: studio-embed
article_video: sharing-studio-embed
author: mark
time: 4
time_video: 1
category_id: sharing
subcategory: sharing_embed
areas: [studio]
scripts: [prism]
stylesheets: [prism-dark]
---
<p>You can share your forms with a <a href="{{ page.base }}help/articles/how-to-share-a-link-to-your-form-from-the-studio/">simple link</a>, but you can also embed the form in your own website.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the embed code of your Tripetto form if you want to show your form inside your own website's content, or if you want to share your form via your own domain. Your collected data is stored under your account at Tripetto in Western Europe.</p>
<blockquote>
  <h4>🧯 Troubleshooting: Update your embeds after May 2023 studio update</h4>
  <p>With the studio update of May 2023 the studio embed codes are revised. Find out if you have to update your embedded forms:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/troubleshooting-update-your-embeds-after-may-2023-studio-update/">Update your embeds after May 2023 studio update</a></li>
  </ul>
</blockquote>

<h3 id="embed-codes">About embed codes</h3>
<p>Tripetto offers an embed code for each form you create in the studio. With that embed code you can implement your Tripetto form in your own website. It comes with several options and embed types to perfectly match your implementation purpose.</p>
<blockquote>
  <h4>📌 Also see: Looking for data control?</h4>
  <p>With the studio embed all collected data is stored under your account at Tripetto. It's also possible to take control over your data and store it on your own. In that case choose for integrating your Tripetto form with the <a href="{{ page.base }}sdk/" target="_blank">Tripetto FormBuilder SDK</a>:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-integrate-a-form-form-the-studio-into-your-website-or-application/">How to integrate a form from the studio into your website or application</a></li>
  </ul>
</blockquote>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>At the top menu bar of the form builder click <code><i class="fab fa-chromecast"></i> Share</code>. The Share pane will show up on the right side of the form builder.</p>
<p>Over there choose for method <code>Embed in a website</code> and you will see the embed code and some settings.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-embed.png" width="1200" height="760" alt="Screenshot of embedding in Tripetto" loading="lazy" />
  <figcaption>Choose for embedding your form.</figcaption>
</figure>

<h3 id="copy-embed">Embed code</h3>
<p>By default you see the most-used configuration for embed codes. If you simply want to embed your form inline with the other content on your website, you can use this default embed code.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/05-embed-code.png" width="609" height="484" alt="Screenshot of embed code in Tripetto studio" loading="lazy" />
  <figcaption>Your embed code.</figcaption>
</figure>

<h3 id="types">Embed types</h3>
<p>We provide multiple embed types that affect how the embed code is formatted. It depends on how you want to embed your form in your website which is the best type for you.</p>
<p>You can select one of the following embed types:</p>
<ul>
  <li><code>Inline with other content (HTML snippet)</code> - The form gets placed inline with your content on the position where you place the Tripetto element (div element with unique id);</li>
  <li><code>Full page (HTML snippet)</code> - The form covers the whole page. You can place the snippet at the desired position in your code;</li>
  <li><code>Full page (HTML page)</code> - A full working HTML page with the form covering the whole page;</li>
  <li><code>Using JavaScript or TypeScript</code> - The form gets embedded using ES6 imports. Make sure you also install the required npm packages (commands also included in the studio).</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-embed-type.png" width="612" height="244" alt="Screenshot of embed types in Tripetto" loading="lazy" />
  <figcaption>Select the embed type.</figcaption>
</figure>

<h3 id="options">Options</h3>
<p>Inside the Share screen you can customize the embed code to configure some options in the embedded form. You can just toggle the desired options and the embed code will update automatically.</p>
<p>The following options are available:</p>
<ul>
  <li>
    <p><code>Allow pausing and resuming</code> - Option to determine if the form can be <a href="{{ page.base }}help/articles/let-your-respondents-pause-and-resume-their-form-entries/">paused and resumed</a> by your respondents.</p>
  </li>
  <li>
    <p><code>Save and restore uncompleted forms</code> - Option to determine if <a href="{{ page.base }}help/articles/let-your-form-save-and-restore-uncompleted-entries/">data persists in the browser</a> if a respondent visits the form again at a later moment.</p>
  </li>
  <li>
    <p><code>Custom CSS</code> - It's also possible to add custom CSS to parts of your embedded Tripetto form. To do this, manually add the <code>customCSS</code> property to your embed code and add the desired custom CSS code to that: <code>customCSS: "YOUR CSS"</code>.</p>
    <ul class="fa-ul related">
      <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-custom-css-in-embedded-forms/">How to use custom CSS in embedded forms</a></li>
    </ul>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/03-embed-options.png" width="603" height="233" alt="Screenshot of embed options in Tripetto" loading="lazy" />
  <figcaption>Toggle the embed options.</figcaption>
</figure>

<h3 id="cdn">CDN</h3>
<p>If you embed the form via HTML (see <a href="#types" class="anchor">Embed types</a>), the embed code uses some Tripetto scripts that are loaded from a <a href="https://en.wikipedia.org/wiki/Content_delivery_network" target="_blank" rel="noopener noreferrer">content delivery network (CDN)</a>. Tripetto lets you choose which CDN you want to use for your embed. This will not affect the working of your form, but there can be performance and reliability differences between CDN's.</p>
<p>You can select one of the following CDN's:</p>
<ul>
  <li><code>jsDelivr</code> (<a href="https://www.jsdelivr.com/" target="_blank" rel="noopener noreferrer">website</a>);</li>
  <li><code>unpkg</code> (<a href="https://unpkg.com/" target="_blank" rel="noopener noreferrer">website</a>);</li>
  <li><code>Custom domain/CDN</code> - Make sure you host the Tripetto scripts on the desired domain and update the <code>script src</code>'s in the embed code to point to the right domain and location of the scripts.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/04-cdn.png" width="598" height="229" alt="Screenshot of CDN options in Tripetto" loading="lazy" />
  <figcaption>Select the content delivery network (CDN).</figcaption>
</figure>

<h3 id="embed-form">Embed form</h3>
<p>Now that you've got your embed code, you can simply paste that at the right position in the HTML structure of your website.</p>
<p>Please notice that you can attach your form to any element you'd like inside your page. By default the inline embed code includes a div element with a unique Tripetto id. The <code>element</code> parameter in the embed code attaches the form to that div element, using the unique id. You can control where that div element is shown in your HTML structure and you can style that div via your own stylesheet.</p>
<p>If you choose to embed your form as a full page, the form gets attached to the body element of the page where you place the embed code at. The form then will be shown as a full page form on top of any other content that's on that page.</p>

<h3 id="responsiveness">Responsiveness</h3>
<p>Tripetto forms are of course designed to work properly on all screen sizes. This is called 'responsiveness'.</p>
<p>If you embed a Tripetto form in your own page, the responsiveness of the form will depend on the responsiveness settings of your own page. In most cases that is controlled by the viewport settings in the <code>head</code> section of your website. We advise to configure that like this:</p>
<pre class="line-numbers"><code class="language-html">&lt;meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"></code></pre>
<hr />

<h2 id="data-storage">Data storage</h2>
<p>By using the studio embed, both the form and collected data are always stored under your account at Tripetto in Western Europe.</p>

<h3 id="self-hosted">Self-hosted</h3>
<p>If you want to take full control over your data and store it on your own, the studio embed is not for you. Instead you can integrate the form in your website/application with the Tripetto FormBuilder SDK, which gives you control over your data storage.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-integrate-a-form-form-the-studio-into-your-website-or-application/">How to integrate a form from the studio into your website or application</a></li>
</ul>
