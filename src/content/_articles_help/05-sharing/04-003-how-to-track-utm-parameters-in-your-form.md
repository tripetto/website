---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-track-utm-parameters-in-your-form/
title: Track UTM parameters - Tripetto Help Center
description: You can add UTM campaign parameters to your form URL and collect those UTM values in your form responses.
article_title: How to track UTM parameters in your form
article_folder: share-utm
author: jurgen
time: 4
category_id: sharing
subcategory: sharing_methods
areas: [studio, wordpress]
---
<p>You can add UTM campaign parameters to your form URL and collect those UTM values in your form responses.</p>

<h2 id="when-to-use">When to use</h2>
<p><a href="https://en.wikipedia.org/wiki/UTM_parameters" target="_blank" rel="noopener noreferrer">UTM parameters</a> can be added to your form URL to measure the source of your respondents. That way you can track where your respondents came from when they opened your form, for example to see which marketing efforts have the most effect.</p>
<p>Tracking services, like Google Analytics, often support UTM parameters, so you can easily analyze the UTM data.</p>

<h3 id="types">Available UTM parameters</h3>
<p>There are 6 UTM parameters available:</p>
<ul>
  <li><code>utm_source</code> - Identifies which site sent the traffic (required);</li>
  <li><code>utm_medium</code> - Identifies what type of link was used;</li>
  <li><code>utm_campaign</code> - Identifies a specific product promotion or strategic campaign;</li>
  <li><code>utm_term</code> - Identifies search terms;</li>
  <li><code>utm_content</code> - Identifies what specifically was clicked to bring the user to the site;</li>
  <li><code>utm_id</code> - Identifies which ads campaign this referral references.</li>
</ul>
<p>You can combine those parameters in your form URL to measure what you want.</p>
<blockquote>
  <h4>📌 Also see: Hidden field block</h4>
  <p>In the article you're reading now we describe how to collect UTM parameters in your form. We use the hidden field block for that. For global instructions about the hidden field block, please have a look at this article:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-hidden-field-block/">How to use the hidden field block</a></li>
  </ul>
</blockquote>

<h2 id="how-to-use">How to use</h2>
<p>Now let's see how you can collect UTM parameters through your Tripetto form.</p>

<h3 id="generate">Generate UTM URL's</h3>
<p>First step is to add the UTM parameters to your form URL. You can use a UTM campaign builder to easily create such URL's.</p>
<div>
  <a href="https://ga-dev-tools.google/ga4/campaign-url-builder/" target="_blank" rel="noopener noreferrer" class="blocklink">
    <div>
      <span class="title">Campaign URL Builder</span>
      <span class="description">This tool allows you to easily add campaign parameters to URLs so you can measure Custom Campaigns in Google Analytics.</span>
      <span class="url">ga-dev-tools.google</span>
    </div>
  </a>
</div>
<p>As base URL use the URL where your form is served from. If you use the direct link to your form, that's your base URL. If you have embedded your form, the URL of that page on your website is your base URL.</p>
<p>After that, add the desired UTM parameters with your values. Generate a unique UTM URL for each variant of your campaign(s). You can now use these URL('s) to send to your respondents.</p>

<h3 id="collect">Collect UTM parameters</h3>
<p>In your form you can now collect the different UTM parameter values. Those will be stored with each form response you receive, so you can see where each form response came from.</p>
<p>To do so, you use the <a href="{{ page.base }}help/articles/how-to-use-the-hidden-field-block/">hidden field block</a> in your form structure. You can store the whole query string at once, but for better analysis it's advised to store each UTM parameter in a separate hidden field block. You do that as follow:</p>
<ol>
  <li>Add a <a href="{{ page.base }}help/articles/how-to-use-the-hidden-field-block/">hidden field block</a> to your form structure. Give it a name to use in your form data (this will not be visible for your respondents);</li>
  <li>Set the <code>Type of field</code> to <code>Query string</code>;</li>
  <li>Enable the <code>Parameter</code> feature and enter the exact query string parameter you want to store in this hidden field. For UTM parameters these are the available parameter names (which we explained earlier in this article):
    <ul>
      <li><code>utm_source</code></li>
      <li><code>utm_medium</code></li>
      <li><code>utm_campaign</code></li>
      <li><code>utm_term</code></li>
      <li><code>utm_content</code></li>
      <li><code>utm_id</code></li>
    </ul>
  </li>
</ol>
<p>Repeat this for every UTM parameter you use in your UTM URL's that you share with your audience. In case you use all 6 UTM parameters, you'll have 6 hidden field blocks: one per parameter.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/hidden-field-utm.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Example of a hidden field retrieving the UTM parameter <code>utm_source</code>.</figcaption>
</figure>

<h3 id="track">Track UTM parameters</h3>
<p>When sharing your form URL, make sure you always include your UTM parameters in your URL, according to each campaign.</p>
<p>All your form responses will now store the values of your UTM parameters. These are simply part of the form data that each respondent submits. You can now analyze that data, for example to learn which campaign is most effective to attract respondents.</p>
