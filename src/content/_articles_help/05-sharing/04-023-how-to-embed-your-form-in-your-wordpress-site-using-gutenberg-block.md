---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-gutenberg-block/
title: Embed form in WordPress with Gutenberg block - Tripetto Help Center
description: If you use the Gutenberg editor in WordPress, you can embed your Tripetto forms inside your WordPress site using Tripetto's Gutenberg block.
article_title: How to embed your form in your WordPress site using the Gutenberg block
article_id: embed-wordpress
article_video: sharing-wordpress-gutenberg
article_folder: wordpress-gutenberg
author: mark
time: 3
time_video: 2
category_id: sharing
subcategory: sharing_embed
areas: [wordpress]
scripts: [prism]
stylesheets: [prism-dark]
---
<p>If you use the Gutenberg editor in WordPress, you can embed your Tripetto forms inside your WordPress site using Tripetto's Gutenberg block.</p>

<h2 id="when-to-use">When to use</h2>
<p>By embedding your form in your site you can place your form inside your site's content. If you use the modern WordPress page builder (also known as Gutenberg editor), Tripetto's Gutenberg block is the easiest way to achieve this. All data will be stored inside your own WordPress instance.</p>
<blockquote>
  <h4>📌 Also see: Tripetto's other embed options</h4>
  <p>We offer several ways to embed your forms in your WordPress site. It largely depends on the WordPress builder you use which method is best and easiest for you:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-shortcode/">Embed with WordPress shortcode</a>, if you use a traditional editor</li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-gutenberg-block/">Embed with Gutenberg block</a>, if you use the Gutenberg editor<span class="related-current">Current article</span></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-elementor-widget/">Embed with Elementor widget</a>, if you use the Elementor builder</li>
  </ul>
</blockquote>

<h3 id="gutenberg">About Gutenberg editor</h3>
<p>Gutenberg is the modern content editor in WordPress. It makes building those amazing pages and posts in your WordPress site even easier, more flexible and more powerful. <a href="https://wordpress.org/support/article/wordpress-editor/" target="_blank" rel="noopener noreferrer">You can read all about it at wordpress.org.</a></p>
<p>After you have installed the Tripetto WordPress plugin, Tripetto is available as a Gutenberg block inside the Gutenberg editor to easily embed Tripetto forms in your content.</p>
<div>
  <a href="https://wordpress.org/support/article/wordpress-editor/" target="_blank" class="blocklink" rel="noopener noreferrer">
    <div>
      <span class="title">WordPress Gutenberg Editor<i class="fas fa-external-link-alt"></i></span>
      <span class="description">The WordPress Editor is a new publishing experience. You can use it to create media-rich pages and posts and to control their layout with ease.</span>
      <span class="url">wordpress.org</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-wordpress.svg" width="160" height="160" alt="WordPress logo" loading="lazy" />
    </div>
  </a>
</div>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>In your Gutenberg editor you start with adding a new block. From the overview of available blocks select <code>Tripetto Form</code>. You can also use the search bar to quickly find the Tripetto block.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/gutenberg-editor.png" width="1200" height="760" alt="Screenshot of Tripetto in the Gutenberg editor" loading="lazy" />
  <figcaption>Add Tripetto to the Gutenberg editor.</figcaption>
</figure>
<p>This will add a new Tripetto block to your Gutenberg editor. Now you can select what you want to insert in that position:</p>
<ul>
  <li><code>Select an existing form</code>;</li>
  <li><code>Build a new form</code>.</li>
</ul>

<h3 id="insert">Insert an existing form</h3>
<p>Inserting an existing form is easy. Add a new <code>Tripetto Form</code> block to your page/post. Then simply select the form from the dropdown list. After you have selected the desired form, it will be added to your block and you will see a live preview of the form right away. That's it!</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/insert-existing.gif" width="1200" height="760" alt="Screen recording of the Tripetto Gutenberg block" loading="lazy" />
  <figcaption>Insert an existing form in the Tripetto Gutenberg block.</figcaption>
</figure>

<h3 id="build">Build a new form</h3>
<p>Tripetto's Gutenberg block even enables you to build your forms right inside the Gutenberg editor. Add a new <code>Tripetto Form</code> block to your page/post. Then simply click <code>Build new form</code>. The new form will be generated and a live preview is shown.</p>
<p>Since the form is empty, the preview will show a message about that. To start building your form, click the <code><i class="fas fa-pen"></i> Edit</code> button in the Gutenberg block. You can now open the form builder in a fullscreen view (giving you maximum space to build your forms with logic), or as a side-by-side overlay. And you can start building your form.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/build-new.gif" width="1200" height="760" alt="Screen recording of the Tripetto Gutenberg block" loading="lazy" />
  <figcaption>Add and build a new form in the Tripetto Gutenberg block.</figcaption>
</figure>

<h3 id="options">Options</h3>
<p>At the right side of the Gutenberg editor you can configure the implementation of the form into your WordPress site. You can just toggle the desired options and the Tripetto form will update automatically.</p>
<p>The following options are available:</p>
<ul>
  <li><h4>Allow pausing and resuming</h4>Setting to determine if the form can be <a href="{{ page.base }}help/articles/let-your-respondents-pause-and-resume-their-form-entries/">paused and resumed</a> by your respondents.</li>
  <li><h4>Save and restore uncompleted forms</h4>Setting to determine if <a href="{{ page.base }}help/articles/let-your-form-save-and-restore-uncompleted-entries/">data persists in the browser</a> if a user enters the form later on again.</li>
  <li><h4>Disable asynchronous loading</h4>Option to disable asynchronous loading of your form to enhance the form loading time (please be aware that caching plugins may have effect on your Tripetto forms if you use this option).</li>
  <li><h4>Width</h4>Setting to set the width of form container.</li>
  <li><h4>Height</h4>Setting to set the height of form container.</li>
  <li><h4>Placeholder</h4>Option to show a message that is shown while the form is loading. You can specify text or HTML.</li>
  <li><h4>Custom CSS</h4>Option to add custom CSS to parts of your form (<a href="{{ page.base }}help/articles/how-to-use-custom-css-in-embedded-forms/">see instructions</a>).</li>
  <li><h4>Additional CSS class(es)</h4>Option to add additional CSS class(es) to the Gutenberg block. This can not be used to customize CSS inside the Tripetto form.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/options.png" width="1200" height="760" alt="Screenshot of Tripetto in the Gutenberg editor" loading="lazy" />
  <figcaption>Customize the desired options for your form.</figcaption>
</figure>

<h3 id="responsiveness">Responsiveness</h3>
<p>Tripetto forms are of course designed to work properly on all screen sizes. This is called 'responsiveness'.</p>
<p>If you embed a Tripetto form in your own page, the responsiveness of the form will depend on the responsiveness settings of your own page. In most cases that is controlled by the viewport settings in the <code>head</code> section of your website. We advise to configure that like this:</p>
<pre class="line-numbers"><code class="language-html">&lt;meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"></code></pre>
<hr />

<h2 id="data">Data storage</h2>
<p>Both the form and collected data are always hosted and stored in your own WordPress instance. Not a single connection related to Tripetto is ever made with an external host other than yours. It's all in your own instance, under your control.</p>
