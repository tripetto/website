---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-share-a-link-to-your-form-from-the-studio/
title: Share a link to your form from the studio - Tripetto Help Center
description: Learn how you can share your forms with a simple, shareable link.
article_title: How to share a link to your form from the studio
article_id: shareable-studio
article_folder: studio-share
article_video: sharing-studio-link
author: jurgen
time: 2
time_video: 1
category_id: sharing
subcategory: sharing_link
areas: [studio]
---
<p>When your form structure is ready, you can use the shareable link to share your form/survey on social media, via email, or whatever way you want to reach your potential respondents.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the direct link to your Tripetto form if you simply want to collect responses to your form, or if you don't have a website. The direct link is served via the <code>tripetto.app</code> domain. Your collected data is stored under your account at Tripetto in Western Europe.</p>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>At the top menu bar of the form builder click <code><i class="fab fa-chromecast"></i> Share</code>. The Share pane will show up on the right side of the form builder.</p>
<p>Over there choose for method <code>Share a link</code> and you will see the URL of the shareable link. You can open the link immediately for yourself, or copy the URL to your clipboard to paste it somewhere else.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-share.png" width="1200" height="760" alt="Screenshot of sharing in Tripetto" loading="lazy" />
  <figcaption>The Share screen in the studio.</figcaption>
</figure>

<h3 id="url">URL</h3>
<p>The shareable link is always served from the <code>tripetto.app</code> domain. It's not possible to customize this direct link.</p>
<p>If you don't want to show the <code>tripetto.app</code> domain to your respondents, you can use a URL shortener service (for example Bitly), or <a href="{{ page.base }}help/articles/how-to-embed-a-form-from-the-studio-into-your-website/">embed the form in your website</a> to serve it from your own domain.</p>

<h3 id="optimize" data-anchor="Optimize sharing preview">Optimize your sharing preview</h3>
<p>You can optimize the direct link to your form for better sharing it across the web by adding a title, description and keywords to it. That information will be used inside previews when you share the link to your form.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-optimize-your-form-for-better-sharing/">How to optimize your form for better sharing</a></li>
</ul>

<h3 id="pause-resume">Pause and resume</h3>
<p>Tripetto offers a <a href="{{ page.base }}help/articles/let-your-respondents-pause-and-resume-their-form-entries/">pause and resume functionality</a>, so your respondents can continue filling out your form later on.</p>
<p>In the shareable link this function is always enabled and can not be disabled.</p>
<hr/>

<h2 id="data-storage">Data storage</h2>
<p>By using the shareable link, both the form and collected data are always stored under your account at Tripetto in Western Europe.</p>

<h3 id="self-hosted">Self-hosted</h3>
<p>If you want to take full control over your data and store it on your own, the shareable link is not for you. Instead you can integrate the form in your website/application with the Tripetto FormBuilder SDK, which gives you control over your data storage.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-integrate-a-form-form-the-studio-into-your-website-or-application/">How to integrate a form from the studio into your website or application</a></li>
</ul>
