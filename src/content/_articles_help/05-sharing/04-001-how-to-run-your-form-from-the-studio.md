---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-run-your-form-from-the-studio/
title: Run your form from the studio - Tripetto Help Center
description: In the studio you can choose from different sharing methods, with data storage at Tripetto or self-hosted.
article_title: How to run your form from the studio
article_folder: studio-share
author: jurgen
time: 2
category_id: sharing
subcategory: sharing_methods
areas: [studio]
---
<p>In the studio you can choose from different sharing methods, with data storage at Tripetto or self-hosted.</p>

<h2 id="share">Sharing forms</h2>
<p>With the Tripetto studio there are several options to share your form, depending on where you want to store your collected data and how you want to share your form.</p>
<p>At the top menu bar of the form builder click <code><i class="fab fa-chromecast"></i> Share</code>. The Share pane will show up on the right side of the form builder.</p>

<h3 id="methods">Sharing methods</h3>
<p>Now you can choose from different sharing methods, divided by where you want to store your data:</p>
<ul>
  <li><strong>Hosted by Tripetto</strong>: if you want to store data at Tripetto, you can share your form with a direct link or embed it in your website.</li>
  <li><strong>Self-hosted</strong>: if you want to store data outside of Tripetto, you can take control over your data. In that case you can build your form in the Tripetto studio and then integrate the form in your website/application with the Tripetto FormBuilder SDK, which gives you control over your data storage.</li>
</ul>
<p>Read further to learn about the different options and which is meant for your case.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-share.png" width="1200" height="760" alt="Screenshot of sharing in Tripetto" loading="lazy" />
  <figcaption>The Share screen in the studio.</figcaption>
</figure>
<hr/>

<h2 id="tripetto-hosted">Hosted by Tripetto</h2>
<p>By default your data is stored at Tripetto. That means both the form and collected data are stored under your account at Tripetto in Western Europe. In that case you can share a direct link to your form or embed it in your website.</p>

<h3 id="share" data-anchor="Share a link">🧭 Share a link</h3>
<p>Select <code>Share a link</code> to receive a direct link to your form via the <code>tripetto.app</code> domain. You can just share that link with your audience and store your collected data at Tripetto.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-share-a-link-to-your-form-from-the-studio/">How to share a link to your form from the studio</a></li>
</ul>

<h3 id="embed" data-anchor="Embed in a website">📋 Embed in a website</h3>
<p>Select <code>Embed in a website</code> to receive an embed code that you can use to show your form in a website. The form then becomes available via your website's domain. The collected data is stored at Tripetto.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-embed-a-form-from-the-studio-into-your-website/">How to embed a form from the studio into your website</a></li>
</ul>
<hr/>

<h2 id="self-hosted">Self-hosted</h2>
<p>The above methods all store your data at Tripetto. It's also possible to take control over your data and store the collected data on your own. In that case you can integrate your form in a website or application using the Tripetto FormBuilder SDK.</p>
<blockquote>
  <h4>🔔 Notice: Tripetto FormBuilder SDK required</h4>
  <p>By using the self-hosted option from the studio, you enter a different version of Tripetto: the Tripetto FormBuilder SDK, for which specific terms and pricing apply. More information:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/" target="_blank">Tripetto FormBuilder SDK website</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/pricing/" target="_blank">Tripetto FormBuilder SDK pricing</a></li>
  </ul>
</blockquote>

<h3 id="integrate" data-anchor="Integrate in a website/application">👨‍💻 Integrate in a website or application</h3>
<p>Select <code>Integrate in a website or application</code> to receive the so-called "form definition" of the form. With that you can integrate the form in a website or application using the Tripetto FormBuilder SDK and take control over your data storage.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-integrate-a-form-form-the-studio-into-your-website-or-application/">How to integrate a form from the studio into your website or application</a></li>
</ul>
