---
layout: help-article
base: ../../../
permalink: /help/articles/let-your-form-save-and-restore-uncompleted-entries/
title: Save and restore uncompleted entries - Tripetto Help Center
description: Tripetto forms can automatically save and restore uncompleted forms, so your respondents can continue where they left off.
article_title: Let your form save and restore uncompleted entries
author: mark
time: 2
category_id: sharing
subcategory: sharing_uncompleted
areas: [studio, wordpress]
---
<p>Tripetto forms can automatically save and restore uncompleted forms, so your respondents can continue where they left off. For example handy when you have a support form that's on each page in your website.</p>

<blockquote>
  <h4>📣 Info: No data from uncompleted entries</h4>
  <p>This feature does NOT store form data from uncompleted entries to Tripetto. Tripetto forms only store form data when the respondent actively submits the form. As the owner of the form you will NOT be able to see form data from uncompleted entries.</p>
</blockquote>
<h2 id="how-it-works">How it works</h2>
<p>If the save and restore functionality is enabled, the form will save a snapshot of the given answers inside the <strong>local storage of the respondent's browser</strong>. If the form is loaded again, the form will use that local storage snapshot to fill out the known answers and take the respondent back to the place where they left the form.</p>
<p>As we use the local storage of the browser, this will only work when the respondent uses the same browser when they revisit the form.</p>
<hr/>

<h2 id="availability">Availability</h2>
<p>Depending on how you use your form, the availability of the save and restore function can differ:</p>
<ul>
  <li>
    <h3 id="shareable-links">In a shareable link</h3>
    <p>If your respondents use the form from a shareable link, the save and restore functionality is not available.</p>
  </li>
  <li>
    <h3 id="embed">In an embedded form</h3>
    <p>If your respondents use an embedded version of your form, you can manually activate the save and restore functionality inside the embed code options.</p>
  </li>
</ul>
