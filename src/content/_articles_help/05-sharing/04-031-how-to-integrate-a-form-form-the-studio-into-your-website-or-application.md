---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-integrate-a-form-form-the-studio-into-your-website-or-application/
title: Integrate a form from the studio into your website - Tripetto Help Center
description: Learn how to integrate a form from the studio into your own website or application and take control over the collected data.
article_title: How to integrate a form from the studio into your website or application
article_id: integrate-studio
article_folder: studio-integrate
author: mark
time: 5
category_id: sharing
subcategory: sharing_integrate
areas: [studio]
---
<p>Learn how to integrate a form from the studio into your own website or application and take control over the collected data.</p>

<h2 id="when-to-use">When to use</h2>
<p>If you want to deeply integrate your form into your website or application and take control over your data storage, you can use a combination of the Tripetto studio and the Tripetto FormBuilder SDK. Using this method none of your collected data is stored at Tripetto's infrastructure.</p>

<h3 id="integrating">About integrating</h3>
<p>With the integration method you use a combination of the Tripetto studio and the Tripetto FormBuilder SDK. You use the studio to build your forms and the FormBuilder SDK to run your forms outside of any Tripetto environment. This way you can still use Tripetto's form builder, but take control over your collected data.</p>
<blockquote>
  <h4>🔔 Notice: Tripetto FormBuilder SDK required</h4>
  <p>By using this share method, you enter a different version of Tripetto: the Tripetto FormBuilder SDK, for which specific terms and pricing apply. More information:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/" target="_blank">Tripetto FormBuilder SDK website</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/pricing/" target="_blank">Tripetto FormBuilder SDK pricing</a></li>
  </ul>
</blockquote>
<h3 id="embed-difference">Difference with embedding</h3>
<p>It's also possible to simply embed your form in a website, using the <a href="{{ page.base }}help/articles/how-to-embed-a-form-from-the-studio-into-your-website/">studio embed code</a>. Then what's the difference between embedding and integrating?</p>
<p>Integrating takes things to the next level. It enables you to use the core components from the FormBuilder SDK, instead of the end user scripts from the studio.</p>
<p>Next to that, there's a big difference with data storage. The studio embed code stores collected data under your account at Tripetto in Western Europe. It's not possible to self-host that data from an embedded form. By integrating your form with the FormBuilder SDK though, you get full control over your data storage. This enables you to store the collected data on your own, without it ever touching Tripetto's studio environment.</p>
<blockquote>
  <h4>📌 Also see: Looking for simple embed?</h4>
  <p>You just want to embed your form and store collected data under your account at Tripetto? In that case choose for embedding your Tripetto form:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-embed-a-form-from-the-studio-into-your-website/">How to embed a form from the studio into your website</a></li>
  </ul>
</blockquote>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>At the top menu bar of the form builder click <code><i class="fab fa-chromecast"></i> Share</code>. The Share pane will show up on the right side of the form builder.</p>
<p>Over there choose for method <code>Integrate in a website or application</code> and you will see the instructions for integrating.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-integrate.png" width="1200" height="760" alt="Screenshot of integrating in Tripetto" loading="lazy" />
  <figcaption>Choose for integrating your form.</figcaption>
</figure>

<h3 id="copy-embed">Form definition</h3>
<p>You now see the form definition that you can use in the FormBuilder SDK. It's a JSON string containing your entire form and is all you need to run a form.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-form-definition.png" width="605" height="376" alt="Screenshot of a form definition in Tripetto" loading="lazy" />
  <figcaption>Your form definition.</figcaption>
</figure>
<hr/>

<h2 id="integrate">Integrate form</h2>
<p>Now you can integrate this form definition into a working form with the FormBuilder SDK.</p>
<blockquote>
  <h4>🔔 Notice: Tripetto FormBuilder SDK required</h4>
  <p>From here on you use the Tripetto FormBuilder SDK, for which specific terms and pricing apply. More information:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/" target="_blank">Tripetto FormBuilder SDK website</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/pricing/" target="_blank">Tripetto FormBuilder SDK pricing</a></li>
  </ul>
</blockquote>

<h3 id="runner">Form runner</h3>
<p>To run a Tripetto form, you need a so-called runner. A runner turns a form definition (which you have extracted from the studio) into a fully working form. It performs the actual rendering of the form. Tripetto offers <a href="{{ page.base }}sdk/docs/runner/stock/introduction/" target="_blank">3 stock runners</a> which are full-featured runners with <a href="{{ page.base }}form-layouts/">its own unique UI</a>, built and maintained by the Tripetto team.</p>
<blockquote>
  <h4>📌 Also see: FormBuilder SDK Docs</h4>
  <p>Please see the <a href="{{ page.base }}sdk/" target="_blank">Tripetto FormBuilder SDK</a> documentation for more technical guidance on the runner implementation:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/docs/runner/stock/quickstart/" target="_blank">Form Runner quickstart</a></li>
  </ul>
</blockquote>

<h3 id="response-data">Collecting response data</h3>
<p>Now that you have your form running with a runner, you can make sure that the collected data gets stored on your end. This is done with the <a href="{{ page.base }}sdk/docs/runner/stock/guides/collecting/" target="_blank"><code>onSubmit</code></a> event in the runner that you just have implemented. By using one of the supplied <a href="{{ page.base }}sdk/docs/runner/api/library/modules/Export/" target="_blank"><code>Export</code></a> functions from the Runner library you can retrieve the data in a convenient format.</p>
<blockquote>
  <h4>📌 Also see: FormBuilder SDK Docs</h4>
  <p>Please see the <a href="{{ page.base }}sdk/" target="_blank">Tripetto FormBuilder SDK</a> documentation for more technical guidance on data storage:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/docs/runner/stock/guides/collecting/" target="_blank">Collecting response data guide</a></li>
  </ul>
</blockquote>
<hr/>

<h2 id="data-storage">Data storage</h2>
<p>By using the Tripetto FormBuilder SDK to integrate your form, you take full control over your data. You can still build your form in the Tripetto studio, but the collected data is stored on your own infrastructure.</p>
