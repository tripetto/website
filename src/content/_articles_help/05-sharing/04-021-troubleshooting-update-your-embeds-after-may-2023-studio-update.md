---
layout: help-article
base: ../../../
permalink: /help/articles/troubleshooting-update-your-embeds-after-may-2023-studio-update/
title: Update studio embeds (May 2023 update) - Tripetto Help Center
description: With the studio update of May 2023 the studio embed codes are revised. Read this article to learn how this impacts you.
article_title: Troubleshooting - Update your embeds after May 2023 studio update
author: jurgen
time: 5
category_id: sharing
subcategory: sharing_embed
areas: [studio]
scripts: [prism]
stylesheets: [prism-dark]
---
<p>With the studio update of May 2023 the studio embed codes are revised. Read this article to learn how this impacts you.</p>

<h2 id="may-2023-update">May 2023 studio update</h2>
<p>With the May 2023 update of the Tripetto studio, the embed code got a revision. Besides some under the hood improvements to make the embeds future-proof, the embed code itself also became smaller and easier.</p>
<h3 id="comparison">Comparison</h3>
<p>Compare the different embed codes yourself, so you can determine which version you are currently using:</p>

<h4>Embed code before May 2023 update</h4>
<pre class="line-numbers"><code class="language-html">&lt;div id="tripetto">&lt;/div>
&lt;script src="https://cdn.jsdelivr.net/npm/tripetto-runner-foundation">&lt;/script>
&lt;script src="https://cdn.jsdelivr.net/npm/tripetto-runner-autoscroll">&lt;/script>
&lt;script src="https://cdn.jsdelivr.net/npm/tripetto-services">&lt;/script>
&lt;script>
var tripetto = TripettoServices.init({ token: "STUDIO_FORM_TOKEN" });

TripettoAutoscroll.run({
    element: document.getElementById("tripetto"),
    definition: tripetto.definition,
    styles: tripetto.styles,
    l10n: tripetto.l10n,
    locale: tripetto.locale,
    translations: tripetto.translations,
    attachments: tripetto.attachments,
    onSubmit: tripetto.onSubmit
});
&lt;/script></code></pre>

<h4>Embed code since May 2023 update</h4>
<pre class="line-numbers"><code class="language-html">&lt;div id="tripetto-div-id">&lt;/div>
&lt;script src="https://cdn.jsdelivr.net/npm/@tripetto/runner">&lt;/script>
&lt;script src="https://cdn.jsdelivr.net/npm/@tripetto/runner-autoscroll">&lt;/script>
&lt;script src="https://cdn.jsdelivr.net/npm/@tripetto/studio">&lt;/script>
&lt;script>
TripettoStudio.form({
    runner: TripettoAutoscroll,
    token: "STUDIO_FORM_TOKEN",
    element: "tripetto-div-id"
});
&lt;/script></code></pre>

<h3 id="legacy">Legacy</h3>
<p>The new embed code runs on the latest Tripetto core components. The core components that the old embed code uses, will not be updated anymore from here on. Therefore we advise to always update your embed code(s), so you keep up to date with the latest version of Tripetto and you are sure everything keeps working; now and in the future.</p>
<hr/>

<h2 id="when-to-update">When to update</h2>
<p>Although it is advised to update your embed code anyway, your current embed code keeps working. Let's have a look at the most common situations in which you have to take action:</p>
<ul>
  <li>Embed with studio-hosted data: updating not necessary, but advised.</li>
  <li>Embed with self-hosted data: updating advised and shift to Tripetto FormBuilder SDK required.</li>
</ul>

<h3 id="embed-studio-hosted" data-anchor="Studio-hosted embed">Embed with studio-hosted data</h3>
<p>The old embed codes of forms that store their data at Tripetto, will simply keep working. Still, it is advised to update your embed code(s) (<a href="#how-to-update" class="anchor">see instructions</a>). Only then you're guaranteed that your embed code keeps working with any change you might do in your form in the future. Plus it guarantees that you can use any new features that we introduce in the future!</p>

<h3 id="embed-self-hosted" data-anchor="Self-hosted embed">Embed with self-hosted data</h3>
<p>In case you are using self-hosting options in the old embed code (for example to store data on your own infrastructure), these will keep working. Still, it is advised to update your embed code to be sure your embed code keeps working with any modifications you do in your form and is able to use any new features in Tripetto.</p>
<p>In regards to self-hosting the May 2023 update possibly has some more impact for you: self-hosting is no longer possible in the default embed codes from the Tripetto studio. The self-hosting options have shifted to the 'Integrate' share method in the Tripetto studio. By using this new share method, you enter a different version of Tripetto: the Tripetto FormBuilder SDK, for which specific terms and pricing apply. For more information <a href="{{ page.base }}help/articles/how-to-integrate-a-form-form-the-studio-into-your-website-or-application/">see this help article about integrating</a>.</p>
<blockquote>
  <h4>🔔 Notice: Tripetto FormBuilder SDK required</h4>
  <p>By using the 'Integrate' share method, you enter a different version of Tripetto: the Tripetto FormBuilder SDK, for which specific terms and pricing apply. More information:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/" target="_blank">Tripetto FormBuilder SDK website</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/pricing/" target="_blank">Tripetto FormBuilder SDK pricing</a></li>
  </ul>
</blockquote>
<p>If this is the case for you, we gladly hear from you to help you migrating. Please <a href="{{ page.base }}support/">submit a support ticket</a> and we'll contact you as soon as possible.</p>
<hr/>

<h2 id="how-to-update">How to update</h2>
<p>Updating your embed codes is quite easy in most cases, except when you use self-hosting (<a href="#update-self-hosting" class="anchor">see below</a>).</p>
<h3 id="steps">Steps to take</h3>
<ol>
  <li><strong>Get embed code</strong> - At the top menu bar of the form builder click <code><i class="fab fa-chromecast"></i> Share</code>. The Share pane will show up on the right side of the form builder. Over there choose for method <code>Embed in a website</code> and you will see the embed code and some settings. Configure the embed code as required for your case and copy it to your clipboard.</li>
  <li><strong>Replace embed code</strong> - In your site replace the old embed code with the newly generated embed code. Please notice the following:
    <ul>
      <li><strong>Check HTML element</strong> - The new embed code uses unique div id's for inline implementation. Please make sure you attach the form to the corresponding div element via the <code>element</code> parameter in your embed code.</li>
      <li><strong>Copy custom CSS</strong> - The embed code from the studio does not include any custom CSS. In case you had implemented the <code>customCSS</code> parameter in your old embed code, make sure you add this to your new embed code as well, within the <code>TripettoStudio.form()</code> function.</li>
      <li><strong>Update custom CSS</strong> - The way you add custom CSS also has gotten a slightly revision, because the package names of the question blocks have been altered. To see the differences <a href="{{ page.base }}help/articles/how-to-use-custom-css-in-embedded-forms/">have a look at this help article about custom CSS</a>.</li>
      <li><strong>Scripts via custom domain/CDN</strong> - Using a custom domain/CDN to host the Tripetto scripts? Make sure you update these scripts to the latest versions. You can download the latest versions via the Share pane in the Tripetto studio. Please note that the file names of the scripts have changed as well.</li>
    </ul>
  </li>
  <li><strong>Publish update</strong> - After updating, of course don't forget to publish the update to your live website.</li>
</ol>

<h3 id="update-self-hosting">Self-hosting</h3>
<p>Have you implemented self-hosting options in your old embed code (used the <code>definition</code> parameter or customized the <code>onSubmit</code> parameter)? Self-hosting options are no longer part of the studio embed terms, but now are part of integrating with the FormBuilder SDK. If this is the case for you, <a href="{{ page.base }}help/articles/how-to-integrate-a-form-form-the-studio-into-your-website-or-application/">have a look at this help article about integrating</a>.</p>
<p>If you need any help with switching to the Integrate method or to determine your FormBuilder SDK license, please <a href="{{ page.base }}support/">submit a support ticket</a> and we'll contact you as soon as possible.</p>
<hr/>

<h2 id="support">Still having issues?</h2>
<p>Despite the above instructions, you're still experiencing issues? Don't hesitate to <a href="{{ page.base }}support/">submit a support ticket</a>. We're happy to help you!</p>
