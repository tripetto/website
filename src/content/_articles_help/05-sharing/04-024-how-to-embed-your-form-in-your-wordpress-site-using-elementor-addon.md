---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-elementor-widget/
title: Embed form in WordPress with Elementor widget - Tripetto Help Center
description: If you use the Elementor builder in WordPress, you can embed your Tripetto forms inside your WordPress site using Tripetto's Elementor widget.
article_title: How to embed your form in your WordPress site using the Elementor widget
article_id: embed-wordpress
article_video: sharing-wordpress-elementor
article_folder: wordpress-elementor
author: mark
time: 3
time_video: 2
category_id: sharing
subcategory: sharing_embed
areas: [wordpress]
scripts: [prism]
stylesheets: [prism-dark]
---
<p>If you use the Elementor builder in WordPress, you can embed your Tripetto forms inside your WordPress site using Tripetto's Elementor widget.</p>

<h2 id="when-to-use">When to use</h2>
<p>By embedding your form in your site you can place your form inside your site's content. If you use the popular Elementor page builder, Tripetto's Elementor widget is the easiest way to achieve this. All data will be stored inside your own WordPress instance.</p>
<blockquote>
  <h4>📌 Also see: Tripetto's other embed options</h4>
  <p>We offer several ways to embed your forms in your WordPress site. It largely depends on the WordPress builder you use which method is best and easiest for you:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-shortcode/">Embed with WordPress shortcode</a>, if you use a traditional editor</li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-gutenberg-block/">Embed with Gutenberg block</a>, if you use the Gutenberg editor</li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-elementor-widget/">Embed with Elementor widget</a>, if you use the Elementor builder<span class="related-current">Current article</span></li>
  </ul>
</blockquote>

<h3 id="elementor">About Elementor</h3>
<p>Elementor is the leading WordPress website builder to design and build everything you need for your website. <a href="https://elementor.com/" target="_blank" rel="noopener noreferrer">You can read all about it at elementor.com.</a></p>
<p>After you have installed the Tripetto WordPress plugin, Tripetto is available as an Elementor widget inside the Elementor builder to easily embed Tripetto forms in your content. You can use Tripetto forms in Elementor pages, but also inside Elementor popups.</p>
<div>
  <a href="https://elementor.com/" target="_blank" rel="noopener noreferrer" class="blocklink">
    <div>
      <span class="title">Elementor: #1 Free WordPress Website Builder<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Elementor is the platform web creators choose to build professional WordPress websites, grow their skills, and build their business.</span>
      <span class="url">elementor.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-elementor.png" width="151" height="151" alt="Elementor logo" loading="lazy" />
    </div>
  </a>
</div>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>At the left side of your Elementor builder you can search for the <code>Tripetto Form</code> widget. You can drag and drop it into the place where you want to show the form. You can use the Tripetto Form widget in Elementor pages, but also inside Elementor popups.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/elementor-widget.png" width="1200" height="760" alt="Screenshot of Tripetto in the Elementor builder" loading="lazy" />
  <figcaption>Add Tripetto as an Elementor widget.</figcaption>
</figure>
<p>This will add a new Tripetto widget to your Elementor builder. At the left side you can now simply select the Tripetto form that you want to show. After you have selected the desired form, it will be added to your content and you will see a live preview of the form right away. That's it!</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/insert-existing.gif" width="1200" height="760" alt="Screen recording of the Tripetto Elementor widget" loading="lazy" />
  <figcaption>Select the desired form in the Tripetto Elementor widget.</figcaption>
</figure>

<h3 id="options">Options</h3>
<p>At the left side of the Elementor builder you can configure the implementation of the form into your WordPress site. You can just toggle the desired options and the Tripetto form will update automatically.</p>
<p>The following options are available:</p>
<ul>
  <li><h4>Allow pausing and resuming</h4>Setting to determine if the form can be <a href="{{ page.base }}help/articles/let-your-respondents-pause-and-resume-their-form-entries/">paused and resumed</a> by your respondents.</li>
  <li><h4>Save and restore uncompleted forms</h4>Setting to determine if <a href="{{ page.base }}help/articles/let-your-form-save-and-restore-uncompleted-entries/">data persists in the browser</a> if a user enters the form later on again.</li>
  <li><h4>Disable asynchronous loading</h4>Option to disable asynchronous loading of your form to enhance the form loading time (please be aware that caching plugins may have effect on your Tripetto forms if you use this option).</li>
  <li><h4>Width</h4>Setting to set the width of form container.</li>
  <li><h4>Height</h4>Setting to set the height of form container.</li>
  <li><h4>Placeholder</h4>Option to show a message that is shown while the form is loading. You can specify text or HTML.</li>
  <li><h4>Custom CSS</h4>Option to add custom CSS to parts of your form (<a href="{{ page.base }}help/articles/how-to-use-custom-css-in-embedded-forms/">see instructions</a>).</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/options.png" width="1200" height="760" alt="Screenshot of Tripetto in the Elementor builder" loading="lazy" />
  <figcaption>Customize the desired options for your form.</figcaption>
</figure>

<h3 id="responsiveness">Responsiveness</h3>
<p>Tripetto forms are of course designed to work properly on all screen sizes. This is called 'responsiveness'.</p>
<p>If you embed a Tripetto form in your own page, the responsiveness of the form will depend on the responsiveness settings of your own page. In most cases that is controlled by the viewport settings in the <code>head</code> section of your website. We advise to configure that like this:</p>
<pre class="line-numbers"><code class="language-html">&lt;meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"></code></pre>
<hr />

<h2 id="data">Data storage</h2>
<p>Both the form and collected data are always hosted and stored in your own WordPress instance. Not a single connection related to Tripetto is ever made with an external host other than yours. It's all in your own instance, under your control.</p>
