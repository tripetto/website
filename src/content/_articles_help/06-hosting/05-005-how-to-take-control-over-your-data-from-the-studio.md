---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-take-control-over-your-data-from-the-studio/
title: Take control over your data from the studio - Tripetto Help Center
description: Data control is important to us (and hopefully to you too). With Tripetto you can take full control over your data, so no data will be stored at our end.
article_title: How to take control over your data from the studio
article_id: data-studio
article_folder: studio-embed
author: mark
time: 4
category_id: hosting
subcategory: hosting_data_control
areas: [studio]
scripts: [prism]
stylesheets: [prism-dark]
---
<p>Data control is important to us (and hopefully to you too). With Tripetto you can take full control over your data, so no data will be stored at our end. It's up to you!</p>

<h2 id="about">The importance of data control</h2>
<p>As we’ve all shifted to the cloud, the way we think about data storage has changed. Most form builder solutions, like Typeform and SurveyMonkey, made it very easy to build forms with great user experiences. The downside of those solutions is they always store your collected data on their infrastructure. That way you don't have control over your collected data. You have ro rely on their security infrastructures (which were breached multiple times in the past). Besides that you also don't know how they use your collected data, for example for market research.</p>

<h3 id="gdpr">GDPR</h3>
<p>With the introduction of the General Data Protection Regulation (GDPR) in 2018, the European Union protects persons regarding the processing and free movement of their personal data. This is another reason to be highly careful with storing sensitive data with third-party services.</p>

<h3 id="form-data-control">Form data control</h3>
<p>Form data, even if it doesn’t seem that important, is regularly some of the most private information we put online. We type our names, addresses, social security numbers and much more into forms on the internet, without considering who’s hosting it.</p>
<p>Especially when you are collecting sensitive data, you don't want to be dependent on those third-party solutions where you have zero control over your data. In that case you ideally want to use a form solution to offer the best experience to your respondents while filling out your form, but store the data on your own infrastructure. That way you have full control over data flows and security, without it being stored on third-party infrastructure.</p>
<hr/>

<h2 id="studio">Data storage in Tripetto studio</h2>
<p>By using the Tripetto studio you get the best of both worlds: you build highly attractive and conversion-friendly forms for your respondents. And on top of that you are flexible in terms of data storage: you can store your data at Tripetto, but also take control over your data with self-hosting.</p>

<h3 id="tripetto-hosted">Hosted by Tripetto</h3>
<p>When you share your form from the studio via a <a href="{{ page.base }}help/articles/how-to-share-a-link-to-your-form-from-the-studio/">direct link</a> or a <a href="{{ page.base }}help/articles/how-to-embed-a-form-from-the-studio-into-your-website/">studio embed</a>, both the form and collected data are always stored under your account at Tripetto in Western Europe. Of course your data is stored safe and we don't ever use your collected data.</p>

<h3 id="self-hosted">Self-hosted</h3>
<p>But from the studio it's also possible to store the collected data on your own, completely bypassing the Tripetto infrastructure. That way you still use the Tripetto studio to build and style your form, but you integrate your form in a website or application using the Tripetto FormBuilder SDK. You can then store the collected data on your own infrastructure, without it ever touching Tripetto's infrastructure.</p>
<blockquote>
  <h4>🔔 Notice: Tripetto FormBuilder SDK required</h4>
  <p>By integrating a form from the studio, you enter a different version of Tripetto: the Tripetto FormBuilder SDK, for which specific terms and pricing apply. More information:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/" target="_blank">Tripetto FormBuilder SDK website</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/pricing/" target="_blank">Tripetto FormBuilder SDK pricing</a></li>
  </ul>
</blockquote>
<p>Intructions on how to set this up are described in an other help article. <a href="{{ page.base }}help/articles/how-to-integrate-a-form-form-the-studio-into-your-website-or-application/">See this article for more information about integrating with data control.</a></p>
