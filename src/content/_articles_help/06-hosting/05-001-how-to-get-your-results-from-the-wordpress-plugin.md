---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-get-your-results-from-the-wordpress-plugin/
title: Get your results from the WordPress plugin - Tripetto Help Center
description: Learn how you can manage and export your results to CSV from the WordPress plugin to use in Excel or Google Sheets.
article_title: How to get your results from the WordPress plugin
article_id: download-wordpress
article_folder: wordpress-results
article_video: hosting-wordpress-results
author: jurgen
time: 3
time_video: 1
category_id: hosting
subcategory: hosting_results
areas: [wordpress]
---
<p>Learn how you can manage and export your results to CSV so you can use your data for example in Excel or Google Sheets.</p>

<h2 id="manage">Manage results</h2>
<p>Inside the WordPress plugin you can see and manage all results of each form. From the list of your forms (also see <a href="{{ page.base }}help/articles/how-to-add-and-manage-forms-in-the-wordpress-plugin/">this article</a>) you can click <code>Results</code> by moving your cursor over each row in the list.</p>
<p>You can also click <code>Results</code> at the top menu of the form builder while editing a certain form.</p>

<h3 id="results">View results</h3>
<p>This will show a list of all results you received for the selected form.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/results-list.png" width="1200" height="760" alt="Screenshot of results in Tripetto" loading="lazy" />
  <figcaption>List of results with custom columns.</figcaption>
</figure>

<h4>Column selection</h4>
<p>By default this list will only contain some basic columns with meta data of each result (Number/Identifier/Date submitted). To make the list of results more useful, you can customize the columns and show actual data from your form in that list.</p>
<p>To do so, click <code>Choose columns</code> at the top right corner of the results list. This option will become available when you have at least one result in your results list.</p>
<p>From there on you can select which columns you want to show in the results list:</p>
<ul>
  <li>Basic meta data: you can select which meta data columns you want to show (Number/Identifier/Date submitted);</li>
  <li>Submission data: you can select which question block(s) from your form you want to show. This way you can for example show the name of each respondent in the results list.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/columns.png" width="1200" height="760" alt="Screenshot of column selection in Tripetto" loading="lazy" />
  <figcaption>Select the columns you want to use in the results list.</figcaption>
</figure>

<h4>View result</h4>
<p>From the list of results, you can click <code>View result</code> to open that particular result. That will show all given answers of that result right away.</p>
<p>If you have a <a href="{{ page.base }}help/articles/how-to-use-the-file-upload-block/">File upload block</a> and/or a <a href="{{ page.base }}help/articles/how-to-use-the-signature-block/">Signature block</a> in your form, the files from your respondents are available as preview and as individual download links in the result view.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/result.png" width="1200" height="760" alt="Screenshot of a result in Tripetto" loading="lazy" />
  <figcaption>View the submitted data of each response.</figcaption>
</figure>

<h3 id="delete">Delete results</h3>
<p>From the list of results, you can delete result(s) that you no longer need.</p>
<h4>Single delete</h4>
<p>You can delete single results by moving your cursor over a row in the results list and click <code>Delete</code>. You will be asked for confirmation before the result gets deleted. This will permanently delete the selected result.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/delete.png" width="1200" height="760" alt="Screenshot of deleting a result in Tripetto" loading="lazy" />
  <figcaption>Confirm deleting a single result.</figcaption>
</figure>
<h4>Bulk delete</h4>
<p>You can delete multiple results at once by selecting the checkboxes in front of the results you want to delete. After you selected the desired result(s), you can collapse the <code>Bulk actions</code> dropdown<i class="fas fa-arrow-right"></i>Select<code> Delete</code><i class="fas fa-arrow-right"></i>Click <code>Apply</code>. This action will be executed without an extra confirmation. This will permanently delete the selected result(s).</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/delete-bulk.png" width="1200" height="760" alt="Screenshot of bulk deleting results in Tripetto" loading="lazy" />
  <figcaption>Select multiple results to delete them.</figcaption>
</figure>
<hr/>

<h2 id="export">Export results</h2>
<p>Exporting your results is very easy. From the top of the Results screen just click <code>Download to CSV</code>. The download of your results will start immediately and will be downloaded as a CSV file to your Downloads folder.</p>
<p>If you have a <a href="{{ page.base }}help/articles/how-to-use-the-file-upload-block/">File upload block</a> and/or a <a href="{{ page.base }}help/articles/how-to-use-the-signature-block/">Signature block</a> in your form, the files from your respondents are available as individual download links in the CSV export.</p>

<h3 id="multiple">Multiple download buttons</h3>
<p>It can be possible that you see multiple download buttons, grouped by date. This means the form has results with different data fieldsets (columns in your CSV file). This can occur when you change the structure of your form, resulting in a different fieldset of your form data. More information:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/troubleshooting-seeing-multiple-download-buttons/">Troubleshooting - Seeing multiple download buttons</a></li>
</ul>
<hr/>

<h2 id="use">Use results</h2>
<p>If you have downloaded the CSV file you can do what you'd like with the data. For example, open the CSV file in a spreadsheet editor, like Microsoft Excel or Google Sheets. In there you can convert the comma-separated values (CSV) to columns. More information:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-your-csv-file-in-office-excel-or-google-sheets/">How to use your CSV file in Office Excel or Google Sheets</a></li>
</ul>
<p>After that, you can process your data, for example, by calculating averages or displaying the data in graphs. How to do that, depends on the spreadsheet editor you are using.</p>
<hr/>

<h2 id="notifications">Receive notifications</h2>
<p>You can also get notifications of new responses by email and/or in Slack. You can include all form data directly in those notifications. Please see the following articles for more information:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automate-email-notifications-for-each-new-result/">How to get email notifications for each new result</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automate-slack-notifications-for-each-new-result/">How to get Slack notifications for each new result</a></li>
</ul>
<hr/>

<h2 id="automate">Automate follow-ups</h2>
<p>Another option to use your results is by connecting your form with a webhook to automate all kinds of actions. More information:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/">How to automate a webhook to connect to other services for each new result</a></li>
</ul>

<h3>Automation examples</h3>
<p>We have made some step-by-step examples for often used automation scenarios:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-store-form-responses-in-google-sheets/">How to automatically store form responses in Google Sheets and generate analysis reports</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-store-form-responses-in-notion/">How to automatically store form responses in Notion</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-create-PDF-files-from-form-responses/">How to automatically create PDF files from form responses and send these to respondents</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-add-mailchimp-subscribers-from-form-responses/">How to automatically add Mailchimp subscribers from form responses</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-create-zendesk-support-tickets-from-form-responses/">How to automatically create Zendesk support tickets from form responses</a></li>
</ul>
