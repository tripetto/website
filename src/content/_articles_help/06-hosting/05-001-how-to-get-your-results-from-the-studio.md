---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-get-your-results-from-the-studio/
title: Get your results from the studio - Tripetto Help Center
description: Learn how you can manage and export your results to CSV from the studio to use in Excel or Google Sheets.
article_title: How to get your results from the studio
article_id: download-studio
article_folder: studio-results
article_video: hosting-studio-results
author: jurgen
time: 3
time_video: 1
category_id: hosting
subcategory: hosting_results
areas: [studio]
---
<p>Learn how you can manage and export your results to CSV so you can use your data for example in Excel or Google Sheets.</p>

<h2 id="manage">Manage results</h2>
<p>Inside the studio you can see and manage all results of each form. At the top menu bar of the form builder click <code><i class="fas fa-download"></i> Results</code>. The Result pane will show up on the right side of the form builder.</p>

<h3 id="view">View results</h3>
<p>This will show a list of all results you received for the particular form.</p>

<h4>View result</h4>
<p>From the list of results, you can click on a result to open that particular result. That will show all given answers of that result right away.</p>
<p>If you have a <a href="{{ page.base }}help/articles/how-to-use-the-file-upload-block/">File upload block</a> and/or a <a href="{{ page.base }}help/articles/how-to-use-the-signature-block/">Signature block</a> in your form, the files from your respondents are available as preview and as individual download links in the result view.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/view.png" width="1200" height="768" alt="Screenshot of results in Tripetto" loading="lazy" />
  <figcaption>View list of entries and the details of each response.</figcaption>
</figure>

<h3 id="delete">Delete results</h3>
<p>From the list of results, you can delete one or multiple result(s) that you don't longer need. By clicking the <code><i class="fas fa-trash-alt"></i></code> icon without selecting a result row, the result list will become multiselectable, so you can select the rows you want to delete. After the selection, click <code><i class="fas fa-trash-alt"></i> Delete</code> to permanently delete those results (after confirmation).</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/delete.png" width="1200" height="768" alt="Screenshot of results in Tripetto" loading="lazy" />
  <figcaption>Select multiple responses to delete them.</figcaption>
</figure>
<hr/>

<h2 id="export">Export results</h2>
<p>Exporting your results is very easy. From the top of the Results pane, just click <code><i class="fas fa-download"></i> Download</code><i class="fas fa-arrow-right"></i>Select right download version (also see chapter <a href="#multiple" class="anchor">Multiple download buttons</a>). The download of your results will start immediately and will be downloaded as a CSV file to your Downloads folder.</p>
<p>If you have a <a href="{{ page.base }}help/articles/how-to-use-the-file-upload-block/">File upload block</a> and/or a <a href="{{ page.base }}help/articles/how-to-use-the-signature-block/">Signature block</a> in your form, the files from your respondents are available as individual download links in the CSV export.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/download.png" width="1200" height="768" alt="Screenshot of results in Tripetto" loading="lazy" />
  <figcaption>Click the download button to export your results to CSV.</figcaption>
</figure>

<h3 id="multiple">Multiple download buttons</h3>
<p>It can be possible that you see multiple download buttons, grouped by date. This means the form has results with different data fieldsets (columns in your CSV file). This can occur when you change the structure of your form, resulting in a different fieldset of your form data. More information:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/troubleshooting-seeing-multiple-download-buttons/">Troubleshooting - Seeing multiple download buttons</a></li>
</ul>
<hr/>

<h2 id="use">Use results</h2>
<p>If you have downloaded the CSV file you can do what you'd like with the data. For example, open the CSV file in a spreadsheet editor, like Microsoft Excel or Google Sheets. In there, you can convert the comma-separated values (CSV) to columns. More information:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-your-csv-file-in-office-excel-or-google-sheets/">How to use your CSV file in Office Excel or Google Sheets</a></li>
</ul>
<p>After that, you can process your data, for example, by calculating averages or displaying the data in graphs. How to do that, depends on the spreadsheet editor you are using.</p>
<hr/>

<h2 id="notifications">Receive notifications</h2>
<p>You can also get notifications of new responses by email and/or in Slack. You can include all form data directly in those notifications. Please see the following articles for more information:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automate-email-notifications-for-each-new-result/">How to get email notifications for each new result</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automate-slack-notifications-for-each-new-result/">How to get Slack notifications for each new result</a></li>
</ul>
<hr/>

<h2 id="automate">Automate follow-ups</h2>
<p>Another option to use your results is by connecting your form with a webhook to automate all kinds of actions. More information:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/">How to automate a webhook to connect to other services for each new result</a></li>
</ul>

<h3>Automation examples</h3>
<p>We have made some step-by-step examples for often used automation scenarios:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-store-form-responses-in-google-sheets/">How to automatically store form responses in Google Sheets and generate analysis reports</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-store-form-responses-in-notion/">How to automatically store form responses in Notion</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-create-PDF-files-from-form-responses/">How to automatically create PDF files from form responses and send these to respondents</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-add-mailchimp-subscribers-from-form-responses/">How to automatically add Mailchimp subscribers from form responses</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-create-zendesk-support-tickets-from-form-responses/">How to automatically create Zendesk support tickets from form responses</a></li>
</ul>
