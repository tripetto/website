---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-update-values-throughout-your-form/
title: Update values throughout your form - Tripetto Help Center
description: You can dynamically update block values throughout your form to different values, for example based on given answers of your respondent.
article_title: How to update values throughout your form
article_folder: editor-block-set-value
author: jurgen
time: 4
category_id: logic
subcategory: logic_data_actions
areas: [studio, wordpress]
---
<p>You can dynamically update block values throughout your form to different values, for example based on given answers of your respondent.</p>

<h2 id="when-to-use">When to use</h2>
<p>You can simply prefill certain values in your form, but there are also situations where it can be handy to <strong>update</strong> certain values while the respondent is filling out the form. Some examples:</p>
<ul>
  <li>Based on a given answer to a multiple choice question you can already check some checkboxes later in the form;</li>
  <li>You have two separate email address blocks in your order form: one for the 'Confirmation recipient' and one for the 'Invoice recipient'. Your respondent first fills out the confirmation recipient. Now you can use that entered email address as the value for the invoice recipient;</li>
  <li>You want to compose an advice based on given answers of your respondent and you want to show that advice at the end of the form.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/demo-advanced.gif" width="1200" height="760" alt="Screenshot of prefilled values in Tripetto" loading="lazy" />
  <figcaption>Demonstration of taking over the value from another question block.</figcaption>
</figure>
<blockquote>
  <h4>📌 Also see: Set value block</h4>
  <p>In the article you're reading now we describe how to update values throughout your form. We use the set value block for that. For global instructions about the set value block, please have a look at this article:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-set-value-block/">How to use the set value block</a></li>
  </ul>
</blockquote>
<blockquote>
  <h4>📌 Also see: Prefill values</h4>
  <p>Are you just looking to prefill blocks with an initial value? Please have a look at this article:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-prefill-question-blocks-with-initial-values/">How to prefill question blocks with initial values</a></li>
  </ul>
</blockquote>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>You can update values with the set value block.</p>

<h3 id="add-blocks">Add set value block</h3>
<p>First build your form with the question blocks you want to update the value of.</p>
<p>Continue with adding a <a href="{{ page.base }}help/articles/how-to-use-the-set-value-block/">set value block</a>. Please be aware that the position where you place the set value block in your form can influence how it will behave. Two examples to demonstrate:</p>
<ul>
  <li>If you place a set value block at the beginning of your form, it will immediately set the values that you define;</li>
  <li>But if you place a set value block for example inside a branch, then the defined values/actions will only be executed if the respondent enters that branch.</li>
</ul>

<h3 id="purpose">Select purpose</h3>
<p>Now open the set value block. You will first see the <code>Purpose</code> feature. Set that to <code>Advanced</code>. Now the set value block gives you way more freedom to do all kinds of data actions. You will see those possibilities in the next step.</p>

<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/advanced-setup.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>A form setup with an empty set value block with purpose <code>Advanced</code>.</figcaption>
</figure>

<h3 id="values">Add values</h3>
<p>Now you can make a list of question blocks that you want to update. Per row you can now configure exactly how the value setting should behave:</p>
<ul>
  <li><code>Values</code> - Configure what value you want to enter, for example a fixed value, query string value or from another variable;</li>
  <li><code>Mode</code> - Configure what needs to be done with the current value of a block, for example overwrite it, concatenate it (text values) or perform calculations (number values);</li>
  <li><code>Locking</code> - Configure if the block should be locked for changes by the respondent.</li>
</ul>

<h2 id="variable">Example: take over block/variable value</h2>
<p>In this example we want to take over the entered value of two email address fields. Let's say the value from the 'Confirmation recipient' question should be copied into the 'Invoice recipient' question. Follow these steps:</p>
<ol>
  <li>Start with adding the two email address blocks for 'Confirmation recipient' and 'Invoice recipient';</li>
  <li>Add a set value block at the end af the form (to take over other variables, the set value block has to be entered <u>after</u> the block that you want to take the value from);</li>
  <li>In the new set value block, set the <code>Purpose</code> to <code>Advanced</code>;</li>
  <li>Then click the <code><i class="fas fa-plus"></i></code> button at the bottom of the <code>Values</code> list to add the desired value actions;</li>
  <li>Next select the <code>Invoice recipient</code> question from the form;</li>
  <li>Now select <code>From other variable</code> as the desired value;</li>
  <li>Now select the question block you want to get the value from. In this case that is our <code>Confirmation recipient</code> question;</li>
  <li>Make sure the <code>Mode</code> is set to <code>Overwrite</code>, so the value will be overwritten at each value change.</li>
</ol>
<p>That's it! Now the form will constantly update the value of the 'Invoice recipient' with the entered value in 'Confirmation recipient'.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/advanced-variable.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>The steps to take to overwrite the value with a value from another question block/variable in your form.</figcaption>
</figure>

<h2 id="advice">Example: compose an advice</h2>
<p>In this example we want to compose a list of suggestions, based on selected options by the respondent. Each selected option adds a row to our advice list. At the end we show the full advice as a text. Follow these steps:</p>
<ol>
  <li>Start with building your form. In our example we show checkboxes that respondents can select. For each checkbox the respondent selects, we want to add a suggestion to our advice;</li>
  <li>Add a custom variable block and set the <code>Type</code> of that to <code>Text</code>. Give it a name, for example <code>Advice</code>. We will use this custom variable to compose the advice text;</li>
  <li>Create a logic branch for each checkbox from our checkboxes question;</li>
  <li>Add a set value block inside each branch. We will use those to add the right suggestion for each selected checkbox;</li>
  <li>In each set value block, set the <code>Purpose</code> to <code>Advanced</code>;</li>
  <li>Then click the <code><i class="fas fa-plus"></i></code> button at the bottom of the <code>Values</code> list;</li>
  <li>Next select your custom variable block called <code>Advice</code>;</li>
  <li>Now first set the <code>Mode</code> to <code>Concatenate</code>; with this mode the value of the custom variable will be appended with the advice we will give for this branch. This way we can compose a list of items;</li>
  <li>Now select <code>Fixed value</code> as the desired value;</li>
  <li>You can now enter the advice. And you can select how this must be added to the text value. In this case we want it to start on a <code>New line</code>.</li>
  <li>Repeat this for each branch;</li>
  <li>Now the custom variable <code>Advice</code> will have the full advice. And you can show that to your respondent.</li>
</ol>
<p>That's it! Now the form will show the whole advice that's saved in the custom variable.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/advanced-concatenate.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>The steps to take to concatenate a text value in a custom variable.</figcaption>
</figure>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/advanced-concatenate-end.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>The end result to compose an advice and show that to the respondent.</figcaption>
</figure>
<hr/>

<h2>More information</h2>
<p>The set value block has lots of possibilities, so we have several help articles to learn all about it:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-set-value-block/">How to use the set value block</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-prefill-question-blocks-with-initial-values/">How to prefill question blocks with initial values</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-update-values-throughout-your-form/">How to update values throughout your form</a><span class="related-current">Current article</span></li>
</ul>
