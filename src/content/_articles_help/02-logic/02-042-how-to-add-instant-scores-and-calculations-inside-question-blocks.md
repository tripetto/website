---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/
title: Add instant scores and calculations - Tripetto Help Center
description: For lots of question blocks we made it easier to use the most used calculator features, without adding a separate calculator block.
article_title: How to add instant scores and calculations inside question blocks
article_folder: editor-block-calculator
article_id: action-blocks-calculator-detail
article_video: logic-scores
author: jurgen
time: 4
time_video: 2
category_id: logic
subcategory: logic_calculator_actions
areas: [studio, wordpress]
---
<p>For lots of question blocks we made it easier to use the most common calculator features, without adding a separate calculator block. For example to quickly add scores to your quiz questions.</p>

<h2 id="when-to-use">When to use</h2>
<p>The <a href="{{ page.base }}calculator/">calculator block</a> is a separate action block in your form. But sometimes you just want to perform a relatively easy calculation, like adding a score based on the given answer(s) to a multiple choice questions.</p>
<p>Such quick calculations can also be done without the need of a separate calculation block, by using <strong>instant scores</strong> or <strong>instant calculations</strong> inside question blocks. For example:</p>
<ul>
  <li>Instantly score the answer options of a multiple choice question in a quiz;</li>
  <li>Instantly perform an advanced formula inside a number block and use the entered number as input for the formula.</li>
</ul>
<blockquote>
  <h4>📌 Also see: Calculator block</h4>
  <p>In the article you're reading now we describe how to perform calculations directly inside question blocks. This helps you to easily perform calculations, without the need for one or more loose calculator blocks in your form. If you need more advanced calculation actions, please have a look at this article:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">How to use the calculator block</a></li>
  </ul>
</blockquote>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>For all question blocks that have answer options in them, you can instantly score those answer options to get a score out of that block.</p>

<h3 id="score">Enable instant scoring</h3>
<p>By enabling the <code>Score</code> feature inside a question block you can enter the desired scores for the various answer options right away, without adding a separate calculator block.</p>
<p>This question block will now save the selected option(s) by the respondent, but will also save the score outcome based on the selected option(s).</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/instant-score.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Example of an instant scoring of a multiple choice question.</figcaption>
</figure>
<blockquote>
  <h4>📌 Also see: More about scoring</h4>
  <p>We have a separate article that describes more about using scores in the calculator:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-scores-in-your-calculations/">How to use scores in your calculations</a></li>
  </ul>
</blockquote>

<h3 id="calculator">Enable instant calculations</h3>
<p>For blocks that output a number, it is possible to instantly use the full calculator features, without adding a separate calculator block. By enabling the <code>Calculator</code> feature inside a question block you get access to the full calculator.</p>
<p>The calculator then uses the entered value of the pertaining question as the initial value and you can instantly perform the right calculations with that value.</p>
<p>This question block will now save the entered number input by the respondent, but will also save the calculator outcome.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/instant-calculation.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Example of an instant calculation to calculate the yearly salary based on the entered monthly salary.</figcaption>
</figure>
<blockquote>
  <h4>📌 Also see: More about calculations</h4>
  <p>We have a separate article about all capabilities of the calculator block:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">How to use the calculator block</a></li>
  </ul>
</blockquote>

<h3 id="piping">Use outcome</h3>
<p>The outcome of the integrated scoring/calculator in each question block is also directly available. You can use that for all kinds of actions, as also described in the article about <a href="{{ page.base }}help/articles/how-to-use-the-outcomes-of-calculator-blocks/">how to use calculator outcomes</a>:</p>
<ul>
  <li>Save the outcome in dataset;</li>
  <li>Show the outcome to your respondents in your form (piping logic);</li>
  <li>Use the outcome as input for an other calculator block<sup>1</sup>;</li>
  <li>Use the outcome as logic condition to determine the follow-up (branch logic).</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/instant-piping.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Example of showing the outcome of a score in the closing message.</figcaption>
</figure>
<blockquote>
  <h4>🔖 Ad 1: Scores in other calculator block</h4>
  <p>If you have added scores inside question blocks, you can use those scores in other calculators instantly. In that case the value that's entered in the question block is the place where you manage the score values.</p>
  <p>It's also possible to use new scoring values inside a new calculator block. Those can differ from the scores that are entered in the question block itself. Those new score values are only valid within the pertaining calculator block.</p>
</blockquote>

<hr />
<h2>More information</h2>
<p>The calculator block has lots of features, so we have several ways to learn all about it.</p>
<h3>Help center</h3>
<p>Our help articles help you out on all different aspects of the calculator:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/">How to add instant scores and calculations inside question blocks</a><span class="related-current">Current article</span></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">How to use the calculator block</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-operations-in-the-calculator-block/">How to use operations in the calculator block (add, subtract, multiply, divide, equal)</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-given-answers-from-respondents-in-your-calculations/">How to use given answers from respondents in your calculations</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-scores-in-your-calculations/">How to use scores in your calculations</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-comparisons-in-your-calculations/">How to use comparisons in your calculations</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/">How to use functions and constants in your calculations</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-subcalculations-multistep-formulas-in-your-calculations/">How to use subcalculations (multistep formulas) in your calculations</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-text-counters-in-your-calculations/">How to use text counters in your calculations</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-calculations-with-logic-branches/">How to use calculations with logic branches</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-outcomes-of-calculator-blocks/">How to use the outcomes of calculator blocks</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-compare-the-outcomes-of-calculator-blocks/">How to compare the outcomes of calculator blocks</a></li>
</ul>
<h3>Overviews</h3>
<p>We also made some overviews of the capabilities that the calculator block provides:</p>
<div>
  <a href="{{ page.base }}calculator/" class="blocklink">
    <div>
      <span class="title">No-code calculations with the calculator block</span>
      <span class="description">Make quizzes, order forms, exams and more with no-code calculations. All without any coding in Tripetto's calculator block.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" loading="lazy" />
    </div>
  </a>
</div>
<div>
  <a href="{{ page.base }}calculator-features/" class="blocklink">
    <div>
      <span class="title">All calculator features</span>
      <span class="description">A complete overview of all features the calculator block has to offer, including operations, scores, comparisons, functions and constants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" loading="lazy" />
    </div>
  </a>
</div>
