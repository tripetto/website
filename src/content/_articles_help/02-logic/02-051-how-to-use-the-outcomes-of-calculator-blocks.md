---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-outcomes-of-calculator-blocks/
title: Use the outcomes of calculator blocks - Tripetto Help Center
description: The outcome of each calculator block can be used in several ways inside your form and your dataset.
article_title: How to use the outcomes of calculator blocks
article_folder: editor-block-calculator
article_id: action-blocks-calculator-detail
author: jurgen
time: 3
category_id: logic
subcategory: logic_calculator_actions
areas: [studio, wordpress]
---
<p>The outcome of each calculator block can be used in several ways inside your form and your dataset.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use <a href="{{ page.base }}calculator/">the calculator block</a> when you want to perform calculations inside your form. You can use the outcomes of calculator blocks in several ways, for example:</p>
<ul>
  <li>Show the outcome of a calculator block to your respondent;</li>
  <li>Use the outcome of a calculator block as a branch condition;</li>
  <li>Compare the outcomes of multiple calculator blocks in a branch condition;</li>
  <li>Use the outcome of a calculator block as the input for another calculator block;</li>
  <li>Use the outcome of a calculator block in your dataset, like results, downloads, notifications and webhooks.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/demo-quiz.gif" width="1200" height="760" alt="Screenshot of a quiz in Tripetto" loading="lazy" />
  <figcaption>Demonstration of showing the quiz score and a different text based on the score.</figcaption>
</figure>
<blockquote>
  <h4>📌 Also see: Calculator block</h4>
  <p>In the article you're reading now we describe how to use the outcomes of calculator blocks. For global instructions about the calculator block, please have a look at this article:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">How to use the calculator block</a></li>
  </ul>
</blockquote>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>In general you can do the following with calculator outcomes:</p>
<ul>
  <li>Save outcome in dataset;</li>
  <li>Show outcome in form (piping logic);</li>
  <li>Use outcome as input for other calculator block;</li>
  <li>Use outcome as value in other input block;</li>
  <li>Use outcome as logic condition (branch logic).</li>
</ul>
<p>We will explain each of these possibilities below.</p>

<h3 id="dataset" data-anchor="In dataset">Save outcome in dataset</h3>
<p>By default the outcome of the calculator block will be saved to your dataset. That way the outcome will be available in your results, exports, notifications and webhooks.</p>

<h3 id="piping" data-anchor="In piping logic">Show outcome in form (piping logic)</h3>
<p>You can show the outcome of a calculator block to your respondent in the form in realtime. All calculator blocks are available for piping logic, by typing the <code>@</code> sign at the position you want to show the outcome.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/">How to show respondents' answers using piping logic</a></li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/piping.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Show the outcome of a quiz calculator.</figcaption>
</figure>

<h3 id="calculator-input" data-anchor="As calculator input">Use outcome as input for other calculator block</h3>
<p>The outcome of each calculator block is also available in any calculator block that comes after. So you can use the outcome of the first calculator block as the input to a second calculator block.</p>

<h3 id="block-value" data-anchor="As block value">Use outcome as value in other input block</h3>
<p>The outcome of each calculator block can also be used to set the value of other input blocks in your form, for example to update the value of a number input block.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-set-value-block/">How to use the set value block</a></li>
</ul>

<h3 id="branch" data-anchor="In branch logic">Use outcome as logic condition (branch logic)</h3>
<p>You can use the outcome of a calculator block to check if a certain branch applies to your filter for that branch. The following filters are available for this:</p>
<ul>
  <li>When calculation is equal to <code>your filter</code>;</li>
  <li>When calculation is not equal to <code>your filter</code>;</li>
  <li>When calculation is lower than<code>your filter</code>;</li>
  <li>When calculation is higher than <code>your filter</code>;</li>
  <li>When calculation is between <code>your filters</code>;</li>
  <li>When calculation is not between <code>your filters</code>;</li>
  <li>When calculation is valid;</li>
  <li>When calculation is not valid.</li>
</ul>
<h4>Filters</h4>
<p>When we mention <code>your filter(s)</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Number - Compare with a fixed number that you enter;</li>
  <li>Value - Compare with another block value supplied in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers/">more info</a>).</li>
</ul>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/">Discover the power of branches for your logic</a></li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/branch.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Use the outcome of a quiz calculator as branch condition.</figcaption>
</figure>
<p>With some smart usage of branch logic you can also compare the outcomes of multiple calculator blocks with each other, for example to determine the highest scoring calculator block.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-compare-the-outcomes-of-calculator-blocks/">How to compare the outcomes of calculator blocks</a></li>
</ul>

<hr />
<h2>More information</h2>
<p>The calculator block has lots of features, so we have several ways to learn all about it.</p>
<h3>Help center</h3>
<p>Our help articles help you out on all different aspects of the calculator:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/">How to add instant scores and calculations inside question blocks</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">How to use the calculator block</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-operations-in-the-calculator-block/">How to use operations in the calculator block (add, subtract, multiply, divide, equal)</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-given-answers-from-respondents-in-your-calculations/">How to use given answers from respondents in your calculations</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-scores-in-your-calculations/">How to use scores in your calculations</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-comparisons-in-your-calculations/">How to use comparisons in your calculations</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/">How to use functions and constants in your calculations</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-subcalculations-multistep-formulas-in-your-calculations/">How to use subcalculations (multistep formulas) in your calculations</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-text-counters-in-your-calculations/">How to use text counters in your calculations</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-calculations-with-logic-branches/">How to use calculations with logic branches</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-outcomes-of-calculator-blocks/">How to use the outcomes of calculator blocks</a><span class="related-current">Current article</span></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-compare-the-outcomes-of-calculator-blocks/">How to compare the outcomes of calculator blocks</a></li>
</ul>
<h3>Overviews</h3>
<p>We also made some overviews of the capabilities that the calculator block provides:</p>
<div>
  <a href="{{ page.base }}calculator/" class="blocklink">
    <div>
      <span class="title">No-code calculations with the calculator block</span>
      <span class="description">Make quizzes, order forms, exams and more with no-code calculations. All without any coding in Tripetto's calculator block.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" loading="lazy" />
    </div>
  </a>
</div>
<div>
  <a href="{{ page.base }}calculator-features/" class="blocklink">
    <div>
      <span class="title">All calculator features</span>
      <span class="description">A complete overview of all features the calculator block has to offer, including operations, scores, comparisons, functions and constants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" loading="lazy" />
    </div>
  </a>
</div>
