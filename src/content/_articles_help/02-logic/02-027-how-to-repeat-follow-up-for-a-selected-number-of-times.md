---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-repeat-follow-up-for-a-selected-number-of-times/
title: Repeat follow-up for a selected number of times - Tripetto Help Center
description: Learn how to repeat follow-up questions for the number of times that your respondent has selected.
article_title: How to repeat follow-up for a selected number of times
article_id: logic-branch-repeat-number
article_folder: editor-branch-repeat-number
article_video: logic-branch-behaviors
author: jurgen
time: 4
time_video: 5
category_id: logic
subcategory: logic_branch_behavior
areas: [studio, wordpress]
---
<p>Learn how to repeat follow-up questions for the number of times that your respondent has selected.</p>

<h2 id="when-to-use">When to use</h2>
<p>In some situations you want to repeat the same question(s) a couple of times. In Tripetto it is possible to automatically iterate through a set of follow-up questions for a number of times that your respondent has entered.</p>
<p>A simple example is when you want to ask for the names and dates of birth of someones children. You can ask how many children your respondent has and then ask the follow-up questions (name and date of birth) for the amount of children the respondent has.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/demo.gif" width="1200" height="760" alt="Screenshot of a form in Tripetto" loading="lazy" />
  <figcaption>Demonstration of iterating questions for the selected number of times.</figcaption>
</figure>
<blockquote>
  <h4>📌 Also see: More logic possibilities</h4>
  <p>The logic described in this article is just one example of what Tripetto can do to make your forms smart. Have a look at this article to see all logic capabilities:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/">Discover the power of branches for your logic</a></li>
  </ul>
</blockquote>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>The idea is to first let the respondent enter the amount and then repeat the follow-up question(s) for the entered number of times.</p>
<h3 id="number-block">Add number block</h3>
<p>We start with adding a <a href="{{ page.base }}help/articles/how-to-use-the-number-block/">number block</a> and entering the question we want to ask.</p>
<p>We advise to enable the <code>Limits</code> feature in the number block and set the maximum number of times you want to repeat the branch. This is important to remember when we create the branch conditions.</p>
<p>And in our example we have set an alias <code>KIDS</code> to the number block.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/number-block.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Settings of the number block. In this example we have a maximum of 4.</figcaption>
</figure>
<blockquote>
  <h4>💡 Tip: Use other question types</h4>
  <p>Instead of the number question type you can also use other question types to let your respondents enter the amount, for example a <a href="{{ page.base }}help/articles/how-to-use-the-dropdown-block/">dropdown</a>, <a href="{{ page.base }}help/articles/how-to-use-the-multiple-choice-block/">multiple choice buttons</a> or <a href="{{ page.base }}help/articles/how-to-use-the-radio-buttons-block/">radio buttons</a>.</p>
  <p>We will explain how to set that up at the end of this article (<a href="#question-types" class="anchor">click here</a>).</p>
</blockquote>

<h3 id="branch">Add branch</h3>
<p>Now, instead of creating the same follow-up questions multiple times, we will create a branch that we will be repeating. To do so, we add an empty branch by clicking the <code><i class="fas fa-plus"></i></code> diamond at the right side of the section.</p>
<p>In the newly created branch, click the branch bubble at the top of it and then select the <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-behavior-for-your-logic/">branch behavior</a> <code>For each condition match (iteration)</code>. This will make your branch repeatable for each condition that gets matched.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/branch-behavior.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Add an empty branch and set the branch behavior to <code>For each condition match (iteration)</code>.</figcaption>
</figure>
<p>Next step is to add the <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">branch conditions</a>. To do so, we click the <code><i class="fas fa-plus"></i></code> button at the bottom of the branch<i class="fas fa-arrow-right"></i>Select our <code>KIDS</code> question<i class="fas fa-arrow-right"></i>Select the condition <code>Number is higher than</code>.</p>
<p>In our example we add the following branch conditions (corresponding the maximum amount we entered in the number block):</p>
<ul>
  <li><code>KIDS</code> is higher than <code>0</code>;</li>
  <li><code>KIDS</code> is higher than <code>1</code>;</li>
  <li><code>KIDS</code> is higher than <code>2</code>;</li>
  <li><code>KIDS</code> is higher than <code>3</code>.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/branch-conditions.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Add the <code>Higher than</code> branch conditions.</figcaption>
</figure>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/branch.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>The branch setup with branch conditions.</figcaption>
</figure>

<h3 id="follow-up">Add follow-up</h3>
<p>Now you can add the desired follow-up questions underneath the branch. You can add as many blocks as you'd like and they all will be repeated for the selected number of times.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/result.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>The iterating branch with the follow-up.</figcaption>
</figure>
<h3 id="branch-number">Add branch number</h3>
<p>Inside the iterating branch you might want to add an ascending counter to show the branch iteration number that your respondent is in. To do that you can add a <a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/">calculator block</a> and use the constant <code>Branch number</code>.
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/calculator.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>The calculator block inside the branch with a <code>Branch number</code> constant.</figcaption>
</figure>
The calculator block now holds the number of the iteration that the respondent is in. By using <a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/">piping logic</a> you can then show that branch number in a question that you show in your iteration.</p>
<blockquote>
  <h4>📌 Also see: Piping logic</h4>
  <p>Please have a look at this article for more information about recalling values with piping logic:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/">How to show respondents' answers using piping logic</a></li>
  </ul>
</blockquote>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/branch-number.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Show the branch number in a question in your iteration.</figcaption>
</figure>
<hr/>

<h2 id="question-types" data-anchor="Other question types">Use other question types</h2>
<p>In the article above we used the question type 'Number' to let the respondents enter the amount, but you can also use other question types to let your respondents enter that amount, for example a <a href="{{ page.base }}help/articles/how-to-use-the-dropdown-block/">dropdown</a>, <a href="{{ page.base }}help/articles/how-to-use-the-multiple-choice-block/">multiple choice buttons</a> or <a href="{{ page.base }}help/articles/how-to-use-the-radio-buttons-block/">radio buttons</a>.</p>
<h3>Add choices and scores</h3>
<p>To do so, add one of the mentioned questions types to your form and add the desired amounts as choices in that question. Next, enable the <code>Score</code> feature for this question block. Now you can add a score for each choice, corresponding the amount that each choice represents.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/dropdown-block.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>The choices and scores of a dropdown question.</figcaption>
</figure>
<blockquote>
  <h4>📌 Also see: Scores</h4>
  <p>Please have a look at this article for more information about adding scores to your question blocks:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/">How to add instant scores and calculations inside question blocks</a></li>
  </ul>
</blockquote>
<h3>Use scores in branches</h3>
<p>Now you can use those scores to setup the branch conditions that we described in the above article.</p>
<hr/>

<h2>More video tutorials</h2>
<p>You can have a look at <a href="{{ page.base }}help/articles/">all our help articles</a> for more in-depth (video) tutorials to get the most out of Tripetto. Or have a look at our <a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank" rel="noopener noreferrer">YouTube channel</a> for all videos.</p>
<div>
  <a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank" rel="noopener noreferrer" class="blocklink">
    <div>
      <span class="title">Tripetto YouTube channel<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Visit our YouTube channel with tutorial videos.</span>
      <span class="url">youtube.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-youtube.svg" width="160" height="160" alt="YouTube logo" loading="lazy" />
    </div>
  </a>
</div>
