---
layout: help-article
base: ../../../
permalink: /help/articles/discover-the-power-of-branches-for-your-logic/
title: Discover the power of branches for your logic - Tripetto Help Center
description: The way Tripetto lets you create branches for your logic is so flexible the possibilities are limitless. Let us show you how to get the most out of it.
article_title: Discover the power of branches for your logic
article_id: logic-branch
article_video: logic-branch-logic
author: jurgen
time: 4
time_video: 2
category_id: logic
subcategory: logic_branch
areas: [studio, wordpress]
---
<p>The way Tripetto lets you create branches for your logic is so flexible the possibilities are limitless. Let us show you how to get the most out of it.</p>

<h2 id="builder" data-anchor="Visual form builder">Logic in our visual form builder</h2>
<p>Tripetto is built up from the base to support advanced logic options to make your forms really smart. To easily let you create (and maintain) that logic, you create your forms in <a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/">our visual form builder</a> that represents your form's flow on a storyboard.</p>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/editor-branch/00-add.gif" width="891" height="833" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Add a new branch.</figcaption>
</figure>

<h2 id="branches">Branches</h2>
<p>To add the magic of logic to it, you create <strong>branches</strong>, that are shown as a new horizontal track in the form builder. Inside each branch you can add unlimited sections, question blocks, action blocks or even more branches. There are <a href="{{ page.base }}help/articles/how-to-add-branches-in-the-form-builder/">several ways to create a branch</a>, but for this article we create an empty branch by clicking the <code><i class="fas fa-plus"></i></code> icon on the right side of a section.</p>

<h3 id="branch-conditions">Branch conditions</h3>
<p>To determine when a certain branch should be shown to your respondents you have to add the desired <strong>branch condition(s)</strong> to each branch. To do so, you click the <code><i class="fas fa-plus"></i></code> button at the bottom of the branch block. Now you can use several branch conditions:</p>
<ul>
  <li><strong>Block conditions</strong> - Basic conditions based on the value of a certain question block and/or action block;</li>
  <li><strong>Evaluate conditions</strong> - Advanced conditions based on the value of a certain question block and/or action block;</li>
  <li><strong>Regular expression conditions</strong> - Conditions based on a regular expression;</li>
  <li><strong>Device conditions</strong> - Conditions based on the device of the respondent.</li>
</ul>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">Learn about different types of branch conditions for your logic</a></li>
</ul>

<h3 id="branch-behavior">Branch behavior</h3>
<p>To determine how a branch should behave when one or more conditions match, you can select the right <strong>branch behavior</strong>. To do so, you click the green bubble at the top of a branch. Now you can choose from different branch behaviors:</p>
<ul>
  <li><strong>For the first condition match</strong> - Follow the branch if <i>at least one</i> of the conditions matches;</li>
  <li><strong>When all conditions match</strong> - Follow the branch if <i>all</i> the conditions match;</li>
  <li><strong>When no conditions match</strong> - Follow the branch if <i>none</i> of the conditions match;</li>
  <li><strong>For each condition match (iteration)</strong> - Follow the branch for <i>each</i> condition match. This will create an iteration of the branch.</li>
</ul>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-behavior-for-your-logic/">Learn about different types of branch behaviors for your logic</a></li>
</ul>

<h3 id="branch-ending">Branch ending</h3>
<p>To determine what should happen at the end of a branch, you can set the desired <strong>branch ending</strong>. To do so, you click the green bubble at the bottom of a branch. Now you can choose from different branch endings:</p>
<ul>
  <li><strong>Continue with the next section or branch</strong> - Let the form determine the continuation;</li>
  <li><strong>Jump to a specific point</strong> - Let the form jump to a specific section in your form;</li>
  <li><strong>Jump to end</strong> - Let the form jump to the very end immediately;</li>
  <li><strong>End with closing message</strong> - Let the form end and show a custom closing message.</li>
</ul>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-endings-for-your-logic/">Learn about different types of branch endings for your logic</a></li>
</ul>

<h3 id="branch-properties">Branch properties</h3>
<p>Instead of using the bubbles at the beginning (to set branch behavior) and ending (to set branch ending) of a branch, you can also click on the name of the branch to open up the branch properties. In the pane that opens you can set the branch name, branch behavior and branch ending.</p>
<figure>
  <img src="{{ page.base }}images/help/editor-branch/01-properties.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Open up the branch properties.</figcaption>
</figure>
<hr />

<h2>More about branch logic</h2>
<p>Branch logic is very powerful, so we made some different help articles to help you with this:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/">Discover the power of branches for your logic</a><span class="related-current">Current article</span></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-add-branches-in-the-form-builder/">How to add branches in the form builder</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">Learn about different types of branch conditions for your logic</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-behavior-for-your-logic/">Learn about different types of branch behavior for your logic</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-endings-for-your-logic/">Learn about different types of branch endings for your logic</a></li>
</ul>
