---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-hidden-field-block/
title: Use the hidden field block - Tripetto Help Center
description: Use hidden fields to gather information from outside the form into your form's dataset.
article_title: How to use the hidden field block
article_id: action-blocks
article_folder: editor-block-hidden-field
author: mark
time: 5
category_id: logic
subcategory: logic_data_actions
areas: [studio, wordpress]
---
<p>Hidden fields are like hidden gems. Use them to make your forms and surveys more personal. Or keep track of your respondents or other information you pass to the form.</p>

<h2 id="when-to-use">When to use</h2>
<p>The hidden field block enables you to gather information from outside the form into your form's dataset. That information is then saved with the rest of the form data when the form is submitted. Some examples:</p>
<ul>
  <li>Get the name of a respondent from a query string parameter and show it in your form;</li>
  <li>Save the URL of the page that your form is embedded into;</li>
  <li>Make a logic branch in your form based on the device size of the respondent.</li>
</ul>
<blockquote class="help_article_wp_only">
  <h4>📌 Also see: WordPress variables (WordPress only)</h4>
  <p>In the Tripetto WordPress plugin the hidden field block offers an <a href="{{ page.base }}help/articles/how-to-use-wordpress-variables-in-your-form/">extended list of WordPress variables</a>. You can use those to instantly collect WordPress specific data, for example:</p>
  <ul>
    <li>User information of the logged in WordPress user, like the name and email address;</li>
    <li>Website data, like the URL and language;</li>
    <li>Visitor data, like the IP address and the referrer URL;</li>
    <li>Server data, like the WordPress version and PHP version.</li>
  </ul>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-wordpress-variables-in-your-form/">How to use WordPress variables in your form</a></li>
  </ul>
</blockquote>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>The hidden field block is available as a question type. Add a new block to your form and then select the question type <code>Hidden field</code>. You can add it in any given position and in an unlimited amount inside your forms.</p>

<h3 id="features-general">Name</h3>
<p>You can give your hidden field block a <code>Name</code> so you can recognize it in your form's structure and dataset. This name is not visible for your respondents.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/menu.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Hidden fields are available from the Question type menu.</figcaption>
</figure>

<h3 id="features-type">Type of field</h3>
<p>The <code>Type of field</code> determines the data that you want to store inside the hidden field block. The following field types are available:</p>
<ul>
  <li><strong>Basic fields</strong>
    <ul>
      <li><code>Fixed value</code> - Defines a fixed value for the hidden field;</li>
      <li><code>Timestamp</code> - Retrieves the current date and time;</li>
      <li><code>Random value</code> - Retrieves a random string value;</li>
      <li><code>Global JavaScript variable</code> - Retrieves a JavaScript global variable (only string values are supported).</li>
    </ul>
  </li>
  <li><strong>Browser information</strong>
    <ul>
      <li><code>User language</code> - Retrieves the preferred language of the user;</li>
      <li><code>Query string</code> - Retrieves the complete query string or a specific query string parameter value;</li>
      <li><code>Cookie</code> - Read one or all cookies associated with the document;</li>
      <li><code>Local storage</code> - Retrieves an item from the browser local storage (localStorage);</li>
      <li><code>Session storage</code> - Retrieves an item from the browser session storage (sessionStorage);</li>
      <li><code>User-Agent string</code> - Retrieves the browser User-Agent string.</li>
    </ul>
  </li>
  <li><strong>Page information</strong>
    <ul>
      <li><code>Page title</code> - Retrieves the title of the page;</li>
      <li><code>Page URL</code> - Retrieves the URL of the page;</li>
      <li><code>Page referrer</code> - Retrieves the URL of the page that linked to this page.</li>
    </ul>
  </li>
  <li><strong>Screen information</strong>
    <ul>
      <li><code>Screen orientation</code> - Retrieves the current orientation of the screen (landscape or portrait);</li>
      <li><code>Screen width</code> - Retrieves the width of the screen in pixels;</li>
      <li><code>Screen height</code> - Retrieves the height of the screen in pixels;</li>
      <li><code>Screen pixel ratio</code> - Retrieves the ratio of the resolution in physical pixels to the resolution in CSS pixels for the current display device.</li>
    </ul>
  </li>
  <li class="help_article_wp_only"><strong>WordPress variables</strong><p>In the WordPress plugin you can also <a href="{{ page.base }}help/articles/how-to-use-wordpress-variables-in-your-form/">instantly use WordPress variables</a>.</p></li>
</ul>

<h3 id="features-settings">Settings</h3>
<p>Depending on the selected type of field, additional settings can become activated to help you get the right value in your hidden field. These features will popup on the left side of the pane. You can enable and configure these to your own needs.</p>

<h3 id="features-options">Options</h3>
<ul>
  <li><strong>Required</strong><br/>By default this block is not required to fill out by your respondents. Enable the <code>Required</code> feature to make this block required to fill out.</li>
  <li><strong>Status</strong><br/>By default this block will be executed in your form. Enable the <code>Status</code> feature to disable the execution.</li>
  <li><strong>Alias</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about alias"><i class="fas fa-info-circle"></i></a><br/>By default the name/title of this block will be used in your dataset. Enable the <code>Alias</code> feature to use a different name in your dataset.</li>
  <li><strong>Exportability</strong><a href="{{ page.base }}help/articles/how-to-determine-what-data-fields-get-saved/" class="article-more-info" title="More information about exportability"><i class="fas fa-info-circle"></i></a><br/>By default the collected data of this block will be stored to the dataset of each result. Enable the <code>Exportability</code> feature to take control over the storage. You can then include/exclude the data from your dataset. Please note that if you exclude data from the dataset you can never see and/or use the given answers from your respondents to this block, because the data is simply not stored at all.</li>
</ul>

<hr/>

<h2 id="logic">Logic<a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" class="article-more-info" title="More information about logic"><i class="fas fa-info-circle"></i></a></h2>
<p>After you retrieved a hidden field in your form, you can instantly use that value to execute logic based on the value of the hidden field.</p>

<h3 id="piping-logic">Piping logic</h3>
<p>You can show the value of the hidden field to your respondents, by using our piping logic. This works the same way as you can use 'normal' question blocks as piping values, like you can see in <a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/">this article about piping logic</a>.</p>

<h3 id="branch-logic">Branch logic</h3>
<p>You can also use the value of the hidden field as branch conditions, so the form can take decisions based on the hidden field. This works the same way as you can add logic based on 'normal' question blocks, like you can see in <a href="{{ page.base }}help/articles/how-to-set-a-follow-up-based-on-given-answers/">this article about branch logic</a>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-logic.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Use a hidden field value to execute logic.</figcaption>
</figure>

<p>Due to the flexibility of the hidden field block, the value that it generates can contain several types. It depends on the type of value that is saved inside the hidden field, which <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">branch conditions</a> you can create with it.</p>
<p>Please see the following articles for the possible branch conditions:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-text-single-line-block/#logic">Branch conditions for <strong>textual values</strong></a>;</li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-number-block/#logic">Branch conditions for <strong>numeric values</strong></a>;</li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-date-and-time-block/#logic">Branch conditions for <strong>date (and time) values</strong></a>.</li>
</ul>
<hr />

<h2 id="calculations">Calculations<a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" class="article-more-info" title="More information about calculations"><i class="fas fa-info-circle"></i></a></h2>
<p>You can use the <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">calculator block</a> to perform calculations with hidden fields. Due to the flexibility of the hidden field block, the value of it can contain several types. It depends on the type of value that is saved inside the hidden field, which calculation operations are available in the calculator block.</p>
<hr />

<h2 id="examples">Examples</h2>
<p>Let us show you some examples of how to use the hidden field.</p>

<h3 id="example-page-url">Retrieve page URL</h3>
<p>Let's say you have embedded a Tripetto form into various pages inside your website and you want to know from which page someone has submitted a form. The hidden field can simply gather the page URL of the page that the form is embedded into and save that data to the entry data.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-page-url.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Example of a hidden field getting the page URL.</figcaption>
</figure>

<h3 id="example-querystring">Retrieve query string values</h3>
<p>The URL of a form/page can also contain certain values. Those are often stored in the <strong>query string</strong> of the URL (<a href="https://en.wikipedia.org/wiki/Query_string" target="_blank">what's a query string?</a>). The query string is at the end of the URL and can contain fully flexible <strong>name-value pairs</strong>.</p>
<blockquote>
  <h4>📣 Info: About query strings</h4>
  <p>A query string is an often used method to send data in a URL. Some background information on how to supply a query string:</p>
  <ul>
    <li>You can add multiple parameters to a query string;</li>
    <li>Each parameter consists of a name and a value, separated by a <code> = </code> sign, for example <code>name=value</code>;</li>
    <li>The first parameter is preceded by a <code> ? </code> sign;</li>
    <li>Any following parameters are preceded by a <code> & </code> sign;</li>
  </ul>
  <p>A full example could be something like this: <code>https://yoursite.com/?name=abc&city=klm&country=xyz</code>.</p>
</blockquote>
<p>Let's say we have a form that we share via different channels, for example a mailing and Twitter. And you want to know how someone entered the form, so you can analyse which channel worked best. By adding a query string named <code>referral</code> to your URL's, you can add various values for that query string parameter that can be saved in a hidden field in the form.</p>
<p>So in the URL you share via a mailing the query string in the URL is something like <code>?referral=mailing</code>. And for the URL you share on Twitter something like <code>?referral=twitter</code>. Notice that all parameter names and values we use are just examples; those are totally flexible for your own needs.</p>
<p>Now, by adding a hidden field that reads the query string parameter <code>referral</code>, you can save where someone came from. Each entry will now contain where someone came from: the mailing, or Twitter.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-querystring.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Example of a hidden field getting the query string parameter <code>referral</code>.</figcaption>
</figure>

<h3 id="example-utm">Retrieve UTM parameters</h3>
<p>Another example of query string usage are <a href="https://en.wikipedia.org/wiki/UTM_parameters" target="_blank" rel="noopener noreferrer">UTM parameters</a>. UTM parameters are a standard practice to measure the source of your visitors/respondents.</p>
<p>You can retrieve UTM parameters just like the query string, as described above. For more information about using UTM parameters, please have a look at this article:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-track-utm-parameters-in-your-form/">How to track UTM parameters in your form</a></li>
</ul>
<hr/>

<h2>Also read our blog</h2>
<p>We also wrote a blog post with some more background information, use cases and working examples of hidden fields.</p>
<div>
  <a href="{{ page.base }}blog/hidden-fields-in-tripetto/" class="blocklink">
    <div>
      <span class="title">Hidden fields in Tripetto - Tripetto Blog</span>
      <span class="description">Hidden fields are like hidden gems. Use them to make your forms and surveys more personal. Or keep track of your respondents or other information you pass to the form.</span>
      <span class="url">tripetto.com/blog</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" loading="lazy" />
    </div>
  </a>
</div>
<hr />

{% include help-article-blocks.html %}
