---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-differentiate-your-form-based-on-device/
title: Differentiate your form based on device - Tripetto Help Center
description: Learn how you can use the device type of respondents (mobile/tablet/desktop) to differentiate your form.
article_title: How to differentiate your form based on device
article_folder: editor-branch-conditions-device
author: jurgen
time: 2
category_id: logic
subcategory: logic_branch_conditions
areas: [studio, wordpress]
---
<p>Learn how you can use the device type of respondents (mobile/tablet/desktop) to differentiate your form.</p>

<h2 id="when-to-use">When to use</h2>
<p>With the built in branch conditions for devices, you can create branches that only will be executed if the respondent uses the corresponding device (mobile/tablet/desktop). For example in case you only want to ask certain questions to respondents that fill out your form on a mobile or tablet device, but not to respondents that are on a desktop device.</p>
<p>The different devices are determined by the screen size. The following screen sizes are used for each device type:</p>
<ul>
  <li><strong>Phone</strong> - Screen width under 400 pixels;</li>
  <li><strong>Tablet</strong> - Screen width between 400 pixels and 800 pixels;</li>
  <li><strong>Desktop</strong> - Screen width above 800 pixels.</li>
</ul>
<blockquote>
  <h4>📌 Also see: More logic possibilities</h4>
  <p>The logic described in this article is just one example of what Tripetto can do to make your forms smart. Have a look at this article to see all logic capabilities:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/">Discover the power of branches for your logic</a></li>
  </ul>
</blockquote>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>As a demonstration scenario we want to show a certain question only to respondents that are on a mobile device.</p>
<h3 id="branch">Create branch</h3>
<p>To do so we create a branch and we add a <code>Device</code> condition. This opens the device screen right away. In there you can now select the device that you want to include in this condition, so in this case we select <code>Phone</code>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-branch.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Add a device condition and select the device you want to include.</figcaption>
</figure>
<h3 id="follow-up">Add follow-up</h3>
<p>Now we have setup the branch, we can add the desired follow-up of our form that only will be visible for respondents that use a mobile device (screen width under 400 pixels).</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-follow-up.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>The full setup of a device condition and the follow-up.</figcaption>
</figure>
<p>That's it! The form will now show this question only to respondents on mobile screen sizes.</p>
