---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-wordpress-variables-in-your-form/
title: Use WordPress variables in your form - Tripetto Help Center
description: With the hidden field block you can instantly collect and use WordPress variables, like user information, website data, visitor data and server data.
article_title: How to use WordPress variables in your form
article_folder: wordpress-variables
author: jurgen
time: 5
category_id: logic
subcategory: logic_data_actions
areas: [wordpress]
---
<p>With the hidden field block in Tripetto you can instantly collect and use WordPress variables, like user information, website data, visitor data and server data.</p>

<h2 id="when-to-use">When to use</h2>
<p>There are lots of use cases in which you can use certain variables from outside the form to make your form more smart or more personal. In the WordPress plugin you can collect such data instantly. Some use cases:</p>
<ul>
  <li>Get the email address of the logged in WordPress user, so the user does not have to enter the email address;</li>
  <li>Collect the IP addresses of your respondents to filter duplicate entries from your results data afterwards;</li>
  <li>Gather the referrer URL to see where the respondent came from before entering your form.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-demo.png" width="1200" height="760" alt="Screenshot of variables in a form in Tripetto" loading="lazy" />
  <figcaption>Demonstration of WordPress variables in a Tripetto form.</figcaption>
</figure>
<blockquote>
  <h4>📌 Also see: Hidden field block</h4>
  <p>In the article you're reading now we describe how to use WordPress variables in your form. We use the hidden field block for that. For global instructions about the hidden field block, please have a look at this article:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-hidden-field-block/">How to use the hidden field block</a></li>
  </ul>
</blockquote>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>WordPress variables are part of the hidden field block.</p>

<h3 id="add-block">Add hidden field block</h3>
<p>First add a <a href="{{ page.base }}help/articles/how-to-use-the-hidden-field-block/">hidden field block</a> and open it in the builder. You can add it in any given position and in an unlimited amount inside your forms.</p>
<p>You can give your hidden field block a <code>Name</code> so you can recognize it in your form's structure and dataset. This name is not visible for your respondents.</p>

<h3 id="select">Select WordPress variable</h3>
<p>In the hidden field block you'll see a dropdown menu <code>Type of field</code>. That determines the data you want to collect with this hidden field block. Part of the options are WordPress specific variables. The following WordPress variables are available:</p>
<ul>
  <li><strong>User information</strong>
    <ul>
    <li><code>Username</code> - Retrieves the value of <i>User / Username</i>;</li>
    <li><code>Nickname</code> - Retrieves the value of <i>User / Nickname</i>;</li>
    <li><code>Display name</code> - Retrieves the value of <i>User / Display name</i>;</li>
    <li><code>First name</code> - Retrieves the value of <i>User / First name</i>;</li>
    <li><code>Last name</code> - Retrieves the value of <i>User / Last name</i>;</li>
    <li><code>Email address</code> - Retrieves the value of <i>User / Email address</i>;</li>
    <li><code>Website</code> - Retrieves the value of <i>User / Website</i>;</li>
    <li><code>Biographical info</code> - Retrieves the value of <i>User / Biographical info</i>;</li>
    <li><code>Avatar (URL)</code> - Retrieves the value of <i>User / Avatar (URL)</i>;</li>
    <li><code>Language</code> - Retrieves the value of <i>User / Language</i>;</li>
    <li><code>ID</code> - Retrieves the value of <i>User / ID</i>.</li>
    </ul>
  </li>
  <li><strong>Website information</strong>
    <ul>
      <li><code>URL</code> - Retrieves the value of <i>Website / URL</i>;</li>
      <li><code>Title</code> - Retrieves the value of <i>Website / Title</i>;</li>
      <li><code>Tagline</code> - Retrieves the value of <i>Website / Tagline</i>;</li>
      <li><code>Administration email address</code> - Retrieves the value of <i>Website / Administration email address</i>;</li>
      <li><code>Language</code> - Retrieves the value of <i>Website / Language</i>.</li>
    </ul>
  </li>
  <li><strong>Visitor information</strong>
    <ul>
      <li><code>IP address</code> - Retrieves the value of <i>Visitor / IP address</i>;</li>
      <li><code>Language</code> - Retrieves the value of <i>Visitor / Language</i>;</li>
      <li><code>Referrer URL</code> - Retrieves the value of <i>Visitor / Referrer URL</i>.</li>
    </ul>
  </li>
  <li><strong>Server information</strong>
    <ul>
      <li><code>URL</code> - Retrieves the value of <i>Server / URL</i>;</li>
      <li><code>Protocol</code> - Retrieves the value of <i>Server / Protocol</i>;</li>
      <li><code>Port</code> - Retrieves the value of <i>Server / Port</i>;</li>
      <li><code>Host name</code> - Retrieves the value of <i>Server / Host name</i>;</li>
      <li><code>Path</code> - Retrieves the value of <i>Server / Path</i>;</li>
      <li><code>Querystring</code> - Retrieves the value of <i>Server / Querystring</i>;</li>
      <li><code>IP address</code> - Retrieves the value of <i>Server / IP address</i>;</li>
      <li><code>Software</code> - Retrieves the value of <i>Server / Software</i>;</li>
      <li><code>WordPress version</code> - Retrieves the value of <i>Server / WordPress version</i>;</li>
      <li><code>PHP version</code> - Retrieves the value of <i>Server / PHP version</i>.</li>
    </ul>
  </li>
</ul>
<p>Just select the WordPress variable you want to collect and that's it! The hidden field block will now collect and store the selected WordPress variable in your form.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-hidden-field.png" width="1200" height="760" alt="Screenshot of a hidden field block with WordPress variable in Tripetto" loading="lazy" />
  <figcaption>A form setup with several hidden field blocks that collect WordPress variables.</figcaption>
</figure>

<h3 id="legislation" data-anchor="Data privacy legislation">Important! Data privacy legislation</h3>
<p>Please be aware you comply with any data privacy legislation that affects your data gathering. With the WordPress variables in the hidden field block, it's possible to collect personal information that may be subject to data privacy legislation. Tripetto does not collect any personal information on its own, so it's your own responsibility which personal information you collect and to comply to data privacy legislation. Data privacy legislation can differ per worldwide location, so please make sure you comply to the legislation that applies to your form.</p>
<h4>Example: collecting IP addresses in accordance with European legislation (GDPR)</h4>
<p>In Europe the General Data Protection Regulation (GDPR) applies to all data you collect. This states that IP addresses should be considered as personal data and thus collecting such data must comply with the corresponding GDPR legislation. It's your own responsibility to decide if you need to collect IP addresses and to comply to the rules set by the GDPR.</p>
<hr />

<h2 id="use-variable">Use WordPress variable</h2>
<p>Now the hidden field block automatically collects the WordPress variable you selected. In general you can now do the following with the value in that hidden field block:</p>
<ul>
  <li>Save variable in dataset;</li>
  <li>Show variable in form (piping logic);</li>
  <li>Use variable as value in other input block;</li>
  <li>Use variable as logic condition (branch logic).</li>
</ul>
<p>We will explain each of these possibilities below.</p>

<h3 id="dataset" data-anchor="In dataset">Save variable in dataset</h3>
<p>By default the value of each hidden field block will be saved to your dataset. That way the variable will be available in your results, exports, notifications and webhooks.</p>

<h3 id="piping" data-anchor="In piping logic">Show variable in form (piping logic)</h3>
<p>You can show the variable to your respondent in the form in realtime. All hidden field blocks are available for piping logic, by typing the <code>@</code> sign at the position you want to show the value.</p>
<p>Have a look at <a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/">this article for more information about piping logic</a>.</p>

<h3 id="block-value" data-anchor="As block value">Use variable as value in other input block</h3>
<p>The variable can also be used to set the value of other input blocks in your form, for example to update the value of an email address input block with the email address of the WordPress user.</p>
<p>Have a look at <a href="{{ page.base }}help/articles/how-to-use-the-set-value-block/">this article for more information about the set value block</a>.</p>

<h3 id="branch" data-anchor="In branch logic">Use variable as logic condition (branch logic)</h3>
<p>You can use the variable to check if a certain branch applies to your filter for that branch.
<p>Have a look at <a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/">this article for more information about branch logic</a>.</p>

<h2>More information</h2>
<p>The WordPress variables are part of the hidden field block: <a href="{{ page.base }}help/articles/how-to-use-the-hidden-field-block/">How to use the hidden field block</a>.</p>
