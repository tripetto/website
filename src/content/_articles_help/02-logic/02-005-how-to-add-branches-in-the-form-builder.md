---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-add-branches-in-the-form-builder/
title: Add branches in the form builder - Tripetto Help Center
description: Let's have a look at the different ways to add such branches to your form structure using Tripetto's form builder.
article_title: How to add branches in the form builder
article_id: logic-branch-add
article_folder: editor-add-branches
author: jurgen
time: 4
category_id: logic
subcategory: logic_branch
areas: [studio, wordpress]
---
<p>Branch logic makes your forms smart and conversational. Let's have a look at the different ways to add such branches to your form structure using Tripetto's form builder.</p>

<h2 id="builder" data-anchor="Visual form builder">Logic in our visual form builder</h2>
<p>Tripetto is built up from the base to support advanced logic options to make your forms really smart. To easily let you create (and maintain) that logic, you create your forms in <a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/">our visual form builder</a> that represents your form's flow on a storyboard.</p>
<p>On the storyboard you use <strong>branches</strong> to set up different paths that your respondents can take in your form, depending on the given answers of each individual respondent. More in depth information about logic branches:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/">Discover the power of branches for your logic</a></li>
</ul>

<h2 id="branches">Add branches</h2>
<p>To add branches to your form structure there are three different ways:</p>
<ul>
  <li>Add a branch from a form block (question/action blocks);</li>
  <li>Add a branch from a section;</li>
  <li>Add a branch in between sections.</li>
</ul>

<h3 id="from-block" data-anchor="From a form block">Add a branch from a form block (question/action blocks)</h3>
<p>In most cases you'll want to add a branch based on the input of your respondents on a certain question, or based on the output of a certain action block. That's why you can easily access all branch options from a form block and instantly add a branch from there on.</p>
<p>You can access the block menu (which contains the branch options) of a form block in a few ways:</p>
<ul>
  <li>Click the <code><i class="fas fa-ellipsis-h"></i></code> icon inside a form block. From this block menu you can now select the desired branch condition for this block that you want to add to your form structure. It depends on the type of question/action block which branch conditions are available. The branch, including your selected branch condition, will now be added to your storyboard instantly.</li>
  <li>You can also right-click a form block to open the same block menu.</li>
</ul>
<p>From there on you can add the follow-up that respondents get to see when they match the branch condition(s).</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/from-question.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Add a branch from a form block, in this case for a scale block.</figcaption>
</figure>

<h3 id="from-section" data-anchor="From a section">Add a branch from a section</h3>
<p>The second way to add a branch to your form structure is from a section. In most cases the end of a section is the place where you'll want to add a branch.</p>
<p>You can add a branch from a section in a few ways:</p>
<ul>
  <li>Click the <code><i class="fas fa-plus"></i></code> icon next to a section to add an empty branch. This only adds an empty branch flow. Please note that you'll have to manually add branch condition(s) to make this branch work correctly;</li>
  <li>Right-click the <code><i class="fas fa-plus"></i></code> icon next to a section to open the branch menu. You can now add an empty branch, but this branch menu also contains all possible branch conditions that are available in your whole form structure. Simply select the desired branch condition and that branch will be added to your storyboard instantly.</li>
  <li>On touch-enabled devices you can also hold the <code><i class="fas fa-plus"></i></code> icon next to a section for a brief moment to open the same branch menu.</li>
</ul>
<p>From there on you can add the follow-up that respondents get to see when they match the branch condition(s).</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/from-section.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Add a branch from a section, in this case for a multiple choice block.</figcaption>
</figure>

<h3 id="in-between-sections" data-anchor="In between sections">Add a branch in between sections</h3>
<p>The third way to add a branch is in between sections. In some occasions you don't need to attach your branch to a section, but the branch can be on its own, in between sections.</p>
<p>You can add a branch in between sections in a few ways:</p>
<ul>
  <li>Right-click the <code><i class="fas fa-plus"></i></code> icon in between sections and select <code>Insert branch</code>. You can now add an empty branch, but this branch menu also contains all possible branch conditions that are available in your whole form structure. Simply select the desired branch condition and that branch will be added to your storyboard instantly. And as you will see this branch is not connected to a section, but it's drawn on its own.</li>
  <li>On touch-enabled devices you can also hold the <code><i class="fas fa-plus"></i></code> icon in between sections for a brief moment to open the same branch menu.</li>
</ul>
<p>From there on you can add the follow-up that respondents get to see when they match the branch condition(s).</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/in-between-sections.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Add a branch in between sections, in this case for a multiple choice block.</figcaption>
</figure>
<hr/>

<h2>More about branch logic</h2>
<p>Branch logic is very powerful, so we made some different help articles to help you with this:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/">Discover the power of branches for your logic</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-add-branches-in-the-form-builder/">How to add branches in the form builder</a><span class="related-current">Current article</span></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">Learn about different types of branch conditions for your logic</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-behavior-for-your-logic/">Learn about different types of branch behavior for your logic</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-endings-for-your-logic/">Learn about different types of branch endings for your logic</a></li>
</ul>
