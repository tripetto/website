---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-verify-given-answers-with-regular-expressions/
title: Verify given answers with regular expressions - Tripetto Help Center
description: Learn how regular expressions help you to check if given answers inside your form comply with your data format standards and use them in branch logic.
article_title: How to verify given answers with regular expressions
article_folder: editor-branch-conditions-regular-expressions
article_video: logic-branch-conditions
author: mark
time: 3
time_video: 3
category_id: logic
subcategory: logic_branch_conditions
areas: [studio, wordpress]
---
<p>Learn how regular expressions help you to check if given answers comply with your data format standards and use them in branch logic.</p>

<h2 id="when-to-use">When to use</h2>
<p>A regular expression is a sequence of characters that specifies a certain pattern in text (<a href="https://en.wikipedia.org/wiki/Regular_expression" target="_blank" rel="noopener noreferrer">more information</a>). In Tripetto you can use such regular expressions in branch conditions to verify if an entered value complies with a certain pattern (or not).</p>
<p>Some examples of regular expression conditions:</p>
<ul>
  <li>If the respondent enters a value that starts with <i>'hi'</i> at the text question <i>'Question 1'</i>, then show this branch (regular expression used for this: <code>/^hi/</code>);</li>
  <li>If the respondent enters a value that's exactly 8 uppercase characters (for example a discount code) at the text question <i>'Question 2'</i>, then show this branch (regular expression used for this: <code>/^[A-Z]{8}$/</code>);</li>
  <li>If the respondent enters a value that's exactly 16 digits (for example a social security number) at the text question <i>'Question 3'</i>, then show this branch (regular expression used for this: <code>/^[0-9]{16}$/</code>).</li>
</ul>
<blockquote>
  <h4>📌 Also see: More logic possibilities</h4>
  <p>The logic described in this article is just one example of what Tripetto can do to make your forms smart. Have a look at this article to see all logic capabilities:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/">Discover the power of branches for your logic</a></li>
  </ul>
</blockquote>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>As a demonstration scenario we want to verify a social security number that has to contain 16 digits. If the respondent did not enter a valid 16 digits social security number, we will show an error message so the respondent can not continue the form without entering a valid social security number.</p>
<p>We already added a <a href="{{ page.base }}help/articles/how-to-use-the-number-block/">number block</a> for the social security number and we now want to validate that input.</p>
<h3 id="branch">Create branch</h3>
<p>To do so we create a branch and we add a <code>Regular expression</code> condition. This opens the regular expression screen right away. In there you can now select the question that you want to validate.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-branch.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Add a regular expression condition and select your question.</figcaption>
</figure>
<p>Now we can enter the regular expression that we want to apply to the entered value. In this case we simply check if there are 16 digits entered, using this regular expression: <code>/^[0-9]{16}$/</code>. And because we actually want to check if the regular expression is <u>not</u> fulfilled (so we can show an error message in that case), we check the <code>Invert regular expression</code> option.</p>
<blockquote>
  <h4>💡 Tip: Generate regular expressions</h4>
  <p>You can use <a href="https://regex101.com/" target="_blank" rel="noopener noreferrer">regex101.com</a> to create and test your regular expressions.</p>
</blockquote>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-condition.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Configure the regular expression condition.</figcaption>
</figure>
<h3 id="follow-up">Add follow-up</h3>
<p>Now we have setup the branch, we can add the desired follow-up of our form that only will be visible for respondents that haven't entered a valid social security number. In this case we add a <a href="{{ page.base }}help/articles/how-to-use-the-raise-error-block/">raise error block</a> to show an error message.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/03-follow-up.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>The full setup of a regular expression condition and the follow-up.</figcaption>
</figure>
<p>That's it! The form will now show an error message, preventing the form to continue, whilst the respondent has not entered a valid social security number.</p>
