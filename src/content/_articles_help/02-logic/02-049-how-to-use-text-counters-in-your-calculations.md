---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-text-counters-in-your-calculations/
title: Use text counters in your calculations - Tripetto Help Center
description: The calculator block is able to calculate with texts, for example to count characters, count words and even count occurrences of a certain text or character.
article_title: How to use text counters in your calculations
article_folder: editor-block-calculator
article_id: action-blocks-calculator-detail
author: mark
time: 4
category_id: logic
subcategory: logic_calculator_actions
areas: [studio, wordpress]
---
<p>The calculator block is able to calculate with texts, for example to count characters, count words and even count occurrences of a certain text or character.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use <a href="{{ page.base }}calculator/">the calculator block</a> when you want to perform calculations inside your form. The calculator block can also work with text values. Sounds odd, but think of these use cases:</p>
<ul>
  <li>Count the number of entered characters and show the amount of entered characters in your form;</li>
  <li>Count the number of entered words and show the amount of entered words in your form;</li>
  <li>Count the number of occurrences of a certain text so you can use that to analyse the entered text.</li>
</ul>
<p>These are just some examples. Basically you can calculate anything you want with the calculator block. Please have a look at our <a href="{{ page.base }}calculator-features/">calculator features overview</a> to see everything you can do with the calculator block.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/demo-text-counters.gif" width="1200" height="760" alt="Screenshot of a text analysis in Tripetto" loading="lazy" />
  <figcaption>Demonstration of a text input that has some text counters and text analysis.</figcaption>
</figure>
<blockquote>
  <h4>📌 Also see: Calculator block</h4>
  <p>In the article you're reading now we describe how to use text counters in your calculator blocks. For global instructions about the calculator block, please have a look at this article:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">How to use the calculator block</a></li>
  </ul>
</blockquote>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>You can use text counters inside each operation in combination with all text input fields.</p>

<h3 id="add-operation">Add operation</h3>
<p>From the menu to add an operation, you will see a list of questions in your form that you can use to calculate with. For the text input fields in your form the following options will be available:</p>
<ul>
  <li><code>Character count</code> - Count the amount of entered characters;</li>
  <li><code>Word count</code> - Count the amount of entered words;</li>
  <li><code>Line count</code> - Count the amount of entered line breaks;</li>
  <li><code>Count occurrences</code> - Count the amount a certain text/character/regular expression occurs.</li>
</ul>
<p>We will explain these options below.</p>

<h3 id="counters">Setup text counters</h3>
<p>There are several counters that can count the <strong>amount of characters, words and lines</strong> entered by the respondent in the selected text input field. Just add them as an operation in the calculator.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/block-text-count.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>In this example we count the number of characters in the selected question.</figcaption>
</figure>

<h3 id="analysis">Setup text analysis</h3>
<p>A powerful tool to analyse the entered text is to count the <strong>occurrences of a certain text or character</strong>. By adding this as an operation you can specify which text/character you want to analyse.</p>
<p>For really advanced purposes you can even analyse text using regular expressions. This will count the occurrences of the regular expression that you enter.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/block-text-analysis.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>In this example we count the number that the word <code>Happy</code> is entered in the selected question.</figcaption>
</figure>
<hr />

<h2 id="example">Example: keep track of entered characters</h2>
<p>An often seen use case is you have a maximum amount of characters to enter in a text input field. And you want to show your respondent how many characters are left while they are typing. Let's see how we can set this up.</p>
<h4>1. Create text input</h4>
<p>First simply add the text question you want to ask. And set the maximum amount of characters that can be entered by your respondent, for example <code>1000</code>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tutorial-text-count-1.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Our text input block.</figcaption>
</figure>
<h4>2. Add calculator block</h4>
<p>Now, we want to keep track of the entered characters of calculate how many characters are left. To do so, we add a <code>Calculator</code> block and we give that block a name, for example <code># Characters left</code>.</p>
<p>We start with the amount of maximum characters that can be entered, in this case <code>1000</code>.</p>
<p>And we add another operation that will subtract the amount of the already entered characters. You do that by adding a <code>Subtract</code> operation<i class="fas fa-arrow-right"></i>Select the desired question<i class="fas fa-arrow-right"></i>Select <code>Character count</code>.</p>
<p>This calculator block will now hold the realtime amount of characters that are left for the respondent.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tutorial-text-count-2.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Our calculator block.</figcaption>
</figure>
<h4>3. Show counters</h4>
<p>Back to the text input question. Let's say we want to show our counter in the help text of that question. You can show the value of the calculator by typing the <code>@</code> sign and select the calculator block.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tutorial-text-count-3.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Our help text of the text input block.</figcaption>
</figure>

<hr />
<h2>More information</h2>
<p>The calculator block has lots of features, so we have several ways to learn all about it.</p>
<h3>Help center</h3>
<p>Our help articles help you out on all different aspects of the calculator:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/">How to add instant scores and calculations inside question blocks</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">How to use the calculator block</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-operations-in-the-calculator-block/">How to use operations in the calculator block (add, subtract, multiply, divide, equal)</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-given-answers-from-respondents-in-your-calculations/">How to use given answers from respondents in your calculations</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-scores-in-your-calculations/">How to use scores in your calculations</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-comparisons-in-your-calculations/">How to use comparisons in your calculations</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/">How to use functions and constants in your calculations</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-subcalculations-multistep-formulas-in-your-calculations/">How to use subcalculations (multistep formulas) in your calculations</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-text-counters-in-your-calculations/">How to use text counters in your calculations</a><span class="related-current">Current article</span></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-calculations-with-logic-branches/">How to use calculations with logic branches</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-outcomes-of-calculator-blocks/">How to use the outcomes of calculator blocks</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-compare-the-outcomes-of-calculator-blocks/">How to compare the outcomes of calculator blocks</a></li>
</ul>
<h3>Overviews</h3>
<p>We also made some overviews of the capabilities that the calculator block provides:</p>
<div>
  <a href="{{ page.base }}calculator/" class="blocklink">
    <div>
      <span class="title">No-code calculations with the calculator block</span>
      <span class="description">Make quizzes, order forms, exams and more with no-code calculations. All without any coding in Tripetto's calculator block.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" loading="lazy" />
    </div>
  </a>
</div>
<div>
  <a href="{{ page.base }}calculator-features/" class="blocklink">
    <div>
      <span class="title">All calculator features</span>
      <span class="description">A complete overview of all features the calculator block has to offer, including operations, scores, comparisons, functions and constants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" loading="lazy" />
    </div>
  </a>
</div>
