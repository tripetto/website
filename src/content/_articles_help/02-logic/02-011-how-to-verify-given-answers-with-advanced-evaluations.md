---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-verify-given-answers-with-advanced-evaluations/
title: Verify given answers with advanced evaluations - Tripetto Help Center
description: Learn how advanced evaluations help you to verify given answers inside your form and use them in branch logic.
article_title: How to verify given answers with advanced evaluations
article_folder: editor-branch-conditions-evaluate
article_video: logic-branch-conditions
author: mark
time: 4
time_video: 3
category_id: logic
subcategory: logic_branch_conditions
areas: [studio, wordpress]
---
<p>Learn how advanced evaluations help you to verify given answers and use them in branch logic.</p>

<h2 id="when-to-use">When to use</h2>
<p>By default each question block offers the most used branch conditions, for example to check if a certain option has been selected. For more advanced conditions based on the respondent's answers, you can use the evaluate condition. Just like the block conditions, you use them to validate if the given answer of a certain question matches your wish(es), but then with some more advanced filters.</p>
<p>Some examples of evaluate conditions:</p>
<ul>
  <li>If the respondent does not select <i>'Option C'</i> at the dropdown question <i>'Question 1'</i>, then show this branch;</li>
  <li>If the respondent selects a date in the date question <i>'Question 2'</i> that's between <i>'January 1, 2020'</i> and <i>'January 31, 2020'</i>, then show this branch;</li>
  <li>If the respondent gives a rating of <i>'3 stars or higher'</i> at the rating question <i>'Question 3'</i>, then show this branch;</li>
  <li>If the respondent enters an email address that contains <i>'@gmail.com'</i> at the email address question <i>'Question 4'</i>, then show this branch;</li>
  <li>If the respondent does not answer 'Statement 1' of the matrix question <i>'Question 5'</i>, then show this branch.</li>
</ul>
<blockquote>
  <h4>📌 Also see: More logic possibilities</h4>
  <p>The logic described in this article is just one example of what Tripetto can do to make your forms smart. Have a look at this article to see all logic capabilities:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/">Discover the power of branches for your logic</a></li>
  </ul>
</blockquote>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>As a demonstration scenario we want to check if a respondent did <i>not</i> select a certain option from a dropdown question.</p>
<p>We already added a <a href="{{ page.base }}help/articles/how-to-use-the-dropdown-block/">dropdown block</a> to our form so our respondents can select an option from that. By default you can now easily create a branch that checks if a certain option has been selected. But in this case we want to do a more advanced condition, namely check if a certain option is <i>not</i> selected.</p>
<h3 id="branch">Create branch</h3>
<p>To do so we create a branch and we add an <code>Evaluate</code> condition. This opens the evaluate screen right away. In there you can now select the question that you want to evaluate to see the possible evaluation options.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-branch.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Add an evaluate condition and select your question.</figcaption>
</figure>
<p>Depending on the question type, you have several compare modes and options to check the value. After you selected the compare mode, you can enter the value that you want to check. In general you can choose between:</p>
<ul>
  <li><code>Fixed value</code> - A fixed value that you enter in the form builder;</li>
  <li><code>Answered value</code> - A flexible value that your respondent has answered in another question in your form.</li>
</ul>
<p>For our example we want to execute this branch when the respondent has <i>not</i> selected <code>Option C</code>. To do so, we configure the following:</p>
<ul>
  <li>As <code>Compare mode</code> select <code>Value does not match</code>;</li>
  <li>As comparison select <code>Text</code> and then select <code>Option C</code>.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-condition.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Configure the evaluate condition.</figcaption>
</figure>
<h3 id="follow-up">Add follow-up</h3>
<p>Now we have setup the branch, we can add the desired follow-up of our form that only will be visible for respondents that have not selected <code>Option C</code> in the dropdown question.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/03-follow-up.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>The full setup of an evaluate condition and the follow-up.</figcaption>
</figure>
<p>That's it! The follow-up question will now only be shown to respondents that not selected Option C from the dropdown menu.</p>
