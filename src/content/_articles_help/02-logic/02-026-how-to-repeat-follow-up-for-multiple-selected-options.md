---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-repeat-follow-up-for-multiple-selected-options/
title: Repeat follow-up for multiple selected options - Tripetto Help Center
description: Learn how to repeat follow-up questions for each of the selected options from a list.
article_title: How to repeat follow-up for multiple selected options
article_id: logic-branch-repeat-options
article_video: logic-branch-behaviors
author: jurgen
time: 3
time_video: 5
category_id: logic
subcategory: logic_branch_behavior
areas: [studio, wordpress]
---
<p>Learn how to repeat follow-up questions for each of the selected options from a list.</p>

<h2 id="when-to-use">When to use</h2>
<p>A good example of smartness inside a form is when you can repeat follow-up for each of the selected options from a multiple choice list.</p>
<p>Let's say you've got a multiple choice list of five product options and your respondents can select multiple of those products. For each selected product you want to ask how many the respondent wants to order and calculate the subtotal per product.</p>
<figure>
  <img src="{{ page.base }}images/help/editor-branch-repeat/demo.gif" width="1200" height="760" alt="Screenshot of a form in Tripetto" loading="lazy" />
  <figcaption>Demonstration of iterating questions for selected products.</figcaption>
</figure>
<blockquote>
  <h4>📌 Also see: More logic possibilities</h4>
  <p>The logic described in this article is just one example of what Tripetto can do to make your forms smart. Have a look at this article to see all logic capabilities:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/">Discover the power of branches for your logic</a></li>
  </ul>
</blockquote>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>The idea is to make a question that respondents can select multiple options from and then ask a follow-up question for each of the selected items.</p>
<h3 id="question-types">Question types</h3>
<p>You can use several question types to let your respondents select multiple options from a list:</p>
<ul>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-checkboxes-block/">Checkboxes</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-dropdown-multi-select-block/">Dropdown (multi-select)</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-multiple-choice-block/">Multiple choice</a> (if set to multiple select);</li>
  <li><a href="{{ page.base }}help/articles/how-to-use-the-picture-choice-block/">Picture choice</a> (if set to multiple select).</li>
</ul>
<h3 id="branch">Create branch</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/editor-branch-repeat/00-repeat.gif" alt="Screenshot of the form builder in Tripetto" width="730" height="731" loading="lazy" />
  <figcaption>Select the branch behavior.</figcaption>
</figure>
<p>Now, instead of creating separate branches for each option (like you would do in other form builders), you can just create one single branch and let the form repeat the question blocks inside that branch for <i>each</i> selected option.</p>
<p>To do so, click the branch bubble at the top of the branch and then select <code>For each condition match (iteration)</code>.</p>
<h3 id="follow-up">Add follow-up</h3>
<p>In the example we created a branch with one question. That question will be asked for each of the options the respondent selects.</p>
<h3 id="piping">Use piping values</h3>
<p>Inside the iterating branch the same question blocks will be used multiple times, namely for each of the items your respondent selects. To show your respondent about which selected option each question is, you can use <a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/">piping logic</a>. Inside an iterating branch there is a special piping value available, called <code>Automatic text value</code>. By using that piping value, the form will automatically show the name of the selected option that the iterating question is about.</p>
<h3>Add branch number</h3>
<p>Inside the iterating branch you might want to add an ascending counter to show the branch iteration number that your respondent is in. To do that you can add a <a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/">calculator block</a> and use the constant <code>Branch number</code>. And by using <a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/">piping logic</a> you can then show that branch number in a question that you show in your iteration.</p>
<blockquote>
  <h4>📌 Also see: Piping logic</h4>
  <p>Please have a look at this article for more information about recalling values with piping logic:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/">How to show respondents' answers using piping logic</a></li>
  </ul>
</blockquote>
<hr />

<h2>More video tutorials</h2>
<p>You can have a look at <a href="{{ page.base }}help/articles/">all our help articles</a> for more in-depth (video) tutorials to get the most out of Tripetto. Or have a look at our <a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank" rel="noopener noreferrer">YouTube channel</a> for all videos.</p>
<div>
  <a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank" rel="noopener noreferrer" class="blocklink">
    <div>
      <span class="title">Tripetto YouTube channel<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Visit our YouTube channel with tutorial videos.</span>
      <span class="url">youtube.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-youtube.svg" width="160" height="160" alt="YouTube logo" loading="lazy" />
    </div>
  </a>
</div>
