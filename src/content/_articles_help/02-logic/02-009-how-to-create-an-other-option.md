---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-create-an-other-option/
title: Create an 'Other...' option - Tripetto Help Center
description: Learn how to create an 'Other...' option, for example to use in multiple choice questions.
article_title: How to create an 'Other...' option
article_id: logic-branch-other
article_folder: editor-branch-other
article_video: logic-branch-logic
author: jurgen
time: 2
time_video: 2
category_id: logic
subcategory: logic_branch_conditions
areas: [studio, wordpress]
---
<p>Learn how to create an 'Other...' option, for example to use in multiple choice questions.</p>

<h2 id="when-to-use">When to use</h2>
<p>A common scenario inside forms is the case of an 'Other...' option as part of a list of options, giving the respondent the possibility to enter a missing option.</p>
<figure>
  <img src="{{ page.base }}images/help/editor-branch-other/demo.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Demonstration of an 'Other...' option.</figcaption>
</figure>
<blockquote>
  <h4>📌 Also see: More logic possibilities</h4>
  <p>The logic described in this article is just one example of what Tripetto can do to make your forms smart. Have a look at this article to see all logic capabilities:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/">Discover the power of branches for your logic</a></li>
  </ul>
</blockquote>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>The idea is to use a simple branch based on a match on the 'Other...' option. In case a respondent selects that option, a follow-up question is shown. In most cases this will be a text input field, so the respondent can enter its own answer.</p>

<h3 id="branch">Create branch</h3>
<p>To do so, we have already added a <a href="{{ page.base }}help/articles/how-to-use-the-multiple-choice-block/">multiple choice block</a> with several options, including one option named 'Other...'.</p>
<p>By clicking the <code><i class="fas fa-ellipsis-h"></i></code> button in the multiple choice block we can instantly create branches per selected option. In this case we select the <code>Other...</code> option. The branch will be created immediately.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-branch.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Creating a branch for an 'Other...' option.</figcaption>
</figure>

<h3 id="follow-up">Add follow-up</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-follow-up.png" width="820" height="725" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>The follow-up if a respondent selected the 'Other...' option.</figcaption>
</figure>
<p>Now you can add the desired follow-up when a respondent selected the 'Other...' option. In this example we have just added one text field to let our respondent enter the name of their other favorite food. But you can add anything you need as a follow-up. That includes unlimited questions, but also new branches and other logic types. The possibilities are endless; just how you need it.</p>
<hr />

<h2>More video tutorials</h2>
<p>You can have a look at <a href="{{ page.base }}help/articles/">all our help articles</a> for more in-depth (video) tutorials to get the most out of Tripetto. Or have a look at our <a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank" rel="noopener noreferrer">YouTube channel</a> for all videos.</p>
<div>
  <a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank" rel="noopener noreferrer" class="blocklink">
    <div>
      <span class="title">Tripetto YouTube channel<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Visit our YouTube channel with tutorial videos.</span>
      <span class="url">youtube.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-youtube.svg" width="160" height="160" alt="YouTube logo" loading="lazy" />
    </div>
  </a>
</div>
