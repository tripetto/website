---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-skip-questions-or-parts-of-your-form/
title: Skip questions or parts of your form - Tripetto Help Center
description: Learn how you can skip certain questions or whole parts of your form, based on the given answers of your respondent.
article_id: logic-skip
article_title: How to skip questions or parts of your form
article_folder: editor-jump
article_video: logic-branch-endings
author: jurgen
time: 4
time_video: 2
category_id: logic
subcategory: logic_branch_endings
areas: [studio, wordpress]
---
<p>Learn how you can skip certain questions or whole parts of your form, based on the given answers of your respondent.</p>

<h2 id="when-to-use">When to use</h2>
<p><a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/">Branch logic</a> is ideal to show certain follow-up questions only to respondents that answered the previous questions in a certain way (<a href="{{ page.base }}help/articles/how-to-set-a-follow-up-based-on-given-answers/">more information</a>). That way you make sure you only ask the right questions, which leads to higher completion rates.</p>
<p>In some cases though, it's easier to just skip certain questions (or whole parts) of your form when a certain respondent does not need to answer those questions/parts. For such cases you can jump to a certain upcoming point in your form, bypassing the questions/parts that are in between.</p>
<blockquote>
  <h4>📌 Also see: More logic possibilities</h4>
  <p>The logic described in this article is just one example of what Tripetto can do to make your forms smart. Have a look at this article to see all logic capabilities:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/">Discover the power of branches for your logic</a></li>
  </ul>
</blockquote>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>In Tripetto you can use <a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/">branch logic</a> to let respondents follow a certain flow in your form, based on their given answers. For this example we're going to add the branch conditions that the respondent needs to match to skip the questions/part of your form.</p>
<p>There are a few things to keep in mind to be able to jump inside your form:</p>
<ul>
  <li>You can only jump to sections that have a name. To give a section a name, you can click the section's header and provide a name. This name will only be used in the form builder; not in your actual form;</li>
  <li>You can only jump to upcoming sections; not to sections in the past;</li>
  <li>You can only jump to sections that can be reached in the form structure. It's for example not possible to jump to a section that's inside another branch that relies on branch conditions that aren't fulfilled to access that branch.</li>
</ul>

<h3 id="prepare">Prepare form structure</h3>
<p>To demonstrate how to make jumps in your form, we have already added a <a href="{{ page.base }}help/articles/how-to-use-the-yes-no-block/">yes/no block</a>. Now we want to bypass a certain part of the form when <code>No</code> is selected in that yes/no question.</p>

<h3 id="branch">Create branch</h3>
<p>The easiest way to create a branch is by clicking the <code><i class="fas fa-ellipsis-h"></i></code> button in a question block. In that menu the possible branch filters will be shown and you can select one of them right away. For our example we click the <code>No</code> option and a new branch will be created automatically with the <code>No</code> option as the branch condition. Respondents that select <code>No</code> will now follow that route.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-branch.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Creating a branch for the <code>No</code> option.</figcaption>
</figure>

<h3 id="branch-end">Set branch end to jump</h3>
<p>By default this branch will simply continue with the form, but now we want those respondents to jump over a part of the form and continue with another section later in the form. In Tripetto you can do that quite easily by clicking the end bubble of the branch. Then select <code>Jump to a specific point</code>. This will open a menu with the sections in your form that you can jump to. From that menu select the section that you want to jump to, in our case the branch with name <code>Part 2 - Movies</code>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-jump.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Set the branch end to jump to a specific point.</figcaption>
</figure>
<p>And that's it! Respondents that select <code>No</code> will now jump over the <code>Sports</code> part of our example form and will continue with the <code>Movies</code> part that we jumped to.</p>
