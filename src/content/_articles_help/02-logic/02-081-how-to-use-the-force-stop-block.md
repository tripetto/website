---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-force-stop-block/
title: Use the force stop block - Tripetto Help Center
description: Use the force stop block if you want to prevent a form from submitting, for example when a certain condition is (not) matched.
article_title: How to use the force stop block
article_id: action-blocks
article_folder: editor-block-force-stop
author: mark
time: 2
category_id: logic
subcategory: logic_stop_actions
areas: [studio, wordpress]
---
<p>Use the force stop block if you want to prevent a form from submitting, for example when a certain condition is (not) matched.</p>

<h2 id="when-to-use">When to use</h2>
<p>The force stop blocks helps you to keep your results clean. It shows a message in your form without the possibility for your respondent to proceed and submit the form. This can be handy when a respondent for example does not meet the requirements that you have set for your target audience. Then you can show a message explaining the respondent does not belong to your target audience.</p>
<blockquote>
  <h4>🚧 Warning</h4>
  <p>The force stop block prevents users from being able to complete your form. Therefore make sure you only add force stop blocks on the right places in your form's structure, for example in a branch that gets executed when a respondent does not meet your requirements to fill out the form.</p>
</blockquote>
<figure>
  <img src="{{ page.base }}images/help/blocks/force-stop.png" width="1200" height="760" alt="Screenshot of a force stop block in Tripetto" loading="lazy" />
  <figcaption>Demonstration of a force stop block.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>The force stop block is available as a question type. Add a new block to your form and then select the question type <code>Force stop</code>. You can add it in any given position and in an unlimited amount inside your forms.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/menu.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Force stop blocks are available from the Question type menu.</figcaption>
</figure>

<p>You can now customize this block to your needs with the following features:</p>
<h3 id="features-general">General</h3>
<ul>
  <li><strong>Text</strong><br/>Use the <code>Text</code> feature for the name/title of this block.</li>
  <li><strong>Description</strong><br/>Enable the <code>Description</code> feature to add a description to this block.</li>
  <li><strong>Image</strong><a href="{{ page.base }}help/articles/how-to-use-images-and-videos-in-your-form/" class="article-more-info" title="More information about images"><i class="fas fa-info-circle"></i></a><br/>Enable the <code>Image</code> feature to add an image to this block.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/message.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Example of a force stop block, placed inside a branch if the respondent does not belong to your target audience.</figcaption>
</figure>

<h3 id="features-options">Options</h3>
<ul>
  <li><strong>Status</strong><br/>By default this block will be executed in your form. Enable the <code>Status</code> feature to disable the execution.</li>
</ul>
<hr />

{% include help-article-blocks.html %}
