---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/
title: Show respondents' answers using piping logic - Tripetto Help Center
description: Learn how to use piping logic to show respondents' answers in your form.
article_title: How to show respondents' answers using piping logic
article_id: logic-piping
article_folder: editor-piping
article_video: logic-piping
author: jurgen
time: 2
time_video: 5
category_id: logic
subcategory: logic_piping
areas: [studio, wordpress]
---
<p>Learn how to use piping logic to show respondents' answers in your form.</p>

<h2 id="when-to-use">When to use</h2>
<p>Making your form feel like a personal conversation, improves your completion rates, as your respondents are more likely to complete the form. An ideal way to achieve that is by using the given answers later on in the form, so people really have the feeling they are being heard. This is called <strong>piping logic</strong>.</p>
<p>A simple example could be to ask for the name of your respondent and after that personally greet them with their name.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-example.gif" width="1200" height="760" alt="Screenshot of a form in Tripetto" loading="lazy" />
  <figcaption>Demonstration of piping logic.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>You can show given answers in almost every text feature inside question blocks. At the place where you want to show a given answer of a specific question block, you can make a reference to that question block.</p>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-piping.gif" width="466" height="147" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Select the desired question value. In this example the selected rating.</figcaption>
</figure>
<p>To do so, you type the <code> @ </code> sign at the desired position in your text. A menu will appear with all question blocks in your form that you can use for piping logic. After selecting the right question block, Tripetto will take care of showing the respondents' answer on that position.</p>
<blockquote>
  <h4>📣 Info: What's that long code I see?</h4>
  <p>For now, when you select a question block for piping, a pretty long code will appear at the position where you want to show the piping value. It's important you do <strong>not change</strong> this code, as it's the system's reference to the selected question block.</p>
  <p>We know this isn't the most beautiful solution, so it's already in our planning to improve this in an upcoming update.</p>
</blockquote>
<h3 id="iterating">Iterating branches</h3>
<p>A handy feature in Tripetto is you can iterate branches based on the selected options by a respondent (<a href="{{ page.base }}help/articles/how-to-repeat-follow-up-for-multiple-selected-options/">more information</a>). For each selected option, the same follow-up questions will be shown, but of course you want to make clear about which if the selected options each question is.</p>
<p>That's why you've got an extra piping option inside iterating branches, namely <code>Automatic text value</code>. That takes care of showing the name of the corresponding selected option that the current follow-up question is about.</p>
<hr/>

<h2>More information</h2>
<p>We have made a video tutorial on how to use piping logic. It shows multiple implementations of piping logic, for example with different kinds of question types.</p>
<figure>
  <div class="youtube-player" data-id="S4VJ4qu3SIY"></div>
  <figcaption>Video tutorial on piping logic.</figcaption>
</figure>
