---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-prefill-question-blocks-with-initial-values/
title: Prefill question blocks with initial values - Tripetto Help Center
description: You can enter initial values to your question blocks, so you can help your respondents to fill out your form quickly.
article_title: How to prefill question blocks with initial values
article_folder: editor-block-set-value
article_video: logic-prefill
author: jurgen
time: 4
time_video: 2
category_id: logic
subcategory: logic_data_actions
areas: [studio, wordpress]
redirect_from:
- /help/articles/how-to-prefill-question-blocks-with-the-right-values/
---
<p>You can enter initial values to your question blocks, so you can help your respondents to fill out your form quickly.</p>

<h2 id="when-to-use">When to use</h2>
<p>Normally all forms start with all blank input fields. But in some cases you already know some answers of your respondents, so why not prefill them? By doing so your respondents can complete your form easier and faster, resulting in higher completion rates. Some examples:</p>
<ul>
  <li>Most of your respondents live in New York City, so in the 'City' question in your form, you prefill 'New York City'. Most of your respondents can now simply accept the answer and continue your form;</li>
  <li>You have sent personalized invitation links to your respondents, so you already know the name of each respondent. The name is part of the form URL that you have sent them. You can now use that value from the URL to prefill in the 'Name' question;</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/demo.png" width="1200" height="733" alt="Screenshot of a prefilled form in Tripetto" loading="lazy" />
  <figcaption>Demonstration of prefilling a question block from the URL (<code>?name=Julie</code>).</figcaption>
</figure>
<blockquote>
  <h4>📌 Also see: Set value block</h4>
  <p>In the article you're reading now we describe how to prefill values in your question blocks. We use the set value block for that. For global instructions about the set value block, please have a look at this article:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-set-value-block/">How to use the set value block</a></li>
  </ul>
</blockquote>
<blockquote>
  <h4>📌 Also see: Update values</h4>
  <p>It's important to know that prefilling only works for blocks that don't have a value yet. If you want to update blocks that already have a value, please have a look at this article:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-update-values-throughout-your-form/">How to update values throughout your form</a></li>
  </ul>
</blockquote>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>You can prefill question blocks with the set value block.</p>

<h3 id="add-blocks">Add set value block</h3>
<p>First build your form with the question blocks you want to prefill.</p>
<p>Continue with adding a <a href="{{ page.base }}help/articles/how-to-use-the-set-value-block/">set value block</a>. In this case we add that block at the start of our form, so all our blocks will become prefilled when the respondent opens the form.</p>

<h3 id="purpose">Select purpose</h3>
<p>Now open the set value block. You will first see the <code>Purpose</code> feature. Set that to <code>Prefilling</code> and the builder will make sure all settings are configured correctly for you.</p>
<blockquote>
  <h4>📌 Also see: Update values</h4>
  <p>It's important to know that prefilling only works for blocks that don't have a value yet. If you want to update blocks that already have a value, please have a look at this article:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-update-values-throughout-your-form/">How to update values throughout your form</a></li>
  </ul>
</blockquote>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/prefill-setup.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>A form setup with an empty set value block with purpose <code>Prefilling</code>.</figcaption>
</figure>

<h3 id="values">Add values</h3>
<p>Now you can make a list of question blocks that you want to prefill with a certain value. Per row in your list you can determine how you want to prefill that, for example:</p>
<ul>
  <li>Prefill with a static value;</li>
  <li>Prefill with a value from the query string.</li>
</ul>

<h2 id="static">Example: prefill with static value</h2>
<p>In this example we want to prefill our 'City' question with the fixed value 'New York City', as we know most of our respondents live there. Follow these steps:</p>
<ol>
  <li>In the set value block click the <code><i class="fas fa-plus"></i></code> button at the bottom of the <code>Values</code> list;</li>
  <li>Next select the <code>City</code> question from the form. In our example form this is a dropdown question type;</li>
  <li>Now select <code>Fixed value</code> as the desired value;</li>
  <li>Now select the right value from the dropdown options, in this case we select <code>New York City</code>.</li>
</ol>
<p>That's it! Now the form will always select 'New York City' as the default selected option.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/prefill-static.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>The steps to take to prefill with a static value.</figcaption>
</figure>

<h2 id="querystring">Example: prefill with query string value</h2>
<p>In this example we want to prefill our 'Name' question with a value that we collect from the query string of the form URL (<a href="https://en.wikipedia.org/wiki/Query_string" target="_blank" rel="noopener noreferrer">what's a query string?</a>). Our form URL for example looks something like this: <code>https://tripetto.app/your-form/<strong>?name=Julie</strong></code>. Follow these steps:</p>
<ol>
  <li>We can use the same set value block to prefill multiple question blocks. Just click the <code><i class="fas fa-plus"></i></code> button at the bottom of the <code>Values</code> list again;</li>
  <li>Next select the <code>Name</code> question from the form. In our example form this is a text (single line) question type;</li>
  <li>Now select <code>From query string</code> as the desired value;</li>
  <li>Now enter the query string parameter that you want to use the value of. In this case we want to use the value of parameter <code>name</code>.</li>
</ol>
<p>That's it! Now the form will use the value that's entered in the URL as the default entered text value.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/prefill-querystring.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>The steps to take to prefill with a value from the query string.</figcaption>
</figure>
<hr/>

<h2>More information</h2>
<p>The set value block has lots of possibilities, so we have several help articles to learn all about it:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-set-value-block/">How to use the set value block</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-prefill-question-blocks-with-initial-values/">How to prefill question blocks with initial values</a><span class="related-current">Current article</span></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-update-values-throughout-your-form/">How to update values throughout your form</a></li>
</ul>
