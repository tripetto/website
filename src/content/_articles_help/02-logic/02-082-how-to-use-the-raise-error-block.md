---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-raise-error-block/
title: Use the raise error block - Tripetto Help Center
description: Use the raise error block if you want to show an error message to the respondent and prevent a form from submitting.
article_title: How to use the raise error block
article_id: action-blocks
article_folder: editor-block-raise-error
author: mark
time: 2
category_id: logic
subcategory: logic_stop_actions
areas: [studio, wordpress]
---
<p>Use the raise error block if you want to show an error message to the respondent and prevent a form from submitting.</p>

<h2 id="when-to-use">When to use</h2>
<p>The raise error blocks shows an error message in your form without the possibility for your respondent to proceed and submit the form. This can be handy when a respondent for example entered an email address, but that email address does not meet your requirements for that address. Then you can show an error message explaining why the email address is not correct. And asking them to correct the given answer.</p>
<blockquote>
  <h4>🚧 Warning</h4>
  <p>The raise error block prevents users from being able to complete your form. Therefore make sure you only add raise error blocks on the right places in your form's structure, for example in a branch that gets executed when a respondent hasn't entered a valid answer.</p>
</blockquote>
<figure>
  <img src="{{ page.base }}images/help/blocks/raise-error.png" width="1200" height="760" alt="Screenshot of a raise error block in Tripetto" loading="lazy" />
  <figcaption>Demonstration of a raise error block.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>The raise error block is available as a question type. Add a new block to your form and then select the question type <code>Raise error</code>. You can add it in any given position and in an unlimited amount inside your forms.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/menu.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Raise error blocks are available from the Question type menu.</figcaption>
</figure>

<p>You can now customize this block to your needs with the following features:</p>
<h3 id="features-general">General</h3>
<ul>
  <li><strong>Error message</strong><br/>Use the <code>Error message</code> feature for the name/title of this block.</li>
  <li><strong>Description</strong><br/>Enable the <code>Description</code> feature to add a description to this block.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/error.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Example of a raise error block, placed inside a branch if the respondent entered an invalid email address.</figcaption>
</figure>

<h3 id="features-options">Options</h3>
<ul>
  <li><strong>Status</strong><br/>By default this block will be executed in your form. Enable the <code>Status</code> feature to disable the execution.</li>
</ul>
<hr />

{% include help-article-blocks.html %}
