---
layout: help-article
base: ../../../
permalink: /help/articles/learn-about-different-types-of-branch-endings-for-your-logic/
title: Types of branch endings for logic - Tripetto Help Center
description: You can determine how each branch should behave at the end of the branch. This will affect how the form continues after the branch.
article_title: Learn about different types of branch endings for your logic
article_id: logic-branch-endings
article_folder: editor-branch-endings
article_video: logic-branch-endings
author: jurgen
time: 3
time_video: 2
category_id: logic
subcategory: logic_branch
areas: [studio, wordpress]
---
<p>You can determine how each branch should behave at the end of the branch. This will affect how the form continues after the branch, for example to skip to another point, the end, or show a custom closing message.</p>

<h2 id="builder">Branches</h2>
<p>In Tripetto you use branches to add the desired logic to your form. In <a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/">this article we showed how to add branches to your form</a>.</p>
<p>Part of each branch is the <strong>branch ending</strong> that determines what happens at the end of the branch. Let's take a deeper look into that.</p>

<h2 id="types">Branch endings</h2>
<p>At the end of each branch it's possible to set what should happen after a respondent completes the branch.  To do so, you click the green bubble at the bottom of a branch. There are four options:</p>
<ul>
  <li>Continue with the next section or branch;</li>
  <li>Jump to a specific point;</li>
  <li>Jump to end;</li>
  <li>End with closing message.</li>
</ul>

<h3 id="next">Continue with the next section or branch</h3>
<p>This is the default branch end behavior. It lets the form determine the first logical continuation and show that to the respondent. Depending on your form's structure that could be the next question block, an action block, the end, etc.</p>

<h3 id="jump-point">Jump to a specific point</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-jump.gif" width="513" height="304" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Example of a jump to a specific point.</figcaption>
</figure>
<p>This lets the form jump to a specific section inside your form that you select. This way you can for example skip a few questions and jump to a later point in your form. <a href="{{ page.base }}help/articles/how-to-skip-questions-or-parts-of-your-form/">This article dives deeper into jumping in your form</a>.</p>
<p>Keep in mind you can only jump to sections that are in the future and have a name. So before you setup the jump, make sure you have given a name to the section that you want to jump to.</p>

<h3 id="jump-end">Jump to end</h3>
<p>This lets the form jump to the very end immediately. This will show the <a href="{{ page.base }}help/articles/how-to-add-a-closing-message/">common closing message</a> (including the <a href="{{ page.base }}help/articles/how-to-redirect-to-a-url-at-form-completion/">possibility to redirect</a>) to your respondents.</p>

<h3 id="closing-message">End with closing message</h3>
<p>This lets the form end and show a <a href="{{ page.base }}help/articles/how-to-add-one-or-multiple-closing-messages/">custom closing message</a> (including the <a href="{{ page.base }}help/articles/how-to-redirect-to-a-url-at-form-completion/">possibility to redirect)</a> to your respondents that end the form here.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-custom-end.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Example of a custom closing message.</figcaption>
</figure>
<hr />

<h2>More about branch logic</h2>
<p>Branch logic is very powerful, so we made some different help articles to help you with this:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/">Discover the power of branches for your logic</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-add-branches-in-the-form-builder/">How to add branches in the form builder</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">Learn about different types of branch conditions for your logic</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-behavior-for-your-logic/">Learn about different types of branch behavior for your logic</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-endings-for-your-logic/">Learn about different types of branch endings for your logic</a><span class="related-current">Current article</span></li>
</ul>
