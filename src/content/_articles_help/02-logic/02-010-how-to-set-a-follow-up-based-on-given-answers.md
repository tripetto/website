---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-set-a-follow-up-based-on-given-answers/
title: Set a follow-up based on given answers - Tripetto Help Center
description: Learn how to use various kinds of logic to show a certain follow-up based on the given answers of your respondents.
article_title: How to set a follow-up based on given answers
article_id: logic-branch-given-answers
article_folder: editor-branches
article_video: logic-branch-logic
author: jurgen
time: 4
time_video: 2
category_id: logic
subcategory: logic_branch_conditions
areas: [studio, wordpress]
redirect_from:
- /help/articles/how-to-set-a-follow-up-based-on-respondents-answers/
---
<p>Learn how to use various kinds of logic to show a certain follow-up based on the given answers of your respondents.</p>

<h2 id="when-to-use">When to use</h2>
<p>As we stated in <a href="{{ page.base }}help/articles/how-to-make-your-forms-smart-and-conversational/">our article about smart forms</a>, making your forms listen and react to your respondents will improve the completion rate. For each question you ask, there are several ways to listen and react to the givens answers by your respondents.</p>
<figure>
  <img src="{{ page.base }}images/help/editor-branches/demo.gif" width="1200" height="760" alt="Screenshot of a form in Tripetto" loading="lazy" />
  <figcaption>Demonstration of an 'Other...' option.</figcaption>
</figure>

<p>The filter options differ per question type. Discover our form blocks from the menu below to get an idea of what's possible in each question type.</p>
{% include help-article-blocks.html %}
<h3>Examples</h3>
<ul>
  <li>Based on a single selection from a list of options (i.e. single choice buttons, radio buttons, or dropdown);</li>
  <li>Based on a multiple selection from a list of options (i.e. multiple choice picture buttons, or checkboxes);</li>
  <li>Based on a (un)checked checkbox;</li>
  <li>Based on a yes-no decision;</li>
  <li>Based on a rating match;</li>
  <li>Based on a date match;</li>
  <li>Based on a text match (exact or contains);</li>
  <li>Based on a number verification (equal, lower, higher);</li>
  <li>Based on a calculator outcome (equal, lower, higher).</li>
</ul>
<blockquote>
  <h4>📌 Also see: More logic possibilities</h4>
  <p>The logic described in this article is just one example of what Tripetto can do to make your forms smart. Have a look at this article to see all logic capabilities:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/">Discover the power of branches for your logic</a></li>
  </ul>
</blockquote>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>For each condition you want to make your form smart, you can create a branch. And as soon as you're going to use branches, the visual form builder comes in handy, because this will help you to visualize the flows and decisions in your forms. Branches are plotted in the horizontal direction in the form builder.</p>
<p>For this example, we have already added a <a href="{{ page.base }}help/articles/how-to-use-the-multiple-choice-block/">multiple choice block</a> with several options.</p>

<h3 id="branch">Create branches</h3>
<p>The easiest way to create a branch is by clicking the <code><i class="fas fa-ellipsis-h"></i></code> button in a question block. In that menu the possible branch filters will be shown and you can select on of them right away. In our example we can click one of the options in the multiple choice block. A branch will then be created automatically with the selected option as the branch condition.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-branches.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Creating two separate branches for selected options.</figcaption>
</figure>

<p>Another option to create branches is by first creating an empty branch and then select the desired branch conditions for that branch. To do that, click the <code><i class="fas fa-plus"></i></code> button on the right side of the desired section. An empty branch will be added. After that click the <code><i class="fas fa-plus"></i></code> button in the branch to add the desired branch condition(s).</p>
<h3 id="follow-up">Add follow-ups</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-follow-up.png" width="1189" height="679" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Two branches for different user selections.</figcaption>
</figure>
<p>Now you can add the desired follow-ups for each branch. In our example we just added one question, but you can add anything you need as a follow-up. That includes unlimited questions, but also new branches and other logic types. The possibilities are endless; just how you need it.</p>
<hr />

<h2>More video tutorials</h2>
<p>You can have a look at <a href="{{ page.base }}help/articles/">all our help articles</a> for more in-depth (video) tutorials to get the most out of Tripetto. Or have a look at our <a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank" rel="noopener noreferrer">YouTube channel</a> for all videos.</p>
<div>
  <a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank" rel="noopener noreferrer" class="blocklink">
    <div>
      <span class="title">Tripetto YouTube channel<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Visit our YouTube channel with tutorial videos.</span>
      <span class="url">youtube.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-youtube.svg" width="160" height="160" alt="YouTube logo" loading="lazy" />
    </div>
  </a>
</div>
