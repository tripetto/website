---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-compare-the-outcomes-of-calculator-blocks/
title: Compare the outcomes of calculator blocks - Tripetto Help Center
description: You can compare the outcomes of different calculator blocks, for example to determine the highest score.
article_title: How to compare the outcomes of calculator blocks
article_folder: editor-block-calculator
article_id: action-blocks-calculator-detail
author: jurgen
time: 6
category_id: logic
subcategory: logic_calculator_actions
areas: [studio, wordpress]
---
<p>You can compare the outcomes of different calculator blocks, for example to determine the highest score.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use <a href="{{ page.base }}calculator/">the calculator block</a> when you want to perform calculations inside your form. When you have multiple calculators in your form you can compare them with each other to perform a certain follow-up, for example:</p>
<ul>
  <li>Score possible different outcomes based on given answers and show an advice for the highest outcome;</li>
  <li>Count the amounts of selected corresponding answer choices (for example in multiple choice questions) and show a follow-up for the most selected answer choices.</li>
</ul>
<blockquote>
  <h4>📌 Also see: Calculator block</h4>
  <p>In the article you're reading now we describe how to compare the outcomes of calculator blocks. For global instructions about the calculator block, please have a look at this article:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">How to use the calculator block</a></li>
  </ul>
</blockquote>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>The global idea of comparing calculator outcomes is that you have multiple calculator blocks in your form that have a comparable outcome. In most cases you then want to check which of those has the highest outcome, so you can act upon that, for example to give an advice.</p>

<h3 id="calculators">Add calculator blocks</h3>
<p>Start with adding your <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">calculator blocks</a>. In there add the operations to come to the outcome per separate calculator block. You can <a href="{{ page.base }}help/articles/how-to-use-given-answers-from-respondents-in-your-calculations/">use given answers</a> to determine the calculation and the outcome.</p>

<h3 id="branch-logic">Add branch logic</h3>
<p>Now that we have the different outcomes, we want to compare them with each other. To do so, we add an empty branch by clicking the <code><i class="fas fa-plus"></i></code> diamond at the right side of the section.</p>
<p>In the newly created branch, click the branch bubble at the top of it and then select the <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-behavior-for-your-logic/">branch behavior</a> <code>When all conditions match</code>. Now that branch will only be executed when <i>all</i> conditions in that branch match.</p>

<p>Now we will add those <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">branch conditions</a>. In this case we want to compare one of the calculator outcomes to the other calculator outcomes. To do so, we click the <code><i class="fas fa-plus"></i></code> button at the bottom of the branch<i class="fas fa-arrow-right"></i>Select your first calculator block <i class="fas fa-arrow-right"></i>Select the condition <code>Calculation is higher than</code>. Then in the settings of this branch condition set the comparison to <code>Value</code><i class="fas fa-arrow-right"></i>Select one of the other calculator blocks to compare with.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/compare-branch-condition.png" width="1200" height="760" alt="Screenshot of a comparison branch in Tripetto" loading="lazy" />
  <figcaption>Demonstration of comparing the outcome of a calculator block with an other calculator block.</figcaption>
</figure>
<p>Now you have one condition that checks if your first calculator block is higher than your second calculator block. Repeat this for every calculator block you have in this branch. In case you have 4 calculator blocks, you end with these conditions to check if Calculator 1 is the highest:</p>
<ul>
  <li><code>Calculator 1</code> is higher than <code>Calculator 2</code> AND</li>
  <li><code>Calculator 1</code> is higher than <code>Calculator 3</code> AND</li>
  <li><code>Calculator 1</code> is higher than <code>Calculator 4</code>.</li>
</ul>
<p>Now you have a branch that checks if one of the outcomes is higher than all other calculator outcomes.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/compare-branch.png" width="1200" height="760" alt="Screenshot of a comparison branch in Tripetto" loading="lazy" />
  <figcaption>A branch that compares one calculator with multiple other calculators.</figcaption>
</figure>

<h3 id="follow-up">Add follow-up</h3>
<p>Now you can add the desired follow-up if this branch is taken, or configure a <a href="{{ page.base }}help/articles/how-to-add-one-or-multiple-closing-messages/">custom closing message</a> for this branch.</p>

<h3 id="repeat">Repeat branch setup</h3>
<p>Now create such a branch setup for every of your calculators. In our example with 4 calculators you would have these branches in total:</p>
<h4>To check if Calculator 1 is the highest:</h4>
<ul>
  <li><code>Calculator 1</code> is higher than <code>Calculator 2</code> AND</li>
  <li><code>Calculator 1</code> is higher than <code>Calculator 3</code> AND</li>
  <li><code>Calculator 1</code> is higher than <code>Calculator 4</code>.</li>
</ul>
<h4>To check if Calculator 2 is the highest:</h4>
<ul>
  <li><code>Calculator 2</code> is higher than <code>Calculator 1</code> AND</li>
  <li><code>Calculator 2</code> is higher than <code>Calculator 3</code> AND</li>
  <li><code>Calculator 2</code> is higher than <code>Calculator 4</code>.</li>
</ul>
<h4>To check if Calculator 3 is the highest:</h4>
<ul>
  <li><code>Calculator 3</code> is higher than <code>Calculator 1</code> AND</li>
  <li><code>Calculator 3</code> is higher than <code>Calculator 2</code> AND</li>
  <li><code>Calculator 3</code> is higher than <code>Calculator 4</code>.</li>
</ul>
<h4>To check if Calculator 4 is the highest:</h4>
<ul>
  <li><code>Calculator 4</code> is higher than <code>Calculator 1</code> AND</li>
  <li><code>Calculator 4</code> is higher than <code>Calculator 2</code> AND</li>
  <li><code>Calculator 4</code> is higher than <code>Calculator 3</code>.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/compare-branches.png" width="1200" height="760" alt="Screenshot of comparison branches in Tripetto" loading="lazy" />
  <figcaption>Multiple branches that compare calculator outcomes.</figcaption>
</figure>
<blockquote>
  <h4>💡 Tip: Check identical outcomes</h4>
  <p>In the described setup, it is possible that multiple calculators have the same highest outcome. In that case none of the branches will be executed. Please take that into account while configuring your scores and/or branches.</p>
</blockquote>

<h2 id="score">Example: compare scores for personality types</h2>
<p>For this example we want to score 4 personality types, based on given answers to different question types. Each personality type gets its own scoring and in the end we want to redirect to a certain landingpage, based on the personality type with the highest score.</p>

<h4>1. Create questions</h4>
<p>First we have added our questions to determine the personality type, in this case four of them. You can use different question types, which all have their own way to be used in the calculator.</p>

<h4>2. Add calculators</h4>
<p>For each personality type we add a separate calculator block. Inside each calculator block we do the calculations to score that personality type.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/compare-scores.png" width="1200" height="760" alt="Screenshot of a calculator in Tripetto" loading="lazy" />
  <figcaption>A calculator block calculating the score for Personality type A.</figcaption>
</figure>

<h4>3. Add branches</h4>
<p>Start with adding the first branch to check if Personality type A has the highest score. Set the branch behavior to <code>When all conditions match</code>.</p>
<p>Then add the following branch conditions:</p>
<ul>
  <li><code>Personality type A</code> is higher than <code>Personality type B</code> AND</li>
  <li><code>Personality type A</code> is higher than <code>Personality type C</code> AND</li>
  <li><code>Personality type A</code> is higher than <code>Personality type D</code>.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/compare-scores-branch.png" width="1200" height="760" alt="Screenshot of a comparison branch in Tripetto" loading="lazy" />
  <figcaption>A branch that compares the score of Personality type A with the other personality types.</figcaption>
</figure>
<p>Repeat this branch setup for the other personality types. You now have 4 branches, each to check if that personality type has the highest score.</p>
<blockquote>
  <h4>💡 Tip: Check identical outcomes</h4>
  <p>In the described setup, it is possible that multiple calculators have the same highest outcome. In that case none of the branches will be executed. Please take that into account while configuring your scores and/or branches.</p>
</blockquote>

<h4>4. Add follow-up</h4>
<p>Inside each branch you can now add the follow-up in case that personality type has the highest score. In this case we want to redirect to a landingpage, so we setup a <a href="{{ page.base }}help/articles/how-to-add-one-or-multiple-closing-messages/">custom closing message</a> with a <a href="{{ page.base }}help/articles/how-to-redirect-to-a-url-at-form-completion/">redirect</a>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/compare-scores-branches.png" width="1200" height="760" alt="Screenshot of comparison branches in Tripetto" loading="lazy" />
  <figcaption>All branches that check which personality type has the highest score and a redirection.</figcaption>
</figure>

<h2 id="count">Example: compare amount of corresponding answer choices</h2>
<p>For this example we have 5 multiple choice questions with answer options A, B, C and D. To determine the follow-up, we want to count the amount of times that the respondent chose each option (A, B, C and D). The most answered option determines the follow-up.</p>

<h4>1. Create questions</h4>
<p>First we have added our multiple choice questions, in this case five of them. Each of them has 4 possible answers (A, B, C and D).</p>

<h4>2. Add calculators</h4>
<p>For each of the answer options we add a separate calculator block. Inside each calculator block we add up 5 operations, each to score 1 of our 5 questions. In the calculator block for answer A, we give each A answer a score of 1; the rest of the options a score of 0.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/compare-answers.png" width="1200" height="760" alt="Screenshot of a calculator in Tripetto" loading="lazy" />
  <figcaption>A calculator block counting the amount of selected A's.</figcaption>
</figure>
<p>Do the same for the other calculator blocks, with the corresponding answer option and scoring. You now have 4 calculator blocks that count the amount of selected A's, B's, C's and D's.</p>

<h4>3. Add branches</h4>
<p>Start with adding the first branch to check if answer A is the most selected. Set the branch behavior to <code>When all conditions match</code>.</p>
<p>Then add the following branch conditions:</p>
<ul>
  <li><code>Calculator A</code> is higher than <code>Calculator B</code> AND</li>
  <li><code>Calculator A</code> is higher than <code>Calculator C</code> AND</li>
  <li><code>Calculator A</code> is higher than <code>Calculator D</code>.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/compare-answers-branch.png" width="1200" height="760" alt="Screenshot of a comparison branch in Tripetto" loading="lazy" />
  <figcaption>A branch that compares the amount of selected A's with other answers.</figcaption>
</figure>
<p>Repeat this branch setup for the other options. You now have 4 branches, each to check if that option is more selected than the others.</p>
<blockquote>
  <h4>💡 Tip: Check identical outcomes</h4>
  <p>In the described setup, it is possible that multiple calculators have the same highest outcome. In that case none of the branches will be executed. Please take that into account while configuring your scores and/or branches.</p>
</blockquote>

<h4>4. Add follow-up</h4>
<p>Inside each branch you can now add the follow-up in case that option is the most selected.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/compare-answers-branches.png" width="1200" height="760" alt="Screenshot of comparison branches in Tripetto" loading="lazy" />
  <figcaption>All branches that check which answer is selected most.</figcaption>
</figure>
<hr />

<h2>More information</h2>
<p>The calculator block has lots of features, so we have several ways to learn all about it.</p>
<h3>Help center</h3>
<p>Our help articles help you out on all different aspects of the calculator:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/">How to add instant scores and calculations inside question blocks</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">How to use the calculator block</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-operations-in-the-calculator-block/">How to use operations in the calculator block (add, subtract, multiply, divide, equal)</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-given-answers-from-respondents-in-your-calculations/">How to use given answers from respondents in your calculations</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-scores-in-your-calculations/">How to use scores in your calculations</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-comparisons-in-your-calculations/">How to use comparisons in your calculations</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-functions-and-constants-in-your-calculations/">How to use functions and constants in your calculations</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-subcalculations-multistep-formulas-in-your-calculations/">How to use subcalculations (multistep formulas) in your calculations</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-text-counters-in-your-calculations/">How to use text counters in your calculations</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-calculations-with-logic-branches/">How to use calculations with logic branches</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-outcomes-of-calculator-blocks/">How to use the outcomes of calculator blocks</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-compare-the-outcomes-of-calculator-blocks/">How to compare the outcomes of calculator blocks</a><span class="related-current">Current article</span></li>
</ul>
<h3>Overviews</h3>
<p>We also made some overviews of the capabilities that the calculator block provides:</p>
<div>
  <a href="{{ page.base }}calculator/" class="blocklink">
    <div>
      <span class="title">No-code calculations with the calculator block</span>
      <span class="description">Make quizzes, order forms, exams and more with no-code calculations. All without any coding in Tripetto's calculator block.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" loading="lazy" />
    </div>
  </a>
</div>
<div>
  <a href="{{ page.base }}calculator-features/" class="blocklink">
    <div>
      <span class="title">All calculator features</span>
      <span class="description">A complete overview of all features the calculator block has to offer, including operations, scores, comparisons, functions and constants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" loading="lazy" />
    </div>
  </a>
</div>
