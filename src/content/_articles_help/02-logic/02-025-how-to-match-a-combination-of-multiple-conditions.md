---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-match-a-combination-of-multiple-conditions/
title: Match a combination of multiple conditions - Tripetto Help Center
description: Learn how you can combine multiple conditions to create perfect matches for your branches.
article_title: How to match a combination of multiple conditions
article_folder: editor-branch-all
article_video: logic-branch-behaviors
author: jurgen
time: 3
time_video: 2
category_id: logic
subcategory: logic_branch_behavior
areas: [studio, wordpress]
---
<p>Learn how you can combine multiple conditions to create perfect matches for your branches.</p>

<h2 id="when-to-use">When to use</h2>
<p>We've seen a simple example of a follow up that was in a branch with a single condition in <a href="{{ page.base }}help/articles/how-to-set-a-follow-up-based-on-given-answers/">this article</a>. But instead of one condition per branch it is possible to add as many conditions as you need to make the perfect match for your branch.</p>
<figure>
  <img src="{{ page.base }}images/help/editor-branch-all/demo.gif" width="1200" height="760" alt="Screenshot of a form in Tripetto" loading="lazy" />
  <figcaption>Demonstration of multiple matching conditions.</figcaption>
</figure>
<blockquote>
  <h4>📌 Also see: More logic possibilities</h4>
  <p>The logic described in this article is just one example of what Tripetto can do to make your forms smart. Have a look at this article to see all logic capabilities:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/">Discover the power of branches for your logic</a></li>
  </ul>
</blockquote>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>For this example we already added a couple of question blocks and created an empty branch.</p>

<h3 id="branch-conditions">Add branch conditions</h3>
<p>In each branch you can add an unlimited amount of branch conditions, by clicking the <code><i class="fas fa-plus"></i></code> button at the bottom of a branch. You can always use and combine <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">all different types of branch conditions</a>.</p>

<h3 id="branch-behavior">Set branch behavior</h3>
<p>Each branch can behave in three different ways (<a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-behavior-for-your-logic/">more information</a>):
<ul>
  <li>For the <i>first</i> condition match (logical <code>OR</code>);</li>
  <li>When <i>all</i> conditions match (logical <code>AND</code>);</li>
  <li>When <i>no</i> conditions match (logical <code>NOT</code>);</li>
  <li>For <i>each</i> condition match (logical <code>FOR</code>).</li>
</ul>
<p>By default a branch gets executed on the <i>first</i> condition match, so if one of the conditions match. In this article we want to check if <i>all</i> conditions match. To do so, we click the branch bubble at the top and set the branch behavior to <code>When all conditions match</code>. Now the branch will only be followed if the respondent matches all conditions.</p>
<p>In the example below we created a branch with three conditions. All three conditions have to match to go into the branch. In this example that branch approves the appointment and sends a confirmation message.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-behavior.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Set the behavior to match all branch conditions.</figcaption>
</figure>
<hr />

<h2>More video tutorials</h2>
<p>You can have a look at <a href="{{ page.base }}help/articles/">all our help articles</a> for more in-depth (video) tutorials to get the most out of Tripetto. Or have a look at our <a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank" rel="noopener noreferrer">YouTube channel</a> for all videos.</p>
<div>
  <a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank" rel="noopener noreferrer" class="blocklink">
    <div>
      <span class="title">Tripetto YouTube channel<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Visit our YouTube channel with tutorial videos.</span>
      <span class="url">youtube.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-youtube.svg" width="160" height="160" alt="YouTube logo" loading="lazy" />
    </div>
  </a>
</div>
