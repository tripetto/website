---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-add-one-or-multiple-closing-messages/
title: Add one or multiple closing messages - Tripetto Help Center
description: Learn how to make your closing messages smart by adding flexible closing messages.
article_title: How to add one or multiple closing messages
article_id: closings
article_folder: editor-end
article_video: logic-flexible-messages
author: jurgen
time: 4
time_video: 1
category_id: logic
subcategory: logic_branch_endings
areas: [studio, wordpress]
---
<p>Learn how to make your closing messages smart by adding flexible closing messages.</p>

<h2 id="when-to-use">When to use</h2>
<p>Every Tripetto form has the <a href="{{ page.base }}help/articles/how-to-add-a-closing-message/">common closing message</a>, but that message will be the same for every respondent that completes your form. Tripetto of course is a bit smarter than that, so you can add unlimited flexible closing messages, based on the given answers of each respondent. In that way you can differentiate the closing message for each possible form outcome.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/demo.gif" width="1200" height="700" alt="Screenshot of a closing message in Tripetto" loading="lazy" />
  <figcaption>Demonstration of a closing message.<br/><a href="https://unsplash.com/photos/IPx7J1n_xUc" target="_blank" rel="noopener noreferrer">Photo credits Unsplash</a>.</figcaption>
</figure>
<blockquote>
  <h4>📌 Also see: More logic possibilities</h4>
  <p>The logic described in this article is just one example of what Tripetto can do to make your forms smart. Have a look at this article to see all logic capabilities:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/">Discover the power of branches for your logic</a></li>
  </ul>
</blockquote>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>Flexible closing messages are tied to the <strong>branch endings</strong> you have in your form. In the form builder you <a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/">create branches</a> for each decision the form has to make.</p>

<h3 id="branch-endings">Branch endings</h3>
<p>For each branch you can determine what should happen at <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-endings-for-your-logic/">the end of that branch</a>. By default a new branch will continue to the upcoming section/question after that branch is completed by the respondent. In the form builder this is presented by a green bubble with a <code><i class="fas fa-arrow-down"></i></code> icon at the end of the branch.</p>

<h3 id="custom-closing">Custom closing message</h3>
<p>You can change the ending behavior of each branch by clicking the bubble at the end of the desired branch. You can then choose from two options to end the form:</p>
<ul>
  <li><strong>Jump to end</strong> - Jump to the end of the form and show the common closing message that's entered at the very end of the form. If you select this option the green end bubble will change into a red bubble with a <code><i class="fas fa-sign-out-alt"></i></code> icon;</li>
  <li><strong>End with closing message</strong> - Finish the form, but use a custom closing message for respondents that completed the form in this branch. If you select this option the green end bubble will change into a red bubble with a <code><i class="fas fa-comment"></i></code> icon. Now you can <a href="{{ page.base }}help/articles/how-to-add-a-closing-message/">enter a custom closing message</a> for this particular branch end. It's also possible to setup a <a href="{{ page.base }}help/articles/how-to-redirect-to-a-url-at-form-completion/">redirect after form completion</a>.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-custom-end.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>You can enable custom end screens based on your respondents answers.</figcaption>
</figure>
<hr/>

<h2>More information</h2>
<p>For more information about how to setup a closing message or a redirect, please see the following help articles:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-add-a-closing-message/">How to add a closing message</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-redirect-to-a-url-at-form-completion/">How to redirect to a URL at form completion</a></li>
</ul>
