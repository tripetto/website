---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-send-email-block/
title: Use the send email block - Tripetto Help Center
description: You can let your Tripetto form send email messages to yourself or your respondents. And even use form data inside those emails.
article_title: How to use the send email block
article_id: action-blocks
article_folder: editor-block-send-email
author: mark
time: 5
category_id: logic
subcategory: logic_email_actions
areas: [studio, wordpress]
redirect_from:
- /help/articles/how-to-use-a-mailer-block/
---
<p>You can let your Tripetto form send email messages to yourself or your respondents. And even use form data inside those emails.</p>

<h2 id="when-to-use">When to use</h2>
<p>The send email block enables you to send emails from within your forms. You can use the send email block for a notification mail to yourself, but also for sending a confimation mail to your respondent that filled out his email address in your form.</p>
<p>Each send email block enables you to send an email to one recipient. Need to send more emails? Then simply add multiple send email blocks to your form structure.</p>
<blockquote class="help_article_wp_only">
  <h4>📌 Also see: Email settings (WordPress only)</h4>
  <p>In the Tripetto WordPress plugin you can configure the sender name and sender address. More information:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-configure-plugin-settings-for-email-sending-and-spam-protection/">How to configure plugin settings for email sending and spam protection</a></li>
  </ul>
</blockquote>
<blockquote class="help_article_wp_only">
  <h4>📌 Also see: Email issues (WordPress only)</h4>
  <p>If you're experiencing mailing issues in the WordPress plugin, please have a look at this article for some troubleshooting:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/troubleshooting-email-notifications-from-wordpress-not-sent-or-marked-as-spam/">Email notifications from WordPress not sent or marked as spam</a></li>
  </ul>
</blockquote>

<h3>When is the email sent?</h3>
<p>Emails from your Tripetto form only get sent <i>after</i> the form is completed. So the emails are not sent immediately when someone passes a mailer block in your form structure, but only after the respondent clicked the Submit button.</p>
<p>Also, keep in mind that the position of the send email block in your form structure determines if the email is sent or not. For example, if you place the mailer block inside a branch that checks if a certain answer option is selected, then the email will only be sent if a respondent has passed that specific branch in your form.</p>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>The send email block is available as a question type. Add a new block to your form and then select the question type <code>Send email</code>. You can add it in any given position and in an unlimited amount inside your forms.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/menu.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Send email blocks are available from the Question type menu.</figcaption>
</figure>

<h3 id="recipient" data-anchor="Recipient">Determine recipient address</h3>
<p>You can email yourself, or the respondent (if you know the email address of the respondent):</p>
<ul>
  <li>Select <code>Fixed recipient address</code> - You will use this setting to mail to the same person each time a form is completed. For example to get notified when someone completes the form. Just enter an email address you'd like to receive the mail on;</li>
  <li>Select from <code>Available email addresses</code> - You can also email to an email address that your respondent enters inside your form. For example to send a confirmation mail or thank you mail. <a href="{{ page.base }}help/articles/how-to-send-a-confirmation-mail-to-your-respondents/">Click here for more information on how to set that up.</a></li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-recipient.png" width="476" height="301" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Example of using an answer as a recipient.</figcaption>
</figure>

<h3 id="message" data-anchor="Email message">Determine email message</h3>
<p>Next step you can enter the email message:</p>
<ul>
  <li>Enter <code>Subject</code> - The subject line of the email;</li>
  <li>Enter <code>Message</code> - The actual content of the email message.</li>
</ul>

<h4>Use form data in your email</h4>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/03-message.png" width="477" height="330" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Example of selecting a piping value in an email message.</figcaption>
</figure>
<p>If you want to include a copy of all given answers, you can activate the feature <code>Form data</code> (see below).</p>
<p>If you only want to use certain given answers from each entry inside your mails you can use piping logic. These piping values are available in both the subject and the message of an email. In that way you can for example address your recipient personally in the salutation of the email if you asked for the name in your form.</p>
<p>To do so, you can use piping values by typing the <code>@</code> sign and then select the desired question you want to show the answer of. More information can be found in <a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/">this article about piping logic</a>.</p>

<h3 id="settings">Settings</h3>
<p>The following advanced settings are available for the mailer block:</p>
<ul>
  <li>
    <h4>Sender address</h4>
    <p>You can enter a <code>Sender address</code> that will be used as a <strong>reply-to address</strong> in your email message (the mails will still be sent by the original sender address). Useful for example when you send an email from within the form to the respondent and the respondent wants to reply to that email. The reply message will then be addressed to the sender address you entered.</p>
    <p>To do so, enable the <code>Sender</code> feature and then select the sender address:</p>
    <ul>
      <li>Select <code>Fixed sender address</code> - You will use this setting to set a sender address that's the same for every mail sent. For example the support email address of your company so respondents can mail the support department afterwards;</li>
      <li>Select from <code>Available email addresses</code> - You can also email on behalf of an email address that your respondent enters inside your form. To use this feature, please provide a question block that can contain an email address. In Tripetto this can be a block with question type 'Email address', or a '<a href="{{ page.base }}help/articles/how-to-use-the-hidden-field-block/">Hidden field</a>' block. Make sure this block is entered <i>before</i> the mailer block.<br/>After you've set up a block that contains the email address, you can select this particular block inside the mailer block as a sender address. The form will now use the given email address as the sender address (reply-to).<br/></li>
    </ul>
  </li>
  <li>
    <h4>Form data</h4>
    <p>You can choose to <code>Append all form data</code> to the message. This will append all exportable data in the dataset of the form to the message. Useful, for example, if you want to send a copy of the form data to the respondent.</p>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/04-settings.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Advanced settings of the mailer block.</figcaption>
</figure>

<h3 id="features-options">Options</h3>
<ul>
  <li><strong>Status</strong><br/>By default this block will be executed in your form. Enable the <code>Status</code> feature to disable the execution.</li>
  <li><strong>Alias</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about alias"><i class="fas fa-info-circle"></i></a><br/>By default the name/title of this block will be used in your dataset. Enable the <code>Alias</code> feature to use a different name in your dataset.</li>
  <li><strong>Exportability</strong><a href="{{ page.base }}help/articles/how-to-determine-what-data-fields-get-saved/" class="article-more-info" title="More information about exportability"><i class="fas fa-info-circle"></i></a><br/>By default the data of send email blocks (recipient address, subject, message and optionally sender address) isn't saved to the dataset of the form entries.<br/>If you wish to include any of this mailer data in your dataset anyway, you can overrule the default setting by enabling the <code>Exportability</code> feature and activate the desired include option(s):
    <ul>
      <li><code>Include recipient address in the dataset</code>;</li>
      <li><code>Include subject in the dataset</code>;</li>
      <li><code>Include message in the dataset</code>;</li>
      <li><code>Include sender address in the dataset</code> (only available when the sender address is enabled).</li>
    </ul>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/05-exportability.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Exportability settings of the mailer block.</figcaption>
</figure>
<hr />

<h2 id="logic">Logic<a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" class="article-more-info" title="More information about logic"><i class="fas fa-info-circle"></i></a></h2>
<p>Logic is important to make your forms smart and conversational. The send email block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Mail will be send;</li>
  <li>No mail will be send.</li>
</ul>
<h3 id="evaluate-conditions">Evaluate conditions</h3>
<p>Evaluate conditions can work separately for the <code>Recipient</code>, <code>Subject</code>, <code>Message</code> and <code>Sender</code> of the send email block:</p>
<ul>
  <li>Text matches <code>your filter</code>;</li>
  <li>Text does not match <code>your filter</code>;</li>
  <li>Text contains <code>your filter</code>;</li>
  <li>Text does not contain <code>your filter</code>;</li>
  <li>Text starts with <code>your filter</code>;</li>
  <li>Text ends with <code>your filter</code>;</li>
  <li>Text is empty;</li>
  <li>Text is not empty.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter(s)</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Text - Compare with a fixed text that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers/">more info</a>).</li>
</ul>
<hr />

<h2 id="calculations">Calculations<a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" class="article-more-info" title="More information about calculations"><i class="fas fa-info-circle"></i></a></h2>
<p>You can use the <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">calculator block</a> to perform calculations with given answers. The send email block supports the following calculation operations for the <code>Recipient</code>, <code>Subject</code>, <code>Message</code> and <code>Sender</code>:</p>
<ul>
  <li>Compare - Compare a text and output a value based on the result of the comparison;</li>
  <li>Character count - Count the number of characters in a text;</li>
  <li>Word count - Count the number of words in a text;</li>
  <li>Line count - Count the number of lines in a text;</li>
  <li>Count occurrences - Count the number of occurrences of a certain text, character or regular expression in a text;</li>
  <li>Convert to number - Convert a text to a number value.</li>
</ul>
<hr />

<h2>Also read our blog</h2>
<p>We also wrote a blog post with some more background information, use cases and working examples of mailer blocks.</p>
<div>
  <a href="{{ page.base }}blog/your-form-is-now-a-mail-agent/" class="blocklink">
    <div>
      <span class="title">Your form is now a mail agent - Tripetto Blog</span>
      <span class="description">Introducing a new action block to send mails right from your form. Opening new possibilities for even smarter forms and surveys.</span>
      <span class="url">tripetto.com/blog</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" loading="lazy" />
    </div>
  </a>
</div>
<hr />

{% include help-article-blocks.html %}

