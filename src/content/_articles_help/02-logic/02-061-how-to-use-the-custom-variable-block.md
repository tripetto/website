---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-custom-variable-block/
title: Use the custom variable block - Tripetto Help Center
description: Use the custom variable block for more advanced use cases where you want to store a certain value in a variable and use that throughout your form.
article_title: How to use the custom variable block
article_id: action-blocks
article_folder: editor-block-custom-variable
author: mark
time: 5
category_id: logic
subcategory: logic_data_actions
areas: [studio, wordpress]
---
<p>Use the custom variable block for more advanced use cases where you want to store a certain value (text, number, date or boolean) in a variable and use that throughout your form.</p>

<h2 id="when-to-use">When to use</h2>
<p>The custom variable block is an advanced action block that's primarily designed for no-code purposes. You can use it to store different types of values and use those values inside your form.</p>
<p>In combination with other blocks (like the <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">calculator block</a> and the <a href="{{ page.base }}help/articles/how-to-use-the-set-value-block/">set value</a> block) and all logic features in Tripetto it enables you to do all kinds of no-code actions, for example:</p>
<ul>
  <li>Store a string value and use that in several places in your form;</li>
  <li>Store a numeric value and increase the value of it when a certain answer is given;</li>
  <li>Store a date value to check if that date is in the future in several places in your form;</li>
  <li>Store a boolean value and determine the closing message based on the boolean value.</li>
</ul>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>The custom variable block is available as a question type. Add a new block to your form and then select the question type <code>Custom variable</code>. You can add it in any given position and in an unlimited amount inside your forms.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/menu.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>The custom variable block is available from the Question type menu.</figcaption>
</figure>

<h3 id="features-general">Name</h3>
<p>You can give your custom variable block a <code>Name</code> so you can recognize it in your form's structure and dataset. This name is not visible for your respondents.</p>


<h3 id="features-type">Type of variable</h3>
<p>The <code>Type of variable</code> determines the type of data that you want to store in the variable. The following types are available:</p>
<ul>
  <li>A textual value;</li>
  <li>A numeric value;</li>
  <li>A date (and time) value;</li>
  <li>A boolean value (<code>true/false</code>).</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/settings.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Example of a custom variable to store a numeric value.</figcaption>
</figure>

<h3 id="features-settings">Settings</h3>
<p>Depending on the selected type of variable, additional settings can become activated to help you setup the variable. These features will popup on the left side of the pane. You can enable and configure these to your own needs.</p>

<h3 id="features-options">Options</h3>
<p>Depending on the selected type of variable, additional options can become activated to help you setup the variable. These features will popup on the left side of the pane. You can enable and configure these to your own needs.</p>
<p>In general the following options are available:</p>
<ul>
  <li><strong>Prefill</strong><a href="{{ page.base }}help/articles/how-to-prefill-question-blocks-with-initial-values/" class="article-more-info" title="More information about prefilling"><i class="fas fa-info-circle"></i></a><br/>Enable the <code>Prefill</code> feature to set a fixed initial value of this block.</li>
  <li><strong>Alias</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about alias"><i class="fas fa-info-circle"></i></a><br/>By default the name/title of this block will be used in your dataset. Enable the <code>Alias</code> feature to use a different name in your dataset.</li>
  <li><strong>Exportability</strong><a href="{{ page.base }}help/articles/how-to-determine-what-data-fields-get-saved/" class="article-more-info" title="More information about exportability"><i class="fas fa-info-circle"></i></a><br/>By default the collected data of this block will be stored to the dataset of each result. Enable the <code>Exportability</code> feature to take control over the storage. You can then include/exclude the data from your dataset. Please note that if you exclude data from the dataset you can never see and/or use the given answers from your respondents to this block, because the data is simply not stored at all.</li>
</ul>
<hr/>

<h2 id="piping-logic">Show variables<a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/" class="article-more-info" title="More information about piping logic"><i class="fas fa-info-circle"></i></a></h2>
<p>You can show the value of the variable to your respondents, by using our piping logic. This works the same way as you can use 'normal' question blocks as piping values, by typing the <code>@</code> sign at the desired position and select the custom variable.</p>
<hr/>

<h2 id="branch-logic">Branch logic<a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" class="article-more-info" title="More information about logic"><i class="fas fa-info-circle"></i></a></h2>
<p>You can also use the value of the variable as branch conditions, so the form can take decisions based on the variable. This works the same way as you can add logic based on 'normal' question blocks, using branch logic.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/logic.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Use a variable to execute logic.</figcaption>
</figure>

<p>Due to the flexibility of the custom variable block, the value of it can contain several types. It depends on the type of value that is saved inside the variable, which <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">branch conditions</a> you can create with it. Below you see the condition options per variable type.</p>
<h3 id="text-conditions">Text conditions</h3>
<ul>
  <li>Text matches <code>your filter</code>;</li>
  <li>Text does not match <code>your filter</code>;</li>
  <li>Text contains <code>your filter</code>;</li>
  <li>Text does not contain <code>your filter</code>;</li>
  <li>Text starts with <code>your filter</code>;</li>
  <li>Text ends with <code>your filter</code>;</li>
  <li>Text is empty;</li>
  <li>Text is not empty.</li>
</ul>
<h3 id="number-conditions">Number conditions</h3>
<ul>
  <li>Number is equal to <code>your filter</code>;</li>
  <li>Number is not equal to <code>your filter</code>;</li>
  <li>Number is lower than<code>your filter</code>;</li>
  <li>Number is higher than <code>your filter</code>;</li>
  <li>Number is between <code>your filters</code>;</li>
  <li>Number is not between <code>your filters</code>;</li>
  <li>Number is empty;</li>
  <li>Number is not empty.</li>
</ul>
<h3 id="date-conditions">Date conditions</h3>
<ul>
  <li>Date is equal to <code>your filter</code>;</li>
  <li>Date is not equal to <code>your filter</code>;</li>
  <li>Date is before <code>your filter</code>;</li>
  <li>Date is after <code>your filter</code>;</li>
  <li>Date is between <code>your filters</code>;</li>
  <li>Date is not between <code>your filters</code>;</li>
  <li>Date is empty;</li>
  <li>Date is not empty.</li>
</ul>
<h3 id="boolean-conditions">Boolean conditions</h3>
<ul>
  <li>Value is true;</li>
  <li>Value is not false;</li>
  <li>Value is equal to <code>your filter</code>;</li>
  <li>Value is not equal to <code>your filter</code>;</li>
  <li>Value is empty;</li>
  <li>Value is not empty.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Fixed value - Compare with a fixed value that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers/">more info</a>).</li>
</ul>
<hr />

<h2 id="calculations">Calculations<a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" class="article-more-info" title="More information about calculations"><i class="fas fa-info-circle"></i></a></h2>
<p>You can use the <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">calculator block</a> to perform calculations with custom variables. Due to the flexibility of the custom variable block, the value of it can contain several types. It depends on the type of value that is saved inside the variable, which calculation operations are available in the calculator block.</p>
<hr />
{% include help-article-blocks.html %}
