---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-checkboxes-block/
title: Use the checkboxes block - Tripetto Help Center
description: Learn everything you need to know to use the checkboxes block in your forms.
article_title: How to use the checkboxes block
article_folder: editor-block-checkboxes
author: jurgen
time: 4
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-checkboxes-block/
---
<p>Learn everything you need to know to use the checkboxes block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the checkboxes block to let your respondents select multiple items from a list of checkbox choices you provide them.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/checkboxes.gif" width="1200" height="700" alt="Screenshot of checkboxes in Tripetto" loading="lazy" />
  <figcaption>Demonstration of a checkboxes block.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Checkboxes</code>. You can now customize this block to your needs with the following features:</p>

<h3 id="features-general">General</h3>
<ul>
  <li><strong>Text</strong><br/>Use the <code>Text</code> feature for the name/title of this block.</li>
  <li><strong>Description</strong><br/>Enable the <code>Description</code> feature to add a description to this block.</li>
  <li><strong>Help text</strong><br/>Enable the <code>Help text</code> feature to add a help text to this block.</li>
</ul>

<h3 id="features-settings">Settings</h3>
<ul>
  <li><strong>Limits</strong><br/>Enable the <code>Limits</code> feature to enter a minimum and maximum number of choices that respondents can select.</li>
  <li><strong>Randomization</strong><br/>Enable the <code>Randomization</code> feature to present the choices in a random order to your respondents (using <a href="https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle" target="_blank" rel="noopener noreferrer">Fisher–Yates shuffle</a>).</li>
</ul>

<h3 id="features-options">Options</h3>
<ul>
  <li><strong>Required</strong><br/>By default this block is not required to fill out by your respondents. Enable the <code>Required</code> feature to make this block required to fill out.</li>
  <li><strong>Visibility</strong><br/>By default this block is visible in your form. Enable the <code>Visibility</code> feature to hide it for your respondents.</li>
  <li><strong>Labels</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about labels"><i class="fas fa-info-circle"></i></a><br/>By default the choices will be marked as <code>Checked</code> or <code>Not checked</code> in your dataset (your results). Enable the <code>Labels</code> feature to overwrite these labels in your dataset with your own values.</li>
  <li><strong>Score</strong><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/" class="article-more-info" title="More information about scores"><i class="fas fa-info-circle"></i></a><br/>You can attach scores to the choices to perform instant calculations. Enable the <code>Score</code> feature to enter the desired score values.</li>
  <li><strong>Alias</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about alias"><i class="fas fa-info-circle"></i></a><br/>By default the name/title of this block will be used in your dataset. Enable the <code>Alias</code> feature to use a different name in your dataset.</li>
  <li><strong>Data format</strong><br/>By default the collected data of this block is stored as a separate value per available checkbox. Enable the <code>Data format</code> feature to determine how the data is stored in the dataset:
    <ul>
      <li><strong>Every checkbox as a separate field</strong> - Every checkbox is included in the dataset as a separate value (<code>Checked</code> or <code>Not checked</code>).</li>
      <li><strong>Text field with a list of selected checkboxes</strong> - All the selected checkboxes are concatenated to a single string of text, separated using a configurable separator.</li>
      <li><strong>Both</strong> - Includes every checkbox in the dataset, together with the concatenated text.</li>
    </ul>
  </li>
  <li><strong>Exportability</strong><a href="{{ page.base }}help/articles/how-to-determine-what-data-fields-get-saved/" class="article-more-info" title="More information about exportability"><i class="fas fa-info-circle"></i></a><br/>By default the collected data of this block will be stored to the dataset of each result. Enable the <code>Exportability</code> feature to take control over the storage. You can then include/exclude the data from your dataset. Please note that if you exclude data from the dataset you can never see and/or use the given answers from your respondents to this block, because the data is simply not stored at all.</li>
</ul>

<h3 id="choices">Choices</h3>
<p>Next, you can enter the list of choices that you want to show to your respondents:</p>
<ul>
  <li><strong>Add choices one by one</strong><br/>Click the <code><i class="fas fa-plus"></i></code> icon at the bottom of the list and enter the choice name.</li>
  <li><strong>Import a list of choices</strong><br/>Click the <code><i class="fas fa-download"></i></code> icon at the top of the list. Now supply a list with one choice name per text line and click <code>Import</code> to add them all at once to your list of choices.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/import.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Importing choices in the checkboxes block.</figcaption>
</figure>
<p>From your list of choices you can open each choice to get to the settings of that single choice. Over there you can use these features:</p>
<ul>
  <li><strong>Name</strong><br/>Use the <code>Name</code> feature for the name/title of this choice.</li>
  <li><strong>Description</strong><br/>Enable the <code>Description</code> feature to extend the choice name with a description, for example to show an extra explanation. This description is shown below the name.</li>
  <li><strong>Exclusivity</strong><br/>Enable the <code>Exclusivity</code> feature to make that choice exclusive. If a respondent selects that choice, all other choices will be unselected, making the selected choice exclusive.</li>
  <li><strong>Labels</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about labels"><i class="fas fa-info-circle"></i></a><br/>By default each choice will be marked as <code>Checked</code> or <code>Not checked</code> in your dataset (your results). Enable the <code>Labels</code> feature to overwrite these labels in your dataset with your own values for each choice individually.</li>
  <li><strong>Identifier</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about identifiers"><i class="fas fa-info-circle"></i></a><br/>By default the name of each choice will be used in your dataset. Enable the <code>Identifier</code> feature to use a different name in your dataset.</li>
  <li><strong>Score</strong><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/" class="article-more-info" title="More information about scores"><i class="fas fa-info-circle"></i></a><br/>Enable the <code>Score</code> feature to enter the desired score value for that choice.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-checkboxes.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Advanced options of the choices of the checkboxes block.</figcaption>
</figure>
<hr />

<h2 id="logic">Logic<a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" class="article-more-info" title="More information about logic"><i class="fas fa-info-circle"></i></a></h2>
<p>Logic is important to make your forms smart and conversational. The checkboxes block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Checkbox state (checked/unchecked per option);</li>
  <li>All checkboxes unchecked.</li>
</ul>
<h3 id="evaluate-conditions-block">Evaluate conditions</h3>
<p>Conditions for whole block:</p>
<ul>
  <li>Value matches <code>your filter</code>;</li>
  <li>Value does not match <code>your filter</code>;</li>
  <li>Value contains <code>your filter</code>;</li>
  <li>Value does not contain <code>your filter</code>;</li>
  <li>Value starts with <code>your filter</code>;</li>
  <li>Value ends with <code>your filter</code>;</li>
  <li>Value is empty;</li>
  <li>Value is not empty.</li>
</ul>
<p>Conditions for each option:</p>
<ul>
  <li>Option is true;</li>
  <li>Option is false;</li>
  <li>Option equals <code>your filter</code>;</li>
  <li>Option not equals <code>your filter</code>;</li>
  <li>Option is empty;</li>
  <li>Option is not empty.</li>
</ul>
<h3 id="counter-conditions">Counter conditions</h3>
<p>Conditions based on the number of selected options:</p>
<ul>
  <li>Counter is equal to <code>your filter</code>;</li>
  <li>Counter is not equal to <code>your filter</code>;</li>
  <li>Counter is lower than<code>your filter</code>;</li>
  <li>Counter is higher than <code>your filter</code>;</li>
  <li>Counter is between <code>your filters</code>;</li>
  <li>Counter is not between <code>your filters</code>.</li>
</ul>
<h3 id="score-conditions">Score conditions</h3>
<p>Conditions based on the total score of the selected options:</p>
<ul>
  <li>Score is equal to <code>your filter</code>;</li>
  <li>Score is not equal to <code>your filter</code>;</li>
  <li>Score is lower than<code>your filter</code>;</li>
  <li>Score is higher than <code>your filter</code>;</li>
  <li>Score is between <code>your filters</code>;</li>
  <li>Score is not between <code>your filters</code>;</li>
  <li>Score is calculated;</li>
  <li>Score is not calculated.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Text - Compare with a fixed text that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers/">more info</a>).</li>
</ul>
<hr />

<h2 id="calculations">Calculations<a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" class="article-more-info" title="More information about calculations"><i class="fas fa-info-circle"></i></a></h2>
<p>You can use the <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">calculator block</a> to perform calculations with given answers. The checkboxes block supports the following calculation operations:</p>
<ul>
  <li>Score - Calculate a score based on the entered score list for choices;</li>
  <li>Count - Count the number of selected choices;</li>
  <li>Choice value - Output a value based on an individual selected/not-selected choice (boolean).</li>
</ul>
<hr />

{% include help-article-blocks.html %}
