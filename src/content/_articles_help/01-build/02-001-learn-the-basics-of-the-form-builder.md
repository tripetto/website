---
layout: help-article
base: ../../../
permalink: /help/articles/learn-the-basic-controls-to-use-the-form-builder/
title: Basic controls to use the form builder - Tripetto Help Center
description: Learn the basics and gestures to control the form builder.
article_title: Learn the basic controls to use the form builder
article_id: builder
article_video: build-builder
author: martijn
time: 2
time_video: 4
category_id: build
subcategory: build_basics
areas: [studio, wordpress]
redirect_from:
- /help/articles/learn-the-basics-of-the-form-builder/
---
<p>The Tripetto form builder is a bit different from existing tools, but you'll know why and love it almost instantly when you spend a minute getting the hang of it. Let us show you how to use it.</p>

<h2 id="concept" data-anchor="Concept">The concept</h2>
<p>Our form builder is not just a list of questions. As we want to make it easy to create smart forms with logic, we use a <strong>storyboard</strong>. This board gives a visual presentation of the flows inside your form, making it much better understandable what's happening in your form structure.</p>

<blockquote>
  <h4>📌 Also see: Build a simple form</h4>
  <p>In the article you're reading now we show how to control the storyboard. If you're looking for a video tutorial about building a simple form, please have a look at this article:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/">How to build your forms in the form builder</a></li>
  </ul>
</blockquote>

<h2 id="autosave">Autosave</h2>
<p>Everything you do in the form builder gets saved immediately (autosave), so you don't have to worry about saving your form project yourself. It's done automatically all the time.</p>

<h2 id="arrange">Arrange</h2>
<p>If you have multiple question blocks on your storyboard (and maybe some logic in it as well), you can arrange your question blocks, for example to change the order of two questions. You can do that by dragging and dropping a block.</p>
<p>Click and hold a specific block for a brief moment, so it sticks to your cursor. Then you can drag it to the right position by holding your mouse click. And drop it at the right position by releasing your mouse click.</p>
<figure>
  <div class="youtube-player" data-id="zhaHhlAzKNU"></div>
  <figcaption>Video tutorial about arranging form elements.</figcaption>
</figure>
<p>This arrange gesture is not only available for individual question blocks, but also for whole sections, branch conditions and even for whole branches, including everything that's inside that branch. That way you can easily arrange your whole form structure.</p>

<h2 id="navigate">Navigate</h2>
<p>There are a few controls you need to know to navigate the storyboard in Tripetto.</p>

<h3 id="move">Move</h3>
<p>You can move through your form (especially with large forms) by holding the storyboard and drag it into the direction you want.</p>
<p>You can also use your scroll wheel to navigate vertically through your form. If you scroll while holding <code>CTRL</code> key, you can navigate horizontally.</p>

<h3 id="zoom">Zoom</h3>
<p>If you have a large form with lots of branches, you can zoom out to see the whole structure of your form, or zoom in to see a particular part of your form. You can use the zoom bar for that.</p>
<p>You can also double click on a specific point in your form structure to zoom into that place directly.</p>
<p>You can also use your scroll wheel to zoom in/out. While holding <code>Shift</code> key, you can use the scroll wheel to zoom.</p>
<figure>
  <div class="youtube-player" data-id="n4ahGuFtuDg"></div>
  <figcaption>Video tutorial about navigating the storyboard.</figcaption>
</figure>
<h3 id="menu">Open menus</h3>
<p>Almost every element on your storyboard has a so-called 'context menu' to give you quick access to the possibilities of that element. To open such menus you can often click the <code><i class="fas fa-ellipsis-h"></i></code> icon, but you can also use the <code>right-click</code> of your mouse on any element on the storyboard to open that same context menu. This makes navigating and building your form even easier.</p>
<hr />

<h2>More video tutorials</h2>
<p>You can have a look at <a href="{{ page.base }}help/articles/">all our help articles</a> for more in-depth (video) tutorials to get the most out of Tripetto. Or have a look at our <a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank" rel="noopener noreferrer">YouTube channel</a> for all videos.</p>
<div>
  <a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank" rel="noopener noreferrer" class="blocklink">
    <div>
      <span class="title">Tripetto YouTube channel<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Visit our YouTube channel with tutorial videos.</span>
      <span class="url">youtube.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-youtube.svg" width="160" height="160" alt="YouTube logo" loading="lazy" />
    </div>
  </a>
</div>
