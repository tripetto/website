---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-add-a-closing-message/
title: Add a closing message - Tripetto Help Center
description: Learn how to alter the common closing message that's shown after form completion.
article_id: closing
article_title: How to add a closing message
article_folder: editor-end
article_video: build-closing-message
author: jurgen
time: 4
time_video: 2
category_id: build
subcategory: build_messages
areas: [studio, wordpress]
---
<p>Learn how to alter the common closing message that's shown after form completion.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the closing message to show your own message after your respondents have completed your form. For example to thank them or present them the result of the form they filled out.</p>

<h3 id="form-faces">Form faces</h3>
<p>Tripetto forms can be presented in different layouts, that we call <a href="{{ page.base }}form-layouts/">form faces</a>. It depends on the form face of your form how the closing message gets presented and behaves.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/demo.gif" width="1200" height="700" alt="Screenshot of a closing message in Tripetto" loading="lazy" />
  <figcaption>Demonstration of a closing message.<br/><a href="https://unsplash.com/photos/IPx7J1n_xUc" target="_blank" rel="noopener noreferrer">Photo credits Unsplash</a>.</figcaption>
</figure>
<blockquote>
  <h4>📌 Also see: Flexible closing messages</h4>
  <p>Closing messages are way more flexible than just one closing message for all your respondents. In this article we will show the common closing message, but you can also add unlimited flexible closing messages, based on the given answers of your respondents.</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-add-one-or-multiple-closing-messages/">How to add one or multiple closing messages</a></li>
  </ul>
</blockquote>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>To enable the closing message, open your form inside the form builder. Each form always has at least one ending of the form, represented by the red bubble with a <code><i class="fas fa-sign-out-alt"></i></code> icon at the bottom of the form. By clicking that bubble the pane to add/edit the closing message opens up.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-add.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>The closing message can be set from the red bubble.</figcaption>
</figure>

<h3 id="features">Features</h3>
<p>The closing message consists of some features that you can enable, just the way you like/need it. You can see those features on the left of the closing message pane. The following features are available:</p>
<ul>
  <li><strong>Text</strong> - Enable the <code>Text</code> feature to supply a title to the closing message.</li>
  <li><strong>Description</strong> - Enable the <code>Description</code> feature to supply a description to the closing message.</li>
  <li><strong>Image</strong> - Enable the <code>Image</code> feature to add an image to the closing message. The image must be hosted somewhere else so you can supply the URL to the image;</li>
  <li><strong>Video</strong> - Enable the <code>Video</code> feature to add a video to the closing message. We support YouTube and Vimeo videos. All you have to do is copy-paste the URL of the video from YouTube/Vimeo and the video will be embedded;</li>
  <li><strong>Button</strong> - Enable the <code>Button</code> feature to add a button to the closing message, with the following options:
    <ul>
      <li>Label - The text label shown inside the button;</li>
      <li>URL - The URL to open, including <code>https://</code>;</li>
      <li>Open in - Determine if the URL should be opened in the same browser window (form gets closed) or a new browser window (form stays opened in original browser window).</li>
    </ul>
  </li>
  <li><strong>Repeatability</strong> - Enable the <code>Repeatability</code> feature to determine if a form that is completed can be started over again immediately after completion;</li>
  <li><strong>Redirect</strong> - Enable the <code>Redirect</code> feature to automatically redirect your respondents to an external URL on completion. All of the above features will be disabled when the redirect is enabled. More information on redirecting after completion can be found in <a href="{{ page.base }}help/articles/how-to-redirect-to-a-url-at-form-completion/">this article</a>.</li>
</ul>

<h3 id="piping">Recall values</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/editor-end/02-identifier.gif" width="470" height="164" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Add the identification number of the entry in the redirect URL.</figcaption>
</figure>
<p>In the closing message you can use <a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/">piping logic</a> to show answers from your respondent in your closing message. To do that, simply type the <code>@</code> sign at the desired position in your closing message and select the block from your form that you want to recall the value from.</p>

<h4>Button URL and redirect URL</h4>
<p>You can also recall values in the button URL and <a href="{{ page.base }}help/articles/how-to-redirect-to-a-url-at-form-completion/">redirect URL</a>, which are available in the closing message pane. To do that, simply type the <code>@</code> sign at the desired position in your button URL/redirect URL and select the block from your form that you want to recall the value from.</p>
<blockquote>
  <h4>📣 Info: About query strings</h4>
  <p>A query string is an often used method to send data in a URL. Some background information on how to supply a query string:</p>
  <ul>
    <li>You can add multiple parameters to a query string;</li>
    <li>Each parameter consists of a name and a value, separated by a <code> = </code> sign, for example <code>name=value</code>;</li>
    <li>The first parameter is preceded by a <code> ? </code> sign;</li>
    <li>Any following parameters are preceded by a <code> & </code> sign;</li>
  </ul>
  <p>A full example could be something like this: <code>https://yoursite.com/?name=abc&city=klm&country=xyz</code>.</p>
</blockquote>

<h4>Identification number</h4>
<p>On top of the given answers from a submission, there is an extra piping value available in the closing message: the <code>Identification number</code>. This is the unique identification number of each entry by which you can identify/track a certain entry on the URL that's opened. This even enables you to <a href="{{ page.base }}help/articles/how-to-share-response-data-between-forms/">share response data between different Tripetto forms</a>.</p>
