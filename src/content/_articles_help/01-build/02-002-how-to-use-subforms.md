---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-subforms-in-the-form-builder/
title: How to use subforms in the form builder - Tripetto Help Center
description: Learn how you can use subforms to create forms-in-forms for improved organization and structuring of large forms.
article_title: How to use subforms in the form builder
article_id: subforms
article_folder: editor-subforms
author: mark
time: 3
category_id: build
subcategory: build_basics
areas: [studio, wordpress]
---
<p>Learn how you can use subforms to create forms-in-forms for improved organization and structuring of large forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>A subform is a part of your form that is bundled in one block on the storyboard of the form builder (form-in-form). You can open such a subform on its own storyboard.</p>
<p>It helps to make your form structure more organized, because you can extract parts of your form into a subform, instead of showing your whole form structure in one large storyboard. This is especially handy when you have a large form structure, which can become difficult to oversee on one storyboard.</p>
<p>Your respondents won't notice any difference while filling out your form. Subforms are just a tool for you in the form builder to make your form structure more organized.</p>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>On the storyboard of the form builder you can insert a subform via every square <code><i class="fas fa-plus"></i></code> icon. The default click inserts a section, but you can also right-click the icon (or holding it with your finger on touch-enabled devices) and select <code>Insert form</code>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/add-subform.png" width="1200" height="760" alt="Screenshot of a subform menu in Tripetto" loading="lazy" />
  <figcaption>Use the menu to insert a subform.</figcaption>
</figure>

<h3 id="subform">Subform</h3>
<p>A subform will now be added to the storyboard and the form builder will open that new subform in a new storyboard right away. You can now give this subform a name, so you can recognize this subform later on in your parent storyboard. You can always edit the name by clicking the subform name in the top menu bar.</p>
<p>In your subform you now see an empty storyboard. The first blue bubble with the <code><i class="fas fa-sign-in-alt"></i></code> icon indicates the entrance of this subform (from the previous form level). The second blue bubble with the <code><i class="fas fa-sign-out-alt fa-flip-horizontal"></i></code> icon indicates the exit of this subform (back to the previous form level). In between these bubbles you can build the part of your form, with all form building features available. You can even add unlimited levels of subforms inside subforms.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/subform.png" width="1200" height="760" alt="Screenshot of a subform in Tripetto" loading="lazy" />
  <figcaption>Build this subform in a separate storyboard.</figcaption>
</figure>

<h3 id="navigation">Navigation</h3>
<p>When you open a subform in the form builder, you can use the top menu bar to navigate back to the previous form level.</p>

<h3 id="convert">Convert section/subform</h3>
<p>To help you improve your form structure, you can also convert a section into a subform. That way you extract that whole section (including logic branches) from your main form structure and convert it into a subform, which you can then open in a separate storyboard.</p>
<p>The other way around is also possible: if you have a subform that you want back in your parent form, you can simply convert that into a section again. The content of your subform will then become available in the parent form again.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/convert.png" width="1200" height="760" alt="Screenshot of converting a subform into a section in Tripetto" loading="lazy" />
  <figcaption>Convert a subform into a section.</figcaption>
</figure>

<h3 id="move">Move between subforms</h3>
<p>It's also possible to move parts of your form between subforms or the parent form. That way you can for example move a section with questions into an already existing subform. Or the other way around: move a section from a subform back to its parent form.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/move.png" width="1200" height="760" alt="Screenshot of moving a section to a subform in Tripetto" loading="lazy" />
  <figcaption>Move a section to a subform.</figcaption>
</figure>
<hr/>

<h2 id="interaction">Data interaction</h2>
<p>All levels of subforms can interact with each other, for example to add logic to your form. Let's say you want to recall a value (<a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/">piping logic</a>) from the main form in a block in a subform. That's no problem! The subform will be able to recall all blocks from any form level.</p>
<p>The other way around it's also possible to use blocks from a subform in a higher level of your form. Let's say you want to add a branch (<a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/">branch logic</a>) in your parent form, based on a question block from a subform. The parent form will simply have all question blocks available to perform the desired logic!</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/interaction.png" width="1200" height="760" alt="Screenshot of using a question block from a subform in Tripetto" loading="lazy" />
  <figcaption>Recall a value from a block in a subform.</figcaption>
</figure>
