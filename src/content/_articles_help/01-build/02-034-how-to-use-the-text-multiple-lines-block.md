---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-text-multiple-lines-block/
title: Use the text (multiple lines) block - Tripetto Help Center
description: Learn everything you need to know to use the text (multiple lines) block in your forms.
article_title: How to use the text (multiple lines) block
article_folder: editor-block-text-multiple
author: jurgen
time: 3
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-text-multiple-lines-block/
---
<p>Learn everything you need to know to use the text (multiple lines) block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the text (multiple lines) block to let your respondents enter a text answer that can contain line breaks. Usually used for long text answers, like explanations.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/text-multiple.gif" width="1200" height="700" alt="Screenshot of text inputs in Tripetto" loading="lazy" />
  <figcaption>Demonstration of a text multiple lines block.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Text (multiple lines)</code>. You can now customize this block to your needs with the following features:</p>

<h3 id="features-general">General</h3>
<ul>
  <li><strong>Text</strong><br/>Use the <code>Text</code> feature for the name/title of this block.</li>
  <li><strong>Description</strong><br/>Enable the <code>Description</code> feature to add a description to this block.</li>
  <li><strong>Placeholder</strong><br/>Enable the <code>Placeholder</code> feature to add a text inside the empty input control of this block.</li>
  <li><strong>Help text</strong><br/>Enable the <code>Help text</code> feature to add a help text to this block.</li>
</ul>

<h3 id="features-settings">Settings</h3>
<ul>
  <li><strong>Limits</strong><br/>Enable the <code>Limits</code> feature to control the amount of characters your respondents can enter. You can set the <code>Minimum</code> and <code>Maximum</code> character limits.</li>
  <li><strong>Transform</strong><br/>Enable the <code>Transform</code> feature to determine how the answered text should be transformed. For example you can capitalize certain parts of the answer, or convert all characters to lowercase/uppercase.</li>
</ul>

<h3 id="features-options">Options</h3>
<ul>
  <li><strong>Prefill</strong><a href="{{ page.base }}help/articles/how-to-prefill-question-blocks-with-initial-values/" class="article-more-info" title="More information about prefilling"><i class="fas fa-info-circle"></i></a><br/>Enable the <code>Prefill</code> feature to set a fixed initial value of this block.</li>
  <li><strong>Required</strong><br/>By default this block is not required to fill out by your respondents. Enable the <code>Required</code> feature to make this block required to fill out.</li>
  <li><strong>Visibility</strong><br/>By default this block is visible in your form. Enable the <code>Visibility</code> feature to hide it for your respondents.</li>
  <li><strong>Alias</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about alias"><i class="fas fa-info-circle"></i></a><br/>By default the name/title of this block will be used in your dataset. Enable the <code>Alias</code> feature to use a different name in your dataset.</li>
  <li><strong>Exportability</strong><a href="{{ page.base }}help/articles/how-to-determine-what-data-fields-get-saved/" class="article-more-info" title="More information about exportability"><i class="fas fa-info-circle"></i></a><br/>By default the collected data of this block will be stored to the dataset of each result. Enable the <code>Exportability</code> feature to take control over the storage. You can then include/exclude the data from your dataset. Please note that if you exclude data from the dataset you can never see and/or use the given answers from your respondents to this block, because the data is simply not stored at all.</li>
</ul>

<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-text-multiple.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Advanced settings of the text multiple lines block.</figcaption>
</figure>
<hr />

<h2 id="logic">Logic<a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" class="article-more-info" title="More information about logic"><i class="fas fa-info-circle"></i></a></h2>
<p>Logic is important to make your forms smart and conversational. The text (multiple lines) block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Text matches <code>your filter</code>;</li>
  <li>Text does not match <code>your filter</code>;</li>
  <li>Text contains <code>your filter</code>;</li>
  <li>Text does not contain <code>your filter</code>;</li>
  <li>Text starts with <code>your filter</code>;</li>
  <li>Text ends with <code>your filter</code>;</li>
  <li>Text is empty;</li>
  <li>Text is not empty.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Text - Compare with a fixed text that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers/">more info</a>).</li>
</ul>
<hr />

<h2 id="calculations">Calculations<a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" class="article-more-info" title="More information about calculations"><i class="fas fa-info-circle"></i></a></h2>
<p>You can use the <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">calculator block</a> to perform calculations with given answers. The text (multiple lines) block supports the following calculation operations:</p>
<ul>
  <li>Compare - Compare a text and output a value based on the result of the comparison;</li>
  <li>Character count - Count the number of characters in a text;</li>
  <li>Word count - Count the number of words in a text;</li>
  <li>Line count - Count the number of lines in a text;</li>
  <li>Count occurrences - Count the number of occurrences of a certain text, character or regular expression in a text;</li>
  <li>Convert to number - Convert a text to a number value.</li>
</ul>
<hr />
{% include help-article-blocks.html %}
