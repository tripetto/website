---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-rating-block/
title: Use the rating block - Tripetto Help Center
description: Learn everything you need to know to use the rating block in your forms.
article_title: How to use the rating block
article_folder: editor-block-rating
author: jurgen
time: 3
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-rating-block/
---
<p>Learn everything you need to know to use the rating block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the rating block to let your respondents select a rating on the scale you provide them. The scale can have various shapes (like stars, hearts, thumbs) and various steps.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/rating.gif" width="1200" height="760" alt="Screenshot of rating blocks in Tripetto" loading="lazy" />
  <figcaption>Demonstration of three rating blocks with different shapes and steps.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Rating</code>. You can now customize this block to your needs with the following features:</p>

<h3 id="features-general">General</h3>
<ul>
  <li><strong>Text</strong><br/>Use the <code>Text</code> feature for the name/title of this block.</li>
  <li><strong>Description</strong><br/>Enable the <code>Description</code> feature to add a description to this block.</li>
  <li><strong>Image</strong><a href="{{ page.base }}help/articles/how-to-use-images-and-videos-in-your-form/" class="article-more-info" title="More information about images"><i class="fas fa-info-circle"></i></a><br/>Enable the <code>Image</code> feature to add an image to this block.</li>
  <li><strong>Help text</strong><br/>Enable the <code>Help text</code> feature to add a help text to this block.</li>
</ul>

<h3 id="features-settings">Settings</h3>
<ul>
  <li><strong>Shape</strong><br/>By default the rating is showed in stars shape. Enable the <code>Shape</code> feature to select the shape of the rating.</li>
  <li><strong>Steps</strong><br/>By default the rating has 5 steps. Enable the <code>Steps</code> feature to determine the scale of your rating question. For example from 1 to 10.</li>
</ul>

<h3 id="features-options">Options</h3>
<ul>
  <li><strong>Required</strong><br/>By default this block is not required to fill out by your respondents. Enable the <code>Required</code> feature to make this block required to fill out.</li>
  <li><strong>Visibility</strong><br/>By default this block is visible in your form. Enable the <code>Visibility</code> feature to hide it for your respondents.</li>
  <li><strong>Alias</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about alias"><i class="fas fa-info-circle"></i></a><br/>By default the name/title of this block will be used in your dataset. Enable the <code>Alias</code> feature to use a different name in your dataset.</li>
  <li><strong>Exportability</strong><a href="{{ page.base }}help/articles/how-to-determine-what-data-fields-get-saved/" class="article-more-info" title="More information about exportability"><i class="fas fa-info-circle"></i></a><br/>By default the collected data of this block will be stored to the dataset of each result. Enable the <code>Exportability</code> feature to take control over the storage. You can then include/exclude the data from your dataset. Please note that if you exclude data from the dataset you can never see and/or use the given answers from your respondents to this block, because the data is simply not stored at all.</li>
</ul>

<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-rating.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Advanced options of the rating block.</figcaption>
</figure>
<hr />

<h2 id="logic">Logic<a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" class="article-more-info" title="More information about logic"><i class="fas fa-info-circle"></i></a></h2>
<p>Logic is important to make your forms smart and conversational. The rating block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Match the rating value selected.</li>
</ul>
<h3 id="evaluate-conditions">Evaluate conditions</h3>
<ul>
  <li>Value is equal to <code>your filter</code>;</li>
  <li>Value is not equal to <code>your filter</code>;</li>
  <li>Value is lower than<code>your filter</code>;</li>
  <li>Value is higher than <code>your filter</code>;</li>
  <li>Value is between <code>your filters</code>;</li>
  <li>Value is not between <code>your filters</code>;</li>
  <li>Value is empty;</li>
  <li>Value is not empty.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter(s)</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Number - Compare with a fixed number that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers/">more info</a>).</li>
</ul>
<hr />

<h2 id="calculations">Calculations<a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" class="article-more-info" title="More information about calculations"><i class="fas fa-info-circle"></i></a></h2>
<p>You can use the <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">calculator block</a> to perform calculations with given answers. The rating block supports the following calculation operations:</p>
<ul>
  <li>Current value - Supply a selected rating;</li>
  <li>Compare - Compare a rating and output a value based on the result of the comparison;</li>
  <li>Functions - Execute all kinds of <a href="{{ page.base }}calculator-features/">mathematical functions</a> to a rating.</li>
</ul>
<hr />

{% include help-article-blocks.html %}
