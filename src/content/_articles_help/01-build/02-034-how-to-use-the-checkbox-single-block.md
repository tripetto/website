---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-checkbox-single-block/
title: Use the checkbox (single) block - Tripetto Help Center
description: Learn everything you need to know to use the checkbox (single) block in your forms.
article_title: How to use the checkbox (single) block
article_folder: editor-block-checkbox
author: jurgen
time: 3
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-checkbox-single-block/
---
<p>Learn everything you need to know to use the checkbox block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the checkbox block to let your respondents check/uncheck one checkbox, for example to agree your terms.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/checkbox.gif" width="1200" height="700" alt="Screenshot of a checkbox block in Tripetto" loading="lazy" />
  <figcaption>Demonstration of a checkbox block.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Checkbox (single)</code>. You can now customize this block to your needs with the following features:</p>

<h3 id="features-general">General</h3>
<ul>
  <li><strong>Text</strong><br/>Use the <code>Text</code> feature for the name/title of this block.</li>
  <li><strong>Description</strong><br/>Enable the <code>Description</code> feature to add a description to this block.</li>
  <li><strong>Placeholder</strong><br/>By default the name of the block will be used as the label for the checkbox. Enable the <code>Placeholder</code> feature to show the name of the block on top of the checkbox and show the placeholder as the label for the checkbox.</li>
  <li><strong>Help text</strong><br/>Enable the <code>Help text</code> feature to add a help text to this block.</li>
</ul>

<h3 id="features-options">Options</h3>
<ul>
  <li><strong>Required</strong><br/>By default this block is not required to fill out by your respondents. Enable the <code>Required</code> feature to make this block required to fill out.</li>
  <li><strong>Visibility</strong><br/>By default this block is visible in your form. Enable the <code>Visibility</code> feature to hide it for your respondents.</li>
  <li><strong>Labels</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about labels"><i class="fas fa-info-circle"></i></a><br/>By default the checkbox will be marked as <code>Checked</code> or <code>Not checked</code> in your dataset (your results). Enable the <code>Labels</code> feature to overwrite these labels in your dataset with your own values.</li>
  <li><strong>Score</strong><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/" class="article-more-info" title="More information about scores"><i class="fas fa-info-circle"></i></a><br/>You can attach scores to the checkbox states to perform instant calculations. Enable the <code>Score</code> feature to enter the desired score values.</li>
  <li><strong>Alias</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about alias"><i class="fas fa-info-circle"></i></a><br/>By default the name/title of this block will be used in your dataset. Enable the <code>Alias</code> feature to use a different name in your dataset.</li>
  <li><strong>Exportability</strong><a href="{{ page.base }}help/articles/how-to-determine-what-data-fields-get-saved/" class="article-more-info" title="More information about exportability"><i class="fas fa-info-circle"></i></a><br/>By default the collected data of this block will be stored to the dataset of each result. Enable the <code>Exportability</code> feature to take control over the storage. You can then include/exclude the data from your dataset. Please note that if you exclude data from the dataset you can never see and/or use the given answers from your respondents to this block, because the data is simply not stored at all.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/options.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>The checkbox block in Tripetto's form builder.</figcaption>
</figure>
<hr />

<h2 id="logic">Logic<a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" class="article-more-info" title="More information about logic"><i class="fas fa-info-circle"></i></a></h2>
<p>Logic is important to make your forms smart and conversational. The checkbox block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Checkbox is checked;</li>
  <li>Checkbox is not checked.</li>
</ul>
<h3 id="evaluate-conditions">Evaluate conditions</h3>
<ul>
  <li>Value is true;</li>
  <li>Value is false;</li>
  <li>Value equals <code>your filter</code>;</li>
  <li>Value not equals <code>your filter</code>;</li>
  <li>Value is empty;</li>
  <li>Value is not empty.</li>
</ul>
<h3 id="score-conditions">Score conditions</h3>
<ul>
  <li>Score is equal to <code>your filter</code>;</li>
  <li>Score is not equal to <code>your filter</code>;</li>
  <li>Score is lower than<code>your filter</code>;</li>
  <li>Score is higher than <code>your filter</code>;</li>
  <li>Score is between <code>your filters</code>;</li>
  <li>Score is not between <code>your filters</code>;</li>
  <li>Score is calculated;</li>
  <li>Score is not calculated.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter(s)</code> above, the checkbox block supports the following filters to make the right comparison:</p>
<ul>
  <li>Value - Compare with another block value entered in the form by a respondent.</li>
</ul>
<hr />

<h2 id="calculations">Calculations<a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" class="article-more-info" title="More information about calculations"><i class="fas fa-info-circle"></i></a></h2>
<p>You can use the <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">calculator block</a> to perform calculations with given answers. The checkbox block supports the following calculation operations:</p>
<ul>
  <li>Choice value - Output a value based on the selected/not-selected state (boolean).</li>
</ul>
<hr />

{% include help-article-blocks.html %}
