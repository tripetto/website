---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-insert-hyperlinks/
title: Insert hyperlinks - Tripetto Help Center
description: There are several ways to insert hyperlinks or buttons into your form.
article_title: How to insert hyperlinks
author: martijn
time: 2
category_id: build
subcategory: build_formatting
areas: [studio, wordpress]
---
<p>There are several ways to insert hyperlinks or buttons into your form.</p>

<h2 id="when-to-use">When to use</h2>
<p>You can insert hyperlinks and/or buttons to open a link from your form to another URL. You can do that in the following places in your form:</p>
<ul>
  <li>In texts, using markdown;</li>
  <li>In multiple choice blocks and picture choice blocks;</li>
  <li>In closing messages.</li>
</ul>

<h2 id="how-to-use">How to use</h2>
<p>Below we will explain how to insert each of these hyperlink possibilities.</p>

<h3 id="markdown">In texts (markdown)</h3>
<p>If you want to insert a text hyperlink in a question name, description, or help text in your form, you can use <a href="{{ page.base }}help/articles/how-to-use-markdown-formatting-in-texts/">markdown formatting</a> to do that. Use the following format at the position in your text where you want to show the text hyperlink: <code>[Link text](link URL)</code>.</p>
<figure>
  <img src="{{ page.base }}images/help/editor-markdown/01-markdown.png" width="1200" height="760" alt="Screenshot of markdown in Tripetto" loading="lazy" />
  <figcaption>Example of a hyperlink in the description of a question block.</figcaption>
</figure>

<h3 id="blocks">In multiple choices/picture choices</h3>
<p>It's also possible to show a button to an external link. With a little trick, you can use our <a href="{{ page.base }}help/articles/how-to-use-the-multiple-choice-block/">multiple choice block</a> or <a href="{{ page.base }}help/articles/how-to-use-the-picture-choice-block/">picture choice block</a> for that.</p>
<p>Each option you enter inside these question blocks, can perform as a button link instead of a real option. By clicking an option and then enable the feature <code>URL</code>, you can enter the link you want to open.</p>
<p>Important notice: a choice that opens a URL can NOT be selected as an answer by your respondents.</p>
<figure>
  <img src="{{ page.base }}images/help/editor-markdown/02-multiple-choice.png" width="1200" height="760" alt="Screenshot of a button in Tripetto" loading="lazy" />
  <figcaption>Example of a button in a multiple choice block.</figcaption>
</figure>

<h3 id="closing">In closing message(s)</h3>
<p>You can also show a button in <a href="{{ page.base }}help/articles/how-to-add-a-closing-message/">closing messages</a>. Simply open the closing message pane and enable the feature <code>Button</code>. You can then enter the button label, URL and target.</p>
<figure>
  <img src="{{ page.base }}images/help/editor-markdown/03-closing.png" width="1200" height="760" alt="Screenshot of a button in Tripetto" loading="lazy" />
  <figcaption>Example of a button in a closing message.</figcaption>
</figure>
