---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-text-single-line-block/
title: Use the text (single line) block - Tripetto Help Center
description: Learn everything you need to know to use the text (single line) block in your forms.
article_title: How to use the text (single line) block
article_folder: editor-block-text-single
author: jurgen
time: 4
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-text-single-line-block/
---
<p>Learn everything you need to know to use the text (single line) block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the text (single line) block to let your respondents enter a text answer that consists of one line. Mostly used for short text answers, like personal data.</p>
<p>You also can extend the text input with <a href="#suggestions-feature" class="anchor">suggestions</a> to let the respondent select from a list of suggestions you provide them, while they are typing.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/text-single.gif" width="1200" height="700" alt="Screenshot of text inputs in Tripetto" loading="lazy" />
  <figcaption>Demonstration of a text single line block. The second question shows the usage of suggestions.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Text (single line)</code>. You can now customize this block to your needs with the following features:</p>

<h3 id="features-general">General</h3>
<ul>
  <li><strong>Text</strong><br/>Use the <code>Text</code> feature for the name/title of this block.</li>
  <li><strong>Description</strong><br/>Enable the <code>Description</code> feature to add a description to this block.</li>
  <li><strong>Placeholder</strong><br/>Enable the <code>Placeholder</code> feature to add a text inside the empty input control of this block.</li>
  <li><strong>Help text</strong><br/>Enable the <code>Help text</code> feature to add a help text to this block.</li>
</ul>

<h3 id="features-settings">Settings</h3>
<ul>
  <li><strong>Limits</strong><br/>Enable the <code>Limits</code> feature to control the amount of characters your respondents can enter. You can set the <code>Minimum</code> and <code>Maximum</code> character limits.</li>
  <li><strong>Autocomplete</strong><br/>Enable the <code>Autocomplete</code> feature to use the autocomplete functionality of the respondent's browser to help the respondent to fill out the form as quickly as possible. You can select which autocomplete value you'd like to insert, like the name or address of the respondent.</li>
  <li><strong>Suggestions</strong><a href="#suggestions-feature" class="article-more-info anchor" title="More information about suggestions"><i class="fas fa-info-circle"></i></a><br/>Enable the <code>Suggestions</code> feature to extend your text input with suggestions that popup while the respondent types in your text input.</li>
  <li><strong>Transform</strong><br/>Enable the <code>Transform</code> feature to determine how the answered text should be transformed. For example you can capitalize certain parts of the answer, or convert all characters to lowercase/uppercase.</li>
</ul>

<h3 id="features-options">Options</h3>
<ul>
  <li><strong>Prefill</strong><a href="{{ page.base }}help/articles/how-to-prefill-question-blocks-with-initial-values/" class="article-more-info" title="More information about prefilling"><i class="fas fa-info-circle"></i></a><br/>Enable the <code>Prefill</code> feature to set a fixed initial value of this block.</li>
  <li><strong>Required</strong><br/>By default this block is not required to fill out by your respondents. Enable the <code>Required</code> feature to make this block required to fill out.</li>
  <li><strong>Visibility</strong><br/>By default this block is visible in your form. Enable the <code>Visibility</code> feature to hide it for your respondents.</li>
  <li><strong>Score</strong><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/" class="article-more-info" title="More information about scores"><i class="fas fa-info-circle"></i></a><br/>You can attach scores to the suggestions to perform instant calculations. Enable the <code>Score</code> feature to enter the desired score values. This feature is only available if you have enabled the <a href="#suggestions-feature" class="anchor">suggestions feature</a>.</li>
  <li><strong>Alias</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about alias"><i class="fas fa-info-circle"></i></a><br/>By default the name/title of this block will be used in your dataset. Enable the <code>Alias</code> feature to use a different name in your dataset.</li>
  <li><strong>Exportability</strong><a href="{{ page.base }}help/articles/how-to-determine-what-data-fields-get-saved/" class="article-more-info" title="More information about exportability"><i class="fas fa-info-circle"></i></a><br/>By default the collected data of this block will be stored to the dataset of each result. Enable the <code>Exportability</code> feature to take control over the storage. You can then include/exclude the data from your dataset. Please note that if you exclude data from the dataset you can never see and/or use the given answers from your respondents to this block, because the data is simply not stored at all.</li>
</ul>

<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-text-single.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Advanced settings of the text single line block.</figcaption>
</figure>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-autocomplete.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Autocomplete options of the text single line block.</figcaption>
</figure>

<h3 id="suggestions-feature">Suggestions feature</h3>
<p>You can extend your text input with suggestions that popup while the respondent types in your text input. The respondent can quickly select an item from your suggestions list. Please note that the respondent still can answer any free text, so also something different than the suggestions you supply. </p>
<p>To activate this, enable the <code>Suggestions</code> feature. Now you can add/import your list of suggestions that you want your respondents to choose from.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-suggestions.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>List of suggestions in the text single line block.</figcaption>
</figure>
<blockquote>
  <h4>📌 Also see: Dropdown with keyword search</h4>
  <p>Are you looking for a dropdown question type in which the respondent can search and select multiple options? Please have a look at the dropdown (multi-select) block.</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-dropdown-multi-select-block/">How to use the dropdown (multi-select) block</a></li>
  </ul>
</blockquote>
<hr />

<h2 id="logic">Logic<a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" class="article-more-info" title="More information about logic"><i class="fas fa-info-circle"></i></a></h2>
<p>Logic is important to make your forms smart and conversational. The text (single line) block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Text matches <code>your filter</code>;</li>
  <li>Text does not match <code>your filter</code>;</li>
  <li>Text contains <code>your filter</code>;</li>
  <li>Text does not contain <code>your filter</code>;</li>
  <li>Text starts with <code>your filter</code>;</li>
  <li>Text ends with <code>your filter</code>;</li>
  <li>Text is empty;</li>
  <li>Text is not empty.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter(s)</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Text - Compare with a fixed text that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers/">more info</a>).</li>
</ul>
<hr />

<h2 id="calculations">Calculations<a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" class="article-more-info" title="More information about calculations"><i class="fas fa-info-circle"></i></a></h2>
<p>You can use the <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">calculator block</a> to perform calculations with given answers. The text (single line) block supports the following calculation operations:</p>
<ul>
  <li>Compare - Compare a text and output a value based on the result of the comparison;</li>
  <li>Character count - Count the number of characters in a text;</li>
  <li>Word count - Count the number of words in a text;</li>
  <li>Line count - Count the number of lines in a text;</li>
  <li>Count occurrences - Count the number of occurrences of a certain text, character or regular expression in a text;</li>
  <li>Convert to number - Convert a text to a number value.</li>
</ul>
<hr />

{% include help-article-blocks.html %}
