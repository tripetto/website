---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-scale-block/
title: Use the scale block - Tripetto Help Center
description: Learn everything you need to know to use the scale block in your forms.
article_title: How to use the scale block
article_folder: editor-block-scale
author: jurgen
time: 4
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-scale-block/
---
<p>Learn everything you need to know to use the scale block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the scale block to let your respondents select an opinion on the scale you provide them, for example for a Net Promoter Score (NPS) on the scale from 0 to 10. But you can also use texts scales instead of numeric scales.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/scale.gif" width="1200" height="760" alt="Screenshot of scales in Tripetto" loading="lazy" />
  <figcaption>Demonstration of two scale blocks with a numeric scale and a text scale.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Scale</code>. You can now customize this block to your needs with the following features:</p>


<h3 id="features-general">General</h3>
<ul>
  <li><strong>Text</strong><br/>Use the <code>Text</code> feature for the name/title of this block.</li>
  <li><strong>Description</strong><br/>Enable the <code>Description</code> feature to add a description to this block.</li>
  <li><strong>Help text</strong><br/>Enable the <code>Help text</code> feature to add a help text to this block.</li>
</ul>

<h3 id="features-settings">Settings</h3>
<ul>
  <li><strong>Mode</strong><br/>The <code>Mode</code> feature is always activated. You can select if your scale is a <code>Numeric scale</code> (for example from 0 to 10) or a <code>Text scale</code> (for example from bad to good). Depending on this setting, one of the following features gets activated:
    <ul>
      <li>If you selected a numeric scale, you can determine the <code>Scale range</code>. You can even use negative scales over here.</li>
      <li>If you selected a text scale, you can compose the list of choices you want to show. You can add choices one by one, or import a list of choices from a text file at once.</li>
    </ul>
  </li>
  <li><strong>Labels</strong><br/>Enable the <code>Labels</code> feature to add labels that are shown on the left, center and/or right of the scale.</li>
  <li><strong>Width</strong><br/>Enable the <code>Width</code> feature to determine if the width of the scale should be fixed or the scale can grow with the size of the screen of the respondent.</li>
  <li><strong>Score</strong><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/" class="article-more-info" title="More information about scores"><i class="fas fa-info-circle"></i></a><br/>You can attach scores to the choices to perform instant calculations. Enable the <code>Score</code> feature to enter the desired score values. This feature is only available if your scale mode is set to text scale.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-scale.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Advanced options of the scale block.</figcaption>
</figure>

<h3 id="features-options">Options</h3>
<ul>
  <li><strong>Required</strong><br/>By default this block is not required to fill out by your respondents. Enable the <code>Required</code> feature to make this block required to fill out.</li>
  <li><strong>Visibility</strong><br/>By default this block is visible in your form. Enable the <code>Visibility</code> feature to hide it for your respondents.</li>
  <li><strong>Score</strong><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/" class="article-more-info" title="More information about scores"><i class="fas fa-info-circle"></i></a><br/>You can attach scores to the choices to perform instant calculations. Enable the <code>Score</code> feature to enter the desired score values. This feature is only available if your scale mode is set to text scale.</li>
  <li><strong>Alias</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about alias"><i class="fas fa-info-circle"></i></a><br/>By default the name/title of this block will be used in your dataset. Enable the <code>Alias</code> feature to use a different name in your dataset.</li>
  <li><strong>Exportability</strong><a href="{{ page.base }}help/articles/how-to-determine-what-data-fields-get-saved/" class="article-more-info" title="More information about exportability"><i class="fas fa-info-circle"></i></a><br/>By default the collected data of this block will be stored to the dataset of each result. Enable the <code>Exportability</code> feature to take control over the storage. You can then include/exclude the data from your dataset. Please note that if you exclude data from the dataset you can never see and/or use the given answers from your respondents to this block, because the data is simply not stored at all.</li>
</ul>

<h3 id="features-choices">Choices (text scale)</h3>
<p>If your scale mode is set to text scale, you can open each scale option to get to the settings of that single option. Over there you can use these features:</p>
<ul>
  <li><strong>Name</strong><br/>Use the <code>Name</code> feature for the name/title of this choice.</li>
  <li><strong>Identifier</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about identifiers"><i class="fas fa-info-circle"></i></a><br/>By default the name of each choice will be used in your dataset. Enable the <code>Identifier</code> feature to use a different name in your dataset.</li>
  <li><strong>Score</strong><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/" class="article-more-info" title="More information about scores"><i class="fas fa-info-circle"></i></a><br/>Enable the <code>Score</code> feature to enter the desired score value for that choice.</li>
</ul>
<hr />

<h2 id="logic-numeric">Logic (numeric scales)<a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" class="article-more-info" title="More information about logic"><i class="fas fa-info-circle"></i></a></h2>

<p>Logic is important to make your forms smart and conversational. The scale block with a numeric scale can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Match the scale value selected.</li>
</ul>
<h3 id="evaluate-conditions">Evaluate conditions</h3>
<ul>
  <li>Value is equal to <code>your filter</code>;</li>
  <li>Value is not equal to <code>your filter</code>;</li>
  <li>Value is lower than<code>your filter</code>;</li>
  <li>Value is higher than <code>your filter</code>;</li>
  <li>Value is between <code>your filters</code>;</li>
  <li>Value is not between <code>your filters</code>;</li>
  <li>Value is empty;</li>
  <li>Value is not empty.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter(s)</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Number - Compare with a fixed number that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers/">more info</a>).</li>
</ul>

<h2 id="logic-text">Logic (text scales)<a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" class="article-more-info" title="More information about logic"><i class="fas fa-info-circle"></i></a></h2>
<p>The scale block with a text scale can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Match one of the options;</li>
  <li>Unanswered.</li>
</ul>
<h3 id="evaluate-conditions">Evaluate conditions</h3>
<ul>
  <li>Value matches <code>your filter</code>;</li>
  <li>Value does not match <code>your filter</code>;</li>
  <li>Value contains <code>your filter</code>;</li>
  <li>Value does not contain <code>your filter</code>;</li>
  <li>Value starts with <code>your filter</code>;</li>
  <li>Value ends with <code>your filter</code>;</li>
  <li>Value is empty;</li>
  <li>Value is not empty.</li>
</ul>
<h3 id="score-conditions">Score conditions</h3>
<ul>
  <li>Score is equal to <code>your filter</code>;</li>
  <li>Score is not equal to <code>your filter</code>;</li>
  <li>Score is lower than<code>your filter</code>;</li>
  <li>Score is higher than <code>your filter</code>;</li>
  <li>Score is between <code>your filters</code>;</li>
  <li>Score is not between <code>your filters</code>;</li>
  <li>Score is calculated;</li>
  <li>Score is not calculated.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Text - Compare with a fixed text that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers/">more info</a>).</li>
</ul>
<hr />

<h2 id="calculations-numeric">Calculations (numeric scale)<a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" class="article-more-info" title="More information about calculations"><i class="fas fa-info-circle"></i></a></h2>
<p>You can use the <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">calculator block</a> to perform calculations with given answers. The scale block with a numeric scale supports the following calculation operations:</p>
<ul>
  <li>Current value - Supply a selected scale value;</li>
  <li>Compare - Compare a scale value and output a value based on the result of the comparison;</li>
  <li>Functions - Execute all kinds of <a href="{{ page.base }}calculator-features/">mathematical functions</a> to a scale value.</li>
</ul>
<h2 id="calculations-text">Calculations (text scale)<a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" class="article-more-info" title="More information about calculations"><i class="fas fa-info-circle"></i></a></h2>
<p>The scale block with a text scale supports the following calculation operations:</p>
<ul>
  <li>Score - Calculate a score based on the entered score list for choices.</li>
</ul>
<hr />
{% include help-article-blocks.html %}
