---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-file-upload-block/
title: Use the file upload block - Tripetto Help Center
description: Learn everything you need to know to use the file upload block in your forms.
article_title: How to use the file upload block
article_folder: editor-block-file-upload
author: jurgen
time: 3
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-file-upload-block/
---
<p>Learn everything you need to know to use the file upload block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the file upload block to let your respondents upload a file or image to their responses.</p>
<p>Each file upload block enables your respondents to upload one file. Need your respondents to upload multiple files? Then add multiple file upload blocks to your form structure. <a href="{{ page.base }}help/articles/how-to-repeat-follow-up-for-a-selected-number-of-times/">Or repeat a file upload block for a selected amount of times, using branch logic.</a></p>
<figure>
  <img src="{{ page.base }}images/help/blocks/file-upload.gif" width="1200" height="700" alt="Screenshot of a file upload block in Tripetto" loading="lazy" />
  <figcaption>Demonstration of a file upload block.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>File upload</code>. You can now customize this block to your needs with the following features:</p>

<h3 id="features-general">General</h3>
<ul>
  <li><strong>Text</strong><br/>Use the <code>Text</code> feature for the name/title of this block.</li>
  <li><strong>Description</strong><br/>Enable the <code>Description</code> feature to add a description to this block.</li>
  <li><strong>Help text</strong><br/>Enable the <code>Help text</code> feature to add a help text to this block.</li>
</ul>

<h3 id="features-settings">Settings</h3>
<ul>
  <li><strong>File size</strong><br/>Enable the <code>File size</code> feature to determine the maximum file size (in Mb) that respondents can upload.</li>
  <li><strong>File type</strong><br/>Enable the <code>File type</code> feature to determine what file type(s) respondents can select. Make sure you start the file type with a dot (for example <code>.pdf</code>). You can supply multiple file types, separated with a comma (<code>,</code>).</li>
</ul>

<h3 id="features-options">Options</h3>
<ul>
  <li><strong>Required</strong><br/>By default this block is not required to fill out by your respondents. Enable the <code>Required</code> feature to make this block required to fill out.</li>
  <li><strong>Visibility</strong><br/>By default this block is visible in your form. Enable the <code>Visibility</code> feature to hide it for your respondents.</li>
  <li><strong>Alias</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about alias"><i class="fas fa-info-circle"></i></a><br/>By default the name/title of this block will be used in your dataset. Enable the <code>Alias</code> feature to use a different name in your dataset.</li>
</ul>

<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-file-upload.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Advanced settings of the file upload block.</figcaption>
</figure>
<hr />

<h2 id="storage">Storage</h2>
<p>Each file is stored in your dataset with the original file name. When the file is an image (file type <code>.jpg</code>, <code>.jpeg</code>, <code>.png</code>, <code>.gif</code>, or <code>.webp</code>) you can easily see a preview in the results. You can always download a copy of the file.</p>
<hr />


<h2 id="logic">Logic<a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" class="article-more-info" title="More information about logic"><i class="fas fa-info-circle"></i></a></h2>
<p>Logic is important to make your forms smart and conversational. The file upload block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>File uploaded;</li>
  <li>No file uploaded.</li>
</ul>
<h3 id="evaluate-conditions">Evaluate conditions</h3>
<p>Evaluate conditions are done with the file name of the uploaded file.</p>
<ul>
  <li>Value matches <code>your filter</code>;</li>
  <li>Value does not match <code>your filter</code>;</li>
  <li>Value contains <code>your filter</code>;</li>
  <li>Value does not contain <code>your filter</code>;</li>
  <li>Value starts with <code>your filter</code>;</li>
  <li>Value ends with <code>your filter</code>;</li>
  <li>Value is empty;</li>
  <li>Value is not empty.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Text - Compare with a fixed text that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers/">more info</a>).</li>
</ul>
<hr />

<h2 id="calculations">Calculations<a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" class="article-more-info" title="More information about calculations"><i class="fas fa-info-circle"></i></a></h2>
<p>You can use the <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">calculator block</a> to perform calculations with given answers. For the file upload block these calculations are done using the file name. The file upload block supports the following calculation operations:</p>
<ul>
  <li>Compare - Compare a file name and output a value based on the result of the comparison;</li>
  <li>Character count - Count the number of characters in a file name;</li>
  <li>Word count - Count the number of words in a file name;</li>
  <li>Line count - Count the number of lines in a file name;</li>
  <li>Count occurrences - Count the number of occurrences of a certain text, character or regular expression in a file name;</li>
  <li>Convert to number - Convert a file name to a number value.</li>
</ul>
<hr />

{% include help-article-blocks.html %}
