---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-build-your-forms-in-the-form-builder/
title: Build your forms in the form builder - Tripetto Help Center
description: Learn how to use our unique form builder to create your forms.
article_title: How to build your forms in the form builder
article_id: build
article_video: build-simple-form
author: martijn
time: 4
time_video: 5
category_id: build
subcategory: build_basics
areas: [studio, wordpress]
---
<p>Let's have a quick look on the principles of our form builder, how to add the desired content (like question blocks, welcome and closing messages) and how to add some smartness to it all.</p>

<h2 id="concept" data-anchor="Form builder">Concept of the form builder</h2>
<p>As you may have noticed, our form builder is a bit different from traditional form builders. Let us explain some principles of our form builder.</p>

<h3 id="storyboard">Storyboard</h3>
<p>The form builder is intended to build smart forms. Smart forms need logic and that's what the form builder helps you with: it shows a visual presentation of the flows in your forms on a storyboard.
  <ul>
    <li>The top-down direction shows the order of the form blocks inside the form;</li>
    <li>The left-right direction shows decisions inside the form that can be taken depending on the input of your respondents. We call those decisions <strong>branches</strong>.</li>
  </ul>
</p>

<h3 id="autosave">Autosave</h3>
<p>Everything you do in the form builder gets saved immediately (autosave), so you don't have to worry about saving your form project yourself. It's done automatically all the time.</p>

<h3 id="gestures">Gestures</h3>
<p>The form builder works with some smart gestures to navigate through the storyboard and to drag-and-drop items on the storyboard. Please have a look at the video tutorial below or at this article to learn how to control the form builder: <a href="{{ page.base }}help/articles/learn-the-basic-controls-to-use-the-form-builder/">Learn the basic controls to use the form builder</a>.</p>
<figure>
  <div class="youtube-player" data-id="C8sfFTPGASg"></div>
  <figcaption>Video tutorial about using the form builder.</figcaption>
</figure>

<h2 id="form-structure">Form structure</h2>
<p>When you start a new form in the form builder, all you see is a green and a red bubble, indicating the start and end of your form. In between these bubbles, we're going to build our form structure.</p>

<h3 id="start">Start</h3>
<p>The green bubble with the <code><i class="fas fa-sign-in-alt"></i></code> icon indicates the <strong>start of your form</strong>. You can click it to insert a <a href="{{ page.base }}help/articles/how-to-add-a-welcome-message/">welcome message</a> that is shown to your respondents before the actual form starts.</p>
<figure>
  <img src="{{ page.base }}images/help/editor-start/00-add.gif" width="1200" height="760" alt="Screenshot of a welcome message in Tripetto" loading="lazy" />
  <figcaption>Add a welcome message.</figcaption>
</figure>

<h3 id="closing">End</h3>
<p>The red bubble with the <code><i class="fas fa-sign-out-alt"></i></code> icon indicates the <strong>default ending of your form</strong>. You can click it to insert a <a href="{{ page.base }}help/articles/how-to-add-a-closing-message/">closing message</a> to your form that is shown when your respondents completed the form.</p>
<p>It's also possible to insert <a href="{{ page.base }}help/articles/how-to-add-one-or-multiple-closing-messages/">different closing messages</a> based on your respondent's answers. You use form logic for that, which we'll discuss later in this article.</p>

<h3 id="sections">Sections</h3>
<p>All form blocks are placed within <strong>sections</strong>. Sections are the containers for the questions you're asking/actions you're performing. You can use sections to add logic and create cuts inside your form.</p>

<h3 id="subforms">Subforms</h3>
<p>Next to sections, it's also possible to use so-called <strong>subforms</strong> to create forms-in-forms for improved organization and structuring of large forms. With a <a href="{{ page.base }}help/articles/how-to-use-subforms-in-the-form-builder/">subform</a> you can bundle a part of your form into one block in your form structure. That way you can make your form structure a lot smaller on the storyboard and therefore easier to oversee.</p>
<hr />

<h2 id="form-content">Form content</h2>
<p>The actual content of your form gets determined by the <strong>form blocks</strong> you add in the form builder. After you added a form block you can select the desired block type. We offer two types of blocks:</p>
<ul>
  <li><h3 id="question-blocks">Question blocks</h3><p>Question blocks can be used to let your respondents interact with your form. For each question block you can select the <strong>question type</strong>. The question type takes care of the input control that is associated with each block. <a href="{{ page.base }}help/articles/question-blocks-guide-which-question-type-to-use/">Have a look at this article for a guide on determing the right question type for your questions.</a></p>
  <p>Examples of question types are text (single and multiple line), multiple choice, picture choice, checkboxes, radio buttons, date, matrix, file upload, etc.;</p></li>
  <li><h3 id="action-blocks">Action blocks</h3><p>Action blocks, just like question blocks, can be used anywhere in the form, but instead of providing a certain input, it performs a certain action.</p>
  <p>Examples of actions are performing a calculation, sending an email or prefilling a value in a question.</p></li>
</ul>
<p>You can see an overview of all question and action blocks Tripetto offers via the following link:</p>
<div>
  <a href="{{ page.base }}question-types/" class="blocklink">
    <div>
      <span class="title">All Question Types</span>
      <span class="description">Ask all the right questions inside your forms. Tripetto offers every question type you need to build advanced forms, surveys, quizzes and more.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" loading="lazy" />
    </div>
  </a>
</div>

<h3 id="block-featuress">Block features</h3>
<p>When you have selected the desired block type, you can enter the content/settings for that block. On the left of the block pane, you'll see a list of <strong>block features</strong>. These are switches you can toggle to add content and settings to each form block.</p>
<figure>
  <img src="{{ page.base }}images/help/editor-features/00-features.gif" width="718" height="691" alt="Screenshot of features in Tripetto" loading="lazy" />
  <figcaption>Enabling the needed features of a block.</figcaption>
</figure>
<h4>Common block features</h4>
<p>Most of these block features are available for all question types, for example:</p>
<ul>
  <li><code>Text</code> - The main text of the form block;</li>
  <li><code>Description</code> - An optional description of the form block;</li>
  <li><code>Help text</code> - An optional help text of the form block;</li>
  <li><code>Required</code> - An option to determine if the question is required to fill out by the respondents;</li>
  <li><code>Visibility</code> - An option to determine if the block is visible in the form;</li>
  <li><code>Alias</code> - An option to set an alias to use in the dataset (<a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/">more information on alias over here</a>);</li>
  <li><code>Exportability</code> - An option to determine if the collected data gets saved (<a href="{{ page.base }}help/articles/how-to-determine-what-data-fields-get-saved/">more information on the exportability over here</a>).</li>
</ul>
<h3>Discover all form blocks</h3>
<p>We made dedicated help articles for each block type we offer. Select one from the list below to see full details about that block type.</p>
{% include help-article-blocks.html %}
<hr />

<h2 id="form-logic">Form logic</h2>
<p>To create the necessary logic for smart forms, you use so called <strong>branch logic</strong>. Such branches are drawn in the vertical direction, making it visual to you which paths can be taken in your form.</p>

<h3 id="branch-logic">Branch logic</h3>
<p>By adding the desired <strong><a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">branch conditions</a></strong> and <strong><a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-behavior-for-your-logic/">branch behavior</a></strong> to each branch, you determine what condition(s) must be matched to enter a certain branch in the form. Branches are very powerful and you can <a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/">read all about it in this article</a>.</p>
<p>Underneath each branch you can fill in the follow-up, containing the form blocks and/or logic that respondents get to see when they have entered a certain branch in the form.</p>
<figure>
  <img src="{{ page.base }}images/help/editor-branch-conditions/00-basic.png" width="1200" height="760" alt="Screenshot of branches in Tripetto" loading="lazy" />
  <figcaption>Example of a branch.</figcaption>
</figure>

<h3 id="branch-endings">Branch endings</h3>
<p>At the end of each branch you can select how the form should proceed after that branch is completed by your respondents. You can choose from different types of <strong><a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-endings-for-your-logic/">branch endings</a></strong>, for example jump to a later part of your form, or a custom closing message.</p>
<p>You can see an overview of all logic types Tripetto offers via the following link:</p>
<div>
  <a href="{{ page.base }}logic-types/" class="blocklink">
    <div>
      <span class="title">All Logic Types</span>
      <span class="description">Boost completion rates by using the right logic.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" loading="lazy" />
    </div>
  </a>
</div>
<hr />

<h2 id="video">Video tutorials</h2>
<p>We also made some video tutorials about how to use the storyboard of Tripetto.</p>
<figure>
  <div class="youtube-player" data-id="zhaHhlAzKNU"></div>
  <figcaption>Video tutorial about arranging form elements.</figcaption>
</figure>
<figure>
  <div class="youtube-player" data-id="n4ahGuFtuDg"></div>
  <figcaption>Video tutorial about navigating the storyboard.</figcaption>
</figure>
<hr />

<h2>More video tutorials</h2>
<p>You can have a look at <a href="{{ page.base }}help/articles/">all our help articles</a> for more in-depth (video) tutorials to get the most out of Tripetto. Or have a look at our <a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank" rel="noopener noreferrer">YouTube channel</a> for all videos.</p>
<div>
  <a href="https://www.youtube.com/channel/{{ site.accounts.youtube }}" target="_blank" rel="noopener noreferrer" class="blocklink">
    <div>
      <span class="title">Tripetto YouTube channel<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Visit our YouTube channel with tutorial videos.</span>
      <span class="url">youtube.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-youtube.svg" width="160" height="160" alt="YouTube logo" loading="lazy" />
    </div>
  </a>
</div>
