---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-dropdown-block/
title: Use the dropdown block - Tripetto Help Center
description: Learn everything you need to know to use the dropdown block in your forms.
article_title: How to use the dropdown block
article_folder: editor-block-dropdown
author: jurgen
time: 3
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-dropdown-block/
---
<p>Learn everything you need to know to use the dropdown block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the dropdown block to let your respondents select one item from a dropdown list with choices you provide them.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/dropdown.gif" width="1200" height="700" alt="Screenshot of a dropdown block in Tripetto" loading="lazy" />
  <figcaption>Demonstration of a dropdown block.</figcaption>
</figure>
<blockquote>
  <h4>📌 Also see: Dropdown with keyword search</h4>
  <p>Are you looking for a dropdown question type in which the respondent also can search and select multiple options? Please have a look at the dropdown (multi-select) block.</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-dropdown-multi-select-block/">How to use the dropdown (multi-select) block</a></li>
  </ul>
</blockquote>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Dropdown (single-select)</code>. You can now customize this block to your needs with the following features:</p>

<h3 id="features-general">General</h3>
<ul>
  <li><strong>Text</strong><br/>Use the <code>Text</code> feature for the name/title of this block.</li>
  <li><strong>Description</strong><br/>Enable the <code>Description</code> feature to add a description to this block.</li>
  <li><strong>Placeholder</strong><br/>Enable the <code>Placeholder</code> feature to add a text inside the empty input control of this block.</li>
  <li><strong>Help text</strong><br/>Enable the <code>Help text</code> feature to add a help text to this block.</li>
</ul>

<h3 id="features-settings">Settings</h3>
<ul>
  <li><strong>Randomization</strong><br/>Enable the <code>Randomization</code> feature to present the choices in a random order to your respondents (using <a href="https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle" target="_blank" rel="noopener noreferrer">Fisher–Yates shuffle</a>).</li>
</ul>

<h3 id="features-options">Options</h3>
<ul>
  <li><strong>Required</strong><br/>By default this block is not required to fill out by your respondents. Enable the <code>Required</code> feature to make this block required to fill out.</li>
  <li><strong>Visibility</strong><br/>By default this block is visible in your form. Enable the <code>Visibility</code> feature to hide it for your respondents.</li>
  <li><strong>Score</strong><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/" class="article-more-info" title="More information about scores"><i class="fas fa-info-circle"></i></a><br/>You can attach scores to the choices to perform instant calculations. Enable the <code>Score</code> feature to enter the desired score values.</li>
  <li><strong>Alias</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about alias"><i class="fas fa-info-circle"></i></a><br/>By default the name/title of this block will be used in your dataset. Enable the <code>Alias</code> feature to use a different name in your dataset.</li>
  <li><strong>Exportability</strong><a href="{{ page.base }}help/articles/how-to-determine-what-data-fields-get-saved/" class="article-more-info" title="More information about exportability"><i class="fas fa-info-circle"></i></a><br/>By default the collected data of this block will be stored to the dataset of each result. Enable the <code>Exportability</code> feature to take control over the storage. You can then include/exclude the data from your dataset. Please note that if you exclude data from the dataset you can never see and/or use the given answers from your respondents to this block, because the data is simply not stored at all.</li>
</ul>

<h3 id="choices">Choices</h3>
<p>Next, you can enter the list of choices that you want to show to your respondents:</p>
<ul>
  <li><strong>Add choices one by one</strong><br/>Click the <code><i class="fas fa-plus"></i></code> icon at the bottom of the list and enter the choice name.</li>
  <li><strong>Import a list of choices</strong><br/>Click the <code><i class="fas fa-download"></i></code> icon at the top of the list. Now supply a list with one choice name per text line and click <code>Import</code> to add them all at once to your list of choices.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/import.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Importing choices in the dropdown block.</figcaption>
</figure>
<p>From your list of choices you can open each choice to get to the settings of that single choice. Over there you can use these features:</p>
<ul>
  <li><strong>Name</strong><br/>Use the <code>Name</code> feature for the name/title of this choice.</li>
  <li><strong>Identifier</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about identifiers"><i class="fas fa-info-circle"></i></a><br/>By default the name of each choice will be used in your dataset. Enable the <code>Identifier</code> feature to use a different name in your dataset.</li>
  <li><strong>Score</strong><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/" class="article-more-info" title="More information about scores"><i class="fas fa-info-circle"></i></a><br/>Enable the <code>Score</code> feature to enter the desired score value for that choice.</li>
</ul>

<h2 id="logic">Logic<a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" class="article-more-info" title="More information about logic"><i class="fas fa-info-circle"></i></a></h2>
<p>Logic is important to make your forms smart and conversational. The dropdown block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Match one of the options.</li>
  <li>Nothing selected.</li>
</ul>
<h3 id="evaluate-conditions">Evaluate conditions</h3>
<ul>
  <li>Value matches <code>your filter</code>;</li>
  <li>Value does not match <code>your filter</code>;</li>
  <li>Value contains <code>your filter</code>;</li>
  <li>Value does not contain <code>your filter</code>;</li>
  <li>Value starts with <code>your filter</code>;</li>
  <li>Value ends with <code>your filter</code>;</li>
  <li>Value is empty;</li>
  <li>Value is not empty.</li>
</ul>
<h3 id="score-conditions">Score conditions</h3>
<ul>
  <li>Score is equal to <code>your filter</code>;</li>
  <li>Score is not equal to <code>your filter</code>;</li>
  <li>Score is lower than<code>your filter</code>;</li>
  <li>Score is higher than <code>your filter</code>;</li>
  <li>Score is between <code>your filters</code>;</li>
  <li>Score is not between <code>your filters</code>;</li>
  <li>Score is calculated;</li>
  <li>Score is not calculated.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Text - Compare with a fixed text that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers/">more info</a>).</li>
</ul>
<hr />

<h2 id="calculations">Calculations<a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" class="article-more-info" title="More information about calculations"><i class="fas fa-info-circle"></i></a></h2>
<p>You can use the <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">calculator block</a> to perform calculations with given answers. The dropdown block supports the following calculation operations:</p>
<ul>
  <li>Score - Calculate a score based on the entered score list for choices;</li>
  <li>Convert to number - Convert the selected choice text to a number value.</li>
</ul>
<hr />

{% include help-article-blocks.html %}
