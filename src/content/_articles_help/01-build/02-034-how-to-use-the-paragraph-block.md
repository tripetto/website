---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-paragraph-block/
title: Use the paragraph block - Tripetto Help Center
description: Learn everything you need to know to use the paragraph block in your forms.
article_title: How to use the paragraph block
article_folder: editor-block-paragraph
author: jurgen
time: 3
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-paragraph-block/
---
<p>Learn everything you need to know to use the paragraph block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the paragraph block to show a static block of text without any input controls for your respondents.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/paragraph.png" width="1200" height="700" alt="Screenshot of a paragraph in Tripetto" loading="lazy" />
  <figcaption>Demonstration of a paragraph block with an image.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Paragraph</code>. You can now customize this block to your needs with the following features:</p>

<h3 id="features-general">General</h3>
<ul>
  <li><strong>Text</strong><br/>Use the <code>Text</code> feature for the name/title of this block.</li>
  <li><strong>Caption</strong><br/>Use the <code>Caption</code> feature to add an extra caption to this block.</li>
  <li><strong>Description</strong><br/>Enable the <code>Description</code> feature to add a description to this block.</li>
  <li><strong>Image</strong><a href="{{ page.base }}help/articles/how-to-use-images-and-videos-in-your-form/" class="article-more-info" title="More information about images"><i class="fas fa-info-circle"></i></a><br/>Enable the <code>Image</code> feature to add an image to this block.</li>
  <li><strong>Video</strong><a href="{{ page.base }}help/articles/how-to-use-images-and-videos-in-your-form/" class="article-more-info" title="More information about videos"><i class="fas fa-info-circle"></i></a><br/>Enable the <code>Video</code> feature to embed a video from YouTube or Vimeo to this block.</li>
  <li><strong>Help text</strong><br/>Enable the <code>Help text</code> feature to add a help text to this block.</li>
</ul>

<h3 id="features-options">Options</h3>
<ul>
  <li><strong>Visibility</strong><br/>By default this block is visible in your form. Enable the <code>Visibility</code> feature to hide it for your respondents.</li>
</ul>

<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-paragraph.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Settings in the paragraph block.</figcaption>
</figure>
<hr />

{% include help-article-blocks.html %}
