---
layout: help-article
base: ../../../
permalink: /help/articles/question-blocks-guide-which-question-type-to-use/
title: Question blocks guide - Tripetto Help Center
description: Use this guide to determine the right question type for each question in your form.
article_title: Which question type to use (question blocks guide)
author: jurgen
time: 3
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/form-blocks-guide-which-question-type-to-use/
---
<p>Use this guide to determine the right question type for each question in your form.</p>

<h2 id="about">About form blocks</h2>
<p>In Tripetto you build your form structure with <strong>form blocks</strong>. For each form block you can select the <strong>question type</strong>. This determines how the respondent can enter the answer to that question, for example by typing an answer, or by selecting an option from a list of options you provide them.</p>
<p>You can see an overview of all question types Tripetto offers via the following link:</p>
<div>
  <a href="{{ page.base }}question-types/" class="blocklink">
    <div>
      <span class="title">All Question Types</span>
      <span class="description">Ask all the right questions inside your forms. Tripetto offers every question type you need to build advanced forms, surveys, quizzes and more.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" loading="lazy" />
    </div>
  </a>
</div>

<p>Depending on the question type you have selected for each block, you can configure that block exactly like you want. Click on a question type in the guide below to see all options and settings per question type.</p>
<hr />

<h2 id="guide">Guide</h2>
<p>This guide helps you to determine the right question type to use for each question in your form.</p>
<p>First determine what kind of input you'd like to present to your respondent:</p>
<ul>
  <li><a href="#text-input" class="anchor">Text input</a>;</li>
  <li><a href="#number-input" class="anchor">Number input</a>;</li>
  <li><a href="#date-time-input" class="anchor">Date/time input</a>;</li>
  <li><a href="#selection-input" class="anchor">Selection input</a>;</li>
  <li><a href="#toggle-input" class="anchor">Toggle input</a>;</li>
  <li><a href="#ranking-input" class="anchor">Ranking input</a>;</li>
  <li><a href="#scale-input" class="anchor">Scale input</a>;</li>
  <li><a href="#file-input" class="anchor">File input</a>;</li>
  <li><a href="#draw-input" class="anchor">Draw input</a>;</li>
  <li><a href="#no-input" class="anchor">No input</a>.</li>
</ul>
<p>Then determine which question type suits your use case best for that particular question.</p>

<h3 id="text-input">Text input</h3>
<table class="table">
  <thead>
    <tr><th>Let your respondents...</th><th>Possible question type(s)</th></tr>
  </thead>
  <tbody>
    <tr><td>Enter a short answer to an open question, without line breaks</td><td><a href="{{ page.base }}help/articles/how-to-use-the-text-single-line-block/">Text (single line)</a></td></tr>
    <tr><td>Enter a short answer and select from a list of suggestions</td><td><a href="{{ page.base }}help/articles/how-to-use-the-text-single-line-block/">Text (single line)</a> with suggestions feature</td></tr>
    <tr><td>Enter a long answer to an open question, possibly with line breaks</td><td><a href="{{ page.base }}help/articles/how-to-use-the-text-multiple-lines-block/">Text (multiple lines)</a></td></tr>
    <tr><td>Enter a valid email address</td><td><a href="{{ page.base }}help/articles/how-to-use-the-email-address-block/">Email address</a></td></tr>
    <tr><td>Enter a valid URL</td><td><a href="{{ page.base }}help/articles/how-to-use-the-url-block/">URL</a></td></tr>
    <tr><td>Enter a password</td><td><a href="{{ page.base }}help/articles/how-to-use-the-password-block/">Password</a></td></tr>
  </tbody>
</table>

<h3 id="number-input">Number input</h3>
<table class="table">
  <thead>
    <tr><th>Let your respondents...</th><th>Possible question type(s)</th></tr>
  </thead>
  <tbody>
    <tr><td>Enter a number</td><td><a href="{{ page.base }}help/articles/how-to-use-the-number-block/">Number</a></td></tr>
    <tr><td>Enter a decimal number</td><td><a href="{{ page.base }}help/articles/how-to-use-the-number-block/">Number</a></td></tr>
    <tr><td>Select a number from a list</td><td><a href="{{ page.base }}help/articles/how-to-use-the-dropdown-block/">Dropdown</a> with numbers as options</td></tr>
    <tr><td>Enter a telephone number</td><td><a href="{{ page.base }}help/articles/how-to-use-the-phone-number-block/">Phone number</a></td></tr>
  </tbody>
</table>

<h3 id="date-time-input">Date/time input</h3>
<table class="table">
  <thead>
    <tr><th>Let your respondents...</th><th>Possible question type(s)</th></tr>
  </thead>
  <tbody>
    <tr><td>Enter/select a date</td><td><a href="{{ page.base }}help/articles/how-to-use-the-date-and-time-block/">Date (and time)</a></td></tr>
    <tr><td>Enter/select a date and a time</td><td><a href="{{ page.base }}help/articles/how-to-use-the-date-and-time-block/">Date (and time)</a></td></tr>
    <tr><td>Enter/select a date range (from-to)</td><td><a href="{{ page.base }}help/articles/how-to-use-the-date-and-time-block/">Date (and time)</a></td></tr>
  </tbody>
</table>

<h3 id="selection-input">Selection input</h3>
<table class="table">
  <thead>
    <tr><th>Let your respondents...</th><th>Possible question type(s)</th></tr>
  </thead>
  <tbody>
    <tr><td>Select one option from a set of options</td><td><a href="{{ page.base }}help/articles/how-to-use-the-dropdown-block/">Dropdown</a> or<br/><a href="{{ page.base }}help/articles/how-to-use-the-multiple-choice-block/">Multiple choice</a> or<br/><a href="{{ page.base }}help/articles/how-to-use-the-picture-choice-block/">Picture choice</a> or<br/><a href="{{ page.base }}help/articles/how-to-use-the-radio-buttons-block/">Radio buttons</a></td></tr>
    <tr><td>Select multiple options from a set of options</td><td><a href="{{ page.base }}help/articles/how-to-use-the-checkboxes-block/">Checkboxes</a> or<br/><a href="{{ page.base }}help/articles/how-to-use-the-dropdown-multi-select-block/">Dropdown (multi-select)</a> or<br/><a href="{{ page.base }}help/articles/how-to-use-the-multiple-choice-block/">Multiple choice</a> or<br/><a href="{{ page.base }}help/articles/how-to-use-the-picture-choice-block/">Picture choice</a></td></tr>
    <tr><td>Select option(s) from a set of images</td><td><a href="{{ page.base }}help/articles/how-to-use-the-picture-choice-block/">Picture choice</a></td></tr>
    <tr><td>Select option(s) from a set of emojis</td><td><a href="{{ page.base }}help/articles/how-to-use-the-picture-choice-block/">Picture choice</a></td></tr>
    <tr><td>Select one option from a set of options for a list of statements</td><td><a href="{{ page.base }}help/articles/how-to-use-the-matrix-block/">Matrix</a></td></tr>
  </tbody>
</table>

<h3 id="toggle-input">Toggle input</h3>
<table class="table">
  <thead>
    <tr><th>Let your respondents...</th><th>Possible question type(s)</th></tr>
  </thead>
  <tbody>
    <tr><td>Toggle a single checkbox</td><td><a href="{{ page.base }}help/articles/how-to-use-the-checkbox-single-block/">Checkbox</a></td></tr>
    <tr><td>Select yes or no</td><td><a href="{{ page.base }}help/articles/how-to-use-the-yes-no-block/">Yes/no</a></td></tr>
  </tbody>
</table>

<h3 id="ranking-input">Ranking input</h3>
<table class="table">
  <thead>
    <tr><th>Let your respondents...</th><th>Possible question type(s)</th></tr>
  </thead>
  <tbody>
    <tr><td>Compose a full ranking from a list of options</td><td><a href="{{ page.base }}help/articles/how-to-use-the-ranking-block/">Ranking</a></td></tr>
    <tr><td>Compose a top ranking from a list of options</td><td><a href="{{ page.base }}help/articles/how-to-use-the-ranking-block/">Ranking</a> with maximum <i>n</i> limit</td></tr>
  </tbody>
</table>

<h3 id="scale-input">Scale input</h3>
<table class="table">
  <thead>
    <tr><th>Let your respondents...</th><th>Possible question type(s)</th></tr>
  </thead>
  <tbody>
    <tr><td>Select from a numeric scale</td><td><a href="{{ page.base }}help/articles/how-to-use-the-scale-block/">Scale</a> with numeric scale mode</td></tr>
    <tr><td>Select from a text scale</td><td><a href="{{ page.base }}help/articles/how-to-use-the-scale-block/">Scale</a> with text scale mode</td></tr>
    <tr><td>Select a rating from an icon scale</td><td><a href="{{ page.base }}help/articles/how-to-use-the-rating-block/">Rating</a></td></tr>
  </tbody>
</table>

<h3 id="file-input">File input</h3>
<table class="table">
  <thead>
    <tr><th>Let your respondents...</th><th>Possible question type(s)</th></tr>
  </thead>
  <tbody>
    <tr><td>Upload an image</td><td><a href="{{ page.base }}help/articles/how-to-use-the-file-upload-block/">File upload</a></td></tr>
    <tr><td>Upload a file</td><td><a href="{{ page.base }}help/articles/how-to-use-the-file-upload-block/">File upload</a></td></tr>
  </tbody>
</table>

<h3 id="draw-input">Draw input</h3>
<table class="table">
  <thead>
    <tr><th>Let your respondents...</th><th>Possible question type(s)</th></tr>
  </thead>
  <tbody>
    <tr><td>Sign a signature</td><td><a href="{{ page.base }}help/articles/how-to-use-the-signature-block/">Signature</a></td></tr>
    <tr><td>Draw something</td><td><a href="{{ page.base }}help/articles/how-to-use-the-signature-block/">Signature</a></td></tr>
  </tbody>
</table>

<h3 id="no-input">No input (static content)</h3>
<table class="table">
  <thead>
    <tr><th>Show to your respondents...</th><th>Possible question type(s)</th></tr>
  </thead>
  <tbody>
    <tr><td>A static text</td><td><a href="{{ page.base }}help/articles/how-to-use-the-paragraph-block/">Paragraph</a> or<br/><a href="{{ page.base }}help/articles/how-to-use-the-statement-block/">Statement</a></td></tr>
    <tr><td>An image</td><td><a href="{{ page.base }}help/articles/how-to-use-the-paragraph-block/">Paragraph</a> or<br/><a href="{{ page.base }}help/articles/how-to-use-the-statement-block/">Statement</a></td></tr>
    <tr><td>A video</td><td><a href="{{ page.base }}help/articles/how-to-use-the-paragraph-block/">Paragraph</a></td></tr>
  </tbody>
</table>

<hr />

{% include help-article-blocks.html %}
