---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-password-block/
title: Use the password block - Tripetto Help Center
description: Learn everything you need to know to use the password block in your forms.
article_title: How to use the password block
author: jurgen
time: 4
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-password-block/
---
<p>Learn everything you need to know to use the password block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the password block to let your respondent enter a value that's not visible in the form. Optionally you can use this block to <a href="{{ page.base }}help/articles/how-to-verify-passwords-with-password-match/">protect parts of your form</a>.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/password.gif" width="1200" height="700" alt="Screenshot of a password block in Tripetto" loading="lazy" />
  <figcaption>Demonstration of a password block.</figcaption>
</figure>
<blockquote>
  <h4>🚧 Warning: Storing password</h4>
  <p>By default the password block does not store entered passwords in your form. If you choose to store the passwords nonetheless, please make sure you are always careful with using your dataset, as it contains vulnerable information.</p>
</blockquote>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Password</code>. You can now customize this block to your needs with the following features:</p>

<h3 id="features-general">General</h3>
<ul>
  <li><strong>Text</strong><br/>Use the <code>Text</code> feature for the name/title of this block.</li>
  <li><strong>Description</strong><br/>Enable the <code>Description</code> feature to add a description to this block.</li>
  <li><strong>Placeholder</strong><br/>Enable the <code>Placeholder</code> feature to add a text inside the empty input control of this block.</li>
  <li><strong>Help text</strong><br/>Enable the <code>Help text</code> feature to add a help text to this block.</li>
</ul>

<h3 id="features-options">Options</h3>
<ul>
  <li><strong>Required</strong><br/>By default this block is not required to fill out by your respondents. Enable the <code>Required</code> feature to make this block required to fill out.</li>
  <li><strong>Visibility</strong><br/>By default this block is visible in your form. Enable the <code>Visibility</code> feature to hide it for your respondents.</li>
  <li><strong>Alias</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about alias"><i class="fas fa-info-circle"></i></a><br/>By default the name/title of this block will be used in your dataset. Enable the <code>Alias</code> feature to use a different name in your dataset.</li>
  <li><strong>Exportability</strong><a href="{{ page.base }}help/articles/how-to-determine-what-data-fields-get-saved/" class="article-more-info" title="More information about exportability"><i class="fas fa-info-circle"></i></a><br/>By default entered values of password blocks aren't saved to the dataset of the form entries. In that way the passwords that your respondents enter, aren't saved unnecessarily.<br/>If you wish to include the entered passwords in your dataset anyway, you can overrule the default setting by enabling the <code>Exportability</code> feature and activate the <code>Include in the dataset</code> setting. Entered passwords will now be saved and readable inside your dataset. Please make sure you are always careful with using the dataset if you choose to save passwords!</li>
</ul>
<hr />

<h2 id="verification">Secure password verification<a href="{{ page.base }}help/articles/how-to-verify-passwords-with-password-match/" class="article-more-info" title="More information about password verification"><i class="fas fa-info-circle"></i></a></h2>
<p>You can use the password block to protect (parts of) your form by using the secure password match check. <a href="{{ page.base }}help/articles/how-to-verify-passwords-with-password-match/">More information on that can be found in this article</a>.</p>
<hr />

<h2 id="logic">Logic<a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" class="article-more-info" title="More information about logic"><i class="fas fa-info-circle"></i></a></h2>
<p>Logic is important to make your forms smart and conversational. The password block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Password matches <code>your password</code> (<a href="{{ page.base }}help/articles/how-to-verify-passwords-with-password-match/">more information about password verification</a>);</li>
  <li>Password does not match <code>your password</code> (<a href="{{ page.base }}help/articles/how-to-verify-passwords-with-password-match/">more information about password verification</a>).</li>
</ul>
<h3 id="evaluate-conditions">Evaluate conditions</h3>
<ul>
  <li>Value matches <code>your filter</code>;</li>
  <li>Value does not match <code>your filter</code>;</li>
  <li>Value contains <code>your filter</code>;</li>
  <li>Value does not contain <code>your filter</code>;</li>
  <li>Value starts with <code>your filter</code>;</li>
  <li>Value ends with <code>your filter</code>;</li>
  <li>Value is empty;</li>
  <li>Value is not empty.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Text - Compare with a fixed text that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers/">more info</a>).</li>
</ul>
<hr />

<h2 id="calculations">Calculations<a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" class="article-more-info" title="More information about calculations"><i class="fas fa-info-circle"></i></a></h2>
<p>You can use the <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">calculator block</a> to perform calculations with given answers. The password block supports the following calculation operations:</p>
<ul>
  <li>Compare - Compare a password and output a value based on the result of the comparison;</li>
  <li>Character count - Count the number of characters in a password;</li>
  <li>Word count - Count the number of words in a password;</li>
  <li>Line count - Count the number of lines in a password;</li>
  <li>Count occurrences - Count the number of occurrences of a certain text, character or regular expression in a password;</li>
  <li>Convert to number - Convert a password to a number value.</li>
</ul>
<hr />

{% include help-article-blocks.html %}
