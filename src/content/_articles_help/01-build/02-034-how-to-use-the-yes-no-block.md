---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-yes-no-block/
title: Use the yes/no block - Tripetto Help Center
description: Learn everything you need to know to use the yes/no block in your forms.
article_title: How to use the yes/no block
article_folder: editor-block-yes-no
author: jurgen
time: 3
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-yes-no-block/
---
<p>Learn everything you need to know to use the yes/no block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the yes/no block to let your respondents answer with a yes or a no.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/yes-no.gif" width="1200" height="700" alt="Screenshot of a yes/no block in Tripetto" loading="lazy" />
  <figcaption>Demonstration of a yes/no block.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Yes/No</code>. You can now customize this block to your needs with the following features:</p>

<h3 id="features-general">General</h3>
<ul>
  <li><strong>Text</strong><br/>Use the <code>Text</code> feature for the name/title of this block.</li>
  <li><strong>Description</strong><br/>Enable the <code>Description</code> feature to add a description to this block.</li>
  <li><strong>Image</strong><a href="{{ page.base }}help/articles/how-to-use-images-and-videos-in-your-form/" class="article-more-info" title="More information about images"><i class="fas fa-info-circle"></i></a><br/>Enable the <code>Image</code> feature to add an image to this block.</li>
  <li><strong>Help text</strong><br/>Enable the <code>Help text</code> feature to add a help text to this block.</li>
</ul>

<h3 id="features-settings">Settings</h3>
<ul>
  <li><strong>Labels</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about labels"><i class="fas fa-info-circle"></i></a><br/>By default the answer will be shown and stored as <code>Yes</code> or <code>No</code>. Enable the <code>Labels</code> feature to overwrite these labels in your dataset with your own values.</li>
  <li><strong>Colors</strong><br/>Enable the <code>Colors</code> feature to configure alternative colors for the yes and no buttons in this question block. Simply select a color from the color control, or enter a color in RGB(A) format or HEX format. These colors overrule the default button colors from the <a href="{{ page.base }}help/articles/how-to-style-your-forms/">Styles</a> pane.</li>
</ul>

<h3 id="features-options">Options</h3>
<ul>
  <li><strong>Required</strong><br/>By default this block is not required to fill out by your respondents. Enable the <code>Required</code> feature to make this block required to fill out.</li>
  <li><strong>Visibility</strong><br/>By default this block is visible in your form. Enable the <code>Visibility</code> feature to hide it for your respondents.</li>
  <li><strong>Score</strong><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/" class="article-more-info" title="More information about scores"><i class="fas fa-info-circle"></i></a><br/>You can attach scores to the yes and no options to perform instant calculations. Enable the <code>Score</code> feature to enter the desired score values.</li>
  <li><strong>Alias</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about alias"><i class="fas fa-info-circle"></i></a><br/>By default the name/title of this block will be used in your dataset. Enable the <code>Alias</code> feature to use a different name in your dataset.</li>
  <li><strong>Exportability</strong><a href="{{ page.base }}help/articles/how-to-determine-what-data-fields-get-saved/" class="article-more-info" title="More information about exportability"><i class="fas fa-info-circle"></i></a><br/>By default the collected data of this block will be stored to the dataset of each result. Enable the <code>Exportability</code> feature to take control over the storage. You can then include/exclude the data from your dataset. Please note that if you exclude data from the dataset you can never see and/or use the given answers from your respondents to this block, because the data is simply not stored at all.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-yes-no.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Advanced settings of the yes/no block.</figcaption>
</figure>
<hr />

<h2 id="logic">Logic<a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" class="article-more-info" title="More information about logic"><i class="fas fa-info-circle"></i></a></h2>
<p>Logic is important to make your forms smart and conversational. The yes/no block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Yes selected;</li>
  <li>No selected.</li>
</ul>
<h3 id="evaluate-conditions">Evaluate conditions</h3>
<ul>
  <li>Value matches <code>your filter</code>;</li>
  <li>Value does not match <code>your filter</code>;</li>
  <li>Value contains <code>your filter</code>;</li>
  <li>Value does not contain <code>your filter</code>;</li>
  <li>Value starts with <code>your filter</code>;</li>
  <li>Value ends with <code>your filter</code>;</li>
  <li>Value is empty;</li>
  <li>Value is not empty.</li>
</ul>
<h3 id="score-conditions">Score conditions</h3>
<ul>
  <li>Score is equal to <code>your filter</code>;</li>
  <li>Score is not equal to <code>your filter</code>;</li>
  <li>Score is lower than<code>your filter</code>;</li>
  <li>Score is higher than <code>your filter</code>;</li>
  <li>Score is between <code>your filters</code>;</li>
  <li>Score is not between <code>your filters</code>;</li>
  <li>Score is calculated;</li>
  <li>Score is not calculated.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Text - Compare with a fixed text that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers/">more info</a>).</li>
</ul>
<hr />

<h2 id="calculations">Calculations<a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" class="article-more-info" title="More information about calculations"><i class="fas fa-info-circle"></i></a></h2>
<p>You can use the <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">calculator block</a> to perform calculations with given answers. The yes/no block supports the following calculation operations:</p>
<ul>
  <li>Score - Calculate a score based on the entered score list for yes and no.</li>
</ul>
<hr />

{% include help-article-blocks.html %}
