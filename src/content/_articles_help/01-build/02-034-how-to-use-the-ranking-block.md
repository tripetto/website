---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-ranking-block/
title: Use the ranking block - Tripetto Help Center
description: Learn everything you need to know to use the ranking block in your forms.
article_title: How to use the ranking block
article_folder: editor-block-ranking
author: jurgen
time: 5
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
---
<p>Learn everything you need to know to use the ranking block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the ranking block to let your respondents compose a ranking from a list of options you provide them. The respondent can simply drag-and-drop each item to the desired position in the ranking, or select the desired position from each option's dropdown. The ranking block can work in 2 modes:</p>
<ul>
  <li><strong>Full ranking</strong> - Let your respondents compose a full ranking of all the options you give them.</li>
  <li><strong>Top ranking</strong> - Let your respondents select a certain amount from the options that you give them and let them compose a ranking for their selected options. For example to compose a top-3 from a list of 10 options.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/blocks/ranking.gif" width="1200" height="700" alt="Screenshot of ranking block in Tripetto" loading="lazy" />
  <figcaption>Demonstration of a ranking block.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Ranking</code>. You can now customize this block to your needs with the following features:</p>

<h3 id="features-general">General</h3>
<ul>
  <li><strong>Text</strong><br/>Use the <code>Text</code> feature for the name/title of this block.</li>
  <li><strong>Description</strong><br/>Enable the <code>Description</code> feature to add a description to this block.</li>
  <li><strong>Help text</strong><br/>Enable the <code>Help text</code> feature to add a help text to this block.</li>
</ul>

<h3 id="features-settings">Settings</h3>
<ul>
  <li><strong>Limits</strong><br/>Enable the <code>Limits</code> feature to enter a maximum number of options that respondents can rank. If the amount of options is larger than the entered limit, the ranking block will display the options inside that limit as selected.</li>
  <li><strong>Randomization</strong><br/>Enable the <code>Randomization</code> feature to present the options in a random order to your respondents (using <a href="https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle" target="_blank" rel="noopener noreferrer">Fisher–Yates shuffle</a>).</li>
</ul>

<h3 id="features-options">Options</h3>
<ul>
  <li><strong>Required</strong><br/>By default this block is not required to fill out by your respondents. Enable the <code>Required</code> feature to make this block required to fill out.</li>
  <li><strong>Visibility</strong><br/>By default this block is visible in your form. Enable the <code>Visibility</code> feature to hide it for your respondents.</li>
  <li><strong>Score</strong><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/" class="article-more-info" title="More information about scores"><i class="fas fa-info-circle"></i></a><br/>You can attach scores to the options to perform instant calculations. Enable the <code>Score</code> feature to enter the desired score values. If you enabled the <code>Limits</code> feature, only the selected options will add up to the score.</li>
  <li><strong>Alias</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about alias"><i class="fas fa-info-circle"></i></a><br/>By default the name/title of this block will be used in your dataset. Enable the <code>Alias</code> feature to use a different name in your dataset.</li>
  <li><strong>Data format</strong><br/>By default the collected data of this block is stored as a separate value per ranking position. Enable the <code>Data format</code> feature to determine how the data is stored in the dataset:
    <ul>
      <li><strong>Every position as a separate field</strong> - Every position is included in the dataset as a separate value, meaning the selected option for that position.</li>
      <li><strong>Single text field with the ranking</strong> - The ranking is merged into a single string of text, separated using a configurable separator.</li>
      <li><strong>Both</strong> - Includes every ranking position in the dataset, together with the concatenated text.</li>
    </ul>
  </li>
  <li><strong>Exportability</strong><a href="{{ page.base }}help/articles/how-to-determine-what-data-fields-get-saved/" class="article-more-info" title="More information about exportability"><i class="fas fa-info-circle"></i></a><br/>By default the collected data of this block will be stored to the dataset of each result. Enable the <code>Exportability</code> feature to take control over the storage. You can then include/exclude the data from your dataset. Please note that if you exclude data from the dataset you can never see and/or use the given answers from your respondents to this block, because the data is simply not stored at all.</li>
</ul>

<h3 id="ranking-options">Ranking options</h3>
<p>Next, you can enter the list of ranking options that you want to provide your respondents:</p>
<ul>
  <li><strong>Add options one by one</strong><br/>Click the <code><i class="fas fa-plus"></i></code> icon at the bottom of the list and enter the option name.</li>
  <li><strong>Import a list of options</strong><br/>Click the <code><i class="fas fa-download"></i></code> icon at the top of the list. Now supply a list with one option name per text line and click <code>Import</code> to add them all at once to your list of ranking options.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/import.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Importing options in the ranking block.</figcaption>
</figure>
<p>From your list of options you can open each option to get to the settings of that single option. Over there you can use these features:</p>
<ul>
  <li><strong>Name</strong><br/>Use the <code>Name</code> feature for the name/title of this option.</li>
  <li><strong>Description</strong><br/>Enable the <code>Description</code> feature to extend the option name with a description, for example to show an extra explanation. This description is shown below the name.</li>
  <li><strong>Identifier</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about identifiers"><i class="fas fa-info-circle"></i></a><br/>By default the name of each option will be used in your dataset. Enable the <code>Identifier</code> feature to use a different name in your dataset.</li>
  <li><strong>Score</strong><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/" class="article-more-info" title="More information about scores"><i class="fas fa-info-circle"></i></a><br/>Enable the <code>Score</code> feature to enter the desired score value for that option.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/option.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Advanced options of the ranking options of the ranking block.</figcaption>
</figure>
<hr />

<h2 id="logic">Logic<a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" class="article-more-info" title="More information about logic"><i class="fas fa-info-circle"></i></a></h2>
<p>Logic is important to make your forms smart and conversational. The ranking block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Selected option per ranking position.</li>
</ul>
<h3 id="evaluate-conditions">Evaluate conditions</h3>
<ul>
  <li>Value of ranking position matches <code>your filter</code>;</li>
  <li>Value of ranking position does not match <code>your filter</code>;</li>
  <li>Value of ranking position contains <code>your filter</code>;</li>
  <li>Value of ranking position does not contain <code>your filter</code>;</li>
  <li>Value of ranking position starts with <code>your filter</code>;</li>
  <li>Value of ranking position ends with <code>your filter</code>;</li>
  <li>Value of ranking position is empty;</li>
  <li>Value of ranking position is not empty.</li>
</ul>
<h3 id="score-conditions">Score conditions</h3>
<p>Conditions based on the total score of the selected options:</p>
<ul>
  <li>Total score is equal to <code>your filter</code>;</li>
  <li>Total score is not equal to <code>your filter</code>;</li>
  <li>Total score is lower than<code>your filter</code>;</li>
  <li>Total score is higher than <code>your filter</code>;</li>
  <li>Total score is between <code>your filters</code>;</li>
  <li>Total score is not between <code>your filters</code>;</li>
  <li>Total score is calculated;</li>
  <li>Total score is not calculated.</li>
</ul>
<p>Conditions based on the score of each individual ranking position:</p>
<ul>
  <li>Position score is equal to <code>your filter</code>;</li>
  <li>Position score is not equal to <code>your filter</code>;</li>
  <li>Position score is lower than<code>your filter</code>;</li>
  <li>Position score is higher than <code>your filter</code>;</li>
  <li>Position score is between <code>your filters</code>;</li>
  <li>Position score is not between <code>your filters</code>;</li>
  <li>Position score is calculated;</li>
  <li>Position score is not calculated.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Text - Compare with a fixed text that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers/">more info</a>).</li>
</ul>
<hr />

<h2 id="calculations">Calculations<a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" class="article-more-info" title="More information about calculations"><i class="fas fa-info-circle"></i></a></h2>
<p>You can use the <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">calculator block</a> to perform calculations with given answers. The ranking block supports the following calculation operations:</p>
<ul>
  <li>Ranking position:
    <ul>
      <li>Score - Calculate a score based on the score of the selected option at that position;</li>
      <li>Compare - Compare the name of the selected option at that position and output a value based on the result of the comparison;</li>
      <li>Character count - Count the number of characters in the name of the selected option at that position;</li>
      <li>Word count - Count the number of words in the name of the selected option at that position;</li>
      <li>Line count - Count the number of lines in the name of the selected option at that position;</li>
      <li>Count occurrences - Count the number of occurrences of a certain text, character or regular expression in the name of the selected option at that position;</li>
      <li>Convert to number - Convert the name of the selected option at that position to a number value.</li>
    </ul>
  </li>
  <li>Score (total) - Calculate a score based on the total score of the selected options;</li>
  <li>Score (per ranking position) - Output a value based on the score of the selected option at a certain ranking position.</li>
</ul>
<hr />

{% include help-article-blocks.html %}
