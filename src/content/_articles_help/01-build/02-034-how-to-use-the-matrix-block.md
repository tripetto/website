---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-matrix-block/
title: Use the matrix block - Tripetto Help Center
description: Learn everything you need to know to use the matrix block in your forms.
article_title: How to use the matrix block
article_folder: editor-block-matrix
author: jurgen
time: 5
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-matrix-block/
---
<p>Learn everything you need to know to use the matrix block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the matrix block to let your respondents choose from a set of options (columns) for multiple questions/statements (rows).</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/matrix.gif" width="1200" height="700" alt="Screenshot of a matrix block in Tripetto" loading="lazy" />
  <figcaption>Demonstration of a matrix block.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Matrix</code>. You can now customize this block to your needs with the following features:</p>

<h3 id="features-general">General</h3>
<ul>
  <li><strong>Text</strong><br/>Use the <code>Text</code> feature for the name/title of this block.</li>
  <li><strong>Description</strong><br/>Enable the <code>Description</code> feature to add a description to this block.</li>
  <li><strong>Help text</strong><br/>Enable the <code>Help text</code> feature to add a help text to this block.</li>
</ul>

<h3 id="features-options">Options</h3>
<ul>
  <li><strong>Required</strong><br/>By default this block is not required to fill out by your respondents. Enable the <code>Required</code> feature to make this block required to fill out.</li>
  <li><strong>Visibility</strong><br/>By default this block is visible in your form. Enable the <code>Visibility</code> feature to hide it for your respondents.</li>
  <li><strong>Score</strong><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/" class="article-more-info" title="More information about scores"><i class="fas fa-info-circle"></i></a><br/>You can attach scores to the choices to perform instant calculations. Enable the <code>Score</code> feature to enter the desired score values.</li>
  <li><strong>Exportability</strong><a href="{{ page.base }}help/articles/how-to-determine-what-data-fields-get-saved/" class="article-more-info" title="More information about exportability"><i class="fas fa-info-circle"></i></a><br/>By default the collected data of this block will be stored to the dataset of each result. Enable the <code>Exportability</code> feature to take control over the storage. You can then include/exclude the data from your dataset. Please note that if you exclude data from the dataset you can never see and/or use the given answers from your respondents to this block, because the data is simply not stored at all.</li>
</ul>

<h3 id="rows">Rows</h3>
<p>Next, you can enter the list of rows that you want to show to your respondents:</p>
<ul>
  <li><strong>Add rows one by one</strong><br/>Click the <code><i class="fas fa-plus"></i></code> icon at the bottom of the list and enter the row name.</li>
  <li><strong>Import a list of rows</strong><br/>Click the <code><i class="fas fa-download"></i></code> icon at the top of the list. Now supply a list with one row name per text line and click <code>Import</code> to add them all at once to your list of rows.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/import.gif" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Importing rows in the matrix block.</figcaption>
</figure>
<p>From your list of rows you can open each row to get to the settings of that single row. Over there you can use these features:</p>
<ul>
  <li><strong>Row</strong><br/>This is the main text of the row.</li>
  <li><strong>Description</strong><br/>Enable the <code>Description</code> feature to extend the row name with a description, for example to show an extra explanation. This description is shown below the name.</li>
  <li><strong>Required</strong><br/>You can make all matrix rows required by enabling the <code>Required</code> feature on the whole block, but you can also determine this per row. Enable the <code>Required</code> feature per row to mark specific rows to be required. In that way not all rows are required, but only the ones you mark.</li>
  <li><strong>Identifier</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about identifiers"><i class="fas fa-info-circle"></i></a><br/>By default the name of each row will be used in your dataset. Enable the <code>Identifier</code> feature to use a different name in your dataset.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-matrix.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Advanced options of the rows of the matrix block.</figcaption>
</figure>
<hr />

<h3 id="columns">Columns</h3>
<p>For the matrix block you can enter the list of columns that your respondents can choose from:</p>
<ul>
  <li><strong>Add columns one by one</strong><br/>Click the <code><i class="fas fa-plus"></i></code> icon at the bottom of the list and enter the column name.</li>
  <li><strong>Import a list of columns</strong><br/>Click the <code><i class="fas fa-download"></i></code> icon at the top of the list. Now supply a list with one column name per text line and click <code>Import</code> to add them all at once to your list of columns.</li>
</ul>
<p>From your list of columns you can open each column to get to the settings of that single column. Over there you can use these features:</p>
<ul>
  <li><strong>Column</strong><br/>This is the main text of the column.</li>
  <li><strong>Identifier</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about identifiers"><i class="fas fa-info-circle"></i></a><br/>By default the name of each column will be used in your dataset. Enable the <code>Identifier</code> feature to use a different name in your dataset.</li>
  <li><strong>Score</strong><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/" class="article-more-info" title="More information about scores"><i class="fas fa-info-circle"></i></a><br/>Enable the <code>Score</code> feature to enter the desired score value for that column.</li>
</ul>
<hr/>

<h2 id="logic">Logic<a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" class="article-more-info" title="More information about logic"><i class="fas fa-info-circle"></i></a></h2>
<p>Logic is important to make your forms smart and conversational. The matrix block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Answer state (per row).</li>
</ul>
<h3 id="evaluate-conditions">Evaluate conditions</h3>
<ul>
  <li>Value matches <code>your filter</code>;</li>
  <li>Value does not match <code>your filter</code>;</li>
  <li>Value contains <code>your filter</code>;</li>
  <li>Value does not contain <code>your filter</code>;</li>
  <li>Value starts with <code>your filter</code>;</li>
  <li>Value ends with <code>your filter</code>;</li>
  <li>Value is empty;</li>
  <li>Value is not empty.</li>
</ul>
<h3 id="score-conditions">Score conditions</h3>
<ul>
  <li>Score is equal to <code>your filter</code>;</li>
  <li>Score is not equal to <code>your filter</code>;</li>
  <li>Score is lower than<code>your filter</code>;</li>
  <li>Score is higher than <code>your filter</code>;</li>
  <li>Score is between <code>your filters</code>;</li>
  <li>Score is not between <code>your filters</code>;</li>
  <li>Score is calculated;</li>
  <li>Score is not calculated.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Text - Compare with a fixed text that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers/">more info</a>).</li>
</ul>
<hr />

<h2 id="calculations">Calculations<a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" class="article-more-info" title="More information about calculations"><i class="fas fa-info-circle"></i></a></h2>
<p>You can use the <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">calculator block</a> to perform calculations with given answers. The matrix block supports the following calculation operations:</p>
<ul>
  <li>Score - Calculate a total matrix score based on the entered score list for columns;</li>
  <li>Row score - Calculate a row score for an individual matrix row based on the entered score list for columns.</li>
</ul>
<hr />

{% include help-article-blocks.html %}
