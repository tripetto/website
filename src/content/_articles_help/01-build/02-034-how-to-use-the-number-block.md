---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-number-block/
title: Use the number block - Tripetto Help Center
description: Learn everything you need to know to use the number block in your forms.
article_title: How to use the number block
article_folder: editor-block-number
author: jurgen
time: 4
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-number-block/
---
<p>Learn everything you need to know to use the number block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the number block to let your respondents enter a number, including automated checks for valid number inputs.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/number.gif" width="1200" height="700" alt="Screenshot of a number block in Tripetto" loading="lazy" />
  <figcaption>Demonstration of a number block.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Number</code>. You can now customize this block to your needs with the following features:</p>

<h3 id="features-general">General</h3>
<ul>
  <li><strong>Text</strong><br/>Use the <code>Text</code> feature for the name/title of this block.</li>
  <li><strong>Description</strong><br/>Enable the <code>Description</code> feature to add a description to this block.</li>
  <li><strong>Placeholder</strong><br/>Enable the <code>Placeholder</code> feature to add a text inside the empty input control of this block.</li>
  <li><strong>Help text</strong><br/>Enable the <code>Help text</code> feature to add a help text to this block.</li>
</ul>

<h3 id="features-settings">Settings</h3>
<ul>
  <li><strong>Format</strong><br/>Enable the <code>Format</code> feature to determine how many decimal places your number input may contain.</li>
  <li><strong>Limits</strong><br/>Enable the <code>Limits</code> feature to enter a minimum and maximum value that respondents can enter.</li>
  <li><strong>Prefix</strong><br/>Enable the <code>Prefix</code> feature to enter a label that's shown in front of the entered number value. Optionally you can specify a different prefix for plural values.</li>
  <li><strong>Suffix</strong><br/>Enable the <code>Suffix</code> feature to enter a label that's shown at the end of the entered number value. Optionally you can specify a different suffix for plural values.</li>
  <li><strong>Signs</strong><br/>Enable the <code>Signs</code> feature to set what signs to use as decimal sign and thousands separator. Please note that in your forms these signs can be overwritten by the respondent's locale settings. In your dataset the selected signs always will be used.</li>
</ul>

<h3 id="features-options">Options</h3>
<ul>
  <li><strong>Prefill</strong><a href="{{ page.base }}help/articles/how-to-prefill-question-blocks-with-initial-values/" class="article-more-info" title="More information about prefilling"><i class="fas fa-info-circle"></i></a><br/>Enable the <code>Prefill</code> feature to set a fixed initial value of this block.</li>
  <li><strong>Calculator</strong><a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" class="article-more-info" title="More information about calculator"><i class="fas fa-info-circle"></i></a><br/>Enable the <code>Calculator</code> feature to instantly use <a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/">all calculator features</a> inside the number block.</li>
  <li><strong>Required</strong><br/>By default this block is not required to fill out by your respondents. Enable the <code>Required</code> feature to make this block required to fill out.</li>
  <li><strong>Visibility</strong><br/>By default this block is visible in your form. Enable the <code>Visibility</code> feature to hide it for your respondents.</li>
  <li><strong>Alias</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about alias"><i class="fas fa-info-circle"></i></a><br/>By default the name/title of this block will be used in your dataset. Enable the <code>Alias</code> feature to use a different name in your dataset.</li>
  <li><strong>Exportability</strong><a href="{{ page.base }}help/articles/how-to-determine-what-data-fields-get-saved/" class="article-more-info" title="More information about exportability"><i class="fas fa-info-circle"></i></a><br/>By default the collected data of this block will be stored to the dataset of each result. Enable the <code>Exportability</code> feature to take control over the storage. You can then include/exclude the data from your dataset. Please note that if you exclude data from the dataset you can never see and/or use the given answers from your respondents to this block, because the data is simply not stored at all.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-number.png" width="1200" height="1129" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Advanced options of the number block.</figcaption>
</figure>
<hr />

<h2 id="logic">Logic<a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" class="article-more-info" title="More information about logic"><i class="fas fa-info-circle"></i></a></h2>
<p>Logic is important to make your forms smart and conversational. The number block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Number is equal to <code>your filter</code>;</li>
  <li>Number is not equal to <code>your filter</code>;</li>
  <li>Number is lower than<code>your filter</code>;</li>
  <li>Number is higher than <code>your filter</code>;</li>
  <li>Number is between <code>your filters</code>;</li>
  <li>Number is not between <code>your filters</code>;</li>
  <li>Number is empty;</li>
  <li>Number is not empty.</li>
</ul>
<h3 id="calculation-conditions">Calculation conditions</h3>
<ul>
  <li>Calculation is equal to <code>your filter</code>;</li>
  <li>Calculation is not equal to <code>your filter</code>;</li>
  <li>Calculation is lower than<code>your filter</code>;</li>
  <li>Calculation is higher than <code>your filter</code>;</li>
  <li>Calculation is between <code>your filters</code>;</li>
  <li>Calculation is not between <code>your filters</code>;</li>
  <li>Calculation is valid;</li>
  <li>Calculation is not valid.</li>
</ul>
<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter(s)</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Number - Compare with a fixed number that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers/">more info</a>).</li>
</ul>
<hr />

<h2 id="calculations">Calculations<a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" class="article-more-info" title="More information about calculations"><i class="fas fa-info-circle"></i></a></h2>
<p>You can use the <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">calculator block</a> to perform calculations with given answers. The number block supports the following calculation operations:</p>
<ul>
  <li>Current value - Supply an entered number;</li>
  <li>Compare - Compare a number and output a value based on the result of the comparison;</li>
  <li>Functions - Execute all kinds of <a href="{{ page.base }}calculator-features/">mathematical functions</a> to a number.</li>
</ul>
<hr />
{% include help-article-blocks.html %}
