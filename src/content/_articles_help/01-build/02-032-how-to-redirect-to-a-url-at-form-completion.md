---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-redirect-to-a-url-at-form-completion/
title: Redirect to a URL at form completion - Tripetto Help Center
description: Learn how you can automatically redirect respondents to an external URL after form completion.
article_title: How to redirect to a URL at form completion
article_id: closing
article_folder: editor-end-redirect
article_video: build-redirect
author: jurgen
time: 1
time_video: 1
category_id: build
subcategory: build_messages
areas: [studio, wordpress]
---
<p>Learn how you can automatically redirect respondents to an external URL after form completion.</p>

<h2 id="when-to-use">When to use</h2>
<p>After respondents completed your form, you can show them a <a href="{{ page.base }}help/articles/how-to-add-a-closing-message/"> closing message</a>, but you can also redirect them to an external URL, for example your own website.</p>
<blockquote>
  <h4>📌 Also see: Flexible redirects</h4>
  <p>Redirects are part of the closing message. Closing messages are way more flexible than just one closing message for all your respondents. In this current article we will show how to redirect the common closing message, but you can also add unlimited flexible closing messages with redirects in it, based on the given answers of your respondents.</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-add-one-or-multiple-closing-messages/">How to add one or multiple closing messages</a></li>
  </ul>
</blockquote>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>To set a redirect at the end of your form, you have to <a href="{{ page.base }}help/articles/how-to-add-a-closing-message/">enable the closing message</a> inside the form builder. To do so, click the red bubble with a <code><i class="fas fa-sign-out-alt"></i></code> icon at the bottom of the form.</p>

<h3 id="redirect">Enable redirect</h3>
<p>Now that you've opened the closing message pane, you can enable the feature <code>Redirect</code> on the left.</p>
<p>This will disable all other features for the closing message, as your respondents won't see those anymore. Instead you can now enter the URL (including <code>https://</code>) you want to redirect to.</p>
<p>And that's it! Your form will now save the entry at completion and then redirect to the given URL. There is no longer a closing message visible.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-redirect.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>The settings to redirect your form at completion.</figcaption>
</figure>

<h3 id="piping">Recall values</h3>
<figure class="inline-right">
  <img src="{{ page.base }}images/help/editor-end/02-identifier.gif" width="470" height="164" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Add the identification number of the entry in the redirect URL.</figcaption>
</figure>
<p>In the redirect URL you can use <a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/">piping logic</a> to add answers from your respondent to the URL that you redirect to. To do that, simply type the <code>@</code> sign at the desired position in your redirect URL and select the block from your form that you want to recall the value from.</p>
<blockquote>
  <h4>📣 Info: About query strings</h4>
  <p>A query string is an often used method to send data in a URL. Some background information on how to supply a query string:</p>
  <ul>
    <li>You can add multiple parameters to a query string;</li>
    <li>Each parameter consists of a name and a value, separated by a <code> = </code> sign, for example <code>name=value</code>;</li>
    <li>The first parameter is preceded by a <code> ? </code> sign;</li>
    <li>Any following parameters are preceded by a <code> & </code> sign;</li>
  </ul>
  <p>A full example could be something like this: <code>https://yoursite.com/?name=abc&city=klm&country=xyz</code>.</p>
</blockquote>

<h4>Identification number</h4>
<p>On top of the given answers from a submission, there is an extra piping value available in the closing message: the <code>Identification number</code>. This is the unique identification number of each entry by which you can identify/track a certain entry on the URL that's opened. This even enables you to <a href="{{ page.base }}help/articles/how-to-share-response-data-between-forms/">share response data between different Tripetto forms</a>.</p>
