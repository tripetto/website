---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-let-the-live-preview-work-for-you/
title: Let the live preview work for you - Tripetto Help Center
description: Learn how the live preview actively helps you to build your forms.
article_title: How to let the live preview work for you
article_id: preview
article_video: build-simple-form
author: martijn
time: 3
time_video: 2
category_id: build
subcategory: build_basics
areas: [studio, wordpress]
---
<p>While building your form, you can immediately see what you're working on in the <strong>live preview</strong>. Let us take a closer look at how the preview can help you while building your forms.</p>

<h2 id="preview">About live preview</h2>
<p>The live form preview helps you to see what you're building. Every little change you do in the form builder gets updated in the preview right away. From a simple text change to changing a question type, to rearranging the order of questions. All changes have an immediate effect on the live preview.</p>
<p>Also changes in the <a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/">form face</a>, <a href="{{ page.base }}help/articles/how-to-style-your-forms/">styling</a> and <a href="{{ page.base }}help/articles/how-to-edit-or-translate-all-text-labels-in-your-forms/">translations</a> get executed right away, so you can play with all these customizations very easily and quickly.</p>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>The preview is always shown on the right side of the form builder, so you can see your form structure and the form itself side-by-side.</p>

<h3 id="modes">Logic modes</h3>
<p>At the top of the preview pane you can switch between two modes that determine if the logic should be applied to the preview form:</p>
<ul>
  <li><strong>All without logic</strong> - Click <code>All without logic</code> to display all question blocks in the preview, without executing any logic you might have built. You can use this mode if you're still designing the outline of your form, so you can quickly edit texts, question types and the order of blocks;</li>
  <li><strong>Test with logic</strong> - Click <code>Test with logic</code> to display the form like your respondents will use your form, including the logic you have built. Click the <code><i class="fas fa-sync"></i></code> icon at the top of the preview to start the test form from the beginning.</li>
</ul>
<blockquote>
  <h4>💡 Tip: Check logic mode</h4>
  <p>If you want to test your form as if you were a real respondent, make sure that the preview mode is set to <code>Test with logic</code>, so all logic gets executed in the form preview.</p>
</blockquote>

<h3 id="device">Device types</h3>
<p>Of course, all forms are designed in a responsive way for optimal usage on all modern devices. In the top bar, you can switch the preview to different device sizes, to see how the form looks at each device type:</p>
<ul>
  <li><strong>Mobile</strong> - Click the <code><i class="fas fa-mobile-alt"></i></code> icon to preview the form on a small screen size;</li>
  <li><strong>Tablet</strong> - Click the <code><i class="fas fa-tablet-alt"></i></code> icon to preview the form on a medium screen size;</li>
  <li><strong>Desktop</strong> - Click the <code><i class="fas fa-desktop"></i></code> icon to preview the form on a large screen size.</li>
</ul>

<h3 id="edit">Quick edit</h3>
<p>From the preview, you can quickly jump to a specific block in the form builder to start editing that block, so you don't have to search inside your form structure to edit a specific question block.</p>
<p>To do so, make sure the preview mode is set to <code>All without logic</code> mode, so all questions in your form are shown in the preview. Then click the title of the desired block inside the preview to open it in the form builder. You can now edit that block immediately and of course see those changes in the preview right away.</p>
