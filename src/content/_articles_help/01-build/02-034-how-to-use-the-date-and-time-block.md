---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-date-and-time-block/
title: Use the date (and time) block - Tripetto Help Center
description: Learn everything you need to know to use the date (and time) block in your forms.
article_title: How to use the date (and time) block
article_folder: editor-block-date
author: jurgen
time: 4
category_id: build
subcategory: build_blocks
areas: [studio, wordpress]
redirect_from:
- /help/articles/discover-advanced-options-of-the-date-block/
---
<p>Learn everything you need to know to use the date (and time) block in your forms.</p>

<h2 id="when-to-use">When to use</h2>
<p>Use the date block to let your respondents select a date and optionally a time, for example to make an appointment.</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/date.gif" width="1200" height="700" alt="Screenshot of a date block Tripetto" loading="lazy" />
  <figcaption>Demonstration of a date selection block.</figcaption>
</figure>
<hr />

<h2 id="how-to-use">How to use</h2>
<p>Add a new block to your form and then select the question type <code>Date</code>. You can now customize this block to your needs with the following features:</p>

<h3 id="features-general">General</h3>
<ul>
  <li><strong>Text</strong><br/>Use the <code>Text</code> feature for the name/title of this block.</li>
  <li><strong>Description</strong><br/>Enable the <code>Description</code> feature to add a description to this block.</li>
  <li><strong>Placeholder</strong><br/>Enable the <code>Placeholder</code> feature to add a text inside the empty input control of this block.</li>
  <li><strong>Help text</strong><br/>Enable the <code>Help text</code> feature to add a help text to this block.</li>
</ul>

<h3 id="features-settings">Settings</h3>
<ul>
  <li><strong>Time</strong><br/>Enable the <code>Time</code> feature to let your respondents also enter a time with the date.</li>
  <li><strong>Range</strong><br/>Enable the <code>Range</code> feature to let the block ask for two dates, namely a <code>From</code> date and a <code>To</code> date. The form will automatically show two date fields if this feature is enabled.</li>
  <li><strong>Limits</strong><br/>Enable the <code>Limits</code> feature to determine in between what dates the answered date(s) have to be. You can select the minimum and maximum date (including an optional time). Next to static date limits you can also determine if the date that your respondent selects has to be <code>In the future</code> and/or <code>In the past</code>. This will be checked at the moment the respondent fills out the form.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-date-settings.png" width="1200" height="760" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Settings of the date block.</figcaption>
</figure>

<h3 id="features-options">Options</h3>
<ul>
  <li><strong>Required</strong><br/>By default this block is not required to fill out by your respondents. Enable the <code>Required</code> feature to make this block required to fill out.</li>
  <li><strong>Visibility</strong><br/>By default this block is visible in your form. Enable the <code>Visibility</code> feature to hide it for your respondents.</li>
  <li><strong>Alias</strong><a href="{{ page.base }}help/articles/how-to-optimize-your-exported-data-using-aliases-identifiers-and-labels/" class="article-more-info" title="More information about alias"><i class="fas fa-info-circle"></i></a><br/>By default the name/title of this block will be used in your dataset. Enable the <code>Alias</code> feature to use a different name in your dataset.</li>
  <li><strong>Exportability</strong><a href="{{ page.base }}help/articles/how-to-determine-what-data-fields-get-saved/" class="article-more-info" title="More information about exportability"><i class="fas fa-info-circle"></i></a><br/>By default the collected data of this block will be stored to the dataset of each result. Enable the <code>Exportability</code> feature to take control over the storage. You can then include/exclude the data from your dataset. Please note that if you exclude data from the dataset you can never see and/or use the given answers from your respondents to this block, because the data is simply not stored at all.</li>
</ul>
<hr />

<h2 id="logic">Logic<a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" class="article-more-info" title="More information about logic"><i class="fas fa-info-circle"></i></a></h2>
<p>Logic is important to make your forms smart and conversational. The date block can work with the following <a href="{{ page.base }}help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/">branch conditions</a> to help you with that:</p>
<h3 id="block-conditions">Block conditions</h3>
<ul>
  <li>Date (and time) is equal to <code>your filter</code>;</li>
  <li>Date (and time) is not equal to <code>your filter</code>;</li>
  <li>Date (and time) is before <code>your filter</code>;</li>
  <li>Date (and time) is after <code>your filter</code>;</li>
  <li>Date (and time) is between <code>your filters</code>;</li>
  <li>Date (and time) is not between <code>your filters</code>;</li>
  <li>Date (and time) is empty;</li>
  <li>Date (and time) is not empty.</li>
</ul>
<p>If the <code>Range</code> is enabled, you can use these conditions for the <code>From</code> date and the <code>To</code> date separately.</p>

<h3 id="filters">Filters</h3>
<p>When we mention <code>your filter(s)</code> above, there are some different filters that you can use to make the right comparison:</p>
<ul>
  <li>Current date/time - Compare with the current date (and time) of the moment of form usage by a respondent;</li>
  <li>Fixed date/time - Compare with a fixed date (and time) that you enter;</li>
  <li>Value - Compare with another block value entered in the form by a respondent (<a href="{{ page.base }}help/articles/how-to-compare-given-answers/">more info</a>).</li>
</ul>
<hr />

<h2 id="calculations">Calculations<a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" class="article-more-info" title="More information about calculations"><i class="fas fa-info-circle"></i></a></h2>
<p>You can use the <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">calculator block</a> to perform calculations with given answers. The date block supports the following calculation operations:</p>
<ul>
  <li>Compare - Compare a date and output a value based on the result of the comparison;</li>
  <li>Age - Calculate the age (time) between two points in time;</li>
  <li>Year - Retrieve the year of a date;</li>
  <li>Month - Retrieve the month of a date (1-12);</li>
  <li>Day of month - Retrieve the day of month of a date (1-31);</li>
  <li>Day of week - Retrieve the day of week of a date (0-6).</li>
</ul>
<hr />
{% include help-article-blocks.html %}
