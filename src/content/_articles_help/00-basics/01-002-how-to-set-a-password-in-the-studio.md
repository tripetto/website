---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-set-a-password-for-your-account-in-the-studio/
title: Set a password for your account in the studio - Tripetto Help Center
description: You can set a password to your studio account, so you can use that to quickly login the next time.
article_title: How to set a password for your account in the studio
article_folder: studio-account-password
author: martijn
time: 1
category_id: studio-basics
subcategory: basics_account
areas: [studio]
---
<p>You can set a password to your studio account, so you can use that to quickly login the next time.</p>

<h2 id="password">About studio password</h2>
<p>Easiest way to login to your account is with the <a href="{{ page.base }}help/articles/how-to-login-to-an-existing-account-in-the-studio/">email login</a>, but it's also possible to set a password to your account. This is especially handy when you have multiple studio accounts that you regularly switch between, or when more than one person needs to access an account.</p>
<hr/>

<h2 id="set" data-anchor="Set password">Set your studio password</h2>
<p>To set a studio password, you first need to be logged in. If you're not logged in, you have to use the <a href="{{ page.base }}help/articles/how-to-login-to-an-existing-account-in-the-studio/">email login</a> one more time for now.</p>
<p>Once you're logged in, click the <code><i class="fas fa-user-lock"></i></code> icon at the top right corner of the studio<i class="fas fa-arrow-right"></i>Click <code>Account</code>. The Account pane will show up on the right side of the form builder.</p>
<p>In there, you can now go to <code>Change password</code>. Simply enter the password you'd like to set it to and confirm that password by entering it once more. Now click <code>Update password</code> and that's it!</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/set-password.png" width="1200" height="760" alt="Screenshot of the studio of Tripetto" loading="lazy" />
  <figcaption>Enter and confirm your new password in your studio account.</figcaption>
</figure>
<hr/>

<h2 id="login" data-anchor="Login with password">Login with studio password</h2>
<p>Next time you want to <a href="{{ page.base }}help/articles/how-to-login-to-an-existing-account-in-the-studio/">login to your studio account</a>, you can now select <code>Login with password</code> in the Login pane and enter your password to login directly, without the need of the email login.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/login.png" width="1200" height="760" alt="Screenshot of the studio of Tripetto" loading="lazy" />
  <figcaption>Login with your password.</figcaption>
</figure>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/login-password.png" width="1200" height="760" alt="Screenshot of the studio of Tripetto" loading="lazy" />
  <figcaption>Enter your password.</figcaption>
</figure>
