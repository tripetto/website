---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-configure-plugin-access-and-capabilities-with-wordpress-user-roles/
title: Configure plugin access and capabilities - Tripetto Help Center
description: Learn how to configure the access and capabilities of each WordPress user role in the plugin.
article_title: How to configure plugin access and capabilities with WordPress user roles
article_folder: wordpress-roles
author: mark
time: 4
category_id: wordpress-basics
subcategory: basics_settings
areas: [wordpress]
---
<p>The Tripetto WordPress plugin fully supports WordPress user roles. This enables you to configure which roles have access to the plugin and which capabilities each role has inside the plugin.</p>

<h2 id="about">About WordPress roles</h2>
<p>One of the great things about WordPress is that it has a built-in user roles concept, designed to give the site owner the ability to control what users can and cannot do within the site. Each user that has access to your WP Admin has one of those WordPress roles and with that the appropriate capabilities.</p>
<p>More information about WordPress roles and capabilities can be found in <a href="https://wordpress.org/support/article/roles-and-capabilities/" target="_blank" rel="noopener noreferrer">this support article at wordpress.org</a>.</p>
<hr/>

<h2 id="default-access">Default access to Tripetto</h2>
<p>The Tripetto WordPress plugin has fully implemented the usage of those WordPress roles. WordPress offers six pre-defined roles that Tripetto also uses to grant the right default access and capabilities:</p>
<ul>
  <li>✅ <strong>Super Admin</strong><br/>Full access to all functions of the Tripetto plugin, including the <a href="{{ page.base }}help/articles/how-to-configure-plugin-settings-for-email-sending-and-spam-protection/">general Tripetto settings</a> and the Freemius account page for license management.</li>
  <li>✅ <strong>Administrator</strong><br/>Full access to all functions of the Tripetto plugin, including the <a href="{{ page.base }}help/articles/how-to-configure-plugin-settings-for-email-sending-and-spam-protection/">general Tripetto settings</a> and, in case of single-site installation, the Freemius account page for license management.</li>
  <li>✅ <strong>Editor</strong><br/>Full access to all functions of the Tripetto plugin.</li>
  <li>❌ <strong>Author</strong><br/>No access to the Tripetto plugin.</li>
  <li>❌ <strong>Contributor</strong><br/>No access to the Tripetto plugin.</li>
  <li>❌ <strong>Subscriber</strong><br/>No access to the Tripetto plugin.</li>
</ul>
<hr/>

<h2 id="settings">Custom access to Tripetto</h2>
<p>If you want to overrule the default WordPress roles settings, you can take full control over the roles that have access and even over the exact capabilities you wish to grant to each role. Tripetto helps you to do that easily.</p>

<h3 id="access">Access to role settings</h3>
<p>Access to the role settings is handled by the overall WordPress capability <code>manage_options</code>. By default only users with <a href="https://wordpress.org/support/article/roles-and-capabilities/#manage_options" target="_blank" rel="noopener noreferrer">Super Admin or Administrator role</a> have that capability enabled.</p>

<h3 id="open">Open role settings</h3>
<p>Users with an appropriate role can open the role settings in two ways:</p>
<ul>
  <li><strong>In the Tripetto plugin</strong> - When you activate the Tripetto plugin for the first time or just installed an update to it, we will give you the option to follow an onboarding wizard. Part of that onboarding wizard is configuring the desired roles and capabilities (if you have the right WordPress capabilities to do so).<br/>This onboarding wizard is also available from the Tripetto dashboard to configure your settings at a later moment.</li>
  <li><strong>Via a third-party user roles plugin</strong> - Use a third-party user roles plugin to manage all roles and capabilities for your total WordPress environment (for example <a href="https://wordpress.org/plugins/user-role-editor/" target="_blank" rel="noopener noreferrer">User Role Editor</a>).</li>
</ul>
<p>In this article we will use the Tripetto onboarding wizard to configure the roles.</p>

<h3 id="custom-role-access">Custom role access</h3>
<p>In the onboarding wizard you can determine which roles have access to your Tripetto plugin. You simply select the desired roles that need to have access and that's it.</p>
<p>By default each role that you select will then get full access to all functions of the Tripetto plugin.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-custom-access.png" width="1200" height="760" alt="Screenshot of the onboarding wizard" loading="lazy" />
  <figcaption>Configure custom role access in the onboarding wizard.</figcaption>
</figure>

<h3 id="custom-role-capabilities">Custom role capabilities</h3>
<p>To go one step further you can also customize the exact capabilities that each role has for Tripetto. In the onboarding wizard you will get the possibility to select the desired capabilities per role that you have selected.
<p>The following capabilities are available for the Tripetto plugin:</p>
<ul>
  <li><code>tripetto_create_forms</code>: Allows users to create new forms;</li>
  <li><code>tripetto_edit_forms</code>: Allows users to edit existing forms;</li>
  <li><code>tripetto_run_forms</code>: Allows users to get the shareable link of a form and run it;</li>
  <li><code>tripetto_delete_forms</code>: Allows users to delete forms (first delete all form results);</li>
  <li><code>tripetto_view_results</code>: Allows users to view the results of forms;</li>
  <li><code>tripetto_export_results</code>: Allows users to export the results to a CSV-file;</li>
  <li><code>tripetto_delete_results</code>: Allows users to delete results.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-custom-capabilities.png" width="1200" height="760" alt="Screenshot of the onboarding wizard" loading="lazy" />
  <figcaption>Configure custom capabilities per role in the onboarding wizard.</figcaption>
</figure>
<h4>Minimal capabilities</h4>
<p>To give a role access to Tripetto, it should at least have one or more of the following capabilities:</p>
<ul>
  <li><code>tripetto_edit_forms</code>;</li>
  <li><code>tripetto_run_forms</code>;</li>
  <li><code>tripetto_view_results</code>.</li>
</ul>

<h3 id="custom-roles">Custom roles</h3>
<p>On top of the six pre-defined roles, you can also add your own custom roles to your WordPress environment. In that case you will have to use a third-party user roles plugin to manage those roles and capabilities (for example <a href="https://wordpress.org/plugins/user-role-editor/" target="_blank" rel="noopener noreferrer">User Role Editor</a>).</p>
<p>In such a plugin you will see the above mentioned capabilities of Tripetto and you can simply activate the desired capabilities per role in your user roles plugin.</p>
