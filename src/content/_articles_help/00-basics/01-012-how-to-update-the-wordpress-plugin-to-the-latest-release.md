---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-update-the-wordpress-plugin-to-the-latest-release/
title: Update the WordPress plugin - Tripetto Help Center
description: Learn how to update the WordPress plugin to the latest release version.
article_title: How to update the WordPress plugin to the latest release
author: mark
time: 1
category_id: wordpress-basics
subcategory: basics_plugin
areas: [wordpress]
---
<p>We regularly update the WordPress plugin with new features, improvements and bugfixes. We advise to always stay up-to-date. Let us show you how to do that.</p>

<h2 id="check">Check available updates</h2>
<p>In case one or more plugin updates are available inside your WP Admin, you'll see a badge in the Plugins section. Navigate to your installed plugins to see which plugins can be updated. Click <code>Plugins</code><i class="fas fa-arrow-right"></i>Click <code>Installed Plugins</code>.</p>
<p>If the Tripetto plugin has an update waiting for you, it will show an update notification in the list of installed plugins. The update will also be mentioned inside your list of installed plugins.</p>
<hr/>

<h2 id="update">Update plugin</h2>
<p>Updating is simple. Just click <code>Update now</code> and the update will be executed right away. A confirmation will be shown after the update is completed.</p>
<h3 id="onboarding">Onboarding wizard</h3>
<p>The first time you start the Tripetto plugin after the update, we will guide you through the onboarding wizard to show our changelog. The onboading wizard also helps you to check your <a href="{{ page.base }}help/articles/how-to-configure-plugin-access-and-capabilities-with-wordpress-user-roles/">plugin access</a> and <a href="{{ page.base }}help/articles/how-to-configure-plugin-settings-for-email-sending-and-spam-protection/">plugin settings</a>.</p>
