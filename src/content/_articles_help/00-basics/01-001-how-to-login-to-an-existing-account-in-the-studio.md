---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-login-to-an-existing-account-in-the-studio/
title: Login to an existing account in the studio - Tripetto Help Center
description: Once you have a studio account, you can login to that at any given time. There are a few different ways to login.
article_title: How to login to an existing account in the studio
article_folder: studio-account-login
author: martijn
time: 3
category_id: studio-basics
subcategory: basics_account
areas: [studio]
---
<p>Once you have a studio account, you can login to that at any given time. There are a few different ways to login.</p>

<h2 id="login" data-anchor="Login to account">Login to your studio account</h2>
<p>To login to your studio account, click the <code><i class="fas fa-user"></i></code> icon at the top right corner of the studio. The Login pane will show up on the right side of the form builder.</p>
<p>In there, enter the email address of your Tripetto studio account. Next, determine how you'd like to login:</p>
<ul>
  <li>Select <code>Login with email</code> to receive an email with a magic link and a temporary password, which you can use to quickly login.</li>
  <li>Select <code>Login with password</code> if you <a href="{{ page.base }}help/articles/how-to-set-a-password-for-your-account-in-the-studio/">set a password</a> for your account and want to login with that password.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/login.png" width="1200" height="760" alt="Screenshot of the Tripetto studio" loading="lazy" />
  <figcaption>Enter your email address.</figcaption>
</figure>
<p>After you selected your preferred login method, click <code>Next</code> and proceed with the selected method.</p>

<h3 id="login-email" data-anchor="With email">Login with email</h3>
<p>If you selected to login via email, now go to your Inbox and open the login email you received. With this email you can now login to your existing account. You can do that in two ways:</p>
<ul>
  <li><strong>Via magic link</strong> - Simply click the magic link in the email and you will be logged in right away.</li>
  <li><strong>Via temporary password</strong> - Copy the temporary password you see in the email and return to the browser where you were logging in to the Tripetto studio. Now paste the temporary password in there and click <code>Login</code>. You will now be logged in right away.</li>
</ul>
<p>Please note that each login email is usable only once with a time limit of 15 minutes.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/login-email.png" width="1150" height="523" alt="Screenshot of the Tripetto login email" loading="lazy" />
  <figcaption>Use the magic link or temporary password from the login email.</figcaption>
</figure>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/login-code.png" width="1200" height="760" alt="Screenshot of the Tripetto studio" loading="lazy" />
  <figcaption>Paste the temporary password from the login email.</figcaption>
</figure>

<h3 id="login-password" data-anchor="With password">Login with password</h3>
<p>If you selected to login via password, you can now simply enter the password that you set for this account and click <code>Login</code>.</p>
<p>In case the account had no password set yet, the login email will still be sent and you can login via that route.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/login-password.png" width="1200" height="760" alt="Screenshot of the studio of Tripetto" loading="lazy" />
  <figcaption>Enter your password to login.</figcaption>
</figure>
<hr/>

<h2 id="troubleshooting">Troubleshooting</h2>
<p>Having trouble with logging in? Let's do some troubleshooting.</p>

<h3 id="not-receiving">Not receiving email</h3>
<p>If you're using email login, it can happen you don't see the email message coming into your email inbox. Please have a look if these actions help:</p>
<ul>
  <li>Check your spam folder. Maybe our email accidently got marked as spam.</li>
  <li>Whitelist our email address <code>no-reply@tripetto.com</code>. Whitelisting this address could prevent our mails getting rejected. It depends on your email provider/software how to achieve whitelisting. After you've whitelisted us, please try again to login in the studio.</li>
  <li>Still not receiving the email? <a href="{{ page.base }}support/">Drop us a line</a> and let us know the email address of your account.</li>
</ul>

<h3 id="not-working">Not able to login with email</h3>
<p>If you received the email, but the login is not working, a few things can be happening:</p>
<ul>
  <li>If the magic link is not working correctly, use the temporary password to login to your account instead.</li>
  <li>Magic link and temporary password both not working? Possibly this request is already used (each request can be used once) or the time limit expired (each request is valid for 15 minutes). Please try again to login in the studio.</li>
  <li>Still not able to login to your account? <a href="{{ page.base }}support/">Drop us a line</a> and let us know the email address of your account.</li>
</ul>

<h3 id="no-password">Not able to login with password</h3>
<p>If your password doesn't work, please try the following:</p>
<ul>
  <li>If you receive the email while you tried to login with a password, there apparently is no password set for your account yet. Use the email login instead and <a href="{{ page.base }}help/articles/how-to-set-a-password-for-your-account-in-the-studio/">set a password</a> when you are logged in.</li>
  <li>Your password isn't correct? Please note that passwords are case sensitive. If the password doesn't work, please start the login process from step 1 again and use the email login instead. After login you can <a href="{{ page.base }}help/articles/how-to-set-a-password-for-your-account-in-the-studio/">set a new password</a>.</li>
  <li>Still not able to login to your account? <a href="{{ page.base }}support/">Drop us a line</a> and let us know the email address of your account.</li>
</ul>
