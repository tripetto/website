---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-configure-plugin-settings-for-email-sending-and-spam-protection/
title: Configure plugin settings (email and spam) - Tripetto Help Center
description: Learn how to use general plugin settings for email sending and the level of spam protection.
article_title: How to configure plugin settings for email sending and spam protection
article_folder: wordpress-settings
author: mark
time: 3
category_id: wordpress-basics
subcategory: basics_settings
areas: [wordpress]
---
<p>Tripetto provides some general settings in WordPress to let your forms behave exactly like you want them to. This article describes how to access those settings and what you can do with it.</p>

<h2 id="about">About plugin settings</h2>
<p>In WordPress you can configure the following general settings for your Tripetto plugin:</p>
<ul>
  <li><strong>Email settings</strong> - Configure the settings to use when Tripetto sends an email message.</li>
  <li><strong>Spam protection</strong> - Configure the level of Tripetto's built-in <a href="{{ page.base }}help/articles/how-tripetto-prevents-spam-entries-from-your-forms/">spam protection system</a> that's used in your Tripetto forms.</li>
</ul>
<hr/>

<h2 id="settings">Plugin settings</h2>
<p>You can configure those plugin settings yourself. Your settings will apply to all forms in your plugin.</p>

<h3 id="access">Access to plugin settings</h3>
<p>Access to the plugin settings is handled by the overall WordPress capability <code>manage_options</code>. By default only users with <a href="https://wordpress.org/support/article/roles-and-capabilities/#manage_options" target="_blank" rel="noopener noreferrer">Super Admin or Administrator role</a> have that capability enabled.</p>

<h3 id="open">Open plugin settings</h3>
<p>Users with an appropriate role can open the plugin settings in two ways:</p>
<ul>
  <li><strong>In the Tripetto plugin</strong> - When you activate the Tripetto plugin for the first time or just installed an update to it, we will give you the option to follow an onboarding wizard. Part of that onboarding wizard is configuring the plugin settings (if you have the right WordPress capabilities to do so).<br/>This onboarding wizard is also available from the Tripetto dashboard to configure your settings at a later moment.</li>
  <li><strong>In your WordPress settings</strong> - In your WP Admin menu navigate to <code>Settings</code><i class="fas fa-arrow-right"></i><code>Tripetto</code>.</li>
</ul>
<p>In this article we will use the Tripetto onboarding wizard to configure the settings.</p>

<h3 id="email-settings">Email settings</h3>
<p>The email settings are used when Tripetto sends an email message. That can be via a <a href="{{ page.base }}help/articles/how-to-use-the-send-email-block/">send email block</a> in your form or via an <a href="{{ page.base }}help/articles/how-to-automate-email-notifications-for-each-new-result/">email notification</a> for a new response.</p>

<h4>Sender</h4>
<p>For these email messages you can configure the desired sender name and sender email address. The following options are available:</p>
<ul>
  <li><code>WordPress default sender</code> - Use the default sender that's configured in your mail server;</li>
  <li><code>WordPress general settings</code> - Use your site's title and administration email address from the general settings;</li>
  <li><code>Custom sender</code> - Set a custom sender name and sender email address.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-email.png" width="1200" height="760" alt="Screenshot of the onboarding wizard" loading="lazy" />
  <figcaption>Configure the email settings in the onboarding wizard.</figcaption>
</figure>
<blockquote>
  <h4>🧯 Troubleshooting: Email notifications not sent or marked as spam</h4>
  <p>It's important to use a correct email address that is able to send emails from your own mail server. In most cases it's best to use an email address from your own domain name.</p>
  <p>If you're still having troubles with sending emails, please have a look at this article:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/troubleshooting-email-notifications-from-wordpress-not-sent-or-marked-as-spam/">Email notifications from WordPress not sent or marked as spam</a></li>
  </ul>
</blockquote>

<h3 id="spam-protection-settings">Spam protection settings</h3>
<p>Tripetto has a built-in spam protection mechanism that takes care of preventing abuse of your forms, without having to bother your respondents. More information:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-tripetto-prevents-spam-entries-from-your-forms/">How Tripetto prevents spam entries from your forms</a></li>
</ul>
<p>In some cases you may want to adjust the level of spam protection that your form operates with, for example if you expect many responses from the same IP address.</p>

<h4>Spam protection mode</h4>
<p>For such cases you can configure the desired spam protection mode. We strongly advise to only change the mode of operation if you have a good reason for it. You can choose between:</p>
<ul>
  <li><code>Maximal</code> - Maximum protection against spambots, but form submission might be a bit slower in certain conditions;</li>
  <li><code>Normal</code> - The default protection mode provides good protection against spambots while maintaining optimal performance;</li>
  <li><code>Minimal</code> - IP filtering will be disabled (use this mode if you expect lots of submission from the same IP address);</li>
  <li><code>Off</code> - Disables the spam protection (not recommended).</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-spam.png" width="1200" height="760" alt="Screenshot of the onboarding wizard" loading="lazy" />
  <figcaption>Configure the spam protection mode in the onboarding wizard.</figcaption>
</figure>

<h4>Allow list</h4>
<p>You can enter a list of IP addresses (separated with a comma) that are always allowed to bypass the spam protection. Both IPv4 and IPv6 addresses are allowed.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/03-allowlist.png" width="1200" height="760" alt="Screenshot of the onboarding wizard" loading="lazy" />
  <figcaption>Enter the allowlist of IP addresses in the onboarding wizard.</figcaption>
</figure>
