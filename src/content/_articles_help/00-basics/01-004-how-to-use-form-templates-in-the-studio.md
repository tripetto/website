---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-form-templates-in-the-studio/
title: Use form templates in the studio - Tripetto Help Center
description: In the Tripetto studio you can use form templates to share a copy of your form stucture with other users, or to duplicate a form in your own studio account.
article_title: How to use form templates in the studio
article_folder: studio-templates
author: martin
time: 2
category_id: studio-basics
subcategory: basics_forms
areas: [studio]
---
<p>In the Tripetto studio you can use form templates to share a copy of your form stucture with other users, or to duplicate a form in your own studio account.</p>

<h2 id="when-to-use">When to use</h2>
<p>Form templates in the studio can be used to share a copy of your form stucture with other users, or to duplicate a form. You can for example use it to share a form structure with someone else to have a look at your form design. Or you can use it for yourself to create a duplicate/backup of a form.</p>
<blockquote>
  <h4>📣 Info</h4>
  <p>A form template is just an exact copy of a source form at that moment. There's no connection between the source form and the new form you saved in your own account using the template link.</p>
</blockquote>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>The form template has two parts:</p>
<ul>
  <li>Sharing the template link from an existing form;</li>
  <li>Using the template link to store a copy.</li>
</ul>

<h3 id="share">Share a template</h3>
<p>To share a form structure from tripetto.app with someone else, click the <code><i class="fas fa-bars"></i></code> icon on the top left<i class="fas fa-arrow-right"></i>Click <code>Share as template</code>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-menu.png" width="1200" height="760" alt="Screenshot of template link in Tripetto" loading="lazy" />
  <figcaption>Start sharing a template.</figcaption>
</figure>
<p>A new pane will show up on the right side of the form builder. Over there you'll find the URL to share this form as a template. You can send this template link to the desired recipient(s), for example via email or Slack.</p>

<h3 id="use">Use a template</h3>
<p>When you received a template link, just click it and the studio will open the form structure of the source form right away. You can save this template as a form in your own account by clicking <code>Use this template</code> in the top menu bar. The form will then be saved to your account in your default workspace/collection.</p>
<blockquote>
  <h4>📣 Info</h4>
  <p>Remember: this just creates a copy of the form at that moment. There's no connection between the source form and the new form.</p>
</blockquote>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-use.png" width="1200" height="760" alt="Screenshot of template usage in Tripetto" loading="lazy" />
  <figcaption>Start using a template.</figcaption>
</figure>

<h2 id="duplicate">Duplicate a form</h2>
<p>If you want to duplicate or make a copy of your own form, you can also use the template link for that. Just open the template link of your original form yourself and click <code>Use this template</code>. Then the template will be saved into your own account as a new form.</p>
<blockquote>
  <h4>💡 Tip: Use form names</h4>
  <p>Before you save the template, give your form a name, so you can recognize the new form in your account.</p>
</blockquote>
