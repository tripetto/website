---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-use-the-tripetto-form-builder-in-the-wordpress-gutenberg-editor/
title: Tripetto in WordPress Gutenberg editor - Tripetto Help Center
description: Tripetto's fullblown form builder is also available right inside the WordPress Gutenberg editor.
article_title: How to use the Tripetto form builder in the WordPress Gutenberg editor
article_video: sharing-wordpress-gutenberg
article_folder: wordpress-gutenberg
author: jurgen
time: 4
time_video: 2
category_id: wordpress-basics
subcategory: basics_forms
areas: [wordpress]
---
<p>Tripetto's fullblown form builder is also available right inside the WordPress Gutenberg editor. The Tripetto Gutenberg block makes it easy to build, customize and automate your forms without leaving the Gutenberg editor.</p>

<h2 id="gutenberg-editor">About Gutenberg editor</h2>
<p>Gutenberg is the modern content editor in WordPress. It makes building those amazing pages and posts in your WordPress site even easier, more flexible and more powerful. <a href="https://wordpress.org/support/article/wordpress-editor/" target="_blank" rel="noopener noreferrer">You can read all about it at wordpress.org.</a></p>
<div>
  <a href="https://wordpress.org/support/article/wordpress-editor/" target="_blank" class="blocklink" rel="noopener noreferrer">
    <div>
      <span class="title">WordPress Gutenberg Editor<i class="fas fa-external-link-alt"></i></span>
      <span class="description">The WordPress Editor is a new publishing experience. You can use it to create media-rich pages and posts and to control their layout with ease.</span>
      <span class="url">wordpress.org</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-wordpress.svg" width="160" height="160" alt="WordPress logo" loading="lazy" />
    </div>
  </a>
</div>

<h3 id="gutenberg-blocks">Gutenberg blocks</h3>
<p>In the Gutenberg editor you create your content in <strong>blocks</strong>. Every block is an element on its own in your page/post and you can fully customize it.</p>
<p>But the power of blocks goes even further, because it gives plugins (like Tripetto) the possibility to integrate with the Gutenberg editor. And that's exactly what we did! Let's see how to use that.</p>
<hr />

<h2 id="tripetto-gutenberg-block">Tripetto's Gutenberg block</h2>
<p>Tripetto's Gutenberg block isn't just a simple block to insert a form, but it brings the full featured form builder into your Gutenberg editor. You get to see a live preview of your form right inside your page/post, so you can instantly see how your form blends with your content.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/gutenberg-editor.png" width="1200" height="760" alt="Screenshot of Tripetto in the Gutenberg editor" loading="lazy" />
  <figcaption>Add Tripetto to the Gutenberg editor.</figcaption>
</figure>
<p>And from there on, without ever leaving the Gutenberg editor, you can do everything that Tripetto offers:</p>
<ul>
  <li><a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/">Build and edit the structure/content of your form</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/">Select the desired form face (autoscroll/chat/classic form face)</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">Style and customize your form</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-edit-or-translate-all-text-labels-in-your-forms/">Translate and edit form labels</a>;</li>
  <li><a href="{{ page.base }}wordpress/help/automating-things/#notifications">Configure notifications to email and Slack</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/">Configure connections to 1.000+ services with webhooks</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-automate-form-activity-tracking/">Configure form tracking</a>;</li>
  <li><a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-gutenberg-block/">Configure all options and settings</a>.</li>
</ul>
<p>All without leaving the Gutenberg editor!</p>
<hr/>

<h2 id="add-block">Add Tripetto Gutenberg block</h2>
<p>In your Gutenberg editor you start with adding a new block. From the overview of available blocks select <code>Tripetto Form</code>. You can also use the search bar to quickly find the Tripetto block.</p>
<p>This will add a new Tripetto block to your Gutenberg editor. Now you can select what you want to insert in that position:</p>
<ul>
  <li><code>Select an existing form</code>;</li>
  <li><code>Build a new form</code>.</li>
</ul>

<h3 id="insert">Insert an existing form</h3>
<p>Inserting an existing form is easy. Add a new <code>Tripetto Form</code> block to your page/post. Then simply select the form from the dropdown list. After you have selected the desired form, it will be added to your block and you will see a live preview of the form right away. That's it!</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/insert-existing.gif" width="1200" height="760" alt="Screen recording of the Tripetto Gutenberg block" loading="lazy" />
  <figcaption>Insert an existing form in the Tripetto Gutenberg block.</figcaption>
</figure>

<h3 id="build">Build a new form</h3>
<p>Tripetto's Gutenberg block even enables you to build your forms right inside the Gutenberg editor. Add a new <code>Tripetto Form</code> block to your page/post. Then simply click <code>Build new form</code>. The new form will be generated and a live preview is shown.</p>
<p>Since the form is empty, the preview will show a message about that. To start building your form, click the <code><i class="fas fa-pen"></i> Edit</code> button in the Gutenberg block. You can now open the form builder in a fullscreen view (giving you maximum space to build your forms with logic), or as a side-by-side overlay. And you can start building your form.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/build-new.gif" width="1200" height="760" alt="Screen recording of the Tripetto Gutenberg block" loading="lazy" />
  <figcaption>Add and build a new form in the Tripetto Gutenberg block.</figcaption>
</figure>
<hr/>

<h2 id="edit">Build, customize and automate</h2>
<p>The cool thing about Tripetto's Gutenberg block is you can do everything from inside the Gutenberg editor. On the right side of the editor you will see all actions you can do to build, customize and automate your form just like you need it. All Tripetto features are simply there! Even the fullblown form builder is available without leaving the Gutenberg editor. You can open the form builder in a fullscreen view, or as a side-by-side overlay.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/edit.gif" width="1200" height="760" alt="Screen recording of the Tripetto Gutenberg block" loading="lazy" />
  <figcaption>Build, customize and automate your form in the Tripetto Gutenberg block.</figcaption>
</figure>
<p>The Gutenberg block also includes all options/settings you need to embed the form the right way. All of this makes Tripetto's Gutenberg block a very powerful block that makes it even easier to use Tripetto in your WordPress site. For more information about these options, please have a look at this article:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-gutenberg-block/">How to embed your form in your WordPress site using the Gutenberg block</a></li>
</ul>
