---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-add-and-manage-forms-in-the-studio/
title: Add and manage forms in the studio - Tripetto Help Center
description: You can create an unlimited amount of forms and surveys in the studio at tripetto.app. Learn how to organize all your projects using studio workspaces.
article_title: How to add and manage forms in the studio
article_folder: studio-manage-forms
author: martijn
time: 2
category_id: studio-basics
subcategory: basics_forms
areas: [studio]
---
<p>You can create an unlimited amount of forms and surveys in the studio at tripetto.app. Learn how to organize all your projects using studio workspaces.</p>

<h2 id="organization">Organizing tiles</h2>
<p>In the studio at tripetto.app you don't manage your forms in a simple list. We created a tile system that gives you more control over the organization of your forms. There are multiple levels available to arrange your forms.</p>
<ul>
  <li>
    <h3 id="workspaces">Workspaces</h3>
    <p>Workspaces can be seen as pages inside your structure. You can use them to bundle collections and forms that belong together. You can create unlimited amount of workspaces. Your account always has at least one workspace.</p>
  </li>
  <li>
    <h3 id="collections">Collections</h3>
    <p>Collections can be used to categorize forms and to bundle corresponding forms. The system is so flexible you also can create workspaces inside collections for more levels, depending on the structure you want.</p>
  </li>
  <li>
    <h3 id="forms">Forms</h3>
    <p>Inside a collection you can create an unlimited amount of forms. These are of course your actual forms that you can open in the form builder.</p>
    <p>To keep track of your forms, it's recommended to give each form a good name, so you can find it back later on.</p>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/03-new-form.png" width="1200" height="760" alt="Screenshot of forms in Tripetto" loading="lazy" />
  <figcaption>Example of a workspace with collections and forms.</figcaption>
</figure>
<hr />

<h2 id="arranging">Arranging tiles</h2>
<p>You can simply change the order of your forms/collections/workspaces by dragging and dropping the tiles. You can even drag and drop tiles between collections.</p>
<p>Click and hold a specific tile (for example a form tile) for a brief moment, so it sticks to your cursor. Then you can drag it to the right position by holding your mouse click. And drop it at the right position by releasing your mouse click.</p>
<hr />

<h2 id="example">Example: survey agency</h2>
<p>Let's suppose we're a survey agency that creates surveys for multiple clients. We're totally free on how to organize this, but a possible structure could be the following:</p>
<p>We can create a workspace for each client. In that way all forms of each client are separated.</p>
<p>In each workspace (the client) we can create collections to organize the forms of that client. Let's say we want to organize all forms per year. So we can create a collection for each year.</p>
<p>Inside each collection (the year) we create the forms of that particular year.</p>
<p>So we end up with something like this:</p>
<p><b>Workspace<i class="fas fa-arrow-right"></i>Collection<i class="fas fa-arrow-right"></i>Form</b></p>
<p>Client A<i class="fas fa-arrow-right"></i>2019<i class="fas fa-arrow-right"></i>Form Z</p>
<p>Client A<i class="fas fa-arrow-right"></i>2020<i class="fas fa-arrow-right"></i>Form Y</p>
<p>Client A<i class="fas fa-arrow-right"></i>2020<i class="fas fa-arrow-right"></i>Form X</p>
<p>Client B<i class="fas fa-arrow-right"></i>2020<i class="fas fa-arrow-right"></i>Form W</p>
<p>Client C<i class="fas fa-arrow-right"></i>2018<i class="fas fa-arrow-right"></i>Form V</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/04-example.png" width="1200" height="760" alt="Screenshot of workspaces, collections and forms in Tripetto" loading="lazy" />
  <figcaption>The workspace of Client A with two collections and three forms.</figcaption>
</figure>
