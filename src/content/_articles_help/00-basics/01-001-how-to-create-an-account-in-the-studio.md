---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-create-a-new-account-in-the-studio/
title: Create a new account in the studio - Tripetto Help Center
description: Your forms and responses are stored in your Tripetto studio account. Let's have a look how to create such a studio account.
article_title: How to create a new account in the studio
article_folder: studio-account-create
author: martijn
time: 3
category_id: studio-basics
subcategory: basics_account
areas: [studio]
---
<p>Your forms and responses are stored in your Tripetto studio account. Let's have a look how to create such a studio account.</p>

<h2 id="account">About studio account</h2>
<p>When you are new to the Tripetto studio, you can start building a form without logging in. That way you can get a quick grasp of what Tripetto can do for you and how your form will look like. Once you're ready to store your form and/or you want to start sharing your form, you can create a studio account. From then on all your forms and submissions will be stored inside that account.</p>
<hr/>

<h2 id="create" data-anchor="Create account">Create your studio account</h2>
<p>Creating a new studio account is very simple. On the top right corner of the studio click the <code><i class="fas fa-user"></i></code> icon. The Login pane will show up on the right side of the form builder.</p>

<h3 id="create-step-1">Step 1 - Enter email address</h3>
<p>In there, enter the email address that you'd like to use and click <code>Next</code>. Since you create a new account, you can ignore the login options below. The Tripetto studio will recognize you are a new user and will continue with the registration process. This means you will receive an email on the email address that you entered.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/create-account.png" width="1200" height="760" alt="Screenshot of the studio of Tripetto" loading="lazy" />
  <figcaption>Enter your email address.</figcaption>
</figure>

<h3 id="create-step-2">Step 2 - Verify and login</h3>
<p>Now go to your Inbox and open the verification email you received. Via this email you can now verify your email address and login directly. You can do that in two ways:</p>
<ul>
  <li><strong>Via magic link</strong> - Simply click the magic link in the email and you will be logged in right away. Please make sure this magic link is opened in the same browser as you were working in, so the form that you were working on can get stored to your newly created account.</li>
  <li><strong>Via temporary password</strong> - Copy the temporary password you see in the email and return to the browser where you were working in the Tripetto studio. Now paste the temporary password in there and click <code>Login</code>. You will now be logged in and the form that you were working on will be stored in your newly created account.</li>
</ul>
<p>Please note that each verification email is usable only once with a time limit of 15 minutes.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/create-account-email.png" width="1150" height="523" alt="Screenshot of the Tripetto verification email" loading="lazy" />
  <figcaption>Use the magic link or temporary password from the verification email.</figcaption>
</figure>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/create-account-code.png" width="1200" height="760" alt="Screenshot of the Tripetto studio" loading="lazy" />
  <figcaption>Paste the temporary password from the verification email.</figcaption>
</figure>
<p><strong>And that's it!</strong> Your account is created and you're logged in right away, including the form that you were working on. You will remain logged in for one month after your last activity in your account. If you logged out, you can <a href="{{ page.base }}help/articles/how-to-login-to-an-existing-account-in-the-studio/">login again</a> at any time.</p>
<hr/>

<h2 id="troubleshooting">Troubleshooting</h2>
<p>Having trouble with creating your account? Let's do some troubleshooting.</p>

<h3 id="not-receiving">Not receiving email</h3>
<p>It can happen you don't see the email message coming into your email inbox. Please have a look if these actions help:</p>
<ul>
  <li>Check your spam folder. Maybe our email accidently got marked as spam.</li>
  <li>Whitelist our email address <code>no-reply@tripetto.com</code>. Whitelisting this address could prevent our mails getting rejected. It depends on your email provider/software how to achieve whitelisting. After you've whitelisted us, please try again to create your account in the studio.</li>
  <li>Still not receiving the email? <a href="{{ page.base }}support/">Drop us a line</a> and let us know the email address that you're creating an account for.</li>
</ul>

<h3 id="not-verifying">Not able to verify</h3>
<p>If you received the email, but the verification is not working, a few things can be happening:</p>
<ul>
  <li>If the magic link is not working correctly, use the temporary password to create your account instead.</li>
  <li>Magic link and temporary password both not working? Possibly this request is already used (each request can be used once) or the time limit expired (each request is valid for 15 minutes). Please try again to create your account in the studio.</li>
  <li>Still not able to create the account? <a href="{{ page.base }}support/">Drop us a line</a> and let us know the email address that you're creating an account for.</li>
</ul>
