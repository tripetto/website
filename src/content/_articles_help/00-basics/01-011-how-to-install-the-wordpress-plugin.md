---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-install-the-wordpress-plugin/
title: Install the WordPress plugin - Tripetto Help Center
description: Learn how to get the WordPress plugin running inside your WP Admin.
article_title: How to install the WordPress plugin
article_folder: wordpress-install
author: mark
time: 3
category_id: wordpress-basics
subcategory: basics_plugin
areas: [wordpress]
---
<p>The Tripetto WordPress plugin runs entirely inside your own WP Admin. Installing the plugin follows the standard workflow for plugin installation. Not familiar with this process? We're happy to help you.</p>

<h2 id="signin">Sign in to WP Admin</h2>
<p>The Tripetto WordPress plugin runs entirely inside your own WP Admin. To begin, login to the WP Admin of the site where you want to install Tripetto. Make sure you login with a WP Admin account that is able to manage the site's plugins.</p>

<h2 id="installation">Install plugin</h2>
<p>Once signed in to your WP Admin, we offer three ways to get the plugin running inside your WP Admin:</p>
<ul>
  <li>Free plugin: install from inside your WP Admin;</li>
  <li>Free plugin: install from the WordPress.org plugin repository;</li>
  <li>Pro plugin: install from a Tripetto Pro license purchase.</li>
</ul>

<h3 id="wp_admin" data-anchor="Free plugin from WP Admin">Install Free plugin from inside your WP Admin</h3>
<p>The free version of the Tripetto WordPress plugin can be installed directly from inside your WP Admin.</p>
<p>To do so, open your WP Admin and navigate to <code>Plugins</code><i class="fas fa-arrow-right"></i>Click <code>Add New</code><i class="fas fa-arrow-right"></i>Search by keyword <code>Tripetto</code><i class="fas fa-arrow-right"></i>Click <code>Install Now</code> within the search result<i class="fas fa-arrow-right"></i>Click <code>Activate</code>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-plugins.png" width="1200" height="760" alt="Screenshot of WordPress plugins" loading="lazy" />
  <figcaption>Install free plugin from inside your WP Admin.</figcaption>
</figure>

<h3 id="wp_repository" data-anchor="Free plugin from WP repository">Install Free plugin from the WordPress.org plugin repository</h3>
<p>It's also possible to download the free plugin via the WordPress.org plugin repository.</p>
<p>To do so, <a href="{{ site.url_wordpress_plugin }}" target="_blank" rel="noopener noreferrer">download the free Tripetto plugin from WordPress.org</a>. You'll receive a .zip file, which you'll need in the next steps. You don't have to unpack the file. Now open your WP Admin and navigate to <code>Plugins</code><i class="fas fa-arrow-right"></i>Click <code>Add New</code><i class="fas fa-arrow-right"></i>Click <code>Upload Plugin</code><i class="fas fa-arrow-right"></i>Select your downloaded file<i class="fas fa-arrow-right"></i>Click <code>Install Now</code><i class="fas fa-arrow-right"></i>Click <code>Activate</code>.</p>
<div>
  <a href="{{ site.url_wordpress_plugin }}" target="_blank" class="blocklink" rel="noopener noreferrer">
    <div>
      <span class="title">Tripetto - WordPress.org plugin repository<i class="fas fa-external-link-alt"></i></span>
      <span class="description">A full WordPress form plugin for smart forms and surveys. Use Tripetto to do it all just a bit better, without a single line of code. All inside your own WP Admin. No third-party account needed, not even a Tripetto account. GDPR proof.</span>
      <span class="url">wordpress.org</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-wordpress.svg" width="160" height="160" alt="WordPress logo" loading="lazy" />
    </div>
  </a>
</div>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-upload.png" width="1200" height="760" alt="Screenshot of WordPress plugin upload" loading="lazy" />
  <figcaption>Upload plugin from the WordPress.org plugin repository.</figcaption>
</figure>

<h3 id="pro" data-anchor="Pro plugin from purchase">Install Pro plugin from a purchase</h3>
<p>The Pro version of the Tripetto plugin is a totally separate plugin from the free plugin. If you purchased a <a href="{{ page.base }}wordpress/pricing/">Tripetto Pro license</a>, you will receive an email with everything you need to proceed, namely:</p>
<ul>
  <li>A .zip file with the Pro version of the plugin. Save this file to your computer. You don't have to unpack the .zip file;</li>
  <li>Your personal license key.</li>
</ul>
<p>To install the Pro version, open your WP Admin and navigate to <code>Plugins</code><i class="fas fa-arrow-right"></i>Click <code>Add New</code><i class="fas fa-arrow-right"></i>Click <code>Upload Plugin</code><i class="fas fa-arrow-right"></i>Select your downloaded file<i class="fas fa-arrow-right"></i>Click <code>Install Now</code><i class="fas fa-arrow-right"></i>Click <code>Activate</code><i class="fas fa-arrow-right"></i>Enter your personal license key.</p>
<blockquote>
  <h4>💡 What happens with your data?</h4>
  <p>If you already had Tripetto installed, for example the free version, that plugin will be deactivated automatically during the upgrade process. Any existing forms and entries will be availabe in the Pro version right away, so you won't lose any data.</p>
</blockquote>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-upload-pro.png" width="1200" height="760" alt="Screenshot of WordPress plugin upload" loading="lazy" />
  <figcaption>Upload Pro version of the plugin.</figcaption>
</figure>
<hr/>

<h2 id="start">Start using plugin</h2>
<p>After activation, the Tripetto plugin is shown in the WP Admin menu on the left. Navigate to the <code>Tripetto</code> section in this menu.</p>

<h3 id="onboarding">Onboarding wizard</h3>
<p>The first time you start the Tripetto plugin, we will guide you through the onboarding wizard to give you a kickstart with Tripetto. The onboading wizard also helps you to configure your <a href="{{ page.base }}help/articles/how-to-configure-plugin-access-and-capabilities-with-wordpress-user-roles/">plugin access</a> and <a href="{{ page.base }}help/articles/how-to-configure-plugin-settings-for-email-sending-and-spam-protection/">plugin settings</a>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/03-onboarding.png" width="1200" height="760" alt="Screenshot of the onboarding wizard in the WordPress plugin" loading="lazy" />
  <figcaption>Follow the onboarding wizard to configure plugin access and plugin settings.</figcaption>
</figure>

<h3 id="dashboard">Dashboard</h3>
<p>After the onboarding wizard, you continue to the Tripetto dashboard. The dashboard gives shortcuts to go to your forms, but also helps you with direct access to the help center and video tutorials.</p>
<p>Click the <code>Build Your First Form</code> button to create a new form. Read <a href="{{ page.base }}help/articles/how-to-add-and-manage-forms-in-the-wordpress-plugin/">this article</a> for more information about managing your forms in the plugin.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/04-dashboard.png" width="1200" height="760" alt="Screenshot of the dashboard in the WordPress plugin" loading="lazy" />
  <figcaption>Open the dashboard to start your Tripetto experience.</figcaption>
</figure>
