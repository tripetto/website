---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-add-and-manage-forms-in-the-wordpress-plugin/
title: Add and manage forms in the WordPress plugin - Tripetto Help Center
description: Learn how to add and manage forms in the WordPress plugin using the forms list.
article_title: How to add and manage forms in the WordPress plugin
article_folder: wordpress-manage-forms
author: jurgen
time: 3
category_id: wordpress-basics
subcategory: basics_forms
areas: [wordpress]
---
<p>With Tripetto you can create an unlimited amount of forms, but how do you keep these forms organized? Let's find out.</p>

<h2 id="create">Build new form</h2>
<p>After you <a href="{{ page.base }}help/articles/how-to-install-the-wordpress-plugin/">installed the WordPress plugin</a>, you can start a new form in a few ways:</p>
<ul>
  <li>Open the Tripetto plugin menu from the WP Admin menu<i class="fas fa-arrow-right"></i>Click <code>Build Form</code>;</li>
  <li>Open the Tripetto Dashboard inside the plugin<i class="fas fa-arrow-right"></i>Click <code>Build Form</code>;</li>
  <li>Open the list of your Tripetto forms inside the plugin<i class="fas fa-arrow-right"></i>Click <code>Build Form</code>;</li>
</ul>
<p>Next, you can determine how you want to start your new form:</p>
<ul>
  <li><code>Start from scratch</code> - Choose your desired <a href="{{ page.base }}form-layouts/">form face</a> and start building right away on an empty storyboard. You can always <a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/">switch form faces</a> instantly while working on your form.</li>
  <li><code>Build from a template</code> - Choose any of the offered templates to build your form even faster. To get a good vision on each template, you can test the fully working form before you use it as a template for your new form. Templates include a basic form structure and styling. Templates are fully editable and customizable of course.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/build-new-form.png" width="1775" height="1114" alt="Screenshot of the 'Build New form' screen in the Tripetto WordPress plugin" loading="lazy" />
  <figcaption>Start from scratch, or build from a template in the WordPress plugin.</figcaption>
</figure>
<h3 id="limits">No limitations</h3>
<p>Tripetto has absolutely no limitations when it comes to the amount of forms, the size of those forms, amount of results, etc.</p>
<hr/>

<h2 id="manage" data-anchor="Manage forms">Manage your forms</h2>
<p>To manage your forms in the plugin, go to <code>All Forms</code>. This shows the list of all Tripetto forms in your WP Admin, including the amount of entries and the shortcode for each form.</p>

<h3 id="actions">Form actions</h3>
<p>By moving your cursor over each row in the list, you can execute the following actions.</p>
<p>Underneath the name of each form in the list:</p>
<ul>
  <li><code>Edit</code> - Edit the form in the form builder;</li>
  <li><code>Run</code> - Open the form with the <a href="{{ page.base }}help/articles/how-to-share-a-link-to-your-form-in-your-wordpress-site/">shareable link</a>;</li>
  <li><code>Results</code> - Open the list of entries to see and <a href="{{ page.base }}help/articles/how-to-get-your-results-from-the-wordpress-plugin/">download the results</a>;</li>
  <li><code>Duplicate</code> - Create an exact duplicate of the form structure in one click;</li>
  <li><code>Delete</code> - Delete the form, after confirmation. To prevent data loss, you can only delete forms that have no entries left in it. So please first <a href="{{ page.base }}help/articles/how-to-get-your-results-from-the-wordpress-plugin/">delete all the results</a> of the form, before deleting the form itself.</li>
</ul>
<p>Underneath the shortcode of each form in the list:</p>
<ul>
  <li><code>Copy to clipboard</code> - Copy the full shortcode to your clipboard, so you can use it anywhere in your WP site;</li>
  <li><code>Customize</code> - Open the <a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-shortcode/">shortcode editor</a> to modify the parameters of your shortcode.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/forms.png" width="1109" height="502" alt="Screenshot of the list of forms in the Tripetto WordPress plugin" loading="lazy" />
  <figcaption>Your list of forms in the WordPress plugin.</figcaption>
</figure>

<h3 id="form-name">Form names</h3>
<p>To keep track of your forms, it's recommended to give each form a good name, so you can find it back later on. To set the name of the form use the following options:</p>
<ul>
  <li>Open the form in the form builder<i class="fas fa-arrow-right"></i>Click the name of the form in the top left corner (<code>Unnamed form</code> by default)<i class="fas fa-arrow-right"></i>Name your form;</li>
  <li>Open the form in the form builder<i class="fas fa-arrow-right"></i>Click <code>Customize</code><i class="fas fa-arrow-right"></i><code>Properties</code><i class="fas fa-arrow-right"></i>Name your form.</li>
</ul>
<hr/>

<h2 id="gutenberg">Gutenberg block</h2>
<p>Tripetto's fullblown form builder is also available right inside the <strong><a href="https://wordpress.org/support/article/wordpress-editor/" target="_blank" rel="noopener noreferrer">WordPress Gutenberg editor</a></strong>. The Tripetto Gutenberg block makes it possible to build, customize and automate your forms without leaving the Gutenberg editor. And that makes it even easier to create your forms right at the place where you are going to use them: your WordPress website!</p>
<p>Tripetto's Gutenberg block isn't just a simple block to insert a form, but it brings the full featured form builder right into your Gutenberg editor. To see all its capabilities, please have a look at this article:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-tripetto-form-builder-in-the-wordpress-gutenberg-editor/">How to use the Tripetto form builder in the WordPress Gutenberg editor</a></li>
</ul>
