---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-track-form-activity-with-custom-tracking-code/
title: Track form activity with custom tracking code - Tripetto Help Center
description: Tripetto offers direct tracking with Google Analytics, Google Tag Manager and Facebook Pixel, but you can also use your own custom tracking code.
article_title: How to track form activity with custom tracking code
article_id: tracking
article_folder: automate-tracking-custom
author: mark
time: 5
category_id: automations
subcategory: automations_tracking_tools
areas: [studio, wordpress]
scripts: [prism]
stylesheets: [prism-dark]
---
<p>Tripetto offers direct tracking with <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-google-analytics/">Google Analytics</a>, <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-google-tag-manager/">Google Tag Manager</a> and <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-facebook-pixel/">Facebook Pixel</a>, but you can also use your own custom tracking code to track form activity with other tracking services.</p>

<h2 id="when-to-use">When to use</h2>
<p>By tracking the activity of respondents in your forms, you can get insight information of their behaviors. This helps you to analyze form starts, form completions, interactions and drop-offs.</p>
<p>There are lots of services that can help you to analyze your activity data. Tripetto offers direct integrations with <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-google-analytics/">Google Analytics</a>, <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-google-tag-manager/">Google Tag Manager</a> and <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-facebook-pixel/">Facebook Pixel</a>, but if you want to connect with other tracking services you can use the custom tracking code option. To do so, you need to have JavaScript programming skills.</p>

<blockquote class="help_article_studio_only">
  <h4>🚧 Warning: Only in embedded forms (studio only)</h4>
  <p>You can use the custom tracking code in the Tripetto studio at tripetto.app, but this will only work for your embedded forms. This will not work if you use the shareable link of your form at the tripetto.app domain.</p>
  <p>Please also make sure you always update your embed code in your website/app.</p>
</blockquote>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>In this article we will show the steps you have to take to send Tripetto activity data to a tracking service of your choosing. You can also have a look at our <a href="{{ page.base }}help/articles/how-to-automate-form-activity-tracking/">global article about how form activity tracking works</a>.</p>
<blockquote>
  <h4>🚧 Warning: JavaScript programming skills needed</h4>
  <p>The custom tracking code works with JavaScript. We assume you have the needed JavaScript programming skills to setup your own custom tracking code. Tripetto support does not offer any programming help on writing custom tracking code.</p>
</blockquote>

<h3 id="prepare">Step 1 - Prepare tracking service</h3>
<p>First step is to make sure your tracking service is ready to receive data from Tripetto. Please first dive into the documentation of your tracking service to find out if/how it's possible to connect with it via JavaScript.</p>
<h4>In your tracking service:</h4>
<p>Make sure you have configured your tracking service. It depends on the tracking service you're using how to do this. It's important that your tracking service is able to access the domain that the Tripetto form is served from.</p>
<p class="help_article_wp_only">In your WordPress site this will be your own domain name in most cases, because your form will run on your own site's domain.</p>
<p class="help_article_studio_only">If you're using the Tripetto studio you can only track form activity if you have embedded the form in your own site. That's why you have to make sure that your tracking service is able to access that domain.</p>

<h3 id="connect">Step 2 - Connect tracking service</h3>
<p>Next step is to connect your Tripetto form to your tracking service. This is where your JavaScript programming skills come in.</p>
<h4>In Tripetto:</h4>
<p>Open your form in Tripetto. At the top menu bar of the form builder click <code><i class="fas fa-share-alt"></i> Automate</code><i class="fas fa-arrow-right"></i> Click <code>Tracking</code>. The Tracking pane will show up on the right side of the form builder.</p>
<p>After enabling the feature <code>Track form activity with custom tracking code</code> you can start with the custom tracking code.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-tripetto.png" width="1200" height="760" alt="Screenshot of tracking in Tripetto" loading="lazy" />
  <figcaption>Enable custom tracking code.</figcaption>
</figure>

<p>Now we have to develop the custom tracking code. The custom tracking code needs to be a function and should begin with <code>function() {</code> and end with <code>}</code>.</p>
<blockquote>
  <h4>🧯 Troubleshooting: Code analysis</h4>
  <p>Tripetto performs some static analysis on the correctness of the code. It is key to supply a function with the specified beginning and ending. Otherwise, the tracking code will simply not run.</p>
</blockquote>
<p>Within the function signature you should do two things:</p>
<ol>
  <li>Implement any initialization code for your tracking service (for example loading scripts);</li>
  <li>Return a function from your tracking function. That return function will be invoked by Tripetto every time an event is triggered.</li>
</ol>
<p>So, the basic tracking code will look like this:</p>
<pre class="line-numbers"><code class="language-javascript">function() {
  // Place your tracker initialization code here.

  // The function returned here is invoked by Tripetto each time a trackable event occurs.
  return function(event, form, block) {
    // Place your event tracking code here.
  }
}</code></pre>
<p>The return function receives three arguments:</p>
<ul>
  <li><code>event</code> - This is the event name and contains one of the following string values:
    <ul>
      <li><code>start</code> - Tracks when a form is started;</li>
      <li><code>complete</code> - Tracks when a form is completed;</li>
      <li><code>stage</code> - Tracks when a block becomes available.<br/>It depends on your selected <a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/">form face</a> when this event will be fired:
        <ul>
          <li>Autoscroll form face: this event will be fired when a block gets activated;</li>
          <li>Chat form face: this event fires when a block becomes answerable;</li>
          <li>Classic form face: this even fires when the block becomes visible.</li>
        </ul>
      </li>
      <li><code>unstage</code> - Tracks when a block becomes unavailable.<br/>It depends on your selected <a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/">form face</a> when this event will be fired:
        <ul>
          <li>Autoscroll form face: this event will be fired when a block gets deactivated;</li>
          <li>Chat form face: this event fires when a block becomes unanswerable;</li>
          <li>Classic form face: this even fires when the block becomes invisible.</li>
        </ul>
      </li>
      <li><code>focus</code> - Tracks when an input element gains focus;</li>
      <li><code>blur</code> - Tracks when an input element loses focus;</li>
      <li><code>pause</code> - Tracks when a form is paused.</li>
    </ul>
  </li>
  <li><code>form</code> - Object that contains information about the form. Has the following properties:
    <ul>
      <li><code>name</code> - String that contains the name of the form;</li>
      <li><code>reference</code> - String with the reference (identifier) of the form;</li>
      <li><code>runner</code> - String with the name of the runner;</li>
      <li><code>fingerprint</code> - String that contains the fingerprint of the form.</li>
    </ul>
  </li>
  <li><code>block</code> - Object that contains information about a block. Has the following properties:
    <ul>
      <li><code>id</code> - String with the identifier of the block;</li>
      <li><code>name</code> - String with the name of the block;</li>
      <li><code>alias</code> - String with the optional alias of the block.</li>
    </ul>
  </li>
</ul>

<h3 id="receive">Step 3 - Receive tracking data</h3>
<p>Assuming your tracking code is correct, your tracking service will now receive the configured tracking data from your Tripetto form. Let's test this.</p>
<blockquote class="help_article_studio_only">
  <h4>🚧 Warning: Only in embedded forms (studio only)</h4>
  <p>You can use the custom tracking code in the Tripetto studio at tripetto.app, but this will only work for your embedded forms. This will not work if you use the shareable link of your form at the tripetto.app domain.</p>
  <p>Please also make sure you always update your embed code in your website/app.</p>
</blockquote>
<h4>In Tripetto:</h4>
<p>Open your Tripetto form and submit an entry to it. The best way to do so, is by just opening your form and perform the actions you want to track yourself.</p>
<h4>In tracking service:</h4>
<p>Now switch back to your tracking service and have a look if the data is shown.</p>
<blockquote>
  <h4>📣 Info: Delay in your tracking service</h4>
  <p>Please notice that although Tripetto sends all events in realtime, it can take some time before a tracking service shows the events from your Tripetto form. Depending on your tracking service this can take up to 24 hours.</p>
</blockquote>
<p>From there on you can follow the instructions of your tracking service to configure the desired analysis. For help with this, please have a look at the help section of your tracking service.</p>
<blockquote>
  <h4>📌 Also see: Tracking data analysis</h4>
  <p>We have some help articles about the most common use cases for tracking data analysis:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-measure-completion-rates-with-form-activity-tracking/">How to measure completion rates with form activity tracking</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-measure-drop-offs-with-form-activity-tracking/">How to measure drop-offs with form activity tracking</a></li>
  </ul>
</blockquote>
<hr/>

<h2 id="examples">Examples</h2>
<p>To demonstrate the usage of the custom tracking code, we will show two custom tracking codes for often used tracking services (other than <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-google-analytics/">Google Analytics</a>, <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-google-tag-manager/">Google Tag Manager</a> and <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-facebook-pixel/">Facebook Pixel</a>), namely:</p>
<ul>
  <li>Fathom;</li>
  <li>Matomo.</li>
</ul>
<blockquote>
  <h4>🚧 Warning: No rights can be derived</h4>
  <p>The custom tracking code examples below are just to demonstrate the custom tracking code. No rights can be derived from these code examples. Tripetto support does not offer any programming help on writing custom tracking code.</p>
</blockquote>

<h3 id="fathom">Fathom</h3>
<p>See below the custom tracking code to <a href="https://usefathom.com/docs/script/embed" target="_blank" rel="noopener noreferrer">initialize the Fathom script</a> and <a href="https://usefathom.com/docs/features/events" target="_blank" rel="noopener noreferrer">send events to Fathom</a> with the <code>fathom.trackGoal()</code> function.</p>
<pre class="line-numbers"><code class="language-javascript">function() {
  // Init Fathom, replace FATHOM_SITE_ID
  var d=document,g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
  g.src='https://cdn.usefathom.com/script.js';
  g.async=true;
  g.dataset.site='FATHOM_SITE_ID';
  s.parentNode.insertBefore(g,s);

  // Send events to Fathom (only include the event rules below that you want to track)
  return function(event, form, block) {
    var fn = function() {
      if (typeof fathom === 'undefined') {
        return requestAnimationFrame(fn);
      }

      switch (event) {
        case 'start': fathom.trackGoal('START-EVENT-ID', 0); break;
        case 'stage': fathom.trackGoal('STAGE-EVENT-ID', 0); break;
        case 'unstage': fathom.trackGoal('UNSTAGE-EVENT-ID', 0); break;
        case 'focus': fathom.trackGoal('FOCUS-EVENT-ID', 0); break;
        case 'blur': fathom.trackGoal('BLUR-EVENT-ID', 0); break;
        case 'pause': fathom.trackGoal('PAUSE-EVENT-ID', 0); break;
        case 'complete': fathom.trackGoal('COMPLETE-EVENT-ID', 0); break;
      }
    };

    fn();
  };
}</code></pre>

<h3 id="matomo">Matomo</h3>
<p>See below the custom tracking code to <a href="https://developer.matomo.org/guides/tracking-javascript-guide" target="_blank" rel="noopener noreferrer">initialize the Matomo script</a> and <a href="https://developer.matomo.org/guides/tracking-javascript-guide" target="_blank" rel="noopener noreferrer">send events to Matomo</a> with the <code>_paq.push()</code> function.</p>
<pre class="line-numbers"><code class="language-javascript">function() {
  // Init Matomo, replace MATOMO_URL and MATOMO_SITE_ID
  var _paq = window._paq = window._paq || [];
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u='//MATOMO_URL/';
    _paq.push(['setTrackerUrl', u+'matomo.php']);
    _paq.push(['setSiteId', 'MATOMO_SITE_ID']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
  })();

  // Send events to Matomo (only include the event rules below that you want to track)
  return function(event, form, block) {
    switch (event) {
      case 'start': _paq.push(['trackEvent', 'Tripetto Form', 'Form is started.', form.name ]); break;
      case 'stage': _paq.push(['trackEvent', 'Tripetto Form', 'Form block becomes available.', block.name ]); break;
      case 'unstage': _paq.push(['trackEvent', 'Tripetto Form', 'Form block becomes unavailable.', block.name ]); break;
      case 'focus': _paq.push(['trackEvent', 'Tripetto Form', 'Form input element gained focus.', block.name ]); break;
      case 'blur': _paq.push(['trackEvent', 'Tripetto Form', 'Form input element lost focus.', block.name ]); break;
      case 'pause': _paq.push(['trackEvent', 'Tripetto Form', 'Form is paused.', form.name ]); break;
      case 'complete': _paq.push(['trackEvent', 'Tripetto Form', 'Form is completed.', form.name ]); break;
    }
  }
}</code></pre>
