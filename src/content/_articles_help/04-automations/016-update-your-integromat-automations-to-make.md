---
layout: help-article
base: ../../../
permalink: /help/articles/update-your-integromat-automations-to-make/
title: Update Integromat automations to Make - Tripetto Help Center
description: Since February 2022 Integromat is rebranded to Make. In this article we demonstrate how to update your Integromat automations in Tripetto to Make.
article_title: Update your Integromat automations to Make
article_folder: automate-webhook-integromat-make
author: jurgen
time: 3
category_id: automations
subcategory: automations_webhook_tools
areas: [studio, wordpress]
---
<p>Since February 2022 Integromat is rebranded to <a href="https://www.make.com/en?utm_source=tripetto-app&utm_medium=partner&utm_campaign=tripetto-app-partner-program" target="_blank" rel="noopener noreferrer">Make</a>. With that, Integromat becomes a legacy product offering. In this article we demonstrate how to update your Integromat automations in Tripetto to Make.</p>

<h2 id="when-to-update">When to update</h2>
<p>We advise to update your automation if you have a Tripetto form connected to Integromat via a webhook. In most cases that means you send data to a webhook URL that starts with <code>https://hook.integromat.com</code>.</p>
<p>Although Integromat will continue supporting those webhook URL's, they (and we) advise to prioritize upgrading your automations to the new Make platform, so you know for sure your automations keep working. Upgrading your Integromat automations to Make is easy and without any interruptions/downtime. We will demonstrate the steps to take below.</p>
<blockquote>
  <h4>📣 Info: What if I don't update my Integromat connections?</h4>
  <p>As far as we know, the old Integromat webhook URL's keep working, but they will send your data to the legacy product of Integromat. Integromat discontinues the support of their legacy product in 2023, so from then on you are at risk that your Integromat connections stop working and/or are not supported anymore.</p>
</blockquote>

<h2 id="how-to-update">How to update</h2>
<p>These are the global steps to update your Integromat automations to Make:</p>
<ol>
  <li>Upgrade your Integromat account to Make;</li>
  <li>Update your webhook URL in Tripetto.</li>
</ol>

<h3 id="upgrade-integromat-make">1. Upgrade Integromat to Make</h3>
<p>First you have to upgrade/migrate your Integromat account to a Make account. Make helps you with this.</p>
<h4>In Make</h4>
<p><a href="https://www.make.com/en?utm_source=tripetto-app&utm_medium=partner&utm_campaign=tripetto-app-partner-program" target="_blank" rel="noopener noreferrer">Sign up to Make</a> with the email address of your Integromat account. Make will recognize your email address and you can login with your Integromat credentials.</p>
<p>Make will now start their 'Upgrade hub' for you, which guides you through the whole automated migration. For most Integromat users, the upgrade process is fully automated. If your upgrade requires some manual steps, the Make upgrade tools inform you of this and guide you through the process step by step.</p>
<p>During the migration process all your legacy scenarios in Integromat will be disabled automatically. The corresponding scenarios in your new Make account will be enabled automatically. From then on the receiving webhook URL of your scenario has changed, but your legacy Integromat webhook URL will keep working. This way there's no interruption/downtime of your active automations.</p>
<p>After completion, all your Integromat scenarios, connections, webhooks and all other data are migrated to your new Make account.</p>
<blockquote>
  <h4>📣 Info: Update payment details</h4>
  <p>In case you're on a paid Integromat plan, you'll have to enter your payment details once in Make to keep using your original Integromat plan in Make.</p>
</blockquote>

<h3 id="update-webhook">2. Update webhook URL in Tripetto</h3>
<p>Now that you're upgraded to Make, you can update the webhook URL's in your Tripetto forms.</p>

<h4>In Make</h4>
<p>Open the scenario that uses Tripetto as a trigger to receive response data. In there, click the Tripetto app and copy the new Make webhook URL. That URL will start with: <code>https://hook.[region].make.com</code>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/make-webhook.png" width="1200" height="760" alt="Screenshot of Make" loading="lazy" />
  <figcaption>Copy the Make webhook URL.</figcaption>
</figure>

<h4>In Tripetto</h4>
<p>Now switch to Tripetto and open the form that is connected to this automation. At the top menu bar of the form builder click <code><i class="fas fa-share-alt"></i> Automate</code><i class="fas fa-arrow-right"></i> Click <code>Connections</code>. The Connections pane will show up on the right side of the form builder.</p>
<p>Now replace the old Integromat webhook URL with the new Make webhook URL that's on your clipboard.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tripetto-connections.png" width="1200" height="760" alt="Screenshot of Tripetto" loading="lazy" />
  <figcaption>Paste the Make webhook URL in the Connections screen in Tripetto.</figcaption>
</figure>
<h4>And that's it!</h4>
<p>Your Tripetto form will now send the collected data of each new form response to the new Make webhook URL. From there on Make will execute the scenario that gets triggered by the webhook URL.</p>
