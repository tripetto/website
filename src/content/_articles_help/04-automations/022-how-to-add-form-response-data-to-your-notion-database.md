---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-automatically-store-form-responses-in-notion/
title: Automatically store responses in Notion - Tripetto Help Center
description: Connect your Tripetto form to Notion to automatically store the form response data in a Notion database.
article_id: connection-notion
article_title: How to automatically store form responses in Notion
article_folder: automate-webhook-notion
author: jurgen
time: 4
category_id: automations
subcategory: automations_webhook_examples
areas: [studio, wordpress]
---
<p>Connect your Tripetto form to Notion to automatically store the form response data in a Notion database. For this example we use Make as our automation tool.</p>

<h2 id="when-to-use">When to use</h2>
<p>You can use this automation if you want to collect all data in your own Notion database automatically.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/notion-results.png" width="1200" height="760" alt="Screenshot of a database in Notion" loading="lazy" />
  <figcaption>Collect and use form data in Notion.</figcaption>
</figure>
<hr/>

<h2 id="what-you-need">What you need</h2>
<p>The scenario we describe in this article is an example of connecting Tripetto to other services, in this case Notion. <a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/">Click here for more general information about connecting Tripetto to other services.</a></p>
<p>For this scenario you need the following:</p>
<ul>
  <li><strong>Tripetto</strong>, to collect your form submissions;</li>
  <li><strong>Make</strong><sup>1</sup>, to connect your form data to other services;</li>
  <li><strong>Notion</strong>, to store the form data.</li>
</ul>
<blockquote>
  <h4>🔖 Ad 1: Make alternatives</h4>
  <p>There are alternatives to use as your automation tool. In our example we use Make as our automation tool, but you can also use Zapier, Pabbly Connect or even a custom webhook. Of course it's up to you to use your favorite automation tool.</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-connect-form-responses-to-automation-tools/">How to connect form responses to automation tools</a></li>
  </ul>
</blockquote>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>These are the steps to take to automate the process of storing form data in Google Sheets.</p>

<h3 id="step-1">Step 1 - Build Tripetto form</h3>
<ul>
  <li>First make sure your Tripetto form collects the desired data.</li>
</ul>
<p>The result of this step is your Tripetto form, which you will use to collect your data from your respondents.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tripetto-form.png" width="1200" height="760" alt="Screenshot of Tripetto" loading="lazy" />
  <figcaption>Build your Tripetto form.</figcaption>
</figure>

<h3 id="step-2">Step 2 - Prepare Notion database</h3>
<ul>
  <li>In your Notion workspace create a new database.</li>
  <li>Give your database a name, for example <code>Tripetto Form Results</code>.</li>
  <li>Add the column header names of the data fields you want to store in this database.</li>
  <li>By default Notion adds 3 empty rows to your database. You can delete those.</li>
</ul>
<p>The result of this step is your Notion database with the desired column headers, that we will use later on in the process.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/notion-template.png" width="1200" height="760" alt="Screenshot of Notion" loading="lazy" />
  <figcaption>Prepare your Notion database, with the columns header.</figcaption>
</figure>

<h3 id="step-3">Step 3 - Connect Tripetto to Make</h3>
<ul>
  <li>In your Make account add a new scenario.</li>
  <li>Give your scenario a name, for example <code>Add new database item to Notion for new Tripetto submission</code>.</li>
  <li>Add a <code>Tripetto</code> module to your Make scenario. As trigger select <code>Watch Responses</code>.</li>
  <li>Connect your Tripetto form to that trigger, by copy-pasting the webhook URL from Make into your Tripetto form (navigate to <code><i class="fas fa-share-alt"></i> Automate</code><i class="fas fa-arrow-right"></i><code>Connections</code><i class="fas fa-arrow-right"></i><code>Make</code>). <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-make/">Have a look at this article for the detailed description how to connect Tripetto to Make.</a></li>
  <li>Test the connection, so Make receives the structure of your dataset.</li>
</ul>
<p>The result of this step is that your Make scenario gets executed for each new form submission and you can use the data fields from your Tripetto form in the Make scenario.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/integromat-tripetto.png" width="1200" height="760" alt="Screenshot of Tripetto module in Make" loading="lazy" />
  <figcaption>Add a Tripetto trigger in your Make scenario.</figcaption>
</figure>

<h3 id="step-4">Step 4 - Add database item to Notion</h3>
<ul>
  <li>Add a <code>Notion</code> module to your Make scenario. As action select <code>Create a Database Item</code>.</li>
  <li>Grant access to your Notion workspace.</li>
  <li>Under <code>Database ID</code> select your earlier created Notion database.</li>
  <li>Make will now recognize the column header names that you prepared earlier. For each column name you can now select the corresponding data field from your Tripetto form. This is the part where you map the entered data from your Tripetto form to the columns in your Notion database.</li>
</ul>
<p>The result of this step is you now have automated sending the data from Tripetto to Notion.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/integromat-notion.png" width="1200" height="760" alt="Screenshot of Notion module in Make" loading="lazy" />
  <figcaption>Add a Notion module in your Make scenario to add a database item.</figcaption>
</figure>

<h3 id="step-5">Step 5 - Test and activate</h3>
<ul>
  <li>Click <code>Run once</code> in Make to start testing your automation.</li>
  <li>Fill out and submit your Tripetto form once.</li>
  <li>Check if your Make scenario has been processed correctly. If so, check the Notion database to see if the data is indeed added as a new database item.</li>
  <li>Activate your scenario in Make.</li>
</ul>
<p>From now on this scenario will be executed automatically for each new form submission! Every new submission will now automatically be added to your Notion database 🎉</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/notion-results.png" width="1200" height="760" alt="Screenshot of Notion" loading="lazy" />
  <figcaption>The data of each form submission has been added as a new database item in Notion.</figcaption>
</figure>

<hr />
<h2>Other examples</h2>
<p>Tripetto lets you connect to 1.000+ services. We have made some step-by-step examples for often used scenarios:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-store-form-responses-in-google-sheets/">How to automatically store form responses in Google Sheets and generate analysis reports</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-store-form-responses-in-notion/">How to automatically store form responses in Notion</a><span class="related-current">Current article</span></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-create-PDF-files-from-form-responses/">How to automatically create PDF files from form responses and send these to respondents</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-add-mailchimp-subscribers-from-form-responses/">How to automatically add Mailchimp subscribers from form responses</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-create-zendesk-support-tickets-from-form-responses/">How to automatically create Zendesk support tickets from form responses</a></li>
</ul>
