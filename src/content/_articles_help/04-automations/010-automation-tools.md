---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-connect-form-responses-to-automation-tools/
title: Connect form responses to automation tools - Tripetto Help Center
description: Automation tools are the glue between your Tripetto form responses and the online services that you want to automate.
article_title: How to connect form responses to automation tools
article_folder: automate-tools
article_video: automations-webhook
author: jurgen
time: 5
time_video: 2
category_id: automations
subcategory: automations_webhook_tools
areas: [studio, wordpress]
common_content_core: true
---
<p>Automation tools are the glue between your Tripetto form responses and the online services that you want to automate. This article covers some of the most used automation tools.</p>

<div class="article-content-core">
<h2 id="when-to-use">When to use</h2>
<p>Tripetto lets you collect data from your respondents in a smart way. That collected data on its own is already very valuable of course, but it becomes even more valuable if you let that data work for you automatically. And that's where automation tools come into the picture.</p>
<p>Automation tools enable you to connect different online services together, so you can automate workflows without any manual actions. In the case of Tripetto you can automate workflows when a new form response gets submitted and use the submitted data in those workflows. The automation tool is the place where you build such workflows. Therefore you can say that the automation tool is the glue between your Tripetto form responses and any other online services that you connect it with.</p>
<h3 id="examples">Examples</h3>
<p>There are lots of possible connections you could think of, but these are some popular services that help you with everyday tasks:</p>
</div>

<ul class="tiles tiles-two">
{% include tile.html url='/help/articles/how-to-automatically-store-form-responses-in-google-sheets/' type='Make' title='Tripetto to Google Sheets' description='Add a new row to a Google Sheet with all Tripetto response data.' webhook-service='google-sheets' webhook-service-name='Google Sheets' palette-top='light' palette-bottom='light' %}
{% include tile.html url='/help/articles/how-to-automatically-add-mailchimp-subscribers-from-form-responses/' type='Make' title='Tripetto to Mailchimp' description='Add a new subscriber to a Mailchimp audience based on Tripetto response data.' webhook-service='mailchimp' webhook-service-name='Mailchimp' palette-top='light' palette-bottom='light' %}
{% include tile.html url='/help/articles/how-to-automatically-create-zendesk-support-tickets-from-form-responses/' type='Make' title='Tripetto to Zendesk' description='Create a new Zendesk support ticket based on Tripetto response data.' webhook-service='zendesk' webhook-service-name='Zendesk' palette-top='light' palette-bottom='light' %}
{% include tile.html url='/help/articles/how-to-automatically-create-PDF-files-from-form-responses/' type='Make' title='Tripetto to Google Docs + Gmail' description='Generate a personalized Google Docs PDF based on Tripetto response data and send that via Gmail.' webhook-service='google-docs' webhook-service-name='Google Docs' webhook-chain-service='gmail' webhook-chain-service-name='Gmail' palette-top='light' palette-bottom='light' %}
</ul>

<div class="article-content-core">
<hr/>
<h2 id="tools">Automation tools</h2>
<p>There are lots of automation tools that you can use to build your workflows. Tripetto offers direct integrations with the most used automation tools, namely:</p>
<ul>
  <li>Make;</li>
  <li>Zapier;</li>
  <li>Pabbly Connect.</li>
</ul>
<p>Next to these often used automation tools, it's also possible to connect to any other automation tool that can receive data from a webhook. Or you can even use you own custom endpoint to receive the data and automate it from there on.</p>
<blockquote>
  <h4>💡 Tip: Choose your automation tool wisely</h4>
  <p>Make, Zapier and Pabbly Connect are some examples, but there are lots of other automation tools that you can use. It depends on your own needs, wishes and budget which tool fits you best. That's why we advise to have a close look at the features and pricings before you select a certain automation tool.</p>
</blockquote>

<h3 id="make" data-anchor="Make" class="title-logo"><img src="{{ page.base }}images/help/automate-webhook-services/make.png" width="200" height="200" alt="Logo Make" loading="lazy" />Make</h3>
<p>Make is our preferred automation partner that helps you to connect your Tripetto response data to other online services. They offer a Tripetto app to help you with this.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-make/">How to connect to other services with Make</a></li>
</ul>

<h3 id="zapier" class="title-logo"><img src="{{ page.base }}images/help/automate-webhook-services/zapier.svg" width="200" height="200" alt="Logo Zapier" loading="lazy" />Zapier</h3>
<p>Zapier is one of the earliest and well known automation tools. You can connect your Tripetto form responses via Zapier's Webhook app.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-zapier/">How to connect to other services with Zapier</a></li>
</ul>

<h3 id="pabbly" class="title-logo"><img src="{{ page.base }}images/help/automate-webhook-services/pabbly.png" width="200" height="200" alt="Logo Pabbly" loading="lazy" />Pabbly Connect</h3>
<p>Pabbly offers an automation tool that helps you to connect your Tripetto response data to other online services: Pabbly Connect. They offer a Tripetto app to help you with this.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-pabbly-connect/">How to connect to other services with Pabbly Connect</a></li>
</ul>

<h3 id="other">Other automation tools</h3>
<p>There are lots of alternative automation tools and to connect with those, Tripetto also offers an option to connect to any other webhook URL.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-custom-webhook/">How to connect to other services with custom webhook</a></li>
</ul>
</div>
