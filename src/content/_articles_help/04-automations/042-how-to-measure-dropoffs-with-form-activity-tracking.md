---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-measure-drop-offs-with-form-activity-tracking/
title: Measure drop-offs - Tripetto Help Center
description: By using form activity tracking you can analyze where respondents tend to drop off in your form.
article_id: tracking-dropoffs
article_title: How to measure drop-offs with form activity tracking
article_folder: automate-tracking
author: jurgen
time: 3
category_id: automations
subcategory: automations_tracking_examples
areas: [studio, wordpress]
---
<p>By using form activity tracking you can analyze where respondents tend to drop off in your form. Let's dive a little deeper into using tracking data for this kind of analysis.</p>

<h2 id="when-to-use">When to use</h2>
<p>You can track which questions in your form that get presented to each respondent and with that analyze where respondents tend to drop off. And with that analysis you can see if you need to improve your form at certain part(s).</p>
<p>To do this, Tripetto can track form activity of your respondents in your forms and send that data to <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-google-analytics/">Google Analytics</a>, <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-google-tag-manager/">Google Tag Manager</a>, <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-facebook-pixel/">Facebook Pixel</a> or even a <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-custom-tracking-code/">custom tracking code</a>. Let's see how to set this up and analyze the drop-offs in your form.
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>For this article we have connected our Tripetto form with <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-google-analytics/">Google Analytics</a> (GA4) and we'll use that to take a closer look at the analysis.</p>
<h3 id="track">Events to track</h3>
<p>You can activate the desired events in the tracking settings in Tripetto (<code><i class="fas fa-share-alt"></i> Automate</code><i class="fas fa-arrow-right"></i><code>Tracking</code>). To track questions that are presented to your respondents you need to track the following event:</p>
<ul>
  <li><code>Track staged blocks (<i>tripetto_stage</i>)</code> - Tracks when a block becomes available.<br/>It depends on your selected <a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/">form face</a> when this event will be fired:
    <ul>
      <li>Autoscroll form face: this event will be fired when a block gets activated;</li>
      <li>Chat form face: this event fires when a block becomes answerable;</li>
      <li>Classic form face: this even fires when the block becomes visible.</li>
    </ul>
  </li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/automate-tracking-analysis/tripetto-drop-offs.png" width="634" height="555" alt="Screenshot of tracking in Tripetto" loading="lazy" />
  <figcaption>Connect your Tripetto form with Google Analytics and activate the desired event.</figcaption>
</figure>
<h3 id="events">Events in Google Analytics</h3>
<p>If you now test your form, you will see that Google Analytics receives the tracking data. In Google Analytics the tracking data from Tripetto will be received in the section <code>Engagement > Events</code>. And you can see the recent events appear in the <code>Real-time</code> section (with a little delay).</p>
<blockquote>
  <h4>📣 Info: Delay in Google Analytics</h4>
  <p>Please notice that although Tripetto sends all events in realtime, it can take some time (up to 24 hours) before Google Analytics shows the events from your Tripetto form.</p>
</blockquote>
<figure>
  <img src="{{ page.base }}images/help/automate-tracking-analysis/staged.png" width="1200" height="760" alt="Screenshot of tripetto_stage event in Google Analytics" loading="lazy" />
  <figcaption>Tracking data of <code>tripetto_stage</code>: 3 question with different counts.</figcaption>
</figure>
<h3 id="analysis">Analysis</h3>
<p>In our example you can see our form has 3 questions, which not all have been presented to our respondents:</p>
<ul>
  <li>My first question: <strong>staged 5 times</strong>;</li>
  <li>My second question: <strong>staged 5 times</strong>;</li>
  <li>My third question: <strong>staged 4 times</strong>.</li>
</ul>
<p>With that data we can now analyze the drop-off. As we see a drop in the amount of times the third question has been presented, apparently there is one respondent that stopped the form at the second question and thus not proceeded to the third question.</p>
<blockquote>
  <h4>📣 Disclaimer</h4>
  <p>This analysis is not 100% bullet proof, but it gives an idea of where your respondents tend to drop off in your form.</p>
</blockquote>
