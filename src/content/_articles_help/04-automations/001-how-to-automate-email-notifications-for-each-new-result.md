---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-automate-email-notifications-for-each-new-result/
title: Automate email notifications - Tripetto Help Center
description: You can get automated email notifications when someone completes a form. Here is how to enable this feature.
article_title: How to automate email notifications for each new result
article_id: email
article_video: automations-email
article_folder: automate-email
author: jurgen
time: 2
time_video: 1
category_id: automations
subcategory: automations_notifications
areas: [studio, wordpress]
redirect_from:
- /help/articles/how-to-automate-email-notifications-in-the-studio/
- /help/articles/how-to-automate-email-notifications-in-the-wordpress-plugin/
---
<p>You can get automated email notifications when someone completes a form. Here is how to enable this feature.</p>

<h2 id="when-to-use">When to use</h2>
<p>It can be handy to get notified when a respondent completes a form, for example to have a look at its response. Tripetto offers multiple ways to get notified, including an <strong>email notification</strong>. You can very easily enable this for each form you'd like to.</p>
<p>This enables you to send email notifications to one recipient. Need to notify more recipients? Then add multiple <a href="{{ page.base }}help/articles/how-to-use-the-send-email-block/">send email blocks</a> to your form structure.</p>
<blockquote class="help_article_wp_only">
  <h4>📌 Also see: Email settings (WordPress only)</h4>
  <p>In the Tripetto WordPress plugin you can configure the sender name and sender address. More information:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-configure-plugin-settings-for-email-sending-and-spam-protection/">How to configure plugin settings for email sending and spam protection</a></li>
  </ul>
</blockquote>
<blockquote class="help_article_wp_only">
  <h4>📌 Also see: Email issues (WordPress only)</h4>
  <p>If you're experiencing mailing issues in the WordPress plugin, please have a look at this article for some troubleshooting:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/troubleshooting-email-notifications-from-wordpress-not-sent-or-marked-as-spam/">Email notifications from WordPress not sent or marked as spam</a></li>
  </ul>
</blockquote>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>At the top menu bar of the form builder click <code><i class="fas fa-share-alt"></i> Automate</code><i class="fas fa-arrow-right"></i> Click <code>Notifications</code>. The Notifications pane will show up on the right side of the form builder.</p>
<p>The first feature of this screen is <code>Email notification</code>. After enabling the feature <code>Send an email when someone completes your form</code> you can enter the email address of the recipient. From now on the entered recipient will receive an email message when someone completes the form.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/email-notification.png" width="1200" height="760" alt="Screenshot of email notification in Tripetto" loading="lazy" />
  <figcaption>Enable email notification.</figcaption>
</figure>

<h3 id="response-data">Include response data</h3>
<p>It's also possible to include all response data of the completed form directly inside the email message. To do so, just enable the checkbox <code>Include response data in the message</code> and all given answers will be shown in the email message automatically.</p>
<div class="help_article_wp_only">
  <h4>Make files accessible (WordPress only)</h4>
  <p>If you have a <a href="{{ page.base }}help/articles/how-to-use-the-file-upload-block/">File Upload block</a> or a <a href="{{ page.base }}help/articles/how-to-use-the-signature-block/">Signature block</a> in your form in WordPress, you can download the files from your respondents with a download link. For security reasons the download links will only work if you are logged in to your WP Admin.</p>
  <p>If you want to make the download links available from the email notification, without the need to be logged in, you can enable the setting <code>Allow direct access to uploaded files without WP Admin login</code>.</p>
</div>
