---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-connect-to-other-services-with-make/
title: Connect to other services with Make - Tripetto Help Center
description: Make is our preferred automation partner that helps you to connect your Tripetto response data to other online services.
article_title: How to connect to other services with Make
article_id: webhook
article_folder: automate-webhook-integromat
article_video: automations-make
author: jurgen
time: 5
time_video: 4
category_id: automations
subcategory: automations_webhook_tools
areas: [studio, wordpress]
common_content_core: true
redirect_from:
- /help/articles/how-to-connect-to-other-services-with-integromat/
- /help/articles/how-to-connect-to-other-services-with-make-formerly-integromat/
---
<p>Make is our preferred automation partner that helps you to connect your Tripetto response data to other online services. This article describes how to enable our webhook for this and how to configure it with Make.</p>

<div class="article-content-core">
<blockquote>
  <h4>🔔 Notice: Update your Integromat automations to Make</h4>
  <p>Since February 2022 Integromat is rebranded to <a href="https://www.make.com/en?utm_source=tripetto-app&utm_medium=partner&utm_campaign=tripetto-app-partner-program" target="_blank" rel="noopener noreferrer">Make</a>. With that, Integromat becomes a legacy product offering. This can have an impact on your existing automations from Tripetto to Integromat. More information:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/update-your-integromat-automations-to-make/">Update your Integromat automations to Make</a></li>
  </ul>
</blockquote>

<h2 id="when-to-use">When to use</h2>
<p>By connecting Tripetto to other services you can do all kinds of actions with your response data, like pushing it to a spreadsheet editor (Microsoft Excel/Google Sheets) or a database, or trigger other follow-up actions. The possibilities are endless!</p>
<p>Make is our preferred partner to help you with this. Follow the instructions in this article if you want to use Make to connect your Tripetto response data with other software services.</p>
<blockquote>
  <h4>📣 Info: Make alternatives</h4>
  <p>There are alternatives to use for Make, for example <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-zapier/">Zapier</a>, <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-pabbly-connect/">Pabbly Connect</a> or even a <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-custom-webhook/">custom webhook</a>. Of course it's up to you to use your favorite automation tool.</p>
</blockquote>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>In this article we will show the steps you have to take to connect Tripetto with Make. You can also have a look at our <a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/">global article about how webhooks work</a>.</p>
<blockquote>
  <h4>🚧 Warning: Make knowlegde needed</h4>
  <p>We assume you know how to configure and use Make. Tripetto support can not help you with configuring this and/or the services that you want to connect your Tripetto form to.</p>
</blockquote>

<h3 id="step-1">Step 1 - Prepare Make</h3>
<p>Make makes it easy for you to connect with Tripetto. They offer a dedicated <a href="https://www.make.com/en/integrations/tripetto?utm_source=tripetto-app&utm_medium=partner&utm_campaign=tripetto-app-partner-program" target="_blank" rel="noopener noreferrer">Tripetto integration, which you can find over here</a>.</p>
<div>
  <a href="https://www.make.com/en/integrations/tripetto?utm_source=tripetto-app&utm_medium=partner&utm_campaign=tripetto-app-partner-program" target="_blank" rel="noopener noreferrer" class="blocklink">
    <div>
      <span class="title">Tripetto on Make<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Create your customized scenario with Make. Automate repetitive tasks involved in using Tripetto and make your work easier.</span>
      <span class="url">make.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/automate-webhook-services/make.png" alt="Make logo" width="281" height="281" loading="lazy" />
    </div>
  </a>
</div>
<h4>In Make:</h4>
<p>In Make simply create a new scenario and add the Tripetto module to it. Select the trigger <code>Watch responses</code> to let Make watch for new Tripetto responses.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-trigger.png" width="1200" height="760" alt="Screenshot of Make" loading="lazy" />
  <figcaption>Select the <code>Watch responses</code> trigger of Tripetto.</figcaption>
</figure>

<h3 id="step-2">Step 2 - Connect Make</h3>
<p>After you selected the <code>Watch responses</code> trigger, you can add a webhook connection to it. We advise to create a new webhook connection for each Tripetto form.</p>
<h4>In Make:</h4>
<p>Click <code>Add</code> to create a new webhook. You can give the webhook a name, so you can recognize to which Tripetto form that webhook is connected.</p>
<p>Now, underneath the selected webhook, you see a URL. That's your webhook URL that you need in your Tripetto form. Click <code>Copy address to clipboard</code>, as we will need that URL in Tripetto now.</p>
<p>Click <code>OK</code> to save your settings.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-webhook.png" width="1200" height="760" alt="Screenshot of Make" loading="lazy" />
  <figcaption>The result of your webhook connection with the webhook URL.</figcaption>
</figure>

<h4>In Tripetto:</h4>
<p>Now switch to your form in Tripetto. At the top menu bar of the form builder click <code><i class="fas fa-share-alt"></i> Automate</code><i class="fas fa-arrow-right"></i> Click <code>Connections</code>. The Connections pane will show up on the right side of the form builder.</p>
<p>The first option in this screen is <code>Make</code>. After enabling the feature <code>Submit completed forms to Make</code> you can paste the webhook URL you got from your new Make scenario.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/03-tripetto.png" width="1200" height="760" alt="Screenshot of the webhook in Tripetto" loading="lazy" />
  <figcaption>Copy-paste your webhook URL in Tripetto.</figcaption>
</figure>
<blockquote class="help_article_wp_only">
  <h4>📌 Also see: Using file uploads (WordPress)</h4>
  <p>If you have a <a href="{{ page.base }}help/articles/how-to-use-the-file-upload-block/">File Upload block</a> or a <a href="{{ page.base }}help/articles/how-to-use-the-signature-block/">Signature block</a> in your form in WordPress, you can download the files from your respondents with a download link. By default such download links are only available to the owner of the form. You have to be logged in to the corresponding WordPress site (WP Admin) to be able to download the files. If you want to make the download links available for Make you can enable the setting <code>Allow access to uploaded files</code>.</p>
  <p>For more instructions have a look at this help article about using file uploads from the WordPress plugin in webhooks:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/use-file-uploads-from-the-wordpress-plugin-in-webhooks/">Use file uploads from the WordPress plugin in webhooks</a></li>
  </ul>
</blockquote>
<blockquote class="help_article_studio_only">
  <h4>📌 Also see: Using file uploads (studio)</h4>
  <p>If you have a <a href="{{ page.base }}help/articles/how-to-use-the-file-upload-block/">File Upload block</a> or a <a href="{{ page.base }}help/articles/how-to-use-the-signature-block/">Signature block</a> in your form in the studio, you can download the files from your respondents with a download link. In general such download links are only available to the owner of the form. You have to be logged in to the corresponding Tripetto studio account to be able to download the files. Only for the first 24 hours after form submissions, those download links will be available to Make.</p>
  <p>For more instructions have a look at this help article about using file uploads from the studio in webhooks:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/use-file-uploads-from-the-studio-in-webhooks/">Use file uploads from the studio in webhooks</a></li>
  </ul>
</blockquote>

<h3 id="step-3">Step 3 - Receive response data</h3>
<p>Now your Tripetto form is connected to Make. Next step is to get the structure of your form to Make, so you can use that structure and data in your follow-up services.</p>
<h4>In Make:</h4>
<p>To do so, first switch back to Make and click the big <code>Run once</code> button. Make is now temporarily opened to receive a response from your Tripetto form.</p>
<h4>In Tripetto:</h4>
<p>Now, switch back to Tripetto and submit some response data. You can do that by simply clicking the <code>Test</code> button. That will send a test response from your form to Make with some dummy content. Or you can also simply submit a real form entry yourself via the shareable link of your form.</p>
<h4>In Make:</h4>
<p>After that, you can switch to Make again. You'll see the Tripetto module has an update now. When you click that update bubble, you'll see the response data from your form. Great, you're connected!</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/04-data.png" width="1200" height="760" alt="Screenshot of Make" loading="lazy" />
  <figcaption>Your response data has been received by Make.</figcaption>
</figure>
<p>Your connection between Tripetto and Make is all set! Now it's up to you what follow-up actions you want to attach to it.</p>

<h3 id="step-4">Step 4 - Add services</h3>
<p>Now it's time to connect the services you want.</p>
<h4>In Make:</h4>
<p>At the right side of the Tripetto module you can add one or more modules of other services that you want to connect. Simply add a module and search for the wanted service. It depends on the service how you connect to it and what options are available.</p>
<p>If the service you want to connect supports the input of response fields, you can map which Tripetto field must be entered in each input field. That way you can determine where your Tripetto response data goes to in the connected service.</p>
<p>You can test your scenario all the time by clicking the <code>Run once</code> button in Make. Make sure you submit a new Tripetto response each time you want to test it.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/05-scenario.png" width="1200" height="760" alt="Screenshot of Make" loading="lazy" />
  <figcaption>An example of a scenario that's connected to a Google Sheets file.</figcaption>
</figure>

<h3 id="step-5">Step 5 - Activate magic</h3>
<p>If you're done editing your scenario, you can activate it.</p>
<h4>In Make:</h4>
<p>Switch the schedule button at the bottom from <code><i class="fas fa-toggle-off"></i> Off</code> to <code><i class="fas fa-toggle-on"></i> On</code>. We advise to schedule the scenario <code>Immediately</code>, so the scenario handles each Tripetto response immediately when it is submitted.</p>
<p>From now on, each completed submission of this Tripetto form will send its data to Make and Make will execute the magic into other services.</p>
<hr />

<h2 id="possibilities">Discover the possibilities!</h2>
<p>Make supports lots of great services to connect to.</p>

<h3 id="examples">Examples</h3>
<p>We listed some common used scenarios that you can start with right away!</p>
</div>
<ul class="tiles tiles-two">
{% include tile.html url='https://www.make.com/en/integrations/tripetto/google-sheets?utm_source=tripetto-app&utm_medium=partner&utm_campaign=tripetto-app-partner-program' target=true type='Make' title='Tripetto to Google Sheets' description='Add a new row to a Google Sheet with all Tripetto response data.' webhook-service='google-sheets' webhook-service-name='Google Sheets' palette-top='light' palette-bottom='light' %}
{% include tile.html url='https://www.make.com/en/integrations/tripetto/microsoft-to-do?utm_source=tripetto-app&utm_medium=partner&utm_campaign=tripetto-app-partner-program' target=true type='Make' title='Tripetto to Microsoft To Do' description='Add a new task to a list in Microsoft To Do based on Tripetto response data.' webhook-service='microsoft-to-do' webhook-service-name='Microsoft To Do' palette-top='light' palette-bottom='light' %}
{% include tile.html url='https://www.make.com/en/integrations/tripetto/mailchimp?utm_source=tripetto-app&utm_medium=partner&utm_campaign=tripetto-app-partner-program' target=true type='Make' title='Tripetto to Mailchimp' description='Add a new subscriber to a Mailchimp audience based on Tripetto response data.' webhook-service='mailchimp' webhook-service-name='Mailchimp' palette-top='light' palette-bottom='light' %}
{% include tile.html url='https://www.make.com/en/integrations/tripetto/shopify?utm_source=tripetto-app&utm_medium=partner&utm_campaign=tripetto-app-partner-program' target=true type='Make' title='Tripetto to Shopify' description='Create a new customer and create a new order in Shopify based on Tripetto response data.' webhook-service='shopify' webhook-service-name='Shopify' webhook-chain-service='shopify' webhook-chain-service-name='Shopify' palette-top='light' palette-bottom='light' %}
{% include tile.html url='https://www.make.com/en/integrations/tripetto/zendesk?utm_source=tripetto-app&utm_medium=partner&utm_campaign=tripetto-app-partner-program' target=true type='Make' title='Tripetto to Zendesk' description='Create a new Zendesk support ticket based on Tripetto response data.' webhook-service='zendesk' webhook-service-name='Zendesk' palette-top='light' palette-bottom='light' %}
{% include tile.html url='https://www.make.com/en/integrations/tripetto/airtable?utm_source=tripetto-app&utm_medium=partner&utm_campaign=tripetto-app-partner-program' target=true type='Make' title='Tripetto to Airtable + PayPal' description='Add a new record to an Airtable database with all Tripetto response data and create a payment in PayPal.' webhook-service='airtable' webhook-service-name='Airtable' webhook-chain-service='paypal' webhook-chain-service-name='PayPal' palette-top='light' palette-bottom='light' %}
</ul>
<div class="article-content-core">

<h3 id="templates">Templates</h3>
<p>We prepared some templates that you can start with right away in Make. Templates help you to quickly setup a scenario.</p>
<div>
  <a href="https://www.make.com/en/integrations/tripetto?utm_source=tripetto-app&utm_medium=partner&utm_campaign=tripetto-app-partner-program" target="_blank" rel="noopener noreferrer" class="blocklink">
    <div>
      <span class="title">Tripetto templates on Make<i class="fas fa-external-link-alt"></i></span>
      <span class="description">You can choose from our templates for Tripetto. You can use them as they are or customize them to suit your needs.</span>
      <span class="url">make.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/automate-webhook-services/make.png" alt="Make logo" width="281" height="281" loading="lazy" />
    </div>
  </a>
</div>

<h3 id="all">All Make modules</h3>
<p>Take a look at the endless possibilities of services in Make in their <a href="https://www.make.com/en/integrations?community=1&verified=1&utm_source=tripetto-app&utm_medium=partner&utm_campaign=tri" target="_blank" rel="noopener noreferrer">apps & services overview</a>. Just make sure you configure the Tripetto module as described in the article above and then connect it with the service(s) you want.</p>
<div>
  <a href="https://www.make.com/en/integrations?community=1&verified=1&utm_source=tripetto-app&utm_medium=partner&utm_campaign=tri" target="_blank" rel="noopener noreferrer" class="blocklink">
    <div>
      <span class="title">Apps & Services - Make<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Explorer all apps and services you can connect to with Make.</span>
      <span class="url">make.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/automate-webhook-services/make.png" alt="Make logo" width="281" height="281" loading="lazy" />
    </div>
  </a>
</div>

</div>
