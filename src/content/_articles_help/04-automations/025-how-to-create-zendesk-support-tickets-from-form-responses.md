---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-automatically-create-zendesk-support-tickets-from-form-responses/
title: Automatically create Zendesk support tickets - Tripetto Help Center
description: Connect your Tripetto form to Zendesk to automatically create users and support tickets.
article_id: connection-zendesk
article_title: How to automatically create Zendesk support tickets from form responses
article_folder: automate-webhook-zendesk
author: jurgen
time: 5
category_id: automations
subcategory: automations_webhook_examples
areas: [studio, wordpress]
---
<p>Connect your Tripetto form to Zendesk to automatically create users and support tickets. For this example we use Make as our automation tool.</p>

<h2 id="when-to-use">When to use</h2>
<p>You can use this automation if you use a Tripetto form as a support form. It will automatically create support tickets in Zendesk based on the submitted form data.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/zendesk-ticket.png" width="1200" height="760" alt="Screenshot of a support ticket in Zendesk" loading="lazy" />
  <figcaption>Add support tickets in Zendesk.</figcaption>
</figure>
<hr/>

<h2 id="what-you-need">What you need</h2>
<p>The scenario we describe in this article is an example of connecting Tripetto to other services, in this case Zendesk. <a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/">Click here for more general information about connecting Tripetto to other services.</a></p>
<p>For this scenario you need the following:</p>
<ul>
  <li><strong>Tripetto</strong>, to collect your form submissions;</li>
  <li><strong>Make</strong><sup>1</sup>, to connect your form data to other services;</li>
  <li><strong>Zendesk</strong>, to store your support tickets.</li>
</ul>
<blockquote>
  <h4>🔖 Ad 1: Make alternatives</h4>
  <p>There are alternatives to use as your automation tool. In our example we use Make as our automation tool, but you can also use Zapier, Pabbly Connect or even a custom webhook. Of course it's up to you to use your favorite automation tool.</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-connect-form-responses-to-automation-tools/">How to connect form responses to automation tools</a></li>
  </ul>
</blockquote>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>These are the steps to take to automate the process of creating support tickets in Zendesk.</p>

<h3 id="step-1">Step 1 - Build Tripetto form</h3>
<ul>
  <li>First make sure your Tripetto form collects the desired data.</li>
  <li>Please make sure you at least ask for the client's name and email address. And collect the needed data for the support ticket, at least with a subject and a support message.</li>
</ul>
<p>The result of this step is your Tripetto form, which you will use to collect your data from your respondents.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tripetto-form.png" width="1200" height="760" alt="Screenshot of Tripetto" loading="lazy" />
  <figcaption>Build your Tripetto form.</figcaption>
</figure>

<h3 id="step-2">Step 2 - Prepare Zendesk</h3>
<ul>
  <li>If you already have an existing Zendesk setup you can skip this step.</li>
  <li>If you don't have configured Zendesk yet, create an account and configure it for your purpose.</li>
</ul>
<p>The result of this step is your Zendesk ticket system, that we will use later on in the process.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/zendesk.png" width="1200" height="760" alt="Screenshot of Zendesk" loading="lazy" />
  <figcaption>Prepare your Zendesk ticket system.</figcaption>
</figure>

<h3 id="step-3">Step 3 - Connect Tripetto to Make</h3>
<ul>
  <li>In your Make account add a new scenario.</li>
  <li>Give your scenario a name, for example <code>Create support ticket for new Tripetto submission</code>.</li>
  <li>Add a <code>Tripetto</code> module to your Make scenario. As trigger select <code>Watch Responses</code>.</li>
  <li>Connect your Tripetto form to that trigger, by copy-pasting the webhook URL from Make into your Tripetto form (navigate to <code><i class="fas fa-share-alt"></i> Automate</code><i class="fas fa-arrow-right"></i><code>Connections</code><i class="fas fa-arrow-right"></i><code>Make</code>). <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-make/">Have a look at this article for the detailed description how to connect Tripetto to Make.</a></li>
  <li>Test the connection, so Make receives the structure of your dataset.</li>
</ul>
<p>The result of this step is that your Make scenario gets executed for each new form submission and you can use the data fields from your Tripetto form in the Make scenario.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/integromat-tripetto.png" width="1200" height="760" alt="Screenshot of Tripetto module in Make" loading="lazy" />
  <figcaption>Add a Tripetto trigger in your Make scenario.</figcaption>
</figure>

<h3 id="step-4">Step 4 - Add user to Zendesk</h3>
<ul>
  <li>Add a <code>Zendesk</code> module to your Make scenario. As action select <code>Create or Update a User</code>.</li>
  <li>Grant access to your Zendesk account.</li>
  <li>Make will now show the data fields to create a user in Zendesk. For each field you can now select the corresponding data field from your Tripetto form. This is the part where you map the entered user data from your Tripetto form to the fields in your Zendesk user. Most important fields are <code>Name</code> and <code>Email</code>.</li>
</ul>
<p>The result of this step is you now have automated creating a user in Zendesk.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/integromat-zendesk-user.png" width="1200" height="760" alt="Screenshot of Zendesk module in Make" loading="lazy" />
  <figcaption>Add a Zendesk module in your Make scenario to create a user.</figcaption>
</figure>

<h3 id="step-5">Step 5 - Create ticket in Zendesk</h3>
<ul>
  <li>Add another <code>Zendesk</code> module to your Make scenario. As action select <code>Create a Ticket</code>.</li>
  <li>Make will now show the data fields to create a ticket in Zendesk. For each field you can now select the corresponding data field from your Tripetto form. This is the part where you map the entered support request data from your Tripetto form to the fields in your Zendesk ticket. Most important fields are <code>Subject</code> and <code>Body</code>.</li>
  <li>Determine if you want to add this message as a <code>Public comment</code> or as an internal note.</li>
  <li>To attach the previously created Zendesk user to this ticket, insert the <code>User ID</code> from the previous Zendesk module into the ticket fields <code>Author ID</code> and <code>Requester ID</code>.</li>
</ul>
<p>The result of this step is you now have automated creating a ticket in Zendesk for the selected Zendesk user.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/integromat-zendesk-ticket.png" width="1200" height="760" alt="Screenshot of Zendesk module in Make" loading="lazy" />
  <figcaption>Add a Zendesk module in your Make scenario to create a ticket.</figcaption>
</figure>

<h3 id="step-6">Step 6 - Test and activate</h3>
<ul>
  <li>Click <code>Run once</code> in Make to start testing your automation.</li>
  <li>Fill out and submit your Tripetto form once.</li>
  <li>Check if your Make scenario has been processed correctly. If so, check Zendesk if the new ticket is created and the user is attached to that ticket.</li>
  <li>Activate your scenario in Make.</li>
</ul>
<p>From now on this scenario will be executed automatically for each new form submission! Every new submission will now automatically create a new user and a new support ticket in Zendesk 🎉</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/zendesk-ticket.png" width="1200" height="760" alt="Screenshot of Zendesk" loading="lazy" />
  <figcaption>The ticket has been created in Zendesk.</figcaption>
</figure>

<hr />
<h2>Other examples</h2>
<p>Tripetto lets you connect to 1.000+ services. We have made some step-by-step examples for often used scenarios:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-store-form-responses-in-google-sheets/">How to automatically store form responses in Google Sheets and generate analysis reports</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-store-form-responses-in-notion/">How to automatically store form responses in Notion</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-create-PDF-files-from-form-responses/">How to automatically create PDF files from form responses and send these to respondents</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-add-mailchimp-subscribers-from-form-responses/">How to automatically add Mailchimp subscribers from form responses</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-create-zendesk-support-tickets-from-form-responses/">How to automatically create Zendesk support tickets from form responses</a><span class="related-current">Current article</span></li>
</ul>
