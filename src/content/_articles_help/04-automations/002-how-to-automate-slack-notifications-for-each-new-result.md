---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-automate-slack-notifications-for-each-new-result/
title: Automate Slack notifications - Tripetto Help Center
description: You can get automated Slack notifications when someone completes a form. Here is how to enable this feature.
article_title: How to automate Slack notifications for each new result
article_id: slack
article_folder: automate-slack
article_video: automations-slack
author: jurgen
time: 3
time_video: 1
category_id: automations
subcategory: automations_notifications
areas: [studio, wordpress]
redirect_from:
- /help/articles/how-to-automate-slack-notifications-in-the-studio/
- /help/articles/how-to-automate-slack-notifications-in-the-wordpress-plugin/
---
<p>You can get automated Slack notifications when someone completes a form. Here is how to enable this feature.</p>

<h2 id="when-to-use">When to use</h2>
<p>It can be handy to get notified when a respondent completes a form, for example to have a look at its response. Tripetto offers multiple ways to get notified, including a <strong>Slack notification</strong>. You can very easily enable this for each form you'd like to.</p>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>You have to take some steps in Slack and Tripetto to enable the Slack notification. We will help you with that!</p>
<blockquote>
  <h4>🚧 Warning: Slack knowlegde needed</h4>
  <p>We assume you know how to use Slack. Tripetto support can not help you with configuring Slack.</p>
</blockquote>

<h3 id="slack">Step 1 - Prepare Slack</h3>
<p>Before Slack can receive messages from external sources (like in this case Tripetto) you have to create an <strong>Incoming Webhook</strong> in your Slack App.</p>
<h4>In Slack:</h4>
<p>Follow the steps in <a href="https://api.slack.com/messaging/webhooks" target="_blank" rel="noopener noreferrer">this article at the Slack website</a> to prepare Slack for receiving notifications.</p>
<div>
  <a href="https://api.slack.com/messaging/webhooks" target="_blank" rel="noopener noreferrer" class="blocklink">
    <div>
      <span class="title">Sending messages using Incoming Webhooks - Slack<i class="fas fa-external-link-alt"></i></span>
      <span class="description">We're going to walk through a really quick 4-step process that will have you posting messages using Incoming Webhooks in a few minutes.</span>
      <span class="url">api.slack.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-slack.png" width="160" height="160" alt="Slack logo" loading="lazy" />
    </div>
  </a>
</div>
<p>You only have to follow steps 1 to 3 of this Slack article, so don't worry about step 4. At the end of step 3 you will get a webhook URL from Slack. Copy that URL to your clipboard, as we will need that URL in Tripetto now.</p>

<h3 id="enable">Step 2 - Connect Slack</h3>
<p>Next step is to connect your Tripetto form with your Slack channel.</p>
<h4>In Tripetto:</h4>
<p>Open your form in Tripetto. At the top menu bar of the form builder click <code><i class="fas fa-share-alt"></i> Automate</code><i class="fas fa-arrow-right"></i> Click <code>Notifications</code>. The Notifications pane will show up on the right side of the form builder.</p>
<p>The second feature of this screen is <code>Slack notification</code>. After enabling the feature <code>Send a Slack message when someone completes your form</code> you can enter the webhook URL you got from your Slack App. From now on the designated Slack channel will receive a Slack message when someone completes the form.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/slack-notification.png" width="1200" height="760" alt="Screenshot of Slack notification in Tripetto" loading="lazy" />
  <figcaption>Enable Slack notification.</figcaption>
</figure>

<h4>Include response data</h4>
<p>It's also possible to include all response data of the completed form directly inside the Slack message. To do so, just enable the checkbox <code>Include response data in the message</code> and all given answers will be shown in the Slack message automatically.</p>

<div class="help_article_wp_only">
  <h4>Make files accessible (WordPress only)</h4>
  <p>If you have a <a href="{{ page.base }}help/articles/how-to-use-the-file-upload-block/">File Upload block</a> or a <a href="{{ page.base }}help/articles/how-to-use-the-signature-block/">Signature block</a> in your form in WordPress, you can download the files from your respondents with a download link. For security reasons the download links will only work if you are logged in to your WP Admin.</p>
  <p>If you want to make the download links available from the Slack notification, without the need to be logged in, you can enable the setting <code>Allow direct access to uploaded files without WP Admin login</code>.</p>
</div>

<h3 id="test">Step 3 - Test Slack connection</h3>
<p>Now your Slack connection is all set and you can test it.</p>
<h4>In Tripetto:</h4>
<p>Click the <code>Test</code> button. If the connection is configured correctly, you will receive a test response in your Slack channel. If you have enabled the setting to include form data, you will also see some dummy content for your particular form structure.</p>
<hr/>

<h2 id="use-case" data-anchor="Use case">Use case: how we use Slack notifications at Tripetto</h2>
<p>Slack notifications are especially handy for teams. At Tripetto we use Slack notifications for example in all of our website forms. It enables us to keep track of all submitted forms, including a centralized archive of all submitted form data.</p>
<p>We've also made the internal agreement to mark each Slack notification with a <code><i class="fas fa-eye"></i></code> icon when you've read the message, so we all know who saw the message.</p>
<p>And we use the Slack threads for each message to discuss potential follow-ups.</p>
