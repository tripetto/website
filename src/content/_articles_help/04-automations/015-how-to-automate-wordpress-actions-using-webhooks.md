---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-automate-actions-inside-your-wordpress-site-using-automator-plugins/
title: Automate actions inside WordPress - Tripetto Help Center
description: Next to connecting to external services, webhooks also enable you to perform actions inside your own WordPress environment.
article_title: How to automate actions inside your WordPress site using automator plugins
article_id: webhook
article_folder: automate-webhook
author: jurgen
time: 5
category_id: automations
subcategory: automations_webhook_tools
areas: [wordpress]
common_content_core: true
---
<p>Next to connecting to external services, webhooks also enable you to perform actions inside your own WordPress environment for each new result from your Tripetto forms, like creating a new user or post inside your WordPress site. Or even connect to your other WordPress plugins. Without a single line of code. Fully no-code!</p>

<div class="article-content-core">
<h2 id="when-to-use">When to use</h2>
<p>In our <a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/">global help articles about webhooks</a>, we mention all kinds of external services that you can connect your Tripetto form data to, like Google Sheets, Mailchimp, Zendesk, Airtable and PayPal. But it's just as easy to <strong>do actions inside your own WordPress site/environment</strong>, for example to add a new user or edit the roles of an existing user. You can even connect to other plugins that are installed in your WordPress environment.</p>
<p>And the cool thing is your data doesn't even have to leave your own WordPress environment, because there are automation tools that you can use inside your own WordPress environment, like <a href="https://automatorwp.com/" target="_blank" rel="noopener noreferrer"><strong>AutomatorWP</strong></a> and <a href="https://automatorplugin.com/" target="_blank" rel="noopener noreferrer"><strong>Uncanny Automator</strong></a>.</p>

<h3 id="examples">Examples</h3>
<p>There are lots of possible actions you can do in your WordPress site, but these are some popular ones that will connect your Tripetto response data to your WordPress environment:</p>
</div>

<ul class="tiles tiles-two">
{% include tile.html url='https://automatorwp.com/add-ons/wordpress/' target=true type='AutomatorWP' title='Tripetto to WordPress' description='Create a new WordPress user based on Tripetto response data.' webhook-service='wordpress' webhook-service-name='WordPress' palette-top='light' palette-bottom='light' %}
{% include tile.html url='https://automatorwp.com/add-ons/fluentcrm/' target=true type='AutomatorWP' title='Tripetto to Fluent CRM' description='Create a new contact based on Tripetto response data.' webhook-service='fluentcrm' webhook-service-name='FluentCRM' palette-top='light' palette-bottom='light' %}
{% include tile.html url='https://automatorplugin.com/integration/elementor/' target=true type='Uncanny Automator' title='Tripetto to Elementor' description='Show an Elementor popup after form completion.' webhook-service='elementor' webhook-service-name='Elementor' palette-top='light' palette-bottom='light' %}
{% include tile.html url='https://automatorplugin.com/integration/wordpress-core/' target=true type='Uncanny Automator' title='Tripetto to WordPress' description='Edit the content of a WordPress post based on Tripetto response data.' webhook-service='wordpress' webhook-service-name='WordPress' palette-top='light' palette-bottom='light' %}
</ul>
<hr/>

<div class="article-content-core">
<h2 id="how-to-use">How to use</h2>
<p>The global idea for automating WordPress actions based on Tripetto form submissions is the same as we described in our other webhook help articles: you always have a webhook URL in your Tripetto form that gets triggered after each form submission. And with that trigger, you can automate all kinds of actions. <a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/">Click here for the global help article about webhooks.</a></p>
<p>But now we're going to have a look at how to do that within your own WordPress environment. We do that by using WordPress specific automator plugins that let you do actions inside your own WordPress environment, without the data ever leaving your WordPress environment. Setting this up takes a few steps, which we will describe globally in this article.</p>
<blockquote>
  <h4>🚧 Warning: Automator plugin knowlegde needed</h4>
  <p>We assume you know how to configure and use your desired WordPress automator plugin. Tripetto support can not help you with configuring this and/or the services that you want to connect your Tripetto form to.</p>
</blockquote>

<h3 id="step-1">Step 1 - Prepare WordPress automator plugin</h3>
<p>First step is to be able to use a webhook inside your own WordPress environment. There are several plugins that can help you with that. Such plugins let you create a webhook URL on your own domain, which you can use to send your Tripetto data to.</p>
<p>A few popular automator plugins in WordPress are the following:</p>
<h4 class="title-logo"><img src="{{ page.base }}images/help/automate-webhook-services/automatorwp.svg" width="200" height="200" alt="Logo AutomatorWP" loading="lazy" />AutomatorWP</h4>
<p>You can simply install the AutomatorWP plugin to your WP Admin. Webhooks are usable via an add-on. <a href="https://automatorwp.com/" target="_blank" rel="noopener noreferrer">Click here for more information about AutomatorWP</a>.</p>
<h4 class="title-logo"><img src="{{ page.base }}images/help/automate-webhook-services/uncanny-automator.svg" width="200" height="200" alt="Logo Uncanny Automator" loading="lazy" />Uncanny Automator</h4>
<p>You can simply install the Uncanny Automator plugin to your WP Admin. Webhooks are available in the Pro plan. <a href="https://automatorplugin.com/" target="_blank" rel="noopener noreferrer">Click here for more information about Uncanny Automator</a>.</p>
<blockquote>
  <h4>💡 Tip: Choose your automator plugin wisely</h4>
  <p>AutomatorWP and Uncanny Automator are some examples, but there are also other automator plugin that you can use. It depends on your own needs, wishes and budget which plugin suits you best. That's why we advise to have a close look at the features and pricings before you select a certain automator plugin.</p>
</blockquote>
<h4>In your automator plugin</h4>
<p>In the automator plugin of your choosing, create a webhook as a trigger. That will result in a webhook URL that you can copy to your clipboard so you can use that in step 2.</p>

<h3 id="step-2">Step 2 - Connect webhook</h3>
<p>After you have gathered your webhook URL, you can connect that with Tripetto.</p>
<h4>In Tripetto:</h4>
<p>Switch to your form in Tripetto. At the top menu bar of the form builder click <code><i class="fas fa-share-alt"></i> Automate</code><i class="fas fa-arrow-right"></i> Click <code>Connections</code>. The Connections pane will show up on the right side of the form builder.</p>
<p>The fourth option in this screen is <code>Custom webhook</code>. After enabling the feature <code>Submit completed forms to a custom webhook</code> you can paste your webhook URL.</p>
<p>Important: leave the option <code>Send raw response data to webhook</code> disabled. Don't select that option, as it's for experts only.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tripetto.png" width="1200" height="760" alt="Screenshot of the webhook in Tripetto" loading="lazy" />
  <figcaption>Copy-paste your webhook URL in Tripetto.</figcaption>
</figure>

<h3 id="step-3">Step 3 - Receive response data</h3>
<p>Next step is to test if your automator plugin is indeed receiving the response data from your Tripetto form.</p>
<h4>In your automator plugin:</h4>
<p>It depends on your automator plugin how that's done exactly. In most cases, you have to temporarily open your webhook connection to be able to receive a test response. If your webhook is opened, you can submit a test entry.</p>
<h4>In Tripetto:</h4>
<p>Now, switch back to Tripetto and submit some response data. You can do that by simply clicking the <code>Test</code> button. That will send a test response from your form to your automator plugin with some dummy content. Or you can also simply submit a real form entry yourself via the shareable link of your form.</p>
<h4>In your automator plugin:</h4>
<p>If the connection is working, you will now see the response data from your form. Great, you're connected!</p>

<h3 id="step-4">Step 4 - Add WordPress actions</h3>
<p>Now it's time to perform the WordPress actions you want to automate.</p>
<h4>In your automator plugin:</h4>
<p>It depends on your automator plugin and the action(s) you want to perform how that process works exactly. The overall idea is you can add follow-up actions and use the form data from Tripetto in those actions. For example use an email address from the form submission to create a new user in WordPress.</p>

<h3 id="step-5">Step 5 - Activate magic</h3>
<p>Last step is to activate the webhook, so it will receive new form entries.</p>
<h4>In your automator plugin:</h4>
<p>If you have tested your connection, you can activate your automation process. Again, it depends on your automator plugin and the action(s) you're performing how you can activate your automation process.</p>
<p>From now on, each completed submission of this Tripetto form in your WordPress will send its data to your webhook and the webhook will execute the magic.</p>

</div>
