---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-track-form-activity-with-facebook-pixel/
title: Track form activity with Facebook Pixel - Tripetto Help Center
description: Track the activity of your respondents inside your forms by connecting with Facebook Pixel.
article_title: How to track form activity with Facebook Pixel
article_id: tracking
article_folder: automate-tracking-facebook-pixel
article_video: automations-tracking-facebook-pixel
author: jurgen
time: 5
time_video: 2
category_id: automations
subcategory: automations_tracking_tools
areas: [studio, wordpress]
---
<p>Track the activity of your respondents inside your forms by connecting with Facebook Pixel.</p>

<h2 id="when-to-use">When to use</h2>
<p>By tracking the activity of respondents in your forms, you can get insight information of their behaviors. This helps you to analyze form starts, form completions, interactions and drop-offs.</p>
<p>Facebook offers you the tools to do this with their <strong>Facebook Pixel</strong>. Follow the instructions in this article if you want to use Facebook Pixel to receive and analyze your form activity.</p>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>In this article we will show the steps you have to take to send Tripetto activity data to Facebook. You can also have a look at our <a href="{{ page.base }}help/articles/how-to-automate-form-activity-tracking/">global article about how form activity tracking works</a>.</p>
<blockquote>
  <h4>🚧 Warning: Facebook Pixel knowlegde needed</h4>
  <p>We assume you know how to configure and use Facebook Pixel. Tripetto support can not help you with configuring your Facebook Business account and/or Facebook Pixel.Follow the Developer Docs by Meta/Facebook for more information:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="https://developers.facebook.com/docs/meta-pixel" target="_blank" rel="noopener noreferrer">Developer Docs by Meta/Facebook</a></li>
  </ul>
</blockquote>

<h3 id="prepare">Step 1 - Prepare Facebook Pixel</h3>
<p>First step is to get the unique ID of your Facebook Pixel that you will need to connect Tripetto with Facebook.</p>
<h4>In Facebook:</h4>
<p>First make sure you have created and configured your Facebook Pixel like you want to. It's important that your Facebook Pixel is able to access the domain that the Tripetto form is served from.</p>
<p class="help_article_wp_only">In your WordPress site this will be your own domain name in most cases, because your form will run on your own site's domain.</p>
<div class="help_article_studio_only">
  <p>If you're using the Tripetto studio this configuration depends on how you share your form:</p>
  <ul>
    <li>Shareable tripetto.app link: if you share your form via the shareable tripetto.app link, you will have to make sure that your Facebook Pixel can receive events from <code>tripetto.app</code>;</li>
    <li>Embed in your site: if you have embedded your form in a site with the inline embed code, you will have to make sure that your Facebook Pixel can receive events from the domain of that particular site;</li>
    <li>Iframe in your site: if you have embedded your form in a site via an iframe with a <code>src</code> parameter, you will have to make sure that your Facebook Pixel can receive events from the domain of that <code>src</code> parameter. Please notice there can be more additional settings needed to be able to track form activity from an iframe.</li>
  </ul>
</div>

<h3 id="connect">Step 2 - Connect Facebook Pixel</h3>
<p>Next step is to connect your Tripetto form to the Facebook Pixel.</p>

<h4>In Facebook:</h4>
<p>Simply open the <code>Settings</code> and copy the <code>Pixel ID</code> that you see over there. You will need that ID later on in Tripetto.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-facebook-pixel.png" width="1300" height="760" alt="Screenshot of Facebook Pixel settings" loading="lazy" />
  <figcaption>Get the <code>Pixel ID</code> from Facebook Pixel settings.</figcaption>
</figure>

<h4>In Tripetto:</h4>
<p>Switch to your form in Tripetto. At the top menu bar of the form builder click <code><i class="fas fa-share-alt"></i> Automate</code><i class="fas fa-arrow-right"></i> Click <code>Tracking</code>. The Tracking pane will show up on the right side of the form builder.</p>
<p>In there, enable the feature <code>Track form activity with Facebook Pixel</code> to expand the settings for Facebook Pixel.</p>
<p>Before you enter your pixel ID, first ask yourself if the page that your Tripetto form is hosted at already has the Facebook Pixel tag installed to it:</p>
<ul>
  <li>If you already installed the Facebook Pixel tag, you can simply enable the option <code>Facebook Pixel is already installed on my website</code>. If enabled, you don't have to enter your pixel ID anymore and the Tripetto events will be sent to the pixel ID that you have installed yourself.<sup>1</sup></li>
  <li>If you haven't already installed the Facebook Pixel tag, you can paste the pixel ID that you just copied from Facebook Pixel.</li>
</ul>
<blockquote>
  <h4>🔖 Ad 1: Using the shareable link?</h4>
  <p>If you're (also) using the shareable link to your form, make sure you always supply your pixel ID to be able to track events in the shareable link version of your form.</p>
</blockquote>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-tripetto-id.png" width="630" height="273" alt="Screenshot of tracking in Tripetto" loading="lazy" />
  <figcaption>Enter your Facebook Pixel ID.</figcaption>
</figure>

<h3 id="events">Step 3 - Select tracking events</h3>
<p>Now your Tripetto form is connected to your Facebook Pixel. By default Tripetto will track the most important activities (form starting and form completion), but you can control which activities Tripetto needs to track for you.</p>
<h4>In Tripetto:</h4>
<p>Select the tracking events that you want to receive insights for in your Facebook Pixel.</p>
<p>Tripetto forms can record and share the following events:</p>
<ul>
  <li><code>Track form starting (<i>tripetto_start</i>)</code> - Tracks when a form is started;</li>
  <li><code>Track form completion (<i>tripetto_complete</i>)</code> - Tracks when a form is completed;</li>
  <li><code>Track staged blocks (<i>tripetto_stage</i>)</code> - Tracks when a block becomes available.<br/>It depends on your selected <a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/">form face</a> when this event will be fired:
    <ul>
      <li>Autoscroll form face: this event will be fired when a block gets activated;</li>
      <li>Chat form face: this event fires when a block becomes answerable;</li>
      <li>Classic form face: this even fires when the block becomes visible.</li>
    </ul>
  </li>
  <li><code>Track unstaged blocks (<i>tripetto_unstage</i>)</code> - Tracks when a block becomes unavailable.<br/>It depends on your selected <a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/">form face</a> when this event will be fired:
    <ul>
      <li>Autoscroll form face: this event will be fired when a block gets deactivated;</li>
      <li>Chat form face: this event fires when a block becomes unanswerable;</li>
      <li>Classic form face: this even fires when the block becomes invisible.</li>
    </ul>
  </li>
  <li><code>Track focus (<i>tripetto_focus</i>)</code> - Tracks when an input element gains focus;</li>
  <li><code>Track blur (<i>tripetto_blur</i>)</code> - Tracks when an input element loses focus;</li>
  <li><code>Track form pausing (<i>tripetto_pause</i>)</code> - Tracks when a form is paused.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/03-tripetto-events.png" width="622" height="349" alt="Screenshot of tracking in Tripetto" loading="lazy" />
  <figcaption>Select the tracking events to track with Facebook Pixel.</figcaption>
</figure>
<blockquote class="help_article_studio_only">
  <h4>🚧 Warning: Update embed code (studio only)</h4>
  <p>If you have embedded your form in your website/app, please make sure you always update your embed code over there.</p>
</blockquote>

<h3 id="receive">Step 4 - Receive tracking data</h3>
<p>Facebook is now ready to receive the selected tracking data from your Tripetto form. Let's test this.</p>

<h4>In Tripetto:</h4>
<p>Open your Tripetto form and submit an entry to it. The best way to do so, is by just opening your form and perform the actions you want to track yourself.</p>
<h4>In Facebook:</h4>
<p>Now switch back to Facebook and have a look at your Facebook Pixel to see if the data is shown.</p>
<blockquote>
  <h4>📣 Info: Delay in Facebook Pixel</h4>
  <p>Please notice that although Tripetto sends all events in realtime, it can take some time before Facebook Pixel shows the events from your Tripetto form.</p>
</blockquote>
<p>From there on you can follow the instructions of Facebook Pixel to configure the desired analysis. For help with this, please have a look at the <a href="https://developers.facebook.com/docs/meta-pixel" target="_blank" rel="noopener noreferrer">Developer Docs by Meta/Facebook</a>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-facebook-pixel-events.png" width="1200" height="760" alt="Screenshot of Facebook Pixel" loading="lazy" />
  <figcaption>Example of the events in Facebook Business.</figcaption>
</figure>
<blockquote>
  <h4>📌 Also see: Tracking data analysis</h4>
  <p>We have some help articles about the most common use cases for tracking data analysis:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-measure-completion-rates-with-form-activity-tracking/">How to measure completion rates with form activity tracking</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-measure-drop-offs-with-form-activity-tracking/">How to measure drop-offs with form activity tracking</a></li>
  </ul>
</blockquote>

<h2 id="troubleshooting">Troubleshooting</h2>
<p>Are your events not visible in your Facebook Pixel dashboard? There can be a few reasons:
<ul>
  <li>Please notice that although Tripetto sends all events in realtime, it can take some time before Facebook Pixel shows the events from your Tripetto form.</li>
  <li>Next step is to check if the events are triggered in the form. To test this Facebook offers an extension in popular browsers to help you validate your pixel implementation, called <strong>Facebook Pixel Helper</strong>. That gives instant insights on the events that are measured. You can install this extension right in your browser, for example Google Chrome.
    <ul>
      <li>If the events are not visible in the Facebook Pixel Helper, please check if your event tracking is configured correctly in Tripetto.</li>
      <li>If the events are visible in the Facebook Pixel Helper, but not in your Facebook Pixel dashboard, please check if your Facebook Pixel is configured correctly. For example check if the right domain has been entered.</li>
    </ul>
  </li>
</ul>
