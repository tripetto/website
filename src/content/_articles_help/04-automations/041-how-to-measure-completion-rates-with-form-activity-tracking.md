---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-measure-completion-rates-with-form-activity-tracking/
title: Measure completion rates - Tripetto Help Center
description: By using form activity tracking you can measure the completion rates. Let's dive a little deeper into using tracking data for this kind of analysis.
article_id: tracking-completions
article_title: How to measure completion rates with form activity tracking
article_folder: automate-tracking
author: jurgen
time: 3
category_id: automations
subcategory: automations_tracking_examples
areas: [studio, wordpress]
---
<p>By using form activity tracking you can measure the completion rates. Let's dive a little deeper into using tracking data for this kind of analysis.</p>

<h2 id="when-to-use">When to use</h2>
<p>You can calculate the completion rates to analyze how often your form gets started and how many of those starts are actually completed.</p>
<p>To do this, Tripetto can track form activity of your respondents in your forms and send that data to <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-google-analytics/">Google Analytics</a>, <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-google-tag-manager/">Google Tag Manager</a>, <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-facebook-pixel/">Facebook Pixel</a> or even a <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-custom-tracking-code/">custom tracking code</a>. Let's see how to set this up and calculate the completion rate.
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>For this article we have connected our Tripetto form with <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-google-analytics/">Google Analytics</a> (GA4) and we'll use that to take a closer look at the analysis.</p>
<h3 id="track">Events to track</h3>
<p>You can activate the desired events in the tracking settings in Tripetto (<code><i class="fas fa-share-alt"></i> Automate</code><i class="fas fa-arrow-right"></i><code>Tracking</code>). To calculate completion rates you need to track the following two events:</p>
<ul>
  <li><code>Track form starting (<i>tripetto_start</i>)</code> - Tracks when a form is started;</li>
  <li><code>Track form completion (<i>tripetto_complete</i>)</code> - Tracks when a form is completed.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/automate-tracking-analysis/tripetto-completion-rates.png" width="632" height="493" alt="Screenshot of tracking in Tripetto" loading="lazy" />
  <figcaption>Connect your Tripetto form with Google Analytics and activate the desired events.</figcaption>
</figure>
<h3 id="events">Events in Google Analytics</h3>
<p>If you now test your form, you will see that Google Analytics receives the tracking data. In Google Analytics the tracking data from Tripetto will be received in the section <code>Engagement > Events</code>. And you can see the recent events appear in the <code>Real-time</code> section (with a little delay).</p>
<blockquote>
  <h4>📣 Info: Delay in Google Analytics</h4>
  <p>Please notice that although Tripetto sends all events in realtime, it can take some time (up to 24 hours) before Google Analytics shows the events from your Tripetto form.</p>
</blockquote>
<figure>
  <img src="{{ page.base }}images/help/automate-tracking-analysis/start.png" width="1200" height="760" alt="Screenshot of tripetto_start event in Google Analytics" loading="lazy" />
  <figcaption>Tracking data of <code>tripetto_start</code>: 5 forms have been started.</figcaption>
</figure>
<figure>
  <img src="{{ page.base }}images/help/automate-tracking-analysis/complete.png" width="1200" height="760" alt="Screenshot of tripetto_complete event in Google Analytics" loading="lazy" />
  <figcaption>Tracking data of <code>tripetto_complete</code>: 2 forms have been completed.</figcaption>
</figure>
<h3 id="analysis">Analysis</h3>
<p>In our example you can see our form has been started 5 times and completed 2 times. With that data we can now calculate our completion rate like this: <br/><strong>2 out of 5 forms completed = 40% completion rate</strong>.</p>
