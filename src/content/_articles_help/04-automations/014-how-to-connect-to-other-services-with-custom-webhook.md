---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-connect-to-other-services-with-custom-webhook/
title: Connect to other services with custom webhook - Tripetto Help Center
description: You can connect your response data to any webhook that can receive data. This article describes how to enable a custom webhook connection.
article_title: How to connect to other services with custom webhook
article_id: webhook
article_folder: automate-webhook-custom
article_video: automations-webhook
author: jurgen
time: 4
time_video: 2
category_id: automations
subcategory: automations_webhook_tools
areas: [studio, wordpress]
---
<p>You can connect your response data to any webhook that can receive data and with that configure automatic follow-up actions.</p>

<h2 id="when-to-use">When to use</h2>
<p>By connecting Tripetto to other services you can do all kinds of actions with your response data, like pushing it to a spreadsheet editor (Microsoft Excel/Google Sheets) or a database, or trigger other follow-up actions. The possibilities are endless!</p>
<p>To easily do this you can use automation tools that will help you to connect to other services. We offer easy-to-use connections with the most used automation tools <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-make/">Make</a>, <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-zapier/">Zapier</a> and <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-pabbly-connect/">Pabbly Connect</a>.</p>
<p>But if you want to connect with a different automation tool, or even with your own webhook endpoint, that's also possible! Follow the instructions in this article to connect your Tripetto response data with other automation tools/custom webhooks.</p>
<blockquote class="help_article_wp_only">
  <h4>📌 Also see: Perform WordPress actions (WordPress only)</h4>
  <p>The above examples are all external online services, but if you're using Tripetto inside WordPress, you can also connect your Tripetto forms to WordPress specific actions and other WordPress plugins, without your data leaving your WordPress environment. Have a look at this article for more information about WordPress automator plugins:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automate-actions-inside-your-wordpress-site-using-automator-plugins/">How to automate actions inside your WordPress site using automator plugins</a></li>
  </ul>
</blockquote>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>Setting up a webhook usually takes a few steps, which we will describe globally in this article. It depends on your automation tool/endpoint how to perform these steps exactly.</p>
<blockquote>
  <h4>🚧 Warning: Automation tool/webhook knowlegde needed</h4>
  <p>We assume you know how to configure and use your desired automation tool and/or webhook. Tripetto support can not help you with configuring this and/or the services that you want to connect your Tripetto form to.</p>
</blockquote>

<h3 id="step-1">Step 1 - Prepare webhook</h3>
<p>First step is to make sure you have a webhook that can receive data from Tripetto.</p>
<h4>In your automation tool/endpoint:</h4>
<p>It depends on your automation tool/endpoint how you set this up. The outcome of this step should be a webhook URL that you can use in Tripetto that gets triggered for each new form response.</p>
<blockquote>
  <h4>💡 Tip: Choose your automation tool wisely</h4>
  <p>There are lots of automation tools that you can use. It depends on your own needs, wishes and budget which tool fits you best. That's why we advise to have a close look at the features and pricings before you select a certain automation tool.</p>
</blockquote>

<h3 id="step-2">Step 2 - Connect webhook</h3>
<p>After you have gathered your webhook URL, you can connect that with Tripetto.</p>
<h4>In Tripetto:</h4>
<p>Switch to your form in Tripetto. At the top menu bar of the form builder click <code><i class="fas fa-share-alt"></i> Automate</code><i class="fas fa-arrow-right"></i> Click <code>Connections</code>. The Connections pane will show up on the right side of the form builder.</p>
<p>The fourth option in this screen is <code>Custom webhook</code>. After enabling the feature <code>Submit completed forms to a custom webhook</code> you can paste your webhook URL.</p>
<p>Important: leave the option <code>Send raw response data to webhook</code> disabled. Don't select that option, as it's for experts only.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tripetto.png" width="1200" height="760" alt="Screenshot of the webhook in Tripetto" loading="lazy" />
  <figcaption>Copy-paste your webhook URL in Tripetto.</figcaption>
</figure>
<blockquote class="help_article_wp_only">
  <h4>📌 Also see: Using file uploads (WordPress)</h4>
  <p>If you have a <a href="{{ page.base }}help/articles/how-to-use-the-file-upload-block/">File Upload block</a> or a <a href="{{ page.base }}help/articles/how-to-use-the-signature-block/">Signature block</a> in your form in WordPress, you can download the files from your respondents with a download link. By default such download links are only available to the owner of the form. You have to be logged in to the corresponding WordPress site (WP Admin) to be able to download the files. If you want to make the download links available for Make you can enable the setting <code>Allow access to uploaded files</code>.</p>
  <p>For more instructions have a look at this help article about using file uploads from the WordPress plugin in webhooks:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/use-file-uploads-from-the-wordpress-plugin-in-webhooks/">Use file uploads from the WordPress plugin in webhooks</a></li>
  </ul>
</blockquote>
<blockquote class="help_article_studio_only">
  <h4>📌 Also see: Using file uploads (studio)</h4>
  <p>If you have a <a href="{{ page.base }}help/articles/how-to-use-the-file-upload-block/">File Upload block</a> or a <a href="{{ page.base }}help/articles/how-to-use-the-signature-block/">Signature block</a> in your form in the studio, you can download the files from your respondents with a download link. In general such download links are only available to the owner of the form. You have to be logged in to the corresponding Tripetto studio account to be able to download the files. Only for the first 24 hours after form submissions, those download links will be available to Make.</p>
  <p>For more instructions have a look at this help article about using file uploads from the studio in webhooks:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/use-file-uploads-from-the-studio-in-webhooks/">Use file uploads from the studio in webhooks</a></li>
  </ul>
</blockquote>

<h3 id="step-3">Step 3 - Receive response data</h3>
<p>Next step is to test if your automation tool is indeed receiving the response data from your Tripetto form.</p>
<h4>In your automation tool/endpoint:</h4>
<p>It depends on your automation tool how that's done exactly. In most cases, you have to temporarily open your webhook connection to be able to receive a test response. If your webhook is opened, you can submit a test entry.</p>
<h4>In Tripetto:</h4>
<p>Now, switch back to Tripetto and submit some response data. You can do that by simply clicking the <code>Test</code> button. That will send a test response from your form to your automation tool/endpoint with some dummy content. Or you can also simply submit a real form entry yourself via the shareable link of your form.</p>
<h4>In your automation tool/endpoint:</h4>
<p>If the connection is working, you will now see the response data from your form. Great, you're connected!</p>

<h3 id="step-4">Step 4 - Add services</h3>
<p>Now it's time to connect the services you want.</p>
<h4>In your automation tool/endpoint:</h4>
<p>It depends on your automation tool and the service(s) you're connecting to how that process works exactly. The overall idea is you can add follow-up actions and use the form data from Tripetto in those other services.</p>

<h3 id="step-5">Step 5 - Activate magic</h3>
<p>Last step is to activate the webhook, so it will receive new form entries.</p>
<h4>In your automation tool/endpoint:</h4>
<p>If you have tested your connection, you can activate your automation process. Again, it depends on your automation tool and the service(s) you're connecting to how you can activate your automation process.</p>
<p>From now on, each completed submission of this Tripetto form will send its data to your webhook and the webhook will execute the magic into other services.</p>
