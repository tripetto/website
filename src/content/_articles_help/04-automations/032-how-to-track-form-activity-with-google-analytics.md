---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-track-form-activity-with-google-analytics/
title: Track form activity with Google Analytics - Tripetto Help Center
description: Track the activity of your respondents inside your forms by connecting with Google Analytics.
article_title: How to track form activity with Google Analytics
article_id: tracking
article_folder: automate-tracking-google-analytics
article_video: automations-tracking-google-analytics
author: jurgen
time: 5
time_video: 2
category_id: automations
subcategory: automations_tracking_tools
areas: [studio, wordpress]
---
<p>Track the activity of your respondents inside your forms by connecting with Google Analytics.</p>

<h2 id="when-to-use">When to use</h2>
<p>By tracking the activity of respondents in your forms, you can get insight information of their behaviors. This helps you to analyze form starts, form completions, interactions and drop-offs.</p>
<p>Google offers you the tools to do this with their <strong>Google Analytics</strong>. Follow the instructions in this article if you want to use Google Analytics to receive and analyze your form activity.</p>
<blockquote>
  <h4>🔔 Notice: Google Analytics versions (UA vs. GA4)</h4>
  <p>Since July 2023, Universal Analytics (UA) stopped processing new data (<a href="https://support.google.com/analytics/answer/11583528" target="_blank" rel="noopener noreferrer">more info</a>). Therefore this article uses the new standard Google Analytics 4 (GA4) property to demonstrate the setup for form activity tracking with Google Analytics.</p>
</blockquote>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>In this article we will show the steps you have to take to send Tripetto activity data to Google Analytics. You can also have a look at our <a href="{{ page.base }}help/articles/how-to-automate-form-activity-tracking/">global article about how form activity tracking works</a>.</p>
<blockquote>
  <h4>🚧 Warning: Google Analytics knowlegde needed</h4>
  <p>We assume you know how to configure and use Google Analytics. Tripetto support can not help you with configuring your Google Analytics account. Follow the Analytics Help by Google for more information:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="https://support.google.com/analytics/" target="_blank" rel="noopener noreferrer">Analytics Help by Google</a></li>
  </ul>
</blockquote>

<h3 id="prepare">Step 1 - Prepare Google Analytics</h3>
<p>First step is to get your unique ID from Google Analytics that you will need to connect Tripetto with Google Analytics.</p>
<h4>In Google Analytics:</h4>
<p>First make sure you have created and configured your property in Google Analytics like you want to. It's important that your Google Analytics property is able to access the domain that the Tripetto form is served from. You can do this in Google Analytics (GA4) via <code>Settings</code> <i class="fas fa-arrow-right"></i> <code>Data streams</code>.</p>
<p class="help_article_wp_only">In your WordPress site this will be your own domain in most cases, because your form will run on your own site's domain.</p>
<div class="help_article_studio_only">
  <p>If you're using the Tripetto studio this configuration depends on how you share your form:</p>
  <ul>
    <li>Shareable tripetto.app link: if you share your form via the shareable tripetto.app link, you will have to make sure that Google Analytics can receive events from <code>tripetto.app</code>;</li>
    <li>Embed in your site: if you have embedded your form in a site with the inline embed code, you will have to make sure that Google Analytics can receive events from the domain of that particular site;</li>
    <li>Iframe in your site: if you have embedded your form in a site via an iframe with a <code>src</code> parameter, you will have to make sure that Google Analytics can receive events from the domain of that <code>src</code> parameter. Please notice there can be more additional settings needed to be able to track form activity from an iframe.</li>
  </ul>
</div>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-google-analytics.png" width="1200" height="760" alt="Screenshot of Google Analytics" loading="lazy" />
  <figcaption>Example of a Data stream in Google Analytics (GA4).</figcaption>
</figure>

<h3 id="connect">Step 2 - Connect Google Analytics</h3>
<p>Next step is to connect your Tripetto form to Google Analytics.</p>

<h4>In Google Analytics:</h4>
<p>From the Data stream screen in Google Analytics, simply copy the <code>Measurement ID</code> of your data stream. You will need that ID later on in Tripetto.</p>

<h4>In Tripetto:</h4>
<p>Switch to your form in Tripetto. At the top menu bar of the form builder click <code><i class="fas fa-share-alt"></i> Automate</code><i class="fas fa-arrow-right"></i> Click <code>Tracking</code>. The Tracking pane will show up on the right side of the form builder.</p>
<p>In there, enable the feature <code>Track form activity with Google Analytics or Google Tag Manager</code> to expand the settings for Google Analytics.</p>
<p>Before you enter your measurement ID, first ask yourself if the page that your Tripetto form is hosted at already has the Google Analytics tag installed to it:</p>
<ul>
  <li>If you already installed the Google Analytics tag, you can simply enable the option <code>Google Analytics or Google Tag Manager is already installed on my website</code>. If enabled, you don't have to enter your measurement ID anymore and the Tripetto events will be sent to the measurement ID that you have installed yourself.<sup>1</sup></li>
  <li>If you haven't already installed the Google Analytics tag, you can paste the measurement ID that you just copied from Google Analytics. Tripetto will recognize automatically that you're using Google Analytics, based on the measurement ID that you supply.</li>
</ul>
<blockquote>
  <h4>🔖 Ad 1: Using the shareable link?</h4>
  <p>If you're (also) using the shareable link to your form, make sure you always supply your measurement ID to be able to track events in the shareable link version of your form.</p>
</blockquote>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-tripetto-id.png" width="636" height="339" alt="Screenshot of tracking in Tripetto" loading="lazy" />
  <figcaption>Enter your Google Analytics ID.</figcaption>
</figure>

<h3 id="events">Step 3 - Select tracking events</h3>
<p>Now your Tripetto form is connected to your Google Analytics property. By default Tripetto will track the most important activities (form starting and form completion), but you can control which activities Tripetto needs to track for you.</p>
<h4>In Tripetto:</h4>
<p>Select the tracking events that you want to receive insights for in your Google Analytics property.</p>
<p>Tripetto forms can record and share the following events:</p>
<ul>
  <li><code>Track form starting (<i>tripetto_start</i>)</code> - Tracks when a form is started;</li>
  <li><code>Track form completion (<i>tripetto_complete</i>)</code> - Tracks when a form is completed;</li>
  <li><code>Track staged blocks (<i>tripetto_stage</i>)</code> - Tracks when a block becomes available.<br/>It depends on your selected <a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/">form face</a> when this event will be fired:
    <ul>
      <li>Autoscroll form face: this event will be fired when a block gets activated;</li>
      <li>Chat form face: this event fires when a block becomes answerable;</li>
      <li>Classic form face: this even fires when the block becomes visible.</li>
    </ul>
  </li>
  <li><code>Track unstaged blocks (<i>tripetto_unstage</i>)</code> - Tracks when a block becomes unavailable.<br/>It depends on your selected <a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/">form face</a> when this event will be fired:
    <ul>
      <li>Autoscroll form face: this event will be fired when a block gets deactivated;</li>
      <li>Chat form face: this event fires when a block becomes unanswerable;</li>
      <li>Classic form face: this even fires when the block becomes invisible.</li>
    </ul>
  </li>
  <li><code>Track focus (<i>tripetto_focus</i>)</code> - Tracks when an input element gains focus;</li>
  <li><code>Track blur (<i>tripetto_blur</i>)</code> - Tracks when an input element loses focus;</li>
  <li><code>Track form pausing (<i>tripetto_pause</i>)</code> - Tracks when a form is paused.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/03-tripetto-events.png" width="622" height="349" alt="Screenshot of tracking in Tripetto" loading="lazy" />
  <figcaption>Select the tracking events to track with Google Analytics.</figcaption>
</figure>
<blockquote class="help_article_studio_only">
  <h4>🚧 Warning: Update embed code (studio only)</h4>
  <p>If you have embedded your form in your website/app, please make sure you always update your embed code over there.</p>
</blockquote>

<h3 id="receive">Step 4 - Receive tracking data</h3>
<p>Google Analytics is now ready to receive the selected tracking data from your Tripetto form. Let's test this.</p>

<h4>In Tripetto:</h4>
<p>Open your Tripetto form and submit an entry to it. The best way to do so, is by just opening your form and perform the actions you want to track yourself.</p>
<h4>In Google Analytics:</h4>
<p>Now switch back to Google Analytics and have a look at the <code>Events</code> section to see if the data is shown.</p>
<blockquote>
  <h4>📣 Info: Delay in Google Analytics</h4>
  <p>Please notice that although Tripetto sends all events in realtime, it can take some time (up to 24 hours) before Google Analytics shows the events from your Tripetto form.</p>
</blockquote>
<p>From there on you can use the tools of Google Analytics to perform the desired analysis. For help with this, please have a look at the <a href="https://support.google.com/analytics/" target="_blank" rel="noopener noreferrer">Analytics Help by Google</a>.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/01-google-analytics-events.png" width="1200" height="760" alt="Screenshot of Google Analytics" loading="lazy" />
  <figcaption>Example of the events in Google Analytics.</figcaption>
</figure>
<blockquote>
  <h4>📌 Also see: Tracking data analysis</h4>
  <p>We have some help articles about the most common use cases for tracking data analysis:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-measure-completion-rates-with-form-activity-tracking/">How to measure completion rates with form activity tracking</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-measure-drop-offs-with-form-activity-tracking/">How to measure drop-offs with form activity tracking</a></li>
  </ul>
</blockquote>

<h2 id="troubleshooting">Troubleshooting</h2>
<p>Are your events not visible in your Google Analytics dashboard? There can be a few reasons:
<ul>
  <li>Please notice that although Tripetto sends all events in realtime, it can take some time (up to 24 hours) before Google Analytics shows the events from your Tripetto form.</li>
  <li>Next step is to check if the events are triggered in the form. To test this Google offers an extension in popular browsers to help you validate your tracking implementation, called <strong>Google Analytics Debugger</strong>. That gives instant insights on the events that are measured. You can install this extension right in your browser, for example Google Chrome.
    <ul>
      <li>If the events are not visible in the Google Analytics Debugger, please check if your event tracking is configured correctly in Tripetto.</li>
      <li>If the events are visible in the Google Analytics Debugger, but not in your Google Analytics dashboard, please check if your Google Analytics is configured correctly. For example check if the right domain has been entered.</li>
    </ul>
  </li>
  <li>Next step is to check if there can be any conflicts with other Google script tags in your site, for example your global Google Analytics tag and/or Google Adwords tag. To check this, please temporarily remove all other Google script tags from your page where the Tripetto form is at. And then test if the events are triggered and sent correctly.</li>
  <li>To prevent such conflicts, you can enable the setting <code>Google Analytics or Google Tag Manager is already installed on my website</code>. If enabled, Tripetto will not add another Google Analytics tag to your page, which will prevent any conflicts.</li>
</ul>
