---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-automatically-store-form-responses-in-google-sheets/
title: Automatically store responses in Google Sheets - Tripetto Help Center
description: Connect your Tripetto form to Google Sheets to automatically store the form response data in a Google Sheets file and generate analysis reports.
article_id: connection-google-sheets
article_title: How to automatically store form responses in Google Sheets and generate analysis reports
article_folder: automate-webhook-sheets
author: jurgen
time: 4
category_id: automations
subcategory: automations_webhook_examples
areas: [studio, wordpress]
---
<p>Connect your Tripetto form to Google Sheets to automatically store the form response data in a Google Sheets file and use that data to generate valuable analysis reports. For this example we use Make as our automation tool.</p>

<h2 id="when-to-use">When to use</h2>
<p>You can use this automation if you want to collect all data in your own Google Sheets file automatically. And once the data is in your Google Sheets file you can do all kinds of analysis with it.</p>
<blockquote>
  <h4>📌 Also see: Google Sheets documentation</h4>
  <p>This article will demonstrate how to get your Tripetto form data in Google Sheets, but it's not a guide to build analysis reports in Google Sheets. Follow the Google Sheets documentation for more information:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="https://support.google.com/docs/topic/9054603?hl=en-GB&ref_topic=1382883" target="_blank" rel="noopener noreferrer">Google Sheets documentation</a></li>
  </ul>
</blockquote>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/google-sheets-analysis.png" width="1200" height="760" alt="Screenshot of a data report in Google Sheets" loading="lazy" />
  <figcaption>Collect and use form data in Google Sheets, for example to generate data analysis reports.</figcaption>
</figure>
<hr/>

<h2 id="what-you-need">What you need</h2>
<p>The scenario we describe in this article is an example of connecting Tripetto to other services, in this case Google Sheets. <a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/">Click here for more general information about connecting Tripetto to other services.</a></p>
<p>For this scenario you need the following:</p>
<ul>
  <li><strong>Tripetto</strong>, to collect your form submissions;</li>
  <li><strong>Make</strong><sup>1</sup>, to connect your form data to other services;</li>
  <li><strong>Google Sheets</strong>, to store the form data and create analysis reports.</li>
</ul>
<blockquote>
  <h4>🔖 Ad 1: Make alternatives</h4>
  <p>There are alternatives to use as your automation tool. In our example we use Make as our automation tool, but you can also use Zapier, Pabbly Connect or even a custom webhook. Of course it's up to you to use your favorite automation tool.</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-connect-form-responses-to-automation-tools/">How to connect form responses to automation tools</a></li>
  </ul>
</blockquote>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>These are the steps to take to automate the process of storing form data in Google Sheets.</p>

<h3 id="step-1">Step 1 - Build Tripetto form</h3>
<ul>
  <li>First make sure your Tripetto form collects the desired data.</li>
</ul>
<p>The result of this step is your Tripetto form, which you will use to collect your data from your respondents.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tripetto-form.png" width="1200" height="760" alt="Screenshot of Tripetto" loading="lazy" />
  <figcaption>Build your Tripetto form.</figcaption>
</figure>

<h3 id="step-2">Step 2 - Prepare Google Sheets spreadsheet</h3>
<ul>
  <li>In your Google Sheets account create a new file/spreadsheet.</li>
  <li>Give your spreadsheet a name, for example <code>Tripetto Form Results</code>.</li>
  <li>In the first row add the column header names of the data fields you want to store in this spreadsheet.</li>
</ul>
<p>The result of this step is your Google Sheets spreadsheet with the desired column headers, that we will use later on in the process.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/google-sheets-template.png" width="1200" height="760" alt="Screenshot of Google Sheets" loading="lazy" />
  <figcaption>Prepare your Google Sheets spreadsheet, with the columns header in the first row.</figcaption>
</figure>

<h3 id="step-3">Step 3 - Connect Tripetto to Make</h3>
<ul>
  <li>In your Make account add a new scenario.</li>
  <li>Give your scenario a name, for example <code>Add new row to Google Sheets for new Tripetto submission</code>.</li>
  <li>Add a <code>Tripetto</code> module to your Make scenario. As trigger select <code>Watch Responses</code>.</li>
  <li>Connect your Tripetto form to that trigger, by copy-pasting the webhook URL from Make into your Tripetto form (navigate to <code><i class="fas fa-share-alt"></i> Automate</code><i class="fas fa-arrow-right"></i><code>Connections</code><i class="fas fa-arrow-right"></i><code>Make</code>). <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-make/">Have a look at this article for the detailed description how to connect Tripetto to Make.</a></li>
  <li>Test the connection, so Make receives the structure of your dataset.</li>
</ul>
<p>The result of this step is that your Make scenario gets executed for each new form submission and you can use the data fields from your Tripetto form in the Make scenario.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/integromat-tripetto.png" width="1200" height="760" alt="Screenshot of Tripetto module in Make" loading="lazy" />
  <figcaption>Add a Tripetto trigger in your Make scenario.</figcaption>
</figure>

<h3 id="step-4">Step 4 - Add new row to Google Sheets</h3>
<ul>
  <li>Add a <code>Google Sheets</code> module to your Make scenario. As action select <code>Add a Row</code>.</li>
  <li>Grant access to your Google account.</li>
  <li>Under <code>Spreadsheet</code> select your earlier created Google Sheets spreadsheet file.</li>
  <li>Under <code>Table contains headers</code> select <code>Yes</code>.</li>
  <li>Make will now recognize the column header names that you prepared earlier. For each column name you can now select the corresponding data field from your Tripetto form. This is the part where you map the entered data from your Tripetto form to the columns in your Google Sheets spreadsheet file.</li>
</ul>
<p>The result of this step is you now have automated sending the data from Tripetto to Google Sheets.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/integromat-google-sheets.png" width="1200" height="760" alt="Screenshot of Google Sheets module in Make" loading="lazy" />
  <figcaption>Add a Google Sheets module in your Make scenario to add a new row.</figcaption>
</figure>

<h3 id="step-5">Step 5 - Test and activate</h3>
<ul>
  <li>Click <code>Run once</code> in Make to start testing your automation.</li>
  <li>Fill out and submit your Tripetto form once.</li>
  <li>Check if your Make scenario has been processed correctly. If so, check the Google Sheets spreadsheet to see if the data is indeed added as a new row.</li>
  <li>Activate your scenario in Make.</li>
</ul>
<p>From now on this scenario will be executed automatically for each new form submission! Every new submission will now automatically be added to your Google Sheets spreadsheet 🎉</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/google-sheets-results.png" width="1200" height="760" alt="Screenshot of Google Sheets" loading="lazy" />
  <figcaption>The data of each form submission has been added as a new row in Google Sheets.</figcaption>
</figure>

<h3 id="step-6">Step 6 - Generate analysis reports (optional)</h3>
<ul>
  <li>Now that your data comes into your Google Sheets spreadsheet automatically, you can use that data to generate all kinds of analysis reports.</li>
  <li>It totally depends on your use case and wishes for the analysis how to set that up in Google Sheets. <a href="https://support.google.com/docs/topic/9054603?hl=en-GB&ref_topic=1382883" target="_blank" rel="noopener noreferrer">Click here for Google Sheets documentation.</a></li>
</ul>
<p>The result of this step will be you get valuable insights in your collected data.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/google-sheets-analysis.png" width="1200" height="760" alt="Screenshot of a data analysis report in Google Sheets" loading="lazy" />
  <figcaption>Example of a data analysis report with graphs.</figcaption>
</figure>

<hr />
<h2>Other examples</h2>
<p>Tripetto lets you connect to 1.000+ services. We have made some step-by-step examples for often used scenarios:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-store-form-responses-in-google-sheets/">How to automatically store form responses in Google Sheets and generate analysis reports</a><span class="related-current">Current article</span></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-store-form-responses-in-notion/">How to automatically store form responses in Notion</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-create-PDF-files-from-form-responses/">How to automatically create PDF files from form responses and send these to respondents</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-add-mailchimp-subscribers-from-form-responses/">How to automatically add Mailchimp subscribers from form responses</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-create-zendesk-support-tickets-from-form-responses/">How to automatically create Zendesk support tickets from form responses</a></li>
</ul>
