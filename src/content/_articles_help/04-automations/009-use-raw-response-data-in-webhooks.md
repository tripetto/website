---
layout: help-article
base: ../../../
permalink: /help/articles/use-raw-response-data-in-webhooks/
title: Use raw response data in webhooks - Tripetto Help Center
description: In addition to the custom webhook connection, you can use the raw response data in your webhook. This does require additional actions and is for experts only.
article_title: Use raw response data in webhooks (experts only)
article_folder: automate-webhook-raw
author: mark
time: 4
category_id: automations
subcategory: automations_webhook
areas: [studio, wordpress]
scripts: [prism]
stylesheets: [prism-dark]
---
<p>In addition to the custom webhook connection, you can use the raw response data in your webhook. This does require additional actions and is for experts only.</p>

<blockquote>
  <h4>🚧 Warning: Experts only!</h4>
  <p>The article below is for experts only. It does NOT describe how to use the standard, easy to use webhook connection. If you just want to send response data to an automation tool and/or a custom webhook, please have a look at this article:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/">How to automate a webhook to connect to other services for each new result</a></li>
  </ul>
</blockquote>

<h2 id="when-to-use">When to use</h2>
<p>You can easily connect Tripetto to automation tools and other services, as we described in <a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/">this article</a>. That standard connection sends the response data in such a way that automation tools can understand the structure of the data to easily use it in your connections.</p>
<p>We always advise to follow that basic workflow, but in some cases you need more form data than just the questions and answers. For those cases it's also possible to use the raw response data. This will send way more data to the webhook, but also in a more complex JSON structure. That's why this is for experts only!</p>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>To use the raw response data you need to do some modifications in Tripetto, but most importantly, you need to configure your automation tool differently.</p>
<blockquote>
  <h4>🚧 Warning: Automation tool/webhook knowlegde needed</h4>
  <p>We assume you know how to configure and use your desired automation tool and/or webhook. Tripetto support can not help you with configuring this and/or the services that you want to connect your Tripetto form to.</p>
</blockquote>

<h3 id="enable-raw">Enable raw response data</h3>
<p>Let's start in Tripetto. At the top menu bar of the form builder click <code><i class="fas fa-share-alt"></i> Automate</code><i class="fas fa-arrow-right"></i> Click <code>Connections</code>. The Connections pane will show up on the right side of the form builder.</p>
<p>The fourth option in this screen is <code>Custom webhook</code>. We assume you already activated that option and entered your custom webhook URL. Below that input field, you can now enable the option <code>Send raw response data to webhook</code>.</p>
<p>From now on the webhook will no longer receive simple <code>Name-Value-Pairs</code> from Tripetto, but the <code>plain JSON response</code> from Tripetto.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tripetto.png" width="1200" height="760" alt="Screenshot of the webhook in Tripetto" loading="lazy" />
  <figcaption>Enable the setting to send raw response data to your custom webhook.</figcaption>
</figure>

<h3 id="raw-hook">Catch raw webhook</h3>
<p>Now switch to your automation tool/custom webhook endpoint. Over there you need to make sure the automation tool handles the raw data the right way. In automation tools this is often a separate webhook event, for example <code>Catch raw webhook</code>.</p>

<h3 id="manipulate">Manipulate raw data</h3>
<p>Now, it depends on what you want to do with the raw data. In some cases you will need to manipulate the data to make it usable for your follow-up actions.</p>
<p>One example of manipulating the data is making sure you can use the given answers. To do so, you need to execute a code snippet in your automation tool, so the data becomes usable.</p>
<p>It depends on the automation tool you're using how you can do this. For example in Zapier, you can easily add a JavaScript block and then for example enter the following code snippet to that block:</p>
<pre class="line-numbers"><code class="language-javascript">output = {};

var input = inputData.tripettoResult;

if (input.indexOf("\"") === 0) {
  input = JSON.parse(`{"data":${input}}`).data;
}

var tripettoFields = JSON.parse(input).fields;

for (var nField = 0; nField < tripettoFields.length; nField ++) {
   output["tripettoField" + nField] = tripettoFields[nField].string;
}</code></pre>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/raw-zapier-script-update.png" width="1200" height="760" alt="Screenshot of script in Zapier" loading="lazy" />
  <figcaption>Example of a script block in Zapier.</figcaption>
</figure>

<h3 id="connect">Connect raw data to services</h3>
<p>Also the way you can connect the raw data to data fields in other services can be more difficult when you use the raw data. It depends on your automation tool and how you manipulate your raw response data how you can do that.</p>
<p>For example with the code snippet we added to Zapier in this article, your form structure gets available as JavaScript parameters that you can use in your follow-up service.</p>
