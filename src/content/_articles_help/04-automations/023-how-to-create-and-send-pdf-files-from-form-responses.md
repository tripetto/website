---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-automatically-create-PDF-files-from-form-responses/
title: Automatically create PDF files - Tripetto Help Center
description: Connect your Tripetto form to Google Docs and Gmail to automatically create personalized PDF files and send these to your respondents.
article_title: How to automatically create PDF files from form responses and send these to respondents
article_folder: automate-webhook-pdf
author: jurgen
time: 6
category_id: automations
subcategory: automations_webhook_examples
areas: [studio, wordpress]
---
<p>Connect your Tripetto form to Google Docs and Gmail to automatically create personalized PDF files and send these to your respondents. For this example we use Make as our automation tool.</p>

<h2 id="when-to-use">When to use</h2>
<p>You can use this automation if you want to email your respondents after they have completed your Tripetto form. You can attach a PDF file to that email message, for example to include a summary of the given answers.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/pdf-file.png" width="1200" height="760" alt="Screenshot of PDF file" loading="lazy" />
  <figcaption>Create and send your own personalized PDF files.</figcaption>
</figure>
<hr/>

<h2 id="what-you-need">What you need</h2>
<p>The scenario we describe in this article is an example of connecting Tripetto to other services, in this case Google Docs and Gmail. <a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/">Click here for more general information about connecting Tripetto to other services.</a></p>
<p>For this scenario you need the following:</p>
<ul>
  <li><strong>Tripetto</strong>, to collect your form submissions;</li>
  <li><strong>Make</strong><sup>1</sup>, to connect your form data to other services;</li>
  <li><strong>Google Docs</strong>, to create a personalized document;</li>
  <li><strong>Gmail</strong>, to send an email with PDF attachment.</li>
</ul>
<blockquote>
  <h4>🔖 Ad 1: Make alternatives</h4>
  <p>There are alternatives to use as your automation tool. In our example we use Make as our automation tool, but you can also use Zapier, Pabbly Connect or even a custom webhook. Of course it's up to you to use your favorite automation tool.</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-connect-form-responses-to-automation-tools/">How to connect form responses to automation tools</a></li>
  </ul>
</blockquote>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>These are the steps to take to automate the process of creating a personalized PDF file and send that to a recipient.</p>

<h3 id="step-1">Step 1 - Build Tripetto form</h3>
<ul>
  <li>First make sure your Tripetto form collects the desired data.</li>
  <li>Please make sure you have an email address field in your form, as we will need that to be able to send the email with the PDF file to the respondent.</li>
</ul>
<p>The result of this step is your Tripetto form, which you will use to collect your data from your respondents.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tripetto-form.png" width="1200" height="760" alt="Screenshot of Tripetto" loading="lazy" />
  <figcaption>Build your Tripetto form.</figcaption>
</figure>

<h3 id="step-2">Step 2 - Prepare Google Docs template</h3>
<ul>
  <li>In your Google Docs account create a new file/template. This will be the base for your final PDF file.</li>
  <li>Give your template a name, for example <code>Tripetto Template</code>.</li>
  <li>At the positions in your template where you want to use a value from the Tripetto form data, use the following format to insert a variable: <code>&#123;&#123;your_variable_name&#125;&#125;</code> (double brackets, without spaces). In the final file that we will generate for each form submission, those variables will be replaced with the entered value from the respondent in the Tripetto form.</li>
</ul>
<p>The result of this step is your Google Docs template, that we will use later on in the process.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/google-docs-template.png" width="1200" height="760" alt="Screenshot of Google Docs" loading="lazy" />
  <figcaption>Prepare your Google Docs template, including variables.</figcaption>
</figure>

<h3 id="step-3">Step 3 - Connect Tripetto to Make</h3>
<ul>
  <li>In your Make account add a new scenario.</li>
  <li>Give your scenario a name, for example <code>Email PDF attachment from new Tripetto submission</code>.</li>
  <li>Add a <code>Tripetto</code> module to your Make scenario. As trigger select <code>Watch Responses</code>.</li>
  <li>Connect your Tripetto form to that trigger, by copy-pasting the webhook URL from Make into your Tripetto form (navigate to <code><i class="fas fa-share-alt"></i> Automate</code><i class="fas fa-arrow-right"></i><code>Connections</code><i class="fas fa-arrow-right"></i><code>Make</code>). <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-make/">Have a look at this article for the detailed description how to connect Tripetto to Make.</a></li>
  <li>Test the connection, so Make receives the structure of your dataset.</li>
</ul>
<p>The result of this step is that your Make scenario gets executed for each new form submission and you can use the data fields from your Tripetto form in the Make scenario.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/integromat-tripetto.png" width="1200" height="760" alt="Screenshot of Tripetto module in Make" loading="lazy" />
  <figcaption>Add a Tripetto trigger in your Make scenario.</figcaption>
</figure>

<h3 id="step-4">Step 4 - Create Google Docs file</h3>
<ul>
  <li>Add a <code>Google Docs</code> module to your Make scenario. As action select <code>Create a Document from a Template</code>.</li>
  <li>Grant access to your Google account.</li>
  <li>Under <code>Document ID</code> select your earlier created Google Docs template file.</li>
  <li>If you have used variables in your template, you will now see those variables in Make. For each variable you can now select the corresponding data field from your Tripetto form. This is the part where you map the entered data from your Tripetto form to the variables in your Google Docs template.</li>
  <li>Under <code>Title</code> you can enter the file name of the file. We advise to include the email address field from your Tripetto form into this, so you can recognize the different files in your Google Drive.</li>
  <li>Under <code>New Document's Location</code> select the folder where the newly create document will be stored. We advise to store the files in a dedicated folder.</li>
  <li>Test this scenario up to this point. Check if the Google Docs file gets created in the selected folder and the variables in your template are replaced with the form data.</li>
</ul>
<p>The result of this step is you now have automated the creation of a new Google Docs file for each Tripetto form submission, including values from the Tripetto form data.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/integromat-google-docs-1.png" width="1200" height="760" alt="Screenshot of Google Docs module in Make" loading="lazy" />
  <figcaption>Add a Google Docs module in your Make scenario to create a new file.</figcaption>
</figure>

<h3 id="step-5">Step 5 - Download PDF document</h3>
<ul>
  <li>Add another <code>Google Docs</code> module to your Make scenario. As action select <code>Download a Document</code>.</li>
  <li>Under <code>Document ID</code> activate the <code>Map</code> toggle. Now select the <code>Document ID</code> that's outputted from your previous Google Docs module.</li>
  <li>Under <code>Type</code> select <code>PDF Document (.pdf)</code> to download it as a PDF file.</li>
</ul>
<p>The result of this step is your Make scenario now has access to the PDF file, which we will use in our email message.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/integromat-google-docs-2.png" width="1200" height="760" alt="Screenshot of Google Docs module in Make" loading="lazy" />
  <figcaption>Add a Google Docs module in your Make scenario to download the new file.</figcaption>
</figure>

<h3 id="step-6">Step 6 - Send email with PDF attachment</h3>
<ul>
  <li>Now add a <code>Gmail</code> module to your Make scenario. As action select <code>Send an Email</code>.</li>
  <li>Grant access to your Google account. Please make sure your Google account includes Gmail. Please note that you might have to take additional steps to be able to send emails via Gmail from Make (<a href="https://www.make.com/en/help/connections/connecting-to-google-services-using-a-custom-oauth-client?utm_source=tripetto-app&utm_medium=partner&utm_campaign=tripetto-app-partner-program" target="_blank" rel="noopener noreferrer">see instructions</a>).</li>
  <li>Under <code>To</code> enter the recipient. To email to the respondent of your form, select the email address from the Tripetto form data.</li>
  <li>Under <code>Subject</code> and <code>Content</code> you can now enter the desired subject and message of the email. You can also use Tripetto form data in those texts.</li>
  <li>Under <code>Attachments</code> select the option <code>Google Docs - Download a Document</code>. That will automatically use the PDF download from your previous module.</li>
</ul>
<p>The result of this step is an email message will be sent to the entered recipient, with the entered subject and content, including a PDF attachment with the personalized Google Docs template.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/integromat-gmail.png" width="1200" height="760" alt="Screenshot of Gmail module in Make" loading="lazy" />
  <figcaption>Add a Gmail module in your Make scenario to send an email with attachment.</figcaption>
</figure>

<h3 id="step-7">Step 7 - Test and activate</h3>
<ul>
  <li>Click <code>Run once</code> in Make to start testing your automation.</li>
  <li>Fill out and submit your Tripetto form once.</li>
  <li>Check if your Make scenario has been processed correctly. If so, check the Inbox of the recipient to receive the email and check if everything is okay.</li>
  <li>Activate your scenario in Make.</li>
</ul>
<p>From now on this scenario will be executed automatically for each new form submission! Every respondent will now receive an automated email message with a personalized PDF file as attachment 🎉</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/gmail.png" width="1200" height="760" alt="Screenshot of Gmail message" loading="lazy" />
  <figcaption>The automated email to the respondent, including the attachment.</figcaption>
</figure>

<hr />
<h2>Other examples</h2>
<p>Tripetto lets you connect to 1.000+ services. We have made some step-by-step examples for often used scenarios:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-store-form-responses-in-google-sheets/">How to automatically store form responses in Google Sheets and generate analysis reports</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-store-form-responses-in-notion/">How to automatically store form responses in Notion</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-create-PDF-files-from-form-responses/">How to automatically create PDF files from form responses and send these to respondents</a><span class="related-current">Current article</span></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-add-mailchimp-subscribers-from-form-responses/">How to automatically add Mailchimp subscribers from form responses</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-create-zendesk-support-tickets-from-form-responses/">How to automatically create Zendesk support tickets from form responses</a></li>
</ul>
