---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-automate-form-activity-tracking/
title: Automate form activity tracking - Tripetto Help Center
description: You can track the activity of your respondents inside your forms by connecting with tracking services, like Google Analytics and Facebook Pixel.
article_title: How to automate form activity tracking
article_id: tracking
article_folder: automate-tracking
article_video: automations-tracking
author: mark
time: 4
time_video: 2
category_id: automations
subcategory: automations_tracking
areas: [studio, wordpress]
---
<p>You can track the activity of your respondents inside your forms by connecting with tracking services, like Google Analytics and Facebook Pixel. This helps you to analyze form starts, form completions, interactions and drop-offs.</p>

<h2 id="when-to-use">When to use</h2>
<p>By tracking the activity of respondents in your forms, you can get insight information of their behaviors, for example:</p>
<ul>
  <li>Measure how often your form gets started;</li>
  <li>Measure how often your form gets completed, so you can analyze your form completion rates (<a href="{{ page.base }}help/articles/how-to-measure-completion-rates-with-form-activity-tracking/">more information</a>);</li>
  <li>See interactions with individual questions inside your form, so you can analyze drop-offs and possibly improve your form (<a href="{{ page.base }}help/articles/how-to-measure-drop-offs-with-form-activity-tracking/">more information</a>).</li>
</ul>
<p>To track form activity, Tripetto lets you connect to third-party tracking services. These services help you to receive and analyze the data that Tripetto shares.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-tripetto.png" width="1200" height="760" alt="Screenshot of tracking options in Tripetto" loading="lazy" />
  <figcaption>The tracking options in Tripetto.</figcaption>
</figure>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>Setting up activity tracking takes a few steps, which we will describe globally in this article. Based on your choice for a certain tracking service (see step 1), we have other articles with detailed instructions for each tracking service.</p>
<blockquote>
  <h4>🚧 Warning: Tracking service knowlegde needed</h4>
  <p>We assume you know how to configure and use your desired tracking service. Tripetto support can not help you with configuring this.</p>
</blockquote>

<h3 id="prepare">Step 1 - Prepare tracking service</h3>
<p>Let's begin with choosing and preparing the desired tracking service. There are many tracking services available out there. We will help you with setting up activity tracking in the most popular services, namely:</p>
<h4 class="title-logo"><img src="{{ page.base }}images/help/automate-tracking-services/ga.svg" width="200" height="200" alt="Logo Google Analytics" loading="lazy" />Google Analytics</h4>
<p>Google Analytics is the most used tracking service and can receive tracking data from Tripetto forms.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-track-form-activity-with-google-analytics/">How to track form activity with Google Analytics</a></li>
</ul>

<h4 class="title-logo"><img src="{{ page.base }}images/help/automate-tracking-services/gtm.svg" width="200" height="200" alt="Logo Google Tag Manager" loading="lazy" />Google Tag Manager</h4>
<p>Google Tag Manager can receive tracking data from Tripetto forms and from there on you can distribute that data to any service you have connected to Google Tag Manager.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-track-form-activity-with-google-tag-manager/">How to track form activity with Google Tag Manager</a></li>
</ul>

<h4 class="title-logo"><img src="{{ page.base }}images/help/automate-tracking-services/facebook-pixel.png" width="200" height="200" alt="Logo Facebook Pixel" loading="lazy" />Facebook Pixel</h4>
<p>Facebook Pixel lets you track conversions and can receive tracking data from Tripetto forms.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-track-form-activity-with-facebook-pixel/">How to track form activity with Facebook Pixel</a></li>
</ul>

<h4>Custom tracking service</h4>
<p>It's even possible to connect to other tracking services or to your own endpoint to do it all on your own.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-track-form-activity-with-custom-tracking-code/">How to track form activity with custom tracking code</a></li>
</ul>

<blockquote>
  <h4>🚧 Warning: Domain settings</h4>
  <p>Please make sure you configure the domain settings in your tracking service correctly, so your tracking service is able to receive the tracking events from the domain that the form is running at.</p>
  <p class="help_article_wp_only">In your WordPress site this will be your own domain name in most cases, because your form will run on your own site's domain.</p>
  <div class="help_article_studio_only">
    <p>If you're using the Tripetto studio this configuration depends on how you share your form:</p>
    <ul>
      <li>Shareable tripetto.app link: if you share your form via the shareable tripetto.app link, you will have to add <code>tripetto.app</code> as a domain in your tracking service;</li>
      <li>Embed in your site: if you have embedded your form in a site with the inline embed code, you will have to add the domain name of that site as a domain in your tracking service;</li>
      <li>Iframe in your site: if you have embedded your form in a site via an iframe with a <code>src</code> parameter, you will have to add the domain name of that <code>src</code> parameter as a domain in your tracking service. Please notice there can be more additional settings needed to be able to track form activity from an iframe.</li>
    </ul>
  </div>
  <p>It depends on the tracking service you use how to configure the domains. Tripetto support can not help you with configuring this.</p>
</blockquote>

<h3 id="connect">Step 2 - Connect tracking service</h3>
<p>After you have configured your tracking service, you get a unique ID. You need that ID to be pasted into Tripetto.</p>
<p>At the top menu bar of the form builder click <code><i class="fas fa-share-alt"></i> Automate</code><i class="fas fa-arrow-right"></i> Click <code>Tracking</code>. The Tracking pane will show up on the right side of the form builder.</p>
<p>Now enable the tracking service you want to use and you can copy-paste the ID that you got from your tracking service. From now on your Tripetto form is connected to your tracking service and can send data to it.</p>
<figure>
  <img src="{{ page.base }}images/help/automate-tracking-google-analytics/02-tripetto-id.png" width="636" height="339" alt="Screenshot of tracking in Tripetto" loading="lazy" />
  <figcaption>Example of tracking with Google Analytics.</figcaption>
</figure>

<h3 id="events">Step 3 - Select tracking events</h3>
<p>By default Tripetto will track the most important activities (form starting and form completion), but you can control which activities Tripetto needs to track for you.</p>
<p>Tripetto forms can record and share the following events:</p>
<ul>
  <li><code>Track form starting (<i>tripetto_start</i>)</code> - Tracks when a form is started;</li>
  <li><code>Track form completion (<i>tripetto_complete</i>)</code> - Tracks when a form is completed;</li>
  <li><code>Track staged blocks (<i>tripetto_stage</i>)</code> - Tracks when a block becomes available.<br/>It depends on your selected <a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/">form face</a> when this event will be fired:
    <ul>
      <li>Autoscroll form face: this event will be fired when a block gets activated;</li>
      <li>Chat form face: this event fires when a block becomes answerable;</li>
      <li>Classic form face: this even fires when the block becomes visible.</li>
    </ul>
  </li>
  <li><code>Track unstaged blocks (<i>tripetto_unstage</i>)</code> - Tracks when a block becomes unavailable.<br/>It depends on your selected <a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/">form face</a> when this event will be fired:
    <ul>
      <li>Autoscroll form face: this event will be fired when a block gets deactivated;</li>
      <li>Chat form face: this event fires when a block becomes unanswerable;</li>
      <li>Classic form face: this even fires when the block becomes invisible.</li>
    </ul>
  </li>
  <li><code>Track focus (<i>tripetto_focus</i>)</code> - Tracks when an input element gains focus;</li>
  <li><code>Track blur (<i>tripetto_blur</i>)</code> - Tracks when an input element loses focus;</li>
  <li><code>Track form pausing (<i>tripetto_pause</i>)</code> - Tracks when a form is paused.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/automate-tracking-google-analytics/03-tripetto-events.png" width="622" height="349" alt="Screenshot of tracking in Tripetto" loading="lazy" />
  <figcaption>Example of tracking events to track with Google Analytics.</figcaption>
</figure>
<blockquote class="help_article_studio_only">
  <h4>🚧 Warning: Update embed code (studio only)</h4>
  <p>If you have embedded your form in your website/app via the Tripetto studio, please make sure you always update your embed code over there.</p>
</blockquote>

<h3 id="receive">Step 4 - Receive tracking data</h3>
<p>Now your tracking service will receive the selected tracking data from your Tripetto form. From there on you can follow the instructions of your tracking service to configure the desired analysis.</p>
<blockquote>
  <h4>📌 Also see: Tracking data analysis</h4>
  <p>We have some help articles about the most common use cases for tracking data analysis:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-measure-completion-rates-with-form-activity-tracking/">How to measure completion rates with form activity tracking</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-measure-drop-offs-with-form-activity-tracking/">How to measure drop-offs with form activity tracking</a></li>
  </ul>
</blockquote>
<hr/>

<h2 id="custom" data-anchor="Custom tracking code">Custom tracking code</h2>
<p>It's also possible to connect to other tracking services than Google Analytics, Google Tag Manager and Facebook Pixel. To do that you can add your own custom tracking code.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-track-form-activity-with-custom-tracking-code/">How to track form activity with custom tracking code</a></li>
</ul>
<blockquote class="help_article_studio_only">
  <h4>🚧 Warning: Only in embedded forms (studio only)</h4>
  <p>You can use the custom tracking code in the Tripetto studio at tripetto.app, but this will only work for your embedded forms. This will not work if you use the shareable link of your form at the tripetto.app domain.</p>
  <p>Please also make sure you always update your embed code in your website/app.</p>
</blockquote>
