---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-track-form-activity-with-google-tag-manager/
title: Track form activity with Google Tag Manager - Tripetto Help Center
description: Track the activity of your respondents inside your forms by connecting with Google Tag Manager.
article_title: How to track form activity with Google Tag Manager
article_id: tracking
article_folder: automate-tracking-google-tag-manager
author: jurgen
time: 7
category_id: automations
subcategory: automations_tracking_tools
areas: [studio, wordpress]
---
<p>Track the activity of your respondents inside your forms by connecting with Google Tag Manager and distribute your data from there on to your connected services.</p>

<h2 id="when-to-use">When to use</h2>
<p>By tracking the activity of respondents in your forms, you can get insight information of their behaviors. This helps you to analyze form starts, form completions, interactions and drop-offs.</p>
<p>With <strong>Google Tag Manager</strong> you can receive those events and distribute them from there on to your desired connected services. Follow the instructions in this article if you want to use Google Tag Manager to receive your form activity.</p>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>In this article we will show the steps you have to take to send Tripetto activity data to Google Tag Manager. You can also have a look at our <a href="{{ page.base }}help/articles/how-to-automate-form-activity-tracking/">global article about how form activity tracking works</a>.</p>
<blockquote>
  <h4>🚧 Warning: Google Tag Manager knowlegde needed</h4>
  <p>We assume you know how to configure and use Google Tag Manager. Tripetto support can not help you with configuring your Google Tag Manager account. Follow the Tag Manager Help by Google for more information:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="https://support.google.com/tagmanager/" target="_blank" rel="noopener noreferrer">Tag Manager Help by Google</a></li>
  </ul>
</blockquote>

<h3 id="prepare">Step 1 - Prepare Google Tag Manager</h3>
<p>First step is to get your unique ID from Google Tag Manager that you will need to connect Tripetto with Google Tag Manager.</p>
<h4>In Google Tag Manager:</h4>
<p>First make sure you have created and configured your container in Google Tag Manager like you want to. This means you configure your Google Tag and connect the services you want to send data to.</p>

<h3 id="connect">Step 2 - Connect Google Tag Manager</h3>
<p>Next step is to connect your Tripetto form to Google Tag Manager.</p>

<h4>In Google Tag Manager:</h4>
<p>After your Google Tag Manager container is configured, simply copy the <code>Container ID</code> of your container, which starts with <code>GTM-</code>. You can find that in the Admin tab in Google Tag Manager. You will need that ID later on in Tripetto.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/00-google-tag-manager.png" width="1200" height="760" alt="Screenshot of Google Tag Manager" loading="lazy" />
  <figcaption>Get the <code>Container ID</code> from Google Tag Manager.</figcaption>
</figure>

<h4>In Tripetto:</h4>
<p>Switch to your form in Tripetto. At the top menu bar of the form builder click <code><i class="fas fa-share-alt"></i> Automate</code><i class="fas fa-arrow-right"></i> Click <code>Tracking</code>. The Tracking pane will show up on the right side of the form builder.</p>
<p>In there, enable the feature <code>Track form activity with Google Analytics or Google Tag Manager</code> to expand the settings for Google Tag Manager.</p>
<p>Before you enter your container ID, first ask yourself if the page that your Tripetto form is hosted at already has the Google Tag Manager tag installed to it:</p>
<ul>
  <li>If you already installed the Google Tag Manager tag, you can simply enable the option <code>Google Analytics or Google Tag Manager is already installed on my website</code>. If enabled, you don't have to enter your container ID anymore and the Tripetto events will be sent to the container ID that you have installed yourself.<sup>1</sup></li>
  <li>If you haven't already installed the Google Tag Manager tag, you can paste the container ID that you just copied from Google Tag Manager. Tripetto will recognize automatically that you're using Google Tag Manager, based on the container ID that you supply.</li>
</ul>
<blockquote>
  <h4>🔖 Ad 1: Using the shareable link?</h4>
  <p>If you're (also) using the shareable link to your form, make sure you always supply your container ID to be able to track events in the shareable link version of your form.</p>
</blockquote>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/02-tripetto-id.png" width="631" height="324" alt="Screenshot of tracking in Tripetto" loading="lazy" />
  <figcaption>Enter your Google Tag Manager ID.</figcaption>
</figure>

<h3 id="events">Step 3 - Select tracking events</h3>
<p>Now your Tripetto form is connected to your Google Tag Manager container. By default Tripetto will track the most important activities (form starting and form completion), but you can control which activities Tripetto needs to track for you.</p>
<h4>In Tripetto:</h4>
<p>Select the tracking events that you want to receive insights for in your Google Tag Manager container.</p>
<p>Tripetto forms can record and share the following events:</p>
<ul>
  <li><code>Track form starting (<i>tripetto_start</i>)</code> - Tracks when a form is started;</li>
  <li><code>Track form completion (<i>tripetto_complete</i>)</code> - Tracks when a form is completed;</li>
  <li><code>Track staged blocks (<i>tripetto_stage</i>)</code> - Tracks when a block becomes available.<br/>It depends on your selected <a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/">form face</a> when this event will be fired:
    <ul>
      <li>Autoscroll form face: this event will be fired when a block gets activated;</li>
      <li>Chat form face: this event fires when a block becomes answerable;</li>
      <li>Classic form face: this even fires when the block becomes visible.</li>
    </ul>
  </li>
  <li><code>Track unstaged blocks (<i>tripetto_unstage</i>)</code> - Tracks when a block becomes unavailable.<br/>It depends on your selected <a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/">form face</a> when this event will be fired:
    <ul>
      <li>Autoscroll form face: this event will be fired when a block gets deactivated;</li>
      <li>Chat form face: this event fires when a block becomes unanswerable;</li>
      <li>Classic form face: this even fires when the block becomes invisible.</li>
    </ul>
  </li>
  <li><code>Track focus (<i>tripetto_focus</i>)</code> - Tracks when an input element gains focus;</li>
  <li><code>Track blur (<i>tripetto_blur</i>)</code> - Tracks when an input element loses focus;</li>
  <li><code>Track form pausing (<i>tripetto_pause</i>)</code> - Tracks when a form is paused.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/03-tripetto-events.png" width="622" height="349" alt="Screenshot of tracking in Tripetto" loading="lazy" />
  <figcaption>Select the tracking events to track with Google Tag Manager.</figcaption>
</figure>
<blockquote class="help_article_studio_only">
  <h4>🚧 Warning: Update embed code (studio only)</h4>
  <p>If you have embedded your form in your website/app, please make sure you always update your embed code over there.</p>
</blockquote>

<h3 id="receive">Step 4 - Receive tracking data</h3>
<p>Google Tag Manager is now ready to receive the selected tracking data from your Tripetto form.</p>

<h4>In Google Tag Manager</h4>
<p>Tracking events are sent to Google Tag Manager using a <a href="https://support.google.com/tagmanager/answer/6164391?hl=en" target="_blank" rel="noopener noreferrer"><strong>data layer</strong></a>. You can receive that data layer in Google Tag Manager by configuring a <strong>data layer variable</strong> in there. Once that's configured you can use that variable in your tags and triggers in Google Tag Manager to distribute the collected event data to any service you have connected to your Google Tag Manager.</p>
<blockquote>
  <h4>🚧 Warning: Google Tag Manager knowlegde needed</h4>
  <p>We assume you know how to configure and use data layer variables in Google Tag Manager. Tripetto support can not help you with configuring your data layer variables in Google Tag Manager.</p>
  <p>For help with data layer variables, please have a look at the <a href="https://support.google.com/tagmanager/" target="_blank" rel="noopener noreferrer">Tag Manager Help by Google</a> and specifically <a href="https://support.google.com/tagmanager/answer/6164391?hl=en" target="_blank" rel="noopener noreferrer">this article about the data layer</a>.</p>
</blockquote>

<h4>Data layer variables</h4>
<p>Tripetto sends the following variables to Google Tag Manager via the data layer. It's important that you use the exact names in your data layer variable configurations in Google Tag Manager to be able to retrieve the value that the Tripetto form sends.</p>
<ul>
  <li><code>event</code> - The name of the event;</li>
  <li><code>description</code> - The description of the event;</li>
  <li><code>form</code> - The name of the form;</li>
  <li><code>id</code> - The id of the form<span class="help_article_studio_only"> (not available when using the Tripetto studio)</span>;</li>
  <li><code>reference</code> - The reference id of the form<span class="help_article_studio_only"> (not available when using the Tripetto studio)</span>;</li>
  <li><code>fingerprint</code> - The fingerprint of the form;</li>
  <li><code>block</code> - The name of the block (in case an event is about a specific block in your form);</li>
  <li><code>key</code> - The id of the block (in case an event is about a specific block in your form).</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/04-google-tag-manager-variable.png" width="759" height="536" alt="Screenshot of Google Tag Manager" loading="lazy" />
  <figcaption>A variable in Google Tag Manager that retrieves the form name (Data Layer Variable Name is set to <code>form</code>).</figcaption>
</figure>
<p>Now that this data is available in a variable in Google Tag Manager, you can use that variable in your tags in Google Tag Manager. For example to send an event to Google Analytics (GA4) or any other service you connect to via Google Tag Manager.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/05-google-tag-manager-tag-ga4.png" width="931" height="669" alt="Screenshot of Google Tag Manager" loading="lazy" />
  <figcaption>Example of how to use a variable inside a tag in Google Tag Manager (Tag label is set to <code>&#123;&#123;tripetto_form&#125;&#125;</code>, which is a reference to the variable name).</figcaption>
</figure>

<h4>In Tripetto:</h4>
<p>After you have configured your Google Tag Manager and the connected services, open your Tripetto form and submit an entry to it. The best way to do so, is by just opening your form and perform the actions you want to track yourself.</p>
<h4>In Google Tag Manager:</h4>
<p>Now switch back to Google Tag Manager and/or the service that you distributed the data to and see if the data is shown.</p>

<h2 id="troubleshooting">Troubleshooting</h2>
<p>Are your events not received by your Google Tag Manager and/or the connected service(s)? There can be a few reasons:
<ul>
  <li>Please notice that although Tripetto sends all events in realtime, it can take some time (up to 24 hours) before your receiving service shows the events from your Tripetto form.</li>
  <li>Next step is to check if the events are triggered in the form. To test this there are some browser extensions that can give instant insights on the events that are measured. You can install such an extension right in your browser, for example Google Chrome.
    <ul>
      <li>If the events are not visible in such an extension, please check if your event tracking is configured correctly in Tripetto.</li>
      <li>If the events are visible in such an extension, but not in your Google Tag Manager/connected service, please check if your Google Tag Manager is configured correctly. For example check if you have configured the needed data layer variables and if your GTM container has been published.</li>
    </ul>
  </li>
  <li>Next step is to check if there can be any conflicts with other Google script tags in your site, for example if you have already installed the GTM tag yourself. To check this, please temporarily remove all other Google script tags from your page where the Tripetto form is at. And then test if the events are triggered and sent correctly.</li>
  <li>To prevent such conflicts, you can enable the setting <code>Google Analytics or Google Tag Manager is already installed on my website</code>. If enabled, Tripetto will not add another GTM tag to your page, which will prevent any conflicts.</li>
</ul>
