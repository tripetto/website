---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-connect-to-other-services-with-zapier/
title: Connect to other services with Zapier - Tripetto Help Center
description: Zapier is an automation tool that helps you to connect your Tripetto response data to other online services.
article_title: How to connect to other services with Zapier
article_id: webhook
article_folder: automate-webhook-zapier
article_video: automations-zapier
author: jurgen
time: 5
time_video: 3
category_id: automations
subcategory: automations_webhook_tools
areas: [studio, wordpress]
common_content_core: true
---
<p>Zapier is an automation tool that helps you to connect your Tripetto response data to other online services. This article describes how to enable our webhook for this and how to configure it with Zapier.</p>

<div class="article-content-core">
<h2 id="when-to-use">When to use</h2>
<p>By connecting Tripetto to other services you can do all kinds of actions with your response data, like pushing it to a spreadsheet editor (Microsoft Excel/Google Sheets) or a database, or trigger other follow-up actions. The possibilities are endless!</p>
<p>Zapier is a tool that can help you with this. Follow the instructions in this article if you want to use Zapier to connect your Tripetto response data with other software services.</p>
<blockquote>
  <h4>📣 Info: Zapier alternatives</h4>
  <p>There are alternatives to use for Zapier, for example <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-make/">Make</a>, <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-pabbly-connect/">Pabbly Connect</a> or even a <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-custom-webhook/">custom webhook</a>. Of course it's up to you to use your favorite automation tool.</p>
</blockquote>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>In this article we will show the steps you have to take to connect Tripetto with Zapier. You can also have a look at our <a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/">global article about how webhooks work</a>.</p>
<blockquote>
  <h4>🚧 Warning: Zapier knowlegde needed</h4>
  <p>We assume you know how to configure and use Zapier. Tripetto support can not help you with configuring this and/or the services that you want to connect your Tripetto form to.</p>
</blockquote>

<h3 id="step-1">Step 1 - Prepare Zapier</h3>
<p>The method we're going to use is the 'Webhook by Zapier'. You can find all details on that with the link below.</p>
<div>
  <a href="https://zapier.com/apps/webhook" target="_blank" rel="noopener noreferrer" class="blocklink">
    <div>
      <span class="title">Webhooks by Zapier<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Instantly connect Webhooks by Zapier with the apps you use everyday. Webhooks by Zapier integrates with 1,500 other apps on Zapier - it's the easiest way to automate your work.</span>
      <span class="url">zapier.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-zapier.svg" width="160" height="160" alt="Zapier logo" loading="lazy" />
    </div>
  </a>
</div>
<h4>In Zapier:</h4>
<p>In Zapier simply create a new Zap. You'll immediately see the first block called <code>Trigger</code>. This is where we are going to collect the responses from Tripetto.</p>
<p>Click the trigger block and search for <code>Webhooks by Zapier</code>. Select the corresponding app button in the search results. The webhook app will be added to your Zap.</p>
<p>Next step, select the event that triggers your automation. From the dropdown, select <code>Catch Hook</code>.</p>
<p>Click <code>Continue</code> and ignore the next step (click <code>Continue</code> again).</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/catch-hook.png" width="1200" height="760" alt="Screenshot of Zapier" loading="lazy" />
  <figcaption>The settings of the webhook block.</figcaption>
</figure>

<h3 id="step-2">Step 2 - Connect Zapier</h3>
<p>Now we're going to connect your Tripetto form and your webhook in Zapier.</p>
<h4>In Zapier:</h4>
<p>Zapier will show the webhook URL, which you need in Tripetto. So, copy the webhook URL and switch to Tripetto.</p>
<h4>In Tripetto:</h4>
<p>Now switch to your form in Tripetto. At the top menu bar of the form builder click <code><i class="fas fa-share-alt"></i> Automate</code><i class="fas fa-arrow-right"></i> Click <code>Connections</code>. The Connections pane will show up on the right side of the form builder.</p>
<p>The second option in this screen is <code>Zapier</code>. After enabling the feature <code>Submit completed forms to Zapier</code> you can paste the webhook URL you got from your new Zapier Zap.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tripetto.png" width="1200" height="760" alt="Screenshot of Tripetto" loading="lazy" />
  <figcaption>Copy-paste your webhook URL in Tripetto.</figcaption>
</figure>
<blockquote class="help_article_wp_only">
  <h4>📌 Also see: Using file uploads (WordPress)</h4>
  <p>If you have a <a href="{{ page.base }}help/articles/how-to-use-the-file-upload-block/">File Upload block</a> or a <a href="{{ page.base }}help/articles/how-to-use-the-signature-block/">Signature block</a> in your form in WordPress, you can download the files from your respondents with a download link. By default such download links are only available to the owner of the form. You have to be logged in to the corresponding WordPress site (WP Admin) to be able to download the files. If you want to make the download links available for Make you can enable the setting <code>Allow access to uploaded files</code>.</p>
  <p>For more instructions have a look at this help article about using file uploads from the WordPress plugin in webhooks:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/use-file-uploads-from-the-wordpress-plugin-in-webhooks/">Use file uploads from the WordPress plugin in webhooks</a></li>
  </ul>
</blockquote>
<blockquote class="help_article_studio_only">
  <h4>📌 Also see: Using file uploads (studio)</h4>
  <p>If you have a <a href="{{ page.base }}help/articles/how-to-use-the-file-upload-block/">File Upload block</a> or a <a href="{{ page.base }}help/articles/how-to-use-the-signature-block/">Signature block</a> in your form in the studio, you can download the files from your respondents with a download link. In general such download links are only available to the owner of the form. You have to be logged in to the corresponding Tripetto studio account to be able to download the files. Only for the first 24 hours after form submissions, those download links will be available to Make.</p>
  <p>For more instructions have a look at this help article about using file uploads from the studio in webhooks:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/use-file-uploads-from-the-studio-in-webhooks/">Use file uploads from the studio in webhooks</a></li>
  </ul>
</blockquote>

<h3 id="step-3">Step 3 - Receive response data</h3>
<p>Next up, Zapier wants to test if the Zap is receiving data and learn the structure of the form. To test this, we're going to submit a test response in our Tripetto form.</p>
<h4>In Tripetto:</h4>
<p>Go to your Tripetto form and submit some response data. You can do that by simply clicking the <code>Test</code> button. That will send a test response from your form to Zapier with some dummy content. A better way is to simply submit a real form entry yourself via the shareable link of your form. That way you're sure that the test data meets the correct formats.</p>
<h4>In Zapier:</h4>
<p>After that, return to Zapier and click <code>Test trigger</code>. You will see Zapier has found some data. When you click on the response, you'll see the data from the form you just submitted. Sweet, you are connected!</p>
<p>Finally click <code>Continue</code>. You have now prepared Zapier to receive Tripetto response data and Zapier understands the data you send to it. The next step is to add your desired service(s) to your Zap.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/test-data.png" width="1200" height="760" alt="Screenshot of Zapier" loading="lazy" />
  <figcaption>Zapier has received some test data.</figcaption>
</figure>

<h3 id="step-4">Step 4 - Add services</h3>
<p>In Zapier you can now add the follow-up services you want to trigger.</p>
<h4>In Zapier:</h4>
<p>For each service you add another step and follow the instructions of Zapier. How to connect to other services differs for each service, so we can not give an instruction on that.</p>
<p>In most cases Zapier will show data fields of the connected service in which you can enter Tripetto response data. You can select the corresponding data fields that you tested with in step 3. In that way you can connect certain questions in your Tripetto form to the desired end location of another service.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/columns.png" width="1200" height="760" alt="Screenshot of Zapier" loading="lazy" />
  <figcaption>An example of the settings of a Google Sheets file.</figcaption>
</figure>

<h3 id="step-5">Step 5 - Activate magic</h3>
<p>If you're done editing your Zap, you can activate it.
</p>
<h4>In Zapier:</h4>
<p>To really let the Zap receive response data, you'll need to publish it. Use the big switch at the right top corner to switch if from <code><i class="fas fa-toggle-off"></i> Off</code> to <code><i class="fas fa-toggle-on"></i> On</code>.</p>
<blockquote>
  <h4>📣 Info: Confirm Zapier account</h4>
  <p>If this is your first Zap, you might need to confirm your Zapier account, by following the confirmation steps Zapier provides.</p>
</blockquote>
<p>From now on, each completed submission of this Tripetto form will send its data to Zapier and Zapier will execute the magic into other services.</p>
<hr />

<h2 id="possibilities">Discover the possibilities!</h2>
<p>Zapier supports lots of great services to connect to.</p>

<h3 id="examples">Examples</h3>
<p>We listed some common used scenarios that you can start with right away!</p>
</div>
<ul class="tiles tiles-two">
{% include tile.html url='https://zapier.com/apps/webhook/integrations/google-sheets' target=true type='Zapier' title='Tripetto to Google Sheets' description='Add a new row to a Google Sheet with all Tripetto response data.' webhook-service='google-sheets' webhook-service-name='Google Sheets' palette-top='light' palette-bottom='light' %}
{% include tile.html url='https://zapier.com/apps/webhook/integrations/activecampaign' target=true type='Zapier' title='Tripetto to ActiveCampaign' description='Add a new contact to ActiveCampaign based on Tripetto response data.' webhook-service='activecampaign' webhook-service-name='ActiveCampaign' palette-top='light' palette-bottom='light' %}
{% include tile.html url='https://zapier.com/apps/webhook/integrations/mailchimp' target=true type='Zapier' title='Tripetto to Mailchimp' description='Add a new subscriber to a Mailchimp audience based on Tripetto response data.' webhook-service='mailchimp' webhook-service-name='Mailchimp' palette-top='light' palette-bottom='light' %}
{% include tile.html url='https://zapier.com/apps/webhook/integrations/woocommerce' target=true type='Zapier' title='Tripetto to Woocommerce' description='Create a new customer and create a new order in Woocommerce based on Tripetto response data.' webhook-service='woocommerce' webhook-service-name='Woocommerce' webhook-chain-service='woocommerce' webhook-chain-service-name='Woocommerce' palette-top='light' palette-bottom='light' %}
{% include tile.html url='https://zapier.com/apps/webhook/integrations/zendesk' target=true type='Zapier' title='Tripetto to Zendesk' description='Create a new Zendesk support ticket based on Tripetto response data.' webhook-service='zendesk' webhook-service-name='Zendesk' palette-top='light' palette-bottom='light' %}
{% include tile.html url='https://zapier.com/apps/webhook/integrations/airtable' target=true type='Zapier' title='Tripetto to Airtable + Stripe' description='Add a new record to an Airtable database with all Tripetto response data and create a transaction in Stripe.' webhook-service='airtable' webhook-service-name='Airtable' webhook-chain-service='stripe' webhook-chain-service-name='Stripe' palette-top='light' palette-bottom='light' %}
</ul>
<div class="article-content-core">

<h3 id="all">All Zapier apps</h3>
<p>Take a look at the endless possibilities of services in Zapier in their <a href="https://zapier.com/apps" target="_blank" rel="noopener noreferrer">apps overview</a>. Just make sure you configure their webhook connection as described in this article and then integrate it with the service(s) you want.</p>
<div>
  <a href="https://zapier.com/apps" target="_blank" rel="noopener noreferrer" class="blocklink">
    <div>
      <span class="title">Explore all apps - Zapier<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Connect the apps you use everyday to automate your work and be more productive. 1,500+ apps and easy integrations - get started in minutes.</span>
      <span class="url">zapier.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/automate-webhook-services/zapier.svg" width="160" height="160" alt="Zapier logo" loading="lazy" />
    </div>
  </a>
</div>
</div>
