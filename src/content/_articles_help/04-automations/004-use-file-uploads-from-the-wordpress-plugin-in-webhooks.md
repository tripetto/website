---
layout: help-article
base: ../../../
permalink: /help/articles/use-file-uploads-from-the-wordpress-plugin-in-webhooks/
title: Use file uploads from the WordPress plugin in webhooks - Tripetto Help Center
description: Webhook connections send all form data to your automation tool. To use file uploads from the WordPress plugin, there are some additional instructions.
article_title: Use file uploads from the WordPress plugin in webhooks
article_folder: automate-webhook-file-uploads
author: mark
time: 4
category_id: automations
subcategory: automations_webhook
areas: [wordpress]
---
<p>Webhook connections send all form data to your automation tool. To use file uploads from the WordPress plugin in your automations, there are some additional instructions.</p>

<h2 id="when-to-use">When to use</h2>
<p>When you <a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/">connect your Tripetto responses to automation tools</a>, all the submitted data can be used in your automations right away. If your form responses from the Tripetto WordPress plugin include files from the <a href="{{ page.base }}help/articles/how-to-use-the-file-upload-block/">File Upload block</a> or the <a href="{{ page.base }}help/articles/how-to-use-the-signature-block/">Signature block</a>, you can also use those in your automations, for example to store in a cloud service. To do so, you can follow the instructions included in this article.</p>
<blockquote>
  <h4>🚧 Warning: Automation tool/webhook knowlegde needed</h4>
  <p>We assume you know how to configure and use your desired automation tool and/or webhook. Tripetto support can not help you with configuring this and/or the services that you want to connect your Tripetto form to.</p>
</blockquote>
<hr/>

<h2 id="availability">File download availability</h2>
<p>By default the files from the <a href="{{ page.base }}help/articles/how-to-use-the-file-upload-block/">File Upload block</a> and the <a href="{{ page.base }}help/articles/how-to-use-the-signature-block/">Signature block</a> only are available to the owner of the form. You have to be logged in to the corresponding WordPress site (WP Admin) to be able to download the files. Now, this becomes difficult for automation tools, because those can't login to your WP Admin and thus won't have access to the file downloads.</p>
<p>Therefore there is an extra setting available in the Connections pane in the WordPress plugin to make the files available to your automation tool. To activate that, open your form in the Tripetto WordPress plugin. At the top menu bar of the form builder click <code><i class="fas fa-share-alt"></i> Automate</code><i class="fas fa-arrow-right"></i> Click <code>Connections</code>. The Connections pane will show up on the right side of the form builder.</p>
<p>Now in the webhook that you have connected, activate the setting <code>Allow access to uploaded files</code>. Now the download links will be available for that automation tool, so you can use it in your automation scenarios from there on.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tripetto-wordpress.png" width="1200" height="760" alt="Screenshot of the Connections pane in the Tripetto WordPress plugin" loading="lazy" />
  <figcaption>Connections screen in the Tripetto WordPress plugin with a Make connection enabled.</figcaption>
</figure>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>File uploads from Tripetto forms can be downloaded via a download link. To use that in your automations, you need an extra step in your automation tool to download the file into your automation scenario. To demonstrate this, we use Make<sup>1</sup> as our automation tool.</p>
<blockquote>
  <h4>📌 Also see: Make setup instructions</h4>
  <p>We have a more detailed help article about connecting your form response to Make:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-make/">How to connect to other services with Make</a></li>
  </ul>
</blockquote>
<blockquote>
  <h4>🔖 Ad 1: Make alternatives</h4>
  <p>There are alternatives to use as your automation tool. For this demonstration we use Make as our automation tool, but you can also use Zapier, Pabbly Connect or even a custom webhook. Of course it's up to you to use your favorite <a href="{{ page.base }}help/articles/how-to-connect-form-responses-to-automation-tools/">automation tool</a>. Those will have comparable setups available to use files from Tripetto. We assume you know how to configure and use your desired automation tool and/or webhook.</p>
</blockquote>

<h3 id="setup-basic">Basic automation</h3>
<p>First we will demonstrate how to setup the basic automation to download the file into your automation tool. The basic configuration in Make looks like this:</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/make-setup-basic.png" width="1200" height="760" alt="Screenshot of a Make automation scenario" loading="lazy" />
  <figcaption>Basic automation scenario in Make with File uploads from Tripetto.</figcaption>
</figure>

<h4>Step 1 - Watch Tripetto responses</h4>
<ul>
  <li>Add a <code>Tripetto</code> module to your Make scenario. As trigger select <code>Watch responses</code>.</li>
  <li>Connect your Tripetto form to this webhook URL.</li>
  <li>Receive a form response to this module, so the scenario learns your Tripetto form structure.</li>
</ul>

<h4>Step 2 - Download file</h4>
<ul>
  <li>Add a <code>HTTP</code> module to your Make scenario. As action select <code>Make a request</code>.</li>
  <li>As <code>URL</code> select the corresponding File Upload/Signature block of your form from the Tripetto module you created in step 1.</li>
</ul>
<p>Now the Make scenario will download the file from the File Upload/Signature block, assuming the <code>Allow access to uploaded files</code> setting is enabled and thus the file download is available for Make.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/make-http.png" width="1200" height="760" alt="Screenshot of HTTP module in Make" loading="lazy" />
  <figcaption>Add a HTTP module in your Make scenario to download the file.</figcaption>
</figure>
<p>This is the basic setup to download the file data into your automation scenario. From there on you can now use that file data in your follow-ups in your scenario.</p>

<h3 id="setup-filename">Basic automation with file names</h3>
<p>If you want to store the downloaded file, you might want to use the original file name of the file that your respondent uploaded in the Tripetto form. Therefore the download link includes a query string parameter <code>filename</code> with the original file name as value.</p>
<blockquote>
  <h4>📣 Info: Signature block file names</h4>
  <p>Files from the Signature block have a fixed file name in this format: <code>signature-<i>YYYYMMDDHHMMSS</i>.png</code></p>
</blockquote>
<p>The more advanced configuration in Make, including the file name, looks like this:</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/make-setup-file-name.png" width="1200" height="760" alt="Screenshot of a Make automation scenario" loading="lazy" />
  <figcaption>More advanced automation scenario in Make with File uploads from Tripetto.</figcaption>
</figure>

<h4>Step 1-2 - Setup basic configuration</h4>
<p>Setup the basic configuration as described in steps 1-2 above.</p>

<h4>Step 3 - Get file name</h4>
<ul>
  <li>Add a <code>Tool</code> module to your Make scenario. As action select <code>Set variable</code>.</li>
  <li>As <code>Variable name</code> enter a name, for example <code>File name</code>.</li>
  <li>As <code>Variable lifetime</code> select <code>One cycle</code>.</li>
  <li>We now want to extract the filename parameter from the query string of the file download. To do do, as <code>Variable value</code> setup the following:
    <ul>
      <li>Insert a text <code>split</code> function (<a href="https://www.make.com/en/help/functions/string-functions?utm_source=tripetto-app&utm_medium=partner&utm_campaign=tripetto-app-partner-program#split--text--separator-" target="_blank" rel="noopener noreferrer">see Make documentation</a>).
        <figure>
          <img src="{{ page.base }}images/help/{{ page.article_folder }}/make-variable-split.png" width="1200" height="760" alt="Screenshot of a variable in Make" loading="lazy" />
          <figcaption>Add a variable module in your Make scenario and insert a <code>split</code> function.</figcaption>
        </figure>
      </li>
      <li>As first parameter in the <code>split</code> function insert the corresponding File Upload block of your form from the Tripetto module.</li>
      <li>As second parameter in the <code>split</code> function enter <code>filename=</code>.
        <figure>
          <img src="{{ page.base }}images/help/{{ page.article_folder }}/make-variable.png" width="1200" height="760" alt="Screenshot of a variable in Make" loading="lazy" />
          <figcaption>Add a variable module in your Make scenario and setup the <code>split</code> function.</figcaption>
        </figure>
      </li>
    </ul>
  </li>
</ul>
<p>Now the Make scenario has stored the original file name of the file that the respondent uploaded to your Tripetto form. From now on you can use that file name in your follow-ups in your scenario.</p>
<hr/>

<h2 id="example-google-drive">Example: Store file in Google Drive</h2>
<p>An often used scenario will be to store files to a cloud storage service, for example Google Drive. In this example we will demonstrate a possible setup for that scenario in Make.</p>
<p>The full configuration in Make looks like this:</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/make-setup-google-drive.png" width="1200" height="760" alt="Screenshot of a Make automation scenario" loading="lazy" />
  <figcaption>Full automation scenario in Make with File uploads from Tripetto to Google Drive.</figcaption>
</figure>

<h4>Step 1-2-3 - Setup basic configuration with file names</h4>
<p>Setup the basic configuration with file names as described in steps 1-3 above.</p>

<h4>Step 4 - Upload to Google Drive</h4>
<ul>
  <li>Add a <code>Google Drive</code> module to your Make scenario. As action select <code>Upload a file</code>.</li>
  <li>Grant access to your Google account.</li>
  <li>Optionally select the folder in your Google Drive where the files should be stored.</li>
  <li>As <code>File name</code> select the variable you created in step 3 (<code>File name</code> in our example). To prevent issues with encoding you can add a <code>decodeURL</code> function to the file name (<a href="https://www.make.com/en/help/functions/string-functions?utm_source=tripetto-app&utm_medium=partner&utm_campaign=tripetto-app-partner-program#decodeurl--text-" target="_blank" rel="noopener noreferrer">see Make documentation</a>).</li>
  <li>As <code>Data</code> select the <code>Data</code> of the HTTP module you created in step 2.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/make-google-drive.png" width="1200" height="760" alt="Screenshot of Google Drive module in Make" loading="lazy" />
  <figcaption>Add a Google Drive module in your Make scenario to store the file.</figcaption>
</figure>
<p>Now the Make scenario will store the file with the original file name to your Google Drive!</p>
