---
layout: help-article
base: ../../../
permalink: /help/articles/how-to-automatically-add-mailchimp-subscribers-from-form-responses/
title: Automatically add Mailchimp subscribers - Tripetto Help Center
description: Connect your Tripetto form to Mailchimp to automatically add subscribers to your newsletter.
article_id: connection-mailchimp
article_title: How to automatically add Mailchimp subscribers from form responses
article_folder: automate-webhook-mailchimp
author: jurgen
time: 4
category_id: automations
subcategory: automations_webhook_examples
areas: [studio, wordpress]
---
<p>Connect your Tripetto form to Mailchimp to automatically add subscribers to your newsletter. For this example we use Make as our automation tool.</p>

<h2 id="when-to-use">When to use</h2>
<p>You can use this automation if you use a Tripetto form as a subscribe form. It will automatically add respondents from your Tripetto form as subscribers to your Mailchimp newsletter.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/mailchimp-results.png" width="1200" height="760" alt="Screenshot of subscribers in Mailchimp" loading="lazy" />
  <figcaption>Add form respondents as subscribers in Mailchimp.</figcaption>
</figure>
<hr/>

<h2 id="what-you-need">What you need</h2>
<p>The scenario we describe in this article is an example of connecting Tripetto to other services, in this case Mailchimp. <a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/">Click here for more general information about connecting Tripetto to other services.</a></p>
<p>For this scenario you need the following:</p>
<ul>
  <li><strong>Tripetto</strong>, to collect your form submissions;</li>
  <li><strong>Make</strong><sup>1</sup>, to connect your form data to other services;</li>
  <li><strong>Mailchimp</strong>, to store your subscribers.</li>
</ul>
<blockquote>
  <h4>🔖 Ad 1: Make alternatives</h4>
  <p>There are alternatives to use as your automation tool. In our example we use Make as our automation tool, but you can also use Zapier, Pabbly Connect or even a custom webhook. Of course it's up to you to use your favorite automation tool.</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-connect-form-responses-to-automation-tools/">How to connect form responses to automation tools</a></li>
  </ul>
</blockquote>
<hr/>

<h2 id="how-to-use">How to use</h2>
<p>These are the steps to take to automate the process of adding subscribers to Mailchimp.</p>

<h3 id="step-1">Step 1 - Build Tripetto form</h3>
<ul>
  <li>First make sure your Tripetto form collects the desired data.</li>
  <li>Please make sure you explicitly ask if your respondent wants to be subscribed. If so, make sure you have an email address field in your form, as we will need that to be able to subscribe a respondent.</li>
</ul>
<p>The result of this step is your Tripetto form, which you will use to collect your data from your respondents.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/tripetto-form.png" width="1200" height="760" alt="Screenshot of Tripetto" loading="lazy" />
  <figcaption>Build your Tripetto form.</figcaption>
</figure>

<h3 id="step-2">Step 2 - Prepare Mailchimp audience</h3>
<ul>
  <li>If you already have an existing Mailchimp audience you can skip this step.</li>
  <li>If you don't have an audience yet, create one and configure it for your purpose.</li>
  <li>Give your audience a name, for example <code>My newsletter subscribers</code>.</li>
</ul>
<p>The result of this step is your Mailchimp audience, that we will use later on in the process.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/mailchimp-audience.png" width="1200" height="760" alt="Screenshot of Mailchimp" loading="lazy" />
  <figcaption>Prepare your Mailchimp audience.</figcaption>
</figure>

<h3 id="step-3">Step 3 - Connect Tripetto to Make</h3>
<ul>
  <li>In your Make account add a new scenario.</li>
  <li>Give your scenario a name, for example <code>Add subscriber for new Tripetto submission</code>.</li>
  <li>Add a <code>Tripetto</code> module to your Make scenario. As trigger select <code>Watch Responses</code>.</li>
  <li>Connect your Tripetto form to that trigger, by copy-pasting the webhook URL from Make into your Tripetto form (navigate to <code><i class="fas fa-share-alt"></i> Automate</code><i class="fas fa-arrow-right"></i><code>Connections</code><i class="fas fa-arrow-right"></i><code>Make</code>). <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-make/">Have a look at this article for the detailed description how to connect Tripetto to Make.</a></li>
  <li>Test the connection, so Make receives the structure of your dataset.</li>
</ul>
<p>The result of this step is that your Make scenario gets executed for each new form submission and you can use the data fields from your Tripetto form in the Make scenario.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/integromat-tripetto.png" width="1200" height="760" alt="Screenshot of Tripetto module in Make" loading="lazy" />
  <figcaption>Add a Tripetto trigger in your Make scenario.</figcaption>
</figure>

<h3 id="step-4">Step 4 - Add subscriber to Mailchimp</h3>
<ul>
  <li>Add a <code>Mailchimp</code> module to your Make scenario. As action select <code>Add/Update a Subscriber</code>.</li>
  <li>Grant access to your Mailchimp account.</li>
  <li>Under <code>List ID</code> select your earlier created Mailchimp audience.</li>
  <li>Make will now recognize the list fields for that audience. For each field you can now select the corresponding data field from your Tripetto form. This is the part where you map the entered data from your Tripetto form to the fields in your Mailchimp audience. Most important field is of course the <code>Email address</code>.</li>
</ul>
<p>The result of this step is you now have automated subscribing a respondent in Mailchimp.</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/integromat-mailchimp.png" width="1200" height="760" alt="Screenshot of Mailchimp module in Make" loading="lazy" />
  <figcaption>Add a Mailchimp module in your Make scenario to add a subscriber.</figcaption>
</figure>

<h3 id="step-5">Step 5 - Test and activate</h3>
<ul>
  <li>Click <code>Run once</code> in Make to start testing your automation.</li>
  <li>Fill out and submit your Tripetto form once.</li>
  <li>Check if your Make scenario has been processed correctly. If so, check the Mailchimp audience to see if the respondent is indeed added as a subscriber.</li>
  <li>Activate your scenario in Make.</li>
</ul>
<p>From now on this scenario will be executed automatically for each new form submission! Every new submission that opted in for your newsletter will now automatically be added to your Mailchimp audience 🎉</p>
<figure>
  <img src="{{ page.base }}images/help/{{ page.article_folder }}/mailchimp-results.png" width="1200" height="760" alt="Screenshot of Mailchimp" loading="lazy" />
  <figcaption>The respondents have been added as subscribers in Mailchimp.</figcaption>
</figure>

<hr />
<h2>Other examples</h2>
<p>Tripetto lets you connect to 1.000+ services. We have made some step-by-step examples for often used scenarios:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-store-form-responses-in-google-sheets/">How to automatically store form responses in Google Sheets and generate analysis reports</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-store-form-responses-in-notion/">How to automatically store form responses in Notion</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-create-PDF-files-from-form-responses/">How to automatically create PDF files from form responses and send these to respondents</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-add-mailchimp-subscribers-from-form-responses/">How to automatically add Mailchimp subscribers from form responses</a><span class="related-current">Current article</span></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-automatically-create-zendesk-support-tickets-from-form-responses/">How to automatically create Zendesk support tickets from form responses</a></li>
</ul>
