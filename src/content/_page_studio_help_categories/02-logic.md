---
layout: help-category
base: ../../../
permalink: /studio/help/using-logic-features/
area: studio
title: Using logic features in the Tripetto studio
description: Learn how to make your forms smart with advanced logic features in the Tripetto studio.
category_area: studio
category_id: logic
category_type: chapter
category_videos: [logic-branch-logic, logic-piping, logic-calculations, logic-prefill]
redirect_from:
- /help/studio/using-logic-features/
---
