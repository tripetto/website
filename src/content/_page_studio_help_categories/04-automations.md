---
layout: help-category
base: ../../../
permalink: /studio/help/automating-things/
area: studio
title: Automating things in the Tripetto studio
description: Learn how to automate actions with your data in the Tripetto studio.
category_area: studio
category_id: automations
category_type: chapter
category_videos: [automations-email, automations-slack, automations-webhook, automations-tracking]
redirect_from:
- /help/studio/automating-things/
---
