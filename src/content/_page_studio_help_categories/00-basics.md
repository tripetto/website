---
layout: help-category
base: ../../../
permalink: /studio/help/basics/
area: studio
title: Basics for the Tripetto studio
description: Learn the basics on the usage of the Tripetto studio.
category_area: studio
category_id: studio-basics
category_type: basics
redirect_from:
- /help/studio/basics/
---
