---
layout: help-category
base: ../../../
permalink: /studio/help/sharing-forms-and-surveys/
area: studio
title: Sharing forms and surveys in the Tripetto studio
description: See how you can distribute your forms from the Tripetto studio to your audience.
category_area: studio
category_id: sharing
category_type: chapter
category_videos: [sharing-studio-link, sharing-studio-embed]
redirect_from:
- /help/studio/run-forms/
- /help/studio/sharing-forms-and-surveys/
---
