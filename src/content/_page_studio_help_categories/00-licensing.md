---
layout: help-category
base: ../../../
permalink: /studio/help/licensing/
area: studio
title: Licensing for the Tripetto studio
description: See the possibilities of licensing for the Tripetto studio.
category_area: studio
category_id: studio-licensing
category_type: basics
redirect_from:
- /help/studio/licensing/
---
