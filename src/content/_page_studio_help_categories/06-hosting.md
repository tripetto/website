---
layout: help-category
base: ../../../
permalink: /studio/help/managing-data-and-results/
area: studio
title: Managing data and results in the Tripetto studio
description: Find out how you can use your response data and get notified of new results in the Tripetto studio.
category_area: studio
category_id: hosting
category_type: chapter
category_videos: [hosting-studio-results]
redirect_from:
- /help/studio/get-results/
- /help/studio/managing-data-and-results/
---
