---
layout: help-category
base: ../../../
permalink: /studio/help/styling-and-customizing/
area: studio
title: Styling and customizing in the Tripetto studio
description: Learn how to customize your form experience, styling and translations in the Tripetto studio.
category_area: studio
category_id: customization
category_type: chapter
category_videos: [customization-style, customization-form-faces, customization-translate]
redirect_from:
- /help/studio/customize-forms/
- /help/studio/styling-and-customization/
- /help/studio/styling-and-customizing/
---
