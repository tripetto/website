---
layout: help-category
base: ../../../
permalink: /studio/help/building-forms-and-surveys/
area: studio
title: Building forms and surveys in the Tripetto studio
description: Get to know how the form builder works in the Tripetto studio, from the simple basics to advanced tutorials.
category_area: studio
category_id: build
category_type: chapter
category_videos: [build-builder,build-simple-form,build-welcome-message, build-closing-message]
redirect_from:
- /help/studio/build-forms/
- /help/studio/building-forms-and-surveys/
---
