---
base: ../
---

<section class="versions-form">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2>Which version do I need?</h2>
        <p>Answer a few simple questions and we'll show you which version suits you best.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-10">
        <div id="TripettoRunner" class="runner-autoscroll-horizontal"></div>
      </div>
    </div>
  </div>
</section>
{% include tripetto-form.html token='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoibFV6Q2ROWXVWYnEvcmZSRkxObERPQ25JcGtEV0VLbkRLUWlRUVNZSzRSMD0iLCJkZWZpbml0aW9uIjoiNlo3ejRHMFhCYkJaSHV3a290aEZuNUd6MWIrZlBRVW5lN1M5TFpEbVpBcz0iLCJ0eXBlIjoiY29sbGVjdCJ9.GVYISFpi_PDsosiDgRWFWp7c-LUuSAWeA-ZYtBULS2Q' %}
