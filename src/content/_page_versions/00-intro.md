---
base: ../
---

<section class="versions-intro block-first intro">
  <div class="container">
    <div class="row">
      <div class="col-md-12 shape-before">
        <h1>Versions</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-11 col-md-9 col-lg-8">
        <p><strong>Tripetto comes in various flavors.</strong> All are full-blown versions with practically identical feature sets. But made for different users and purposes.</p>
        <ul class="hyperlinks">
          <li><a href="#compare" class="hyperlink anchor"><span>Standard versions</span><i class="fas fa-arrow-down"></i></a></li>
          <li><a href="#sdk" class="hyperlink anchor"><span>SDK version</span><i class="fas fa-arrow-down"></i></a></li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
