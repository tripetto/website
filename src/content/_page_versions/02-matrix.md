---
base: ../
---

<section class="versions-matrix matrix matrix-compare" id="compare">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="table-responsive">
          <table class="table matrix-head">
            <thead>
              <tr>
                <th scope="col" class="matrix-nav shape-before">
                  <div>
                    <span role="button" class="matrix-nav-left"><i class="fas fa-chevron-left"></i></span>
                    <span role="button" class="matrix-nav-right"><i class="fas fa-chevron-right"></i></span>
                  </div>
                  <img src="{{ page.base }}images/versions/matrix.svg" alt="Illustration representing the comparison of the different Tripetto versions." width="320" height="224" loading="lazy" />
                </th>
                <th scope="col" class="matrix-area">
                  <div>
                    <div class="palette-studio">
                      <span>Studio</span>
                      <small>Tripetto in the cloud</small>
                      <ul>
                        <li>Available at tripetto.app</li>
                        <li>Store forms and data at Tripetto</li>
                        <li>Free and pay-once per form</li>
                      </ul>
                      <a href="{{ page.base }}studio/pricing/" class="button button-full">Start For Free</a>
                    </div>
                    <a href="{{ page.base }}studio/" class="hyperlink hyperlink-small"><span>About the studio</span><i class="fas fa-arrow-right"></i></a>
                  </div>
                </th>
                <th scope="col" class="matrix-area">
                  <div>
                    <div class="palette-wordpress">
                      <span>Plugin</span>
                      <small>Tripetto inside WordPress</small>
                      <ul>
                        <li>Runs completely inside WordPress</li>
                        <li>Store data inside the plugin only</li>
                        <li>Free and paid subscriptions</li>
                      </ul>
                      <a href="{{ page.base }}wordpress/pricing/" class="button button-full">Download Free Plugin</a>
                    </div>
                    <a href="{{ page.base }}wordpress/" class="hyperlink hyperlink-small"><span>About the plugin</span><i class="fas fa-arrow-right"></i></a>
                  </div>
                </th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
    {% assign groups = site.data.features-matrix-groups | where_exp: "item", "item.show contains 'compare'" %}
    {% for group in groups %}
    {% assign features = site.data.features-matrix | where_exp: "item", "item.group == group.id" | where_exp: "item", "item.show contains 'compare'" %}
    {% if features.size > 0 %}
    <div class="row">
      <div class="col-12">
        <div class="matrix-title"><h2>{{ group.title }}</h2></div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="table-responsive">
          <table class="table matrix-body">
            {% if group.navigation %}
            <thead class="d-md-none">
              <tr>
                <th scope="col" class="matrix-nav">
                  <div class="d-md-none">
                    <span role="button" class="matrix-nav-left"><i class="fas fa-chevron-left"></i></span>
                    <span role="button" class="matrix-nav-right"><i class="fas fa-chevron-right"></i></span>
                  </div>
                </th>
                <th scope="col">
                  <div class="products-label">Tripetto Studio</div>
                </th>
                <th scope="col">
                  <div class="products-label">Tripetto WP Plugin</div>
                </th>
              </tr>
            </thead>
            {% endif %}
            <tbody>
            {% for feature in features %}
              <tr>
                <td class="matrix-body-row"><h3>{% if feature.icon %}{% assign icon_url = "icons/" | append: feature.icon %}{% include {{ icon_url }}.html %}{% endif %}{% if feature.image %}<span><img src="{{ page.base | append: feature.image }}" alt="{{ feature.title }}" /></span>{% endif %}<span>{{ feature.title }}</span></h3>{% if feature.subtitle %}<small>{{ feature.subtitle }}</small>{% endif %}</td>
                <td>
                {% if feature.studio == true %}
                  <div class="matrix-yes"><i class="fas fa-check fa-fw"></i><span>Yes{% if feature.studio_premium == true %}, available after form upgrade{% endif %}</span></div>
                {% elsif feature.studio == false %}
                  <div class="matrix-no"><i class="fas fa-times fa-fw"></i><span>No</span></div>
                {% else %}
                  <div>{{ feature.studio | replace: "[PRICING_STUDIO_UNLOCK]", site.pricing_studio_unlock }}</div>
                {% endif %}
                {% if feature.studio_subtitle %}
                  <small>{{ feature.studio_subtitle }}</small>
                {% endif %}
                {% if feature.studio_premium == true %}
                  <small class="premium"><a href="{{ page.base }}studio/pricing/"><i class="fas fa-crown fa-fw"></i>Get Upgrade</a></small>
                {% endif %}
                </td>
                <td>
                {% if feature.wordpress == true %}
                  <div class="matrix-yes"><i class="fas fa-check fa-fw"></i><span>Yes{% if feature.wordpress_premium == true %}, available in Tripetto Pro{% endif %}</span></div>
                {% elsif feature.wordpress == false %}
                  <div class="matrix-no"><i class="fas fa-times fa-fw"></i><span>No</span></div>
                {% else %}
                  <div>{{ feature.wordpress | replace: "[PRICING_WORDPRESS_SINGLE]", site.pricing_wordpress_single }}</div>
                {% endif %}
                {% if feature.wordpress_subtitle %}
                  <small>{{ feature.wordpress_subtitle }}</small>
                {% endif %}
                {% if feature.wordpress_premium == true %}
                  <small class="premium"><a href="{{ page.base }}wordpress/pricing/"><i class="fas fa-crown fa-fw"></i>Get Tripetto Pro</a></small>
                {% endif %}
                </td>
              </tr>
            {% endfor %}
              <tr class="matrix-cta">
                <th scope="row"></th>
                <td><a href="{{ site.url_app }}" target="_blank">Start for free in studio<i class="fas fa-arrow-right"></i></a></td>
                <td><a href="{{ site.url_wordpress_plugin }}" target="_blank">Download free WordPress plugin<i class="fas fa-arrow-right"></i></a></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    {% endif %}
    {% endfor %}
  </div>
</section>
