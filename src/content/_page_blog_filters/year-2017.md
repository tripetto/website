---
layout: blog-filter
base: ../../
permalink: /blog/2017/
title: Published in 2017 - Tripetto Blog
description: Read Tripetto blog articles published in 2017.
filter_id: year-2017
filter_name: 2017
filter_title: Published in 2017
filter: "item.year==2017"
---
