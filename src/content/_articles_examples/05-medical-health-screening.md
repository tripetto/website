---
layout: examples-article
base: ../../
permalink: /examples/medical-health-screening/
title: Medical health screening - Tripetto Examples
description: Use Tripetto to create a screening tool for a medical health risk calculation and advice.
article_title: Medical Health Screening
article_breadcrumb: Health Screening
article_description: Use Tripetto to create a screening tool for a medical health risk calculation and advice. This example uses the <strong>autoscroll form face</strong> in a horizontal scroll direction (left-right).
example_id: health-screening
example_face: autoscroll
example_face_name: Autoscroll
example_questions: [multiple-choice, matrix, yes-no, paragraph, scale, signature, text-multiple]
example_logic: [branch, iteration, piping, calculator, endings]
url_runner: https://tripetto.app/run/8OGW2PS30R
url_template: https://tripetto.app/template/I8YLOSS8DA
---
