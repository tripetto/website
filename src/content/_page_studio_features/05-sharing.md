---
base: ../../
---

<section class="studio-features-sharing content">
  <div class="container">
    <div class="row">
      <div class="col-sm-7 col-md-8 col-lg-9 shape-before" id="sharing">
        {% include icon-chapter.html chapter='sharing' size='small-medium' name='Easy Sharing' %}
        <span class="caption caption-rotated">Send out</span>
        <h2 class="palette-sharing shape-after">Share where and how you wish with <span>flexible publication.</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-11 col-lg-10 col-xl-9">
        <p>Tripetto lets you choose how to deploy forms and surveys for collecting your data. Could be somewhere on a <strong>shareable URL</strong> or inside a website with the easily <strong>pastable embed code</strong>.</p>
      </div>
    </div>
    <div class="row">
      <div class="col features-sharing-visual">
        <div class="features-sharing-visual-top">
          <a href="{{ page.base }}help/articles/how-to-share-a-link-to-your-form-from-the-studio/" target="_blank" class="features-sharing-option features-sharing-link">
            {% include icons/shareable-link.html %}
          </a>
          <a href="{{ page.base }}help/articles/how-to-embed-a-form-from-the-studio-into-your-website/" target="_blank" class="features-sharing-option features-sharing-embed">
            {% include icons/embed.html %}
          </a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <a href="{{ page.base }}studio/help/sharing-forms-and-surveys/" class="hyperlink palette-sharing"><span>How to share forms and surveys</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
