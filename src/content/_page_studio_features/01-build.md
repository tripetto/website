---
base: ../../
---

<section class="studio-features-build content">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-lg-11 col-xl-10" id="build">
        {% include icon-chapter.html chapter='build' size='small-medium' name='Visual Builder' %}
        <span class="caption caption-rotated">Give shape</span>
        <h2 class="palette-build">Visually build your smart forms on the <span>magnetic storyboard.</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-10 col-xl-10 shape-after">
        <p><strong>Build forms and surveys like flowcharts</strong> on the storyboard, which actively organizes what you create. This makes designing highly conversational flows clean, fast and fun. No-code, promised!</p>
      </div>
    </div>
    <div class="row row-visual">
      <div class="col-12 col-visual">
        <img src="{{ page.base }}images/studio-features/builder.webp" width="1240" height="762" alt="Screenshot of the storyboard and the preview of the form builder." loading="lazy" />
        <div class="features-build-block features-build-builder">
          <img src="{{ page.base }}images/studio-features/build-storyboard.webp" width="1646" height="1524" alt="Screenshot of the storyboard of the form builder." loading="lazy" />
          <p class="content-explanation">The storyboard shows you what goes where and always <strong>actively aligns your arrangement</strong> while you move things around. The <strong>smart zoom</strong> lets you quickly go into and out of any area on the storyboard once your structure grows bigger than the screen. Things just can’t get messy.</p>
          <a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/" class="hyperlink hyperlink-medium palette-build"><span>How to work the storyboard</span><i class="fas fa-arrow-right"></i></a>
        </div>
        <div class="features-build-block features-build-preview">
          <img src="{{ page.base }}images/studio-features/build-preview.webp" width="818" height="1524" alt="Screenshot of the preview of the form builder." loading="lazy" />
          <p class="content-explanation">The <strong>real-time preview</strong> next to the storyboard always instantly shows what you’re building and styling as <strong>a working form or survey</strong>.</p>
          <a href="{{ page.base }}help/articles/how-to-let-the-live-preview-work-for-you/" class="hyperlink hyperlink-medium palette-build"><span>How to preview</span><i class="fas fa-arrow-right"></i></a>
        </div>
      </div>
    </div>
  </div>
</section>
