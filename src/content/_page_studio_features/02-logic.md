---
base: ../../
---

<section class="studio-features-logic content">
  <div class="container">
    <div class="row">
      <div class="col-sm-9 col-lg-11 col-xl-10 shape-before" id="logic">
        {% include icon-chapter.html chapter='logic' size='small-medium' name='Advanced Logic' %}
        <span class="caption caption-rotated">Add brains</span>
        <h2 class="palette-logic shape-after">Create interactive, personable forms with <span>conversational logic.</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 col-lg-9">
        <p>With advanced logic Tripetto lets you build beautifully interactive, conversational forms that are more engaging than ever before, <strong>all without having to write any code.</strong></p>
      </div>
    </div>
  </div>
  <div class="container-fluid ticker-holder ticker-logic">
    <div class="row">
      <ul class="ticker-blocks ticker-blocks-logic-basic">
        {% assign blocks_logic = site.data.blocks-logic | where_exp: "item", "item.group == 'basic'" %}
        {% for item in blocks_logic %}
        <li>
          <a href="{{ page.base | append: item.url }}" target="_blank" class="palette-build">
          {% assign icon_url = "icons/" | append: item.icon %}
            {% include {{ icon_url }}.html %}
            <span>{{ item.name }}</span>
          </a>
        </li>
        {% endfor %}
      </ul>
      <ul class="ticker-blocks ticker-blocks-logic-action">
        {% assign blocks_logic = site.data.blocks-logic | where_exp: "item", "item.group == 'advanced'" %}
        {% for item in blocks_logic %}
        <li>
          <a href="{{ page.base | append: item.url }}" target="_blank" class="palette-logic">
          {% assign icon_url = "icons/" | append: item.icon %}
            {% include {{ icon_url }}.html %}
            <span>{{ item.name }}</span>
          </a>
        </li>
        {% endfor %}
      </ul>
      <ul class="ticker-blocks ticker-blocks-logic-advanced">
        {% assign blocks_logic = site.data.blocks-logic | where_exp: "item", "item.group == 'action'" %}
        {% for item in blocks_logic %}
        <li>
          <a href="{{ page.base | append: item.url }}" target="_blank" class="palette-customization">
          {% assign icon_url = "icons/" | append: item.icon %}
            {% include {{ icon_url }}.html %}
            <span>{{ item.name }}</span>
          </a>
        </li>
        {% endfor %}
      </ul>
    </div>
  </div>
  <div class="container container-bottom">
    <div class="row">
      <div class="col-md-4">
        <p class="content-explanation">The visual storyboard for building forms and surveys was specifically designed to <strong>spark and simplify using advanced logic</strong> features.</p>
        <a href="{{ page.base }}help/articles/how-to-make-your-forms-smart-and-conversational/#logic" class="hyperlink hyperlink-medium palette-logic"><span>How to use logic</span><i class="fas fa-arrow-right"></i></a>
      </div>
      <div class="col-md-4">
        <p class="content-explanation">Besides the questions that require input from respondents, forms and surveys can <strong>perform powerful real time actions</strong> in the background.</p>
        <a href="{{ page.base }}help/articles/how-to-make-your-forms-smart-and-conversational/#actions" class="hyperlink hyperlink-medium palette-logic"><span>How to set actions</span><i class="fas fa-arrow-right"></i></a>
      </div>
      <div class="col-md-4">
        <p class="content-explanation">Possibly the most powerful logic feature in Tripetto is the <strong>calculator for creating tests, quizzes, quotes, assessments</strong> and so much more.</p>
        <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/" class="hyperlink hyperlink-medium palette-logic"><span>How to calculate</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
