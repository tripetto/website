---
base: ../../
---

<section class="studio-features-hosting content">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-lg-10 col-xl-9 shape-before" id="hosting">
        {% include icon-chapter.html chapter='hosting' size='small-medium' name='Flexible Storage' %}
        <span class="caption caption-rotated">Take stock</span>
        <h2 class="palette-hosting">Where forms and data live is essential.<span>Storage freedom, too.</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-11 col-lg-10">
        <p><strong>You decide where your data is stored.</strong> Could be at Tripetto. But maybe you want to self-host forms and the data you collect elsewhere, bypassing Tripetto entirely. That’s cool, too!</p>
      </div>
    </div>
    <div class="row">
      <div class="col product-hosting-visual">
        <div>
          <div class="product-hosting-option product-hosting-cloud">
            <h3>We host</h3>
            <p>The forms and surveys you build and data you collect <strong>are all stored in your own Tripetto studio account</strong>. You can always extract your work and results.</p>
            <img src="{{ page.base }}images/studio-features/hosting-cloud.svg" alt="Human illustrating hosting at Tripetto" />
          </div>
        </div>
        <div>
          <div class="product-hosting-option product-hosting-self">
            <h3>Self-host</h3>
            <p>You decide what you want to store outside of Tripetto and where. <strong>Self-hosted data never reaches Tripetto servers at all.</strong> <a href="{{ page.base }}blog/dont-trust-someone-else-with-your-form-data/">Read our blog on GDPR!</a></p>
            <img src="{{ page.base }}images/studio-features/hosting-self.svg" alt="Human illustrating self-hosting" />
          </div>
          <span>BTW, you can also do both!</span>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <a href="{{ page.base }}studio/help/managing-data-and-results/" class="hyperlink palette-hosting"><span>How to manage form and survey data</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
