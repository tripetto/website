---
base: ../
---

<section class="runner-referral-mosaic content">
  <div class="container container-content">
    <div class="row">
      <div class="col-lg-10 col-xl-9 shape-before shape-after">
        <h2><span>Enter Tripetto.</span> Wake-up call for form tool dinosaurs.</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-11 col-xl-10">
        <p>We merged the best of SurveyMonkey, Typeform and Landbot into a single conversational solution to let you build beautiful forms and surveys that <strong>boost completion rates for better insights</strong>.</p>
      </div>
    </div>
  </div>
  <div class="container container-mosaic">
    <div class="row">
      <div class="col mosaic shape-before shape-after">
        <div class="mosaic-block mosaic-block-horizontal palette-build">
          <div>
            <h3>Magnetic Storyboard</h3>
            <p>Build smart forms and surveys like flowcharts, with drag-and-drop.</p>
          </div>
        </div>
        <div class="mosaic-block mosaic-block-horizontal palette-logic">
          <div>
            <h3>Conversational Logic Types</h3>
            <p>Build seriously conversational flows with logic, actions and calculations. <strong>All no-code!</strong></p>
          </div>
        </div>
        <div class="mosaic-block mosaic-block-horizontal palette-customization">
          <div>
            <h3>Convertible Form Faces</h3>
            <p>Pick a Typeform-like, a chat or a classic form layout. And switch instantly.</p>
          </div>
        </div>
        <div class="mosaic-block mosaic-block-vertical palette-automations">
          <div>
            <h3>Fast, No-Code Automations</h3>
            <p>Receive <strong>notifications</strong>, <strong>connect to 1.000+ services</strong> and <strong>track form activity</strong>.</p>
          </div>
        </div>
        <div class="mosaic-block mosaic-block-vertical palette-sharing">
          <div>
            <h3>Flexible Publication</h3>
            <p>Share a simple link or embed forms neatly on your page.</p>
          </div>
          <img src="{{ page.base }}images/chapters/sharing.svg" alt="Illustration representing flexible publication" />
        </div>
        <div class="mosaic-block mosaic-block-vertical palette-hosting">
          <div>
            <h3>Data Storage Autonomy</h3>
            <p>Store things at Tripetto or self-host wherever you prefer. <strong>Hello GDPR!</strong></p>
          </div>
          <img src="{{ page.base }}images/chapters/hosting.svg" alt="Illustration representing data storage freedom" />
        </div>
      </div>
    </div>
  </div>
</section>
