---
base: ../
reviews: [remrick,mohit,muyiwa,shirshendu,mike_monty,ari0,danfadida,kgjermani,michaelp,joaquinreyes,markxkr,prestonesto]
---

{% if page.reviews %}
{% assign reviews_count = 0 %}
<section class="runner-referral-reviews content reviews">
  <div class="container-fluid ticker-holder ticker-reviews">
    <div class="row">
      <ul class="ticker-blocks-reviews reviews">
          {% for review in page.reviews %}
          {% assign review_item = site.data.reviews[review] %}
          <li>
            <div class="review-palette-{{ reviews_count }}">
              <div class="review-rating">
              {% for i in (1..review_item.rating) %}
                <i class="fas fa-star"></i>
              {% endfor %}
              </div>
              <div class="review-meta">
                <span>{{ review_item.author }}</span>
                <small>{{ review_item.date }}</small>
              </div>
              <a href="{{ review_item.url }}" target="_blank" rel="noopener noreferrer"><h4>{{ review_item.title }}</h4></a>
              <p>{{ review_item.text }}</p>
            </div>
          </li>
          {% assign reviews_count = reviews_count | plus: 1 %}
          {% if reviews_count == 3 %}
            {% assign reviews_count = 0 %}
          {% endif %}
          {% endfor %}
      </ul>
    </div>
  </div>
</section>
{% endif %}
