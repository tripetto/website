---
base: ../../
---

<nav class="breadcrumb-navigation block-first" aria-label="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col">
        <ol>
          <li><a href="{{ page.base }}wordpress/">Tripetto WordPress plugin</a></li>
          <li><span>Black Friday Lifetime Deal</span></li>
        </ol>
      </div>
    </div>
  </div>
</nav>

<section class="wordpress-checkout-confirmation-hero intro intro-breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <h1>Thank You!</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 col-lg-6">
        <p>Your license has successfully been <strong>converted to a lifetime license</strong> without recurring payments!</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-9 col-xl-8">
        <ul class="fa-ul list-arrows">
          <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><span class="list-arrows-text">Your Tripetto Pro license is now a lifetime license</span></li>
          <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><span class="list-arrows-text">The existing license key remains unchanged and active</span></li>
          <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><span class="list-arrows-text">Your recurring subscription was automatically cancelled</span></li>
          <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><span class="list-arrows-text">A receipt of your LTD purchase has been sent to your inbox</span></li>
        </ul>
      </div>
    </div>
  </div>
</section>
