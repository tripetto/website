---
base: ../../
---


<section class="wordpress-feature-list-matrix matrix">
  <div class="container">
    {% assign groups = site.data.features-matrix-groups | where_exp: "item", "item.show contains 'wordpress'" %}
    {% for group in groups %}
    {% assign features = site.data.features-matrix | where_exp: "item", "item.group == group.id" | where_exp: "item", "item.show contains 'wordpress'" %}
    {% if features.size > 0 %}
    <div class="row">
      <div class="col-12">
        <div class="matrix-title"><h2>{{ group.title }}</h2></div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="table-responsive">
          <table class="table matrix-body">
            <tbody>
            {% for feature in features %}
              <tr>
                <td class="matrix-body-row"><h3>{% if feature.icon %}{% assign icon_url = "icons/" | append: feature.icon %}{% include {{ icon_url }}.html %}{% endif %}{% if feature.image %}<span><img src="{{ page.base | append: feature.image }}" alt="{{ feature.title }}" /></span>{% endif %}<span>{{ feature.title }}</span></h3>{% if feature.subtitle %}<small>{{ feature.subtitle }}</small>{% endif %}</td>
                <td>
                {% if feature.wordpress == true %}
                  <div class="matrix-yes"><i class="fas fa-check fa-fw"></i><span>Yes{% if feature.wordpress_premium == true %}, available in Tripetto Pro{% endif %}</span></div>
                {% elsif feature.wordpress == false %}
                  <div class="matrix-no"><i class="fas fa-times fa-fw"></i><span>No</span></div>
                {% else %}
                  <div>{{ feature.wordpress | replace: "[PRICING_WORDPRESS_SINGLE]", site.pricing_wordpress_single }}</div>
                {% endif %}
                {% if feature.wordpress_subtitle %}
                  <small>{{ feature.wordpress_subtitle }}</small>
                {% endif %}
                {% if feature.wordpress_premium == true %}
                  <small class="premium"><a href="{{ page.base }}wordpress/pricing/"><i class="fas fa-crown fa-fw"></i>Get Tripetto Pro</a></small>
                {% endif %}
                </td>
              </tr>
            {% endfor %}
            </tbody>
          </table>
        </div>
      </div>
    </div>
    {% endif %}
    {% endfor %}
  </div>
</section>
