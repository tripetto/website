---
base: ../../
---

<nav class="breadcrumb-navigation block-first" aria-label="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col">
        <ol>
          <li><a href="{{ page.base }}wordpress/">Tripetto WordPress plugin</a></li>
          <li><span>Feature list</span></li>
        </ol>
      </div>
    </div>
  </div>
</nav>

<section class="wordpress-feature-list-intro intro">
  <div class="container">
    <div class="row">
      <div class="col-md-12 shape-before">
        <h1>Plugin Features</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-10 col-md-8 col-lg-7">
        <p>The Tripetto WordPress plugin comes with everything you need for building beautiful and customizable forms, surveys, quizzes, and more. <strong>All right inside your WordPress.</strong></p>
      </div>
    </div>
  </div>
</section>
