---
base: ../../
---

<section class="sdk-producthunt-mosaic content">
  <div class="container container-content">
    <div class="row">
      <div class="col-md-10 col-xl-9">
        <h2><span class="palette-inline">Every part of Tripetto</span> neatly packed into your application.</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 col-lg-9">
        <p>The SDK's parts for form creation and deployment are designed for quick embedding in straightforward cases as well as <strong>deep integration into advanced applications.</strong></p>
      </div>
    </div>
  </div>
  <div class="container container-mosaic">
    <div class="row">
      <div class="col sdk-mosaic">
        <a href="{{ page.base }}sdk/components/#builder" class="mosaic-block mosaic-block-horizontal mosaic-block-builder">
          <div>
            <h3>Full-fledged Form Builder</h3>
            <p>Build smart forms and surveys like flowcharts on the magnetic drag-and-drop storyboard.</p>
          </div>
        </a>
        <a href="{{ page.base }}sdk/components/#runners" class="mosaic-block mosaic-block-vertical mosaic-block-runners">
          <div>
            <h3>Powerful Form Runners</h3>
            <p>Let the all-in-one runner execute your forms and handle UI rendering, UX, logic and response delivery.</p>
          </div>
        </a>
        <a href="{{ page.base }}sdk/components/#blocks" class="mosaic-block mosaic-block-vertical mosaic-block-blocks">
          <div>
            <h3>Customizable Question Blocks</h3>
            <p>Deeply customize things for the optimal integration into your website or application.</p>
          </div>
        </a>
        <div class="mosaic-block mosaic-block-small mosaic-block-black mosaic-block-documented">
          <div>
            <h3>Trusted, richly documented tech.</h3>
            <p>You’re in good company. The SDK’s components are <strong>used and trusted by Fortune 500 companies.</strong></p>
          </div>
        </div>
        <div class="mosaic-block mosaic-block-horizontal-wide mosaic-block-bordered mosaic-block-typescript">
          <div>
            {% include icons/typescript.html %}
            <h3>TypeScript typings included.</h3>
            <p>All SDK packages include type declarations and support <strong>TypeScript out of the box.</strong></p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container container-hyperlink">
    <div class="row">
      <div class="col-12"><a href="{{ page.base }}sdk/components/" class="hyperlink"><span>Learn more about the SDK components</span><i class="fas fa-arrow-right"></i></a></div>
    </div>
  </div>
</section>
