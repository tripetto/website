---
base: ../../
---

<section class="wordpress-ltd-plans pricing-plans-columns">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <small class="pricing-guarantee">Our 100% Unconditional Money Back Guarantee <strong>does not apply</strong> to lifetime deals.</small>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4">
        <div class="pricing-plan">
          <h2>Single-Site</h2>
          <div class="pricing-price palette-dark">
            <div class="pricing-price-prefix">
              <div class="valuta">$</div>
              <div class="amount amount-small">{{ site.pricing_wordpress_single_ltd }}</div>
              <div class="conditions">
                <span class="conditions-title">Lifetime license</span>
                <span class="conditions-subtitle">Pay-once</span>
                <span class="conditions-subtitle">1 WP site</span>
              </div>
            </div>
          </div>
          <span class="button button-full button-red button-disabled button-purchase-pro-single" role="button" data-toggle="tooltip" data-placement="top" title="Enter your license key first">Buy Single-Site LTD</span>
          <p><strong>Equip 1 WordPress site with Tripetto Pro.</strong> Pro plans include all features and priority support.</p>
          <ul class="pricing-features pricing-features-grouped">
            <li class="featured included">Unlimited form building
              <ul class="pricing-features collapse pricing-features-collapse" id="proSingleBuilding">
                <li class="included">Unlimited forms</li>
                <li class="included">Unlimited subforms</li>
                <li class="included">Unlimited questions</li>
                <li class="included">Unlimited responses</li>
              </ul>
            </li>
            <li class="featured included">Unlimited form logic
              <ul class="pricing-features collapse pricing-features-collapse" id="proSingleLogic">
                <li class="included">Branch logic</li>
                <li class="included">Jump logic</li>
                <li class="included">Pipe logic</li>
              </ul>
            </li>
            <li class="featured included">All layout features
              <ul class="pricing-features collapse pricing-features-collapse" id="proSingleLayout">
                <li class="included">Autoscroll form face</li>
                <li class="included">Chat form face</li>
                <li class="included">Classic form face</li>
                <li class="included">Rich styling features</li>
                <li class="included">Label translations</li>
              </ul>
            </li>
            <li class="featured included">Action blocks
              <ul class="pricing-features collapse pricing-features-collapse" id="proSingleActions">
                <li class="included">Calculator</li>
                <li class="included">Custom variable</li>
                <li class="included">Force stop</li>
                <li class="included">Hidden field</li>
                <li class="included">Raise error</li>
                <li class="included">Send email</li>
                <li class="included">Set value</li>
              </ul>
            </li>
            <li class="featured included">Notifications
              <ul class="pricing-features collapse pricing-features-collapse" id="proSingleNotifications">
                <li class="included">Email notifications</li>
                <li class="included">Slack notifications</li>
              </ul>
            </li>
            <li class="featured included">Connections
              <ul class="pricing-features collapse pricing-features-collapse" id="proSingleConnections">
                <li class="included">Make (formerly Integromat)</li>
                <li class="included">Zapier</li>
                <li class="included">Pabbly Connect</li>
                <li class="included">Custom webhooks</li>
              </ul>
            </li>
            <li class="featured included">Activity tracking
              <ul class="pricing-features collapse pricing-features-collapse" id="proSingleTracking">
                <li class="included">Google Analytics</li>
                <li class="included">Google Tag Manager</li>
                <li class="included">Facebook Pixel</li>
                <li class="included">Custom tracking codes</li>
              </ul>
            </li>
            <li class="featured included">Tripetto unbranding
              <ul class="pricing-features collapse pricing-features-collapse" id="proSingleUnbranding">
                <li class="included">No Tripetto branding in forms</li>
                <li class="included">No Tripetto branding in emails</li>
              </ul>
            </li>
            <li class="featured included">WordPress roles management
              <ul class="pricing-features collapse pricing-features-collapse" id="proSingleRoles">
                <li class="included">User role access settings</li>
                <li class="included">User role capabilities settings</li>
              </ul>
            </li>
            <li class="featured included">Basic support
              <ul class="pricing-features collapse pricing-features-collapse" id="proSingleSupportBasic">
                <li class="included">Access to <a href="{{ page.base }}help/" target="_blank">help center</a><small>[24/7]</small></li>
                <li class="included"><a href="{{ page.base }}help/video-tutorials/" target="_blank">Video tutorials</a></li>
                <li class="included">Plugin updates</li>
              </ul>
            </li>
            <li class="featured included">Priority services
              <ul class="pricing-features collapse pricing-features-collapse" id="proSingleSupportPrio">
                <li class="included">Priority support<small>[Mon-Fri, 9-17 CET]</small></li>
                <li class="included">Plugin updates and upgrades</li>
              </ul>
            </li>
          </ul>
          <span class="hyperlink hyperlink-small collapsed" role="button" data-toggle="collapse" data-target=".pricing-features-collapse" aria-expanded="false"><span>View all features</span><i class="fas fa-arrow-up"></i></span>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="pricing-plan">
          <h2>5-Sites</h2>
          <div class="pricing-price palette-dark">
            <div class="pricing-price-prefix">
              <div class="valuta">$</div>
              <div class="amount amount-small">{{ site.pricing_wordpress_multi_ltd }}</div>
              <div class="conditions">
                <span class="conditions-title">Lifetime license</span>
                <span class="conditions-subtitle">Pay-once</span>
                <span class="conditions-subtitle">Up to 5 WP sites</span>
              </div>
            </div>
          </div>
          <span class="button button-full button-red button-disabled button-purchase-pro-multi" role="button" data-toggle="tooltip" data-placement="top" title="Enter your license key first">Buy 5-Sites LTD</span>
          <p><strong>Equip up to 5 WordPress sites with Tripetto Pro.</strong> Pro plans include all features and priority support.</p>
          <ul class="pricing-features pricing-features-grouped">
            <li class="featured included">Unlimited form building
              <ul class="pricing-features collapse pricing-features-collapse" id="proMultiBuilding">
                <li class="included">Unlimited forms</li>
                <li class="included">Unlimited subforms</li>
                <li class="included">Unlimited questions</li>
                <li class="included">Unlimited responses</li>
              </ul>
            </li>
            <li class="featured included">Unlimited form logic
              <ul class="pricing-features collapse pricing-features-collapse" id="proMultiLogic">
                <li class="included">Branch logic</li>
                <li class="included">Jump logic</li>
                <li class="included">Pipe logic</li>
              </ul>
            </li>
            <li class="featured included">All layout features
              <ul class="pricing-features collapse pricing-features-collapse" id="proMultiLayout">
                <li class="included">Autoscroll form face</li>
                <li class="included">Chat form face</li>
                <li class="included">Classic form face</li>
                <li class="included">Rich styling features</li>
                <li class="included">Label translations</li>
              </ul>
            </li>
            <li class="featured included">Action blocks
              <ul class="pricing-features collapse pricing-features-collapse" id="proMultiActions">
                <li class="included">Calculator</li>
                <li class="included">Custom variable</li>
                <li class="included">Force stop</li>
                <li class="included">Hidden field</li>
                <li class="included">Raise error</li>
                <li class="included">Send email</li>
                <li class="included">Set value</li>
              </ul>
            </li>
            <li class="featured included">Notifications
              <ul class="pricing-features collapse pricing-features-collapse" id="proMultiNotifications">
                <li class="included">Email notifications</li>
                <li class="included">Slack notifications</li>
              </ul>
            </li>
            <li class="featured included">Connections
              <ul class="pricing-features collapse pricing-features-collapse" id="proMultiConnections">
                <li class="included">Make (formerly Integromat)</li>
                <li class="included">Zapier</li>
                <li class="included">Pabbly Connect</li>
                <li class="included">Custom webhooks</li>
              </ul>
            </li>
            <li class="featured included">Activity tracking
              <ul class="pricing-features collapse pricing-features-collapse" id="proMultiTracking">
                <li class="included">Google Analytics</li>
                <li class="included">Google Tag Manager</li>
                <li class="included">Facebook Pixel</li>
                <li class="included">Custom tracking codes</li>
              </ul>
            </li>
            <li class="featured included">Tripetto unbranding
              <ul class="pricing-features collapse pricing-features-collapse" id="proMultiUnbranding">
                <li class="included">No Tripetto branding in forms</li>
                <li class="included">No Tripetto branding in emails</li>
              </ul>
            </li>
            <li class="featured included">WordPress roles management
              <ul class="pricing-features collapse pricing-features-collapse" id="proMultiRoles">
                <li class="included">User role access settings</li>
                <li class="included">User role capabilities settings</li>
              </ul>
            </li>
            <li class="featured included">Basic support
              <ul class="pricing-features collapse pricing-features-collapse" id="proMultiSupportBasic">
                <li class="included">Access to <a href="{{ page.base }}help/" target="_blank">help center</a><small>[24/7]</small></li>
                <li class="included"><a href="{{ page.base }}help/video-tutorials/" target="_blank">Video tutorials</a></li>
                <li class="included">Plugin updates</li>
              </ul>
            </li>
            <li class="featured included">Priority services
              <ul class="pricing-features collapse pricing-features-collapse" id="proMultiSupportPrio">
                <li class="included">Priority support<small>[Mon-Fri, 9-17 CET]</small></li>
                <li class="included">Plugin updates and upgrades</li>
              </ul>
            </li>
          </ul>
          <span class="hyperlink hyperlink-small collapsed" role="button" data-toggle="collapse" data-target=".pricing-features-collapse" aria-expanded="false"><span>View all features</span><i class="fas fa-arrow-up"></i></span>
        </div>
      </div>
      <div class="col-lg-4">
        <div class="pricing-plan">
          <h2>Unlimited</h2>
          <div class="pricing-price palette-dark">
            <div class="pricing-price-prefix">
              <div class="valuta">$</div>
              <div class="amount amount-small">{{ site.pricing_wordpress_unlimited_ltd }}</div>
              <div class="conditions">
                <span class="conditions-title">Lifetime license</span>
                <span class="conditions-subtitle">Pay-once</span>
                <span class="conditions-subtitle">Unlimited WP sites</span>
              </div>
            </div>
          </div>
          <span class="button button-full button-red button-disabled button-purchase-pro-unlimited" role="button" data-toggle="tooltip" data-placement="top" title="Enter your license key first">Buy Unlimited LTD</span>
          <p><strong>Equip unlimited WordPress sites with Tripetto Pro.</strong> Pro plans include all features and priority support.</p>
          <ul class="pricing-features pricing-features-grouped">
            <li class="featured included">Unlimited form building
              <ul class="pricing-features collapse pricing-features-collapse" id="proUnlimitedBuilding">
                <li class="included">Unlimited forms</li>
                <li class="included">Unlimited subforms</li>
                <li class="included">Unlimited questions</li>
                <li class="included">Unlimited responses</li>
              </ul>
            </li>
            <li class="featured included">Unlimited form logic
              <ul class="pricing-features collapse pricing-features-collapse" id="proUnlimitedLogic">
                <li class="included">Branch logic</li>
                <li class="included">Jump logic</li>
                <li class="included">Pipe logic</li>
              </ul>
            </li>
            <li class="featured included">All layout features
              <ul class="pricing-features collapse pricing-features-collapse" id="proUnlimitedLayout">
                <li class="included">Autoscroll form face</li>
                <li class="included">Chat form face</li>
                <li class="included">Classic form face</li>
                <li class="included">Rich styling features</li>
                <li class="included">Label translations</li>
              </ul>
            </li>
            <li class="featured included">Action blocks
              <ul class="pricing-features collapse pricing-features-collapse" id="proUnlimitedActions">
                <li class="included">Calculator</li>
                <li class="included">Custom variable</li>
                <li class="included">Force stop</li>
                <li class="included">Hidden field</li>
                <li class="included">Raise error</li>
                <li class="included">Send email</li>
                <li class="included">Set value</li>
              </ul>
            </li>
            <li class="featured included">Notifications
              <ul class="pricing-features collapse pricing-features-collapse" id="proUnlimitedNotifications">
                <li class="included">Email notifications</li>
                <li class="included">Slack notifications</li>
              </ul>
            </li>
            <li class="featured included">Connections
              <ul class="pricing-features collapse pricing-features-collapse" id="proUnlimitedConnections">
                <li class="included">Make (formerly Integromat)</li>
                <li class="included">Zapier</li>
                <li class="included">Pabbly Connect</li>
                <li class="included">Custom webhooks</li>
              </ul>
            </li>
            <li class="featured included">Activity tracking
              <ul class="pricing-features collapse pricing-features-collapse" id="proUnlimitedTracking">
                <li class="included">Google Analytics</li>
                <li class="included">Google Tag Manager</li>
                <li class="included">Facebook Pixel</li>
                <li class="included">Custom tracking codes</li>
              </ul>
            </li>
            <li class="featured included">Tripetto unbranding
              <ul class="pricing-features collapse pricing-features-collapse" id="proUnlimitedUnbranding">
                <li class="included">No Tripetto branding in forms</li>
                <li class="included">No Tripetto branding in emails</li>
              </ul>
            </li>
            <li class="featured included">WordPress roles management
              <ul class="pricing-features collapse pricing-features-collapse" id="proUnlimitedRoles">
                <li class="included">User role access settings</li>
                <li class="included">User role capabilities settings</li>
              </ul>
            </li>
            <li class="featured included">Basic support
              <ul class="pricing-features collapse pricing-features-collapse" id="proUnlimitedSupportBasic">
                <li class="included">Access to <a href="{{ page.base }}help/" target="_blank">help center</a><small>[24/7]</small></li>
                <li class="included"><a href="{{ page.base }}help/video-tutorials/" target="_blank">Video tutorials</a></li>
                <li class="included">Plugin updates</li>
              </ul>
            </li>
            <li class="featured included">Priority services
              <ul class="pricing-features collapse pricing-features-collapse" id="proUnlimitedSupportPrio">
                <li class="included">Priority support<small>[Mon-Fri, 9-17 CET]</small></li>
                <li class="included">Plugin updates and upgrades</li>
              </ul>
            </li>
          </ul>
          <span class="hyperlink hyperlink-small collapsed" role="button" data-toggle="collapse" data-target=".pricing-features-collapse" aria-expanded="false"><span>View all features</span><i class="fas fa-arrow-up"></i></span>
        </div>
      </div>
    </div>
  </div>
</section>
