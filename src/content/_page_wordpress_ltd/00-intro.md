---
base: ../../
---

<nav class="breadcrumb-navigation block-first" aria-label="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col">
        <ol>
          <li><a href="{{ page.base }}wordpress/">Tripetto WordPress plugin</a></li>
          <li><span>Lifetime Deals</span></li>
        </ol>
      </div>
    </div>
  </div>
</nav>

<section class="wordpress-ltd-intro intro">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h1>This week only: Lifetime Deals.</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-sm-11 col-md-8 col-lg-7">
        <p>Get rid of your recurring Tripetto subscription. Buy it off permanently now with this <strong>exclusive Lifetime Deal</strong>. We normally don’t offer LTD’s, so this week is your only chance!</p>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
