---
base: ../../
---

<section class="wordpress-ltd-instructions">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2>Get Your Lifetime Deal</h2>
        <ul class="fa-ul list-arrows">
          <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><span class="list-arrows-text">Enter your license key</span></li>
          <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><span class="list-arrows-text">Select the LTD of your choice</span></li>
          <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><span class="list-arrows-text">Your last payment is prorated automatically</span></li>
          <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><span class="list-arrows-text">The recurring subscription is cancelled automatically</span></li>
          <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><span class="list-arrows-text">Your license is changed to an LTD <u>without recurring payments</U> 🚀</span></li>
        </ul>
      </div>
    </div>
    <div class="row wordpress-ltd-instructions-license">
      <div class="col-12">
        <small>Find your valid license key in your WP Admin or login to your <a href='https://users.freemius.com/' target='_blank'>Freemius user dashboard</a>.</small>
      </div>
      <div class="col-12 col-lg-8 col-xl-7">
        <form action="" method="get" class="form-inline" id="freemius-license">
          <label class="sr-only" for="freemius-license-key">Enter your current license key</label>
          <input type="text" name="freemius-license-key" autocapitalize="none" placeholder="Enter your current license key" class="form-input" id="freemius-license-key" />
          <button class="button button-red"><span>Check License</span></button>
        </form>
      </div>
      <div class="col-12 col-lg-4 col-xl-5 wordpress-ltd-instructions-license-validation">
        <span id="freemius-license-validation"></span>
      </div>
    </div>
  </div>
</section>
