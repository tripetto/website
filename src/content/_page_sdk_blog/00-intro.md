---
base: ../../
---

<section class="sdk-blog-intro block-first sdk-intro">
  <div class="container">
    <div class="row">
      <div class="col-sm-10 col-md-9 col-lg-8 col-xl-8">
        <h1>FormBuilder SDK Blog</h1>
        <p>The FormBuilder SDK blog offers useful coding tutorials, showcases, background stories, and more to assist you with easily <strong>implementing the SDK in your app or site.</strong></p>
        <a href="{{ page.base }}subscribe/" class="hyperlink" target="_blank"><span>Subscribe to product news</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
