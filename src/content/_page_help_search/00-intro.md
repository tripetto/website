---
base: ../../
---

<nav class="breadcrumb-navigation block-first" aria-label="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col">
        <ol>
          <li><a href="{{ page.base }}help/">Help Center</a></li>
          <li><span>Search</span></li>
        </ol>
      </div>
    </div>
  </div>
</nav>

<section class="help-intro">
  <div class="container">
    <div class="row">
      <div class="col">
        <h1>Search for help</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-9 col-xl-8">
        <form action="{{ page.base }}help/search/" method="get" class="help-search form-inline">
          <label class="sr-only" for="search-input">Search all help articles</label>
          <input type="text" name="q" class="form-input form-input-big" id="search-input" placeholder="Search all help articles" />
          <button class="button button-big"><i class="fas fa-search"></i><span>Search</span></button>
        </form>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
