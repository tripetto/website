---
base: ../../
---

<section class="studio-feature-list-matrix matrix">
  <div class="container">
    {% assign groups = site.data.features-matrix-groups | where_exp: "item", "item.show contains 'studio'" %}
    {% for group in groups %}
    {% assign features = site.data.features-matrix | where_exp: "item", "item.group == group.id" | where_exp: "item", "item.show contains 'studio'" %}
    {% if features.size > 0 %}
    <div class="row">
      <div class="col-12">
        <div class="matrix-title"><h2>{{ group.title }}</h2></div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="table-responsive">
          <table class="table matrix-body">
            <tbody>
            {% for feature in features %}
              <tr>
                <td class="matrix-body-row"><h3>{% if feature.icon %}{% assign icon_url = "icons/" | append: feature.icon %}{% include {{ icon_url }}.html %}{% endif %}{% if feature.image %}<span><img src="{{ page.base | append: feature.image }}" alt="{{ feature.title }}" /></span>{% endif %}<span>{{ feature.title }}</span></h3>{% if feature.subtitle %}<small>{{ feature.subtitle }}</small>{% endif %}</td>
                <td>
                {% if feature.studio == true %}
                  <div class="matrix-yes"><i class="fas fa-check fa-fw"></i><span>Yes{% if feature.studio_premium == true %}, available after form upgrade{% endif %}</span></div>
                {% elsif feature.studio == false %}
                  <div class="matrix-no"><i class="fas fa-times fa-fw"></i><span>No</span></div>
                {% else %}
                  <div>{{ feature.studio | replace: "[PRICING_STUDIO_UNLOCK]", site.pricing_studio_unlock }}</div>
                {% endif %}
                {% if feature.studio_subtitle %}
                  <small>{{ feature.studio_subtitle }}</small>
                {% endif %}
                {% if feature.studio_premium == true %}
                  <small class="premium"><a href="{{ page.base }}studio/pricing/"><i class="fas fa-crown fa-fw"></i>Get Upgrade</a></small>
                {% endif %}
                </td>
              </tr>
            {% endfor %}
            </tbody>
          </table>
        </div>
      </div>
    </div>
    {% endif %}
    {% endfor %}
  </div>
</section>
