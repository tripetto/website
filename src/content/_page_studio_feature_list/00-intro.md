---
base: ../../
---

<nav class="breadcrumb-navigation block-first" aria-label="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col">
        <ol>
          <li><a href="{{ page.base }}studio/">Tripetto studio</a></li>
          <li><span>Feature list</span></li>
        </ol>
      </div>
    </div>
  </div>
</nav>

<section class="studio-feature-list-intro intro">
  <div class="container">
    <div class="row">
      <div class="col-md-12 shape-before">
        <h1>Studio Features</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-10 col-md-8 col-lg-7">
        <p>The studio brings everything Tripetto has to offer for building attractive and deeply customizable forms, surveys, quizzes, and more. <strong>All straight from the Tripetto cloud.</strong></p>
      </div>
    </div>
  </div>
</section>
