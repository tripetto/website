---
base: ../
---

<section class="form-faces-face form-faces-face-autoscroll content">
  <div class="container container-content" id="autoscroll">
    <div class="row">
      <div class="col-12 form-faces-face-intro">
        <div>{% include icon-face.html face='autoscroll' size='xl' name='Autoscroll' template='background' radius='large' %}</div>
        <div class="form-faces-face-title">
          <span class="caption caption-rotated">Conversational layout</span>
          <h2>Autoscroll</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-9 col-lg-8">
        <p>The autoscroll form face <strong>presents your questions in a one-by-one layout</strong>, giving it a modern feeling and optimal focus for inputs. Respondents will thoroughly experience your form’s smartness and finesse.</p>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row form-face">
      <div class="col-lg-8">
        <img src="{{ page.base }}images/scenes/autoscroll-fitness-registration.webp" width="2220" height="1435" alt="Screenshots of a fitness registration form in the autoscroll form face, shown on a tablet and a mobile phone." loading="lazy" />
      </div>
      <div class="col-lg-4 form-face-list">
        <h3>Styling options</h3>
        <p class="content-explanation">The autoscroll face is akin to <strong>Typeform’s conversational format</strong> and lets you tweak:</p>
        <ul class="palette-autoscroll">
          <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/fonts.html %}Fonts</a></li>
          <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/colors.html %}Colors</a></li>
          <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/buttons.html %}Buttons</a></li>
          <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/backgrounds.html %}Backgrounds</a></li>
          <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/input-controls.html %}Input Controls</a></li>
          <li><a href="{{ page.base }}help/articles/discover-additional-options-for-autoscroll-form-face/">{% include icons/scroll-direction.html %}Scroll Direction</a></li>
          <li><a href="{{ page.base }}help/articles/how-to-edit-or-translate-all-text-labels-in-your-forms/">{% include icons/translations.html %}Labels/Translations</a></li>
          <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/remove-branding.html %}Hide Tripetto branding</a></li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <a href="{{ page.base }}help/articles/discover-additional-options-for-autoscroll-form-face/" class="hyperlink palette-autoscroll"><span>How to customize the autoscroll form face</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
