---
base: ../
---

<section class="landing-overview">
  <div class="container">
    <div class="row landing-overview-intro">
      <div class="col-12">
        <small>Pick a conversational, chat or classic form layout - instantly!</small>
        <h2>Interactions with a face.</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-12 landing-overview-anchors">
        <ul class="tiles-text tiles-text-anchor">
          <li data-anchor="#autoscroll">
            <div>
              {% include icon-face.html face='autoscroll' size='small' name='Autoscroll Form Face' template='background' radius='medium' %}
            </div>
            <div>
              <h3>Autoscroll<span> Face</span></h3>
              <p>Fluently presents <strong>one question at a time</strong>.</p>
            </div>
          </li>
          <li data-anchor="#chat">
            <div>
              {% include icon-face.html face='chat' size='small' name='Chat Form Face' template='background' radius='medium' %}
            </div>
            <div>
              <h3>Chat<span> Face</span></h3>
              <p>Presents all <strong>questions and answers as a chat</strong>.</p>
            </div>
          </li>
          <li data-anchor="#classic">
            <div>
              {% include icon-face.html face='classic' size='small' name='Classic Form Face' template='background' radius='medium' %}
            </div>
            <div>
              <h3>Classic<span> Face</span></h3>
              <p>Presents question fields in a <strong>traditional format</strong>.</p>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
