---
base: ../
---

<section class="form-faces-face form-faces-face-classic content">
  <div class="container container-content" id="classic">
    <div class="row">
      <div class="col-12 form-faces-face-intro">
        <div>{% include icon-face.html face='classic' size='xl' name='Chat' template='background' radius='large' %}</div>
        <div class="form-faces-face-title">
          <span class="caption caption-rotated">Traditional layout</span>
          <h2>Classic</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-9 col-lg-8">
        <p>The classic form face <strong>presents your questions in a more traditional layout</strong>, with multiple largely typical input fields at a time. Yet, it still holds all the same logic powers as the other form faces.</p>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row form-face">
      <div class="col-lg-8">
        <img src="{{ page.base }}images/scenes/classic-fitness-registration.webp" width="2220" height="1435" alt="Screenshots of a fitness registration form in the classic form face, shown on a tablet and a mobile phone." loading="lazy" />
      </div>
      <div class="col-lg-4 form-face-list">
        <h3>Styling options</h3>
        <p class="content-explanation">The classic face is inspired by <strong>SurveyMonkey's traditional format</strong> and lets you tweak:</p>
        <ul class="palette-classic">
          <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/fonts.html %}Fonts</a></li>
          <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/colors.html %}Colors</a></li>
          <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/buttons.html %}Buttons</a></li>
          <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/backgrounds.html %}Backgrounds</a></li>
          <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/input-controls.html %}Input Controls</a></li>
          <li><a href="{{ page.base }}help/articles/discover-additional-options-for-classic-form-face/">{% include icons/scroll-direction.html %}Form Appearance</a></li>
          <li><a href="{{ page.base }}help/articles/how-to-edit-or-translate-all-text-labels-in-your-forms/">{% include icons/translations.html %}Labels/Translations</a></li>
          <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/remove-branding.html %}Hide Tripetto branding</a></li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <a href="{{ page.base }}help/articles/discover-additional-options-for-classic-form-face/" class="hyperlink palette-classic"><span>How to customize the classic form face</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
