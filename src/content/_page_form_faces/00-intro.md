---
base: ../
---

<section class="form-faces-intro block-first">
  <div class="container">
    <div class="row">
      <div class="col-lg-11 col-xl-10 shape-before shape-after">
        <h1>Pick the perfect form layout for the occasion with convertible faces.</h1>
      </div>
      <div class="col-md-10 col-lg-8">
        <p>Easily switch between <strong>totally different form experiences</strong>. Choose a Typeform-like, a chat, or a classic form face, and fully customize it for the optimal fit with your audience.</p>
        <a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/" class="hyperlink"><span>About form faces</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
