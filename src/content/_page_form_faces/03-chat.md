---
base: ../
---

<section class="form-faces-face form-faces-face-chat content">
  <div class="container container-content" id="chat">
    <div class="row">
      <div class="col-12 form-faces-face-intro">
        <div>{% include icon-face.html face='chat' size='xl' name='Chat' template='background' radius='large' %}</div>
        <div class="form-faces-face-title">
          <span class="caption caption-rotated">Speech bubble layout</span>
          <h2>Chat</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-9 col-lg-8">
        <p>The chat form face <strong>presents your questions and collected answers in a typical chat layout</strong>, with customizable speech balloons. It visually boosts the interactive experience for your respondents.</p>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row form-face">
      <div class="col-lg-8">
        <img src="{{ page.base }}images/scenes/chat-fitness-registration.webp" width="2220" height="1435" alt="Screenshots of a fitness registration form in the chat form face, shown on a tablet and a mobile phone." loading="lazy" />
      </div>
      <div class="col-lg-4 form-face-list">
        <h3>Styling options</h3>
        <p class="content-explanation">The chat face is partly inspired by <strong>Landbot's chat format</strong> and lets you tweak:</p>
        <ul class="palette-chat">
          <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/fonts.html %}Fonts</a></li>
          <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/colors.html %}Colors</a></li>
          <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/buttons.html %}Buttons</a></li>
          <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/backgrounds.html %}Backgrounds</a></li>
          <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/input-controls.html %}Input Controls</a></li>
          <li><a href="{{ page.base }}help/articles/discover-additional-options-for-chat-form-face/">{% include icons/chat-bubbles.html %}Chat Avatars & Bubbles</a></li>
          <li><a href="{{ page.base }}help/articles/how-to-edit-or-translate-all-text-labels-in-your-forms/">{% include icons/translations.html %}Labels/Translations</a></li>
          <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/remove-branding.html %}Hide Tripetto branding</a></li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <a href="{{ page.base }}help/articles/discover-additional-options-for-chat-form-face/" class="hyperlink palette-chat"><span>How to customize the chat form face</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
