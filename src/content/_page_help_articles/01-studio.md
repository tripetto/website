---
base: ../../
---

<section class="help-articles-all">
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        {% assign articles_basic = site.articles_help | where_exp: "item", "item.areas contains 'studio' and item.category_id == 'studio-basics'" %}
        <div class="category-title">
          <h3>Studio Basics</h3>
        </div>
        <ul>
          {% for article_basic in articles_basic %}
          <li>
            <div onclick="window.location='{{ article_basic.url }}';">
              <a href="{{ article_basic.url }}"><h4>{{ article_basic.article_title }}</h4></a>
              <small class="pills">
                {% if article_basic.time > 0 %}<span><i class="fas fa-file-alt"></i>{{ article_basic.time }} Min.</span>{% endif %}{% if article_basic.time_video > 0 %}<span><i class="fas fa-video"></i>{{ article_basic.time_video }} Min.</span>{% endif %}
              </small>
            </div>
            <hr />
          </li>
          {% endfor %}
        </ul>
        {% assign articles_licensing = site.articles_help | where_exp: "item", "item.areas contains 'studio' and item.category_id == 'studio-licensing'" %}
        <div class="category-title">
          <h3>Studio Licensing</h3>
        </div>
        <ul>
          {% for article_licensing in articles_licensing %}
          <li>
            <div onclick="window.location='{{ article_licensing.url }}';">
              <a href="{{ article_licensing.url }}"><h4>{{ article_licensing.article_title }}</h4></a>
              <small class="pills">
                {% if article_licensing.time > 0 %}<span><i class="fas fa-file-alt"></i>{{ article_licensing.time }} Min.</span>{% endif %}{% if article_licensing.time_video > 0 %}<span><i class="fas fa-video"></i>{{ article_licensing.time_video }} Min.</span>{% endif %}
              </small>
            </div>
            <hr />
          </li>
          {% endfor %}
        </ul>
        {% assign categories = site.page_studio_help_categories | where_exp: "item", "item.category_type == 'chapter'" %}
        {% for category in categories %}
        {% assign data_category = site.data.help-categories[category.category_id] %}
        <div class="category-title">
          <div>{% include icon-chapter.html chapter=category.category_id size='small' name=data_category.name %}</div>
          <h3>{{ data_category.name }}</h3>
        </div>
        <ul>
          {% assign articles = site.articles_help | where_exp: "item", "item.category_id == category.category_id and item.areas contains 'studio'" %}
          {% for article in articles %}
          <li>
            <div onclick="window.location='{{ article.url }}';">
              <a href="{{ article.url }}"><h4>{{ article.article_title }}</h4></a>
              <small class="pills">
                {% if article.time > 0 %}<span><i class="fas fa-file-alt"></i>{{ article.time }} Min.</span>{% endif %}{% if article.time_video > 0 %}<span><i class="fas fa-video"></i>{{ article.time_video }} Min.</span>{% endif %}
              </small>
            </div>
            <hr />
          </li>
          {% endfor %}
        </ul>
        {% endfor %}
      </div>
    </div>
  </div>
</section>
