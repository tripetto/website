---
base: ../
---
<section class="support-hero block-first intro">
  <div class="container">
    <div class="row">
      <div class="col-md-8 shape-before">
        <h1>Support</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 col-lg-9 shape-after">
        <p>Good support is important to us. That’s why we made the <strong><a href="{{ page.base }}help/">help center</a> with how-to’s on every aspect of Tripetto</strong>. And if you can’t find your answer, we’re happy to help.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-9 col-xl-8">
        <form action="{{ page.base }}help/search/" method="get" class="help-search form-inline">
          <label class="sr-only" for="search-input">Search all help articles</label>
          <input type="text" name="q" class="form-input form-input-big" id="search-input" placeholder="Search all help articles" />
          <button class="button button-big"><i class="fas fa-search"></i><span>Search</span></button>
        </form>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
