---
base:
---

<section class="index-sdk">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-md-9">
        <span>FormBuilder SDK for developers</span>
        <h2>Tripetto in pieces for developers.</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 col-xl-6">
        <p>Tripetto’s FormBuilder SDK contains fully customizable components for equipping apps and websites with a <strong>comprehensive form building and deploying solution.</strong></p>
        <a href="{{ page.base }}sdk/" class="button button-large" target="_blank">Learn More</a>
        <small>Fun fact, the <a href="{{ page.base }}studio/">Tripetto studio</a> and <a href="{{ page.base }}wordpress/">Tripetto WordPress plugin</a> are both built with <a href="{{ page.base }}sdk/" target="_blank">this SDK</a>.</small>
      </div>
    </div>
  </div>
</section>
