---
base:
---

<section class="index-logic">
  <div class="container container-top">
    <div class="row content">
      <div class="col-md-9 col-lg-11 col-xl-10">
        <h2 class="palette-logic">Make it personal with <span>conversational logic.</span></h2>
      </div>
    </div>
  </div>
  <div class="container-fluid ticker-holder ticker-logic">
    <div class="row">
      <ul class="ticker-blocks ticker-blocks-logic-basic">
        {% assign blocks_logic = site.data.blocks-logic | where_exp: "item", "item.group == 'basic'" %}
        {% for item in blocks_logic %}
        <li>
          <a href="{{ page.base | append: item.url }}" target="_blank" class="palette-build">
          {% assign icon_url = "icons/" | append: item.icon %}
            {% include {{ icon_url }}.html %}
            <span>{{ item.name }}</span>
          </a>
        </li>
        {% endfor %}
      </ul>
      <ul class="ticker-blocks ticker-blocks-logic-action">
        {% assign blocks_logic = site.data.blocks-logic | where_exp: "item", "item.group == 'advanced'" %}
        {% for item in blocks_logic %}
        <li>
          <a href="{{ page.base | append: item.url }}" target="_blank" class="palette-logic">
          {% assign icon_url = "icons/" | append: item.icon %}
            {% include {{ icon_url }}.html %}
            <span>{{ item.name }}</span>
          </a>
        </li>
        {% endfor %}
      </ul>
      <ul class="ticker-blocks ticker-blocks-logic-advanced">
        {% assign blocks_logic = site.data.blocks-logic | where_exp: "item", "item.group == 'action'" %}
        {% for item in blocks_logic %}
        <li>
          <a href="{{ page.base | append: item.url }}" target="_blank" class="palette-customization">
          {% assign icon_url = "icons/" | append: item.icon %}
            {% include {{ icon_url }}.html %}
            <span>{{ item.name }}</span>
          </a>
        </li>
        {% endfor %}
      </ul>
    </div>
  </div>
  <div class="container container-bottom">
    <div class="row content">
      <div class="col-md-11 col-lg-10">
        <p>Tripetto has all the advanced logic features you need to get on point and conversational with every single respondent, and dramatically <strong>increase completion rates and reduce drop-offs.</strong></p>
        <a href="{{ page.base }}logic-types/" class="hyperlink palette-logic"><span>Using conversational logic</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
