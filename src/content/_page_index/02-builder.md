---
base:
---

<section class="index-builder">
  <div class="container">
    <div class="row content">
      <div class="col-12 index-builder-visual">
        <img src="{{ page.base }}images/index/builder.webp" width="1240" height="762" alt="Screenshots of the Tripetto form builder." loading="lazy">
      </div>
      <div class="col-12 col-sm-10 col-md-6">
        <h2 class="palette-build">Map forms out on the <span>magnetic storyboard.</span></h2>
        <p>Tripetto's magnetic storyboard lets you <strong>build forms like flowcharts</strong>, which makes designing conversational flows clean, fast and fun. And all no-code!</p>
        <a href="{{ page.base }}magnetic-form-builder/" class="hyperlink palette-build"><span>Building on the storyboard</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
