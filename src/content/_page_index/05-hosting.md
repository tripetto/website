---
base:
---

<section class="index-hosting">
  <div class="container">
    <div class="row index-hosting-title content">
      <div class="col-12">
        <h2 class="palette-hosting">Manage data with <span>storage freedom.</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col index-hosting-visual">
        <div class="index-hosting-block index-hosting-content content">
          <p><strong>Decide where your data is stored.</strong> Could be at Tripetto. But maybe elsewhere, bypassing Tripetto entirely. That’s cool, too!</p>
          <a href="{{ page.base }}hosting-freedom/" class="hyperlink palette-hosting"><span>Hosting forms and data</span><i class="fas fa-arrow-right"></i></a>
        </div>
        <div class="index-hosting-block index-hosting-option index-hosting-option-we">
          <h3>We host</h3>
          <p>The forms and surveys you build and data you collect are <strong>all stored in your own Tripetto cloud.</strong> We don’t use your data, and you can always extract your work and results as you please.</p>
          <img src="{{ page.base }}images/index/hosting-we.svg" width="248" height="292" alt="Human illustrating hosting at Tripetto" loading="lazy" />
        </div>
        <div class="index-hosting-block index-hosting-option index-hosting-option-self">
          <h3>Self-host</h3>
          <p>You decide what you want to store outside of Tripetto and where. <strong>Self-hosted data never reaches Tripetto servers at all.</strong> <a href="{{ page.base }}blog/dont-trust-someone-else-with-your-form-data/">Read our blog on GDPR.</a></p>
          <img src="{{ page.base }}images/index/hosting-self.svg" width="398" height="328" alt="Human illustrating self-hosting" loading="lazy" />
        </div>
      </div>
    </div>
  </div>
</section>
