---
base:
---

<section class="index-automations">
  <div class="container">
    <div class="row content">
      <div class="col-md-6">
        <h2 class="palette-automations">Automate with <span>1.000+ connections.</span></h2>
      </div>
      <div class="col-md-6">
        <p>Stay informed. Get <strong>notifications in Slack or email, connect to 1.000+ services and track form activity</strong> for completion and drop-off insights.</p>
        <a href="{{ page.base }}data-automations/" class="hyperlink palette-automations"><span>Automating and connecting</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
    <div class="row">
      <div class="col">{% include automations-visual.html %}</div>
    </div>
  </div>
</section>
