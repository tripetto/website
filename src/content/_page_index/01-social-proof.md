---
base:
---

<section class="index-social-proof">
  <div class="container">
    <div class="row">
      <div class="col-12 index-social-proof-frames">
        <div class="frame">
          <div><a href="https://fortune.com/fortune500/" target="_blank" rel="noopener noreferrer">{% include icons/fortune-500.html %}</a></div>
          <div>
            <h3>Globally Trusted</h3>
            <p>You’re in good company. Tripetto products are <strong>used by Fortune 500 companies</strong> worldwide.</p>
          </div>
        </div>
        <div class="frame">
          <div><a href="{{ page.base }}reviews/">{% include icons/stars.html %}</a></div>
          <div>
            <h3>Proudly Ranked</h3>
            <p>Consistently ranked as top-notch for its powerful features and versatility. <a href="{{ page.base }}reviews/">Read user reviews.</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
