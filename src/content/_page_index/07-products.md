---
base:
---

<section class="index-products">
  <div class="container">
    <div class="row index-products-title">
      <div class="col-12">
        <h2>Get Started</h2>
      </div>
    </div>
    <div class="row index-products-content">
      <div class="col-12 col-md-9 col-lg-4 index-products-text content">
        <p><strong>Tripetto comes in various flavors.</strong> All are full-blown versions, loaded with practically identical feature sets. But made for different users and purposes.</p>
        <a href="{{ page.base }}versions/" class="hyperlink"><span>Which is for me?</span><i class="fas fa-arrow-right"></i></a>
      </div>
      <div class="col-12 col-md-6 col-lg-4 index-products-version">
        <div class="palette-studio">
          <div>
            <h3>Studio</h3>
            <p>Tripetto in the cloud</p>
            <ul>
              <li>Available at tripetto.app</li>
              <li>Store forms and data at Tripetto</li>
              <li>Free and pay-once per form</li>
            </ul>
          </div>
          <div>
            <img src="{{ page.base }}images/index/versions-studio.svg" width="232" height="168" alt="Scene illustrating the Tripetto studio." loading="lazy" />
            <a href="{{ page.base }}studio/" class="button button-full">About The Studio</a>
          </div>
        </div>
        <a href="{{ page.base }}studio/pricing/" class="hyperlink hyperlink-small"><span>Start for free</span><i class="fas fa-arrow-right"></i></a>
      </div>
      <div class="col-12 col-md-6 col-lg-4 index-products-version">
        <div class="palette-wordpress">
          <div>
            <h3>Plugin</h3>
            <p>Tripetto inside WordPress</p>
            <ul>
              <li>Runs completely inside WordPress</li>
              <li>Store data inside the plugin only</li>
              <li>Free and paid subscriptions</li>
            </ul>
          </div>
            <div>
            <img src="{{ page.base }}images/index/versions-wordpress.svg" width="232" height="168" alt="Scene illustrating the Tripetto WordPress plugin." loading="lazy" />
            <a href="{{ page.base }}wordpress/" class="button button-full">About The Plugin</a>
          </div>
        </div>
        <a href="{{ page.base }}wordpress/pricing/" class="hyperlink hyperlink-small"><span>Download free plugin</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
