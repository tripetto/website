---
base:
---

<section class="index-hero block-first">
  <div class="container">
    <div class="row row-hero">
      <div class="col-md-9 col-lg-8 col-xl-7 col-hero">
        <h1>Ask lively.</h1>
        <p>Tripetto is like SurveyMonkey, Typeform and Landbot united, but with a fresh take on truly <strong>smart form building and safe data collection.</strong></p>
        <ul class="buttons">
          <li><a href="{{ page.base }}versions/" class="button button-large">Get Started</a></li>
          <li><a href="{{ page.base }}examples/" class="button button-large button-light">See Examples</a></li>
        </ul>
        <small>Also create <a href="{{ page.base }}examples/customer-satisfaction-with-net-promoter-score-nps/">surveys</a>, <a href="{{ page.base }}examples/trivia-quiz/">quizzes</a>, <a href="{{ page.base }}examples/body-mass-index-bmi-wizard/">wizards</a>, <a href="{{ page.base }}examples/wedding-rsvp/">registration forms</a> and <a href="{{ page.base }}examples/">so much more</a> with Tripetto.</small>
      </div>
    </div>
  </div>
</section>
