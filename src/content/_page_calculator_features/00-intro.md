---
base: ../
---

<section class="calculator-features-intro block-first intro">
  <div class="container">
    <div class="row">
      <div class="col-sm-11 col-lg-10 shape-before shape-after">
        <h1>The countless powers of the calculator.</h1>
        <p>Sure, you can do basic calculations in your Tripetto forms and surveys with the calculator. <strong>But it’s actually cut out for far more advanced purposes, too.</strong> See what it can do.</p>
      </div>
    </div>
  </div>
</section>
