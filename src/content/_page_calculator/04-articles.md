---
base: ../
---

<section class="calculator-articles help-list">
  <div class="container">
    <div class="row">
      <div class="col">
        <h2>Available articles and tutorials</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-11 col-xl-9">
        <ul class="help-list">
          {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'action-blocks-calculator' or item.article_id == 'action-blocks-calculator-detail'" %}
          {% for article in help %}
            {% assign data_category = site.data.help-categories[article.category_id] %}
          <li>
            <div class="palette-{{ article.category_id }}" onclick="window.location='{{ article.url }}';">
              <a href="{{ article.url }}"><h3>{{ article.article_title }}</h3></a>
              <p>{{ article.description }}</p>
              <small class="pills">
                <span class="palette-{{ article.category_id }} pill-splitter pill-splitter-after">{{ data_category.pill }}</span>{% if article.time > 0 %}<span><i class="fas fa-file-alt"></i>{{ article.time }} Min.</span>{% endif %}{% if article.time_video > 0 %}<span><i class="fas fa-video"></i>{{ article.time_video }} Min.</span>{% endif %}
              </small>
            </div>
          </li>
          {% endfor %}
        </ul>
      </div>
    </div>
  </div>
</section>
