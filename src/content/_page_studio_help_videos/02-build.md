---
base: ../../../
---

{% include help-videos-chapter.html video_category='build' category='build' title='Building forms and surveys' link='Show all help articles about form building' url='studio/help/building-forms-and-surveys/' %}
