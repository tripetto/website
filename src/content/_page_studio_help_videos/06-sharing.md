---
base: ../../../
---

{% include help-videos-chapter.html video_category='sharing-studio' category='sharing' title='Sharing forms and surveys' link='Show all help articles about sharing' url='studio/help/sharing-forms-and-surveys/' %}
