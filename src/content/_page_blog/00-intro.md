---
base: ../
---

<section class="blog-intro block-first intro">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-xl-8 shape-before">
        <h1>Blog</h1>
        <p>Tripetto blogs include articles on product launches, features, showcases, background stories, coding tutorials and <strong>open-hearted stories of the road so far</strong>.</p>
        <a href="{{ page.base }}subscribe/" class="hyperlink"><span>Subscribe to product news</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
