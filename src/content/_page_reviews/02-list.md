---
base: ../
reviews_left: [mohit,yamini,guilhermesouza,alexmtv,katrynvonfelsen,ari0,pomkanik,muyiwa,tasksdoneright,raypierce,bousrui,agetea,remrick,obock,markxkr,lianne_interpro,isyedme,franko,movedbymark,brokrbindr,expnomad,mohammadimran,tnaz,hayssamh,attrexx,paul_crossley,karimmypersonalway,baghdad_boy,svaghari,stoxpro7]
reviews_right: [mike_monty,goitseone,shirshendu,vladimir_markovic1,bruno_rigolot,stefb,danfadida,osconnect,timoschmitt1993,michaelp,arturscavone,britt,kgjermani,amkordesign,joaquinreyes,artimondigital,nickbeebox,tiziano,filip_van_hoeckel1,suram,richpw,prestonesto,dreadsupp,drhazeface,ericlucas,dennis_maksimov,silksmoov,avijeet15,divingintux]
---

<section class="reviews-list" id="reviews">
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <ul class="reviews">
          {% for review_left in page.reviews_left %}
          {% assign review_left_item = site.data.reviews[review_left] %}
          <li>
            <div>
              <div class="review-rating">
                {% for i in (1..review_left_item.rating) %}
                <i class="fas fa-star"></i>
                {% endfor %}
              </div>
              <div class="review-meta">
                <span>{{ review_left_item.author }}</span>
                <small>{{ review_left_item.date }}</small>
              </div>
              <a href="{{ review_left_item.url }}" target="_blank" rel="noopener noreferrer"><h4>{{ review_left_item.title }}</h4></a>
              <p>{{ review_left_item.text }}</p>
            </div>
          </li>
          {% endfor %}
        </ul>
      </div>
      <div class="col-md-6 reviews-right">
        <ul class="reviews">
          {% for review_right in page.reviews_right %}
          {% assign review_right_item = site.data.reviews[review_right] %}
          <li>
            <div>
              <div class="review-rating">
                {% for i in (1..review_right_item.rating) %}
                <i class="fas fa-star"></i>
                {% endfor %}
              </div>
              <div class="review-meta">
                <span>{{ review_right_item.author }}</span>
                <small>{{ review_right_item.date }}</small>
              </div>
              <a href="{{ review_right_item.url }}" target="_blank" rel="noopener noreferrer"><h4>{{ review_right_item.title }}</h4></a>
              <p>{{ review_right_item.text }}</p>
            </div>
          </li>
          {% endfor %}
        </ul>
      </div>
    </div>
  </div>
</section>
