---
base: ../../
---

<nav class="breadcrumb-navigation block-first" aria-label="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col">
        <ol>
          <li><a href="{{ page.base }}wordpress/">Tripetto WordPress plugin</a></li>
          <li><span>Pricing</span></li>
        </ol>
      </div>
    </div>
  </div>
</nav>

<section class="wordpress-pricing-hero intro intro-breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-10 shape-before">
        <h1>Pick your plan.</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-9 col-lg-8 shape-after">
        <p>Tripetto for WordPress comes in two flavors. The free version is ideal for trying out Tripetto as your smart form builder. The <strong>Pro version adds action blocks, notifications, connections and tracking</strong> to the mix.</p>
      </div>
    </div>
  </div>
</section>
