---
base: ../../
---

<section class="wordpress-pricing-plans">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 pricing-plan-column">
        <div class="pricing-plan">
          <a href="#money-back-guarantee" class="pricing-plan-badge anchor">
            <span class="pricing-plan-badge-title">14 Days</span>
            <span class="pricing-plan-badge-subtitle">Money Back Guarantee</span>
          </a>
          <h2>Pro</h2>
          <div class="pricing-price palette-dark">
            <div class="pricing-price-prefix">
              <div class="valuta">$</div>
              <div class="amount amount-small" id="proPrice">{{ site.pricing_wordpress_single }}</div>
              <div class="conditions">
                <span class="conditions-title">Per year</span>
                <span class="conditions-subtitle" id="proCondition">For 1 WordPress site</span>
              </div>
            </div>
          </div>
          <small class="pricing-availability">Available as Single-Site, 5-Sites or Unlimited</small>
          <div class="pricing-dropdown">
            <button class="dropdown-button" id="proDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-offset="0,8"><span id="proName">Single-Site</span><i class="fas fa-chevron-down"></i></button>
            <div class="dropdown-menu" aria-labelledby="proDropdown">
              <div class="dropdown-item" role="button" data-name="Single-Site" data-condition="For 1 WordPress site" data-price="{{ site.pricing_wordpress_single }}" data-button="button-purchase-pro-single" data-description="The Single-Site license lets you equip 1 WordPress site with Tripetto Pro.">
                <div><img src="{{ page.base }}images/wordpress-pricing/single-site.svg" alt="Illustration representing the single-site WordPress license" width="64" height="64" loading="lazy" /></div>
                <div>
                  <span><strong>Single-Site</strong> [${{ site.pricing_wordpress_single }}/year]</span>
                  <small>Lets you equip <strong>1 WordPress site</strong> with Tripetto Pro.</small>
                </div>
              </div>
              <div class="dropdown-item" role="button" data-name="5-Sites" data-condition="For up to 5 WordPress sites" data-price="{{ site.pricing_wordpress_multi }}" data-button="button-purchase-pro-multi" data-description="The 5-Sites license lets you equip up to 5 WordPress sites with Tripetto Pro.">
                <div><img src="{{ page.base }}images/wordpress-pricing/5-sites.svg" alt="Illustration representing the 5-sites WordPress license" width="64" height="64" loading="lazy" /></div>
                <div>
                  <span><strong>5-Sites</strong> [${{ site.pricing_wordpress_multi }}/year]</span>
                  <small>Lets you equip <strong>up to 5 WordPress sites</strong> with Tripetto Pro.</small>
                </div>
              </div>
              <div class="dropdown-item" role="button" data-name="Unlimited" data-condition="For unlimited WordPress sites" data-price="{{ site.pricing_wordpress_unlimited}}" data-button="button-purchase-pro-unlimited" data-description="The Unlimited license lets you equip unlimited WordPress sites with Tripetto Pro.">
                <div><img src="{{ page.base }}images/wordpress-pricing/unlimited-sites.svg" alt="Illustration representing the unlimited sites WordPress license" width="64" height="64" loading="lazy" /></div>
                <div>
                  <span><strong>Unlimited</strong> [${{ site.pricing_wordpress_unlimited }}/year]</span>
                  <small>Lets you equip <strong>unlimited WordPress sites</strong> with Tripetto Pro.</small>
                </div>
              </div>
            </div>
            <span class="button button-wide button-red button-purchase-pro-single" role="button" id="proButton">Buy Now</span>
          </div>
          <div class="pricing-description"><p><strong>The Pro plan always includes all features and priority support.</strong> <span id="proDescription">The Single-Site license lets you equip 1 WordPress site with Tripetto Pro.</span></p></div>
          <ul class="pricing-features pricing-features-grouped">
            <li class="featured included">Unlimited form building
              <ul class="pricing-features collapse pricing-features-collapse" id="proBuilding">
                <li class="included">Unlimited forms</li>
                <li class="included">Unlimited questions</li>
                <li class="included">Unlimited responses</li>
              </ul>
            </li>
            <li class="featured included">Unlimited form logic
              <ul class="pricing-features collapse pricing-features-collapse" id="proLogic">
                <li class="included">Branch logic</li>
                <li class="included">Jump logic</li>
                <li class="included">Pipe logic</li>
              </ul>
            </li>
            <li class="featured included">All layout features
              <ul class="pricing-features collapse pricing-features-collapse" id="proLayout">
                <li class="included">Autoscroll form face</li>
                <li class="included">Chat form face</li>
                <li class="included">Classic form face</li>
                <li class="included">Rich styling features</li>
                <li class="included">Label translations</li>
              </ul>
            </li>
            <li class="featured included">Advanced blocks
              <ul class="pricing-features collapse pricing-features-collapse" id="proAdvancedBlocks">
                <li class="included">Subforms</li>
                <li class="included">Signature input</li>
              </ul>
            </li>
            <li class="featured included">Action blocks
              <ul class="pricing-features collapse pricing-features-collapse" id="proActionBlocks">
                <li class="included">Calculator</li>
                <li class="included">Custom variable</li>
                <li class="included">Force stop</li>
                <li class="included">Hidden field</li>
                <li class="included">Raise error</li>
                <li class="included">Send email</li>
                <li class="included">Set value</li>
              </ul>
            </li>
            <li class="featured included">Notifications
              <ul class="pricing-features collapse pricing-features-collapse" id="proNotifications">
                <li class="included">Email notifications</li>
                <li class="included">Slack notifications</li>
              </ul>
            </li>
            <li class="featured included">Connections
              <ul class="pricing-features collapse pricing-features-collapse" id="proConnections">
                <li class="included">Make</li>
                <li class="included">Zapier</li>
                <li class="included">Pabbly Connect</li>
                <li class="included">Custom webhooks</li>
              </ul>
            </li>
            <li class="featured included">Activity tracking
              <ul class="pricing-features collapse pricing-features-collapse" id="proTracking">
                <li class="included">Google Analytics</li>
                <li class="included">Google Tag Manager</li>
                <li class="included">Facebook Pixel</li>
                <li class="included">Custom tracking codes</li>
              </ul>
            </li>
            <li class="featured included">Tripetto unbranding
              <ul class="pricing-features collapse pricing-features-collapse" id="proUnbranding">
                <li class="included">No Tripetto branding in forms</li>
                <li class="included">No Tripetto branding in emails</li>
              </ul>
            </li>
            <li class="featured included">WordPress roles management
              <ul class="pricing-features collapse pricing-features-collapse" id="proRoles">
                <li class="included">User role access settings</li>
                <li class="included">User role capabilities settings</li>
              </ul>
            </li>
            <li class="featured included">Basic support
              <ul class="pricing-features collapse pricing-features-collapse" id="proSupportBasic">
                <li class="included">Access to <a href="{{ page.base }}wordpress/help/">help center</a><small>[24/7]</small></li>
                <li class="included"><a href="{{ page.base }}wordpress/help/video-tutorials/">Video tutorials</a></li>
                <li class="included">Plugin updates</li>
              </ul>
            </li>
            <li class="featured included">Priority services
              <ul class="pricing-features collapse pricing-features-collapse" id="proSupportPrio">
                <li class="included">Priority support<small>[Mon-Fri, 9-17 CET]</small></li>
                <li class="included">Plugin updates and upgrades</li>
              </ul>
            </li>
          </ul>
          <span class="hyperlink hyperlink-small collapsed" role="button" data-toggle="collapse" data-target=".pricing-features-collapse" aria-expanded="false"><span>View all features</span><i class="fas fa-arrow-up"></i></span>
        </div>
      </div>
      <div class="col-lg-6 pricing-plan-column">
        <div class="pricing-plan">
          <h2>Free</h2>
          <div class="pricing-price palette-dark">
            <div class="pricing-price-prefix">
              <div class="valuta">$</div>
              <div class="amount amount-small">0</div>
              <div class="conditions">
                <span class="conditions-title">Per year</span>
                <span class="conditions-subtitle">For 1 WordPress site</span>
              </div>
            </div>
          </div>
          <small class="pricing-availability">Only available as Single-Site</small>
          <a href="{{ site.url_wordpress_plugin }}" target="_blank" class="button button-full button-dark">Download Free Version</a>
          <div class="pricing-description"><p class="pricing-description-free"><strong>The free plan is ideal for trying out Tripetto as your form builder.</strong> The free Single-Site version lets you equip 1 WordPress site with Tripetto.</p></div>
          <ul class="pricing-features pricing-features-grouped">
            <li class="featured included">Unlimited form building
              <ul class="pricing-features collapse pricing-features-collapse" id="freeBuilding">
                <li class="included">Unlimited forms</li>
                <li class="included">Unlimited questions</li>
                <li class="included">Unlimited responses</li>
              </ul>
            </li>
            <li class="featured included">Unlimited form logic
              <ul class="pricing-features collapse pricing-features-collapse" id="freeLogic">
                <li class="included">Branch logic</li>
                <li class="included">Jump logic</li>
                <li class="included">Pipe logic</li>
              </ul>
            </li>
            <li class="featured included">All layout features
              <ul class="pricing-features collapse pricing-features-collapse" id="freeLayout">
                <li class="included">Autoscroll form face</li>
                <li class="included">Chat form face</li>
                <li class="included">Classic form face</li>
                <li class="included">Rich styling features</li>
                <li class="included">Label translations</li>
              </ul>
            </li>
            <li class="featured excluded">Advanced blocks
              <ul class="pricing-features collapse pricing-features-collapse" id="freeAdvancedBlocks">
                <li class="excluded">Subforms</li>
                <li class="excluded">Signature input</li>
              </ul>
            </li>
            <li class="featured excluded">Action blocks
              <ul class="pricing-features collapse pricing-features-collapse" id="freeActionBlocks">
                <li class="excluded">Calculator</li>
                <li class="excluded">Custom variable</li>
                <li class="excluded">Force stop</li>
                <li class="excluded">Hidden field</li>
                <li class="excluded">Raise error</li>
                <li class="excluded">Send email</li>
                <li class="excluded">Set value</li>
              </ul>
            </li>
            <li class="featured excluded">Notifications
              <ul class="pricing-features collapse pricing-features-collapse" id="freeNotifications">
                <li class="excluded">Email notifications</li>
                <li class="excluded">Slack notifications</li>
              </ul>
            </li>
            <li class="featured excluded">Connections
              <ul class="pricing-features collapse pricing-features-collapse" id="freeConnections">
                <li class="excluded">Make</li>
                <li class="excluded">Zapier</li>
                <li class="excluded">Pabbly Connect</li>
                <li class="excluded">Custom webhooks</li>
              </ul>
            </li>
            <li class="featured excluded">Activity tracking
              <ul class="pricing-features collapse pricing-features-collapse" id="freeTracking">
                <li class="excluded">Google Analytics</li>
                <li class="excluded">Google Tag Manager</li>
                <li class="excluded">Facebook Pixel</li>
                <li class="excluded">Custom tracking codes</li>
              </ul>
            </li>
            <li class="featured excluded">Tripetto unbranding
              <ul class="pricing-features collapse pricing-features-collapse" id="freeUnbranding">
                <li class="excluded">No Tripetto branding in forms</li>
                <li class="excluded">No Tripetto branding in emails</li>
              </ul>
            </li>
            <li class="featured included">WordPress roles management
              <ul class="pricing-features collapse pricing-features-collapse" id="freeRoles">
                <li class="included">User role access settings</li>
                <li class="included">User role capabilities settings</li>
              </ul>
            </li>
            <li class="featured included">Basic support
              <ul class="pricing-features collapse pricing-features-collapse" id="freeSupportBasic">
                <li class="included">Access to <a href="{{ page.base }}wordpress/help/">help center</a><small>[24/7]</small></li>
                <li class="included"><a href="{{ page.base }}wordpress/help/video-tutorials/">Video tutorials</a></li>
                <li class="included">Plugin updates</li>
              </ul>
            </li>
            <li class="featured excluded">Priority services
              <ul class="pricing-features collapse pricing-features-collapse" id="freeSupportPrio">
                <li class="excluded">Priority support<small>[Mon-Fri, 9-17 CET]</small></li>
                <li class="excluded">Plugin updates and upgrades</li>
              </ul>
            </li>
          </ul>
          <span class="hyperlink hyperlink-small collapsed" role="button" data-toggle="collapse" data-target=".pricing-features-collapse" aria-expanded="false"><span>View all features</span><i class="fas fa-arrow-up"></i></span>
        </div>
      </div>
    </div>
  </div>
</section>
