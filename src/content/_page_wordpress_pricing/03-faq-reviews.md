---
base: ../../
reviews: [tasksdoneright,mike_monty,muyiwa]
---

{% assign faq_id = 0 %}
{% assign faq = site.data.pricing-faq | where_exp: "item", "item.areas contains 'wordpress'" %}

<section class="pricing-faq-reviews faq">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-xl-6">
        <h2>FAQ</h2>
        <div class="accordion" id="faq">
          {% for item in faq %}
          <div class="card">
            <div class="card-header" id="faq_heading{{ faq_id }}"><a href="#" class="collapsed" role="button" data-toggle="collapse" data-target="#faq_collapse{{ faq_id }}" aria-expanded="true" aria-controls="faq_collapse{{ faq_id }}"><h3>{{ item.question }}</h3><i class="fas fa-chevron-right"></i></a></div>
            <div id="faq_collapse{{ faq_id }}" class="collapse" aria-labelledby="faq_heading{{ faq_id }}" data-parent="#faq"><div class="card-body">{{ item.answer }}</div></div>
          </div>
          {% assign faq_id = faq_id | plus: 1 %}
          {% endfor %}
        </div>
      </div>
      {% if page.reviews %}
      <div class="col-lg-6 col-xl-5 offset-xl-1">
        <ul class="reviews">
          {% for review in page.reviews %}
          {% assign review_item = site.data.reviews[review] %}
          <li>
            <div>
              <div class="review-rating">
                {% for i in (1..review_item.rating) %}
                <i class="fas fa-star"></i>
                {% endfor %}
              </div>
              <div class="review-meta">
                <span>{{ review_item.author }}</span>
                <small>{{ review_item.date }}</small>
              </div>
              <h4>{{ review_item.title }}</h4>
              <p>{{ review_item.text }}</p>
            </div>
          </li>
          {% endfor %}
        </ul>
      </div>
      {% endif %}
    </div>
  </div>
</section>
