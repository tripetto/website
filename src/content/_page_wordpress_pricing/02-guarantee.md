---
base: ../../
---

<section class="wordpress-pricing-guarantee" id="money-back-guarantee">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="pricing-guarantee">
          <div>
            <div class="pricing-guarantee-badge">
              <span class="pricing-guarantee-badge-title">14 Days</span>
              <span class="pricing-guarantee-badge-subtitle">Money Back Guarantee</span>
            </div>
          </div>
          <div>
            <h2>Our 100% Unconditional Money Back Guarantee!</h2>
            <p>We're happy and grateful you’re using Tripetto. Over the next 14 days, if Tripetto isn’t what you hoped, <a href="{{ page.base }}support/">drop us a line</a>. We’ll promptly refund the full 100%. <strong>No questions asked.</strong></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
