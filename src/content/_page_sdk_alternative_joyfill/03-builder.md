---
base: ../../
---

<section class="sdk-alternative-builder" id="builder">
  <div class="container">
    <div class="row content content-small sdk-alternative-builder-title">
      <div class="col-xl-11">
        <h2 class="palette-sdk-builder">Integrate the builder to <span>create forms in your app.</span></h2>
      </div>
    </div>
    {% include sdk-features-builder.html %}
    <div class="row content content-small">
      <div class="col-md-6 sdk-alternative-builder-left">
        <p>Add powerful form building capabilities by also neatly integrating the visual builder in your app or website. <strong>The builder runs entirely inside your project</strong> (on-premise), without any dependencies on external infrastructure.</p>
        <ul class="buttons">
          <li><a href="{{ page.base }}sdk/docs/builder/integrate/introduction/" class="button button-large">Get Started</a></li>
          <li><a href="https://codesandbox.io/s/tripetto-sdk-builder-react-loading-blocks-6tigtu" class="button button-large button-light" target="_blank">Code Demo</a></li>
        </ul>
        <a data-toggle="modal" data-target="#videoModal" data-video="78v901HZbD0" class="hyperlink hyperlink-small palette-sdk-builder sdk-video-builder"><i class="fas fa-clapperboard fa-fw sdk-video-builder"></i><span class="sdk-video-builder">Video tutorial (1m:58s)</span></a>
      </div>
      <div class="col-md-6">
        <a data-toggle="modal" data-target="#videoModal" data-video="78v901HZbD0" class="sdk-video-builder"><img src="{{ page.base }}images/sdk-videos-thumbnails/implementation-builder.png" width="1120" height="641" alt="Visual thumbnail for the video 'Integrating the form builder'" class="content-small-video sdk-video-builder" loading="lazy" /></a>
        <ul class="sdk-logos">
          <li><a href="{{ page.base }}sdk/docs/builder/integrate/quickstart/plain-js/" title="See form builder implementation docs for JavaScript">{% include icons/javascript.html %}</a></li>
          <li><a href="{{ page.base }}sdk/docs/builder/integrate/quickstart/react/" title="See form builder implementation docs for React">{% include icons/react.html %}</a></li>
          <li><a href="{{ page.base }}sdk/docs/builder/integrate/quickstart/angular/" title="See form builder implementation docs for Angular">{% include icons/angular.html %}</a></li>
          <li><a href="{{ page.base }}sdk/docs/builder/integrate/quickstart/html/" title="See form builder implementation docs for HTML5">{% include icons/html5.html %}</a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
