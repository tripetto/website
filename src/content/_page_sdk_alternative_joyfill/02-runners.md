---
base: ../../
---

<section class="sdk-alternative-runners" id="runners">
  <div class="container">
    <div class="row content content-small">
      <div class="col-xl-11">
        <h2 class="palette-sdk-runners">Embed the runner to <span>deploy forms in your app.</span></h2>
      </div>
    </div>
    {% include sdk-features-runners.html %}
    <div class="row content content-small">
      <div class="col-md-6 order-md-last">
        <p>Create a stunning form in the free online Tripetto studio and implement it in your app or website in no time. <strong>The form will run entirely inside your project</strong> (on-premise), without any dependencies on external infrastructure.</p>
        <ul class="buttons">
          <li><a href="{{ page.base }}sdk/docs/runner/stock/introduction/" class="button button-large button-yellow">Get Started</a></li>
          <li><a href="https://codesandbox.io/s/tripetto-sdk-runner-autoscroll-basic-implementation-eyq4x" class="button button-large button-light" target="_blank">Code Demo</a></li>
        </ul>
        <a data-toggle="modal" data-target="#videoModal" data-video="fixp89Yievg" class="hyperlink hyperlink-small palette-sdk-runners sdk-video-runners"><i class="fas fa-clapperboard fa-fw sdk-video-runners"></i><span class="sdk-video-runners">Video tutorial (2m:15s)</span></a>
      </div>
      <div class="col-md-6 order-md-first">
        <a data-toggle="modal" data-target="#videoModal" data-video="fixp89Yievg" class="sdk-video-runners"><img src="{{ page.base }}images/sdk-videos-thumbnails/implementation-runner.png" width="1120" height="641" alt="Visual thumbnail for the video 'Embedding the form runner'" class="content-small-video sdk-video-runners" loading="lazy" /></a>
        <ul class="sdk-logos">
          <li><a href="{{ page.base }}sdk/docs/runner/stock/quickstart/plain-js/" title="See form runner implementation docs for JavaScript">{% include icons/javascript.html %}</a></li>
          <li><a href="{{ page.base }}sdk/docs/runner/stock/quickstart/react/" title="See form runner implementation docs for React">{% include icons/react.html %}</a></li>
          <li><a href="{{ page.base }}sdk/docs/runner/stock/quickstart/angular/" title="See form runner implementation docs for Angular">{% include icons/angular.html %}</a></li>
          <li><a href="{{ page.base }}sdk/docs/runner/stock/quickstart/html/" title="See form runner implementation docs for HTML5">{% include icons/html5.html %}</a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
