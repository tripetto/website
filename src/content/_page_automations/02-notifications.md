---
base: ../
articles: [slack,email]
---

<section class="automations-notifications">
  <div class="container container-intro content" id="notifications">
    <div class="row">
      <div class="col-sm-11 col-md-9 col-lg-8">
        <h2>Notifications</h2>
        <p>Have Tripetto actively update you about new form completions with instant, <strong>automatic notifications straight into your Slack and inbox</strong>.</p>
      </div>
    </div>
  </div>
  <div class="container container-content">
    <div class="row">
      <div class="col-12 col-lg-8">
        <div class="row">
          <div class="col-6 automations-notifications-column automations-notifications-slack">
            <img src="{{ page.base }}images/automations/slack.svg" width="240" height="240" alt="Logo Slack" loading="lazy" />
            <p class="content-explanation">Promptly receive automatic <strong>Slack notifications whenever a new form is completed</strong> by a respondent.</p>
            <a href="{{ page.base }}help/articles/how-to-automate-slack-notifications-for-each-new-result/" class="hyperlink hyperlink-medium palette-automations"><span>Slack notifications</span><i class="fas fa-arrow-right"></i></a>
          </div>
          <div class="col-6 automations-notifications-column automations-notifications-email">
            <img src="{{ page.base }}images/automations/email.svg" width="160" height="130" alt="Icon representing email notification" loading="lazy" />
            <p class="content-explanation">Receive automatic <strong>notifications in your inbox whenever a new form is completed</strong> by a respondent.</p>
            <a href="{{ page.base }}help/articles/how-to-automate-email-notifications-for-each-new-result/" class="hyperlink hyperlink-medium palette-automations"><span>Email notifications</span><i class="fas fa-arrow-right"></i></a>
          </div>
        </div>
        <hr />
      </div>
      <div class="col-12 col-lg-4 automations-notifications-help">
        <ul class="landing-help-list">
          {% for article in page.articles %}
          {% assign article_item = site.articles_help | find: "article_id", article %}
            {% include landing-help-article.html url=article_item.url title=article_item.article_title description=article_item.description category_id=article_item.category_id %}
          {% endfor %}
        </ul>
      </div>
    </div>
  </div>
</section>

