---
base: ../
articles: [connection-google-sheets,connection-mailchimp,connection-notion,connection-zendesk]
---

<section class="automations-connections">
  <div class="container container-intro content" id="connections">
    <div class="row">
      <div class="col-sm-11 col-md-9 col-lg-8">
        <h2>Connections</h2>
        <p>Have Tripetto automatically <strong>send your data to 1.000+ connected services</strong> with Make, Zapier, Pabbly Connect and custom webhooks.</p>
      </div>
    </div>
  </div>
  <div class="container container-content">
    <div class="row">
      <div class="col-md-4 automations-connections-column">
        <p class="content-explanation">Use webhooks to easily <strong>connect collected data to other platforms</strong> without a single line of code.</p>
        <a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/" class="hyperlink hyperlink-medium palette-automations"><span>Webhooks</span><i class="fas fa-arrow-right"></i></a>
      </div>
      <div class="col-md-4 automations-connections-column">
        <p class="content-explanation">Use <strong>Make, Zapier, and Pabbly Connect</strong> to send your data to 1.000+ connected services.</p>
        <a href="{{ page.base }}help/articles/how-to-connect-form-responses-to-automation-tools/" class="hyperlink hyperlink-medium palette-automations"><span>Automation tools</span><i class="fas fa-arrow-right"></i></a>
      </div>
      <div class="col-md-4 automations-connections-column">
        <p class="content-explanation">Easily shape your data into anything you need with <strong>countless powerful, no-code automations</strong>.</p>
        <a href="#connections-examples" class="hyperlink hyperlink-medium palette-automations anchor"><span>No-code examples</span><i class="fas fa-arrow-down"></i></a>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <hr/>
      </div>
    </div>
  </div>
  <div class="container-fluid ticker-holder ticker-connections">
    <div class="row">
      <ul class="ticker-blocks ticker-connections-services1">
        {% assign services = site.data.connections-services | where_exp: "item", "item.row == 1" %}
        {% for item in services %}
        <li>
          <a href="{{ item.url }}" target="_blank" rel="noopener noreferrer">
          <img src="{{ page.base }}images/automations-connections-services/{{ item.logo }}" width="64" height="64" alt="Logo of {{ item.name }}" loading="lazy" />
          <span>{{ item.name }}</span>
          </a>
        </li>
        {% endfor %}
      </ul>
      <ul class="ticker-blocks ticker-connections-services2">
        {% assign services = site.data.connections-services | where_exp: "item", "item.row == 2" %}
        {% for item in services %}
        <li>
          <a href="{{ item.url }}" target="_blank" rel="noopener noreferrer">
          <img src="{{ page.base }}images/automations-connections-services/{{ item.logo }}" width="64" height="64" alt="Logo of {{ item.name }}" loading="lazy" />
          <span>{{ item.name }}</span>
          </a>
        </li>
        {% endfor %}
      </ul>
      <ul class="ticker-blocks ticker-connections-services3">
        {% assign services = site.data.connections-services | where_exp: "item", "item.row == 3" %}
        {% for item in services %}
        <li>
          <a href="{{ item.url }}" target="_blank" rel="noopener noreferrer">
          <img src="{{ page.base }}images/automations-connections-services/{{ item.logo }}" width="64" height="64" alt="Logo of {{ item.name }}" loading="lazy" />
          <span>{{ item.name }}</span>
          </a>
        </li>
        {% endfor %}
      </ul>
    </div>
  </div>
  <div class="container" id="connections-examples">
    <div class="row">
      <div class="col-12">
        <ul class="landing-help-list landing-help-list-horizontal landing-help-list-horizontal-4">
          {% for article in page.articles %}
          {% assign article_item = site.articles_help | find: "article_id", article %}
            {% include landing-help-article.html url=article_item.url title=article_item.article_title description=article_item.description category_id=article_item.category_id %}
          {% endfor %}
        </ul>
      </div>
    </div>
  </div>
</section>

