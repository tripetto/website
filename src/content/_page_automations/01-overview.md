---
base: ../
---

<section class="landing-overview">
  <div class="container">
    <div class="row landing-overview-intro">
      <div class="col-12">
        <small>Stay on top of things with smart automations.</small>
        <h2>From data to insights.</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-12 landing-overview-anchors">
        <ul class="tiles-text tiles-text-medium tiles-text-anchor">
          <li data-anchor="#notifications">
            <div>
              {% include icon-paragraph.html chapter='automations' paragraph='notifications' name='Notifications' folder='automations' %}
            </div>
            <div>
              <h3>Notifications</h3>
              <p>Receive <strong>automatic alerts</strong> about form completions.</p>
            </div>
          </li>
          <li data-anchor="#connections">
            <div>
              {% include icon-paragraph.html chapter='automations' paragraph='connections' name='Connections' folder='automations' %}
            </div>
            <div>
              <h3>Connections</h3>
              <p>Design <strong>automatic data flows</strong> to 1.000+ services.</p>
            </div>
          </li>
          <li data-anchor="#tracking">
            <div>
              {% include icon-paragraph.html chapter='automations' paragraph='tracking' name='Activity Tracking' folder='automations' %}
            </div>
            <div>
              <h3><span>Activity </span>Tracking</h3>
              <p>Track every <strong>respondent activity</strong> inside your forms.</p>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
