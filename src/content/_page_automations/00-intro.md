---
base: ../
---

<section class="automations-intro block-first intro">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-md-10 col-lg-8 col-xl-9">
        <h1>Automate with 1.000+ connections.</h1>
      </div>
      <div class="col-md-7 col-lg-6">
        <p>Stay informed. Get <strong>notifications in Slack or email</strong>, <strong>connect to 1.000+ services</strong> and <strong>track form activity</strong> for completion and drop-off insights.</p>
      </div>
    </div>
  </div>
</section>
