---
base: ../
articles: [tracking-completions,tracking-dropoffs]
---

<section class="automations-tracking">
  <div class="container container-intro content" id="tracking">
    <div class="row">
      <div class="col-sm-11 col-md-9 col-lg-8">
        <h2>Activity Tracking</h2>
        <p>Boost form completions and reduce drop-offs by <strong>tracking activity with Google Analytics</strong>, <strong>Google Tag Manager</strong> and <strong>Facebook Pixel</strong>.</p>
      </div>
    </div>
  </div>
  <div class="container container-help">
    <div class="row">
      <div class="col-12">
        <ul class="landing-help-list landing-help-list-horizontal landing-help-list-horizontal-2">
          {% for article in page.articles %}
          {% assign article_item = site.articles_help | find: "article_id", article %}
            {% include landing-help-article.html url=article_item.url title=article_item.article_title description=article_item.description category_id=article_item.category_id %}
          {% endfor %}
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <hr/>
      </div>
    </div>
  </div>
  <div class="container container-content">
    <div class="row">
      <div class="col-md-4 automations-tracking-column automations-tracking-analytics">
        <img src="{{ page.base }}images/automations/tracking-google-analytics.svg" width="48" height="48" alt="Logo Google Analytics" loading="lazy" />
        <p class="content-explanation">Use <strong>Google Analytics to analyze</strong> form starts, form completions, interactions, drop-offs, and more.</p>
        <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-google-analytics/" class="hyperlink hyperlink-medium palette-automations"><span>Google Analytics</span><i class="fas fa-arrow-right"></i></a>
      </div>
      <div class="col-md-4 automations-tracking-column automations-tracking-tagmanager">
        <img src="{{ page.base }}images/automations/tracking-google-tag-manager.svg" width="256" height="256" alt="Logo Google Tag Manager" loading="lazy" />
        <p class="content-explanation">Use <strong>Tag Manager to analyze</strong> form starts, form completions, interactions, drop-offs, and more.</p>
        <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-google-tag-manager/" class="hyperlink hyperlink-medium palette-automations"><span>Google Tag Manager</span><i class="fas fa-arrow-right"></i></a>
      </div>
      <div class="col-md-4 automations-tracking-column automations-tracking-facebook">
        <img src="{{ page.base }}images/automations/tracking-facebook-pixel.png" width="256" height="256" alt="Logo Facebook Pixel" loading="lazy" />
        <p class="content-explanation">Use <strong>Facebook Pixel to analyze</strong> form starts, form completions, interactions, drop-offs, and more.</p>
        <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-facebook-pixel/" class="hyperlink hyperlink-medium palette-automations"><span>Facebook Pixel</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>

