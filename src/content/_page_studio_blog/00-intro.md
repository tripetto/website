---
base: ../../
---

<nav class="breadcrumb-navigation block-first" aria-label="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col">
        <ol>
          <li><a href="{{ page.base }}studio/">Tripetto studio</a></li>
          <li><span>Blog</span></li>
        </ol>
      </div>
    </div>
  </div>
</nav>

<section class="blog-intro intro intro-breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-xl-8 shape-before">
        <h1>Studio Blog</h1>
        <p>The studio blog includes articles about form building, data collection, product updates, and much more to <strong>get the most out of our free online Tripetto studio.</strong></p>
        <a href="{{ page.base }}subscribe/" class="hyperlink"><span>Subscribe to product news</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
