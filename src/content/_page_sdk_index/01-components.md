---
base: ../
---

<section class="sdk-index-components">
  <div class="container">
    <div class="row content sdk-index-components-intro">
      <div class="col-md-11 col-lg-9">
        <small>Equip your website or app with a complete form solution in no-time!</small>
        <h2>The form kit to fit.</h2>
      </div>
      <div class="col-sm-11 col-md-10 col-lg-9">
        <p>The SDK's parts for form building and deployment are designed for quick embedding in straightforward cases as well as <strong>deep integration into advanced applications.</strong></p>
      </div>
    </div>
    <div class="row">
      <div class="col-12 sdk-carousel-components-buttons">
        <ul class="carousel-buttons">
          <li class="palette-sdk-builder active" data-target="#carouselSDKComponents" data-slide-to="0">
            <div>
              {% include sdk-icon-component.html component='builder' size='small' name='Form Builder' template='background' radius='medium' %}
            </div>
            <div>
              <h3><span>Form </span>Builder</h3>
              <p>The intelligent storyboard is for building forms like flowcharts with powerful logic flows.</p>
            </div>
          </li>
          <li class="palette-sdk-runners" data-target="#carouselSDKComponents" data-slide-to="1">
            <div>
              {% include sdk-icon-component.html component='runners' size='small' name='Form Runners' template='background' radius='medium' %}
            </div>
            <div>
              <h3><span>Form </span>Runners</h3>
              <p>Runners execute forms created in the builder and handle UX, logic and response delivery.</p>
            </div>
          </li>
          <li class="palette-sdk-blocks" data-target="#carouselSDKComponents" data-slide-to="2">
            <div>
              {% include sdk-icon-component.html component='blocks' size='small' name='Question Blocks' template='background' radius='medium' %}
            </div>
            <div>
              <h3><span>Question </span>Blocks</h3>
              <p>Customize and even develop question types to enhance the forms in your app or website.</p>
            </div>
          </li>
        </ul>
      </div>
    </div>
    <div class="row carousel-slides">
      <div class="col">
        <div id="carouselSDKComponents" class="carousel slide">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="row">
                <div class="col-12">
                  <div class="sdk-device-code sdk-device-code-right">
                    <img src="{{ page.base }}images/sdk-scenes/builder.webp" width="2000" height="1441" alt="Screenshot of the form builder." class="sdk-device-code-block sdk-device-code-device" loading="lazy" />
                    {% include sdk-code-snippet-builder.html id="builder" class="sdk-device-code-block sdk-device-code-snippet" %}
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="row">
                <div class="col-12">
                  <div class="sdk-device-code sdk-device-code-right">
                    <img src="{{ page.base }}images/sdk-scenes/runner.webp" width="2000" height="1441" alt="Screenshot of a customer satisfaction form in the autoscroll form face, shown on a tablet." class="sdk-device-code-block sdk-device-code-device" loading="lazy" />
                    {% include sdk-code-snippet-runner-autoscroll.html id="runner" class="sdk-device-code-block sdk-device-code-snippet" %}
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="row">
                <div class="col-12">
                  <div class="sdk-device-code sdk-device-code-right">
                    <img src="{{ page.base }}images/sdk-scenes/blocks.webp" width="2000" height="1441" alt="Screenshot of available question blocks in the form builder." class="sdk-device-code-block sdk-device-code-device" loading="lazy" />
                    {% include sdk-code-snippet-blocks.html id="blocks" class="sdk-device-code-block sdk-device-code-snippet" %}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12"><a href="{{ page.base }}sdk/how-it-works/" class="hyperlink"><span>How the FormBuilder SDK works</span><i class="fas fa-arrow-right"></i></a></div>
    </div>
    <div class="row sdk-index-components-fortune-500">
      <div class="col-12">
        <div class="frame">
          <div><a href="https://fortune.com/fortune500/" target="_blank" rel="noopener noreferrer">{% include icons/fortune-500.html %}</a></div>
          <div>
            <h3>Open source. Extensible. Documented.</h3>
            <p>You’re in good company. The FormBuilder SDK’s components are <strong>used and trusted by <a href="https://fortune.com/fortune500/" target="_blank" rel="noopener noreferrer">Fortune 500 companies</a></strong> around the globe.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
