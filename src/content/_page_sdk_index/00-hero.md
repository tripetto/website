---
base: ../
---

<section class="sdk-index-hero block-first">
  <div class="container">
    <div class="sdk-index-hero-background">
      <div class="row">
        <div class="col-sm-9 col-md-10 col-lg-9">
          <ul class="sdk-logos">
            <li><a href="{{ page.base }}sdk/plain-js/" title="Tripetto's FormBuilder SDK works in JavaScript">{% include icons/javascript.html %}</a></li>
            <li><a href="{{ page.base }}sdk/react/" title="Tripetto's FormBuilder SDK works in React">{% include icons/react.html %}</a></li>
            <li><a href="{{ page.base }}sdk/angular/" title="Tripetto's FormBuilder SDK works in Angular">{% include icons/angular.html %}</a></li>
            <li><a href="{{ page.base }}sdk/html/" title="Tripetto's FormBuilder SDK works in HTML5">{% include icons/html5.html %}</a></li>
          </ul>
          <h1>Instantly equip your site or app with a form solution.</h1>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-10 col-md-8 col-lg-7 col-xl-6">
          <p>Tripetto’s FormBuilder SDK contains fully customizable components for equipping apps and websites with a <strong>comprehensive form building and deploying solution</strong>.</p>
          <ul class="buttons">
            <li><a href="{{ page.base }}sdk/how-it-works/" class="button button-large">Live Demo</a></li>
            <li><a href="{{ page.base }}sdk/docs/" class="button button-large button-white">Get Started</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
