---
base: ../
---

<section class="sdk-index-implementation">
  <div class="container">
    <div class="row content">
      <div class="col-md-11 col-lg-9">
        <h2>The SDK works in any environment running <span class="palette-inline">JavaScript.</span></h2>
      </div>
    </div>
    <div class="row content">
      <div class="col-md-11 col-lg-10 col-xl-9">
        <p>The FormBuilder SDK contains solely client-side components running in the context of the user's browser, without relying on a specific back-end. In other words, <strong>everything works in any client-side environment that supports JavaScript.</strong></p>
      </div>
    </div>
    <div class="row">
      <div class="col sdk-index-implementation-visual">
        <div class="sdk-index-implementation-visual-top">
          <a href="{{ page.base }}sdk/plain-js/" title="See implementation docs for JavaScript" class="sdk-index-implementation-option sdk-index-implementation-javascript">{% include icons/javascript.html %}</a>
          <a href="{{ page.base }}sdk/react/" title="See implementation docs for React" class="sdk-index-implementation-option sdk-index-implementation-react">{% include icons/react.html %}</a>
        </div>
        <div class="sdk-index-implementation-visual-bottom">
          <a href="{{ page.base }}sdk/angular/" title="See implementation docs for Angular" class="sdk-index-implementation-option sdk-index-implementation-angular">{% include icons/angular.html %}</a>
          <a href="{{ page.base }}sdk/html/" title="See implementation docs for HTML5" class="sdk-index-implementation-option sdk-index-implementation-html5">{% include icons/html5.html %}</a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12"><a href="{{ page.base }}sdk/solutions/" class="hyperlink"><span>How to implement SDK components</span><i class="fas fa-arrow-right"></i></a></div>
    </div>
    <div class="row sdk-index-implementation-languages">
      <div class="col-6 col-lg-3">
        <p>The SDK components are pure <a href="https://www.javascript.com/" target="_blank" rel="noopener noreferrer">JavaScript</a> (VanillaJS). They <strong>run in any client-side environment</strong> that supports JavaScript. Just like that.</p>
        <a href="{{ page.base }}sdk/plain-js/" class="hyperlink hyperlink-small"><span>Tripetto and Javascript</span><i class="fas fa-arrow-right"></i></a>
      </div>
      <div class="col-6 col-lg-3">
        <p>The SDK components are <strong>also usable in <a href="https://react.dev/" target="_blank" rel="noopener noreferrer">React</a></strong>. The docs include hands-on guides for implementing Tripetto in a React app.</p>
        <a href="{{ page.base }}sdk/react/" class="hyperlink hyperlink-small"><span>Tripetto for React</span><i class="fas fa-arrow-right"></i></a>
      </div>
      <div class="col-6 col-lg-3">
        <p>Prefer <a href="https://angular.io/" target="_blank" rel="noopener noreferrer">Angular</a>? No problem. The docs cover step-by-step instructions to easily <strong>use the SDK components in an Angular app</strong> in no-time.</p>
        <a href="{{ page.base }}sdk/angular/" class="hyperlink hyperlink-small"><span>Tripetto for Angular</span><i class="fas fa-arrow-right"></i></a>
      </div>
      <div class="col-6 col-lg-3">
        <p><strong>Still feeling HTML? We got you!</strong> Just pull in from a CDN what you need from the SDK for your project, and off you go! Literally in just minutes.</p>
        <a href="{{ page.base }}sdk/html/" class="hyperlink hyperlink-small"><span>Tripetto and HTML5</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
    <div class="row sdk-index-implementation-typescript">
      <div class="col-12">
        <div class="frame">
          <div><a href="https://www.typescriptlang.org/" target="_blank" rel="noopener noreferrer">{% include icons/typescript.html %}</a></div>
          <div>
            <h3>Tripetto ❤️ TypeScript</h3>
            <p>All FormBuilder SDK packages include type declarations and support <strong><a href="https://www.typescriptlang.org/" target="_blank" rel="noopener noreferrer">TypeScript</a> out of the box</strong>.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
