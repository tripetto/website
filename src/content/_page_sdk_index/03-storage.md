---
base: ../
---

<section class="sdk-index-storage">
  <div class="container">
    <div class="row content">
      <div class="col-md-10 col-xl-9">
        <h2><span class="palette-inline">Exclusively self-host</span> things where and how you want.</h2>
      </div>
    </div>
    <div class="row content">
      <div class="col-md-10 col-lg-9">
        <p>The SDK comes without predefined storage solutions. You decide how and where forms and data are stored. Without any dependencies on unwanted infrastructure. <strong>Nothing ever touches any other platform, unless you let it.</strong></p>
      </div>
    </div>
    <div class="row">
      <div class="col">
        {% include sdk-storage.html layout="arrow" %}
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <a href="{{ page.base }}sdk/self-hosting/" class="hyperlink"><span>How to self-host the SDK and data</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
