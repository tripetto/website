---
base: ../
---

<section class="question-types-intro block-first intro">
  <div class="container">
    <div class="row">
      <div class="col-sm-10 col-md-12 col-lg-11 shape-before">
        <h1>Ask all the right questions inside your forms.</h1>
      </div>
      <div class="col-sm-11 col-md-8 col-lg-7 shape-after">
        <p>Tripetto offers <strong>every question type you need</strong> to build advanced forms, surveys, quizzes and more. Including real-time, automatic actions.</p>
      </div>
    </div>
  </div>
</section>
