---
base: ../
---

<section class="landing-overview">
  <div class="container">
    <div class="row landing-overview-intro">
      <div class="col-12">
        <small>Combine data collection and actions in your forms.</small>
        <h2>Ask and act right away.</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-12 landing-overview-anchors">
        <ul class="tiles-text tiles-text-large tiles-text-anchor">
          <li data-anchor="#questions">
            <div>
              {% include icon-paragraph.html chapter='build' paragraph='question-blocks' name='Question Blocks' folder='question-types' %}
            </div>
            <div>
              <h3>Question<span> Blocks</span></h3>
              <p>Ask for respondent input with <strong>all the regular question types</strong>.</p>
            </div>
          </li>
          <li data-anchor="#actions">
            <div>
              {% include icon-paragraph.html chapter='logic' paragraph='action-blocks' name='Action Blocks' folder='question-types' %}
            </div>
            <div>
              <h3>Action<span> Blocks</span></h3>
              <p>Perform real-time actions <strong>based on respondent inputs</strong>.</p>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
