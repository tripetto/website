---
base: ../
---

<section class="question-types question-types-questions content">
  <div class="container container-content" id="questions">
    <div class="row">
      <div class="col-12 question-types-intro">
        <div>{% include icon-chapter.html chapter='build' size='xl' radius='large' name='Question Blocks' %}</div>
        <div class="question-types-title">
          <h2>Questions</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-9 col-lg-8">
        <p>Ask your audience anything using Tripetto’s <strong>complete range of question types</strong>. All question types come with advanced optional settings and logic to get the most out of every interaction.</p>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-12">
        <ul class="question-types-tiles question-types-questions">
          {% include question-type-tile.html url='how-to-use-the-checkbox-single-block' icon='checkbox' name='Checkbox' %}
          {% include question-type-tile.html url='how-to-use-the-checkboxes-block' icon='checkboxes' name='Checkboxes' %}
          {% include question-type-tile.html url='how-to-use-the-date-and-time-block' icon='date' name='Date (and time)' %}
          {% include question-type-tile.html url='how-to-use-the-dropdown-block' icon='dropdown' name='Dropdown' %}
          {% include question-type-tile.html url='how-to-use-the-dropdown-multi-select-block' icon='dropdown-multi-select' name='Dropdown (multi)' %}
          {% include question-type-tile.html url='how-to-use-the-email-address-block' icon='email-address' name='Email Address' %}
          {% include question-type-tile.html url='how-to-use-the-file-upload-block' icon='file-upload' name='File Upload' %}
          {% include question-type-tile.html url='how-to-use-the-matrix-block' icon='matrix' name='Matrix' %}
          {% include question-type-tile.html url='how-to-use-the-multiple-choice-block' icon='multiple-choice' name='Multiple Choice' %}
          {% include question-type-tile.html url='how-to-use-the-number-block' icon='number' name='Number' %}
          {% include question-type-tile.html url='how-to-use-the-paragraph-block' icon='paragraph' name='Paragraph' %}
          {% include question-type-tile.html url='how-to-use-the-password-block' icon='password' name='Password' %}
          {% include question-type-tile.html url='how-to-use-the-phone-number-block' icon='phone-number' name='Phone Number' %}
          {% include question-type-tile.html url='how-to-use-the-picture-choice-block' icon='picture-choice' name='Picture Choice' %}
          {% include question-type-tile.html url='how-to-use-the-radio-buttons-block' icon='radio-buttons' name='Radio Buttons' %}
          {% include question-type-tile.html url='how-to-use-the-ranking-block' icon='ranking' name='Ranking' %}
          {% include question-type-tile.html url='how-to-use-the-rating-block' icon='rating' name='Rating' %}
          {% include question-type-tile.html url='how-to-use-the-scale-block' icon='scale' name='Scale' %}
          {% include question-type-tile.html url='how-to-use-the-signature-block' icon='signature' name='Signature' %}
          {% include question-type-tile.html url='how-to-use-the-statement-block' icon='statement' name='Statement' %}
          {% include question-type-tile.html url='how-to-use-the-text-multiple-lines-block' icon='text-multiple' name='Text - Multiple Lines' %}
          {% include question-type-tile.html url='how-to-use-the-text-single-line-block' icon='text-single' name='Text - Single Line' %}
          {% include question-type-tile.html url='how-to-use-the-url-block' icon='url' name='URL' %}
          {% include question-type-tile.html url='how-to-use-the-yes-no-block' icon='yes-no' name='Yes/No' %}
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <a href="{{ page.base }}help/articles/question-blocks-guide-which-question-type-to-use/" class="hyperlink palette-build"><span>How to determine which question type to use</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>

