---
base: ../
---

<section class="question-types question-types-actions content">
  <div class="container container-content" id="actions">
    <div class="row">
      <div class="col-12 question-types-intro">
        <div>{% include icon-chapter.html chapter='logic' size='xl' radius='large' name='Action Blocks' %}</div>
        <div class="question-types-title">
          <h2>Actions</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-9 col-lg-8">
        <p>Use action blocks inside your form’s flow for even smarter performance with <strong>instant actions and calculations</strong> based on respondent inputs. All of this without having to code anything.</p>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-12">
        <ul class="question-types-tiles question-types-actions">
          {% include question-type-tile.html url='how-to-use-the-calculator-block' icon='calculator' name='Calculator' %}
          {% include question-type-tile.html url='how-to-use-the-custom-variable-block' icon='custom-variable' name='Custom Variable' %}
          {% include question-type-tile.html url='how-to-use-the-force-stop-block' icon='force-stop' name='Force Stop' %}
          {% include question-type-tile.html url='how-to-use-the-hidden-field-block' icon='hidden-field' name='Hidden Field' %}
          {% include question-type-tile.html url='how-to-use-the-raise-error-block' icon='raise-error' name='Raise Error' %}
          {% include question-type-tile.html url='how-to-use-the-send-email-block' icon='send-email' name='Send Email' %}
          {% include question-type-tile.html url='how-to-use-the-set-value-block' icon='set-value' name='Set Value' %}
        </ul>
      </div>
    </div>
  </div>
</section>
