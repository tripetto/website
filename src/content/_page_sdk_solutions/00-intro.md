---
base: ../../
---

<section class="sdk-solutions-intro block-first sdk-intro">
  <div class="container container-intro">
    <div class="row">
      <div class="col-md-8 col-lg-10 col-xl-7">
        <h1>Mix what you need, and how you need it.</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-9 col-xl-6">
        <p>What you need from the SDK’s extensive toolset depends on how far you want to take things in your website or app. <strong>You won’t always need everything.</strong></p>
        <ul class="buttons">
          <li><a href="{{ page.base }}sdk/docs/" class="button button-large">Get Started</a></li>
          <li><a href="{{ page.base }}sdk/pricing/#determine-license" class="button button-large button-light">What Fits Me?</a></li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
  <div class="container container-anchors">
    <div class="row">
      <div class="col-md-11 col-lg-7 col-xl-6">
        <ul class="anchors-icons">
          <li>
            <a href="#hybrid">
              {% include sdk-icon-chapter.html chapter='hybrid' size='big' name='Hybrid Setup' %}
              <span>Hybrid Setup</span>
              <span>Basic Skills</span>
              <small>Up in minutes</small>
            </a>
          </li>
          <li>
            <a href="#standard">
              {% include sdk-icon-chapter.html chapter='standard' size='big' name='Standard Setup' %}
              <span>Standard Setup</span>
              <span>Medium Skills</span>
              <small>Up in an hour</small>
            </a>
          </li>
          <li>
            <a href="#custom">
              {% include sdk-icon-chapter.html chapter='custom' size='big' name='Custom Setup' %}
              <span>Custom Setup</span>
              <span>Advanced Skills</span>
              <small>Up in hours/days</small>
            </a>
          </li>
          <li>
            <a href="#a-team">
              {% include sdk-icon-chapter.html chapter='a-team' size='big' name='A-Team Setup' %}
              <span>A-Team Setup</span>
              <span>Master Skills</span>
              <small>Up in days/weeks</small>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
