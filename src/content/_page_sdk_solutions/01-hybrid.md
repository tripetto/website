---
base: ../../
---

<section class="sdk-solutions-hybrid content sdk-table" id="hybrid">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-lg-12 col-xl-10">
        <span class="caption">Hybrid setup, basic developer skills required.</span>
        <h2 class="palette-sdk-hybrid">Build forms in our studio, and only use <span>the runner in your app.</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-10">
        <p>Tripetto’s stock runners are for <strong>deploying forms in websites and applications.</strong> They run forms with stock blocks from our free, online Tripetto studio. So, if you don’t need the builder inside your website or app, this hybrid solution is your best and fastest fit.</p>
      </div>
    </div>
    <div class="row sdk-solutions-hybrid-visual">
      <div class="col-lg-6">
        <h3 class="sdk-code-snippet-title"><strong>Runner</strong> implementation</h3>
        {% include sdk-code-snippet-runner-autoscroll.html id="hybrid-runner" %}
      </div>
      <div class="col-lg-6">
        <div class="sdk-table-title"><h3>Hybrid Setup</h3></div>
        <div class="table-responsive">
          <table class="table sdk-table-body">
            <tbody>
              <tr>
                <td class="sdk-table-body-row"><h4>Building Forms</h4></td>
                <td><div><strong>Build and edit forms in the free online studio at <a href="{{ site.url_app }}" target="_blank" rel="noopener noreferrer">tripetto.app</a>.</strong> No need to implement the builder into your website or application.</div></td>
              </tr>
              <tr>
                <td class="sdk-table-body-row"><h4>Running Forms</h4></td>
                <td><div><strong>Implement any of the <a href="{{ page.base }}sdk/docs/runner/stock/introduction/">stock runners</a> into your website or application to run forms created in the studio.</strong> Implementing the runners is a matter of minutes and is possible using plain JS, React, Angular and HTML5.</div></td>
              </tr>
              <tr>
                <td class="sdk-table-body-row"><h4>Hosting Setup</h4></td>
                <td><div><strong><a href="{{ page.base }}sdk/docs/runner/stock/guides/collecting/">Self-host</a> collected responses exclusively on your end</strong> for the forms you manage in the studio and run in your website or application.</div></td>
              </tr>
              <tr>
                <td class="sdk-table-body-row"><h4>Available Blocks</h4></td>
                <td><div><strong>Build and run forms with stock blocks <a href="{{ page.base }}sdk/docs/blocks/stock/">stock blocks</a> only.</strong> It’s not possible to customize blocks or develop your own blocks, because of the use of the studio and implementation of stock runners.</div></td>
              </tr>
              <tr>
                <td class="sdk-table-body-row"><h4>Customizations</h4></td>
                <td><div>Customizing the form builder, runners and blocks <strong>is not supported</strong> in this setup.</div></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12"><a href="{{ page.base }}sdk/docs/runner/introduction/" class="hyperlink palette-sdk-hybrid"><span>Learn how to implement the hybrid setup</span><i class="fas fa-arrow-right"></i></a></div>
    </div>
  </div>
</section>
