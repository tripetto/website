---
base: ../../
---

<section class="sdk-solutions-standard content sdk-table" id="standard">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-lg-9 col-xl-8">
        <span class="caption">Standard setup, medium developer skills required.</span>
        <h2 class="palette-sdk-standard">Implement both <span>the builder and the runner in your app.</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-11">
        <p><strong>All components in the FormBuilder SDK can be neatly integrated fully into websites and applications.</strong> So, if you need to be fully independent of any Tripetto infrastructure and want to have total control, then implementing both the builder and runner is the way to go.</p>
      </div>
    </div>
    <div class="row sdk-solutions-standard-visual">
      <div class="col-lg-6">
        <h3 class="sdk-code-snippet-title"><strong>Builder</strong> implementation</h3>
        {% include sdk-code-snippet-builder.html id="standard-builder" %}
      </div>
      <div class="col-lg-6">
        <h3 class="sdk-code-snippet-title"><strong>Runner</strong> implementation</h3>
        {% include sdk-code-snippet-runner-autoscroll.html id="standard-runner" %}
      </div>
      <div class="col-12">
        <div class="sdk-table-title"><h3>Standard Setup</h3></div>
        <div class="table-responsive">
          <table class="table sdk-table-body">
            <tbody>
              <tr>
                <td class="sdk-table-body-row"><h4>Building Forms</h4></td>
                <td><div><strong>Implement the <a href="{{ page.base }}sdk/docs/builder/introduction/">builder</a> into your website or application to create forms.</strong> Implementing the builder is usually a matter of minutes and is possible using plain JS, React, Angular and HTML5.</div></td>
              </tr>
              <tr>
                <td class="sdk-table-body-row"><h4>Running Forms</h4></td>
                <td><div><strong>Implement any of the <a href="{{ page.base }}sdk/docs/runner/stock/introduction/">stock runners</a> into your website or application to run forms created in builder.</strong> Implementing the runners is a matter of minutes and is possible using plain JS, React, Angular and HTML5.</div></td>
              </tr>
              <tr>
                <td class="sdk-table-body-row"><h4>Hosting Setup</h4></td>
                <td><div><strong>Build and run forms entirely inside your website or application, and exclusively <a href="{{ page.base }}sdk/docs/runner/stock/guides/collecting/">self-host</a> collected responses on your end.</strong> All without any dependencies on third-party infrastructure.</div></td>
              </tr>
              <tr>
                <td class="sdk-table-body-row"><h4>Available Blocks</h4></td>
                <td><div><strong>Build and run forms with the available <a href="{{ page.base }}sdk/docs/blocks/stock/">stock blocks</a> only.</strong> Customization of these question blocks and the development of your own original blocks is only supported in the custom setup.</div></td>
              </tr>
              <tr>
                <td class="sdk-table-body-row"><h4>Customizations</h4></td>
                <td><div>Customizing the form builder, runners and blocks <strong>is not supported</strong> in this setup.</div></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12"><a href="{{ page.base }}sdk/docs/builder/introduction/" class="hyperlink palette-sdk-standard"><span>Learn how to implement the standard setup</span><i class="fas fa-arrow-right"></i></a></div>
    </div>
  </div>
</section>
