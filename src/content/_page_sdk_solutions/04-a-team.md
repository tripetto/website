---
base: ../../
---

<section class="sdk-solutions-a-team" id="a-team">
  <div class="container">
    <div class="row content">
      <div class="col-md-8 col-lg-10">
        <span class="caption">A-Team setup, master developer skills required.</span>
        <h2 class="palette-sdk-a-team">Equip the headless runner engine with <span>custom UI and UX.</span></h2>
      </div>
    </div>
    <div class="row content">
      <div class="col-md-11 col-lg-10">
        <p>The form runner smartly handles UI, complex logic and response collection during form execution. <strong>Its engine is headless and just begging for A-Team devs to wield its powers with custom UI and UX layers.</strong> There's a learning curve. But hey, no pain no gain!</p>
        <small>Customizing runner UI/UX may require an additional license.</small>
      </div>
    </div>
    <div class="row sdk-solutions-a-team-visual">
      <div class="col-lg-12">
        <div class="sdk-solutions-a-team-example sdk-solutions-a-team-example-custom">
          <h3 class="sdk-code-snippet-title">Custom UI & UX</h3>
          <a href="{{ page.base }}sdk/docs/runner/custom/examples/"><img src="{{ page.base }}images/sdk-solutions/runner-custom.webp" width="1120" height="1120" alt="Custom runner with custom UI & UX" loading="lazy" /></a>
        </div>
        <div class="sdk-solutions-a-team-example sdk-solutions-a-team-example-material-ui">
          <h3 class="sdk-code-snippet-title">Material UI</h3>
          <a href="{{ page.base }}sdk/docs/runner/custom/examples/"><img src="{{ page.base }}images/sdk-solutions/runner-material-ui.webp" width="960" height="960" alt="Custom runner with Material UI" loading="lazy" /></a>
        </div>
        <div class="sdk-solutions-a-team-example sdk-solutions-a-team-example-fluent-ui">
          <a href="{{ page.base }}sdk/docs/runner/custom/examples/"><img src="{{ page.base }}images/sdk-solutions/runner-fluent-ui.webp" width="800" height="800" alt="Custom runner with Fluent UI" loading="lazy" /></a>
          <h3 class="sdk-code-snippet-title">Fluent UI</h3>
        </div>
        <div class="sdk-solutions-a-team-example sdk-solutions-a-team-example-bootstrap">
          <a href="{{ page.base }}sdk/docs/runner/custom/examples/"><img src="{{ page.base }}images/sdk-solutions/runner-bootstrap.webp" width="640" height="640" alt="Custom runner with Bootstrap" loading="lazy" /></a>
          <h3 class="sdk-code-snippet-title">Bootstrap</h3>
        </div>
        <div class="sdk-solutions-a-team-payoff">These are just some examples. Anything you design can work.</div>
      </div>
    </div>
    <div class="row">
      <div class="col-12"><a href="{{ page.base }}sdk/docs/runner/custom/introduction/" class="hyperlink palette-sdk-a-team"><span>Learn how to implement any custom UI and UX</span><i class="fas fa-arrow-right"></i></a></div>
    </div>
    <div class="row sdk-solutions-a-team-fortune-500">
      <div class="col-12">
        <div class="frame">
          <div><a href="https://fortune.com/fortune500/" target="_blank" rel="noopener noreferrer">{% include icons/fortune-500.html %}</a></div>
          <div>
            <h3>Open source. Extensible. Documented.</h3>
            <p>You’re in good company. The FormBuilder SDK’s components are <strong>used and trusted by <a href="https://fortune.com/fortune500/" target="_blank" rel="noopener noreferrer">Fortune 500 companies</a></strong> around the globe.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
