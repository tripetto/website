---
base: ../../
---

<section class="sdk-solutions-custom content sdk-table" id="custom">
  <div class="container">
    <div class="row">
      <div class="col-xl-11">
        <span class="caption">Custom setup, advanced developer skills required.</span>
        <h2 class="palette-sdk-custom">Boost your app with <span>custom question blocks.</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-11 col-lg-10">
        <p>With both the builder and runner implemented you can start to <strong>customize our question blocks, or enhance an application with your own</strong> to unlock API's and other amazing things. So, if you really need to bend or extend things, the custom setup is ideal for you.</p>
      </div>
    </div>
    <div class="row sdk-solutions-custom-visual">
      <div class="col-lg-6">
        <h3 class="sdk-code-snippet-title"><strong>Builder</strong> part of a custom block</h3>
        {% include sdk-code-snippet-block-builder.html id="custom-builder" %}
      </div>
      <div class="col-lg-6">
        <h3 class="sdk-code-snippet-title"><strong>Runner</strong> part of a custom block</h3>
        {% include sdk-code-snippet-block-runner.html id="custom-runner" %}
      </div>
      <div class="col-12">
        <div class="sdk-table-title"><h3>Custom Setup</h3></div>
        <div class="table-responsive">
          <table class="table sdk-table-body">
            <tbody>
              <tr>
                <td class="sdk-table-body-row"><h4>Building Forms</h4></td>
                <td><div><strong>Implement the <a href="{{ page.base }}sdk/docs/builder/introduction/">builder</a> into your website or application to create forms.</strong> Implementing the builder is usually a matter of minutes and is possible using plain JS, React, Angular and HTML5.</div></td>
              </tr>
              <tr>
                <td class="sdk-table-body-row"><h4>Running Forms</h4></td>
                <td><div><strong>Implement any of the <a href="{{ page.base }}sdk/docs/runner/stock/introduction/">stock runners</a> into your website or application to run forms created in builder.</strong> Implementing the runners is a matter of minutes and is possible using plain JS, React, Angular and HTML5.</div></td>
              </tr>
              <tr>
                <td class="sdk-table-body-row"><h4>Hosting Setup</h4></td>
                <td><div><strong>Build and run forms entirely inside your website or application, and exclusively <a href="{{ page.base }}sdk/docs/runner/stock/guides/collecting/">self-host</a> collected responses on your end.</strong> All without any dependencies on third-party infrastructure.</div></td>
              </tr>
              <tr>
                <td class="sdk-table-body-row"><h4>Available Blocks</h4></td>
                <td><div><strong>Build and run forms with <a href="{{ page.base }}sdk/docs/blocks/stock/">stock blocks</a>, customize these blocks, or <a href="{{ page.base }}sdk/docs/blocks/custom/introduction/">develop your own</a> original blocks.</strong> Because of the implementation of both the builder and stock runners inside your project, it’s possible, with the appropriate license, in place to customize and develop blocks.</div></td>
              </tr>
              <tr>
                <td class="sdk-table-body-row"><h4>Customizations</h4></td>
                <td><div>Customizing and developing question blocks <strong>is supported</strong> in this setup. Customizing the form builder and runners is not.</div></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12"><a href="{{ page.base }}sdk/docs/blocks/introduction/" class="hyperlink palette-sdk-custom"><span>Learn how to implement the custom setup</span><i class="fas fa-arrow-right"></i></a></div>
    </div>
  </div>
</section>
