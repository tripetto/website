---
base: ../../
---

<section class="sdk-usage-runners">
  <div class="container">
    <div class="row content">
      <div class="col">
        <h2 class="palette-sdk-runners">Use the runner library <span>to run forms in your project.</span></h2>
      </div>
    </div>
    <div class="row content">
      <div class="col-lg-11">
        <p>Create a stunning form in the free online <a href="{{ site.url_app }}" target="_blank">Tripetto studio</a> or <a href="#builder" class="anchor">integrated form builder</a> and implement it in your HTML site in no time. <strong>The form will run entirely inside your HTML project</strong> - with the style and UX of your choice. All completely self-hosted, or run straight from a CDN.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-12 sdk-carousel-runners-buttons">
        <ul class="carousel-buttons">
          <li class="palette-autoscroll active" data-target="#carouselSDKRunners" data-slide-to="0">
            <div>
              {% include icon-face.html face='autoscroll' size='small' name='Autoscroll Form Face' template='sdk' radius='medium' %}
            </div>
            <div>
              <h3>Autoscroll<span> Face</span></h3>
              <p>Fluently presents <strong>one question at a time</strong>.</p>
            </div>
          </li>
          <li class="palette-chat" data-target="#carouselSDKRunners" data-slide-to="1">
            <div>
              {% include icon-face.html face='chat' size='small' name='Chat Form Face' template='sdk' radius='medium' %}
            </div>
            <div>
              <h3>Chat<span> Face</span></h3>
              <p>Presents all <strong>questions and answers as a chat</strong>.</p>
            </div>
          </li>
          <li class="palette-classic" data-target="#carouselSDKRunners" data-slide-to="2">
            <div>
              {% include icon-face.html face='classic' size='small' name='Classic Form Face' template='sdk' radius='medium' %}
            </div>
            <div>
              <h3>Classic<span> Face</span></h3>
              <p>Presents question fields in a <strong>traditional format</strong>.</p>
            </div>
          </li>
        </ul>
      </div>
    </div>
    <div class="row carousel-slides">
      <div class="col">
        <div id="carouselSDKRunners" class="carousel slide">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="row">
                <div class="col-12">
                  <div class="sdk-device-code sdk-device-code-right">
                    <img src="{{ page.base }}images/sdk-scenes/runner-autoscroll.webp" width="2220" height="1435" alt="Screenshot of a fitness registration form in the autoscroll form face, shown on a tablet and a mobile phone." class="sdk-device-code-block sdk-device-code-device" loading="lazy" />
                    <div class="sdk-code-snippet sdk-device-code-block sdk-device-code-snippet">
                      <ul class="nav nav-tabs nav-fill sdk-code-snippet-nav sdk-code-snippet-nav-single" role="tablist">
                        <li class="nav-item"><a class="nav-link active" id="nav-runner-autoscroll-html" data-toggle="tab" href="#runner-autoscroll-html5" role="tab" aria-controls="runner-autoscroll-html5" aria-selected="true" title="Implement the autoscroll runner with HTML">{% include icons/html5.html %}</a></li>
                      </ul>
                      <div class="tab-content sdk-code-snippet-content">
                        <div class="tab-pane fade show active" id="runner-autoscroll-html5" role="tabpanel" aria-labelledby="nav-runner-autoscroll-html5">
                          <ul class="nav nav-tabs sdk-code-snippet-subnav" role="tablist">
                            <li class="nav-item"><a class="nav-link active" id="nav-runner-autoscroll-html5-jsdelivr" data-toggle="tab" href="#runner-autoscroll-html5-jsdelivr" role="tab" aria-controls="runner-autoscroll-html5-jsdelivr" aria-selected="true">jsDelivr</a></li>
                            <li class="nav-item"><a class="nav-link" id="nav-runner-autoscroll-html5-unpkg" data-toggle="tab" href="#runner-autoscroll-html5-unpkg" role="tab" aria-controls="runner-autoscroll-html5-unpkg" aria-selected="false">unpkg</a></li>
                          </ul>
                          <div class="tab-content sdk-code-snippet-subcontent" id="runner-autoscroll-html5-content">
                            <div class="tab-pane fade show active" id="runner-autoscroll-html5-jsdelivr" role="tabpanel" aria-labelledby="nav-runner-autoscroll-html5-jsdelivr">
                              {% include sdk-code-snippet-pane.html active=true code="runners-html5-jsdelivr.html" face="autoscroll" face-name="Autoscroll" language="html" palette="runners" url-run="https://codepen.io/tripetto/pen/c7f98c3e434562462e700342fd3401c9" url-docs="https://tripetto.com/sdk/docs/runner/stock/quickstart/html/" %}
                            </div>
                            <div class="tab-pane fade" id="runner-autoscroll-html5-unpkg" role="tabpanel" aria-labelledby="nav-runner-autoscroll-html5-unpkg">
                              {% include sdk-code-snippet-pane.html active=false code="runners-html5-unpkg.html" face="autoscroll" face-name="Autoscroll" language="html" palette="runners" url-run="https://codepen.io/tripetto/pen/835cfff322900f55cd0ac54b2851100e" url-docs="https://tripetto.com/sdk/docs/runner/stock/quickstart/html/" %}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="row">
                <div class="col-12">
                  <div class="sdk-device-code sdk-device-code-right">
                    <img src="{{ page.base }}images/sdk-scenes/runner-chat.webp" width="2220" height="1435" alt="Screenshot of a fitness registration form in the chat form face, shown on a tablet and a mobile phone." class="sdk-device-code-block sdk-device-code-device" loading="lazy" />
                    <div class="sdk-code-snippet sdk-device-code-block sdk-device-code-snippet">
                      <ul class="nav nav-tabs nav-fill sdk-code-snippet-nav sdk-code-snippet-nav-single" role="tablist">
                        <li class="nav-item"><a class="nav-link active" id="nav-runner-chat-html5" data-toggle="tab" href="#runner-chat-html5" role="tab" aria-controls="runner-chat-html5" aria-selected="true" title="Implement the chat runner with HTML">{% include icons/html5.html %}</a></li>
                      </ul>
                      <div class="tab-content sdk-code-snippet-content">
                        <div class="tab-pane fade show active" id="runner-chat-html5" role="tabpanel" aria-labelledby="nav-runner-chat-html5">
                          <ul class="nav nav-tabs sdk-code-snippet-subnav" role="tablist">
                            <li class="nav-item"><a class="nav-link active" id="nav-runner-chat-html5-jsdelivr" data-toggle="tab" href="#runner-chat-html5-jsdelivr" role="tab" aria-controls="runner-chat-html5-jsdelivr" aria-selected="true">jsDelivr</a></li>
                            <li class="nav-item"><a class="nav-link" id="nav-runner-chat-html5-unpkg" data-toggle="tab" href="#runner-chat-html5-unpkg" role="tab" aria-controls="runner-chat-html5-unpkg" aria-selected="false">unpkg</a></li>
                          </ul>
                          <div class="tab-content sdk-code-snippet-subcontent" id="runner-chat-html5-content">
                            <div class="tab-pane fade show active" id="runner-chat-html5-jsdelivr" role="tabpanel" aria-labelledby="nav-runner-chat-html5-jsdelivr">
                              {% include sdk-code-snippet-pane.html active=true code="runners-html5-jsdelivr.html" face="chat" face-name="Chat" language="html" palette="runners" url-run="https://codepen.io/tripetto/pen/eb46c1463dd72556c2dd219150dd80fa" url-docs="https://tripetto.com/sdk/docs/runner/stock/quickstart/html/" %}
                            </div>
                            <div class="tab-pane fade" id="runner-chat-html5-unpkg" role="tabpanel" aria-labelledby="nav-runner-chat-html5-unpkg">
                              {% include sdk-code-snippet-pane.html active=false code="runners-html5-unpkg.html" face="chat" face-name="Chat" language="html" palette="runners" url-run="https://codepen.io/tripetto/pen/e26cc00e6d71f8d8e02d8641633ca97c" url-docs="https://tripetto.com/sdk/docs/runner/stock/quickstart/html/" %}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="row">
                <div class="col-12">
                  <div class="sdk-device-code sdk-device-code-right">
                    <img src="{{ page.base }}images/sdk-scenes/runner-classic.webp" width="2220" height="1435" alt="Screenshot of a fitness registration form in the classic form face, shown on a tablet and a mobile phone." class="sdk-device-code-block sdk-device-code-device" loading="lazy" />
                    <div class="sdk-code-snippet sdk-device-code-block sdk-device-code-snippet">
                      <ul class="nav nav-tabs nav-fill sdk-code-snippet-nav sdk-code-snippet-nav-single" role="tablist">
                        <li class="nav-item"><a class="nav-link active" id="nav-runner-classic-html5" data-toggle="tab" href="#runner-classic-html5" role="tab" aria-controls="runner-classic-html5" aria-selected="true" title="Implement the classic runner with HTML">{% include icons/html5.html %}</a></li>
                      </ul>
                      <div class="tab-content sdk-code-snippet-content">
                        <div class="tab-pane fade show active" id="runner-classic-html5" role="tabpanel" aria-labelledby="nav-runner-classic-html5">
                          <ul class="nav nav-tabs sdk-code-snippet-subnav" role="tablist">
                            <li class="nav-item"><a class="nav-link active" id="nav-runner-classic-html5-jsdelivr" data-toggle="tab" href="#runner-classic-html5-jsdelivr" role="tab" aria-controls="runner-classic-html5-jsdelivr" aria-selected="true">jsDelivr</a></li>
                            <li class="nav-item"><a class="nav-link" id="nav-runner-classic-html5-unpkg" data-toggle="tab" href="#runner-classic-html5-unpkg" role="tab" aria-controls="runner-classic-html5-unpkg" aria-selected="false">unpkg</a></li>
                          </ul>
                          <div class="tab-content sdk-code-snippet-subcontent" id="runner-classic-html5-content">
                            <div class="tab-pane fade show active" id="runner-classic-html5-jsdelivr" role="tabpanel" aria-labelledby="nav-runner-classic-html5-jsdelivr">
                              {% include sdk-code-snippet-pane.html active=true code="runners-html5-jsdelivr.html" face="classic" face-name="Classic" language="html" palette="runners" url-run="https://codepen.io/tripetto/pen/b6765a6f53c42fcef5864fcf78591e52" url-docs="https://tripetto.com/sdk/docs/runner/stock/quickstart/html/" %}
                            </div>
                            <div class="tab-pane fade" id="runner-classic-html5-unpkg" role="tabpanel" aria-labelledby="nav-runner-classic-html5-unpkg">
                              {% include sdk-code-snippet-pane.html active=false code="runners-html5-unpkg.html" face="classic" face-name="Classic" language="html" palette="runners" url-run="https://codepen.io/tripetto/pen/ffd8dced3674a88ea308c0289cf761d2" url-docs="https://tripetto.com/sdk/docs/runner/stock/quickstart/html/" %}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {% include sdk-features-runners.html %}
  </div>
</section>
