---
base: ../../
---

<section class="sdk-usage-customizations">
  <div class="container">
    <div class="row content">
      <div class="col-12">
        <h2 class="palette-sdk-custom">The FormBuilder SDK is <span>deeply customizable.</span></h2>
      </div>
    </div>
    <div class="row sdk-usage-customizations-intro content">
      <div class="col-md-10 col-lg-9">
        <p>The development kit comes with a rich set of customizable, open source question types for developers to use out of the box or enhance. <strong>Actually, developers can create original question blocks, and even form UX from scratch.</strong></p>
        <ul class="buttons">
          <li><a href="{{ page.base }}sdk/docs/" class="button button-large button-orange">Get Started</a></li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
    <div class="row sdk-usage-customizations-stock-runners">
      <div class="col-xl-10">
        <div class="row sdk-usage-customizations-stock-runners-columns">
          <div class="col-md-6 col-lg-5 sdk-usage-customizations-stock-runners-buttons">
            <ul>
              <li class="palette-autoscroll" onclick="window.location='{{ page.base }}sdk/docs/runner/stock/quickstart/react/';">
                <div>
                  {% include icon-face.html face='autoscroll' size='small' name='Autoscroll Form Face' template='sdk' radius='medium' %}
                </div>
                <div>
                  <h4>Autoscroll<span> Layout</span></h4>
                  <p>Fluently presents <strong>one question at a time</strong>.</p>
                </div>
              </li>
              <li class="palette-chat" onclick="window.location='{{ page.base }}sdk/docs/runner/stock/quickstart/react/';">
                <div>
                  {% include icon-face.html face='chat' size='small' name='Chat Form Face' template='sdk' radius='medium' %}
                </div>
                <div>
                  <h4>Chat<span> Layout</span></h4>
                  <p>Presents all <strong>questions and answers as a chat</strong>.</p>
                </div>
              </li>
              <li class="palette-classic" onclick="window.location='{{ page.base }}sdk/docs/runner/stock/quickstart/react/';">
                <div>
                  {% include icon-face.html face='classic' size='small' name='Classic Form Face' template='sdk' radius='medium' %}
                </div>
                <div>
                  <h4>Classic<span> Layout</span></h4>
                  <p>Presents question fields in a <strong>traditional format</strong>.</p>
                </div>
              </li>
            </ul>
          </div>
          <div class="col-md-6 sdk-usage-customizations-stock-runners-content content content-small">
            <h3 class="palette-sdk-custom">Start with any of the runners <span>for the perfect form style.</span></h3>
            <p><strong>Runners are for deploying forms in apps and websites</strong>, turning a form definition into an executable program which handles the form styling, complex logic and response collection.</p>
            <a href="{{ page.base }}sdk/docs/runner/stock/quickstart/react/" class="hyperlink palette-sdk-custom"><span>Embed runners with HTML</span><i class="fas fa-arrow-right"></i></a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
    <div class="row sdk-usage-customizations-columns">
      <div class="col-xl-11">
        <div class="row content content-small">
          <div class="col-md-6 sdk-usage-customizations-column sdk-usage-customizations-custom-blocks">
            <img src="{{ page.base }}images/sdk-alternative/custom-blocks.svg" width="344" height="128" alt="Icons representing custom blocks." loading="lazy" />
            <h3 class="palette-sdk-custom">Boost or extend apps with <span>custom question blocks.</span></h3>
            <p>With the builder and runner easily and neatly implemented, you can start to <strong>customize question blocks, or enhance an app with your own</strong> to unlock API's and other superpowers. So, if you really need to bend or extend things, the FormBuilder SDK has got you covered.</p>
            <a href="{{ page.base }}sdk/docs/blocks/custom/introduction/" class="hyperlink palette-sdk-custom"><span>Build blocks with TypeScript</span><i class="fas fa-arrow-right"></i></a>
          </div>
          <div class="col sdk-usage-customizations-columns-splitter">
            <hr />
          </div>
          <div class="col-md-6 sdk-usage-customizations-column sdk-usage-customizations-custom-runners">
            <ul class="sdk-logos sdk-logos-large">
              <li><a href="{{ page.base }}sdk/docs/runner/custom/implement/plain-js/" title="See form runner customization docs for JavaScript">{% include icons/javascript.html %}</a></li>
              <li><a href="{{ page.base }}sdk/docs/runner/custom/implement/react/" title="See form runner customization docs for React">{% include icons/react.html %}</a></li>
              <li><a href="{{ page.base }}sdk/docs/runner/custom/implement/angular/" title="See form runner customization docs for Angular">{% include icons/angular.html %}</a></li>
            </ul>
            <h3 class="palette-sdk-custom">Equip your runner with <span>custom UI and UX.</span></h3>
            <p>Form runners handle UI, complex logic and response collection during form execution. <strong>The runner engine is headless and just begging for A-Team devs to wield its powers with custom UI and UX layers.</strong> There's a learning curve. But hey, no pain no gain!</p>
            <a href="{{ page.base }}sdk/docs/runner/custom/introduction/" class="hyperlink palette-sdk-custom"><span>Customize UI/UX</span><i class="fas fa-arrow-right"></i></a>
          </div>
        </div>
      </div>
    </div>
    <div class="row sdk-usage-customizations-typescript">
      <div class="col-12">
        <div class="frame">
          <div><a href="https://www.typescriptlang.org/" target="_blank" rel="noopener noreferrer">{% include icons/typescript.html %}</a></div>
          <div>
            <h3>Tripetto ❤️ TypeScript</h3>
            <p>All FormBuilder SDK packages include type declarations and support <strong><a href="https://www.typescriptlang.org/" target="_blank" rel="noopener noreferrer">TypeScript</a> out of the box</strong>.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
