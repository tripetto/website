---
base: ../../
---

<section class="sdk-usage-builder" id="builder">
  <div class="container">
    <div class="row content">
      <div class="col">
        <h2 class="palette-sdk-builder">Add the builder library <span>to build forms in your project.</span></h2>
      </div>
    </div>
    <div class="row content">
      <div class="col-lg-11 col-xl-10">
        <p>Optionally add powerful form building capabilities to your HTML site by neatly integrating the visual form builder in minutes. <strong>The builder will run entirely inside your HTML project</strong> - with custom extensions you develop. All completely self-hosted, or run straight from a CDN.</p>
      </div>
    </div>
    <div class="row">
      <div class="col sdk-usage-builder-visual">
        <div class="sdk-device-code sdk-device-code-left">
          <img src="{{ page.base }}images/sdk-scenes/builder.webp" width="2000" height="1441" alt="Screenshot of the form builder." class="sdk-device-code-block sdk-device-code-device" loading="lazy" />
          <div class="sdk-code-snippet sdk-device-code-block sdk-device-code-snippet">
            <ul class="nav nav-tabs nav-fill sdk-code-snippet-nav sdk-code-snippet-nav-single" role="tablist">
              <li class="nav-item"><a class="nav-link active" id="nav-builder-html5" data-toggle="tab" href="#builder-html5" role="tab" aria-controls="builder-html5" aria-selected="true" title="Implement the builder with HTML">{% include icons/html5.html %}</a></li>
            </ul>
            <div class="tab-content sdk-code-snippet-content">
              <div class="tab-pane fade show active" id="builder-html5" role="tabpanel" aria-labelledby="nav-builder-html5">
                <ul class="nav nav-tabs sdk-code-snippet-subnav" role="tablist">
                  <li class="nav-item"><a class="nav-link active" id="nav-builder-html5-jsdelivr" data-toggle="tab" href="#builder-html5-jsdelivr" role="tab" aria-controls="builder-html5-jsdelivr" aria-selected="true">jsDelivr</a></li>
                  <li class="nav-item"><a class="nav-link" id="nav-builder-html5-unpkg" data-toggle="tab" href="#builder-html5-unpkg" role="tab" aria-controls="builder-html5-unpkg" aria-selected="false">unpkg</a></li>
                </ul>
                <div class="tab-content sdk-code-snippet-subcontent" id="builder-html5-content">
                  <div class="tab-pane fade show active" id="builder-html5-jsdelivr" role="tabpanel" aria-labelledby="nav-builder-html5-jsdelivr">
                    {% include sdk-code-snippet-pane.html active=true code="builder-html5-jsdelivr.html" language="html" palette="builder" url-run="https://codepen.io/tripetto/pen/2ff6822853f92be911c62605e68955b7" url-docs="https://tripetto.com/sdk/docs/builder/integrate/quickstart/html/" %}
                  </div>
                  <div class="tab-pane fade" id="builder-html5-unpkg" role="tabpanel" aria-labelledby="nav-builder-html5-unpkg">
                    {% include sdk-code-snippet-pane.html active=false code="builder-html5-unpkg.html" language="html" palette="builder" url-run="https://codepen.io/tripetto/pen/4de58f7369cace52dd68137aac0f7523" url-docs="https://tripetto.com/sdk/docs/builder/integrate/quickstart/html/" %}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {% include sdk-features-builder.html %}
  </div>
</section>
