---
base: ../
---

<section class="black-friday-subscribe">
  <div class="container">
    <div class="row">
      <div class="col-xl-10">
        <h2>Stay tuned for Tripetto news.</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 col-lg-7 col-xl-6">
        <p><a href="{{ page.base }}subscribe/">Subscribe here</a> to receive Tripetto updates (and possibly any deals in the future).</p>
      </div>
    </div>
    <div class="row">
      <div class="col-8">
        <img src="{{ page.base }}images/black-friday/black-friday-visual.svg" alt="Visual of 2 humans interacting." />
      </div>
    </div>
  </div>
</section>
