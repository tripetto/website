---
base: ../
---

<nav class="breadcrumb-navigation block-first template-dark" aria-label="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col">
        <ol>
          <li><a href="{{ page.base }}wordpress/">Tripetto WordPress plugin</a></li>
          <li><span>Black Friday Sale</span></li>
        </ol>
      </div>
    </div>
  </div>
</nav>

<section class="intro black-friday-intro intro-breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h1>Black Friday is not available right now.</h1>
        <p>But starting at ${{ site.pricing_wordpress_single }} per year Tripetto Pro is always worth it!</p>
        <a href="{{ page.base }}wordpress/pricing/" class="button button-wide">See Tripetto Pro pricing</a>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
