---
layout: blog-article
base: ../../
permalink: /blog/major-update-tripetto-as-gutenberg-block/
title: Major update! Tripetto as Gutenberg block - Tripetto Blog
description: With the release of version 5.0 of our WordPress plugin, Tripetto is now fully available as a Gutenberg block in the WordPress Gutenberg editor.
article_title: Major update! Tripetto as Gutenberg block
article_slug: "Update: WordPress Gutenberg block"
article_folder: 20210916
author: mark
time: 4
category: product
category_name: Product
tags: [product, product-update, feature]
areas: [wordpress]
year: 2021
---
<p>With the release of version 5.0 of our WordPress plugin, Tripetto is now fully available as a Gutenberg block in the WordPress Gutenberg editor. Not just to simply <a href="{{ page.base }}blog/contact-form-widget-wordpress/">embed a form</a>, but in such a way you don't have to leave the Gutenberg editor anymore to fully build, customize and automate your forms!</p>

<h2>Gutenberg?!</h2>
<p>Maybe Gutenberg does not ring a bell immediately... But chances are pretty high you use it every day in your WordPress site, without knowing it is called Gutenberg.</p>
<p>It's all about the editor you use in WordPress to compose your pages and posts. Since a few years the default editor of WordPress is called the <strong><a href="https://wordpress.org/support/article/wordpress-editor/" target="_blank" rel="noopener noreferrer">Gutenberg editor</a></strong>. That improved the way you can compose and publish the content on your WordPress site.</p>
<p>A big advantage of the Gutenberg editor is that WordPress plugins, like Tripetto, can integrate with the Gutenberg editor, by providing a Gutenberg block. And that's exactly what we release today with <strong>version 5.0 of our WordPress plugin</strong>!</p>
<hr/>

<h2>Tripetto's Gutenberg block</h2>
<p>Let's have a look at Tripetto's Gutenberg block. We could have just developed a simple Gutenberg block that makes it easy to select an existing form and embed it into your website. Of course that's also possible, but we went a little further!</p>

<h3>Everything's there 💪</h3>
<p>In Tripetto's Gutenberg block it's possible to do everything you need for your form, just from within the Gutenberg editor. Think of building your full form structure, switching form faces, styling all form elements, translating all form labels, configuring automations like notifications, connections and form activity tracking. Everything is just there! Without leaving the Gutenberg editor!</p>

<h3>Live preview 👀</h3>
<p>And the best thing is you see the live preview of your form the whole time while you're building/customizing/automating it. You'll constantly see and feel how your form will blend with the rest of your content.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/gutenberg.png" width="1200" height="760" alt="A screenshot of the Tripetto Gutenberg block" loading="lazy" />
  <figcaption>Simply add a Tripetto form with our Gutenberg block.</figcaption>
</figure>

<h3>Superpowers 🦸‍♀️</h3>
<p>We believe this really changes the way you can use Tripetto. From now on you can just simply go to the place in your site where you want to show a form and start it all up from there. Tripetto for Gutenberg is so powerful!</p>

<h3>Show me 😍</h3>
<p>Enough talking, let's have a look! <a href="{{ page.base }}help/articles/how-to-use-the-tripetto-form-builder-in-the-wordpress-gutenberg-editor/">Or read the Gutenberg help article for more instructions.</a></p>
<figure>
  <div class="youtube-player" data-id="pRtYDMSUAfI"></div>
  <figcaption>See Tripetto's Gutenberg block in action!</figcaption>
</figure>

<h2>Get started with Tripetto's Gutenberg block</h2>
<p>To use Tripetto as a Gutenberg block, update your WordPress plugin to version 5.0. If you have updated the plugin and you use the Gutenberg editor, you will see the Tripetto block right away in your pages/posts editor.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-tripetto-form-builder-in-the-wordpress-gutenberg-editor/">How to use the Tripetto form builder in the WordPress Gutenberg editor</a></li>
</ul>
<h3>Other updates</h3>
<p>Along the Gutenberg block, version 5.0 also includes numerous smaller and bigger improvements for the WordPress plugin, like totally redesigned headers in the plugin, a new style setting for mobile font size and the ability to customize the data columns in your results list. You can see all our updates in our <a href="{{ page.base }}changelog/">changelog</a>.</p>
