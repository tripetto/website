---
layout: blog-article
base: ../../
permalink: /blog/wordpress-contact-form-not-working/
title: Fix WordPress Contact Form - Complete Guide (2022) - Tripetto Blog
description: Your site needs forms. This post will show you how to fix your WordPress contact form if it’s not working!
article_title: How to Fix Your WordPress Contact Form if It's Not Working - Complete Guide (2022)
article_slug: WordPress contact form not working
article_folder: 20221129
article_image: cover.webp
article_image_width: 1640
article_image_height: 1081
article_image_caption: How to Fix Your WordPress Contact Form if It's Not Working - Complete Guide (2022)
author: jurgen
time: 12
category: guide
category_name: Guide
tags: [guide]
areas: [wordpress]
year: 2022
---
<p>Your site needs forms. This post will show you how to fix your WordPress contact form if it’s not working!</p>

<p>Your contact form is an excellent way to engage with your website’s users and visitors. This means you need the form to work without fail. As such, it can be a frustrating time when your WordPress contact form is not working properly, not to mention detrimental to your business.</p>
<p>The good news is that there are often some common reasons why a contact form won’t work properly. Often, you’ll have to troubleshoot issues with sending notifications and receiving the submissions to your contact form. Other times, there could be a display issue. You’re able to fix all of these easily with a little know-how (and the right WordPress contact form plugin).</p>
<p>For this tutorial, we’ll look at how to fix WordPress contact forms that aren’t working. We’ll also show you one of the best WordPress contact form plugins on the market, which can help you avoid some of the common problems which occur with contact forms in the first place. First, let’s discuss why a contact form might be one of the most important elements of your site.</p>
<hr/>

<h2>Why WordPress Contact Forms Are an Important Factor for Your Site</h2>
<p>A <a href="https://tripetto.com/contact/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">contact form</a> – and the same goes for almost any form on your site – is an excellent way to capture information from your users. Also, you have a direct way to engage from both sides.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/contact-form.png" width="1000" height="400" alt="A contact form on the front-end of a website." loading="lazy" />
  <figcaption>A contact form on the front-end of a website.</figcaption>
</figure>
<p>There are a few standout use cases for <a href="https://tripetto.com/blog/contact-form-generator/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">contact forms</a> that you’ll want to know about:</p>
<ul>
  <li>You’re able to <a href="https://tripetto.com/blog/lead-generation-form/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">generate leads</a>, and gather information that you’ll use in marketing campaigns.</li>
  <li><a href="https://tripetto.com/blog/wordpress-feedback-plugin/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">Customer feedback</a> can help you improve your products and services, and this information will come through a contact form. If you optimize your offerings based on this user feedback, you can increase your customer base, and your sales.</li>
  <li>Speaking of your customer base, a contact form is a top way to offer support. They can help with troubleshooting issues with your products and services, and the customer support you provide will likely be through an email service. This is one of the most direct ways to converse with your customers.</li>
</ul>
<p>However, if your WordPress contact form is not working, you won’t be able to leverage any of these use cases. Instead, you’ll need to diagnose the problem and begin to fix it. We’ll talk about these factors next.</p>
<hr/>

<h2>Some Common Issues If Your WordPress Contact Form is Not Working</h2>
<p>WordPress contact forms have immense value to your site and business. If yours doesn’t work, it can be a frustrating time that might even send you into a panic. There are a few good reasons for this:</p>
<ul>
  <li>If a user can’t contact you, it could make your business seem as though it ignores its customers or doesn’t care.</li>
  <li>Your <a href="https://www.semrush.com/blog/bounce-rate/" target="_blank" rel="noopener noreferrer">bounce rate</a> will suffer, as the user could head off-site without taking any action. In fact, they could go to a competitor, which means you’ll lose business (and money).</li>
  <li>Because you have a bottleneck of a WordPress contact form that’s not working, you’ll slow down the sales cycle, which will affect your income. All of the requests you’ll use the form for will grind to a halt.</li>
</ul>
<p>While there could be a lot of reasons why your WordPress contact form is not working, there are three that the rest of this article will look at. Here’s why they can be problematic:</p>
<ul>
  <li><strong>The form won’t display.</strong> Of course, a form that <a href="https://tripetto.com/help/articles/troubleshooting-form-not-showing-in-wordpress-site/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">doesn’t display</a> can’t help you collect leads and information. This can frustrate both ends of the chain: you and your users.</li>
  <li><strong>Your form doesn’t send emails for notifications.</strong> This keeps the user <a href="https://tripetto.com/help/articles/troubleshooting-email-notifications-from-wordpress-not-sent-or-marked-as-spam/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">out of the loop</a>, because they won’t know that you have their request, or have replied. You’ll want to care a lot about email deliverability if you offer support services through your contact form.</li>
  <li><strong>There are no responses within the WordPress admin screen.</strong> Despite knowing that you have some replies, you might not see them, or they may not save. This ties in with the last point. The form has no value at this point, and can ‘turn off’ your users while giving you a headache.</li>
</ul>
<p>Over the next few sections, we’ll show you how to fix these problems for your WordPress site. First, though, we’ll talk about a WordPress contact form plugin that can help mitigate many of these issues.</p>
<hr/>

<h2>3 Key Reasons Why Your WordPress Contact Form Is Not Working (And How to Fix Them)</h2>
<p>Let’s look at how to fix a WordPress contact form that’s not working, based on the three common reasons why in the last section.</p>
<p>However, before we begin, we’re going to introduce you to a near-perfect WordPress form builder plugin that we’ll mention at multiple points: <a href="https://tripetto.com/wordpress/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">Tripetto</a>.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-logo.png" width="1000" height="500" alt="The Tripetto logo." loading="lazy" />
  <figcaption>The Tripetto logo.</figcaption>
</figure>
<p>This is an all-in-one solution to create <a href="https://tripetto.com/blog/smart-form/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">smart, conversational forms</a> for a variety of purposes. Here’s what makes Tripetto a stand-out solution:</p>
<ul>
  <li>You can customize almost every aspect of your forms using <a href="https://tripetto.com/help/articles/how-to-style-your-forms/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">dedicated styling options</a>. You’re also able to present your forms based on the use case and your needs either by starting with one of Tripetto’s in-built templates, or better yet, by using one of the <a href="https://tripetto.com/help/articles/how-to-switch-between-form-faces/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">dedicated ‘form faces’</a>. This is functionality that’s unique to Tripetto.</li>
  <li>There are lots of advanced ways to apply <a href="https://tripetto.com/blog/elementor-form-conditional-logic/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">conditional logic</a> to your forms. This gives you a way to present relevant questions to the respondent, and personalize a contact form for specific use cases.</li>
  <li>You’ll find that Tripetto offers <a href="https://tripetto.com/wordpress/help/automating-things/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">lots of automation</a> for sending emails and notifications. You’re also able to use webhooks (and an app such as <a href="https://zapier.com" target="_blank" rel="noopener noreferrer">Zapier</a> or <a href="https://www.make.com/en?utm_source=tripetto-app&utm_medium=partner&utm_campaign=tripetto-app-partner-program" target="_blank" rel="noopener noreferrer">Make</a>) to connect to thousands of third-party services, such as <a href="https://mailchimp.com" target="_blank" rel="noopener noreferrer">Mailchimp</a>, <a href="https://sheets.google.com" target="_blank" rel="noopener noreferrer">Google Sheets</a>, and more.</li>
</ul>
<p>We’ll talk more in the following sections about <a href="https://tripetto.com/blog/wordpress-customizable-contact-form/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">how Tripetto can help</a> you resolve some of the common reasons your WordPress contact form is not working. However, everything you need from the plugin is within the WordPress dashboard. This includes data storage.</p>
<p>If you need support, you can find a wealth of articles and videos within the Tripetto <a href="https://tripetto.com/help/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">Help Center</a> too, along with dedicated support from the on-hand team.</p>

<h3>1. Your WordPress Contact Form Won’t Display Properly</h3>
<p>One of the more basic issues you’ll have to deal with is a contact form that doesn’t display, or contains errors in its layout. There are usually <a href="https://tripetto.com/help/articles/troubleshooting-form-not-showing-in-wordpress-site/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">three reasons</a> why this will happen:</p>
<ul>
  <li>You are suffering from a bug in an outdated version of the plugin.</li>
  <li>There’s an issue with the shortcode or widget you use to display the contact form.</li>
  <li>You might have a plugin conflict within your installation.</li>
</ul>
<p>Let’s take a quick look at each of these in turn.</p>
<h4>Handling Bugs</h4>
<p>The first issue is easy to solve. You’ll want to head to the Plugins screen within WordPress, and look at whether your contact form <a href="https://tripetto.com/help/articles/how-to-update-the-wordpress-plugin-to-the-latest-release/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">needs an update</a>. You’ll also want to do this for any page builder plugin you run, as the contact form will need that to work too. It should have a notification if this is the case:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/elementor-update.png" width="1000" height="241" alt="Updating the Elementor plugin within WordPress." loading="lazy" />
  <figcaption>Updating the Elementor plugin within WordPress.</figcaption>
</figure>
<p>This is a super simple way to iron out bugs with your plugins. From here, you can begin to check other elements.</p>
<h4>Checking Shortcodes</h4>
<p>When it comes to your shortcodes, this can be a more complex issue. The most obvious aspect to check is whether you have errors within the format of the shortcode itself. If so, you can correct them and test again.</p>
<p>However, in some cases, you may need to tweak the ‘parameters’ of a shortcode or widget to set things up as you like. This will involve some trial and error, but some plugins (such as Tripetto) can make the task easier.</p>
<p>For instance, there’s a <a href="https://tripetto.com/wordpress/help/sharing-forms-and-surveys/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">dedicated Share screen</a> within Tripetto to set options without the need for technical knowledge or manual typing:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-shortcode.png" width="1000" height="620" alt="Setting shortcode options within Tripetto." loading="lazy" />
  <figcaption>Setting shortcode options within Tripetto.</figcaption>
</figure>
<p>This can save you time, and ensure that you don’t make any careless errors when adding your shortcode to your posts and pages. In addition, you have a few <a href="{{ page.base }}blog/contact-form-widget-wordpress/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">different ways to display forms</a>, such as using an <a href="https://tripetto.com/help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-elementor-widget/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">Elementor widget</a>, a shortcode within your editor, or even a dedicated Block.</p>
<h4>Checking Plugin Conflicts</h4>
<p>You’ll also want to check whether there’s an incompatibility with other WordPress plugins on your system. Most often, you’ll find that caching plugins can cause a problem, along with minification plugins. Our advice is to turn these off on a temporary basis, test your contact form, then reintroduce them one at a time.</p>
<p>If you have to use these plugins alongside a contact form plugin, you can often exclude scripts from them to bypass those pages with forms. Of course, this will be dependent on the plugins you choose.</p>
<p>However, most of the work you’ll undertake to solve plugin conflicts will be manual. This means you’ll want to look at the front end of your site to note any clear visual issues, then begin to hunt for why this is the case.</p>
<p>In lots of cases though, you might not understand why an issue is arising, or even spot an error that needs a fix. Our advice here is to bring an experienced developer on board. That way, you can make sure everything is ship shape with your WordPress contact form before you move on.</p>

<h3>2. You Can’t Send or Receive Notification Emails</h3>
<p>WordPress has a complex email delivery system under the hood that has an elegant solution for developers and end users. Form builder plugins such as Tripetto rely on the WordPress standards regarding email functionality: the wp_mail() function. By default, the WP mail function of WordPress uses the PHP mailer function to create and send unauthenticated emails from the web server.</p>
<p>However, this can still bring about some issues:</p>
<ul>
  <li>Your WordPress hosting provider might not let you use this type of functionality.</li>
  <li>The configuration for the mailer might be broken or incorrect.</li>
  <li>You might have mistyped your <a href="https://www.cloudflare.com/learning/dns/what-is-dns/" target="_blank" rel="noopener noreferrer">Domain Name Server (DNS)</a> settings – a common issue.</li>
</ul>
<p>We have a <a href="https://tripetto.com/help/articles/troubleshooting-email-notifications-from-wordpress-not-sent-or-marked-as-spam/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">complete guide</a> on how to ensure that your WordPress installation can send emails in the correct way. Two easy tasks can be to check for plugin updates that might fix an issue like this, and to check your <a href="https://tripetto.com/blog/contact-form-spam-wordpress/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">inbox Spam folder</a>.</p>
<p>There are a few other elements you can check that might become more complex problems though. For example, you can check your WP Mail configuration, your WordPress log files, and your sender address.</p>

<h3>3. The Contact Form Results Don’t Save to WordPress</h3>
<p>In some cases, you may be able to display a form and see notifications in your inbox, but they won’t save to WordPress. A common resolution in the other steps is to check for plugin updates, and we’d suggest that here too.</p>
<p>You might also want to check for incompatibilities with other plugins too, and this time extend the selection to your security plugins. For example, <a href="https://wordfence.com" target="_blank" rel="noopener noreferrer">Wordfence</a> might not let your contact form plugin submit data. In these instances, you’ll want to head into the Wordfence > Firewall settings and enable Learning Mode:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/wordfence.png" width="1000" height="691" alt="The Wordfence Learning Mode setting." loading="lazy" />
  <figcaption>The Wordfence Learning Mode setting.</figcaption>
</figure>

<p>From here, submit a new request to your form, and Wordfence should understand that your contact form plugin wants to (and should) submit data.</p>
<p>Our <a href="https://tripetto.com/help/articles/troubleshooting-results-not-saved-to-wordpress-admin/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">dedicated guide</a> also highlights two other considerations you’ll want to investigate:</p>
<ul>
  <li><strong>Checking for form errors.</strong> We’re not talking about user errors here, but critical system errors. You’ll often see an error message, which will help you diagnose the issue further. However, it could be something out of your control, so you might need to contact the plugin’s support team.</li>
  <li><strong>Carry out a database check.</strong> In some cases, your database might be missing certain ‘tables’ that let it do its job. This is a technical task and one that we’d suggest you contact support for. It’s also one that is specific to the contact form plugin you use, so we’d come back to this one if you try every other method here, and have no luck.</li>
</ul>
<p>For Tripetto users, there’s a specific page where your form submissions live: the Results screen. In the next section, we’ll talk about some typical practices to test WordPress email forms, and where to find some of these options.</p>
<hr/>

<h2>Best Practices for Testing Your WordPress Contact Form</h2>
<p>You won’t want to wait for a problem before you fix it. Instead, it’s good to make sure that your WordPress contact form works before it goes live. To do this, you can carry out some checks before you launch the form. The first task is to make sure you can view the form, and that it looks as you’d expect.</p>
<p>You’ll want to look at how the form displays on the front end of your site on desktop, mobile, and tablet devices. You should use WordPress’ general preview options for this, but Tripetto includes a <a href="https://tripetto.com/help/articles/how-to-let-the-live-preview-work-for-you/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">live preview</a> within the storyboard too:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-preview.png" width="1000" height="605" alt="Choosing a viewport within Tripetto." loading="lazy" />
  <figcaption>Choosing a viewport within Tripetto.</figcaption>
</figure>
<p>There are selections to view the form using logic, and without. This gives you a good opportunity to test out your buttons, various fields, logic, and much more.</p>
<p>Once you have a handle on how your WordPress contact form looks, you’ll want to make sure a user can receive notifications. This is a simple task: Send test emails using your form, and check your inbox for the relevant notification. You might want to double-check that you have the right formatting and sending rules too. This is something <a href="https://tripetto.com/help/articles/how-to-automate-email-notifications-for-each-new-result/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">you can check</a> within Tripetto:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-notifications.png" width="1000" height="588" alt="Setting up email notifications within Tripetto." loading="lazy" />
  <figcaption>Setting up email notifications within Tripetto.</figcaption>
</figure>
<p>From there, you can check that the data you collect ends up in the right location: often within WordPress. Tripetto includes a dedicated Results screen that will show you this at a glance:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-results.png" width="1000" height="267" alt="The Tripetto Submissions screen." loading="lazy" />
  <figcaption>The Tripetto Submissions screen.</figcaption>
</figure>
<p>At this point, you’ll have all of the core functionality in place and working. This will let you take more of a hands-off approach, and you can let your form do the work, while you concentrate on your business.</p>
<hr/>

<h2>Avoid Common Contact Form Issues with Tripetto</h2>
<p>Your contact form is arguably the most vital component of your WordPress website. It can connect you to both current and potential customers, but it’s also vital that your contact form works properly from the off. A faulty contact form can impact both your revenue and your reputation.</p>
<p>This tutorial has looked at how to fix a WordPress contact form that’s not working. <a href="https://tripetto.com/wordpress/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">Tripetto</a> is the best plugin to help you solve the most common issues you’ll face. Its live preview can help you test your form before it goes live. What’s more, you have a foolproof way to set up email notifications, and read all of your form submissions in one location. The form builder plugin is an ideal way to create smart, conversational forms regardless of the application.</p>
<p>Tripetto starts from <a href="https://tripetto.com/wordpress/pricing/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working">$99 per year</a>, and comes with a 14-day money-back guarantee.</p>
<div>
  <a href="https://tripetto.com/wordpress/pricing/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-contact-form-not-working" class="blocklink">
    <div>
      <span class="title">Get Tripetto Pro for WordPress</span>
      <span class="description">Tripetto Pro is for the Tripetto WordPress plugin and comes in 3 all-inclusive variants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" />
    </div>
  </a>
</div>
