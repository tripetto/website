---
layout: blog-article
base: ../../
permalink: /blog/contact-form-best-practices/
title: Best Practices for Contact Forms (2022) - Tripetto Blog
description: Forms can increase engagement, and your contact forms are central to this. This post will discuss some essential contact form best practices!
article_title: Best Practices for Contact Forms - 15 Tips to Apply for Success in 2022
article_slug: Contact form best practices
article_folder: 20220922
article_image: cover.webp
article_image_width: 1640
article_image_height: 1081
article_image_caption: Best Practices for Contact Forms - 15 Tips to Apply for Success in 2022
author: jurgen
time: 13
category: guide
category_name: Guide
tags: [guide]
areas: [wordpress]
year: 2022
---
<p>Forms can increase engagement, and your contact forms are central to this. This post will discuss some essential contact form best practices!</p>

<p>If you only use one type of form on your WordPress website, a contact form is likely to be it. This is one of the most direct ways your site’s visitors can reach you. It’s also a viable tool for lead generation. However, if you want to maximize engagement and conversions, and minimize drop-offs, you’ll want to employ some contact form best practices.</p>
<p>While a contact form can seem straightforward, you can build in lots of complexity. For starters, you can think about your Call To Action button (CTA) and the layout as you would the entire page. From there, you can use advanced functionality such as conditional logic to improve the User Experience (UX) and boost your conversion rate.</p>
<p>In this tutorial, we’re going to show you a number of contact form best practices, and include examples along the way. Before that, let’s discuss why a quality contact form can be vital, and what goes into one.</p>
<hr/>

<h2>Why It’s Important to Use Quality Contact Forms on Your Website</h2>
<p>A contact form can be the link between your business and potential customers – and sales. Your contact form is the way to connect with users, regardless if they find you through Search Engine Optimization (SEO) tactics, social media, paid advertising, or even word of mouth.</p>
<p>However, contact form conversion rates are some of the lowest around. A typical rate is <a href="https://www.webfx.com/blog/marketing/contact-form-conversion-rate/" target="_blank" rel="noopener noreferrer">around 3–5 percent</a>, so you’ll want to make the path to completion easy. The internet in general delivers information overload and sensory overwhelm. This means any excuse a user has to skip your web form is valid: It could be too long, difficult to parse, or even suffer with its navigation.</p>
<p>However, if you can convert as many users as possible, you have the opportunity to <a href="https://tripetto.com/blog/lead-generation-form/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=contact-form-best-practices">turn them into leads</a>, potential customers, and new sales.</p>
<hr/>

<h2>The Components of a Great Contact Form</h2>
<p>A <a href="https://tripetto.com/blog/contact-form-generator/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=contact-form-best-practices">stellar contact form</a> should have a good design, an easy-to-follow experience, and great content and copy. The form should guide the user by the hand, such as what fields to fill out, and how to best complete them.</p>
<p>What’s more, a good contact form should present all of the relevant and required information in as few steps as possible. On the whole, a high-converting contact form should encourage the user to fill it out and become a new lead. With some contact form design best practices in mind, you can also create a form that delivers the most conversions possible.</p>
<hr/>

<h2>15 Contact Form Best Practices You’ll Want to Use On Your Site</h2>
<p>Over the rest of this tutorial, we’ll show you 15 contact form best practices. They’re in no particular order, so we encourage you to read all of them – and apply them to your own forms.</p>

<h3>1. Use Smart Forms</h3>
<p><a href="https://tripetto.com/blog/smart-form/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=contact-form-best-practices">Smart forms</a> can turn your contact form into a personal experience that can boost your completion rates. This type of form will guide the user to an answer, based on their replies.</p>
<p>Because a smart form tailors its content to the user, you’ll often see better conversion rates, and higher quality answers. This feeds back into your own marketing and business strategies.</p>
<p>The <a href="https://tripetto.com/wordpress/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=contact-form-best-practices">Tripetto</a> form builder plugin has lots of ways to make your form <a href="https://tripetto.com/help/articles/how-to-make-your-forms-smart-and-conversational/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=contact-form-best-practices">smart and conversational</a>. For example, you can use <a href="https://tripetto.com/help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=contact-form-best-practices">piping logic</a> or <a href="https://tripetto.com/help/articles/discover-the-power-of-branches-for-your-logic/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=contact-form-best-practices">branch logic</a> to move the user around your form in the most relevant way:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/branch-logic.png" width="1000" height="657" alt="Using branch logic in the Tripetto storyboard." loading="lazy" />
  <figcaption>Using branch logic in the Tripetto storyboard.</figcaption>
</figure>
<p>In fact, this isn’t the only application for Tripetto’s conditional logic functionality, as you’ll see next.</p>

<h3>2. Show Fields Using Conditional Logic and Personalization</h3>
<p>Tripetto’s <a href="https://tripetto.com/blog/forms-logic/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=contact-form-best-practices">conditional logic</a> can also help you to show and hide form fields based on certain criteria, such as the status of the response to a question. This can let you create a mammoth form that will only show relevant questions and sections to the respondent.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/conditional-logic.gif" width="1000" height="553" alt="A GIF showing and hiding fields within Tripetto." loading="lazy" />
  <figcaption>A GIF showing and hiding fields within Tripetto.</figcaption>
</figure>
<p>What’s more, you can personalize your form using conditional logic too. For instance, you can ask for a user’s name, and reference that throughout your form.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/piping-logic.png" width="1000" height="502" alt="Asking for a name in Tripetto, and displaying it to the user." loading="lazy" />
  <figcaption>Asking for a name in Tripetto, and displaying it to the user.</figcaption>
</figure>
<p>You can also take this further, and show different questions based on a user’s given title, gender, and more. Tripetto’s conditional logic functionality has no restrictions, which means you can create custom forms for your exact purpose and needs, and make them as user-friendly as possible.</p>

<h3>3. Put Your CTA Above the Fold</h3>
<p>Lots of site owners use a contact form as a kind of CTA. As such, you’ll often find it down at the bottom of the page, or even hidden on a separate contact page, as the ‘period’ to the sales pitch of your site.</p>
<p>However, typical <a href="https://99designs.co.uk/blog/web-digital/website-layout-fundamentals/" target="_blank" rel="noopener noreferrer">‘web design 101’</a> suggests that elements of your site ‘above the fold’ – i.e. part of the page you see first – will experience greater engagement.</p>
<p>Maximizing impressions is the key to increasing your conversions. This means you’ll want to put your contact form where users will see it with ease.</p>

<h3>4. Test Your Contact Form Before It Goes Live</h3>
<p>Here’s a quick tip that can benefit <a href="https://tripetto.com/blog/most-common-types-forms/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=contact-form-best-practices">any type of form</a>, not just contact forms. You’ll always want to make sure your form works before you make it live. It might seem like an obvious tip, but without testing, you can fall foul of bugs and errors.</p>
<p>The most straightforward way to test your form is to submit a query to it, as the end user would. Tripetto makes this simple, using its Test with Logic section of the <a href="https://tripetto.com/help/articles/how-to-let-the-live-preview-work-for-you/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=contact-form-best-practices">preview pane</a>:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/preview.png" width="1000" height="575" alt="Submitting a form using the Tripetto preview pane." loading="lazy" />
  <figcaption>Submitting a form using the Tripetto preview pane.</figcaption>
</figure>
<p>Make sure you also check that you receive the submission. Tripetto stores all submissions within WordPress, so you can see everything at a glance. Once you check whether you have received the submission in your inbox, and within your plugin’s dashboard, you can make your form live.</p>

<h3>5. Use ‘First-Person’ Language Within Your Form</h3>
<p>Most web content benefits from using second-person language, from the viewpoint of the reader. This uses “you” as the primary pronoun. The result is that content reads as though it’s a conversation, rather than taking place inside an individual’s head.</p>
<p>However, first-person content can resonate more in certain circumstances. For example, one a/b testing case study showed a <a href="https://unbounce.com/a-b-testing/failed-ab-test-results/" target="_blank" rel="noopener noreferrer">90 percent increase</a> in form conversion rate when switching to using first-person language.</p>
<p>You might want to use this in situations where you would like a conversion or a positive action. “Sign <strong>me</strong> up for the newsletter” could resonate more than “Sign up for the newsletter, ” and end with a new conversion.</p>

<h3>6. Make Sure Your Form Matches the Rest of Your Site</h3>
<p>If you look into optimizing your forms, you will stumble across aspects such as <a href="https://www.optimizely.com/insights/blog/design-principles-you-should-test-color-contrast/" target="_blank" rel="noopener noreferrer">color contrast</a>. This could lead you to design a contact form that looks different from the rest of your site. However, this might harm your User Interface (UI) and page design, and by extension, your conversion rate.</p>
<p>This can be similar if you use a <a href="https://tripetto.com/help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-elementor-widget/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=contact-form-best-practices">third-party plugin</a> or custom Block within WordPress. You’ll want to have synergy between your contact form and your site’s design.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/styling.png" width="1000" height="708" alt="Color options within Tripetto." loading="lazy" />
  <figcaption>Color options within Tripetto.</figcaption>
</figure>
<p>Tripetto slots into your current design choices and workflow. Its wealth of design options, templates, and more can help you match your site’s design, and keep everything in harmony. It also works with nearly every available WordPress theme.</p>

<h3>7. Design Vertical Forms</h3>
<p>This is a bold statement, but the internet exists only in one plane: the vertical one. We all scroll top to bottom, so this contact form best practice should be straightforward. You should design a vertical form, rather than one with horizontal elements. <a href="https://www.cxpartners.co.uk/our-thinking/web_forms_design_guidelines_an_eyetracking_study/" target="_blank" rel="noopener noreferrer">Eye-tracking studies</a> confirm this too.</p>
<p>This will be most relevant if you have a premium on space. The logical solution is to fill the horizontal space with fields. However, users won’t respond as well to this type of design.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/autoscroll.png" width="1000" height="649" alt="Creating a form with an autoscroll form face in Tripetto." loading="lazy" />
  <figcaption>Creating a form with an autoscroll form face in Tripetto.</figcaption>
</figure>
<p>Instead, the better option is to break your form into <a href="https://tripetto.com/blog/wordpress-multi-step-form/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=contact-form-best-practices">multiple pages or sections</a>. Tripetto includes unique <a href="https://tripetto.com/help/articles/how-to-switch-between-form-faces/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=contact-form-best-practices">‘form faces’</a> for this purpose. For example, you can use either the autoscroll or chat form face to create natural focus and save some space.</p>

<h3>8. Track Your CTA Clicks as Custom Events</h3>
<p>Analytics can help you create better forms, which will result in a higher conversion rate. As such, you’ll want to set up a custom event within <a href="https://analytics.google.com" target="_blank" rel="noopener noreferrer">Google Analytics</a> (or your favorite analytics tool) to tally the click on any relevant contact form CTA button.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/google-analytics-custom-events.png" width="1000" height="557" alt="Monitoring a custom event in Google Analytics." loading="lazy" />
  <figcaption>Monitoring a custom event in Google Analytics.</figcaption>
</figure>
<p>At its core, you can measure your completion rate through a comparison between the number of clicks to your form, and the number of users who fill it out. From there, you can begin to carry out further testing (such as split testing) to refine your content copy, design, structure, and more.</p>
<p>Tripetto includes <a href="https://tripetto.com/help/articles/how-to-measure-completion-rates-with-form-activity-tracking/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=contact-form-best-practices">built-in functionality</a> to capture users who start a form, and those who complete it. This gives you a simple way to optimize your forms, right from your WordPress dashboard, to increase the number of completed form submissions you receive.</p>

<h3>9. Don’t Ask for Phone Numbers Within Your Contact Form</h3>
<p>Your contact form should offer the least resistance between the user’s willingness and the Send Form button. One way you can do this is to keep the information you request relevant.</p>
<p>For many businesses, it makes sense to request a telephone number in order to follow up. However, lots of users won’t want to give this up. Because phone numbers have an association with sales calls and a general invasion of privacy, a user might choose to leave your form rather than hand theirs over.</p>
<p>Instead, we recommend you ask for an email address. This can achieve the same goals, and it’s something a potential customer will be more willing to send you. If a phone number has some importance, make the field optional (<a href="https://tripetto.com/help/articles/how-to-use-the-phone-number-block/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=contact-form-best-practices">as you can</a> with Tripetto).</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/telephone-number.png" width="1000" height="570" alt="Entering a telephone number using a specific format with Tripetto’s Phone Number Block." loading="lazy" />
  <figcaption>Entering a telephone number using a specific format with Tripetto’s Phone Number Block.</figcaption>
</figure>
<p>This way, the respondent can fill the form out, and you can send an email at a later date to request further contact information (and say why you need it).</p>

<h3>10. Make Sure Your Forms Work on All Devices</h3>
<p>Around <a href="https://www.statista.com/statistics/277125/share-of-website-traffic-coming-from-mobile-devices/" target="_blank" rel="noopener noreferrer">60 percent of web traffic</a> is from a mobile device. Some niches will see less or more, but on the whole a good contact form best practice is to design with a ‘mobile-first’ philosophy.</p>
<p>If your forms don’t offer mobile devices an easy way to fill out your form, those users will head elsewhere. The good news is that Tripetto builds responsive design into every form you build. You can see how your contact form looks using the live preview:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/devices.png" width="1000" height="733" alt="Choosing different viewports within Tripetto." loading="lazy" />
  <figcaption>Choosing different viewports within Tripetto.</figcaption>
</figure>
<p>What’s more, there is a simple set of options to switch between desktop, tablet, and mobile devices on the fly while you design. This gives you a quick way to see your forms as the end user will see them.</p>

<h3>11. Read Eye-Tracking Studies for Better Contact Form Design</h3>
<p>An earlier contact form best practice talks about eye-tracking, and this is something of immense value. These <a href="https://html.com/blog/23-scientific-website-eye-tracking-studies/" target="_blank" rel="noopener noreferrer">types of studies</a> reveal how a user will subconsciously view your landing page, or even your entire website. You can gauge where they look, how long for, and other elements.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/heatmap.jpg" width="1000" height="790" alt="A heatmap of an eye-tracking experiment." loading="lazy" />
  <figcaption>A heatmap of an eye-tracking experiment.</figcaption>
</figure>
<p>While you do want to focus on the big picture with regards to your form, eye-tracking studies can help you drill down to the micro-moments. These small changes can make or break your form, and raise your completion rates to boot.</p>

<h3>12. Use Rounded Boxes</h3>
<p>Next, a quick contact form best practice that takes seconds to implement. Rather than use squared buttons or input field corners, choose rounded ones instead – even if the rounding is subtle. The human eye can process these better than squared-off boxes.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/styling-buttons.png" width="1000" height="495" alt="Changing the style of a Submit button in Tripetto." loading="lazy" />
  <figcaption>Changing the style of a Submit button in Tripetto.</figcaption>
</figure>
<p>It’s an aesthetically pleasing element that you can add to your own forms using Tripetto’s options. You have lots of ways to customize your buttons and fields, which means you won’t have to suffer with poor, unprofessional design.</p>

<h3>13. Allow for Open Formatting</h3>
<p>Your respondents should have the freedom to type whatever response they wish into your contact form’s fields. As such, you’ll want to make sure you offer open formatting where necessary, for example, using a Tripetto <a href="https://tripetto.com/help/articles/how-to-use-the-text-multiple-lines-block/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=contact-form-best-practices">multi-line text box</a>.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/textarea.png" width="1000" height="260" alt="Typing into a freeform text box within Tripetto." loading="lazy" />
  <figcaption>Typing into a freeform text box within Tripetto.</figcaption>
</figure>
<p>Of course, there are some situations where you can’t do this, such as when you need to ask for an email address or telephone number. However, if you ask the user to conform to a certain standard elsewhere in your form and they don’t want to (or can’t,) you risk them becoming another statistic of your bounce rate.</p>

<h3>14. Consider Multiple Pages for Your Form</h3>
<p>We talk about structure a lot in this post, but it bears repeating again: users don’t like to fill out long forms. A good way to avoid your multi-step form appearing too long is to split it over multiple pages – you could use the Classic or Autoscroll <a href="https://tripetto.com/form-layouts/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=contact-form-best-practices">form faces within Tripetto</a>.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/multipages.png" width="1000" height="644" alt="Splitting a form over multiple pages within Tripetto." loading="lazy" />
  <figcaption>Splitting a form over multiple pages within Tripetto.</figcaption>
</figure>
<p>Multi-page forms have a much better conversion rate than a typical contact form, so you’ll want to consider using this when you have a high number of fields and question types. </p>

<h3>15. Keep Your Contact Form Simple and Straightforward</h3>
<p>Our final contact form best practice is simple. It’s a good idea to keep your contact form straightforward, easy to understand, and a doddle to use. This means you’ll want to consider the following:</p>
<ul>
  <li>Look to cut out verbose language that includes lots of information. Natural language will give users confidence, and ensure they reach the end of your contact form.</li>
  <li>Simple language needs less commitment too, as the user can read fast, and understand the context you provide.</li>
</ul>
<p>We state it earlier, but also keep unnecessary fields off of your contact form. Give the user the path of least resistance, and you’ll skyrocket your conversion rates.</p>
<hr/>

<h2>Boost customer engagement with contact form best practices</h2>
<p>For inbound marketing and sales applications, a contact form is a vital cog in the wheel. It’s also great for engaging with new potential customers. However, abandonment rates for contact forms can be high. As such, you’ll want to create the best contact form you can, using some of the best practices from this post, to keep your abandonment rate percentage as low as possible.</p>
<p><a href="https://tripetto.com/wordpress/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=contact-form-best-practices">Tripetto</a> is a form builder plugin for WordPress that can help you apply all of the contact form best practices in this list. For example, it lets you build smart, conversational forms that include advanced conditional logic and calculations. What’s more, you have swathes of design options and the plugin integrates with almost any current page builder WordPress plugin. This means you can build everything from simple contact forms to complex multi-page lead generators using your current setup, and no code.</p>
<p>A <a href="https://tripetto.com/wordpress/pricing/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=contact-form-best-practices">single-site license</a> for Tripetto starts from $99 per year. You can access the full feature set of the plugin, regardless of the tier you choose. What’s more, you get a 14-day, money-back guarantee on every purchase – no questions asked.</p>
<div>
  <a href="https://tripetto.com/wordpress/pricing/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=contact-form-best-practices" class="blocklink">
    <div>
      <span class="title">Get Tripetto Pro for WordPress</span>
      <span class="description">Tripetto Pro is for the Tripetto WordPress plugin and comes in 3 all-inclusive variants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" />
    </div>
  </a>
</div>
