---
layout: blog-article
base: ../../
permalink: /blog/wordpress-customizable-contact-form/
title: 8 Best Customizable WordPress Contact Form Plugins - Tripetto Blog
description: A website will always need a way for visitors to get in touch with you. This post will look at some of the best WordPress customizable contact form plugins!
article_title: The 8 Best Customizable WordPress Contact Form Plugins (2022)
article_slug: WordPress customizable contact form plugins
article_folder: 20220621
article_image: cover.webp
article_image_width: 1640
article_image_height: 1082
article_image_caption: The 8 Best Customizable WordPress Contact Form Plugins (2022)
author: jurgen
time: 13
category: comparison
category_name: Comparison
tags: [comparison]
areas: [wordpress]
year: 2022
---
<p>A website will always need a way for visitors to get in touch with you. This post will look at some of the best WordPress customizable contact form plugins!</p>

<p>Interactive site elements are one of the key essentials of a modern website. In particular, contact forms help to connect you with your site’s visitors. With good placement, you can improve both the number of leads you win and your bottom line. As such, a WordPress customizable contact form plugin is a vital component.</p>
<p>If you couple the right plugin with optimal placement of the form, you can increase the reach of your website, automate your email responses, and segment your audience in a beneficial way. It helps to better simplify your lead generation efforts.</p>
<p>For this post, we’re going to showcase some of the best WordPress customizable contact form plugins available and help you decide which one is best for your needs. First though, let’s look at why you’d want to use a plugin to display contact forms on your site.</p>
<hr/>

<h2>Why You Should Use a Contact Form Plugin on Your Site</h2>
<p>WordPress is a full-featured platform, it doesn’t come with any way to <a href="https://tripetto.com/help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-shortcode/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-customizable-contact-form">display a contact form</a> without third-party support. While this might seem like a big omission, the reality is that a dedicated contact form plugin can achieve much more than vanilla functionality within the platform.</p>
<p>In fact, there are lots of markers for a good WordPress contact form plugin:</p>
<ul>
  <li>You’ll have the ability to customize how the form will look on the front end, including the layout, form fields, design elements, and more.</li>
  <li>In addition, you can decide how the form works under the hood. This means you can implement functionality such as <a href="https://tripetto.com/help/articles/how-to-use-the-calculator-block/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-customizable-contact-form">advanced calculators</a>, <a href="https://tripetto.com/blog/elementor-form-conditional-logic/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-customizable-contact-form">conditional logic</a>, dynamic variables, and more.</li>
  <li>The best plugins of all stripes will offer user-friendly and straightforward usability, provide compatibility with your existing WordPress themes and plugins, and let you implement all of the functionality without the need for coding knowledge.</li>
</ul>
<p>As such, a quality WordPress customizable contact form plugin will make your site more flexible, dynamic, and relevant to your needs.</p>
<hr/>

<h2>8 of the Best WordPress Customizable Contact Form Plugins on the Market</h2>
<p>Over the rest of this article, we’re going to dive into some of the best WordPress customizable contact form plugins available. Throughout, we’ll list out the key functionality for each solution, and offer a comparison where relevant.</p>
<p>Let’s begin with our favorite, and one we’ll mention again at the end of the post.</p>

<h3>1. Tripetto</h3>
<p>In our opinion, <a href="https://tripetto.com/wordpress/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-customizable-contact-form">Tripetto</a> is one of the most full-featured and complete WordPress customizable contact form plugins around. It comes with almost every feature you need to build all sorts of contact forms.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto.png" width="1544" height="500" alt="The Tripetto plugin." loading="lazy" />
  <figcaption>The Tripetto plugin.</figcaption>
</figure>
<p>What’s more, its feature set is suited to questionnaires, surveys, customer support forms, and much more. This is thanks to its unique array of qualities:</p>
<ul>
  <li>You can work with Tripetto as though you are building a flowchart, thanks to the drag-and-drop <a href="https://tripetto.com/help/articles/how-to-build-your-forms-in-the-form-builder/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-customizable-contact-form">storyboard interface</a>.</li>
  <li>You’ll use ‘<a href="https://tripetto.com/help/articles/how-to-switch-between-form-faces/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-customizable-contact-form">form faces</a>’ to change the overall experience of your form. For example, you can select a typical ‘Classic’ form face, an autoscroll option, or a chat form face that lets you present your form as a series of questions and answers, as though you’re having a conversation.</li>
  <li>It has all of the elements you need to build both basic and complex forms, including conversational and smart types.</li>
  <li>If you have a favorite third-party service that could benefit from Tripetto’s workflow, you can <a href="https://tripetto.com/help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-customizable-contact-form">integrate thousands</a> of them using webhooks, with no need for code.</li>
  <li>There’s flexibility with regards to displaying your form, regardless of whether you use the <a href="https://tripetto.com/help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-gutenberg-block/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-customizable-contact-form">Block Editor</a>, <a href="https://tripetto.com/help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-elementor-widget/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-customizable-contact-form">Elementor</a>, or any other WordPress page builder plugin.</li>
  <li>Each form will comply with a number of data regulations, and your data stays within WordPress. This lets you rest easy that your user’s information sticks with you.</li>
</ul>
<p>Tripetto has lots of ways to add advanced functionality to your forms. <a href="https://tripetto.com/help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-customizable-contact-form">Logic is one area</a> Tripetto is near-perfect for, and it includes a number of logic options for your forms:</p>
<ul>
  <li>You can jump questions to offer something more relevant to your respondents.</li>
  <li>You’re able to evaluate answers and recall values to guide users in the right direction within your form.</li>
  <li>Speaking of which, your form can branch off based on the answers you receive.</li>
  <li>There’s an option to use hidden fields, which help when you want to set choices behind the scenes or provide values to the advanced calculator.</li>
  <li>You can automate your form through the use of notifications. This means you won’t miss a response and can focus on other areas of your site in the meantime.</li>
</ul>
<p>Tripetto is a premium plugin with a budget-friendly price. A <a href="https://tripetto.com/wordpress/pricing/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-customizable-contact-form">single-site license</a> is $99 per year, and you get the full cache of powerful features and functionality. We don’t restrict functionality based on how much you pay, unlike other plugins. What’s more, each purchase comes with a 14-day money-back guarantee, so there’s no risk involved.</p>

<h3>2. Gravity Forms</h3>
<p>If you’re a WordPress user who wants a premium contact form plugin, <a href="https://www.gravityforms.com/" target="_blank" rel="noopener noreferrer">Gravity Forms</a> will likely be on your shortlist.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/gravityforms.png" width="1000" height="660" alt="The Gravity Forms plugin." loading="lazy" />
  <figcaption>The Gravity Forms plugin.</figcaption>
</figure>
<p>It’s a perennial option for many, and comes with its own unique blend of features and functionality:</p>
<ul>
  <li>You can design forms fast from the WordPress dashboard, using a drag-and-drop builder. What’s more, you can choose to make your form paginated and with multiple columns, to pack in as much information as possible.</li>
  <li>There are 30-plus form fields at your disposal, so you will have nearly everything you need on hand to build your forms.</li>
  <li>If you have some CSS knowledge, you can employ it to customize your forms’ designs.</li>
  <li>You have options to capture data from respondents who don’t finish a form, so you can nudge them to come back and finish the job.</li>
  <li>There are field validators, dynamic values, and more on hand to help make sure you capture the right data.</li>
</ul>
<p>Despite the clear benefits of Gravity Forms, there are <a href="https://tripetto.com/gravityforms-alternative/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-customizable-contact-form">two linked drawbacks</a>. First, the plugin is premium only. This isn’t a problem per se, but it also comes with a <a href="https://www.gravityforms.com/pricing/" target="_blank" rel="noopener noreferrer">hefty price tag</a>. While a Basic license is $59 per year, this comes with barely any functionality. For the same experience as Tripetto, you’ll need to opt for the Elite license at $259 per year.</p>

<h3>3. Contact Form 7</h3>
<p>We’d bet that most first-time WordPress users will at least seriously consider – and often choose – <a href="https://wordpress.org/plugins/contact-form-7/" target="_blank" rel="noopener noreferrer">Contact Form 7</a> as their WordPress form plugin. In fact, it has a mammoth number of installs, which proves that the simple contact form plugin has staying power among all sorts of site owners.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/contactform7.png" width="1000" height="322" alt="The Contact Form 7 plugin." loading="lazy" />
  <figcaption>The Contact Form 7 plugin.</figcaption>
</figure>
<p>It comes with essential functionality, albeit wrapped in a dated presentation:</p>
<ul>
  <li>You’re able to create multiple forms and host them on your WordPress website.</li>
  <li>You can use simple markup to customize aspects of the form.</li>
  <li>There are ways to connect it to other services and plugins.</li>
</ul>
<p>Because Contact Form 7 is popular, there are lots of integrations with other third-party services. This makes the plugin a good choice if you need something basic, but once your <a href="https://tripetto.com/contactform7-alternative/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-customizable-contact-form">needs become greater</a>, there are fewer prospects.</p>
<p>For example, you’ll need to use HTML, CSS, and even PHP and JavaScript if you want to customize the form. There is no drag-and-drop functionality, nor any visual way to build your form.</p>
<p>Also, there is almost no advanced functionality built into the plugin. This means you can’t build conversational or smart forms, nor store submissions unless you use another plugin (that has poor reviews.)</p>
<p>While Contact Form 7 is a free plugin, it would suit only the most basic forms. For more sophisticated needs, you’ll likely want to opt for another solution.</p>

<h3>4. Ninja Forms</h3>
<p><a href="https://ninjaforms.com/" target="_blank" rel="noopener noreferrer">Ninja Forms</a> is a plugin that lots of users gravitate to once they move on from Contact Form 7. This is because it offers more functionality, a visual builder, and better customization options.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/ninjaforms.png" width="1000" height="322" alt="The Ninja Forms plugin." loading="lazy" />
  <figcaption>The Ninja Forms plugin.</figcaption>
</figure>
<p>At its core, Ninja Forms is a good addition to your site if you want to build simple forms that look great:</p>
<ul>
  <li>There’s a dedicated control panel to manage your forms.</li>
  <li>You have some flexibility when it comes to styling. Ninja Forms often looks good on your site without any further customization, and you can use pre-built templates to begin your design.</li>
  <li>There is a big add-on library that helps you to boost Ninja Forms’ functionality.</li>
  <li>You can export your submissions if you want to import them elsewhere or save them.</li>
</ul>
<p>If you want to further customize your forms, there are advanced fields to incorporate HTML tags, set hidden fields, and more. However, most of the <a href="https://tripetto.com/ninjaforms-alternative/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-customizable-contact-form">functionality you’ll need</a> to build smart and conversational forms is behind a paywall.</p>
<p>You’ll need to pay at least <a href="https://ninjaforms.com/pricing/" target="_blank" rel="noopener noreferrer">$99 per year</a> for a limited set of features. The full Ninja Forms experience can be yours for $599 per year.</p>

<h3>5. Formidable Forms</h3>
<p>Users who don’t like the look and feel of premium WordPress plugins such as Gravity Forms will often turn to <a href="https://formidableforms.com/" target="_blank" rel="noopener noreferrer">Formidable Forms</a> to get the job done. It offers many of the same features and functionality and comes with plenty of scope to create smart forms.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/formidableforms.png" width="1000" height="465" alt="The Formidable Forms plugin." loading="lazy" />
  <figcaption>The Formidable Forms plugin.</figcaption>
</figure>
<p>We’re not able to cover everything in the box, but there are a few standout facets to note:</p>
<ul>
  <li>You’ll use a combination of contact form templates, drag-and-drop builder, and comprehensive styling options to design your forms.</li>
  <li>You’re able to use a bunch of different form fields, including images on radio buttons, and all of the necessary functionality to create polls, quizzes, surveys, and more.</li>
  <li>There are conditional logic and advanced features built in. For example, you can use calculators with your forms.</li>
  <li>There’s also a full-featured response system that includes the ability to schedule and time limit your responses, harness spam protection, integrate geolocating, and much more.</li>
  <li>The ability to manage your email notifications is also here, including dedicated email logic.</li>
</ul>
<p>For the <a href="https://formidableforms.com/pricing/" target="_blank" rel="noopener noreferrer">full experience</a>, you’ll pay $399 per year. However, there are feature-limited plans available for a better price: The Basic plan comes in at $79 per year.</p>
<p>There is one potential drawback we should mention: The implementation of conditional field values is not as intuitive as Tripetto. The way you add form values together, then have to redirect to a success page could be better. As such, you might spend a longer time creating your smart forms than you’d like.</p>

<h3>6. WPForms</h3>
<p>We think that many users will turn to <a href="https://wpforms.com/" target="_blank" rel="noopener noreferrer">WPForms</a> just as much as options such as Ninja Forms. This is helped by beginner-friendly, rich functionality that you’ll get to grips with fast.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/wpforms.png" width="1000" height="323" alt="The WPForms plugin." loading="lazy" />
  <figcaption>The WPForms plugin.</figcaption>
</figure>
<p>The WordPress plugin feels great to use, thanks to its interface and development team. What’s more, there are lots of features you’ll use every day:</p>
<ul>
  <li>You’re able to split your layout into multi-page forms, which means you can present more information without overwhelming the respondent.</li>
  <li>There is a ‘user journey’ overview, which helps you see how your respondents use your forms.</li>
  <li>There are plenty of add-ons, in order to integrate with services such as <a href="https://paypal.com" target="_blank" rel="noopener noreferrer">PayPal</a>, <a href="https://stripe.com" target="_blank" rel="noopener noreferrer">Stripe</a>, <a href="https://mailchimp.com" target="_blank" rel="noopener noreferrer">Mailchimp</a> (and other email marketing services,) <a href="https://zapier.com" target="_blank" rel="noopener noreferrer">Zapier</a>, and more.</li>
</ul>
<p><a href="https://wpforms.com/pricing/" target="_blank" rel="noopener noreferrer">As for pricing</a>, you can get the full set of WPForms features for $399 per year. However, there’s plenty on offer for the $79 per year Basic tier too, albeit with limited features. The <a href="https://tripetto.com/wpforms-alternative/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-customizable-contact-form">only other drawback</a> is that there is no free version or trial of the plugin.</p>

<h3>7. Everest Forms</h3>
<p><a href="https://wpeverest.com/wordpress-plugins/everest-forms" target="_blank" rel="noopener noreferrer">Everest Forms</a> might be a solution that flies under your radar, but it has a wealth of fantastic reviews on WordPress.org and comes with a solid feature set.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/everestforms.png" width="1000" height="322" alt="The Everest Forms plugin." loading="lazy" />
  <figcaption>The Everest Forms plugin.</figcaption>
</figure>
<p>It’s more of a different ‘flavor’ of plugin, rather than an innovative solution. However, there are a few standout aspects to note:</p>
<ul>
  <li>There’s an email template designer that lets you expand your designers across multiple channels.</li>
  <li>You can collect digital signatures directly from your forms.</li>
  <li>You’re able to let users save a form and continue it at another time.</li>
</ul>
<p><a href="https://wpeverest.com/wordpress-plugins/everest-forms/pricing/" target="_blank" rel="noopener noreferrer">Pricing is competitive</a>, and works out a great deal for the Professional plan at $199 per year. This gives you everything relating to Everest Forms. However, the free version has a number of limitations. As such, you’d need to spend in order to harness everything Everest Forms has to offer.</p>

<h3>8. Jetpack</h3>
<p>The <a href="https://wordpress.org/plugins/jetpack/" target="_blank" rel="noopener noreferrer">Jetpack plugin</a> is one that many WordPress users install because it offers so much functionality in one box. You’ll also get a contact form in there, which could serve you well for basic needs.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/jetpack.png" width="1000" height="323" alt="The Jetpack plugin." loading="lazy" />
  <figcaption>The Jetpack plugin.</figcaption>
</figure>
<p>Despite there being a lack of styling options, the contact form section of the plugin does have some benefits:</p>
<ul>
  <li>First, you’ll have an almost full guarantee that your form will work with your WordPress website.</li>
  <li>You’ll be able to manage your forms from within a familiar WordPress interface.</li>
  <li>There will be a Block for the default editor, and a widget in some circumstances.</li>
  <li>You get spam protection and filtering courtesy of the excellent Akismet.</li>
</ul>
<p>However, if you don’t want to use the other features of Jetpack – and there are a lot – you won’t want the extra ‘bloat’ this plugin can bring. Given that Jetpack is a basic contact form plugin at best, you might do better to choose a smaller and more suitable plugin for your needs.</p>
<hr/>

<h2>Why Tripetto Is the Top WordPress Customizable Contact Form Plugin</h2>
<p>We can wax lyrical about Tripetto, but <a href="https://tripetto.com/reviews/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-customizable-contact-form">our users</a> can be just as passionate! For example, user ‘<i>dreadsupp</i>’ gives the ease of use a five-star rating:</p>
<p><i><strong>“…full of features, intuitive, fast and good looking…”</strong> - dreadsupp</i></p>
<p>The smart functionality and advanced conditional logic helped user ‘<i>remrick</i>’ across the board:</p>
<p><i><strong>“…Tripetto is amazing at every level: styling, built-in conversational forms or regular forms, conditional logic, dynamic variables…”</strong> - remrick</i></p>
<p>Tripetto’s form faces get top marks from user ‘<i>kgjermani</i>’ too:</p>
<p><i><strong>“…the option to convert the flow into a chat instead of a scrollable form is genius, and they’ve knocked the implementation out of the park…”</strong> - kgjermani</i></p>
<p>However, you can see for yourself what the fuss is about. In the next section, we’ll take a quick tour around what Tripetto has to offer.</p>

<h3>Using Tripetto</h3>
<p>We believe that Tripetto is number one when it comes to form builder plugins for WordPress. One reason for this is its interface. The plugin uses a drag-and-drop storyboard interface that lets you build the structure for your form and view it at a glance:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-storyboard.png" width="1000" height="606" alt="The Tripetto storyboard overview." loading="lazy" />
  <figcaption>The Tripetto storyboard overview.</figcaption>
</figure>
<p>You’re able to add and <a href="https://tripetto.com/help/articles/discover-the-power-of-branches-for-your-logic/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-customizable-contact-form">view different branches</a> with ease and organize your form’s sections into relevant and logical blocks. From there, you can add in different elements, such as images, calculators, basic text fields, and many more:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-elements.png" width="1000" height="541" alt="A list of elements you’re able to use within Tripetto, along with a partial display of the storyboard." loading="lazy" />
  <figcaption>A list of elements you’re able to use within Tripetto, along with a partial display of the storyboard.</figcaption>
</figure>
<p>The left-hand menu contains the options available to customize the block, and this will change based on the element you work on. For example, you could use a multiple choice selection using images, complete with options for sizing (and more):</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-picture-choice.png" width="1000" height="515" alt="A customization menu, showing the design of a Picture choice block and settings." loading="lazy" />
  <figcaption>A customization menu, showing the design of a Picture choice block and settings.</figcaption>
</figure>
<p>If you want to give your users a different experience that better suits the design of your form, you can do this <a href="https://tripetto.com/help/articles/discover-additional-options-for-autoscroll-form-face/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-customizable-contact-form">using ‘form faces’</a>. These let you <a href="https://tripetto.com/help/articles/discover-additional-options-for-classic-form-face/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-customizable-contact-form">‘re-skin’ your form</a> to provide a whole new interface on the front end, while you keep the functionality in place on the back end:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-form-faces.png" width="1000" height="583" alt="A form preview showing a drop-down listing form faces for Tripetto." loading="lazy" />
  <figcaption>A form preview showing a drop-down listing form faces for Tripetto.</figcaption>
</figure>
<p>From there, you can look to <a href="https://tripetto.com/wordpress/help/sharing-forms-and-surveys/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-customizable-contact-form">share your contact form</a>. Tripetto offers lots of flexibility. You’re able to use an Elementor widget, a direct link to the form, or a shortcode. All embed options provide a multitude of options to help customize how your form displays to the user:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-shortcode.png" width="1000" height="788" alt="The options for placing shortcodes within Tripetto." loading="lazy" />
  <figcaption>The options for placing shortcodes within Tripetto.</figcaption>
</figure>
<p>You also get to see the form submissions and results within WordPress too. Tripetto keeps all of your data within the platform, which means you don’t have to deal with third-party dashboards and associated privacy policies and data protection.</p>
<hr/>

<h2>Conclusion</h2>
<p>A <a href="https://tripetto.com/blog/contact-form-best-practices/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-customizable-contact-form">contact form</a> is a practical necessity for almost every WordPress site. It’s nearly the only way to get in touch with you in a direct way, on your terms. Because of this, you’ll want to make sure you choose the most relevant and ideal contact form plugin for your WordPress website.</p>
<p>This post has looked at a number of WordPress customizable contact form plugins, but <a href="https://tripetto.com/wordpress/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-customizable-contact-form">Tripetto</a> stands out among the competition. It’s a powerful and versatile WordPress form builder that will help you create conversational surveys and forms that also look beautiful. The drag-and-drop interface is perfect for developers, designers, marketers, and every user who wants to build custom forms without the need for code.</p>
<p><a href="https://tripetto.com/wordpress/pricing/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-customizable-contact-form">Tripetto starts</a> from $99 per year for a single-site license. What’s more, there’s a 14-day money-back guarantee on every purchase.</p>
<div>
  <a href="https://tripetto.com/wordpress/pricing/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-customizable-contact-form" class="blocklink">
    <div>
      <span class="title">Get Tripetto Pro for WordPress</span>
      <span class="description">Tripetto Pro is for the Tripetto WordPress plugin and comes in 3 all-inclusive variants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" />
    </div>
  </a>
</div>
