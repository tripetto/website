---
layout: blog-article
base: ../../
permalink: /blog/making-of-sdk-demo/
title: Making of SDK Demo - Tripetto Blog
description: Get a behind the scenes tour of Tripetto's new FormBuilder SDK demo, named 'How It Works'.
article_title: The Making of the 'How It Works' SDK Demo
article_slug: Making of 'How It Works' SDK Demo
author: jurgen
time: 5
category: coding-tutorial
category_name: Coding tutorial
tags: [coding-tutorial]
areas: [sdk]
year: 2024
---
<p>Our new FormBuilder SDK demo, named 'How It Works', is the perfect demonstration of how easy it is to implement Tripetto's form solution. Let us give you a behind the scenes tour!</p>

<p>Today we are releasing a brand new <a href="https://tripetto.com/sdk/how-it-works/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=making-of-sdk-demo" target="_blank"><strong>FormBuilder SDK demo</strong></a> on our website. It lets you play around with a live form builder, shows you how JSON plays a role in the whole process and how that translates into a working form with response data.</p>
<p>In this blog article we'd like to explain why we added an SDK demo. Besides that, it's a perfect opportunity for us to demonstrate how easy it was to implement the form builder and form runners for this demo with just a few lines of code.</p>
<p>You can have a look at the <a href="https://tripetto.com/sdk/how-it-works/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=making-of-sdk-demo" target="_blank"><strong>How It Works</strong></a> demo via the link below. After playing around with it, don't forget to return to this blog article for some more insights 😉</p>
<div>
  <a href="https://tripetto.com/sdk/how-it-works/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=making-of-sdk-demo" target="_blank" class="blocklink">
    <div>
      <span class="title">Demo for Tripetto FormBuilder SDK - How It Works</span>
      <span class="description">Live demo of how the Tripetto FormBuilder SDK works, including form builder, form runners and data management.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/tripetto-logo.svg" width="160" height="160" alt="Tripetto logo" />
    </div>
  </a>
</div>
<hr/>

<h2>Why this SDK Demo?</h2>
<p>Our <a href="https://tripetto.com/sdk/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=making-of-sdk-demo" target="_blank">FormBuilder SDK</a> proposition already has a dedicated part on our website. It comes with <a href="https://tripetto.com/sdk/docs/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=making-of-sdk-demo" target="_blank">extensive documentation</a> on every aspect of how to integrate our full form solution in any application.</p>
<p>While that documentation includes code demos, we lacked a simple overview of how the SDK components work on both the front end and back end. Our goal with this new SDK demo therefore was twofold:</p>
<ol>
  <li>Let potential implementers play around with the fully functioning components, like the form builder component and form runner components. That way they can determine if the FormBuilder SDK is the right fit for their project in terms of functionality;</li>
  <li>Let potential developers see how the components can be integrated with just a few lines of code and how JSON plays a vital role in the data management. That way they can determine if the FormBuilder SDK is the right fit for their project in terms of technology.</li>
</ol>
<p>The SDK demo combines those goals by showing the fully functioning components (determine functionality fit), while the implementation code snippets and JSON output of the components are displayed in real time (determine technology fit).</p>
<hr/>

<h2>How we built the SDK Demo</h2>
<p>Now, let's focus on how we built this SDK demo, because it's a perfect example of how easily you can use the form components. To keep things simple we used a combination of <a href="https://tripetto.com/sdk/plain-js/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=making-of-sdk-demo-demo" target="_blank">plain JavaScript</a> and <a href="https://tripetto.com/sdk/html/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=making-of-sdk-demo-demo" target="_blank">HTML</a> for this implementation, but the SDK also includes dedicated <a href="https://tripetto.com/sdk/react/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=making-of-sdk-demo-demo" target="_blank">React</a> and <a href="https://tripetto.com/sdk/angular/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=making-of-sdk-demo-demo" target="_blank">Angular</a> components.</p>

<h3>1 - Include JavaScript Bundles</h3>
<p>First, we include the <a href="https://www.npmjs.com/search?q=%40tripetto" target="_blank" rel="noopener noreferrer">JavaScript bundles</a> for the Form Builder component and Form Runner components.</p>

<h3>2 - Integrate Form Builder</h3>
<p>Next, we initiate the form builder component. We do that with the <code>Tripetto.Builder.open()</code> function. With this function the entire Tripetto form builder with all question blocks and functions becomes available!</p>
<div>
  <a href="https://tripetto.com/sdk/docs/builder/integrate/quickstart/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=making-of-sdk-demo" target="_blank" class="blocklink">
    <div>
      <span class="title">Quickstarts - Builder - Tripetto FormBuilder SDK</span>
      <span class="description">This chapter shows you different approaches to implement the builder.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/tripetto-logo.svg" width="160" height="160" alt="Tripetto logo" />
    </div>
  </a>
</div>

<h4>2.1 - Preload a form</h4>
<p>To make the demo instantly usable, we load a predefined form structure, the so-called form definition. We do that in the first parameter in the <code>Tripetto.Builder.open()</code> function.</p>
<p>For this case we have built this form definition in our <a href="https://tripetto.com/studio/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=making-of-sdk-demo-demo">Tripetto studio</a>, which is the SaaS version of Tripetto. A possible setup for SDK projects is to use the Tripetto studio as builder (instead of implementing the builder yourself) and extract the form definition from there on; the so-called <a href="https://tripetto.com/sdk/solutions/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=making-of-sdk-demo-demo" target="_blank">Hybrid setup</a>.</p>

<h4>2.2 - Implement events</h4>
<p>Next, we use some events that are available in the builder component:</p>
<ul>
  <li>The <code>onReady</code> event waits for the builder to be fully loaded, including the form definition. When the event fires we hide the loader and display the builder. The form builder is now ready to use!</li>
  <li>The <code>onChange</code> event fires with every change that's done in the form builder. For this demo we want to display the form definition to make it clear how that's structured. It's a JSON structure that holds all question blocks, content and settings of your form. With every <code>onChange</code> event we update the display of the definition JSON and make it easy to read with <a href="https://prettier.io/" target="_blank" rel="noopener noreferrer">Prettier code formatter</a>.</li>
</ul>
<div>
  <a href="https://tripetto.com/sdk/docs/builder/integrate/guides/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=making-of-sdk-demo" target="_blank" class="blocklink">
    <div>
      <span class="title">Guides - Builder - Tripetto FormBuilder SDK</span>
      <span class="description">This chapter shows you guides to help you master the Tripetto builder.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/tripetto-logo.svg" width="160" height="160" alt="Tripetto logo" />
    </div>
  </a>
</div>

<h3>3 - Integrate Form Runners</h3>
<p>Now that we have the form builder implemented and show the form definition that comes out of it, it's time to implement a form runner. A form runner takes care of displaying the form, including UI/UX, form logic and data output.</p>
<p>Tripetto's FormBuilder SDK comes with 3 stock runners, each with its own look and feel:</p>
<ul>
  <li><strong>Autoscroll Runner</strong> - Fluently presents one question at a time, comparable to Typeform.</li>
  <li><strong>Chat Runner</strong> - Presents all questions and answers as a chat.</li>
  <li><strong>Classic Runner</strong> - Presents question fields in a traditional format.</li>
</ul>
<p>For this demo we implement all 3 form runners to demonstrate that the form definition JSON is separate from the form layout (UI/UX). To initiate those form runners we use the <code>TripettoAutoscroll.run()</code> function, <code>TripettoChat.run()</code> function and <code>TripettoClassic.run()</code> function.</p>
<div>
  <a href="https://tripetto.com/sdk/docs/runner/stock/quickstart/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=making-of-sdk-demo" target="_blank" class="blocklink">
    <div>
      <span class="title">Quickstarts - Stock runners - Tripetto FormBuilder SDK</span>
      <span class="description">This chapter shows you different approaches to implement a stock runner.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/tripetto-logo.svg" width="160" height="160" alt="Tripetto logo" />
    </div>
  </a>
</div>

<h4>3.1 - Load the form definition</h4>
<p>With the <code>builder</code> parameter in each <code>run()</code> function we connect the builder and runner. By doing that, each change in the form structure in the form builder is passed to the runner instantly.</p>
<p>And with the <code>definition</code> parameter in each <code>run()</code> function we feed the form definition from the builder to the runner. The runner reads the definition JSON and will display the form according to the runner's layout (UI/UX).</p>

<h4>3.2 - Implement events</h4>
<p>For this demo we also use some events that are available in the runner component:</p>
<ul>
  <li>The <code>onReady</code> event waits for the runner to be fully loaded, including the form definition. When the event fires we hide the loader and display the runner. The actual form is now ready to use!</li>
  <li>The <code>onChange</code> event fires with every change in the form's dataset. The dataset holds the response data of the respondent. This also is a JSON structured format, in this case with Name-Value-Pairs (NVPs)<sup>1</sup>. Because for this demo we want to display that, with every <code>onChange</code> event we update the display of the response JSON and make it easy to read with <a href="https://prettier.io/" target="_blank" rel="noopener noreferrer">Prettier code formatter</a>.
  </li>
</ul>
<p>Ad 1: The runner component comes with several <a href="https://tripetto.com/sdk/docs/runner/api/library/modules/Export/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=making-of-sdk-demo-demo" target="_blank">export functions</a> to use the response data as you please, for example as NVPs or as a CSV format.</p>
<div>
  <a href="https://tripetto.com/sdk/docs/runner/stock/guides/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=making-of-sdk-demo" target="_blank" class="blocklink">
    <div>
      <span class="title">Guides - Stock runners - Tripetto FormBuilder SDK</span>
      <span class="description">This chapter shows you guides to help you master the Tripetto stock runners.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/tripetto-logo.svg" width="160" height="160" alt="Tripetto logo" />
    </div>
  </a>
</div>
<hr/>

<h2>Conclusion</h2>
<p>The FormBuilder SDK demo unleashes all the SDK components’ functionalities with just a few lines of JavaScript code. Compare that with <a href="https://tripetto.com/blog/time-saving-software-development-kits/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=making-of-sdk-demo">how much code and development time</a> it would take to build all of those advanced form features yourself! That's an important reason why the FormBuilder SDK is so interesting if you're looking for a full form solution in your application. No wonder that Fortune 500 companies already use Tripetto's FormBuilder SDK!</p>
<p>The FormBuilder SDK is largely open-source and comes with <a href="https://tripetto.com/sdk/docs/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=making-of-sdk-demo" target="_blank">extensive technical documentation</a>. Investigating if the FormBuilder SDK is the right for you is totally free. Once you’re ready to deploy it to a production environment an <a href="https://tripetto.com/sdk/pricing/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=making-of-sdk-demo-demo" target="_blank">SDK license</a> is required in most (not all) cases. You can determine license requirements and pricing via our <a href="https://tripetto.com/sdk/get-quote/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=making-of-sdk-demo-demo" target="_blank">license request wizard</a>. If you have any questions, please reach out to us via a <a href="https://tripetto.com/sdk/chat-live/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=making-of-sdk-demo-demo" target="_blank">live chat</a> or <a href="https://tripetto.com/sdk/schedule-call/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=making-of-sdk-demo-demo" target="_blank">schedule a call with us</a>. We’re happy to answer any of your questions and go over your project right away to determine the license.</p>
