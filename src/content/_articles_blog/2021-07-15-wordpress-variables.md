---
layout: blog-article
base: ../../
permalink: /blog/new-feature-instantly-use-wordpress-variables-in-your-forms/
title: New feature! WordPress variables in your forms - Tripetto Blog
description: You can now instantly use WordPress variables right inside your Tripetto forms.
article_title: New feature! Instantly use WordPress variables in your forms
article_slug: "Update: WordPress variables"
author: jurgen
time: 3
category: product
category_name: Product
tags: [product, product-update, feature, editor, blocks]
areas: [wordpress]
year: 2021
---
<p>With the latest update of the Tripetto WordPress plugin (version 4.1) you can now instantly use WordPress variables right inside your Tripetto forms. Very handy to collect background information or prefill values to help your respondents.</p>

<blockquote>
  <h4>👁‍🗨 TLDR</h4>
  <p>You can now use WordPress variables right inside your Tripetto forms. Update your WordPress plugin to the latest version (version 4.1) and <a href="{{ page.base }}help/articles/how-to-use-wordpress-variables-in-your-form/">see our help article for all information to use this new feature!</a></p>
</blockquote>
<h2>Based on your feedback</h2>
<p>We always listen to all feedback we get from our users to determine our development roadmap. The integration of WordPress variables is a good example of that and we're happy we now can offer that to our WordPressers.</p>
<figure>
  <img src="{{ page.base }}images/help/wordpress-variables/00-demo.png" width="1200" height="760" alt="Screenshot of variables in a form in Tripetto" loading="lazy" />
  <figcaption>Demonstration of WordPress variables in a Tripetto form.</figcaption>
</figure>
<p>There are quite some use cases you could think of in which you would want to use a certain WordPress variable. Until now that was not easily possible, but with the latest update of the WordPress plugin this becomes very easy! Let's have a look at two often heard use cases.
<h3>Using WordPress user information</h3>
<p>The most requested use case is to use the personal information from the loggedin user profile. These variables, like the name and email address of the user, enable you to help your respondents with filling out your forms.</p>
<p>You can for example simply store the personal information in the background of your form, without even asking your respondents for that information.</p>
<p>Or prefill some questions in your form so your respondents only have to check their data and can complete your form quickly.</p>
<h3>Gathering IP addresses</h3>
<p>Another often heard request is being able to see the IP addresses of the respondents, so you can for example filter out duplicate entries from the same IP address. Gathering IP addresses is now also possible with this latest release.</p>

<h2>WordPress variables in hidden field block</h2>
<p>These are just some examples. The update actually comes with a pretty long list of possible variables to use, so let's have a quick look at that.</p>
<p>WordPress variables are accessible via the <a href="{{ page.base }}help/articles/how-to-use-the-hidden-field-block/">hidden field block</a> in Tripetto. From there on you can simply select the variable you want to collect and the hidden field will do all the work for you.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-wordpress-variables-in-your-form/">How to use WordPress variables in your form</a></li>
</ul>

<h3>All WordPress variables</h3>
<p>This is the full list of available WordPress variables in the hidden field block:</p>
<ul>
  <li>User / Username;</li>
  <li>User / Nickname;</li>
  <li>User / Display name;</li>
  <li>User / First name;</li>
  <li>User / Last name;</li>
  <li>User / Email address;</li>
  <li>User / Website;</li>
  <li>User / Biographical info;</li>
  <li>User / Avatar (URL);</li>
  <li>User / Language;</li>
  <li>User / ID;</li>
  <li>Website / URL;</li>
  <li>Website / Title;</li>
  <li>Website / Tagline;</li>
  <li>Website / Administration email address;</li>
  <li>Website / Language;</li>
  <li>Visitor / IP address;</li>
  <li>Visitor / Language;</li>
  <li>Visitor / Referrer URL;</li>
  <li>Server / URL;</li>
  <li>Server / Protocol;</li>
  <li>Server / Port;</li>
  <li>Server / Host name;</li>
  <li>Server / Path;</li>
  <li>Server / Querystring;</li>
  <li>Server / IP address;</li>
  <li>Server / Software;</li>
  <li>Server / WordPress version;</li>
  <li>Server / PHP version.</li>
</ul>
<blockquote>
  <h4>🚧 Warning: Data privacy legislation</h4>
  <p>While using WordPress variables make sure you always comply to any data privacy legislation that applies to your data collection.</p>
</blockquote>

<h2>Get started with WordPress variables</h2>
<p>Update your Tripetto WordPress plugin to version 4.1. The hidden field is part of the <a href="{{ page.base }}wordpress/pricing/">WordPress Pro plan</a> and gives you immediate access to all WordPress variables.</p>
