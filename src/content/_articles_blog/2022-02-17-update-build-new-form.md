---
layout: blog-article
base: ../../
permalink: /blog/update-time-introducing-some-easy-and-powerful-new-features/
title: Update time! Introducing various new features - Tripetto Blog
description: Introducing an easier way to start building a new form, a handy improvement to multiselection questions, and Google Tag Manager!
article_title: Update time! Introducing some easy and powerful new features 💪
article_slug: "Update: various new features"
author: mark
time: 4
category: product
category_name: Product
tags: [product, product-update, feature]
areas: [wordpress]
year: 2022
---
<p>Let's make working with Tripetto a little bit easier and a little bit more powerful with today's update. Introducing an easier way to start building a new form, a handy improvement to multiselection questions, and an often requested integration... Google Tag Manager!</p>

<p>The latest version of Tripetto includes lots of new features, improvements and bugfixes. You can find the full changelog below, but we'd like to highlight three updates in particular:</p>
<ol>
  <li>A 'Build New Form' screen, with form templates;</li>
  <li>A 'Data format' setting for multiselection questions;</li>
  <li>A 'Google Tag Manager' integration.</li>
</ol>
<p>Let's dive in!</p>
<hr/>

<h2>1. Kickstart your form building process 🚀</h2>
<p>Let's begin with making it a bit easier to start a new form in the Tripetto WordPress plugin. Until today you always started building a new form from scratch with an empty storyboard in front of you. Handy if you already have in mind what you're going to build exactly. <strong>But wouldn't it be nice to give your form building a kickstart?!</strong></p>

<h3>Introducing the 'Build New Form' screen</h3>
<p>With today's update we introduce the <strong>Build New Form</strong> screen in the Tripetto WordPress plugin. And it looks like this:</p>
<figure>
  <img src="{{ page.base }}images/help/wordpress-manage-forms/build-new-form.png" width="1775" height="1114" alt="The Build New Form screen in the Tripetto WordPress plugin." loading="lazy" />
  <figcaption>The 'Build New Form' screen in the Tripetto WordPress plugin.</figcaption>
</figure>
<p>Before you actually begin to build your form, you can now choose how you want to start with that:</p>
<ul>
  <li><strong>Start from scratch</strong> - Simply choose your desired <a href="{{ page.base }}form-layouts/">form face</a> and start building right away on an empty storyboard. Total freedom! You can always <a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/">switch form faces</a> instantly while working on your form.</li>
  <li><strong>Build from a template</strong> - Templates are totally new and help you kickstart your form building. Choose any of the offered templates to build your form even faster. To get a good vision on each template, you can open a live preview before you use it as a template for your new form. Templates include a basic form structure and styling. Templates are fully editable and customizable of course.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/wordpress-manage-forms/template.png" width="1179" height="757" alt="A live preview of a template in the Build New Form screen in the Tripetto WordPress plugin." loading="lazy" />
  <figcaption>A live preview of a template in the 'Build New Form' screen in the Tripetto WordPress plugin.</figcaption>
</figure>

<p>That's it for the 'Build New Form' screen. We hope you like it. Next: data formatting for multiselection questions!</p>
<hr/>

<h2>2. Easier multiselection data 💾</h2>
<p>Until today the collected data from questions in which your respondents can select multiple answers, was stored in separate data columns per single possible answer. That was not always ideal to use for your analysis. Sometimes you just want a text with only the selected items of each respondent.</p>

<h3>Introducing the 'Data format' setting</h3>
<p>From now on you can simply decide yourself how you want to store multiselection data:</p>
<ul>
  <li>Every option as a separate field;</li>
  <li>Or one text field with a list of selected options;</li>
  <li>Or both.</li>
</ul>
<p>This applies to <a href="{{ page.base }}help/articles/how-to-use-the-checkboxes-block/">checkboxes</a>, <a href="{{ page.base }}help/articles/how-to-use-the-multiple-choice-block/">multiple choice</a> and <a href="{{ page.base }}help/articles/how-to-use-the-picture-choice-block/">picture choice</a> blocks. The new feature is available in each of those blocks in the form builder. It simply gives you a little bit more control over your data. Next: Google Tag Manager!</p>
<hr/>

<h2>3. Boost your form activity tracking ⚡</h2>
<p>Since the <a href="{{ page.base }}blog/new-feature-form-activity-tracking-with-google-analytics-and-facebook-pixel/">introduction of form activity tracking</a> with Google Analytics and Facebook Pixel, an often requested extra integration was <strong>Google Tag Manager (GTM)</strong>. And of course we understand that: Tag Manager enables you to manage your tracking integrations in a central place and lets you connect your tracking data to all kinds of connected services. Therefore it's a very powerful tool to track form activity in Tripetto forms.</p>

<h3>Introducing the 'Google Tag Manager' integration</h3>
<p>In today's update we added Google Tag Manager to the Tracking screen in Tripetto. You can simply enable it, supply your GTM-tag and track the desired events in your Tripetto form. From there on you can receive the events in Google Tag Manager and distribute it to your connected services.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-track-form-activity-with-google-tag-manager/">How to track form activity with Google Tag Manager</a></li>
</ul>
<hr/>

<h2>Full changelog</h2>
<p>Here's an overview of the whole changelog of today's update:</p>
<ul>
  <li>✔️ Added a build new form page that includes a gallery of form templates to choose from</li>
  <li>✔️ Added an option to store a concatenated text with all the selected options in the dataset (available for the checkboxes, multiple-choice, and picture-choice blocks)</li>
  <li>✔️ Added Google Tag Manager support for form tracking</li>
  <li>✔️ Added an option to configure trackers to use global tracker codes when embedding forms</li>
  <li>✔️ Added the possibility to retrieve a result overview page by result reference (using the <code>reference</code> query string parameter)</li>
  <li>✔️ Added a WordPress filter hook <code>tripetto_prefill</code> that allows programmatically prefilling data in forms</li>
  <li>⚡ Improved the exportability feature to make it clearer what this feature does</li>
  <li>🐛 Fixed submission issues when using Safari with response data that contains combining diacritical marks</li>
  <li>🐛 Fixed a bug where icons in buttons would turn the wrong color when hovering over the button</li>
  <li>🐛 Fixed a bug in the required feature of the matrix block</li>
</ul>
<hr/>
<h2>Get the update</h2>
<p>That's it for today's update. If you use the Tripetto WordPress plugin, please update your installation to version 5.3. If you're using our studio at tripetto.app, we have taken care of the update for you and you can use all updates right away!</p>
