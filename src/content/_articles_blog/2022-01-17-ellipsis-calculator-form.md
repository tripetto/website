---
layout: blog-article
base: ../../
permalink: /blog/calculator-form/
title: Calculator Form on Your WordPress Website - Tripetto Blog
description: Calculations can help you build all sorts of form types. This post will show you how to set up a calculator form on your site using Tripetto!
article_title: How to Set Up a Calculator Form on Your WordPress Website - A Complete Guide With Examples
article_slug: Calculator form in WordPress
article_folder: 20220117
article_image: cover.webp
article_image_width: 1640
article_image_height: 1081
article_image_caption: How to Set Up a Calculator Form on Your WordPress Website - A Complete Guide With Examples
author: jurgen
time: 10
category: guide
category_name: Guide
tags: [guide]
areas: [wordpress]
year: 2022
---
<p>Calculations can help you build all sorts of form types. This post will show you how to set up a calculator form on your site using Tripetto!</p>

<p>In the past, forms could be simple elements of a site. However, now they are versatile and powerful, with lots of extra functionality. This can give you a better way to connect with users and provide a greater experience. A <a href="https://tripetto.com/blog/calculated-fields-form/?utm_source=Tripetto&utm_medium=blog&utm_campaign=calculator-form">calculator</a> is a fantastic way to help you apply discount codes, calculate scores, and more. While WordPress doesn’t have native form functionality or templates, a solution such as Tripetto does.</p>
<p>With <a href="https://tripetto.com/calculator/?utm_source=tripetto&utm_medium=content-marketing&utm_content=calculator-form">Tripetto’s Calculator block</a>, you can include multiple calculations in a single WordPress form. You can also get as advanced as you need to, as certain question blocks can also carry out complex and instant calculations for you. Even better, you can achieve all of this without the need for HTML, CSS, JavaScript, or any other code.</p>
<p>For this article, we’re going to show you how to use the Calculator block within Tripetto, and what sets it apart from the competition.</p>
<hr/>

<h2>Why You’d Want to Use Calculator Forms (And When To Do So)</h2>
<p>While lots of businesses won’t need anything more than a simple WordPress form, in some cases, you’ll need to implement greater functionality. A more advanced form builder should include dedicated calculator functionality. With this you can create a number of powerful form types:</p>
<ul>
  <li>Exam, quiz, and assessment forms, that calculates scores in real-time, and provides the results to other elements of your forms.</li>
  <li>Quote and order forms, which lets the calculator combine multiple form elements. For example, you can tally a running total, then apply a discount percentage based on user input.</li>
  <li>Health wizards – and not just for Body Mass Indicator (BMI) calculations, but for macronutrients, and other popular formulas.</li>
</ul>

<p>For example, you can use a simple formula to <a href="https://tripetto.com/examples/order-form/?utm_source=tripetto&utm_medium=content-marketing&utm_content=calculator-form">calculate discount codes</a> which will provide value and increase the effectiveness of your form:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/discount-calculator.png" width="1000" height="568" alt="A Tripetto form that calculates a discount." loading="lazy" />
  <figcaption>A Tripetto form that calculates a discount.</figcaption>
</figure>

<p>You can take things even further though, almost to any level you need. While for example a <a href="https://tripetto.com/examples/body-mass-index-bmi-wizard/?utm_source=tripetto&utm_medium=content-marketing&utm_content=calculator-form">BMI calculator</a> isn’t something every site needs, the underlying principles are something you can adapt to any number of situations:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/bmi-calculator.png" width="1000" height="403" alt="A Tripetto form that calculates a BMI and gives an advice based on the calculation outcome." loading="lazy" />
  <figcaption>A Tripetto form that calculates a BMI and gives an advice based on the calculation outcome.</figcaption>
</figure>

<p>However, there is much more you can do with dedicated calculator functionality for your forms:</p>
<ul>
  <li>The most obvious is to <a href="https://tripetto.com/help/articles/how-to-use-operations-in-the-calculator-block/?utm_source=tripetto&utm_medium=content-marketing&utm_content=calculator-form">use standard operators</a>, such as add, subtract, multiple, and divide.</li>
  <li>You’re able to set fixed numbers, but more importantly <a href="https://tripetto.com/help/articles/how-to-use-given-answers-from-respondents-in-your-calculations/?utm_source=tripetto&utm_medium=content-marketing&utm_content=calculator-form">use given answers</a> from the respondent to other questions in the form, right inside your calculations.</li>
  <li>To easily calculate with selected options in the form, you should be able to <a href="https://tripetto.com/help/articles/how-to-use-scores-in-your-calculations/?utm_source=tripetto&utm_medium=content-marketing&utm_content=calculator-form">add scores</a> to each of the options in your form.</li>
  <li>You should also be able to <a href="https://tripetto.com/help/articles/how-to-use-comparisons-in-your-calculations/?utm_source=tripetto&utm_medium=content-marketing&utm_content=calculator-form">compare values</a>, whether that’s something calculated in real-time, or fixed.</li>
  <li>There should be the ability to <a href="https://tripetto.com/help/articles/how-to-use-functions-and-constants-in-your-calculations/?utm_source=tripetto&utm_medium=content-marketing&utm_content=calculator-form">set constants</a>, such as a time or date, or apply mathematical constants wherever you need.</li>
  <li>In advanced use cases, the presence of <a href="https://tripetto.com/help/articles/how-to-use-functions-and-constants-in-your-calculations/?utm_source=tripetto&utm_medium=content-marketing&utm_content=calculator-form">more advanced mathematical functions</a> will also let you develop complex and unique calculations.</li>
  <li>Calculating a desired outcome is one thing, but <a href="https://tripetto.com/help/articles/how-to-use-the-outcomes-of-calculator-blocks/?utm_source=tripetto&utm_medium=content-marketing&utm_content=calculator-form">using the outcome</a> so you can add the right conclusions to it, is just as important.</li>
</ul>
<p>Overall, calculations are vital when you want to create dynamic content that boosts the User Experience (UX). To implement them, you’ll want to turn to a top-tier solution.</p>
<hr/>

<h2>Introducing Tripetto’s Calculator Block</h2>
<p>By default, WordPress doesn’t include any functionality to display a form. In most circumstances, you would turn to a number of other plugins to help you add a form to your site.</p>
<p>However, Tripetto offers a bunch of stellar features to add forms to your WordPress site, one of which is the <a href="https://tripetto.com/calculator/?utm_source=tripetto&utm_medium=content-marketing&utm_content=calculator-form">Calculator block</a>. This starts with a core base of typical and straightforward operations:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/operations.png" width="1000" height="426" alt="Tripetto calculations start with basic operations." loading="lazy" />
  <figcaption>Tripetto calculations start with basic operations.</figcaption>
</figure>
<p>From there, you can begin to play and perform the calculations you need for each use case. For example, you can <strong>use given answers</strong> and/or <strong>apply scores</strong> to the given answers from other questions in your form and do all kinds of calculations with them.</p>
<p>To take this a step further, you can also <strong>use advanced operations</strong> such as roots, powers, constants, functions, and more. This will be enough for most cases and already lets you create very powerful calculations within your forms.</p>
<p>However, you can also carry out <strong>subcalculations</strong>: This means calculations within calculations. Examples of this are checkout subtotals on order forms, tax calculations, and more. Further to this, you can also use <strong>multiple calculators</strong> in one form. A typical use case here is where you need to make a few different calculations in one form.</p>
<p>Because calculations in Tripetto are so flexibel, you can even <strong>use the outcome of one calculator as the input for another calculator</strong>. To turn back to our BMI calculation form, you could expand the scope to also cover body fat, macronutrient intake, and more.</p>
<p>Last great thing is that calculators in Tripetto can be used in any part of your form. This enables you to <strong>show the outcome</strong> to your respondents at any given position in your form, but you can even use the outcome to <strong>work with your logic flows</strong>. That way you can for example differentiate the route in your form that each respondent takes, based on the outcome of a calculator.</p>
<hr/>

<h2>How to Set Up and Use a Calculator Form With Tripetto</h2>
<p>Over the rest of this article, we’re going to show you <a href="https://tripetto.com/help/articles/how-to-use-the-calculator-block/?utm_source=tripetto&utm_medium=content-marketing&utm_content=calculator-form">how to add and use a calculator form</a>. If you can use WordPress, you can add this powerful Tripetto feature. We’re going to assume that you already have a Pro version of Tripetto, and that it’s installed and activated.</p>
<blockquote>
  <h4>📌 Also see: Instant calculations</h4>
  <p>Note that you don’t even need to work with the full Calculator block in some cases. We include some of the more common calculations within certain question blocks for instant calculations. For example, you can complete a running tally of scores within multiple choice blocks instantly. However, once you add a new Calculator block to your form, you have full power inside that Calculator block.</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-add-instant-scores-and-calculations-inside-question-blocks/?utm_source=tripetto&utm_medium=content-marketing&utm_content=calculator-form">How to add instant scores and calculations inside question blocks</a></li>
  </ul>
</blockquote>

<h3>Step 1. Add a Calculator Block to Your Form</h3>
<p>The calculator is available as a question type in Tripetto's form builder. After you added a new block to your form, you can select the "Calculator" from the Question Type menu.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/question-type.png" width="1000" height="342" alt="The calculator is available as a question type." loading="lazy" />
  <figcaption>The calculator is available as a question type.</figcaption>
</figure>
<p>From here, you can give your Calculator block a description, so you can understand it at a glance later on.</p>

<h3>Step 2. Work With the Calculator’s Operations and Features</h3>
<p>Given the scope of the included operations within Tripetto, it’s understandable that there will be lots to uncover. In simple terms, there are three elements to any calculation:</p>
<ul>
  <li><strong>Initial value.</strong> This is the starting point of your calculation. That can be a fixed number, but also a score or a given answer right away.</li>
  <li><strong>Operation(s).</strong> Next, you supply the operation(s) you want to perform after the initial value, such as add, subtract, multiply or divide. Each operation is a step in your calculation.</li>
  <li><strong>Operation properties.</strong> For each operation there are lots of options to perform the right calculation. You have all kinds of options that help you to use the right values with that, like a fixed value, a given answer, a score or a comparison.</li>
</ul>
<p>With these elements you build up your calculation. For example, consider a three-question quiz, in which you want to calculate the score for each respondent, based on the given answers:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/calculator-quiz-score.png" width="717" height="553" alt="Calculate the score of a quiz based on the given answers." loading="lazy" />
  <figcaption>Calculate the score of a quiz based on the given answers.</figcaption>
</figure>

<p>You can click each row in the calculator to open the operation's properties. For example to determine the scores of each option in a multiple choice question:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/calculator-operation.png" width="1117" height="594" alt="Open an operation in the calculator to configure it." loading="lazy" />
  <figcaption>Open an operation in the calculator to configure it.</figcaption>
</figure>

<h3>Step 3. Run Through the Calculator Block’s Settings and Options</h3>
<p>There are lots of options and settings for each Calculator block to help you format the outcome. You’ll find these at the left-hand side of each Calculator block in the form builder.</p>
<p>To demonstrate this, let's look at an order form, in which you want to calculate the total price of the entered amount of products. You want to show a dollar sign and the price must be calculated with 2 decimals. And you want a minimum purchase of $9.99 and a maximum purchase of $100.00. To achieve this, simply enable the needed options and configure them like you want:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/calculator-price.png" width="715" height="1084" alt="Configure the options of Tripetto's Calculator block to get the total price." loading="lazy" />
  <figcaption>Configure the options of Tripetto's Calculator block to get the total price.</figcaption>
</figure>

<h3>Step 4. Using Logic With Your Calculator Block’s Outcomes</h3>
<p>For a form that’s dynamic and adaptive, conditional logic is vital. Tripetto includes powerful logic options by default. What’s more, you can leverage this logic to work with your <a href="https://tripetto.com/help/articles/how-to-use-the-outcomes-of-calculator-blocks/?utm_source=tripetto&utm_medium=content-marketing&utm_content=calculator-form">calculator outcomes</a>. You can use this in a couple of ways:</p>
<ul>
  <li><strong>As a condition for a branch.</strong> This is akin to an ‘if statement’, which you can use to run a certain follow-up if the calculator returns a certain value.</li>
  <li><strong>Within logic branches.</strong> If the conditions match, you’ll be able to perform calculations. For example, this could be as simple as detecting if a user populates two text fields. From here, you can look to iterate through and find multiple items and calculate subtotals (and more). You’ll then get a grand total at the end of the branch from the calculator.</li>
</ul>
<p>There are lots of options to set branch conditions that extend the operations you carry out on your form calculators. For example, you can set conditions that check whether the calculation is equal or not equal to, higher or lower, between or in between, and much more.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/branch-logic.png" width="1021" height="646" alt="Use the outcome of a Calculator block to follow a logic flow." loading="lazy" />
  <figcaption>Use the outcome of a Calculator block to follow a logic flow (show the bonus question if the score is higher than 2).</figcaption>
</figure>

<p>We have a <a href="https://tripetto.com/help/articles/how-to-use-the-outcomes-of-calculator-blocks/?utm_source=tripetto&utm_medium=content-marketing&utm_content=calculator-form">dedicated page within our Help Center</a> on using logic within your calculations, and it’s essential reading to harness the power this provides.</p>

<hr/>
<h2>The Calculator Block Is Not All Tripetto Can Do</h2>
<p>One of the standout features of the <a href="https://tripetto.com/wordpress/?utm_source=tripetto&utm_medium=content-marketing&utm_content=calculator-form">Tripetto WordPress plugin</a> is the Calculator block. This can take a typical form and turn it into a supercharged, dynamic, and valuable experience on your website. However, it’s not the only string to Tripetto’s bow. There is lots more in the box:</p>
<ul>
  <li>Advanced logic and customization options for your forms.</li>
  <li>Intelligent automation to help you save time.</li>
  <li>Your data is stored within WordPress, which offers you complete control over the information you glean from your forms.</li>
  <li>A fantastic and friendly User Interface (UI) that will enhance the user’s experience on your site.</li>
</ul>
<p>The best news is that you need zero coding knowledge to implement all of these aspects. What’s more, there’s a <a href="https://tripetto.com/wordpress/pricing/?utm_source=tripetto&utm_medium=content-marketing&utm_content=calculator-form">pricing tier</a> to suit all budgets and needs. A single-site license starts from $99 per year, with other tiers giving you more licenses for a greater number of sites.</p>
<div>
  <a href="https://tripetto.com/wordpress/pricing/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculator-form" class="blocklink">
    <div>
      <span class="title">Get Tripetto Pro for WordPress</span>
      <span class="description">Tripetto Pro is for the Tripetto WordPress plugin and comes in 3 all-inclusive variants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" loading="lazy" />
    </div>
  </a>
</div>
