---
layout: blog-article
base: ../../
permalink: /blog/pop-up-contact-form-wordpress/
title: Add a Popup Contact Form in WordPress - Guide - Tripetto Blog
description: For a form that’s highly visible, a popup can’t be beaten. This post will show you how to create a popup contact form in WordPress!
article_title: How to Add a Popup Contact Form in WordPress - A Step-by-Step Guide
article_slug: Popup contact form in WordPress
article_folder: 20221031
article_image: cover.webp
article_image_width: 1640
article_image_height: 1081
article_image_caption: How to Add a Popup Contact Form in WordPress - A Step-by-Step Guide
author: jurgen
time: 12
category: guide
category_name: Guide
tags: [guide]
areas: [wordpress]
year: 2022
---
<p>For a form that’s highly visible, a popup can’t be beaten. This post will show you how to create a popup contact form in WordPress!</p>

<p>Your contact form can help with lead generation, customer support, and much more. However, you’ll need the user to know how to navigate to it. This could reduce your engagement levels and erode the User Experience (UX). However, if you add a popup contact form to WordPress, you can mitigate some of these drawbacks.</p>
<p>A popup form will display on the screen based on user action. This could be an attempt to navigate away from the page, browsing over a specific site element, and more. As such, you have the opportunity to bring your contact form to the user, rather than asking them to hunt it down to get in touch.</p>
<p>For this tutorial, we’re going to show you how to add a popup contact form in WordPress using some popular plugins. First, though, let’s discuss why you’d want to add these types of forms to your WordPress site.</p>
<hr/>

<h2>Why You Should Use a Popup Form in WordPress</h2>
<p>As far as business assets go, a <a href="https://tripetto.com/blog/contact-form-generator/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">contact form</a> is one of the best (if you design and deploy it well). For starters, it is a great way to <a href="https://tripetto.com/blog/lead-generation-form/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">generate leads</a>, given that you often ask for a contact email address.</p>
<p>In addition, you can use contact forms in a number of other ways. For instance, it could be a way to receive feedback on your products or service, or be the first line in your support provision.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/customer-support-form.png" width="1000" height="680" alt="A customer support form within Tripetto." loading="lazy" />
  <figcaption>A customer support form within Tripetto.</figcaption>
</figure>
<p>As such, the placement of your form is crucial. Your home page or <a href="https://tripetto.com/blog/landing-page-form/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">landing page</a> is a good idea, but you could extend this to every page on your site using popup functionality. This is where you’ll use specific user triggers such as the time on site or an attempt to close the page to display your contact form.</p>
<p>There are two big reasons why you’d want to use a popup contact form in WordPress:</p>
<ul>
  <li><strong>Better engagement.</strong> If you present the form to the user without the need for them to navigate your site, you can boost interaction. This should also have a knock-on effect with your conversion rate, <a href="https://tripetto.com/help/articles/how-to-measure-completion-rates-with-form-activity-tracking/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">completion rate</a>, and bounce rate.</li>
  <li><strong>Greater UX.</strong> Speaking of navigation, the user doesn’t have to hunt for your form or leave a specific page. As such, you offer the least ‘path of resistance’ possible.</li>
</ul>
<p>While popups do have drawbacks, we’d suggest that the positives outweigh them. For the next section, we’ll discuss some of the ways you’ll add a popup contact form in WordPress.</p>
<hr/>

<h2>The 2 Ways to Add a Popup Contact Form in WordPress</h2>
<p>As with many features and functionality within WordPress, you have two ways to add a popup form in WordPress:</p>
<ul>
  <li><strong>Manual coding.</strong> This will give you the flexibility and versatility you’ll need to create a unique solution. However, the downside is the advanced technical knowledge you have to have, not to mention the appropriate time and resources to maintain your form.</li>
  <li><strong>Dedicated plugins.</strong> A <a href="https://tripetto.com/blog/wordpress-customizable-contact-form/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">form builder plugin</a> such as <a href="https://tripetto.com/wordpress/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">Tripetto</a> will help you create a suitable WordPress contact form with the same amount of scope as a manual solution, without the need for technical knowledge. What’s more, you can find WordPress plugins to integrate popup functionality too.</li>
</ul>
<p>You’ll find lots of options for contact form plugins, but Tripetto is number one. Here’s why:</p>
<ul>
  <li>Tripetto can counteract the invasive nature of popups through its <a href="https://tripetto.com/wordpress/features/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">feature set</a>. You can create <a href="https://tripetto.com/help/articles/how-to-make-your-forms-smart-and-conversational/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">dynamic, conversational forms</a> that can feel like talking to a human. Also, conditional logic such as the ability to recall values can help you to personalize a contact form based on previous responses.</li>
  <li>You get to <a href="https://tripetto.com/wordpress/help/styling-and-customizing/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">customize almost any aspect</a> of your form (including colors, fonts, and backgrounds) using the drag-and-drop <a href="https://tripetto.com/help/articles/how-to-build-your-forms-in-the-form-builder/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">form builder</a>. You also get to customize input controls such as radio buttons, checkboxes, drop-down menus, and more. You’re able to judge your design using the built-in <a href="https://tripetto.com/help/articles/how-to-let-the-live-preview-work-for-you/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">live preview</a>, both with and without logic.</li>
  <li>Apart from the in-form customizations, you can also influence the overall structure and design using <a href="https://tripetto.com/help/articles/how-to-switch-between-form-faces/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">switchable ‘form faces’</a>. These let you adopt a classic design that only presents the fields, an autoscrolling option that focuses on one field at a time, or the chat form face. This option presents your form in a question-and-answer style, which is perfect for conversational forms.</li>
  <li>Tripetto is a fantastic plugin that works natively within WordPress. However, you can also connect to thousands of other third-party services using Tripetto’s webhooks and platforms such as <a href="https://tripetto.com/help/articles/how-to-connect-to-other-services-with-zapier/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">Zapier</a>, <a href="https://tripetto.com/help/articles/how-to-connect-to-other-services-with-make/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">Make</a>, and <a href="https://tripetto.com/help/articles/how-to-connect-to-other-services-with-pabbly-connect/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">Pabbly Connect</a>. You can access apps such as the email marketing platform <a href="https://tripetto.com/help/articles/how-to-automatically-add-mailchimp-subscribers-from-form-responses/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">Mailchimp</a> or <a href="https://tripetto.com/help/articles/how-to-automatically-store-form-responses-in-google-sheets/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">Google Sheets</a>. What’s more, you can <a href="https://tripetto.com/help/articles/how-to-automate-email-notifications-for-each-new-result/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">automate emails</a> and notifications within Tripetto, export your form data, and save time and effort.</li>
</ul>
<p>Tripetto is an all-in-one solution that gives you the full set of features and functionality regardless of which tier you purchase. A single-site license <a href="https://tripetto.com/wordpress/pricing/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">starts from $99</a>, and every purchase offers a 14-day money-back guarantee.</p>
<hr/>

<h2>How to Add a Popup Contact Form in WordPress in 2 Steps (Using Tripetto)</h2>
<p>While Tripetto can do a lot, it doesn’t have native popup functionality. However, you can add popups to your site in seconds using a number of different approaches – none of which need HTML, CSS, PHP, or JavaScript knowledge! Over the rest of this article, we’ll highlight two:</p>
<ul>
  <li>Use <a href="https://tripetto.com/help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-elementor-widget/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">Tripetto’s Elementor widget</a> within a popup to display your contact form.</li>
  <li>Combine Tripetto with a WordPress popup plugin such as <a href="https://wppopupmaker.com/" target="_blank" rel="noopener noreferrer">Popup Maker</a> to give you greater versatility and scope.</li>
</ul>
<p>Before this step, you’ll have to make sure you have a fantastic contact form to display. We’ll show you how Tripetto can help you with that next.</p>

<h3>1. Install the Plugin and Create Your Contact Form</h3>
<p>First off, you’ll need to install and activate Tripetto before you begin to build. Let’s run through how to do this next.</p>

<h4>Installing and Activating Tripetto</h4>
<p>If you have experience with installing plugins for WordPress, the process for Tripetto is no different. We have a <a href="https://tripetto.com/help/articles/how-to-install-the-wordpress-plugin/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">detailed guide</a> on how to do this.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/install-tripetto-plugin.png" width="1000" height="524" alt="Installing a plugin in WordPress." loading="lazy" />
  <figcaption>Installing a plugin in WordPress.</figcaption>
</figure>
<p>Once you finish with the installation, head to the Tripetto screen within your WordPress dashboard. You’ll come to the Onboarding Wizard:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-onboarding.png" width="1000" height="620" alt="The Tripetto Onboarding Wizard." loading="lazy" />
  <figcaption>The Tripetto Onboarding Wizard.</figcaption>
</figure>
<p>This is a step-by-step configuration to optimize your experience, and takes no more than a couple of minutes. When the wizard completes, you can then begin to create your contact form.</p>

<h4>Building the Contact Form</h4>
<p>To build the contact form, head to the <i>Tripetto > Build Form</i> screen. This will give you two options to choose from: use a form face, or start <a href="https://tripetto.com/examples/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">from a template</a>. We’d recommend a template, because this can build in lots of the functionality you’ll need for your popup form in WordPress. </p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-templates.png" width="1000" height="541" alt="A selection of templates for Tripetto." loading="lazy" />
  <figcaption>A selection of templates for Tripetto.</figcaption>
</figure>
<p>In addition, you can choose a different form face at any time from the drop-down menu on Tripetto’s live preview screen:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-form-faces.png" width="1000" height="608" alt="Choosing a form face from the drop-down menu in Tripetto." loading="lazy" />
  <figcaption>Choosing a form face from the drop-down menu in Tripetto.</figcaption>
</figure>
<p>At this point, you can begin to build in your form fields and questions within the storyboard.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-storyboard.png" width="1000" height="619" alt="An overview of the Tripetto storyboard." loading="lazy" />
  <figcaption>An overview of the Tripetto storyboard.</figcaption>
</figure>
<p>You’ll see from the template that all of your <a href="https://tripetto.com/question-types/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">typical bases</a> are covered. However, Tripetto includes a wealth of <a href="https://tripetto.com/help/articles/question-blocks-guide-which-question-type-to-use/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">question types</a> to cover almost every kind of form. To find them, click any Block within your storyboard, and select a new type from the drop-down menu:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-question-blocks.png" width="1000" height="621" alt="Changing the type of a question in Tripetto." loading="lazy" />
  <figcaption>Changing the type of a question in Tripetto.</figcaption>
</figure>
<p>Here, you have fields to cover lots of different use cases. For example, you can add different dedicated fields for <a href="https://tripetto.com/help/articles/how-to-use-the-email-address-block/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">email addresses</a>, <a href="https://tripetto.com/help/articles/how-to-use-the-phone-number-block/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">telephone numbers</a>, and <a href="https://tripetto.com/help/articles/how-to-use-the-password-block/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">passwords</a>. You also have a variety of formats, such as <a href="https://tripetto.com/help/articles/how-to-use-the-dropdown-block/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">drop-down menus</a>, <a href="https://tripetto.com/help/articles/how-to-use-the-checkboxes-block/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">checkboxes</a>, <a href="https://tripetto.com/help/articles/how-to-use-the-radio-buttons-block/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">radio buttons</a>, <a href="https://tripetto.com/help/articles/how-to-use-the-rating-block/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">ratings</a>, <a href="https://tripetto.com/blog/likert-scale-surveys/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">scales</a>, and <a href="https://tripetto.com/question-types/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">much more</a>. There’s even a way to <a href="https://tripetto.com/help/articles/how-to-use-images-and-videos-in-your-form/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">display images and video</a> as options within your questions:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-picture-choice.png" width="1000" height="469" alt="Selecting an image as an answer within a Tripetto question." loading="lazy" />
  <figcaption>Selecting an image as an answer within a Tripetto question.</figcaption>
</figure>
<p>It’s a good idea to have all your questions finalized before you look at the structure of your popup form in WordPress. For this, you can use <a href="https://tripetto.com/logic-types/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">conditional logic</a>.</p>

<h4>Conditional Logic</h4>
<p>Using conditional logic is a <a href="https://tripetto.com/blog/forms-logic/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">massive topic</a>, and we can only cover the bare essentials here. However, it’s a staple of any good contact form, so you’ll want to use Tripetto’s functionality to implement it.</p>
<p>There are <a href="https://tripetto.com/blog/wordpress-forms-with-conditional-logic/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">three types</a> available to you: Branch logic, jump logic, and piping logic. Branches are something you’ll use to move the respondent through your form based on previous answers. It can be as <a href="https://tripetto.com/help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">straightforward or as powerful</a> as you need:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-branch-logic.png" width="1000" height="489" alt="Adding branches in Tripetto." loading="lazy" />
  <figcaption>Adding branches in Tripetto.</figcaption>
</figure>
<p>Jump logic is similar to branch logic in that you’ll use it to send a respondent down a different path. However, here you’ll miss out entire question sections in order to get the user to the end of the form:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-jump-logic.png" width="1000" height="733" alt="Adding jump logic to a Tripetto form." loading="lazy" />
  <figcaption>Adding jump logic to a Tripetto form.</figcaption>
</figure>
<p><a href="https://tripetto.com/help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">Piping logic</a> can help you to personalize your forms. A good example is where you ask for the name of the user, and reference it elsewhere in the form. In some cases, you’ll use a <a href="https://tripetto.com/help/articles/how-to-use-the-hidden-field-block/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">Hidden Field Block</a> and <a href="https://tripetto.com/help/articles/how-to-use-wordpress-variables-in-your-form/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">a variable</a> to store these dynamic answers:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-piping-logic.png" width="1000" height="578" alt="Asking a user for their name in Tripetto, and showing the piping logic used to display it." loading="lazy" />
  <figcaption>Asking a user for their name in Tripetto, and showing the piping logic used to display it.</figcaption>
</figure>
<p>On the whole, conditional logic is the most powerful way you can customize your forms. As such, it makes sense to spend some time here to build out the rest of your contact form.</p>

<h4>Integrations</h4>
<p>While Tripetto includes a comprehensive set of ways to record form submissions and look at basic analytics, you may also want to integrate your favorite third-party service. Tripetto lets you do this through a combination of webhooks and a dedicated automation app, such as <a href="https://zapier.com" target="_blank" rel="noopener noreferrer">Zapier</a>.</p>
<p>This is a stellar way to automate certain tasks relating to your form. For example, you can save time and effort if you add leads to a mailing list or to a custom database using services such as <a href="https://tripetto.com/help/articles/how-to-automatically-add-mailchimp-subscribers-from-form-responses/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">Mailchimp</a> or <a href="https://tripetto.com/help/articles/how-to-automatically-store-form-responses-in-google-sheets/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">Google Sheets</a>.</p>
<p>Tripetto gives you a super-simple way to achieve this in its <i>Automate > Connections</i> tab:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-connections.png" width="1000" height="227" alt="The Automate > Connections link within Tripetto." loading="lazy" />
  <figcaption>The <i>Automate > Connections</i> link within Tripetto.</figcaption>
</figure>
<p>All you’ll need is the relevant webhook from your automation app, which you’ll paste into Tripetto’s URL field:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-connections-zapier.png" width="1000" height="536" alt="The Connections screen within Tripetto." loading="lazy" />
  <figcaption>The Connections screen within Tripetto.</figcaption>
</figure>
<p>Once you click the button to test the connection, this should link your form and your third-party service in seconds.</p>

<h3>2. Build a Popup Contact Form Using Other Popular WordPress Plugins</h3>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-form-popup.png" width="1000" height="805" alt="A popup contact form in WordPress on the front end using the Popup Maker plugin." loading="lazy" />
  <figcaption>A popup contact form in WordPress on the front end using the Popup Maker plugin.</figcaption>
</figure>
<p>Once you have a contact form that you’re pleased with, you can look to add it to a popup using one of many solutions. However, we recommend two: the <a href="https://elementor.com" target="_blank" rel="noopener noreferrer">Elementor</a> page builder, and the <a href="https://wordpress.org/plugins/popup-maker/" target="_blank" rel="noopener noreferrer">Popup Maker</a> WordPress popup plugin.</p>
<p>In fact, Tripetto has a <a href="https://tripetto.com/help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-elementor-widget/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">widget available for Elementor</a>, which you can access like any other from the Elementor interface:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/elementor-tripetto-widget.png" width="1000" height="528" alt="Tripetto’s Elementor widget in the editor sidebar." loading="lazy" />
  <figcaption>Tripetto’s Elementor widget in the editor sidebar.</figcaption>
</figure>
<p>To <a href="https://elementor.com/help/popups/" target="_blank" rel="noopener noreferrer">create a popup</a> within Elementor, head to the <i>Templates > Popups</i> screen within Elementor, and add a new popup. From there, you can adjust the template like any other widget within Elementor:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/elementor-popup.png" width="1000" height="435" alt="Setting up a popup template within Elementor." loading="lazy" />
  <figcaption>Setting up a popup template within Elementor.</figcaption>
</figure>
<p>Of course, this includes the ability to add a Tripetto form to that popup. If you don’t use Elementor, there are also plugins available to help you build popups. The best WordPress solution we know is Popup Maker.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/popup-maker.png" width="1000" height="218" alt="The Popup Maker plugin." loading="lazy" />
  <figcaption>The Popup Maker plugin.</figcaption>
</figure>
<p>Once you install and activate the plugin, head to the <i>Popup Maker > Create Popup</i> screen within WordPress. This will bring you to an edit screen:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/popup-maker-edit.png" width="1000" height="620" alt="The Popup Maker editing screen within WordPress." loading="lazy" />
  <figcaption>The Popup Maker editing screen within WordPress.</figcaption>
</figure>
<p>You’ll look to fill in all of the fields here (for which Popup Maker offers a <a href="https://docs.wppopupmaker.com/article/488-create-your-first-popup-summary-guide" target="_blank" rel="noopener noreferrer">comprehensive guide</a>) and begin to add content to the popup. This is where you’ll add a Tripetto shortcode. You can find these options on the Share tab of Tripetto:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-shortcode.png" width="1000" height="735" alt="Setting up a Tripetto shortcode within the form builder." loading="lazy" />
  <figcaption>Setting up a Tripetto shortcode within the form builder.</figcaption>
</figure>
<p>You’ll need to set your parameters using the checkboxes, then copy the form shortcode to the clipboard. From here, move to your popup editing screen and paste the shortcode into your content. You’ll also want to toggle the <i>Popup Enabled</i> button on this screen:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/popup-maker-tripetto-form.png" width="1000" height="476" alt="The popup editing screen complete with a Popup Enabled button." loading="lazy" />
  <figcaption>The popup editing screen complete with a <i>Popup Enabled</i> button.</figcaption>
</figure>
<p>Once you click the blue <i>Update</i> button, check out the front end of your site to see how your popup contact form in WordPress performs. From there, make changes as you see fit.</p>
<hr/>

<h2>Quick Tips to Help You Build an Engaging Popup Contact Form</h2>
<p>It can be hard to create the perfect popup contact form in WordPress. However, there are a <a href="https://tripetto.com/blog/four-use-cases-on-how-to-improve-your-forms/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">few typical practices</a> and tips you can follow to ensure you have the basics nailed:</p>
<ul>
  <li>Keep your forms straightforward, in both language and length. You’ll want to use ‘human’ language – informal, business casual, loose – in order to speak in a direct way to respondents.</li>
  <li>When it comes to length, this is where conditional logic can show its worth. You can create many questions for your form, but the user doesn’t have to see them. In fact, they’ll only see the most necessary and relevant questions based on your implementation and their response.</li>
  <li>Speaking of encouraging language, this is also something you’ll want to do with your Call To Action (CTA) buttons. It’s best to employ strong and specific nouns that ask the user to take action, and also speak in the first person: “Send My Email,” “Apply Now,” “Sign Me Up,” and more.
    <figure>
      <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-cta.png" width="1000" height="657" alt="A CTA button within Tripetto." loading="lazy" />
      <figcaption>A CTA button within Tripetto.</figcaption>
    </figure>
  </li>
  <li>Give your buttons premium space, with a contrasting color scheme. This will draw the user’s eyes to it, and provide a clear way to take action on your popup form in WordPress.</li>
  <li>Make sure that you follow up with form leads and responses in a prompt and timely fashion. It gives the user a sense of trust in what you do, and cuts out any ambiguity. What’s more, this is a snap to achieve with Tripetto’s built-in automation functionality.
    <figure>
      <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-notifications.png" width="1000" height="574" alt="Setting up Tripetto’s notifications in the editor." loading="lazy" />
      <figcaption>Setting up Tripetto’s notifications in the editor.</figcaption>
    </figure>
  </li>
</ul>
<p>You can choose to set up and <a href="https://tripetto.com/help/articles/how-to-automate-email-notifications-for-each-new-result/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">send email</a> or <a href="https://tripetto.com/help/articles/how-to-automate-slack-notifications-for-each-new-result/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">Slack notifications</a> with a couple of clicks. Tripetto uses WordPress’ built-in email functionality to send them your way, so you can take a hands-off approach with your form, and get down to other business.</p>
<hr/>

<h2>Boost your form responses with a popup contact form</h2>
<p>Regardless of whether you want to generate leads, offer customer support, or give users a simple way to get in touch, a contact form is an essential element of your WordPress website.</p>
<p>By extension, using popup functionality can make sure this form displays wherever the user is on your site, based on the actions you decide. This will increase completion rates, boost engagement, and take away navigation elements that could see a website visitor disengage with your form.</p>
<p>This tutorial gives you two ways to display a popup contact form in WordPress: Using <a href="https://elementor.com/" target="_blank" rel="noopener noreferrer">Elementor</a> or the <a href="https://wppopupmaker.com/" target="_blank" rel="noopener noreferrer">Popup Maker</a> plugin. However, you’ll need a top-notch contact form, and <a href="https://tripetto.com/wordpress/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">Tripetto</a> can offer what you need.</p>
<p>It gives you an intuitive and flexible way to create smart, conversational forms using conditional logic. The drag-and-drop builder needs no code to operate, and includes almost everything you’ll need for any type of form. What’s more, all of the data stays within your WordPress database. However, with Tripetto’s webhook integrations, you can pull in your favorite third-party platforms too.</p>
<p>You can own a single-site Tripetto license for <a href="https://tripetto.com/wordpress/pricing/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress">$99 per year</a>. In addition, each purchase comes with a 14-day money-back guarantee.</p>
<div>
  <a href="https://tripetto.com/wordpress/pricing/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=pop-up-contact-form-wordpress" class="blocklink">
    <div>
      <span class="title">Get Tripetto Pro for WordPress</span>
      <span class="description">Tripetto Pro is for the Tripetto WordPress plugin and comes in 3 all-inclusive variants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" />
    </div>
  </a>
</div>
