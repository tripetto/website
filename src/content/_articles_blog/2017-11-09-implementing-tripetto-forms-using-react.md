---
layout: blog-article
base: ../../
permalink: /blog/implementing-tripetto-forms-using-react/
title: Implementing Tripetto using React - Tripetto Blog
description: Tripetto brings a new way of creating and deploying forms and surveys in websites and applications.
article_id: implementing-tripetto-forms-using-react
article_title: Implementing Tripetto using React
article_slug: Implementing Tripetto using React
article_folder: 20171109
author: mark
time: 7
category: coding-tutorial
category_name: Coding tutorial
tags: [coding-tutorial, products, editor, collector, sdk, release]
areas: [sdk]
year: 2017
scripts: [prism]
stylesheets: [prism-dark]
---
<p>Tripetto brings a new way of creating and deploying forms and surveys in websites and applications.</p>

<blockquote>
  <h4>❌ This is a deprecated SDK blog article</h4>
  <p>Please note that the SDK concepts and code snippets in this article are about a deprecated version of the Tripetto SDK. In the meantime the SDK has evolved heavingly. For up-to-date SDK documentation, please have a look at the following links:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/" target="_blank">Tripetto FormBuilder SDK website</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/docs/" target="_blank">Tripetto FormBuilder SDK documentation</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}blog/formbuilder-sdk-implementation-guide/">Power Up Your Forms: A Comprehensive Guide to implementing the Tripetto FormBuilder SDK</a></li>
  </ul>
</blockquote>
<p>You use its intuitive <a href="{{ page.base }}sdk/docs/builder/introduction/" target="_blank"><strong>graphical editor</strong></a> to build and edit smart forms with logic and conditional flows in 2D on a self-organizing storyboard. In any modern browser. Mouse, touch or pen.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/editor.gif" width="1200" height="750" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>The Tripetto editor in action!</figcaption>
</figure>
<p>And you then deploy these smart forms in websites and applications using the supplementary <a href="{{ page.base }}sdk/docs/runner/introduction/" target="_blank"><strong>collector library</strong></a>. Anything you build in the editor, the collector will simply run. You just focus on the visuals of the embedded form.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/editor-collector.png" width="3096" height="1260" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>The editor on the left and the React collector with DevTools open on the right</figcaption>
</figure>
<p>We’ll explain here how to build such a collector implementation using <a href="https://react.dev" target="_blank" rel="noopener noreferrer">React</a> that’s capable of running Tripetto smart forms inside a website or application.</p>
<hr />

<h2>The basics</h2>
<p>Before we dive into the real power-coding, let’s briefly touch on the following basics behind Tripetto.</p>
<h3>Editor</h3>
<p>The editor is the graphical 2D form designer. It runs in any modern browser. Forms you build in the editor are stored in JSON files. We call these files <a href="{{ page.base }}sdk/docs/builder/api/interfaces/IDefinition/" target="_blank"><strong>form definitions</strong></a>. A single form definition contains the complete structure of a form. The definition file is parsed by the collector.</p>
<h3>Collector</h3>
<p>The collector basically transforms a form definition into an executable program. Once correctly implemented in your website or application, the collector autonomously parses any form definition you build or alter in the editor to an actual working form. You only have to worry about the visuals. We don’t impose any particular UI for the collector.</p>
<h3>Blocks</h3>
<p>You compose forms in the editor with the available <strong>building blocks</strong>. These essentially are the different element types (e.g. <a href="{{ page.base }}sdk/docs/blocks/stock/text/" target="_blank">text input</a>, <a href="{{ page.base }}sdk/docs/blocks/stock/checkbox/" target="_blank">checkbox</a>, <a href="{{ page.base }}sdk/docs/blocks/stock/dropdown/" target="_blank">dropdown</a> etc.) you typically use in a form. You decide which blocks to use in the editor and collector. And you may also develop your own.</p>
<p><strong>That’s it for now. Let’s start coding!</strong></p>
<blockquote>
  <h4>📌 Also see: Custom blocks</h4>
  <p>For this tutorial we are going to use some of our default blocks. If you want to learn more about creating custom blocks, have a look here:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/docs/blocks/custom/introduction/" target="_blank">FormBuilder SDK documentation - Custom blocks</a></li>
  </ul>
</blockquote>

<h2>Part A — Setting up your project</h2>
<p>First of all, we’re assuming you have <a href="https://nodejs.org/" target="_blank" rel="noopener noreferrer">Node.js</a> installed. If you don’t, please go ahead and take care of that first. After that, do the following.</p>
<p><strong>1. Create</strong> a new folder for your project. Then create two subfolders in there. One for our source and one for some static files:</p>

<pre class="line-numbers"><code class="language-bash">mkdir your-project
cd your-project
mkdir src
mkdir static</code></pre>

<p><strong>2. Initialize</strong> your <code>package.json</code> file:</p>

<pre class="line-numbers"><code class="language-bash">npm init -y</code></pre>

<p><strong>3. Install</strong> the required packages:</p>

<pre class="line-numbers"><code class="language-bash">npm install tripetto-collector react react-dom @types/react @types/react-dom @types/superagent typescript ts-loader webpack webpack-cli webpack-dev-server superagent --save-dev</code></pre>

<p><strong>4. Create</strong> a <code>tsconfig.json</code> file in the root of your project folder with the following content:</p>

<pre class="line-numbers"><code class="language-json">{
  "compilerOptions": {
    "target": "ES5",
    "module": "commonjs",
    "baseUrl": "./src",
    "jsx": "react",
    "strict": true,
    "experimentalDecorators": true
  },
  "include": ["./src/**/*.ts", "./src/**/*.tsx"]
}</code></pre>

<p><strong>6. Setup</strong> some test/build tasks by inserting a <code>script</code> section in your<code>package.json</code> file:</p>

<pre class="line-numbers"><code class="language-json">"scripts": {
  "test": "webpack-dev-server --mode development",
  "make": "webpack --mode production"
}</code></pre>

<p><strong>7. Create</strong> the application entry point by creating the <code>./src/app.tsx</code> file with the following content:</p>

<pre class="line-numbers"><code class="language-tsx">import * as React from "react";
import * as ReactDOM from "react-dom";

class Collector extends React.Component {
  render() {
    return (
      &lt;div>
        Hello world!
      &lt;/div>
    );
  }
}

ReactDOM.render(
  &lt;Collector />,
  document.getElementById("app")
);</code></pre>

<p><strong>8. Create</strong> a static HTML page <code>./static/index.html</code> for your project with the following content:</p>

<pre class="line-numbers"><code class="language-html">&lt;!DOCTYPE html>
&lt;html>
&lt;body>
    &lt;div id="app">&lt;/div>
    &lt;script src="bundle.js">&lt;/script>
&lt;/body>
&lt;/html></code></pre>

<h3>That’s it for now. Let’s check what we’ve got!</h3>
<p>If you’ve correctly followed the steps above you should now have this file tree:</p>

<pre class="line-numbers"><code class="language-bash">your-project/
  src/
    app.tsx
  static/
    index.html
  package.json
  tsconfig.json
  webpack.config.js</code></pre>

<p>This should now be an actual working application. We can run it by executing the following command:</p>

<pre class="line-numbers"><code class="language-bash">npm test</code></pre>

<p>This will start the <em class="kq">webpack-dev-server</em> which invokes webpack, compiles your application and makes it available at <code>http://localhost:9000</code>. You should be able to open it with your browser and it should welcome you with the iconic <code>Hello world!</code>. <strong>Yeah!</strong></p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/hello-world.png" width="1086" height="569" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>The editor on the left and the React collector with DevTools open on the right</figcaption>
</figure>
<hr />

<h2>Part B — Coding the collector</h2>
<p>We are going to create a React component for the Tripetto collector. It will be capable of running Tripetto forms and can be used anywhere in your React application. It accepts a form definition as a prop and we use it to implement three blocks (text input, dropdown and checkbox).</p>
<p>Take the steps below to get this going.</p>
<blockquote>
  <h4>🚧 Warning: Deprecated SDK code snippets and packages</h4>
  <p>Please note that the below code snippets and used packages are about a deprecated version of the Tripetto SDK. In the meantime the SDK has evolved heavingly. For up-to-date SDK documentation, please have a look at the following links:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/" target="_blank">Tripetto FormBuilder SDK website</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/docs/" target="_blank">Tripetto FormBuilder SDK documentation</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}blog/formbuilder-sdk-implementation-guide/">Power Up Your Forms: A Comprehensive Guide to implementing the Tripetto FormBuilder SDK</a></li>
  </ul>
</blockquote>
<p><strong>1. Create</strong> a <code>./src/helper.tsx</code> file with the following content to implement the collector:</p>

<pre class="line-numbers"><code class="language-tsx">import * as Tripetto from "tripetto-collector";
import * as React from "react";

export interface IBlock extends Tripetto.NodeBlock {
  render: () => React.ReactNode;
}

export class CollectorHelper extends Tripetto.Collector&lt;IBlock> {
  render(): React.ReactNode {
    const storyline = this.storyline;

    if (!storyline) {
      return;
    }

    return (
      &lt;>
        {storyline.map((moment: Tripetto.Moment&lt;IBlock>) =>
          moment.nodes.map((node: Tripetto.IObservableNode&lt;IBlock>) =>
            node.block ? (
              &lt;div key={node.key}>{node.block.render()}&lt;/div>
            ) : (
              &lt;div key={node.key}>
                {Tripetto.castToBoolean(node.props.nameVisible, true) && (
                  &lt;h3>{node.props.name || "..."}&lt;/h3>
                )}
                {node.props.description && &lt;p>{node.props.description}&lt;/p>}
              &lt;/div>
            )
          )
        )}

        &lt;button
          type="button"
          disabled={storyline.isAtStart}
          onClick={() => storyline.stepBackward()}
        >
          Back
        &lt;/button>

        &lt;button
          type="button"
          disabled={
            storyline.isFailed ||
            (storyline.isAtFinish && !storyline.isFinishable)
          }
          onClick={() => storyline.stepForward()}
        >
          {storyline.isAtFinish ? "Complete" : "Next"}
        &lt;/button>
      </>
    );
  }
}</code></pre>

<p>This class simply extends the base collector class of Tripetto. It implements a simple render function that renders the blocks and some buttons. It also implements an interface called <code>IBlock</code> that defines the contract for the block implementation. In this case each block should implement a method <code>render</code>. In other words, this class <code>CollectorHelper</code> is able to use all the blocks that have implemented a proper <code>render</code> method.</p>
<p><strong>2. Create</strong> a<code>./src/component.tsx</code> file with the following content to implement our React component:</p>

<pre class="line-numbers"><code class="language-tsx">import * as React from "react";
import * as Tripetto from "tripetto-collector";
import { CollectorHelper } from "./helper";

export class Collector extends React.PureComponent&lt;{
  definition: Tripetto.IDefinition | string;
  onFinish?: (instance: Tripetto.Instance) => void;
}> {
  collector = new CollectorHelper(this.props.definition);

  /** Render the collector. */
  render(): React.ReactNode {
    return (
      &lt;>
        &lt;h1>{this.collector.name}&lt;/h1>
        {this.collector.render()}
      &lt;/>
    );
  }

  componentDidMount(): void {
    this.collector.onChange = () => {
      // Since the collector has the actual state, we need to update the component.
      // We are good React citizens. We only do this when necessary!
      this.forceUpdate();
    };

    this.collector.onFinish = (instance: Tripetto.Instance) => {
      if (this.props.onFinish) {
        this.props.onFinish(instance);
      }
    };
  }
}</code></pre>

<p>The React component shown above is actually quite simple. It creates a new instance of the collector helper (implemented in step 1) and invokes its render function when the component needs to be rendered. Since the state of the form is kept in the collector instance, the collector should be able to request an update of the component. So the rest of the code inside the component does just that. We also implement an <code>onFinish</code> prop that can be used to bind a function that is invoked when the collector is finished.</p>
<hr />

<h2>Part C — Implementing blocks</h2>
<p>This tutorial covers the implementation of three simple blocks:</p>
<ul>
  <li><a href="{{ page.base }}sdk/docs/blocks/stock/checkbox/" target="_blank"><strong>Checkbox</strong></a><strong>;</strong></li>
  <li><a href="{{ page.base }}sdk/docs/blocks/stock/dropdown/" target="_blank"><strong>Dropdown</strong></a><strong>;</strong></li>
  <li><a href="{{ page.base }}sdk/docs/blocks/stock/text/" target="_blank"><strong>Text input</strong></a><strong>.</strong></li>
</ul>
<blockquote>
  <h4>🚧 Warning: Deprecated SDK code snippets and packages</h4>
  <p>Please note that the below code snippets and used packages are about a deprecated version of the Tripetto SDK. In the meantime the SDK has evolved heavingly. For up-to-date SDK documentation, please have a look at the following links:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/" target="_blank">Tripetto FormBuilder SDK website</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/docs/" target="_blank">Tripetto FormBuilder SDK documentation</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}blog/formbuilder-sdk-implementation-guide/">Power Up Your Forms: A Comprehensive Guide to implementing the Tripetto FormBuilder SDK</a></li>
  </ul>
</blockquote>
<p><strong>1. Add</strong> these blocks to the project first. To do so, fire up your terminal/command line and enter:</p>

<pre class="line-numbers"><code class="language-bash">npm install tripetto-block-checkbox tripetto-block-dropdown tripetto-block-text --save-dev</code></pre>

<p><strong>2. Add</strong> a special <a href="{{ page.base }}sdk/docs/builder/cli/configuration/" target="_blank">configuration section</a> to our <code>package.json</code> file:</p>

<pre class="line-numbers"><code class="language-json">"tripetto": {
  "blocks": [
    "tripetto-block-checkbox",
    "tripetto-block-dropdown",
    "tripetto-block-text"
  ],
  "noFurtherLoading": true
}</code></pre>

<p>This section instructs the editor to only load the three blocks we just added. Your <code>package.json</code> should now look like this:</p>

<pre class="line-numbers"><code class="language-json">{
  "name": "your-project",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "test": "webpack-dev-server --mode development",
    "make": "webpack --mode production"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "devDependencies": {
    "@types/react": "^16.7.18",
    "@types/react-dom": "^16.0.11",
    "@types/superagent": "^3.8.5",
    "react": "^16.7.0",
    "react-dom": "^16.7.0",
    "superagent": "^4.1.0",
    "tripetto-block-checkbox": "^2.0.0",
    "tripetto-block-dropdown": "^2.0.0",
    "tripetto-block-text": "^2.0.0",
    "tripetto-collector": "^1.0.0",
    "ts-loader": "^5.3.2",
    "typescript": "^3.2.2",
    "webpack": "^4.28.2",
    "webpack-cli": "^3.1.2",
    "webpack-dev-server": "^3.1.14"
  },
  "tripetto": {
    "blocks": [
      "tripetto-block-checkbox",
      "tripetto-block-dropdown",
      "tripetto-block-text"
    ],
    "noFurtherLoading": true
  }
}</code></pre>

<p><strong>3. Implement</strong> the blocks in the collector component by adding the following files:</p>
<ul>
  <li><code>./src/blocks/checkbox.tsx</code></li>
  <li><code>./src/blocks/dropdown.tsx</code></li>
  <li><code>./src/blocks/text.tsx</code></li>
</ul>
<p>As you can see, a block simply implements a render function. These functions are automatically invoked by the collector library. Each block needs to register itself by calling the <code>@tripetto.block</code> decorator at the top of the class. The source codes for the three blocks we want to implement look as follows.</p>

<pre class="line-numbers"><code class="language-tsx">import * as React from "react";
import * as Tripetto from "tripetto-collector";
import { Checkbox } from "tripetto-block-checkbox/collector";
import { IBlock } from "../helper";

@Tripetto.block({
  type: "node",
  identifier: "tripetto-block-checkbox"
})
export class CheckboxBlock extends Checkbox implements IBlock {
  render(): React.ReactNode {
    return (
      &lt;label>
        &lt;input
          key={this.key()}
          type="checkbox"
          defaultChecked={this.checkboxSlot.value}
          onChange={(e: React.ChangeEvent&lt;HTMLInputElement>) => {
            this.checkboxSlot.value = e.target.checked;
          }}
        />
        {this.node.name}
      &lt;/label>
    );
  }
}</code></pre>

<pre class="line-numbers"><code class="language-tsx">import * as React from "react";
import * as Tripetto from "tripetto-collector";
import { Dropdown, IDropdownOption } from "tripetto-block-dropdown/collector";
import { IBlock } from "../helper";

@Tripetto.block({
  type: "node",
  identifier: "tripetto-block-dropdown"
})
export class DropdownBlock extends Dropdown implements IBlock {
  render(): React.ReactNode {
    return (
      &lt;div>
        &lt;label>
          {this.node.name || "..."}
          {this.required && &lt;span>*&lt;/span>}
        &lt;/label>
        {this.node.description && &lt;p>{this.node.description}&lt;/p>}
        &lt;select
          key={this.key()}
          defaultValue={this.value}
          onChange={(e: React.ChangeEvent&lt;HTMLSelectElement>) => {
            this.value = e.target.value;
          }}
        >
          {this.node.placeholder && (
            &lt;option value="">{this.node.placeholder}&lt;/option>
          )}
          {this.props.options.map(
            (option: IDropdownOption) =>
              option.name && (
                &lt;option key={option.id} value={option.id}>
                  {option.name}
                &lt;/option>
              )
          )}
        &lt;/select>
      &lt;/div>
    );
  }
}</code></pre>

<pre class="line-numbers"><code class="language-tsx">import * as React from "react";
import * as Tripetto from "tripetto-collector";
import { Text } from "tripetto-block-text/collector";
import { IBlock } from "../helper";

@Tripetto.block({
  type: "node",
  identifier: "tripetto-block-text"
})
export class TextBlock extends Text implements IBlock {
  render(): React.ReactNode {
    return (
      &lt;div>
        &lt;label>
          {this.node.name || "..."}
          {this.required && &lt;span>*&lt;/span>}
        &lt;/label>
        {this.node.description && &lt;p>{this.node.description}&lt;/p>}
        &lt;input
          key={this.key()}
          type="text"
          required={this.required}
          defaultValue={this.textSlot.value}
          placeholder={this.node.placeholder}
          maxLength={this.maxLength}
          onChange={(e: React.ChangeEvent&lt;HTMLInputElement>) => {
            this.textSlot.value = e.target.value;
          }}
        />
      &lt;/div>
    );
  }
}</code></pre>
<hr />

<h2>Part D — Firing things up</h2>
<p>We are almost done. Follow these final steps to get airborne.</p>
<blockquote>
  <h4>🚧 Warning: Deprecated SDK code snippets and packages</h4>
  <p>Please note that the below code snippets and used packages are about a deprecated version of the Tripetto SDK. In the meantime the SDK has evolved heavingly. For up-to-date SDK documentation, please have a look at the following links:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/" target="_blank">Tripetto FormBuilder SDK website</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/docs/" target="_blank">Tripetto FormBuilder SDK documentation</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}blog/formbuilder-sdk-implementation-guide/">Power Up Your Forms: A Comprehensive Guide to implementing the Tripetto FormBuilder SDK</a></li>
  </ul>
</blockquote>
<p><strong>1. Import</strong> your blocks in your <code>app.tsx</code> to make sure the blocks are registered:</p>

<pre class="line-numbers"><code class="language-typescript">import "./blocks/checkbox";
import "./blocks/dropdown";
import "./blocks/text";</code></pre>

<p><strong>2. Add</strong> some code to load a form definition and feed it to the collector. Open your <code>app.tsx</code> and replace the code with:</p>

<pre class="line-numbers"><code class="language-tsx">import * as React from "react";
import * as ReactDOM from "react-dom";
import * as Superagent from "superagent";
import { Collector } from "./component";
import "./blocks/checkbox";
import "./blocks/dropdown";
import "./blocks/text";

// Fetch our form and render the collector component.
Superagent.get("form.json").end((error: {}, response: Superagent.Response) => {
  if (response.ok) {
    ReactDOM.render(
      &lt;Collector definition={response.text} />,
      document.getElementById("app")
    );
  } else {
    alert("Bummer! Cannot load form definition. Does the file exists?");
  }
});</code></pre>

<p>We use <a href="https://github.com/visionmedia/superagent" target="_blank" rel="noopener noreferrer">superagent</a> here to fetch the form definition stored in <code>./static/form.json</code> and feed it to the collector component.</p>
<p><strong>3. Install</strong> the Tripetto editor using <a href="https://www.npmjs.com/" target="_blank" rel="noopener noreferrer">npm</a> (detailed instructions <a href="{{ page.base }}sdk/docs/builder/cli/installation/" target="_blank">here</a>) to create a form definition:</p>

<pre class="line-numbers"><code class="language-bash">npm install tripetto -g</code></pre>

<p>This command installs the editor globally and allows you to start it from your command line just by entering <code>tripetto</code>. And it allows you to start creating a form by executing the following command from your project folder (make sure you execute this command from the root <code>./</code> of your project folder):</p>

<pre class="line-numbers"><code class="language-bash">tripetto ./static/form.json</code></pre>

<p>This will start the editor at <code><a href="http://localhost:3333" target="_blank" rel="noopener noreferrer">http://localhost:3333</a></code> so you can create your form with it. When you’re done, hit the <strong>Save </strong>button to store the form definition. Verify that the file <code>./static/form.json</code> is present.</p>
<p><strong>4. Start</strong> your collector by executing <code>npm test</code>. Open <code>http://localhost:9000</code> in your browser. You should then see the form running.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/collector.png" width="1200" height="760" alt="Screenshot of collector in Tripetto" loading="lazy" />
</figure>
<hr />

<h2>Part E — Processing collected data</h2>
<p>Now that your form is running properly, you probably want to do something with data you collect through it. That's where our <code>onFinish</code> prop comes into play.</p>
<blockquote>
  <h4>🚧 Warning: Deprecated SDK code snippets and packages</h4>
  <p>Please note that the below code snippets and used packages are about a deprecated version of the Tripetto SDK. In the meantime the SDK has evolved heavingly. For up-to-date SDK documentation, please have a look at the following links:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/" target="_blank">Tripetto FormBuilder SDK website</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/docs/" target="_blank">Tripetto FormBuilder SDK documentation</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}blog/formbuilder-sdk-implementation-guide/">Power Up Your Forms: A Comprehensive Guide to implementing the Tripetto FormBuilder SDK</a></li>
  </ul>
</blockquote>
<p>Open <code>app.tsx</code> and add the prop as shown below. This will output the collected data to your console when the form is completed. We use the export API for this job.</p>

<pre class="line-numbers"><code class="language-tsx">&lt;Collector
  definition={response.text}
  onFinish={(instance: Tripetto.Instance) =>
    console.dir(Tripetto.Export.fields(instance))}
/></code></pre>

<p>Don't forget to import the required symbols. In this case:</p>

<pre class="line-numbers"><code class="language-typescript">import * as Tripetto from "tripetto-collector";</code></pre>
<hr />

<h2>Browse the full example</h2>
<div>
  <a href="https://gitlab.com/tripetto/examples/react-bootstrap" target="_blank" class="blocklink" rel="noopener noreferrer">
    <div>
      <span class="title">Tripetto / Examples / React example<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Demo for implementing the Tripetto collector using React.</span>
      <span class="url">gitlab.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/blog/{{ page.article_folder }}/logo-react.png" width="160" height="160" alt="React logo" />
    </div>
  </a>
</div>

<h3>What else?</h3>
<p>This tutorial is just a basic example of what you can do with Tripetto. It shows you how to implement a collector and some simple building blocks. But Tripetto can do more than that. For example, you can write your own conditional blocks that allow Tripetto to branch on certain conditions — making ever smarter forms. Dive into our <a href="{{ page.base }}sdk/docs/" target="_blank">documentation</a> to learn more!</p>

<h3>Documentation</h3>
<p>You can find all <a href="{{ page.base }}sdk/docs/" target="_blank">Tripetto docs here</a>. The detailed collector documentation can be found <a href="{{ page.base }}sdk/docs/runner/introduction/" target="_blank">here</a>. If you want to develop your own building blocks, have a look around <a href="{{ page.base }}sdk/docs/blocks/introduction/" target="_blank">here</a>.</p>

<h3>Other examples</h3>
<p>We also have <a href="https://gitlab.com/tripetto/examples" target="_blank" rel="noopener noreferrer">examples</a> for <a href="https://tripetto.gitlab.io/examples/react-mui/" target="_blank" rel="noopener noreferrer">React with Material UI</a>, <a href="https://tripetto.gitlab.io/examples/angular-bootstrap/" target="_blank" rel="noopener noreferrer">Angular</a>, <a href="https://tripetto.gitlab.io/examples/angular-material/" target="_blank" rel="noopener noreferrer">Angular Material</a> and more:</p>
<div>
  <a href="https://gitlab.com/tripetto/examples/react-mui" target="_blank" class="blocklink" rel="noopener noreferrer">
    <div>
      <span class="title">Tripetto / Examples / React Material-UI example<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Demo for implementing the Tripetto collector using React with Material-UI including pause/resume functionality.</span>
      <span class="url">gitlab.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/blog/{{ page.article_folder }}/logo-materialui.png" width="160" height="160" alt="Material-UI logo" />
    </div>
  </a>
</div>
<div>
  <a href="https://gitlab.com/tripetto/examples/react-bootstrap" target="_blank" class="blocklink" rel="noopener noreferrer">
    <div>
      <span class="title">Tripetto / Examples / React Bootstrap example<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Demo for using Tripetto to build a conversational collector (using React).</span>
      <span class="url">gitlab.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/blog/{{ page.article_folder }}/logo-conversational.png" width="128" height="128" alt="Conversational logo" />
    </div>
  </a>
</div>
<div>
  <a href="https://gitlab.com/tripetto/examples/angular-bootstrap" target="_blank" class="blocklink" rel="noopener noreferrer">
    <div>
      <span class="title">Tripetto / Examples / Angular Bootstrap example<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Demo for implementing the Tripetto collector using Angular including pause/resume functionality.</span>
      <span class="url">gitlab.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/blog/{{ page.article_folder }}/logo-angular.png" width="160" height="171" alt="Angular logo" />
    </div>
  </a>
</div>
<div>
  <a href="https://gitlab.com/tripetto/examples/angular-material" target="_blank" class="blocklink" rel="noopener noreferrer">
    <div>
      <span class="title">Tripetto / Examples / Angular Material example<i class="fas fa-external-link-alt"></i></span>
      <span class="description">Demo for implementing the Tripetto collector using Angular Material including pause/resume functionality.</span>
      <span class="url">gitlab.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/blog/{{ page.article_folder }}/logo-angular.png" width="160" height="171" alt="Angular logo" />
    </div>
  </a>
</div>
