---
layout: blog-article
base: ../../
permalink: /blog/forms-logic/
title: The ultimate guide to form logic (2022) - Tripetto Blog
description: If you’re looking for a quick rundown on online form logic, you’re in the right place. Click here for more info!
article_title: The ultimate guide to form logic - Tools, tips, and setup guide (2022)
article_slug: Form logic guide
article_folder: 20220412
article_image: cover.webp
article_image_width: 1640
article_image_height: 1081
article_image_caption: The ultimate guide to form logic - Tools, tips, and setup guide (2022)
author: jurgen
time: 9
category: guide
category_name: Guide
tags: [guide]
areas: [wordpress]
year: 2022
---
<p>If you’re looking for a quick rundown on online form logic, you’re in the right place. Read further for more info!</p>

<p>What's the key to expanding and better understanding your audience?<br/>Answer: gathering and interpreting invaluable customer information - without it being a headache.</p>
<p>The same applies to agencies working on behalf of multiple clients keen to receive website visitor data.</p>
<p>Using WordPress forms is the most straightforward way to achieve this. They're excellent for collecting and processing data to better understand your website visitors.</p>
<p>However, creating engaging, smarter forms that bear fruit can be complicated with a complex setup. Fortunately, there's help out there in the guise of conditional form logic. This empowers you to design forms that only ask questions relevant to the individual website visitor. This, in turn, provides a more personalized experience- without visitors needlessly giving up any info you don’t need.</p>
<p>As such, conditional logic allows you to create dynamic forms that respond to user input. Consequently, such surveys, lead generation tools, and event registration forms tend to convert better than those that don’t use conditional form logic.</p>
<p>That said, we'll explore how to create attractive and interactive forms customized for your target audience.</p>
<hr/>

<h2>Why are forms necessary for businesses, and what are the problems with traditional WordPress forms? </h2>
<p>Forms are an essential part of any business, with <a href="https://wpforms.com/online-form-statistics-facts/#:~:text=4.%2074%25%20of%20companies%20use%20web%20forms%20for%20lead%20generation%2C%20with%2049.7%25%20stating%20their%20online%20forms%20are%20their%20highest%20converting%20lead%20generation%20tool." target="_blank" rel="noopener noreferrer">74% of companies now using them</a>. They’re used online and offline to gather data from existing and potential customers. This data is then stored and used to boost engagement and build a better rapport with your audience.</p>
<p>WordPress forms are one such way to gather website visitor data. However, they’re not always everyone’s default choice. For instance, you may want to gather more info than a traditional WordPress form allows for. Or, you might wish to create a more informal-looking form that seems more like a chatbot.</p>
<p>Fortunately, a workaround is available…</p>

<h3>More about conditional form logic</h3>
<p>As we’ve already hinted, using conditional logic forms enables an environment where you can apply “if x happens, then do y” rules.</p>
<p>In other words, conditional logic makes automatic calculations to show questions and form fields to users based on their previous answers. Needless to say, this works wonders for creating more conversational WordPress forms.</p>
<p>That said, below are some of the benefits of injecting conditional form logic into your online forms:</p>
<ul>
  <li>You provide website visitors with a more personalized user experience by asking questions based on their answers. </li>
  <li>You can send more personalized emails to respondents based on their form responses. For instance, using their full names or sending localized/user-specific content.</li>
  <li>You can create forms that can’t be submitted until the required fields are completed. For example, users ticking that they’ve read and understood your terms and conditions.</li>
  <li>Depending on the respondents' answers, you can allow users to skip pages in long forms.</li>
  <li>You can provide users with different media depending on their stated preference, for example, PDF downloads, videos, links to a website, etc.</li>
</ul>
<hr/>

<h2>How to create forms using conditional logic</h2>

<p>One way to simplify the form creation process using conditional logic is to use a WordPress form building plugin like Tripetto. Using such a plugin empowers you to build customized forms and access all the data they collect inside your WordPress dashboard.</p>
<p>The best part? You don’t need any coding know-how to achieve this (html).</p>
<p>As we’ve already said, you can use conditional logic, actions, and calculations to create more conversational forms. Examples of such form logic include:</p>

<h3>Branch tracks</h3>
<p>This is a basic requirement for conditional logic, as conditions later in the form pipeline might rely on the information requested a few steps before. Create branches that appear as new tracks inside the form builder. For example, you can add unlimited sections, action blocks, or question blocks in each branch. These branches are executed based on the branch conditions, in most cases based on the respondent’s given answer(s). This ensures you only ask questions relevant to the respondent. For example, a medical form might start by asking for basic demographic information, including the user’s sex. Later the form can recall this information and ask the user if they are or might be pregnant. However, this question will only appear if the respondent entered “female.”</p>

<h3>Jump questions</h3>
<p>Enable the form to skip questions that aren’t relevant to you or the user. For example, if users don’t have a discount code to enter, you might want to enable them to jump straight to the final payment section. Otherwise, the form should take users to a section where they can enter their discount code. This also enables respondents to manually skip part of the form they don’t wish to answer.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/discount.png" width="1125" height="615" alt="A Tripetto form that performs logic." loading="lazy" />
  <figcaption>A Tripetto form that skips the discount questions if the users doesn't have a discount code.</figcaption>
</figure>

<h3>Recall values</h3>
<p>Recalling values is the process of saving and remembering previous answers and reusing them later. A simple example is asking for a user’s name and then utilizing this value to greet them in a more personalized fashion before continuing the form.</p>

<h3>Device size</h3>
<p>With device size (also referred to as device conditions), you can show or conceal certain parts of your form depending on your website visitor's device. For example, if your visitor is using a mobile (small), tablet (medium), or desktop (large) device.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/device-size.png" width="1522" height="968" alt="A Tripetto form that performs logic based on the device size." loading="lazy" />
  <figcaption>Determine questions based on the device size of the respondent.</figcaption>
</figure>

<h3>Evaluate</h3>
<p>You may want to create more advanced conditions based on your website user’s responses. When you use this feature to validate if the given answer to specific questions corresponds with your wish(es). For example, a form where a customer is asked to review a product. If they give the product a low-star rating, the form can jump to a question that asks them why.</p>

<h3>Mail trigger</h3>
<p>As this sounds, this enables you to send emails depending on the actions taken within your form. For example, a notification email to yourself once a form is completed and/or a confirmation email to the respondent.</p>

<h3>Webhooks conditions</h3>
<p>This enables you to expand on the functionality of your form. For example, you could use Zapier to connect to other apps such as <a href="https://tripetto.com/blog/wordpress-form-to-google-sheet/?utm_source=Tripetto&utm_medium=blog&utm_campaign=forms-logic">Google Sheets</a>, MailChimp, Gmail, etc.</p>

<h3>Hidden fields</h3>
<p>Hidden fields enable the form to gather basic metadata from the browser (like reading query string parameters). That info is then saved with the rest of the form data upon <a href="https://tripetto.com/blog/wordpress-form-submission/?utm_source=Tripetto&utm_medium=blog&utm_campaign=forms-logic">submission</a>.</p>

<h3>Calculations</h3>
<p>The ability for forms to complete math according to predetermined rules is a valuable logic feature. This is especially useful for forms like mortgage rate calculators and budget calculators. For example, you may want your form to add the values entered under different sections. For instance, adding electricity bills to monthly grocery spending determines how much money remains in the user’s budget.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/calculator.png" width="1125" height="613" alt="A Tripetto form that calculates the BMI based on the given answers." loading="lazy" />
  <figcaption>A Tripetto form that calculates the BMI based on the given answers.</figcaption>
</figure>

<h3>Password match</h3>
<p><a href="https://tripetto.com/help/articles/how-to-verify-passwords-with-password-match/?utm_source=Tripetto&utm_medium=blog&utm_campaign=forms-logic">This can be used to protect a form</a>, by ensuring a respondent enters the right password before they gain access to it.</p>

<h3>Flexible endings</h3>
<p>Many forms use generic closing messages, but conditional logic forms can go further. This is especially useful if creating a more personalized experience is your priority. Flexible endings allow you to use recalled values, for example, the user’s name, and provide different ending messages based on the answers given in the form. For example, an apologetic response to a generally negative survey or a grateful one to those that responded positively.</p>
<p>Using Tripetto, you can end any branch with a custom closing message or set up a redirect after form completion. This is an excellent tool for upselling. For example, you could redirect customers to a specific product category page after they’ve summed up their needs upon form completion.</p>

<h3>Regular expression</h3>
<p>These compare answers against rules that apply to specific questions. For example, if you’re asking users to enter a password, that must meet certain conditions. In this instance, the regular expression may dictate the answer should include at least eight characters, a special sign, and a number. If the answer doesn’t meet these rules, the form can branch to tell the user to re-evaluate their answer. </p>

<h3>Custom variable</h3>
<p>Custom variables are form blocks with advanced use cases. They store a specific value, like text, a number, a date, or a boolean, and maintain that variable throughout your form. For example, a custom variable might store a numeric value that increases when certain answers are given. One great example where custom variables are often used is personality quizzes. You might have three different custom variable counting points relating to specific traits. As answers are given, the numbers add up and allow the form to calculate a result presented in a particular end text summarizing the findings.</p>

<h3>Force stop</h3>
<p>A forced stop can prevent form submission when a condition hasn’t been matched. For example, you can use a force stop to ensure only your target audience participates. For example, let’s say you sell age-restricted items, and a user enters an age below 18. In that case, you might ‘stop the form’ and send an end message explaining their survey data can’t be used due to their age.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/age.png" width="1919" height="292" alt="A Tripetto form that checks the age of the respondent." loading="lazy" />
  <figcaption>A Tripetto form that checks the age of the respondent.</figcaption>
</figure>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/force-stop.png" width="1920" height="233" alt="A Tripetto form that's stopped because the respondent does not meet the required conditions." loading="lazy" />
  <figcaption>A Tripetto form that's stopped because the respondent does not meet the required conditions.</figcaption>
</figure>
<hr/>

<h2>Tips and tricks for creating a successful conversational form using conditional logic forms</h2>
<p>Now you know what features and building blocks are available when creating forms with conditional field logic. At first, the sheer number of possibilities might seem overwhelming. So, here are some simple best practices to follow when creating effective conversational forms:</p>
<ul>
  <li><strong>Choose a good form template.</strong> Typeform-like layouts, for example, are easy and intuitive for respondents to use and look great on mobile devices.</li>
  <li><strong>Use conditional logic and dynamic variables wherever possible.</strong> It makes your form more engaging, reduces dropout rates, and creates a personalized experience.</li>
  <li><strong>Where possible, use calculations to make form results more insightful and valuable.</strong></li>
  <li><strong>Automate tedious processes to speed up customer interactions.</strong> You can use actions to send emails or store data in online databases like Google Sheets, so you don’t have to manually evaluate every form yourself.</li>
  <li><strong>Always test your form to ensure it works as expected before it goes live.</strong></li>
</ul>
<hr/>

<h2>Are You Ready to Start Using Form Logic?</h2>
<p>Smart forms offer a versatile range of benefits for anyone with complex online form needs. They allow you to collect high-quality data, increase conversions, provide better customer support, and collect valuable feedback. Not to mention, conditional logic forms ensure a smoother customer experience, showing respect for your users’ time.</p>
<p>The easiest way to create a smart form with conditional logic is Tripetto. This powerful WordPress form plugin lets you build forms and surveys using flowcharts workflows and its intuitive drag-and-drop builder with no code. </p>
<p>With Tripetto, you can easily add conditional questions, dynamic variables, and much more. You can also add <a href="https://tripetto.com/blog/wordpress-form-zapier/?utm_source=Tripetto&utm_medium=blog&utm_campaign=forms-logic">webhooks and integrations</a> to connect your forms with several automation apps, including:</p>
<ul>
  <li>Zapier;</li>
  <li>Make;</li>
  <li>Pabbly Connect;</li>
  <li>Custom webhooks.</li>
</ul>
<p>Tripetto’s clean and intuitive interface is easy for anyone to configure, not just developers. Furthermore, all data submitted using your forms is stored directly inside your WordPress site using <a href="https://tripetto.com/blog/wordpress-forms-to-database/?utm_source=Tripetto&utm_medium=blog&utm_campaign=forms-logic">Tripetto’s own database tables</a>. This gives Tripetto an edge over competitors like Typeform or Google Forms, which use external services. Tripetto users don’t face additional pricing for this functionality or require extra storage space since all information is stored locally on your site.</p>
<p>So what are you waiting for? Try Tripetto today, it offers a 14-day money-back guarantee to test its features. <a href="https://tripetto.com/wordpress/pricing/?utm_source=Tripetto&utm_medium=blog&utm_campaign=forms-logic">Click here to learn more!</a></p>
<div>
  <a href="https://tripetto.com/wordpress/pricing/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=forms-logic" class="blocklink">
    <div>
      <span class="title">Get Tripetto Pro for WordPress</span>
      <span class="description">Tripetto Pro is for the Tripetto WordPress plugin and comes in 3 all-inclusive variants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" loading="lazy" />
    </div>
  </a>
</div>
