---
layout: blog-article
base: ../../
permalink: /blog/our-shifted-focus-to-wordpress-why-the-change-and-what-does-it-mean-for-you/
title: Our shifted focus to WordPress - Tripetto Blog
description: Today we proudly announce our shifted focus to WordPress. Let us tell you why we decided to focus on WordPress and how that affects you.
article_title: Our shifted focus to WordPress 🎯<br/>Why the change? And what does it mean for you?
article_slug: Our WordPress focus
author: martijn
time: 4
category: product
category_name: Product
tags: [product-release, product, release, showcase]
areas: [wordpress]
year: 2021
---
<p>Today we proudly announce our shifted focus to WordPress. We believe the WordPress community gives us lots of chances to grow Tripetto. But don't worry: we always promised to not forget our early adopters, so let us tell you why we decided to focus on WordPress and how that affects you.</p>

<div class="article-form-embed">
  <div id="TripettoRunner" class="form-embed"></div>
</div>

<h2>Why the change?</h2>
<p>We offer Tripetto in three flavors: the studio for SaaS users, the WordPress plugin for WordPress users, and the SDK version for developers. Each of those platforms really helped us to grow and make Tripetto an alternative to many other form solutions.</p>
<p>We're proud of that and very grateful for all our daily users we have across those platforms 🙏.</p>
<p>Although each platform has proven its value, the time has come to focus more specifically on one of them. And that will be our WordPress plugin!</p>
<p>Before diving deeper into that decision, let us first assure you that this does not mean we will drop our other platforms. The WordPress focus is largely regarding our marketing efforts, but <strong>all Tripetto platforms will still be live, updated and supported</strong>.</p>

<h3>So, why WordPress?</h3>
<p>We gladly share our thoughts with you about why we chose for the WordPress focus:</p>
<ul>
  <li>WordPress is a very large and active community. Did you know that up to 40% of all websites worldwide are built with WordPress?! <i>40%!</i> That's a huge market for any form plugin.</li>
  <li>We think we offer something unique in comparison to other often used form plugins in WordPress. This gives us the opportunity to gather lots of new users to Tripetto.</li>
  <li>WordPressers are pretty tech savvy people that can easily relate to Tripetto, making them exactly the audience we should want to target.</li>
  <li>The infrastructure of WordPress makes it easier for us to grow, as the plugin is served from within each individual WordPress instance, instead of a central Tripetto server.</li>
  <li>And let's be honest: in the end we also have to be making some money to be able to continue Tripetto. With our license partner Freemius we are able to easily offer pro plans inside the WordPress plugin.</li>
</ul>
<hr/>

<h2>What does this mean in general?</h2>
<p>Now of course you're wondering: I'm already a Tripetto user; what does this mean for me? The short answer to that question is: <strong>nothing changes.</strong></p>
<p>First of all it's important to know that <strong>everything Tripetto has to offer will keep working on all our platforms.</strong> We always promised we won't forget our early adopters, so here we are. And we will also keep updating all our platforms, but our marketing efforts will be targeted specifically around the WordPress plugin.</p>
<p>Let's go through some questions that might pop up:</p>
<ul>
  <li><h4>👉 Will my current Tripetto forms still work?</h4><p>Yes, all forms will work exactly like they always worked; nothing changes.</p></li>
  <li><h4>👉 Will my future Tripetto forms still have all features?</h4><p>Yes, all features you have right now (in the free or premium version) will stay available for you; nothing changes.</p></li>
  <li><h4>👉 Will all Tripetto platforms stay up-to-date?</h4><p>Yes, all our platforms will always benefit from improvements and bugfixes we do in the core of Tripetto; nothing changes.</p></li>
  <li><h4>👉 Will all new features be available in all Tripetto platforms?</h4><p>We will always do our best to release new features to all our platforms, but we also have some WordPress specific features planned. Those will not be available in the studio obviously.</p></li>
</ul>
<p>Hopefully these answers will comfort you that we won't let you down of course.</p>
<hr/>

<h2>What does this mean for you?</h2>
<p>Now let us explain in detail what this means for each of our platform users. You can use the form at the top of this article to quickly find out what it means for you, or read along for the full story.</p>

<h3>For WordPress users</h3>
<p>The biggest change in WordPress is the introduction of our new <a href="{{ page.base }}wordpress/pricing/">Pro plan</a>. This includes more pro features than before and we will add more pro features along the way. This has the following effect on our WordPressers:</p>
<ul>
  <li>
    <h4>👉 For <u>free WordPress users</u></h4>
    <p>Nothing changes, no action required. All your forms keep working without any limitations. You can keep using all your current free features, including calculators and email notifications (although these are now part of the new pro plan). The current premium features (Slack notifications, webhook and Tripetto unbranding) stay disabled for you.</p>
    <p>To get access to all pro features, you can <a href="{{ page.base }}wordpress/pricing/">purchase a new Pro license</a>.</p>
  </li>
  <li>
    <h4>👉 For <u>premium WordPress users</u></h4>
    <p>Nothing changes, no action required. All your forms keep working without any limitations. You keep your access to all pro features; now and in the future.</p>
    <p>Your current premium subscription stays active. There is no price change for your subscription. With that subscription you retain access to all current and future pro features.</p>
  </li>
  <li>
    <h4>👉 For <u>new WordPress users</u></h4>
    <p>You can <a href="{{ page.base }}wordpress/pricing/">purchase a new Pro license</a> to use all pro features without any risk. We gladly offer a 14 days money back guarantee. No questions asked.</p>
  </li>
</ul>
<p>Our main website <a href="{{ page.base }}wordpress/">tripetto.com</a> is totally focused on the WordPress plugin from now on, so you quickly have access to <a href="{{ page.base }}wordpress/help/">plugin help</a> and <a href="{{ page.base }}wordpress/pricing/">plugin pricing</a>.</p>

<h3>For studio (tripetto.app) users</h3>
<p>For our studio users nothing changes, so no action is required. All your forms keep working without any limitations. You can keep using all features in the studio.</p>
<ul>
  <li>
    <h4>👉 For <u>unbranded forms</u></h4>
    <p>Nothing changes, no action required. If you purchased a Tripetto unbranding, that will stay active for the designated form.</p>
  </li>
  <li>
    <h4>👉 For <u>newly unbranded forms</u></h4>
    <p>You can purchase a new Tripetto unbranding for a designated form from within the studio. That's still a <a href="{{ page.base }}studio/pricing/">one-time fee per form</a>.</p>
  </li>
</ul>
<p>The only difference is that our main website is now aimed for WordPress marketing only. Of course we did not forget our studio users, so from now on you can simply use <a href="{{ page.base }}studio/">tripetto.com/studio</a> for everything around the studio:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}studio/">Studio Launch</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}studio/pricing/">Studio Pricing</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}studio/help/">Studio Help Center</a></li>
</ul>

<blockquote>
  <h4>🚧 Warning: Update bookmarks</h4>
  <p>Please update your bookmarks if you use our website to launch the studio. From now on you can simply launch the studio via <a href="{{ page.base }}studio/">tripetto.com/studio</a>.</p>
</blockquote>

<h3>For SDK users</h3>
<p>For our SDK users nothing changes, so no action is required. The SDK components and SDK licenses stay the same. Updates we do on the core of Tripetto will also become available in the latest versions of our SDK components.</p>
<p>All information about the SDK is still available via <a href="{{ page.base }}sdk/" target="_blank">tripetto.com/sdk</a>.</p>
<hr/>

<h2>Full speed ahead! 🚀</h2>
<p>We hope this change of focus enables us to grow Tripetto faster, so we can keep investing in making Tripetto even better for many years to come. Hopefully you will sail along with us to achieve some great things together!</p>
<p>If you have any questions regarding the above, of course let us know via our <a href="{{ page.base }}support/">support form</a>. We're happy to answer any questions you might have.</p>
{% include tripetto-form.html token='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoibFV6Q2ROWXVWYnEvcmZSRkxObERPQ25JcGtEV0VLbkRLUWlRUVNZSzRSMD0iLCJkZWZpbml0aW9uIjoiUWMxckwvbGpUNTgwMW9DVndpd2MwV1JJSXlNblFEZTdDODhZdklRTkRPST0iLCJ0eXBlIjoiY29sbGVjdCJ9.qqMgvTEyBo1l0xRQUZyAOvWbTYIGJiNHO_-HEX2595M' %}
