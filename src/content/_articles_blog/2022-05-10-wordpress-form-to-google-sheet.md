---
layout: blog-article
base: ../../
permalink: /blog/wordpress-form-to-google-sheet/
title: Connecting WordPress form to Google Sheet (2022) - Tripetto Blog
description: This post will show how you can connect your WordPress forms to a Google Sheet.
article_title: Connecting your WordPress form to a Google Sheet in 2022 - Step by Step Guide
article_slug: WordPress form to Google Sheet
article_folder: 20220510
article_image: cover.webp
article_image_width: 1640
article_image_height: 1081
article_image_caption: Connecting your WordPress form to a Google Sheet in 2022 - Step by Step Guide
author: jurgen
time: 9
category: guide
category_name: Guide
tags: [guide]
areas: [wordpress]
year: 2022
---
<p>Forms are ideal to easily collect meaningful data, but storing that data so you can use it for analysis is a whole different game. This post will show how you can connect your WordPress forms to a Google Sheet.</p>

<p>Forms are a great way to collect data from your website visitors. They can be used for contact forms, customer feedback, lead generation, sales, support. Basically <a href="https://tripetto.com/blog/most-common-types-forms/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-form-to-google-sheet">any type of interaction</a> you want with your customers. Forms can be used to collect data in a simple way, or to create conversational experiences that help you understand your customers better.</p>
<p>But storing the data collected in a meaningful way for analysis is a whole different game altogether. Quite often, you’ll receive your data via email only, which makes analyzing your data almost impossible. And even if your data is <a href="https://tripetto.com/blog/wordpress-forms-to-database/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-form-to-google-sheet">stored in a database</a>, it’s still hard to structure that in a way that you can analyze it.</p>
<p>For those reasons, you might want to use a different platform or other services like Google Sheets to store your data and be able to analyze it. In this article, we’ll explore how to connect your WordPress form with Google Sheets.</p>
<hr/>

<h2>Why should I connect my WordPress form with Google Sheets?</h2>
<p>Let’s say you have the best performing form on your website that brings you tons of data about the thoughts of your customers about your products and services. The fact that your form performs so well is great of course! But that’s worth nothing if you don’t have the tools to properly analyze that collected data and transform it into valuable insights that you can act upon.</p>
<p>Luckily there are services that can help you to get a step closer to those insights! We all know Microsoft Excel and its powers to store large amounts of data in a structured way. <a href="https://www.google.com/sheets/about/" target="_blank" rel="noopener noreferrer">Google Sheets</a> is a powerful alternative to Microsoft Excel which has also become more popular with the years.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/google-sheets.png" width="1128" height="730" alt="Google Sheets as alternative for Microsoft Excel." loading="lazy" />
  <figcaption>Google Sheets as alternative for Microsoft Excel. <a href="https://www.google.com/sheets/about/" target="_blank" rel="noopener noreferrer">[Source]</a></figcaption>
</figure>
<p>A few great benefits of Google Sheets are:</p>
<ul>
  <li>Google Sheets offers largely the same functionality as Microsoft Excel.</li>
  <li>It is located in your Google cloud, so you always have access to all your spreadsheet files from any location and any device.</li>
  <li>It is free to use and included in <a href="https://workspace.google.com/" target="_blank" rel="noopener noreferrer">Google Workspaces</a> for business teams.</li>
</ul>

<h3>Benefits of connecting Google sheets with WordPress form to manage form data</h3>
<p>Now that we know that Google Sheets is a great Microsoft Excel alternative, let’s dive into the benefits of connecting your forms to Google Sheets:</p>
<ul>
  <li><strong>You can get more out of your data</strong> - by storing your data in a spreadsheet, you can analyze and manipulate it using formulas and functions. You can even automatically generate graphs from your data. This means you can get more insights from your data, which can help you make better decisions.</li>
  <li><strong>You can share your data with others</strong> - if you want to collaborate with someone else on analyzing or using the data, being able to store it in a spreadsheet makes this much easier. Especially when that spreadsheet is in a cloud environment like with Google Sheets.</li>
  <li><strong>You can use your data for other purposes</strong> - if you’re collecting data for one purpose, you might be able to use it for other purposes as well. For example, if you’re collecting information about people’s preferences for a marketing campaign, that same information could be used for customer support or product development by other teams by simply duplicating the sheet.</li>
  <li><strong>You save time and improve accuracy</strong> - It reduces the chances of human error in manual input and storage of data, and automation of data being stored in Google Sheets will save you time!</li>
</ul>
<hr/>

<h2>How to connect WordPress form to Google Sheets?</h2>
<p>Now that we know the benefits of storing your form data in Google Sheets, we can have a look at how you can achieve that for forms in your WordPress website. <a href="https://wordpress.org/" target="_blank" rel="noopener noreferrer">WordPress</a> is the leading open-source CMS. You can extend and customize it just for your needs by installing plugins that you need for your purpose.</p>
<p>To add forms to your WordPress site, you’ll have to install a form builder plugin. There are lots of possible form plugins to choose from. Well-known form plugins are <a href="https://tripetto.com/blog/wordpress-questionnaire-plugin/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-form-to-google-sheet">WPForms, Gravity Forms and Formidable Forms</a>. For our demonstration of connecting form data to Google Sheets, we will be using <a href="https://tripetto.com/wordpress/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-form-to-google-sheet">Tripetto</a>.</p>

<h3>Introducing Tripetto WordPress plugin</h3>
<p>Tripetto is a form plugin that focusses on conversational forms, which is great for your form completion rate. It’s a tool that helps you create smart, conversational forms and surveys using conditional logic and powerful visual design.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-form.jpg" width="2220" height="1435" alt="Example of a Tripetto form." loading="lazy" />
  <figcaption>Example of a Tripetto form.</figcaption>
</figure>
<p>The core functionalities of Tripetto include:</p>
<ul>
  <li><a href="https://tripetto.com/help/articles/how-to-build-your-forms-in-the-form-builder/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-form-to-google-sheet">Drag-and-drop form builder</a> that feels like you’re making a flowchart to visualize the different flows that your form offers.</li>
  <li>Advanced conditional logic options, including <a href="https://tripetto.com/help/articles/discover-the-power-of-branches-for-your-logic/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-form-to-google-sheet">branch logic</a> to only ask the right questions based on the given answers. It also lets you <a href="https://tripetto.com/help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-form-to-google-sheet">recall given answers</a>, for example to make it feel more personal to your respondents.</li>
  <li>An advanced <a href="https://tripetto.com/calculator/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-form-to-google-sheet">calculator</a> to perform all kinds of calculations with the entered data.</li>
  <li>It enables you to display every form in 3 different <a href="https://tripetto.com/form-layouts/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-form-to-google-sheet">form layouts</a>:
    <ul>
      <li>Autoscroll layout, which displays one question at a time and automatically scrolls through the form.</li>
      <li>Chat layout, which presents questions and answers in a chat format, including chat bubbles and avatars.</li>
      <li>Classic layout, for a more traditional format to present multiple questions at a time.</li>
    </ul>
  </li>
</ul>

<h3>Connect Tripetto form to Google Sheets</h3>
<p>Another feature of Tripetto that comes in handy is the <a href="https://tripetto.com/help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-form-to-google-sheet">webhook connection</a>. Webhooks let you connect certain data from one service to another. Google Sheets is one of those services, so in this case we’re going to use a webhook to connect the data that we collect in Tripetto forms to a Google Sheet. Each new form response will then automatically result in a new row in your Google Sheets. From there on you can then do all the analysis that you want with your collected data.</p>
<p>Let’s take you along the steps to set this up!</p>

<h4>Step 1. Prepare a Google Sheet</h4>
<p>First, make sure you have a Google Sheets file that can receive the form data. You can also already prepare a worksheet by adding the column names of the data that you’re going to collect from the WordPress form.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/prepare-google-sheet.png" width="1200" height="760" alt="Screenshot of Google Sheets." loading="lazy" />
  <figcaption>A Google Sheet with headers for the data columns.</figcaption>
</figure>

<h4>Step 2. Setup an automation tool</h4>
<p>To be able to connect Tripetto data to Google Sheets, you need an automation tool that connects those two services to each other. There are many automation tools available, for example <a href="https://zapier.com/" target="_blank" rel="noopener noreferrer">Zapier</a> and <a href="https://www.make.com/en?utm_source=tripetto-app&utm_medium=partner&utm_campaign=tripetto-app-partner-program" target="_blank" rel="noopener noreferrer">Make</a>. For this demo we’re going to use Make and create a scenario in there.</p>
<p>In that scenario you first add the Tripetto app, which results in a webhook URL. You need that webhook URL in Tripetto in the next step, so copy that to your clipboard.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/make-tripetto.png" width="1200" height="760" alt="Screenshot of Tripetto in Make." loading="lazy" />
  <figcaption>The Tripetto app in a Make scenario.</figcaption>
</figure>
<p>Before you switch back to the Tripetto WordPress plugin, first open up the scenario to receive test data. This makes the next step a little easier.</p>

<h4>Step 3. Configure Tripetto webhook connection</h4>
<p>Now, switch to your form in the form builder in the Tripetto plugin and click Automate > Connections. Enable the Make feature over there and paste the webhook URL that you got from Make.</p>
<p>Next, you can send some dummy data to the webhook, so Make will receive the structure of your form data.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-connections.png" width="1200" height="760" alt="Screenshot of the webhook connections in Tripetto." loading="lazy" />
  <figcaption>The Connections screen in Tripetto.</figcaption>
</figure>

<h4>Step 4. Connect your data to Google Sheets</h4>
<p>Switch back to your automation tool, in this case Make. You’ll see Make received the dummy data from Tripetto with the form structure included.</p>
<p>From here on you can connect that data to thousands of online services. For now, we’re going to connect with Google Forms. To do so, add that service to your Make scenario.</p>
<p>Login with your Google account and you’ll see you can select the Google Sheets file that you created before. You’ll then see that Make recognizes the header names that you entered in your Google Sheet. Now you can link the data from the Tripetto form to the corresponding columns in your Google Sheet file.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/make-google-sheets.png" width="1200" height="760" alt="Screenshot of Google Sheets in Make." loading="lazy" />
  <figcaption>The Google Sheets app in a Make scenario.</figcaption>
</figure>

<h4>Step 5. Test and activate automation</h4>
<p>After you have connected all the desired form data to the right columns in Google Sheets, you can test the whole automation scenario. Activate the scenario and fill out a real response to your Tripetto form in your WordPress site.</p>
<p>Immediately after you’ve submitted the form, you’ll see the scenario starting to work. And as a result of that,  all the data that you just entered in the form is now automatically added as a new row to your Google Sheet.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/google-sheets-results.png" width="1200" height="760" alt="The updated Google Sheets file with new rows." loading="lazy" />
  <figcaption>The updated Google Sheets file with new rows.</figcaption>
</figure>
<p>If everything is tested and works as you want, don’t forget to activate the scenario, so you don’t have to worry about this automation anymore. Each new form response that you receive from now on will be added to your Google Sheets file. Just like magic!</p>

<h3>Connect Tripetto to other online services</h3>
<p>The benefit of the webhook connection of Tripetto is that you can use it for almost any online service that you want to connect your Tripetto form data to. Think of your CRM tool, mailing list tool, database tool, planning tool, etc.</p>
<p>Our Help Center helps you with more <a href="https://tripetto.com/help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-form-to-google-sheet">step by step guides about webhooks</a>. It even includes an in-depth tutorial about <a href="https://tripetto.com/help/articles/how-to-automatically-store-form-responses-in-google-sheets/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-form-to-google-sheet">connecting to Google Sheets</a>.</p>
<p>Also remember that you can choose your own automation tool in between Tripetto and your connected services. In this article we used Make, but you can also use Zapier, Pabbly Connect or any other automation tool that supports webhooks.</p>
<hr/>

<h2>Conclusion: Get started with Google Sheets using Tripetto today</h2>
<p>WordPress forms, like contact forms, customer feedback and support forms, are an ideal way to easily collect valuable data from your users and customers. But if that data is not stored in a convenient way, you miss valuable insights that can help you to improve your products and processes.</p>
<p>In this article we have seen that using a good WordPress form plugin like <a href="https://tripetto.com/wordpress/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-form-to-google-sheet">Tripetto</a> helps you to collect the data in a nice way. Tripetto lets you build and distribute smart and conversational forms, right from your WordPress site. It includes advanced conditional logic features to only ask the right questions and make your forms feel like a personal conversation.</p>
<p>With Tripetto’s built in webhook automations you can then connect your form data to a spreadsheet service like Google Sheets. Once your data is stored there, it’s only a small step to get those valuable insights and analysis.</p>
<p>What’s more, you can have all the functionality of the Tripetto WordPress plugin, <a href="https://tripetto.com/wordpress/pricing/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-form-to-google-sheet">including unlimited webhooks starting from only $99 per year</a>. You also get a 14-day money-back guarantee – no questions asked!</p>
<div>
  <a href="https://tripetto.com/wordpress/pricing/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=wordpress-form-to-google-sheet" class="blocklink">
    <div>
      <span class="title">Get Tripetto Pro for WordPress</span>
      <span class="description">Tripetto Pro is for the Tripetto WordPress plugin and comes in 3 all-inclusive variants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" loading="lazy" />
    </div>
  </a>
</div>
