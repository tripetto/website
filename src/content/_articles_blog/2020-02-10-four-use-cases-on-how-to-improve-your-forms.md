---
layout: blog-article
base: ../../
permalink: /blog/four-use-cases-on-how-to-improve-your-forms/
title: 4 Use cases on how to improve your forms - Tripetto Blog
description: Four examples of everyday use cases where Tripetto can help you to improve your forms and reach your goals.
article_title: 4 Use cases on how to improve your forms
article_slug: Use cases to improve forms
article_folder: 20200210
author: jurgen
time: 3
category: guide
category_name: Guide
tags: [guide, showcase]
areas: [studio,wordpress]
year: 2020
---
<p>Four examples of everyday use cases where Tripetto can help you to improve your forms and reach your goals.</p>

<h2>Transform boring forms</h2>
<p>Forms are everywhere these days. Probably it's not your favorite hobby to fill out all these different forms (at least it's not on top of my hobbies list 😅).</p>
<p>That's why it's important to make filling out forms as easy as possible. And maybe even make it fun. Only that way respondents will be most likely to fill out and complete forms.</p>

<h2>Use cases on form improvements</h2>
<p>We've made you some examples of how boring forms can be transformed into easy-to-use, smart and even fun forms, resulting in <a href="{{ page.base }}logic-types/">higher completion rates</a>.</p>

<h3>1 - Product Evaluation</h3>
<p><strong>Goal</strong> - Only ask the right questions.</p>
<p><strong>Problem</strong> - The problem with traditional product evaluations is they often ask you questions that aren't meant for the specific product(s) you use, so you end up filling out a bunch of questions about products you've never used before.</p>
<p><strong>Improvement</strong> - First, ask which product(s) your respondents have used and based on that answer only ask for their evaluation of the <a href="{{ page.base }}help/articles/how-to-repeat-follow-up-for-multiple-selected-options/">selected product(s)</a>. This will result in a much shorter form that only asks questions that the respondent can answer pretty easily.</p>
<figure>
  <a href="https://tripetto.app/run/DD41EIVG0V" target="_blank" rel="noopener noreferrer"><img src="{{ page.base }}images/blog/{{ page.article_folder }}/product.png" width="1020" height="250" alt="Screenshot of a Tripetto form" loading="lazy" /></a>
  <figcaption><a href="https://tripetto.app/run/DD41EIVG0V" target="_blank" rel="noopener noreferrer">Run this example form</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="https://tripetto.app/template/OAEX31GMXP" target="_blank" rel="noopener noreferrer">View this template in the builder</a></figcaption>
</figure>

<h3>2 - Job Application</h3>
<p><strong>Goal</strong> - Make it a conversation.</p>
<p><strong>Problem</strong> - Normally a job application form lists all questions in a long list. This can put off your respondents, because they are confronted with a long and unclear form.</p>
<p><strong>Improvement</strong> - By asking each question one by one, you can make it <a href="{{ page.base }}blog/why-conversational-forms-still-matter/">feel like a conversation</a>, because the focus is on each individual question. This will result in a more friendly interaction, which will trigger respondents to continue, and possibly be more open and elaborate.</p>
<figure>
  <a href="https://tripetto.app/run/BVI9A6I0IP" target="_blank" rel="noopener noreferrer"><img src="{{ page.base }}images/blog/{{ page.article_folder }}/job.png" width="1020" height="250" alt="Screenshot of a Tripetto form" loading="lazy" /></a>
  <figcaption><a href="https://tripetto.app/run/BVI9A6I0IP" target="_blank" rel="noopener noreferrer">Run this example form</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="https://tripetto.app/template/3TREY2P0DV" target="_blank" rel="noopener noreferrer">View this template in the builder</a></figcaption>
</figure>

<h3>3 - Customer Satisfaction</h3>
<p><strong>Goal</strong> - Make it human.</p>
<p><strong>Problem</strong> - Traditional customer satisfaction forms really give you the feeling you're talking to a computer. Just a long list of boring questions that make it difficult to keep your attention.</p>
<p><strong>Improvement</strong> - Using some multimedia inside the form can lighten things up, making it feel like you're <a href="{{ page.base }}blog/five-simple-tips-to-improve-your-online-forms-and-surveys/">talking to a human</a>. For example, a simple gif image representing the feeling of your rating can trigger your respondents to continue.</p>
<figure>
  <a href="https://tripetto.app/run/XQ29WT0EQN" target="_blank" rel="noopener noreferrer"><img src="{{ page.base }}images/blog/{{ page.article_folder }}/customer.png" width="1020" height="250" alt="Screenshot of a Tripetto form" loading="lazy" /></a>
  <figcaption><a href="https://tripetto.app/run/XQ29WT0EQN" target="_blank" rel="noopener noreferrer">Run this example form</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="https://tripetto.app/template/AUC3ILZBJN" target="_blank" rel="noopener noreferrer">View this template in the builder</a></figcaption>
</figure>

<h3>4 - Wedding RSVP</h3>
<p><strong>Goal</strong> - Make it lively and colorful.</p>
<p><strong>Problem</strong> - A form for a wedding RSVP should give you a festive feeling, but that's not quite the feeling you get with traditional messy forms.</p>
<p><strong>Improvement</strong> - By styling forms with colors, fonts (including emojis) and images you can create a positive feeling, making it more fun to complete the form.</p>
<figure>
  <a href="https://tripetto.app/run/IO2S7QPQ7Q" target="_blank" rel="noopener noreferrer"><img src="{{ page.base }}images/blog/{{ page.article_folder }}/wedding.png" width="1020" height="250" alt="Screenshot of a Tripetto form" loading="lazy" /></a>
  <figcaption><a href="https://tripetto.app/run/IO2S7QPQ7Q" target="_blank" rel="noopener noreferrer">Run this example form</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="https://tripetto.app/template/HPGO5NLZEB" target="_blank" rel="noopener noreferrer">View this template in the builder</a></figcaption>
</figure>

<h2>Conclusion</h2>
<p>With the right tools it's pretty easy to embrace these improvements in your own forms. You can read some more tips on how to achieve this all in our other blog articles, for example:
</p>
<ul>
  <li><a href="{{ page.base }}blog/why-our-visual-form-builder-works-like-this/">Why our visual form builder works like this</a>;</li>
  <li><a href="{{ page.base }}blog/five-simple-tips-to-improve-your-online-forms-and-surveys/">5 Simple tips to improve your online forms and surveys</a>;</li>
  <li><a href="{{ page.base }}blog/why-conversational-forms-still-matter/">Why Conversational Forms still matter</a>;</li>
  <li><a href="{{ page.base }}blog/forms-dont-need-to-suck/">Forms don’t need to suck</a>.</li>
</ul>
