---
layout: blog-article
base: ../../
permalink: /blog/calculated-fields-form/
title: Calculated Fields in a Form - Tripetto Blog
description: This post will show you how to create a calculated fields form within WordPress. You can give your forms complexity under the hood, and boost its efficiency!
article_title: How to Set Up Calculated Fields in a Form (Best Plugin Alternative)
article_slug: Calculated fields form
article_folder: 20220907
article_image: cover.webp
article_image_width: 1640
article_image_height: 1081
article_image_caption: How to Set Up Calculated Fields in a Form (Best Plugin Alternative)
author: jurgen
time: 12
category: guide
category_name: Guide
tags: [guide]
areas: [wordpress]
year: 2022
---
<p>This post will show you how to create a calculated fields form within WordPress. You can give your forms complexity under the hood, and boost its efficiency!</p>

<p>Dynamic online forms often have a much better conversion rate than passive ones. As such, you’ll want to employ all of the tricks and techniques to increase the engagement of those forms. Creating a calculated fields form can be one way to do this.</p>
<p>For example, if you want to provide estimates to your customers, calculated fields will be the approach you’ll want to take. Calculations can be flexible too. You can carry out simple or advanced calculations, and bring in other advanced form functionality, such as conditional logic.</p>
<p>For this tutorial, we’re going to show you how to set up a calculated fields form in WordPress, using a popular form builder plugin. First, let’s further discuss what calculated fields are, why they’re useful, and mention some plugins to help.</p>
<hr/>

<h2>What Calculated Fields Are (And Why You’ll Find Them Useful)</h2>
<p>A calculated field takes its value from one or more fields within your form. If you are a <a href="https://tripetto.com/help/articles/how-to-use-your-csv-file-in-office-excel-or-google-sheets/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form">spreadsheet user</a>, through <a href="https://www.microsoft.com/en-us/microsoft-365/excel" target="_blank" rel="noopener noreferrer">Microsoft Excel</a>, <a href="https://tripetto.com/help/articles/how-to-automatically-store-form-responses-in-google-sheets/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form">Google Sheets</a>, or another solution, you’ll likely understand how to use formulas. Calculated fields work in the same way: as mathematical equations.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/scores.png" width="1000" height="611" alt="An example of a maths equation in a calculated fields form." loading="lazy" />
  <figcaption>An example of a maths equation in a calculated fields form.</figcaption>
</figure>
<p>You can use calculated fields to find out totals, calculate averages, tally up scores, and much more. In real-world cases, you’ll often use a calculated field to create shipping prices or provide cost estimates for your potential customers.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/shipping-costs.png" width="1000" height="719" alt="Shipping costs displayed on a form." loading="lazy" />
  <figcaption>Shipping costs displayed on a form.</figcaption>
</figure>
<p>A calculated fields form can offer immense value to your site and business. First, you can build extra functionality to your form without the need for technical knowledge or coding skills. You can also save time, as the form data you collect has no need to go into another spreadsheet or database.</p>
<hr/>

<h2>The Types of Websites that Benefit From a Calculated Fields Form</h2>
<p>You’ll find myriad different use cases for a calculated fields form. While any type of site that needs to use calculations to provide information to customers can benefit, there are some specific examples to note:</p>
<ul>
  <li>eCommerce websites will have to calculate a number of different aspects to come to a grand total. For example, shipping costs can be complex. Product pages have lots of values to calculate too: quantities, prices, coupon code discounts, bulk discounts, and much more.</li>
  <li>Tradespeople often calculate prices based on the cost of materials, labor, the dimensions and scope of the work, and more. A site with a calculated fields form can help them provide a quote fast.</li>
  <li>An accountant or finance worker might want to provide on-site tax calculators, which will have a calculated fields form at its core.</li>
  <li>Even seemingly simple applications, such as <a href="https://tripetto.com/blog/wordpress-assessment-plugin/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form">online tests</a>, quizzes, aptitude tests, and job applications can leverage calculated fields. This is a clear use case as the user input will need calculating to come to a final total.</li>
  <li>Physical printing websites (such as merchandise companies) will also work with dimensions. They might also use elements such as paper or garment sizes. All of these values would work well within a calculated fields form.</li>
  <li>Any logistics service, such as taxis, removals, delivery companies, and more will also use calculated fields. Time, distance, frequency, and transportation methods will all use calculated values that have to combine into a total.</li>
  <li>Companies that handle bookings, such as hotels or rental companies, can use calculated field forms to calculate costs based on the length of your stay or rental period.</li>
</ul>
<p>If you run a <a href="https://wordpress.org/" target="_blank" rel="noopener noreferrer">WordPress</a> or <a href="https://woocommerce.com/" target="_blank" rel="noopener noreferrer">WooCommerce</a> website that requires calculated fields forms, you’ll want to choose the right plugin for your needs. Most people will turn to one plugin in particular, but it might not be the best option available. We’ll talk about this option, and an alternative, next.</p>
<hr/>

<h2>Why the Calculated Fields Form Plugin Might Not Be an Ideal Option for Your Needs</h2>
<p>A WordPress website will use plugins to boost and expand its functionality. When it comes to combining values, many site owners will turn to the <a href="https://wordpress.org/plugins/calculated-fields-form/" target="_blank" rel="noopener noreferrer">Calculated Fields Form</a> plugin. Its name will tell you exactly what it does, which means users gravitate to it almost without another thought:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/calculated-fields-form-plugin.png" width="1000" height="323" alt="The Calculated Fields Form plugin plugin." loading="lazy" />
  <figcaption>The Calculated Fields Form plugin plugin.</figcaption>
</figure>
<p>It’s a form builder that can carry out calculations, but has other benefits too:</p>
<ul>
  <li>You can use the plugin with almost any page builder on the market, including the Block Editor, <a href="https://elementor.com/" target="_blank" rel="noopener noreferrer">Elementor</a>, and more.</li>
  <li>The plugin uses a visual form builder to help you piece together your calculated fields form.</li>
  <li>You have a number of field types to help you create different types of forms, such as contact forms.</li>
  <li>There’s the option to send out email notifications on an automatic basis.</li>
</ul>
<p>Given its feature set and user feedback, Calculated Fields Form is a solid option for the task. However, it does have some drawbacks. First, the builder is visual – <a href="https://tripetto.com/help/articles/how-to-build-your-forms-in-the-form-builder/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form">not drag-and-drop</a> – which makes it less intuitive to put your form together.</p>
<p>Also, the plugin has limitations inherent in its design. The core functionality looks to achieve field calculations, but its scope for other types of forms is less impressive. You’ll likely not want to use multiple plugins for different types of form on your site. As such, a flexible and versatile form builder plugin should be on your shortlist.</p>
<hr/>

<h2>How Tripetto Can Help You Leverage Calculated Fields For Your WordPress Site</h2>
<p>When it comes to comprehensive features and functionality, top-notch support, and powerful flexibility, <a href="https://tripetto.com/wordpress/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form">Tripetto</a> is a number one form builder plugin. It lets you create <a href="https://tripetto.com/blog/smart-form/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form">smart, conversational forms</a> in a flash, and distribute them in a number of ways.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-plugin.png" width="1544" height="500" alt="The Tripetto plugin." loading="lazy" />
  <figcaption>The Tripetto plugin.</figcaption>
</figure>
<p>You have a wide range of form types available within Tripetto, and you’ll use a drag-and-drop builder to create dynamic and engaging forms. Here’s what makes Tripetto so special:</p>
<ul>
  <li>You won’t need any HTML, CSS, PHP, or JavaScript knowledge to use Tripetto. The full feature set of the plugin is accessible without any required technical knowledge. In contrast, Calculated Fields Form needs CSS to help you style forms.</li>
  <li>You’re able to use Tripetto’s <a href="https://tripetto.com/form-layouts/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form">unique ‘form faces’</a> to create a unique look and feel for your form, fast. For example, you can switch between a classic layout, an autoscrolling form, and a chat form face with a few button presses. If you decide you would prefer a different form face after publishing your form, you can switch between different options – this is a feature other form builder plugins don’t have.</li>
  <li>Tripetto uses <a href="https://tripetto.com/blog/elementor-form-conditional-logic/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form">conditional logic</a> to help you build conversational, smart forms. You have the ability to program in a near-unlimited number of logic jumps per form, which can expand its complexity.</li>
  <li>You can connect Tripetto to thousands of external services, depending on your business needs. Linking your form data to your preferred customer relationship management (CRM) system (e.g. Salesforce), Google Sheets, or Google Analytics are common applications.</li>
</ul>
<p>Tripetto works with just as many page builder plugins as Calculated Fields Form, too. In fact, there’s a <a href="https://tripetto.com/help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-elementor-widget/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form">dedicated Elementor widget</a> for that page builder, but you can use <a href="https://tripetto.com/help/articles/how-to-embed-your-form-in-your-wordpress-site/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form">shortcodes and other widgets</a> regardless of your choice of builder.</p>
<p>While the free version of Calculated Fields Form has a lot to offer, there is a paid upgrade that locks some essential functionality behind a paywall. For example, you can add in <a href="https://tripetto.com/blog/contact-form-spam-wordpress/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form">WordPress security measures</a>, such as CAPTCHA, set up notifications, store the data you collect on your site, and more.</p>
<p>If you want to spend more money on the plugin, you can add in <a href="https://tripetto.com/help/articles/how-to-use-the-date-and-time-block/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form">date and time</a> operations, and enjoy <a href="https://tripetto.com/help/articles/how-to-connect-to-other-services-with-make/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form">advanced connections</a> to other data sources. However, Tripetto does things in a different way, and even lets you use multiple (interacting) calculators in a single form.</p>
<p>Regardless of the pricing tier you choose, you’ll have the full feature set of the plugin at your disposal, including all calculator functionality. This means you’ll only pay more if you require a greater number of licenses; there are no hidden add-ons to unlock Tripetto’s full feature set. A <a href="https://tripetto.com/wordpress/pricing/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form">single-site license</a> starts from $99 per year, and also comes with a 14-day money-back guarantee.</p>
<hr/>

<h2>How to Create a Calculated Fields Form in WordPress Using Tripetto</h2>
<p>Over the rest of the article, we’re going to show you how to create a calculated fields form using Tripetto. The first step is to download, install, activate, and set up the plugin.</p>

<h3>1. Install and Set Up Tripetto Within WordPress</h3>
<p>You’ll install Tripetto in much the same way as you would any other WordPress plugin. If this is a new task for you, we have a <a href="https://tripetto.com/help/articles/how-to-install-the-wordpress-plugin/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form">complete guide</a> over in our <a href="https://tripetto.com/help/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form">Help Center</a> (along with a lot of other information on how to use Tripetto.)</p>
<p>Once you install and activate the plugin on your WordPress site, you’ll then want to run through the Onboarding Wizard. For your first time, you can do this through the Tripetto link on your WordPress dashboard:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/onboarding.png" width="1000" height="622" alt="The Tripetto Onboarding Wizard." loading="lazy" />
  <figcaption>The Tripetto Onboarding Wizard.</figcaption>
</figure>
<p>This will take you through some essential WordPress settings for the plugin, and won’t take more than a few minutes. There’s even a tutorial to understand how Tripetto works in practice.</p>
<p>Once you do this, you’ll want to begin to create a new form, then develop it into a calculated fields form. The rest of the post will look at this in detail.</p>

<h3>2. Create a New Form and Add a Calculator Block</h3>
<p>To create a new form, head to the Tripetto dashboard and click the Build Form button:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/dashboard.png" width="1000" height="438" alt="Tripetto’s Build Form button on the dashboard." loading="lazy" />
  <figcaption>Tripetto’s Build Form button on the dashboard.</figcaption>
</figure>
<p>You’ll next need to select either a form face from the options, or choose from the list of form templates in the Tripetto library:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/templates.png" width="1000" height="621" alt="Choosing a form face and template within Tripetto." loading="lazy" />
  <figcaption>Choosing a form face and template within Tripetto.</figcaption>
</figure>
<p>Once you choose a form template, you’ll come to the Tripetto storyboard. It’s here you’ll choose a <a href="https://tripetto.com/help/articles/how-to-use-the-calculator-block/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form">Calculator Block</a>. To do this, add a new Block using the Plus icon within a section, then select the More Items menu and choose the Calculator action within the Item > Type menu:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/add-calculator-block.png" width="1000" height="640" alt="Choosing a calculator type within Tripetto." loading="lazy" />
  <figcaption>Choosing a calculator type within Tripetto.</figcaption>
</figure>
<p>From here, click <a href="https://tripetto.com/blog/calculator-form/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form">into the Calculator Block</a>, and add a description. Note that you can also change a Block type using the Change type drop-down menu. Regardless, you can add an initial value using the Plus icon at the bottom of the Operations box:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/calculator-1.png" width="1000" height="556" alt="The available options for a Calculator Block within Tripetto." loading="lazy" />
  <figcaption>The available options for a Calculator Block within Tripetto.</figcaption>
</figure>
<p>You’ll start with this initial value, which you can fix or use a dynamic value from a previous answer:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/calculator-2.png" width="1000" height="396" alt="Choosing an initial value within Tripetto." loading="lazy" />
  <figcaption>Choosing an initial value within Tripetto.</figcaption>
</figure>
<p>Once you select this, you can <a href="https://tripetto.com/help/articles/how-to-use-operations-in-the-calculator-block/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form">add further operators</a> using the Plus icon. This lets you choose between Add, Subtract, Multiply, Divide, and Equal:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/calculator-3.png" width="1000" height="478" alt="Choosing more operators for a Calculator Block." loading="lazy" />
  <figcaption>Choosing more operators for a Calculator Block.</figcaption>
</figure>
<p>At this point, you’ll likely want to customize your calculated fields form further. Tripetto has plenty of customization options to offer.</p>

<h3>3. Customize Your Calculator Field Settings</h3>
<p>You don’t necessarily have to use one calculator within your form and call it a day. You can chain together a series of calculators (and operations) to create the specific formula you need.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/calculators.png" width="1000" height="578" alt="A quote form showing different calculators within steps of the form." loading="lazy" />
  <figcaption>A quote form showing different calculators within steps of the form.</figcaption>
</figure>
<p>For example, our quote form uses a set value in the initial step. From there, each subsequent step will use a Calculator Block to piece together the <a href="https://tripetto.com/help/articles/how-to-update-values-throughout-your-form/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form">different values</a> into a grand total at the end of the form.</p>
<p>You can also customize the various calculator options too. For instance, you can alter the number format, limit minimum and maximum values, add a prefix, and more. You can find these within the left-hand sidebar for any Block:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/calculator-features.png" width="1000" height="704" alt="The different options for numbers within a Tripetto Calculator Block." loading="lazy" />
  <figcaption>The different options for numbers within a Tripetto Calculator Block.</figcaption>
</figure>
<p>However, you don’t need to show every calculated field output to the end user. Our quote form hides the calculations by default, and uses them as input for other operations down the chain.</p>

<h3>4. Test and Publish Your Calculated Fields Form</h3>
<p>Once you have added all of your required fields, the final steps are to test and publish your form. Testing is a vital step because you’ll want to publish the form without bugs or other errors. The good news is that you can do this from within Tripetto. In fact, it’s something you’ll use during the creation process too.</p>
<p>The right-hand <a href="https://tripetto.com/help/articles/how-to-let-the-live-preview-work-for-you/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form">preview pane</a> shows how your form will look on the front end:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/preview.png" width="1000" height="758" alt="The Tripetto preview, showing a form on the front end." loading="lazy" />
  <figcaption>The Tripetto preview, showing a form on the front end.</figcaption>
</figure>
<p>You can choose between showing logic as it would work on the front end, or without. This lets you test every aspect of your form without the need to open different browser windows.</p>
<p>Tripetto can also help you produce beautiful forms regardless of the user’s end device. You’ll see different viewports at the top of the screen, which will let you preview a form on desktop, tablet, and mobile devices:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/devices.png" width="1000" height="524" alt="Tripetto’s viewport options within its toolbar." loading="lazy" />
  <figcaption>Tripetto’s viewport options within its toolbar.</figcaption>
</figure>
<p>When you want to publish your form, click the <a href="https://tripetto.com/wordpress/help/sharing-forms-and-surveys/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form">Share link</a> in the top toolbar. This will open a sidebar that gives you a number of ways to publish your form:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/share.png" width="1000" height="838" alt="The Tripetto Share sidebar." loading="lazy" />
  <figcaption>The Tripetto Share sidebar.</figcaption>
</figure>
<p>For example, you can share a direct URL to your form, which is fantastic for social media. The dedicated WordPress shortcode lets you place your form almost anywhere on your site, regardless of the page builder or theme you use:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/shortcode.png" width="1000" height="648" alt="The WordPress shortcode options." loading="lazy" />
  <figcaption>The WordPress shortcode options.</figcaption>
</figure>
<p>You get lots of options (‘parameters’ in WordPress parlance) for your shortcode too. What’s more, you don’t have to code these in – you can use the checkboxes to create your WordPress shortcode.</p>
<p>If you use a page builder plugin such as Elementor, Tripetto also offers a dedicated widget for its editor.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/elementor-widget.png" width="1000" height="474" alt="Adding a Tripetto Form widget to an Elementor page." loading="lazy" />
  <figcaption>Adding a Tripetto Form widget to an Elementor page.</figcaption>
</figure>
<p>You’ll have a whole host of options for your form here too, with the added bonus that you can harness the power of Elementor to integrate your form into your site.</p>
<hr/>

<h2>Conclusion</h2>
<p>Using fields as values to calculate can come in helpful to a multitude of websites. For example, you can calculate quotes and estimates based on user input. This is a vital, ‘self-serving’ usability element. A calculated fields form doesn’t have to require code or technical knowledge either. Because all of the calculations take place inside the form, you’ll save time, money, and resources.</p>
<p>While there are lots of plugins that can help you, <a href="https://tripetto.com/wordpress/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form">Tripetto</a> is the best form builder plugin to help you build calculated fields forms, along with a whole host of other form types. In fact, Tripetto offers a number of benefits regardless of the form you want to create:</p>
<ul>
  <li>You can create beautiful, smart, and conversational forms – from simple forms to complex, multi-page forms – in a snap.</li>
  <li>There are multiple different styles – not to mention form faces – that can help you apply a certain design fast.</li>
  <li>You’re able to use conditional rules and logic within your calculated fields to create smart forms that ‘react’ to the user’s inputs.</li>
  <li>You won’t need any coding knowledge to use Tripetto – the entire feature set is available and accessible.</li>
</ul>
<p>You can <a href="https://tripetto.com/wordpress/pricing/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form">purchase Tripetto</a> starting from $99 per year. In addition, you have a no-risk, 14-day, money-back guarantee – no questions asked.</p>
<div>
  <a href="https://tripetto.com/wordpress/pricing/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=calculated-fields-form" class="blocklink">
    <div>
      <span class="title">Get Tripetto Pro for WordPress</span>
      <span class="description">Tripetto Pro is for the Tripetto WordPress plugin and comes in 3 all-inclusive variants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" />
    </div>
  </a>
</div>
