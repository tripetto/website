---
layout: blog-article
base: ../../
permalink: /blog/contact-form-spam-wordpress/
title: Stop spam in WordPress contact form (2022) - Tripetto Blog
description: Spam coming from your WordPress contact form is annoying and can have serious consequences. Luckily Tripetto offers a built-in solution to prevent spam attacks.
article_title: How to stop spam in your WordPress contact form in 2022 - step by step guide
article_slug: Stop spam in WordPress contact forms
article_folder: 20220607
article_image: cover.webp
article_image_width: 1640
article_image_height: 1081
article_image_caption: How to stop spam in your WordPress contact form in 2022 - step by step guide
article_utm: "?utm_source=Tripetto&utm_medium=content-marketing&utm_content=contact-form-spam-wordpress"
author: jurgen
time: 10
category: guide
category_name: Guide
tags: [guide]
areas: [wordpress]
year: 2022
---
<p>Spam coming from your WordPress contact form is annoying and can have serious consequences. Luckily Tripetto offers a built-in solution to prevent spam attacks.</p>

<p>Imagine waking up to 50 new requests from your <a href="https://tripetto.com/blog/wordpress-customizable-contact-form/{{ page.article_utm }}">contact form</a>. All excited you open them up one by one - only to realize that they’re all from spam bots… Not the best way to start a new day.</p>
<p>Unfortunately contact form spam is very real and the responsible bots don’t discriminate - it can happen to any website! And it can have serious repercussions.</p>
<p>In this article, we’ll explore how contact form spam works, why it happens and the steps you can take to prevent it on your WordPress website.</p>
<hr/>

<h2>Why are contact forms important?</h2>
<p>Contact forms are a core functionality of almost every WordPress site. Although they are primarily meant to let your visitors contact you with questions and requests, they also are an effective way of collecting customer information and feedback from your visitors. They can be used:</p>
<ul>
  <li>To give potential customers a way to <a href="https://tripetto.com/blog/contact-form-generator/{{ page.article_utm }}">contact you with questions or support requests</a> or show interest in your product;</li>
  <li>To understand <a href="https://tripetto.com/blog/wordpress-feedback-plugin/{{ page.article_utm }}">what customers think about your products or services</a> and improve accordingly;</li>
  <li>To gather information about products or services that may be useful for future marketing campaigns;</li>
  <li>To get testimonials from happy customers which can be used in your marketing efforts, like social media posts and email campaigns;</li>
  <li>To allow yourself to be contacted for promotions and collaborations.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-contact-form.png" width="1000" height="344" alt="Screenshot of a simple contact form in Tripetto." loading="lazy" />
  <figcaption>Simple example of a contact form in a WordPress website.</figcaption>
</figure>
<p>The benefits of contact forms are crystal clear and with <a href="https://wordpress.org/" target="_blank" rel="noopener noreferrer">WordPress</a> there are lots of options to add such a form (or <a href="https://tripetto.com/blog/most-common-types-forms/{{ page.article_utm }}">any other form</a>) to your website. A possible consequence of such a form though can be that you receive large amounts of fake responses, the unfortunately well-known ‘spam submissions’. Let’s dive some deeper into what this is exactly and what can be the consequences if you let this happen.</p>
<hr/>

<h2>What is contact form spam?</h2>
<p>Spam can be sent manually by people who want to annoy you or send you unwanted messages. It’s hard to tackle that kind of spam, because it could mean taking measures that can also affect your real users. For now, we’re going to leave that kind of spam out of this; also because it’s probably not the biggest spam enemy you have to face.</p>
<p>What we do want to talk about is spam that is automated by software that sends out thousands of messages at once. Contact form spam is any kind of automated spam that uses contact forms as a way of spreading itself around.</p>

<h3>Why does spam occur?</h3>
<p>Spam is usually sent for two reasons:</p>
<ol>
  <li>Commercial purposes, to sell products or services;</li>
  <li>Malicious intent, to spread viruses or steal data.</li>
</ol>
<p>Spammers will do their best to get your attention and they can do this in several ways:</p>
<ul>
  <li>Spam bots submitting your forms over and over again;</li>
  <li>Spam emails being sent from your form;</li>
  <li>Spam comments on your posts;</li>
  <li>Spam reviews on your products or services.</li>
</ul>
<p>In most case spammers use automated software to scan websites for forms and submit them with random data. One of the reasons they do this is because they want to send as many messages as possible hoping that some will get through and reach real people who might click on their links or buy their products. If enough people click on their links or buy their products, they will keep sending more spammy messages.</p>

<h3>What are the consequences of contact form spam?</h3>
<p>The obvious consequence of spam is that it distracts you from your day-to-day focus on your business, school, training, etc. From bad to worse, these are some possible consequences:</p>
<ul>
  <li>You receive lots of unwanted messages that fill up your inbox and distract you from the emails you really want to receive;</li>
  <li>You lose time having to delete all these unwanted messages manually;</li>
  <li>Your website’s reputation may suffer if spammers use it to spread malicious content;</li>
  <li>Your website may get blacklisted by email providers if too many spammy messages are sent from it.</li>
</ul>
<p>Clearly, contact form spam can be very annoying and time-consuming for site owners, so it’s important to understand how to prevent it.</p>
<hr/>

<h2>How to prevent contact form spam?</h2>
<p>Luckily there are a few different ways to prevent contact form spam:</p>
<ol>
  <li>You can try to <strong>not be distracted by it</strong>;</li>
  <li>You can try to <strong>prevent it with visual anti-spam mechanisms</strong>;</li>
  <li>You can try to <strong>prevent it with invisible anti-spam mechanisms (using Tripetto)</strong>.</li>
</ol>
<p>Let's dive deeper into each of those options, beginning with not being distracted.</p>

<h3>1. Not be distracted by contact form spam</h3>
<p>This first option is not really a solid solution, but we do want to mention it, as it can be a temporary workaround in some cases. In some cases it’s just not possible to take measurements to prevent spam entries at all. In that case you could choose to just let the spam entries happen, but minimize the distraction that you get from it.</p>
<p>You can for example block emails based on certain criteria, such as whether they contain certain keywords or whether they come from certain IP addresses. It depends on your email provider if this is possible and how to do it. <a href="https://google.com/gmail/" target="_blank" rel="noopener noreferrer">Gmail</a> for example offers a <a href="https://support.google.com/a/topic/9981578?hl=en&ref_topic=2683865" target="_blank" rel="noopener noreferrer">spam filter</a> that you can use to filter your incoming messages and make sure that spam messages don't reach your inbox.</p>
<p>This option can primarily work in case you only receive the contact form entries in your mailbox. If you also <a href="https://tripetto.com/blog/wordpress-form-submission/{{ page.article_utm }}">store the entries</a> in a database, or even have configured <a href="https://tripetto.com/blog/wordpress-form-to-google-sheet/{{ page.article_utm }}">automations</a> that automatically follow-up after a form submission, this is not really a proper solution, because you still haven’t tackled it at the core. You just don’t see the spam entries in your inbox anymore. In that case you probably want to dive a little deeper.</p>

<h3>2. Prevent contact form spam with visual anti-spam mechanisms</h3>
<p>Ever since contact form spam really got overwhelming, multiple different ways to prevent such spam entries have been developed. The idea is to let your real users prove that they are indeed real humans and let them do tasks that a robot probably can’t. Well-known techniques for that are:</p>
<ul>
  <li>Solving a simple calculation and entering the outcome;</li>
  <li>Reading a (barely readable) captcha code and entering the combination of letters/numbers;</li>
  <li>Using a <a href="https://www.google.com/recaptcha/about/" target="_blank" rel="noopener noreferrer">reCAPTCHA</a> technique from Google to identify a real person. That can for example work by ticking a <i>“I’m not a robot”</i> checkbox or solving a puzzle in where you have to identify certain visible objects, like <i>“Select all images with a traffic light”</i>.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/recaptcha2.gif" width="616" height="164" alt="Screenshot of a reCAPTCHA v2." loading="lazy" />
  <figcaption>A reCAPTCHA version 2 in action (<a href="https://www.google.com/recaptcha/about/" target="_blank" rel="noopener noreferrer">source</a>).</figcaption>
</figure>
<p>Although they do a good job in preventing spam entries, it also has the risk of <a href="https://designmodo.com/ux-captcha-effect/" target="_blank" rel="noopener noreferrer">impacting your user’s experience</a>. Such visual anti-spam mechanisms are often placed at the end of your form, at the point that your respondents simply want to submit the form as quickly as possible. But then there’s another threshold that your respondent has to take: solving the calculation/captcha/puzzle. Next to a negative experience, this can even result in actual drop-offs from respondents that don’t take the time and effort to solve it and then abandon the form.</p>
<p>Wouldn’t it be nice if there was another solution to prevent large amounts of contact form spam, but without interfering with the user experience? That’s exactly why Tripetto does this a little differently!</p>

<h3>3. Prevent contact form spam with invisible anti-spam mechanisms (using Tripetto)</h3>
<p><a href="https://tripetto.com/{{ page.article_utm }}">Tripetto</a> is an all-in-one form plugin for WordPress websites. It enables you to build fully customizable, conversational forms and surveys. Its drag-and-drop builder helps you to not only add the questions to your form, but also makes <a href="https://tripetto.com/blog/wordpress-forms-with-conditional-logic/{{ page.article_utm }}">conditional logic</a> an integral part of your form design. Because of that your forms become really smart and only ask the right questions, based on the already given answers of each respondent.</p>
<p>On top of that you can display each form in 3 totally different <a href="https://tripetto.com/form-layouts/{{ page.article_utm }}">form layouts</a>:</p>
<ul>
  <li>Autoscroll layout, which displays one question at a time and automatically scrolls through the form;</li>
  <li>Chat layout, which presents questions and answers in a chat format, including chat bubbles and avatars;</li>
  <li>Classic layout, for a more traditional format to present multiple questions at a time.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-autoscroll-form.png" width="2220" height="1435" alt="Screenshot of a Tripetto form in autoscroll form layout." loading="lazy" />
  <figcaption>A Tripetto form in autoscroll form layout.</figcaption>
</figure>
<p>This all helps you to get <a href="https://tripetto.com/logic-types/{{ page.article_utm }}">higher completion rates</a>, because your form is tailored perfectly to your audience and even tailored for every individual respondent. Of course, you don’t want to tear down that experience at the end of your form with an anti-spam mechanism in which the respondent must solve a puzzle. That’s why Tripetto implements an anti-spam solution that’s not visible to the respondent, but does its work in the background.</p>

<h4>How does Tripetto prevent contact form spam?</h4>
<p>Tripetto is <a href="https://tripetto.com/help/articles/how-tripetto-prevents-spam-entries-from-your-forms/{{ page.article_utm }}">built at its core</a> to make it very hard for spammers to use the form. It uses the following principles to take care of that:</p>
<ul>
  <li><strong>Data structure difficulty</strong> - The technical structure of your forms is managed in such a way spam bots will have a very hard time learning the structure of the form. And without understanding the structure, it gets hard for the spam bot to even use that structure to fill out the input fields, preventing it from being able to fill out the form automatically;</li>
  <li><strong>Increasing posting difficulty</strong> - Next to the difficulty of filling out the form, Tripetto also makes it very hard for spam bots to submit Tripetto forms. There is a sophisticated technique behind the submitting process that increases in difficulty each time a spam bot tries to submit a form. This will discourage spammers to repeatedly/automatically keep submitting forms.</li>
</ul>
<p>And the best part is that your real respondents won't notice these built-in hurdles for spam bots at all. It only comes into play when a larger amount of submissions is done from the same IP address, generally typical for a spam attack by spam bots.</p>

<h4>Configuring Tripetto’s spam protection</h4>
<p>By default, all Tripetto forms use the built-in spam protection, so you are protected against spam attacks. On some occasions you might want to alter that spam protection level though, for example if you share your form with a class of students at the same time who are all on the school’s network (and thus using the same IP address). If all those students would submit the form in a short period of time, Tripetto would normally think it is an attack and not all form submissions would be accepted.</p>
<p>That’s why there are two <a href="https://tripetto.com/help/articles/how-to-configure-plugin-settings-for-email-sending-and-spam-protection/{{ page.article_utm }}">anti-spam settings</a> you can change:</p>
<ul>
  <li><strong>Set the spam protection mode</strong> - You can choose from maximal, normal, minimal or fully disable the spam protection.</li>
  <li><strong>Enter an IP allow list</strong> - You can also enter a list of IP addresses that are excluded from the spam protection, regardless of the active spam protection mode. Handy for the mentioned use case where lots of submissions are done from the same IP address, like on a school or an office.</li>
</ul>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-spam-settings.png" width="1200" height="760" alt="Screenshot of anti-spam settings in Tripetto." loading="lazy" />
  <figcaption>Choose the spam protection mode that suits your needs in Tripetto.</figcaption>
</figure>
<hr/>

<h2>Conclusion</h2>
<p>As we have seen spam is a very serious issue if it influences your day-to-day routine. Next to the hassle you get from working around all spam messages, it can also have some serious consequences to your website’s reputation and status.</p>
<p>That’s why it is important to take some measures against such contact form spam. In this article we discussed a few possible solutions. None of those solutions will be 100% bullet proof to prevent spam from ever coming through again. And some of the solutions can also have a negative effect on the user’s experience.</p>
<p>That’s why Tripetto comes with a built-in spam protection mechanism which is a balance between preventing large amounts of spam and not bothering your respondents with anti-spam measures. This is <a href="https://tripetto.com/wordpress/pricing/{{ page.article_utm }}">fully included in all packages</a> of the Tripetto WordPress plugin, which also comes with a 14-day money-back guarantee – no questions asked!</p>
<div>
  <a href="https://tripetto.com/wordpress/pricing/{{ page.article_utm }}" class="blocklink">
    <div>
      <span class="title">Get Tripetto Pro for WordPress</span>
      <span class="description">Tripetto Pro is for the Tripetto WordPress plugin and comes in 3 all-inclusive variants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" loading="lazy" />
    </div>
  </a>
</div>
