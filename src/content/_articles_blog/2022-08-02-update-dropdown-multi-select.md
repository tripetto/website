---
layout: blog-article
base: ../../
permalink: /blog/update-introducing-the-dropdown-multi-select-block-with-keyword-search/
title: Update! Introducing dropdown (multi-select) block - Tripetto Blog
description: In today's update we introduce a very useful question type, namely the dropdown (multi-select) block with keyword search.
article_title: Update! Introducing the dropdown (multi-select) block with keyword search
article_slug: "Update: Dropdown (multi-select) block"
author: mark
time: 4
category: product
category_name: Product
tags: [product, product-update, feature, editor, blocks]
areas: [studio,wordpress,sdk]
year: 2022
---
<p>In today's update we introduce a very useful question type, namely the dropdown (multi-select) block with keyword search. And some improvements to the storyboard of our form builder. All available for free for all our users!</p>

<p>There are tons of use cases in which the new <a href="{{ page.base }}help/articles/how-to-use-the-dropdown-multi-select-block/">dropdown (multi-select) block</a> is ideal. Think of a question in which you want the respondent to select multiple countries from a list. Instead of letting the respondent go through the whole list of countries, the respondent can now simply search for the right countries and add them to their selection. That makes filling out your forms in Tripetto even easier than it already was!</p>
<p>Next to this new question block, we also did some improvements to the storyboard of our form builder, introducing "branch-only structures" and the right-click gesture. We'll explain those below too!</p>
<hr/>

<h2>A closer look at the dropdown (multi-select) block</h2>
<p>The new dropdown (multi-select) block is available from the question type menu in the Tripetto form builder. Simply select it from there and enter your question.</p>
<p>Next step, add or import the options you want to give your respondents. With the import function you can insert a list of possible options and add those as separate options at once. That saves you some time!</p>
<p>And that's basically it! The block now looks like this in your form:</p>
<figure>
  <img src="{{ page.base }}images/help/blocks/dropdown-multi-select.gif" width="1200" height="760" alt="Screenshot of dropdown (multi-select) in Tripetto" loading="lazy" />
  <figcaption>The all new dropdown (multi-select) block in action.</figcaption>
</figure>
<p>Your respondents can select the desired options from the dropdown in two ways:</p>
<ul>
  <li><strong>Scroll and select</strong>: scroll through the list of options manually and select the desired option(s);</li>
  <li><strong>Search and select</strong>: enter a keyword which will filter the options that meet the entered keyword and select the desired matching option(s).</li>
</ul>

<h3>Advanced options</h3>
<p>You can alter the settings of the dropdown (multi-select) block to meet your requirements, for example by enabling limits (min-max amount of options to select), randomization, scores and data format.</p>
<p>The following article in our Help center will explain every possibility of this new block:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-dropdown-multi-select-block/">How to use the dropdown (multi-select) block</a></li>
</ul>
<hr/>

<h2>A closer look at the storyboard improvements</h2>
<p>Up to now, a logic branch was always connected to a section in your form structure, even if that was an empty section without any questions blocks in it. That made your form structure a bit unnecessary complicated.</p>
<p>That's why from now on you can start a branch without the need of a section. We call that a "branch-only structure". Have a look at the following before-after comparison to see the difference.</p>
<figure>
  <img src="{{ page.base }}images/blog/20220802/branch-only.png" width="1200" height="587" alt="Screenshot of branch-only in Tripetto" loading="lazy" />
  <figcaption>Before and after to demonstrate the advantage of the branch-only structure.</figcaption>
</figure>
<p>Looks like a minor difference, but believe us, it can have a major impact on the overview of your form structure! You can add such a branch-only structure by right-clicking the <code><i class="fas fa-plus"></i></code> icon (or holding it with your finger on touch-enabled devices) and select <code>Insert branch</code>.</p>

<h3>Wait... Right-click?!</h3>
<p>Speaking of the "right-click"... That's the second improvement to the storyboard: you can now use the right-click of your mouse across all elements on the storyboard to quickly access all possibilities of each element. Another small improvement, which will turn out to be very useful.</p>
<hr/>

<h2>Now available everywhere for free</h2>
<p>This update is available for free across all our platforms, so in the <strong>WordPress plugin</strong> for our WordPress users (update to version 5.5.0) and in the <strong>studio at <a href="{{ site.url_app }}" target="_blank" rel="noopener noreferrer">tripetto.app</a></strong>. And even for developers using our <strong>SDK</strong>!</p>
