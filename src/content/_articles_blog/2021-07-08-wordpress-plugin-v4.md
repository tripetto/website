---
layout: blog-article
base: ../../
permalink: /blog/wordpress-plugin-v4-introducing-plugin-access-management-and-settings/
title: Update! Plugin access management and settings - Tripetto Blog
description: With version 4 of the Tripetto WordPress plugin we added full support for WordPress user roles and now give you control over email and spam settings.
article_title: WordPress plugin v4 🙌 Introducing plugin access management and settings
article_slug: "Update: Plugin access management and settings"
author: jurgen
time: 4
category: product
category_name: Product
tags: [product, product-update, product, release, showcase]
areas: [wordpress]
year: 2021
---
<p>Last week we announced our <a href="{{ page.base }}blog/our-shifted-focus-to-wordpress-why-the-change-and-what-does-it-mean-for-you/">shifted focus to WordPress</a>. To strengthen that focus we also released a major update to our WordPress plugin. We added full support for WordPress user roles and now give you control over email and spam settings.</p>

<h2>WordPress plugin v4 release</h2>
<p>As part of our WordPress focus, we released version 4 of the WordPress plugin. You can update your plugin installation to this latest version from your WP Admin. This update includes:</p>
<ul>
  <li>Full support for WordPress user roles to configure plugin access and capabilities;</li>
  <li>New settings for emails that are sent from within your Tripetto forms;</li>
  <li>New settings for the level of Tripetto's built-in spam protection system;</li>
  <li>New onboarding wizard to configure settings and get a kickstart with Tripetto;</li>
  <li>Improved dashboard of the plugin;</li>
  <li>Lots of small improvements and bugfixes.</li>
</ul>
<p>Let's have a closer look at these updates.</p>

<h2>Plugin access and capabilities</h2>
<p>Until now only the WordPress administator role had full access to the Tripetto plugin. That wasn't always ideal as you also may want other users of your WP Admin to have access to Tripetto.</p>
<p>One of the great things about WordPress is that it has a built-in user roles concept, designed to give the site owner the ability to control what users can and cannot do within the site. From now on Tripetto fully supports those user roles, giving you control over which roles have access to the plugin and what capabilities they have inside the plugin.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-configure-plugin-access-and-capabilities-with-wordpress-user-roles/">How to configure plugin access and capabilities with WordPress user roles</a></li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/wordpress-roles/01-custom-access.png" width="1200" height="760" alt="Screenshot of the onboarding wizard" loading="lazy" />
  <figcaption>Configure custom role access in the onboarding wizard.</figcaption>
</figure>

<h2>Email settings</h2>
<p>Tripetto forms have a few options to send emails from within the form. Until now we always used the WP Admin email address as the sender address, but in some situations that could cause some troubles with sending and delivering email messages.</p>
<p>From now on we have made a setting for the send email address, so you can simply determine what email address is used to send your messages. If set to a correct email address, that will result in a way better send and deliverance rate.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-configure-plugin-settings-for-email-sending-and-spam-protection/">How to configure plugin settings for email sending and spam protection</a></li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/wordpress-settings/01-email.png" width="1200" height="760" alt="Screenshot of the onboarding wizard" loading="lazy" />
  <figcaption>Configure the email settings in the onboarding wizard.</figcaption>
</figure>

<h2>Spam protection settings</h2>
<p>Tripetto has a built-in spam protection system, so you don't need to bother your respondents with unsolvable puzzles. In some situations that spam protection system could work against itself, for example if lots of respondents submit your form from the same IP address at the same time. Think of a teacher that lets his students complete an exam form in a class room and all students are on the same IP address of the school's network.</p>
<p>That's why from now on you can set the spam protection level yourself. That way you can for example bypass the IP restriction of the spam protection system. Or simply add an allow list of IP addresses that are excluded from the IP checks.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-configure-plugin-settings-for-email-sending-and-spam-protection/">How to configure plugin settings for email sending and spam protection</a></li>
</ul>
<figure>
  <img src="{{ page.base }}images/help/wordpress-settings/02-spam.png" width="1200" height="760" alt="Screenshot of the onboarding wizard" loading="lazy" />
  <figcaption>Configure the spam protection mode in the onboarding wizard.</figcaption>
</figure>

<h2>Onboarding form</h2>
<p>To help you with setting up all the above new features, we also added an onboarding wizard to the plugin. And believe it or not, but that wizard is actually a Tripetto form that runs right inside the Tripetto plugin (maybe we're a bit biased, but we think that's a pretty cool inception 🤯)!</p>
<p>The wizard guides you through all role settings, email settings and spam settings, so you can simply configure this to your needs. And next to that the onboarding wizard helps you to get a kickstart with Tripetto. It takes you through our most important video tutorials, so you are fully prepared to build amazing forms right away!</p>
<p>The onboarding wizard starts after you install or update the plugin. And you can always start it later on via the new dashboard of the Tripetto plugin.</p>

<h2>Other improvements and bugfixes</h2>
<p>This WordPress update also includes some other improvements and bugfixes:</p>
<ul>
  <li>New dashboard with access to the help center, video tutorials and the onboarding wizard;</li>
  <li>Added new branch mode for checking if none of the conditions match (logical NOT);</li>
  <li>Added a new constant to the calculator to find the current branch index number;</li>
  <li>Added a style setting to remove the asterisk for required blocks (only applies to the autoscroll and classic runner);</li>
  <li>Improved the selection of the right question type for new blocks in the form builder. When you add a new block you can now instantly select the desired question type from a cloud of all available question types;</li>
  <li>Improved variables support in URLs so you can use a variable to specify the protocol and domain part of an URL;</li>
  <li>Improved the multiple choice and picture choice blocks so you can select a hyperlink target for URL choices.</li>
</ul>

<h2>Stay tuned!</h2>
<p>We're looking forward to improve and extend the WordPress plugin with each release we do. So stay tuned for more updates in the near future!</p>
