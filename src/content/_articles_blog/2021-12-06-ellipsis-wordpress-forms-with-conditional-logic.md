---
layout: blog-article
base: ../../
permalink: /blog/wordpress-forms-with-conditional-logic/
title: How to Use Conditional Logic with Forms in WordPress - Tripetto Blog
description: If you want to learn more about WordPress forms with conditional logic, you’re in the right place. This blog post will explore why and how to use them!
article_title: How to Use Conditional Logic with Forms in WordPress
article_slug: WordPress forms with conditional logic
article_folder: 20211206
article_image: cover.webp
article_image_width: 1640
article_image_height: 1081
article_image_caption: How to Use Conditional Logic with Forms in WordPress
author: jurgen
time: 10
category: guide
category_name: Guide
tags: [guide]
areas: [wordpress]
year: 2021
---
<p>If you want to learn more about WordPress forms with conditional logic, you’re in the right place. This blog post will explore why and how to use them!</p>

<p>Forms are an extremely important tool for business owners, agencies, and WordPress users because they ultimately support marketing, customer experience, and sales functions, to name a few. However, whether you’re creating a contact form or a survey, your form needs to be smart and intuitive. Not only will that positively impact your user experience, but it will also show a direct impact on your form conversions.</p>
<p>WordPress forms with conditional logic will take your form game to a whole new level by allowing you to create smart forms that delight your users and drive the results you’re looking for.</p>
<p>Read on as we walk you through how to use Tripetto to implement conditional logic in your WordPress forms and explain why it is the best choice for building website forms.</p>
<p>But first, let’s recap a few basics.</p>
<hr/>

<h2>What is Conditional Logic?</h2>
<p>Suppose you want to create smart, powerful forms for your WordPress site that retrieve invaluable data and possibly lead to a boost in conversions and/or increased form completions.</p>
<p><strong>In that case, conditional logic is the way forward.</strong></p>
<p>In short, conditional logic empowers you to build smart forms to collect information about your audience. But, best of all, conditional statements also enable WordPress forms to dynamically change based on how users respond to different sections of your forms. Furthermore, with plugins, you can master all of this without any HTML, PHP, JavaScript, or CSS knowledge.</p>
<hr/>

<h2>Using a Plugin to Create WordPress Forms with Conditional Logic</h2>
<p>Using a form builder plugin that supports conditional logic, such as Tripetto, simplifies the form creation process and comes with some additional benefits:</p>
<ul>
  <li>You don't need multiple forms for multiple use cases</li>
  <li>You offer end-users a more intuitive and pleasant experience because forms are quicker and easier to complete, which enhances how customers feel about your brand and how they engage with it</li>
  <li>Thanks to the previous point, you're more likely to see an increase in both conversion rates and form completion rates</li>
</ul>
<p>That said, before we go any further, let’s clarify the three different types of conditional logic:</p>

<h3>1. Simple Conditional Logic</h3>
<p>This is the most common type of conditional logic and the more basic of the three. Here's how it works:</p>
<p>Depending on the user’s selection, your form hides or shows fields, often from a dropdown menu. This is often called the 'IF/THEN' condition.</p>
<p>For example, if your form asks the user whether they want to be contacted, a field to input their email address appears if they tick the “yes” checkbox.</p>
<p>Another example might be when you create an appointment form, where the user is first asked if they want to book an appointment. Then, once they select ‘Yes,’ they’re then asked to specify from a dropdown menu who they want to schedule their appointment with, on which date, and at what time. Once these conditions are met, a confirmation email is sent.</p>

<h3>2. Complex Conditional Logic</h3>
<p>In contrast, complex conditional logic is also known as the 'AND/OR' condition. This comes in handy if you need more than just one conditional logic rule, as you can insert as many conditions as you’d like in your form.</p>
<p>Using OR logic, you can make a field disappear or appear when at least one of your conditions is met. For example, if you had a rating field between one and five stars and your user awards you either four or five stars, this could trigger a pop-up asking them to fill out a review.</p>
<p>Similarly, if you want to create an appointment form, where the user is first asked to book an appointment, if they select ‘YES’ and select a date, this could trigger a confirmation email only once the user’s ticked both boxes.</p>

<h3>3. Recurring Logic</h3>
<p>Recurring logic allows you to repeat certain questions for multiple items that a respondent has selected from a multi-select question. So, let's say for example you ask respondents, ‘what are your favorite colors?’, and they select 'blue' and 'yellow', recurring logic would generate the same set of questions for each of the selected colors, for example: ‘Why do you like blue?’ followed by ‘why do you like yellow?’.</p>
<hr/>

<h2>How is Conditional Logic Used in Forms</h2>
<p>Now that we’ve covered the basics, let’s look more closely at how you can use Tripetto’s conditional logic features within your WordPress forms.</p>
<p><strong>Here are a few examples:</strong></p>

<h3>Product Evaluation</h3>
<p>For <a href="https://tripetto.com/examples/product-evaluation/?utm_source=Tripetto&utm_medium=blog&utm_campaign=WordPress-forms-with-conditional-logic">product evaluation</a>, you could use Tripetto’s ‘Chat Form’. This is created in the style of an online chat between your business and the product reviewer/customer. In this instance, you could ask the customer to click “Start Evaluation”, and a chat bubble would appear asking the customer to provide their name before evaluating the product.</p>
<p>Then, more chat bubbles would appear, asking the customer to select which products they’ve used in the last three months. Once they’ve chosen the products in question, the chat form automatically sends further queries related to that product.</p>
<p>Needless to say, the ‘Chat Form’ is a very engaging way of gathering customer feedback to improve customer satisfaction and product quality.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-product-evaluation-form.png" width="1173" height="771" alt="A screenshot of a product evaluation form in Tripetto." loading="lazy" />
  <figcaption>Product evaluation form in Tripetto.</figcaption>
</figure>

<h3>Order Form</h3>
<p>Another example of recurring logic is Tripetto’s ‘Classic Form’ face; you can <a href="https://tripetto.com/examples/order-form/?utm_source=Tripetto&utm_medium=blog&utm_campaign=WordPress-forms-with-conditional-logic">create smart order forms</a> that calculate the prices of selected products and services. That way, the end-user doesn’t have to go to the trouble of manually calculating the total cost.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-order-form.png" width="1131" height="714" alt="A screenshot of an order form in Tripetto." loading="lazy" />
  <figcaption>Order form in Tripetto.</figcaption>
</figure>

<h3>Screening for Covid-19</h3>
<p>With <a href="https://tripetto.com/examples/coronavirus-covid-19-screening/?utm_source=Tripetto&utm_medium=blog&utm_campaign=WordPress-forms-with-conditional-logic">Tripetto’s ‘Autoscroll’ form</a>, you can build a screening tool to respond to Covid-19 symptoms.</p>
<p>For instance, users could be asked to click either of these options:</p>
<p><i>They are experiencing at least one of the listed Covid-19 symptoms</i><br/>OR<br/><i>They don’t have any of the listed symptoms</i></p>
<p>If they select the first option, they would be directed to a page that advises them to seek medical advice. Whereas, by choosing the second option, they would be directed to a page that asks them a series of additional questions about their age and other symptoms they’re experiencing.</p>
<p>This example uses the Autoscroll form face in a left-right horizontal scroll direction:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-corona-screening-form.png" width="1637" height="851" alt="A screenshot of a Covid-19 screening form in Tripetto." loading="lazy" />
  <figcaption>Covid-19 screening form in Tripetto.</figcaption>
</figure>
<p>The above are just a <a href="https://tripetto.com/examples/?utm_source=Tripetto&utm_medium=blog&utm_campaign=WordPress-forms-with-conditional-logic">few examples</a> of the many ways your business can use conditional logic to create different forms that meet your website’s bespoke needs. You could also create forms to take restaurant reservations, manage RSVP invitations, calculate BMI - and much more!</p>
<hr/>

<h2>How to Choose the Right WordPress Plugin That Supports Conditional Logic</h2>
<p>If you’ve already done some research in this area, you may already know that there are many WordPress form builder plugins that provide conditional field functionalities. However, it’s essential to choose one that ticks all the boxes for your business needs.</p>
<p>In the case of Tripetto, users have access to a simple-to-use yet <a href="https://tripetto.com/wordpress/features/?utm_source=Tripetto&utm_medium=blog&utm_campaign=WordPress-forms-with-conditional-logic">feature-rich WordPress plugin</a>. Some of its standout hallmarks include:</p>
<ul>
  <li><strong>A simple drag and drop, flowchart-based interface.</strong> This makes creating flows from a virtual drawing board easy, which massively simplifies the creation of logic branches—all the while giving you a birds-eye view of your flow’s overall structure.</li>
  <li><strong>Advanced logic.</strong> Advanced logic will enable you to build interactive and conversational forms, to enhance engagement. These allow you to perform powerful real-time actions that consistently calculate, assess, and respond to user input. All of which can be easily encoded with the builder.</li>
  <li><strong>Rich customization options.</strong> Not all forms need to look the same - you can customize forms depending on the use case. You can customize fonts, colors, screen background, selection tools, hide Tripetto’s branding, and more.</li>
  <li><strong><a href="https://tripetto.com/form-layouts/?utm_source=Tripetto&utm_medium=blog&utm_campaign=WordPress-forms-with-conditional-logic">Three form faces.</a></strong> Tripetto’s form faces offer diverse layouts (autoscroll, chat, and classic). You can use these to completely change the look and feel of your forms according to your use case and audience. For instance, a ‘Chat Face’ may present different questions in the form of a chatbox, whereas an ‘Autoscroll Face’ will display one question at a time.</li>
  <li><strong>Complete control.</strong> All collected data is stored in your <a href="https://tripetto.com/blog/wordpress-forms-to-database/?utm_source=Tripetto&utm_medium=blog&utm_campaign=WordPress-forms-with-conditional-logic">WordPress database</a>.</li>
  <li><strong>Abundant automations.</strong> All logic features are included by default, and you’ll receive form completion notifications via Slack or email and connect to 1000+ services via Zapier, Make, Pabbly Connect, or custom webhooks. You can also track form completion using Google Analytics and your Facebook Pixel. This data is crucial for pinpointing where users are dropping-off so that you can optimize your form strategy.</li>
  <li><strong>Customers first.</strong> Outstanding customer support, clear self-help documents, and step-by-step tutorials.</li>
</ul>
<hr/>

<h2>Tripetto, WordPress Forms, and Conditional Logic</h2>
<p>As you may have already deducted from the previous section, Tripetto is a unique form builder for WordPress sites. It delivers stellar forms to end-users while providing form creators an intuitive drag and drop experience. One of Tripetto’s most attractive aspects is that no coding knowledge or experience is required to build smart conversational forms.</p>
<p>Rather than creating one-size-fits-all forms, with Tripetto, you can make smart, dynamic forms that are <a href="https://tripetto.com/help/articles/how-to-make-your-forms-smart-and-conversational/?utm_source=Tripetto&utm_medium=blog&utm_campaign=WordPress-forms-with-conditional-logic">fun for website visitors to complete</a>. Often, these feel more like having a conversation than completing a chore. In other words, you can publish forms that react to user input, triggering the next series of relevant questions/form fields.</p>
<p>Tripetto builds on traditional conditional logic features by adopting <a href="https://tripetto.com/logic-types/?utm_source=Tripetto&utm_medium=blog&utm_campaign=WordPress-forms-with-conditional-logic">three main types of logic</a>, which all come standard with Tripetto:</p>

<h3>1. Branch Logic</h3>
<p>Branch Logic is sometimes called logic flows, and/or routing, or question logic. But, put simply, Branch Logic directs the right follow-up questions to respondents based on their earlier answers. Using Tripetto’s ‘culling modes,’ you can either ask a straightforward follow-up question or an entirely new set of questions. You can also expand this further by adding other follow-up questions for each additional selected answer.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-builder-1.png" width="1999" height="1001" alt="A screenshot of branch logic in Tripetto's form builder." loading="lazy" />
  <figcaption>Branch logic in Tripetto's form builder.</figcaption>
</figure>

<h3>2. Jump Logic</h3>
<p>Similar to Branch Logic, Jump Logic is sometimes called Routing or Logic Jumps. This conditional logic form permits respondents to skip parts of your form that aren't relevant to them and move onto other selected points. However, you can still ensure that some entries remain ‘required fields’ to guarantee you collect the data you need. In some cases, this type of jump may take a respondent straight to the end of the form without needing to answer any additional questions.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-builder-2.png" width="1999" height="1002" alt="A screenshot of jump logic in Tripetto's form builder." loading="lazy" />
  <figcaption>Jump logic in Tripetto's form builder.</figcaption>
</figure>

<h3>3. Pipe Logic</h3>
<p>Pipe Logic is also called answer merge codes, piping, and/or recall information, or question piping. It enables you to personalize your forms by using data given in earlier questions and (where relevant) to display it in future questions. This works wonders for boosting completion rates.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-piping-logic.png" width="1169" height="523" alt="A screenshot of piping logic in a Tripetto form." loading="lazy" />
  <figcaption>Pipe logic in a Tripetto form.</figcaption>
</figure>
<hr/>

<h2>Tripetto’s Smart Features</h2>
<p>In addition to WordPress forms with conditional logic, Tripetto brings several other competitive features to the world of forms and allows you to:</p>
<ul>
  <li><strong>Personalize closing messages</strong>: You can tailor your form’s closing message to respondents based on their answers. It’s another way of reducing boredom and providing users with a more engaging experience.</li>
  <li><strong>Process <a href="https://tripetto.com/blog/calculated-fields-form/?utm_source=Tripetto&utm_medium=blog&utm_campaign=WordPress-forms-with-conditional-logic">advanced calculations</a> with given answers</strong>: This comes in handy for things like calculating prices for product lists, counting <a href="https://tripetto.com/blog/conditional-logic-quiz-wordpress/?utm_source=Tripetto&utm_medium=blog&utm_campaign=WordPress-forms-with-conditional-logic">quiz scores</a>, applying discounts with coupon codes, performing formula estimates, calculating a BMI score, and so on.</li>
  <li><strong>Set and manipulate form data</strong>: There are three ways you could do this:
    <ol>
      <li><strong>Hidden field block</strong>: This can help you gather and save hidden form data.</li>
      <li><strong>Custom variable block</strong>: This allows you to store a hidden value inside your forms, such as a number, text, boolean value, or date.</li>
      <li><strong>Set value block</strong>: This can change field values and variables inside your form. For example, to prefill values or lock entered values.</li>
    </ol>
 </li>
  <li><strong>Send emails</strong>: Tripetto forms can send emails to both you and your respondents. For example, if someone joins your mailing list, you might receive a notification email, and the subscriber might receive a confirmation email.</li>
  <li><strong>Block form submissions</strong>: There are two ways you can do this:
    <ol>
    <li><strong>Force stop block</strong>: This prevents a form from being submitted if a condition isn’t met. For example, if a survey respondent doesn’t meet the prerequisites, a notification message is generated to inform the user that they don’t fit your target audience.</li>
    <li><strong>Raise error block</strong>: This displays an error message that prevents the form from submitting. For example, if a respondent enters an email address incorrectly, an error block message appears. Hopefully, this will prompt the respondent to re-enter their email address correctly in the email field.</li>
    </ol>
 </li>
</ul>
<hr/>

<h2>Are You Ready to Start Using WordPress Forms with Conditional Logic?</h2>
<p>WordPress forms with conditional logic are an essential part of the online business toolkit for better conversion rates and a positive user experience.</p>
<p>Tripetto’s form plugin offers a wide range of easy-to-use features, including a slick and intuitive interface that doesn’t need additional add-ons. Tripetto’s functionalities are straight out-of-the-box and plentiful.</p>
<p>So, what are you waiting for? <a href="https://tripetto.com/wordpress/pricing/?utm_source=Tripetto&utm_medium=blog&utm_campaign=WordPress-forms-with-conditional-logic">Try Tripetto today</a> to see for yourself how simple it is to use. The 100% 14-day money-back guarantee makes this an easy decision!</p>
<div>
  <a href="https://tripetto.com/wordpress/pricing/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=WordPress-forms-with-conditional-logic" class="blocklink">
    <div>
      <span class="title">Get Tripetto Pro for WordPress</span>
      <span class="description">Tripetto Pro is for the Tripetto WordPress plugin and comes in 3 all-inclusive variants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" loading="lazy" />
    </div>
  </a>
</div>
