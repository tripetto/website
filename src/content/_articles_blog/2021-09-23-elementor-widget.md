---
layout: blog-article
base: ../../
permalink: /blog/another-update-tripetto-as-elementor-widget/
title: Another update! Tripetto as Elementor widget - Tripetto Blog
description: Version 5.1 of the Tripetto WordPress plugin introduces Tripetto as an Elementor widget to the Elementor builder.
article_title: Another update! Tripetto as Elementor widget
article_slug: "Update: WordPress Elementor widget"
article_folder: 20210923
author: mark
time: 3
category: product
category_name: Product
tags: [product, product-update, feature]
areas: [wordpress]
year: 2021
---
<p>Version 5.1 of the Tripetto WordPress plugin introduces Tripetto to the Elementor builder. Embedding your forms in Elementor is now easier and better than ever before! Even within Elementor popups 🎉</p>

<p>Only a week ago we introduced <a href="{{ page.base }}blog/major-update-tripetto-as-gutenberg-block/">Tripetto as a Gutenberg block for WordPress</a>, making it way easier to embed your Tripetto forms if you use the Gutenberg editor.</p>
<p>But of course we know lots of you also use Elementor as their favorite WordPress builder, so <strong>today we're happy to introduce Tripetto as an Elementor widget too!</strong></p>

<h2>Tripetto's Elementor widget</h2>
<p>If you're using Elementor as your WordPress builder you no longer need the Tripetto shortcode to <a href="{{ page.base }}blog/contact-form-widget-wordpress/">add a form</a> to your content. From now on you can simply drag-and-drop the <strong>Tripetto Form widget</strong> at the desired place in your Elementor template. After you have selected the desired form, it will be added to your content and you will see a live preview of the form right away.</p>
<p>And that's it! It doesn't get much easier than that, we think...</p>
<figure>
  <img src="{{ page.base }}images/help/wordpress-elementor/insert-existing.gif" width="1200" height="760" alt="Screen recording of the Tripetto Elementor widget" loading="lazy" />
  <figcaption>Simply add the Tripetto Form widget and select your desired form.</figcaption>
</figure>

<h3>All embed options</h3>
<p>Of course we made sure all options you need to perfectly embed your Tripetto form are there. You can just simply configure everything you need at the left side of Elementor, just like you are used to with other Elementor widgets.</p>

<h3>Also inside popups</h3>
<p>The great thing about the Elementor widget is that it makes Tripetto forms also fully usable in Elementor popups. So you can now trigger a Tripetto form in a <a href="https://tripetto.com/blog/pop-up-contact-form-wordpress/">popup</a> by clicking a button on your website. That's cool!</p>

<h3>Show me 😍</h3>
<p>Let's have a quick look! <a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-elementor-widget/">Or read our help article about the Elementor widget.</a></p>
<figure>
  <div class="youtube-player" data-id="YjX-9oZG4Zs"></div>
  <figcaption>See Tripetto's Elementor widget in action!</figcaption>
</figure>

<h2>Get started with Tripetto's Elementor widget</h2>
<p><strong>Update your WordPress plugin to version 5.1.</strong> This will instantly add the Tripetto Form widget to your Elementor builder.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-elementor-widget/">How to embed your form in your WordPress site using the Elementor widget</a></li>
</ul>
