---
layout: blog-article
base: ../../
permalink: /blog/update-introducing-subforms-and-improved-studio-embeds/
title: "Introducing subforms and improved studio embeds - Tripetto Blog"
description: "The May 2023 update introduces a new handy feature in our form builder: subforms! And the studio embed codes got a revision."
article_title: "Update: Introducing subforms and improved studio embeds"
article_slug: "Update: Subforms"
article_folder: 20230523
author: mark
time: 4
category: product
category_name: Product
tags: [product, product-update, feature, editor]
areas: [studio,wordpress,sdk]
year: 2023
---
<p>The May 2023 update introduces a new handy feature in our form builder: subforms! And the studio embed codes got a revision.</p>

<p>With the May 2023 update we did quite some work behind the scenes to make everything future-proof. We'd love to take you along our journey of the last couple of months and tell you all about today's update!</p>
<blockquote>
  <h4>🔔 Notice for studio embed code users</h4>
  <p>This update includes a revision of the studio embed codes. Old embed codes will keep working, but updating to the new embed code is advised. More information:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/troubleshooting-update-your-embeds-after-may-2023-studio-update/">Update your embeds after May 2023 studio update</a></li>
  </ul>
</blockquote>
<hr/>

<h2>Update on our journey</h2>
<p>As you might know our <a href="{{ page.base }}wordpress/">WordPress plugin</a> was our main focus for the last two years. But with that focus we never lost sight of our other offerings, namely our standalone version of Tripetto in the cloud (<a href="{{ page.base }}studio/">Tripetto studio</a>) and the developer version of Tripetto (<a href="{{ page.base }}sdk/" target="_blank">Tripetto SDK</a>). Each version of Tripetto has its own value for different form solution audiences and we have never lost sight of those values and audiences.</p>
<p>That's why we wanted to emphasize our broad range of product offerings again. That resulted in a new version of our website which you are looking at right now. The main menu now gives access to all different Tripetto versions, so you can always see which version is for you and all its relevant content.</p>

<h3>Shoutout to the SDK</h3>
<p>A special shoutout has to be made to the SDK team. They have been very busy over the last year to complete the full-detailed <a href="{{ page.base }}sdk/docs/" target="_blank">SDK documentation</a>. By finalizing those technical docs and making it all available to developers with step-by-step guides, the SDK gets way more accessible for implementations. We even revamped parts of our SDK core components to make them implementable with just a few lines of code!</p>
<p>With proven implementations of the SDK by Fortune 500 companies and with the <a href="{{ page.base }}sdk/" target="_blank">revised SDK satellite site</a>, we hope to attract more companies to see the value of form integration into their own software. Tripetto is ready for it!</p>
<hr/>

<h2>Today's update</h2>
<p>Back to today's update. It has many behind the scenes improvements and additions to make the whole Tripetto platform future-proof. That said, there are a few updates we want to highlight, because they can force you to take some action, or simply because they are cool features.</p>

<h3>Subforms</h3>
<p>Let's start with subforms. This won't ring a bell right away, but we can promise this will be a delight if you're working with large forms!</p>
<p>Large forms can become a bit overwhelming in our form builder, making your form structure not really easy to maintain and oversee. With subforms you can extract parts of your form from the main storyboard and draw them on their own nested storyboard. That way you can build and maintain parts of your form on their own and minimize the size of the main form structure.</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-subforms-in-the-form-builder/">How to use subforms in the form builder</a></li>
</ul>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/subform.gif" width="1200" height="760" alt="Screen recording of subforms in Tripetto" loading="lazy" />
  <figcaption>Open a part of your form as a subform.</figcaption>
</figure>

<h3>Studio embed codes</h3>
<p>Next up: the studio embed code. The embed codes for form embedding from the Tripetto studio have been revised (this does not affect embeds via our WordPress plugin). Besides some under the hood improvements to make the embeds future-proof, the embed code itself also became smaller and easier.</p>
<p>The new embed code runs on the latest Tripetto core components. The core components that the old embed code uses, will not be updated anymore from here on. The old embed codes will keep working though. That said, we advise to always update your embed code(s), so you keep up to date with the latest version of Tripetto and you are sure everything keeps working; now and in the future.</p>
<blockquote>
  <h4>🧯 Troubleshooting: Update your embeds after May 2023 studio update</h4>
  <p>We wrote an article that helps you to update your studio embeds.</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/troubleshooting-update-your-embeds-after-may-2023-studio-update/">Update your embeds after May 2023 studio update</a></li>
  </ul>
</blockquote>
<h4>Embeds with self-hosting</h4>
<p>A special remark has to be made for embed codes that use self-hosting options, for example to store the collected data on your own endpoint. For such situations the terms have changed and this is no longer possible within the default studio embed code. Instead, the self-hosting options have shifted to the 'Integrate' share method in the Tripetto studio. By using this new share method, you enter a different version of Tripetto: the Tripetto FormBuilder SDK, for which specific terms and pricing apply.</p>
<blockquote>
  <h4>🧯 Troubleshooting: Switch to Integrate method</h4>
  <p>This update might require a switch to the Integrate method for you.</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/troubleshooting-update-your-embeds-after-may-2023-studio-update/">Update your embeds after May 2023 studio update</a></li>
  </ul>
</blockquote>


<h3>Update now available! 🚀</h3>
<p>This update is available across all our platforms, so in the <strong>WordPress plugin</strong> for our WordPress users (update to version 6.0.0; subforms included in the Pro plan) and in the <strong>studio at <a href="{{ site.url_app }}" target="_blank" rel="noopener noreferrer">tripetto.app</a></strong>. And of course for developers using our <strong>SDK</strong>!</p>
