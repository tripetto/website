---
layout: blog-article
base: ../../
permalink: /blog/introducing-our-help-center/
title: Introducing our Help Center - Tripetto Blog
description: With the introduction of the Help Center on our website, we want to help our users to get the most out of Tripetto. We'd like to give you a quick tour.
article_title: Introducing our Help Center
article_slug: Introducing Help Center
article_folder: 20200120
author: jurgen
time: 3
category: product
category_name: Product
tags: [product]
areas: [studio,wordpress]
year: 2020
---
<p>With the introduction of the Help Center on our website, we want to help our users to get the most out of Tripetto. We'd like to give you a quick tour.</p>

<h2>Helping our users</h2>
<p>Since the release of our end user product during 2019, we’ve had the opportunity to learn from users what they think of Tripetto. We do our utmost to help as well and as quickly as we can. These one-to-one contact moments gave us a clear insight into what our users want. We divide their input into support requests and feature requests.</p>

<h2>Support requests</h2>
<p>Quite a few users contacted us with questions regarding the usage of Tripetto, and there appeared to be a clear overlap between the questions users asked us.</p>
<p>This overlap made us think: probably more users may encounter these difficulties, so how can we help users that don't reach out to us immediately?</p>
<p>The solution was quite obvious: a <a href="{{ page.base }}help/"><strong>Help Center</strong></a>.</p>

<h3>What's in the Help Center?</h3>
<p>The Help Center contains lots of articles that cover frequently asked questions and everyday use cases. It's divided into two main sections, bundling the articles of our end user products:
</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}studio/help/">Get help with the studio</a></li>
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}wordpress/help/">Get help with the WordPress plugin</a></li>
</ul>
<p>To easily find the article you're looking for, we categorized all articles on the phase you're in, for example the basics, building forms and getting results.</p>
<p>In each category, you'll find the articles associated with it. Most articles are actually based on the feedback and experiences of our users. Like <a href="{{ page.base }}help/articles/learn-the-basic-controls-to-use-the-form-builder/">how to use the form builder</a> or <a href="{{ page.base }}help/articles/how-to-install-the-wordpress-plugin/">how to install the WordPress plugin</a>. Or more advanced tutorials, like how to setup <a href="{{ page.base }}help/articles/how-to-automate-slack-notifications-for-each-new-result/">Slack notifications</a> or <a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/">webhook connections</a>.</p>
<div>
  <a href="{{ page.base }}help/" class="blocklink">
    <div>
      <span class="title">Help Center - Tripetto</span>
      <span class="description">Our Help Center covers how-to's, tutorials and the most frequently asked questions to get the most out of Tripetto.</span>
      <span class="url">tripetto.com/help</span>
    </div>
    <div>
      <img src="{{ page.base }}images/blog/{{ page.article_folder }}/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" />
    </div>
  </a>
</div>

<h3>Keep extending</h3>
<p>The plan is to extend the Help Center with new articles, how-to's and tutorials regularly. We have lots of ideas for that, but of course we're wide open for your input. So, if there's a subject that you would like to see featured in an in-depth help article, we're happy to hear from you.</p>

<h2>Feature requests</h2>
<p>Besides the support requests, we also have users asking for particular features they miss. We are very happy with this feedback, as it gives us a clear view of what our users need to use Tripetto. Unfortunately, we can’t develop all those requests at once, so we’re looking for a way to be more transparent about our product roadmap.</p>
<p>This resulted in our <a href="{{ page.base }}changelog/"><strong>changelog</strong></a>, so now it's more clear what updates we've released and also what's on our <a href="{{ page.base }}roadmap/"><strong>roadmap</strong></a> for development. In need of a feature that's not on our wishlist? Please, let us know through our <a href="{{ page.base }}roadmap/#request-feature"><strong>feature request form</strong></a>.</p>
<div>
  <a href="{{ page.base }}changelog/" class="blocklink">
    <div>
      <span class="title">Changelog - Tripetto</span>
      <span class="description">In our changelog you'll find all updates we've done so far and what we wish to do in the near future. And if you miss any feature, we're happy to hear from you!</span>
      <span class="url">tripetto.com/changelog</span>
    </div>
    <div>
      <img src="{{ page.base }}images/blog/{{ page.article_folder }}/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" />
    </div>
  </a>
</div>
