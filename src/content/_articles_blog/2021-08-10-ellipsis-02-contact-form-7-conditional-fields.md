---
layout: blog-article
base: ../../
permalink: /blog/contact-form-7-conditional-fields/
title: Contact Form 7 Conditional Fields Alternative - Tripetto Blog
description: The Contact Form 7 Conditional Fields is solid, but there are better alternatives. This post will introduce you to the Tripetto WordPress plugin!
article_title: Contact Form 7 Conditional Fields Alternative with Tripetto
article_slug: Contact Form 7 conditional fields alternative
article_folder: 20210810
article_image: cover.webp
article_image_width: 1640
article_image_height: 1081
article_image_caption: Contact Form 7 Conditional Fields Alternative with Tripetto
author: jurgen
time: 10
category: comparison
category_name: Comparison
tags: [comparison]
areas: [wordpress]
year: 2021
---
<p>The Contact Form 7 Conditional Fields is solid, but there are better alternatives. This post will introduce you to the Tripetto WordPress plugin!</p>

<p>For the unaware, <a href="https://contactform7.com/" target="_blank" rel="noopener noreferrer">Contact Form 7</a> (or CF7 form) is one of the most popular free form plugins for <a href="https://wordpress.org/" target="_blank" rel="noopener noreferrer">WordPress</a>. It lets you add simple contact forms to your WordPress site, and users love both its ease of use and the price.</p>
<p>One way you can enhance the functionality of any form is through ‘conditional fields’. This hides the field by default, and displays it if certain conditions are met. This ‘conditional logic’ is an advanced element, and lets WordPress use dynamics to change the layout based on user input.</p>
<p>Because Contact Form 7 is a simple plugin at its core, it doesn’t have conditional logic or fields by default. Instead, you’ll need to install the Contact Form 7 – Conditional Fields add-on. Even so, despite how straightforward the plugin and its add-ons are to use, there are better alternatives on the market.</p>
<p>In this post, we’re going to provide an honest comparison to Contact Form 7. We believe our plugin – <a href="https://tripetto.com/wordpress/?utm_source=tripetto&utm_medium=content-marketing&utm_content=contact-form-7-conditional-fields">Tripetto</a> – is a better option than this popular WordPress tool, and to prove it, this post will give you the lowdown. We’ll also look at some of the additional features of Tripetto, to help enhance the forms on your site.</p>
<hr/>

<h2>What Conditional Logic Is (And Why It Matters)</h2>
<p>Creating a <a href="https://tripetto.com/help/articles/how-to-build-your-forms-in-the-form-builder/?utm_source=tripetto&utm_medium=content-marketing&utm_content=contact-form-7-conditional-fields">standard contact form</a> with fields for the user’s name, email address, and subject is straightforward. Though, in lots of cases, you’ll need more than this. One example of advanced functionality is conditional logic.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-form-2.png" width="1000" height="572" alt="An example of a form that displays conditional logic." loading="lazy" />
  <figcaption>An example of a form that displays conditional logic.</figcaption>
</figure>
<p>On the technical side, the code will check the state of a piece of user information, and use that to display or hide other elements of the page.</p>
<p>In other words, you can create dynamic and interactive forms on your site that respond to what the user types or selects. Conditional logic is great for showing the user only the elements of the form they need to fill and send to you.</p>
<p>We’ll look at an example of this in greater detail in the next section. For now, consider how you could create a real-time ‘living’ form that responds based on the information your user gives. With the right logic in place, you can enhance the User Experience (UX), and boost form completion rates. After all, a user-friendly, fun, and engaging form will cause the user to click the ‘Submit’ button, rather than abandon it.</p>
<p>In short, conditional logic is a vital cog in your sales funnel’s wheel because it lets you control and guide the interaction with the user and the ‘page flow’.</p>
<hr/>

<h2>When You’d Want to Use Conditional Fields</h2>
<p>You’ll want to use <a href="https://tripetto.com/help/articles/learn-about-different-types-of-branch-conditions-for-your-logic/?utm_source=tripetto&utm_medium=content-marketing&utm_content=contact-form-7-conditional-fields">conditional logic</a> – and by extension, conditional fields – whenever the user has a decision to make. You’d also consider this when you would like the user to make a choice.</p>
<p>There are two broad generalizations you can make with regard to conditional logic - basic and complex:</p>
<ul>
  <li><strong>Basic logic</strong>: This is known as IF-THEN logic. In other words, if a user carries out an action, then display specific information relating to it.</li>
  <li><strong>Complex logic</strong>: This builds on basic logic to use multiple IF parameters using AND-OR operators.</li>
</ul>
<p>To offer more context, consider an example of basic logic using a dropdown. You could include a ‘catch-all’ “Other” item that displays additional fields. In simple English:</p>
<p><i><strong>If</strong> the user selects Other, <strong>then</strong> display a textarea so they can clarify the option.</i></p>
<p>To extrapolate, you could also bring in some advanced logic here too. For example, you could have multiple dropdowns of different business categories, that when combined, will display the text field.</p>
<p><i><strong>If</strong> the user selects Automotive <strong>and</strong> Repairs from two different dropdowns, <strong>then</strong> display a list of local mechanics.</i></p>
<p>Of course, these are simple examples, but they both offer a lot of power and scope for your own forms.</p>
<hr/>

<h2>The Pros and Cons of Using Contact Form 7</h2>
<p>Lots of WordPress form builders, such as Contact Form 7 and Tripetto, use conditional logic. This is because making a connection with users is paramount for a successful form.</p>
<p>What’s more, Contact Form 7 is popular because it has few barriers between your simple needs and wider goals. In fact, there are a number of benefits of using Contact Form 7:</p>
<ul>
  <li>It’s free to use.</li>
  <li>The usability is straightforward.</li>
  <li>It’s a popular WordPress form plugin, with millions of active installs.</li>
  <li>You’re able to use the plugin to create multiple contact forms per site, and customize each one to your requirements.</li>
  <li>There is some powerful functionality under the hood, such as <a href="https://captcha.net/" target="_blank" rel="noopener noreferrer">CAPTCHA support</a>, <a href="https://akismet.com/" target="_blank" rel="noopener noreferrer">Akismet spam filtering</a>, <a href="https://www.geeksforgeeks.org/ajax-introduction/" target="_blank" rel="noopener noreferrer">Ajax-powered</a> submissions, and more.</li>
</ul>
<p>Though, no solution can be perfect. As such, Contact Form 7 also has a number of drawbacks:</p>
<ul>
  <li>There’s no visual builder, which means you have to use HTML and CSS to get the form looking the way you’d like on the front end.</li>
  <li>Some updates cause site-breaking errors, and customers have <a href="https://wordpress.org/support/plugin/contact-form-7/reviews/?filter=1" target="_blank" rel="noopener noreferrer">reviewed the support</a> to be substandard in some cases.</li>
  <li>Contact Form 7 includes no conditional logic by default.</li>
</ul>
<p>In fact, let’s talk a bit more about how Contact Form 7 employs conditional fields, through an extension.</p>

<h3>The Contact Form 7 – Conditional Fields Add-On</h3>
<p>There’s no native support for conditional fields in Contact Form 7, and also no official add-on to include it. As such, Jules Colle developed the Contact Form 7 – Conditional Fields plugin:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/contact-form-7.png" width="1000" height="323" alt="The Contact Form 7 – Conditional Fields plugin." loading="lazy" />
  <figcaption>The Contact Form 7 – Conditional Fields plugin.</figcaption>
</figure>
<p>It has hundreds of five-star reviews on the <a href="https://wordpress.org/plugins" target="_blank" rel="noopener noreferrer">WordPress Plugin Directory</a>. It also has a lot of active installations and the developer provides regular updates.</p>
<p>Much like its parent plugin, the Contact Form 7 – Conditional Fields add-on is a simple tool that offers limited functionality. It lets you set ‘required’ fields, lets you hide or show information on your form, and also enables you to nest groups of information and form fields. What’s more, you can preserve these groups in emails too.</p>
<p>There’s also a premium version of the Contact Form 7 – Conditional Fields plugin that includes ‘multistep’ and ‘repeater’ functionality, custom JavaScript conditions, additional operators (such as greater than and less than), and more.</p>
<hr/>

<h2>Introducing Tripetto: An Alternative to Contact Form 7 Conditional Fields</h2>
<p>Though, despite the functional approach of both Contact Form 7 and the Contact Form 7 – Conditional Fields plugin, there’s a better alternative to both.</p>
<p>Tripetto is a lightweight and easy to configure WordPress plugin with a pleasing User Interface (UI). We’re going to offer an honest comparison here, as we know that Tripetto can stand toe-to-toe with the best – and that includes Contact Form 7 in this case.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-form-1.png" width="1000" height="576" alt="A Tripetto form on both tablet and mobile devices." loading="lazy" />
  <figcaption>A Tripetto form on both tablet and mobile devices.</figcaption>
</figure>
<p>Although the plugin is a ‘new kid on the block’ compared to Contact Form 7 and Contact Form 7 – Conditional Fields (with over 2,000 active installations), it’s received nearly 100 five-star reviews from happy customers. In fact, it has a five-star average rating too:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-reviews.png" width="1000" height="679" alt="The average rating for the Tripetto plugin." loading="lazy" />
  <figcaption>The average rating for the Tripetto plugin.</figcaption>
</figure>
<p>Unlike other form builders, everything you need is within your WordPress dashboard. In other words, there’s no need for a third-party account that links to a far-off database. Tripetto includes all of its functionality within WordPress.</p>
<p>The plugin also has a number of <a href="https://tripetto.com/wordpress/features/?utm_source=tripetto&utm_medium=content-marketing&utm_content=contact-form-7-conditional-fields">enhancing features and functionalities</a>:</p>
<ul>
  <li>Built-in <a href="https://tripetto.com/blog/contact-form-spam-wordpress/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=contact-form-7-conditional-fields">spam protection</a>.</li>
  <li>Slack notifications.</li>
  <li>Webhooks, so you can extend and integrate Tripetto based on your own needs.</li>
  <li>Advanced conditional logic built into the plugin, to help you deliver better UX and boost your completion rates.</li>
</ul>
<p>In addition, <a href="https://tripetto.com/wordpress/pricing/?utm_source=tripetto&utm_medium=content-marketing&utm_content=contact-form-7-conditional-fields">Tripetto’s pro version pricing</a> starts from ${{ site.pricing_wordpress_single }} for one site. The feature set is the same between each tier – we don’t lock essential functionality behind a paywall. You’ll only pay based on the number of sites you want to install Tripetto on.</p>

<h3>How Tripetto Compares to Contact Form 7</h3>
<p>At this point, you’ll likely understand what makes Tripetto a stellar alternative to Contact Form 7. For clarity, let’s recap some of the key points:</p>
<ul>
  <li>Tripetto includes a visual builder within the WordPress dashboard that helps you to create logical smart forms for your site. Contact Form 7 has no visual builder, so you’ll need to use HTML and CSS to achieve your goals.</li>
  <li>While Contact Form 7 users have to go through the WordPress support pages and get answers from the community, Tripetto has weekday priority support, and assistance at most times of the day. There’s also the dedicated <a href="https://tripetto.com/help/?utm_source=tripetto&utm_medium=content-marketing&utm_content=contact-form-7-alternative">Help Center</a>, for when you need to search for an answer to your query.</li>
  <li>Of course, Tripetto includes conditional logic (and logic jumps) as standard. Contact Form 7 doesn’t include this out of the box, and even then, you’ll need to use a third-party unofficial add-on in the form of the Contact Form 7 – Conditional Fields add-on.</li>
</ul>
<p>Rather than discuss the benefits of Tripetto, let’s show you how to create a basic form with conditional logic in the next section.</p>
<hr/>

<h2>How to Use Tripetto to Create Conditional Fields</h2>
<p>The good news is that you don’t need an add-on such as the Contact Form 7 – Conditional Fields plugin to add conditional logic to your WordPress forms. Tripetto can deliver the goods out of the box.</p>
<p>To start, you’ll want to <a href="https://tripetto.com/help/articles/how-to-install-the-wordpress-plugin/?utm_source=tripetto&utm_medium=content-marketing&utm_content=contact-form-7-conditional-fields">install and activate the plugin</a> within WordPress. Once you’re up and running, you’ll come to the Onboarding Wizard:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-onboarding.png" width="1000" height="550" alt="The Tripetto Onboarding Wizard." loading="lazy" />
  <figcaption>The Tripetto Onboarding Wizard.</figcaption>
</figure>
<p>This walks you through the setup, and also offers you a tutorial to create your first form:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-onboarding-tutorial.png" width="1000" height="550" alt="The Tripetto Tutorial dialog." loading="lazy" />
  <figcaption>The Tripetto Tutorial dialog.</figcaption>
</figure>
<p>You can (of course) go through this if you want, although if you click <i>No</i>, you’ll come to the Tripetto Dashboard:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-dashboard.png" width="1000" height="549" alt="The Tripetto Dashboard." loading="lazy" />
  <figcaption>The Tripetto Dashboard.</figcaption>
</figure>
<p>We advise you to <a href="https://tripetto.com/help/?utm_source=tripetto&utm_medium=content-marketing&utm_content=contact-form-7-conditional-fields">take a look around here</a>, as there are lots of tutorials and guides on how to get the best from the plugin. The videos you find dotted around the Dashboard are also part of <a href="https://www.youtube.com/c/tripetto">our YouTube channel</a> – another goldmine of information on using Tripetto.</p>
<p>If you click the <i>Build Your First Form</i> button from the Dashboard screen, you’ll come to the visual form builder:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-builder-1.png" width="1000" height="492" alt="The Tripetto interface." loading="lazy" />
  <figcaption>The Tripetto interface.</figcaption>
</figure>
<p>You start on the <i>Build</i> screen. This is where you create your form. You use the green and orange icons to type welcome or closing text for your form, and you add blocks using the <i>Plus</i> icons. Here, we have an email field, and a checkbox:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-builder-2.png" width="1000" height="553" alt="A form without any logic applied." loading="lazy" />
  <figcaption>A form without any logic applied.</figcaption>
</figure>
<p>From here, we need to add conditional logic. To do this, click the <i>More Options</i> icon next to the relevant block, and click on the option you’d like to add a branch to:
</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-builder-3.png" width="1000" height="645" alt="Adding a branch to Tripetto." loading="lazy" />
  <figcaption>Adding a branch to Tripetto.</figcaption>
</figure>
<p>You’ll see that the storyboard will update to create separate tracks for each branch within your form’s structure. This lets you see the flow of your form in a quick and straightforward way. From here, you can add whatever functionality you’d like to trigger:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/tripetto-builder-4.png" width="1000" height="536" alt="A basic form complete with conditional logic." loading="lazy" />
  <figcaption>A basic form complete with conditional logic.</figcaption>
</figure>
<p>Of course, you can take things further, and add your own logic conditions and branches. The choice is yours!</p>
<hr/>

<h2>3 Top Conditional Logic Rules a Great Form Builder Should Include</h2>
<p>Before we wrap up, let’s talk about an aspect that every top-notch form builder should have – good conditional logic rules.</p>
<p>In our opinion, you’ll want the <a href="https://tripetto.com/logic-types/?utm_source=tripetto&utm_medium=content-marketing&utm_content=contact-form-7-conditional-fields">following in place</a> to ensure you have a flexible solution that can meet your needs:</p>
<ul>
  <li><strong>Dynamic field rules.</strong> This is essential, and you’ll want to control your form’s behavior as the user interacts with it.</li>
  <li><strong>Autoresponder rules.</strong> You’ll want to make sure that form submissions trigger automated emails to users, such as Thank You and other transactional emails.</li>
  <li><strong>Form rules.</strong> You should also look for a way to redirect users once they’ve submitted a form. As an alternative, you could display a message as a way to notify the user that their submission has been successful.</li>
</ul>
<p>When you’re looking for a long-term form plugin solution, check that your options have these rules in place. That way, you can bolster your completion rates, increase the UX of your forms and site, and come away with many happy customers (and the income to go with it).</p>
<hr/>

<h2>Conclusion</h2>
<p>Choosing the right contact form plugin is vital for a smooth experience across your site. What’s more, you need to make sure that the right features and functionality are in place to meet your needs and goals.</p>
<p>Conditional logic is a stellar way to improve the success rate of your site’s forms, and Contact Form 7 combined with the Contact Form 7 – Conditional Fields add-on is a popular choice. Though, choosing a quality premium solution <a href="https://tripetto.com/wordpress/pricing/?utm_source=tripetto&utm_medium=content-marketing&utm_content=contact-form-7-conditional-fields">such as Tripetto</a> over free (yet less advanced) tools will benefit you in the long run. Our plugin has the right blend of features, functionality, and stellar support to become your go-to form building solution.</p>
<p>If you’d like to see what Tripetto can do for your forms and users, we’re <a href="https://tripetto.com/wordpress/pricing/?utm_source=tripetto&utm_medium=content-marketing&utm_content=contact-form-7-conditional-fields">waiting to help you</a>. What’s more, you have a no-risk, unconditional 14-day money-back guarantee on all purchases!</p>
<div>
  <a href="https://tripetto.com/wordpress/pricing/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=contact-form-7-conditional-fields" class="blocklink">
    <div>
      <span class="title">Get Tripetto Pro for WordPress</span>
      <span class="description">Tripetto Pro is for the Tripetto WordPress plugin and comes in 3 all-inclusive variants.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/help/logo-tripetto.png" width="160" height="160" alt="Tripetto logo" loading="lazy" />
    </div>
  </a>
</div>
