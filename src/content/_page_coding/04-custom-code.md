---
base: ../
---

<section class="coding-custom-code content">
  <div class="container container-intro" id="custom-code">
    <div class="row">
      <div class="col-sm-8 col-lg-11 col-xl-10">
        <span class="caption">Custom code form integrations</span>
        <h2 class="palette-sdk-custom"><span>Every part of Tripetto</span> neatly packed into your application.</h2>
      </div>
      <div class="col-sm-11 col-md-10 col-lg-9">
        <p>The SDK's parts for form creation and deployment are designed for quick embedding in straightforward cases as well as <strong>deep integration into advanced applications.</strong></p>
        <ul class="buttons">
          <li><a href="{{ page.base }}sdk/" class="button button-large button-orange">Get Started</a></li>
          <li><a href="{{ page.base }}sdk/how-it-works/" class="button button-large button-light">See Live Demo</a></li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
  <div class="container container-visual">
    <div class="row">
      <div class="col-12 sdk-carousel-components-buttons">
        <ul class="carousel-buttons">
          <li class="palette-sdk-builder active" data-target="#carouselSDKComponents" data-slide-to="0">
            <div>
              {% include sdk-icon-component.html component='builder' size='small' name='Form Builder' template='background' radius='medium' %}
            </div>
            <div>
              <h3><span>Form </span>Builder</h3>
              <p>The intelligent storyboard is for building forms like flowcharts with powerful logic flows.</p>
            </div>
          </li>
          <li class="palette-sdk-runners" data-target="#carouselSDKComponents" data-slide-to="1">
            <div>
              {% include sdk-icon-component.html component='runners' size='small' name='Form Runners' template='background' radius='medium' %}
            </div>
            <div>
              <h3><span>Form </span>Runners</h3>
              <p>Runners execute forms created in the builder and handle UX, logic and response delivery.</p>
            </div>
          </li>
          <li class="palette-sdk-blocks" data-target="#carouselSDKComponents" data-slide-to="2">
            <div>
              {% include sdk-icon-component.html component='blocks' size='small' name='Question Blocks' template='background' radius='medium' %}
            </div>
            <div>
              <h3><span>Question </span>Blocks</h3>
              <p>Customize and even develop question types to enhance the forms in your app or website.</p>
            </div>
          </li>
        </ul>
      </div>
    </div>
    <div class="row carousel-slides">
      <div class="col">
        <div id="carouselSDKComponents" class="carousel slide">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="row">
                <div class="col-12">
                  <div class="sdk-device-code sdk-device-code-right">
                    <img src="{{ page.base }}images/sdk-scenes/builder.webp" width="2000" height="1441" alt="Screenshot of the form builder." class="sdk-device-code-block sdk-device-code-device" loading="lazy" />
                    {% include sdk-code-snippet-builder.html id="builder" class="sdk-device-code-block sdk-device-code-snippet" %}
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="row">
                <div class="col-12">
                  <div class="sdk-device-code sdk-device-code-right">
                    <img src="{{ page.base }}images/sdk-scenes/runner.webp" width="2000" height="1441" alt="Screenshot of a customer satisfaction form in the autoscroll form face, shown on a tablet." class="sdk-device-code-block sdk-device-code-device" loading="lazy" />
                    {% include sdk-code-snippet-runner-autoscroll.html id="runner" class="sdk-device-code-block sdk-device-code-snippet" %}
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="row">
                <div class="col-12">
                  <div class="sdk-device-code sdk-device-code-right">
                    <img src="{{ page.base }}images/sdk-scenes/blocks.webp" width="2000" height="1441" alt="Screenshot of available question blocks in the form builder." class="sdk-device-code-block sdk-device-code-device" loading="lazy" />
                    {% include sdk-code-snippet-blocks.html id="blocks" class="sdk-device-code-block sdk-device-code-snippet" %}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container container-content">
    <div class="row">
      <div class="col-md-4 coding-custom-code-block coding-custom-code-block-components">
        <p class="content-explanation">Pick what you need from the SDK with <strong>advanced visual <a href="{{ page.base }}sdk/docs/builder/introduction/">form builder</a>, <a href="{{ page.base }}sdk/docs/runner/introduction/">form runners</a>, and <a href="{{ page.base }}sdk/docs/blocks/introduction/">question types</a>.</strong> All open source, customizable, extensible, documented.</p>
        <a href="{{ page.base }}sdk/components/" class="hyperlink hyperlink-medium palette-sdk-custom"><span>SDK components</span><i class="fas fa-arrow-right"></i></a>
      </div>
      <div class="col-md-4 coding-custom-code-block coding-custom-code-block-hosting">
        <p class="content-explanation"><strong>Forms and data are exclusively self-hosted by you.</strong> Without any dependencies on unwanted infrastructure. No data ever touches any other platform, unless you let it.</p>
        <a href="{{ page.base }}sdk/self-hosting/" class="hyperlink hyperlink-medium palette-sdk-custom"><span>Self-hosting data</span><i class="fas fa-arrow-right"></i></a>
      </div>
      <div class="col-md-4 coding-custom-code-block coding-custom-code-block-usage">
        <p class="content-explanation">The SDK components are <strong>ready-to-go for <a href="{{ page.base }}sdk/plain-js/">plain JavaScript</a>, <a href="{{ page.base }}sdk/react/">React</a>, <a href="{{ page.base }}sdk/angular/">Angular</a>, and <a href="{{ page.base }}sdk/html/">HTML5</a>.</strong> Including type declarations and TypeScript support out of the box.</p>
        <ul class="sdk-logos">
          <li><a href="{{ page.base }}sdk/plain-js/" title="Tripetto's FormBuilder SDK works in JavaScript">{% include icons/javascript.html %}</a></li>
          <li><a href="{{ page.base }}sdk/react/" title="Tripetto's FormBuilder SDK works in React">{% include icons/react.html %}</a></li>
          <li><a href="{{ page.base }}sdk/angular/" title="Tripetto's FormBuilder SDK works in Angular">{% include icons/angular.html %}</a></li>
          <li><a href="{{ page.base }}sdk/html/" title="Tripetto's FormBuilder SDK works in HTML5">{% include icons/html5.html %}</a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
