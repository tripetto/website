---
base: ../
---

<section class="coding-no-code content">
  <div class="container container-intro" id="no-code">
    <div class="row">
      <div class="col-sm-8 col-lg-11 col-xl-10">
        <span class="caption">No-code form building and data collection</span>
        <h2 class="palette-build">Visually build your smart forms on the <span>magnetic storyboard.</span></h2>
      </div>
      <div class="col-sm-11 col-lg-10">
        <p><strong>Build forms and surveys like flowcharts</strong> on the storyboard, which actively organizes your flows. It makes designing truly conversational interactions clean, fast and fun. All no-code!</p>
        <ul class="buttons">
          <li><a href="{{ page.base }}versions/" class="button button-large">Get Started</a></li>
          <li><a href="{{ page.base }}examples/" class="button button-large button-light">See Examples</a></li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
  <div class="container container-visual">
    <div class="row">
      <div class="col-12"><small>Tripetto is available as no-code form solution in the online <a href="{{ page.base }}studio/">studio</a> and for <a href="{{ page.base }}wordpress/">WordPress</a>.</small></div>
      <div class="col-12 col-visual">
        <img src="{{ page.base }}images/studio-features/builder.webp" width="1240" height="762" alt="Screenshot of the storyboard and the preview of the form builder." loading="lazy" />
        <div class="coding-no-code-visual-block coding-no-code-visual-builder">
          <img src="{{ page.base }}images/studio-features/build-storyboard.webp" width="1646" height="1524" alt="Screenshot of the storyboard of the form builder." loading="lazy" />
        </div>
        <div class="coding-no-code-visual-block coding-no-code-visual-preview">
          <img src="{{ page.base }}images/studio-features/build-preview.webp" width="818" height="1524" alt="Screenshot of the preview of the form builder." loading="lazy" />
        </div>
      </div>
      <div class="col-12 col-visual">
        <div class="coding-no-code-visual-block coding-no-code-visual-explanation coding-no-code-visual-build features-build-explanation">
          <p class="content-explanation">The <a href="{{ page.base }}help/articles/learn-the-basic-controls-to-use-the-form-builder/">storyboard</a> lets you visually <a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/">compose forms and surveys</a> like flowcharts and <strong>simplifies using truly <a href="{{ page.base }}help/articles/how-to-make-your-forms-smart-and-conversational/">conversational logic</a></strong>.</p>
          <a href="{{ page.base }}magnetic-form-builder/" class="hyperlink hyperlink-medium palette-build"><span>Magnetic builder</span><i class="fas fa-arrow-right"></i></a>
          <a href="{{ page.base }}logic-types/" class="hyperlink hyperlink-medium palette-build"><span>Logic types</span><i class="fas fa-arrow-right"></i></a>
        </div>
        <div class="coding-no-code-visual-block coding-no-code-visual-explanation coding-no-code-visual-actions features-build-explanation">
          <p class="content-explanation">Besides questioning respondents, forms and surveys can <strong>perform powerful, real time actions and <a href="{{ page.base }}help/articles/how-to-use-the-calculator-block/">calculations</a></strong> in the background.</p>
          <a href="{{ page.base }}question-types/" class="hyperlink hyperlink-medium palette-build"><span>Question types</span><i class="fas fa-arrow-right"></i></a>
          <a href="{{ page.base }}calculator/" class="hyperlink hyperlink-medium palette-build"><span>No-code calculator</span><i class="fas fa-arrow-right"></i></a>
        </div>
        <div class="coding-no-code-visual-block coding-no-code-visual-explanation coding-no-code-visual-styling features-build-explanation">
          <p class="content-explanation">The <strong><a href="{{ page.base }}help/articles/how-to-let-the-live-preview-work-for-you/">real time preview</a></strong> shows what you’re building and customizing as a working form or survey <strong>with the <a href="{{ page.base }}help/articles/how-to-switch-between-form-faces/">form face</a> and <a href="{{ page.base }}help/articles/how-to-style-your-forms/">style</a></strong> you set.</p>
          <a href="{{ page.base }}form-layouts/" class="hyperlink hyperlink-medium palette-build"><span>Form layouts</span><i class="fas fa-arrow-right"></i></a>
          <a href="{{ page.base }}form-layouts/" class="hyperlink hyperlink-medium palette-build"><span>Styling</span><i class="fas fa-arrow-right"></i></a>
        </div>
      </div>
    </div>
  </div>
</section>
