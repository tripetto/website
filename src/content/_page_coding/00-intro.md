---
base: ../
---

<section class="coding-intro intro">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-md-10 col-lg-8 col-xl-9">
        <h1>No-code.<br/>Low-code.<br/>Or your code.</h1>
      </div>
      <div class="col-sm-9 col-md-10 col-lg-9">
        <p>Boost data collection with <strong>hypersmart no-code forms</strong>, connect with a <strong>1.000+ low-code automations</strong>, or create <strong>your custom code</strong> for the ultimate workflow.</p>
        <a href="{{ page.base }}versions/" class="hyperlink"><span>Which is for me?</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
