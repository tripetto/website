---
base: ../
---

<section class="coding-low-code">
  <div class="container container-intro content" id="low-code">
    <div class="row">
      <div class="col-sm-8 col-md-7 col-lg-10 col-xl-9">
        <span class="caption">Low-code form automations</span>
        <h2 class="palette-automations">Automate form data workflows with <span>1.000+ connections.</span></h2>
      </div>
      <div class="col-sm-11 col-lg-10">
        <p>Stay informed. Tripetto can send <strong>notifications to Slack or email</strong>, lets you <strong>connect to 1.000+ services</strong> and offers <strong>form activity tracking</strong> for valuable completion and drop-off insights.</p>
        <ul class="buttons">
          <li><a href="{{ page.base }}versions/" class="button button-large button-green">Get Started</a></li>
          <li><a href="{{ page.base }}data-automations/" class="button button-large button-light">See Automations</a></li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
  <div class="container-fluid ticker-holder ticker-connections">
    <div class="row">
      <ul class="ticker-blocks ticker-connections-services1">
        {% assign services = site.data.coding-low-code-services | where_exp: "item", "item.row == 1" %}
        {% for item in services %}
        <li>
          <a href="{{ item.url }}" target="_blank" rel="noopener noreferrer">
          <img src="{{ page.base }}images/automations-connections-services/{{ item.logo }}" width="64" height="64" alt="Logo of {{ item.name }}" loading="lazy" />
          <span>{{ item.name }}</span>
          </a>
        </li>
        {% endfor %}
      </ul>
      <ul class="ticker-blocks ticker-connections-services2">
        {% assign services = site.data.coding-low-code-services | where_exp: "item", "item.row == 2" %}
        {% for item in services %}
        <li>
          <a href="{{ item.url }}" target="_blank" rel="noopener noreferrer">
          <img src="{{ page.base }}images/automations-connections-services/{{ item.logo }}" width="64" height="64" alt="Logo of {{ item.name }}" loading="lazy" />
          <span>{{ item.name }}</span>
          </a>
        </li>
        {% endfor %}
      </ul>
      <ul class="ticker-blocks ticker-connections-services3">
        {% assign services = site.data.coding-low-code-services | where_exp: "item", "item.row == 3" %}
        {% for item in services %}
        <li>
          <a href="{{ item.url }}" target="_blank" rel="noopener noreferrer">
          <img src="{{ page.base }}images/automations-connections-services/{{ item.logo }}" width="64" height="64" alt="Logo of {{ item.name }}" loading="lazy" />
          <span>{{ item.name }}</span>
          </a>
        </li>
        {% endfor %}
      </ul>
    </div>
  </div>
  <div class="container container-content">
    <div class="row">
      <div class="col-md-4 coding-low-code-block coding-low-code-block-notifications">
        <p class="content-explanation">Tripetto can keep you updated on new form completions with instant, automatic <strong>notifications straight into your <a href="{{ page.base }}help/articles/how-to-automate-slack-notifications-for-each-new-result/">Slack</a> and <a href="{{ page.base }}help/articles/how-to-automate-email-notifications-for-each-new-result/">inbox</a></strong>.</p>
        <a href="{{ page.base }}data-automations/" class="hyperlink hyperlink-medium palette-automations"><span>Notifications</span><i class="fas fa-arrow-right"></i></a>
      </div>
      <div class="col-md-4 coding-low-code-block coding-low-code-block-connections">
        <p class="content-explanation">Have Tripetto <strong>send your data to 1.000+ connected services</strong> with <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-make/">Make</a>, <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-zapier/">Zapier</a>, <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-pabbly-connect/">Pabbly Connect</a> and <a href="{{ page.base }}help/articles/how-to-connect-to-other-services-with-custom-webhook/">custom webhooks</a>.</p>
        <a href="{{ page.base }}data-automations/" class="hyperlink hyperlink-medium palette-automations"><span>Connections</span><i class="fas fa-arrow-right"></i></a>
      </div>
      <div class="col-md-4 coding-low-code-block coding-low-code-block-tracking">
        <p class="content-explanation">Boost form completions and reduce drop-offs by <strong>tracking activity with <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-google-analytics/">Google Analytics</a>, <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-google-tag-manager/">Google Tag Manager</a> and <a href="{{ page.base }}help/articles/how-to-track-form-activity-with-facebook-pixel/">Facebook Pixel</a></strong>.</p>
        <a href="{{ page.base }}data-automations/" class="hyperlink hyperlink-medium palette-automations"><span>Activity tracking</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>

