---
base: ../
---

<section class="landing-overview">
  <div class="container">
    <div class="row landing-overview-intro">
      <div class="col-12">
        <small>A perfectly powerful form solution on every level.</small>
        <h2>To code or not to code.</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-12 landing-overview-anchors">
        <ul class="tiles-text tiles-text-medium tiles-text-anchor">
          <li data-anchor="#no-code">
            <div>
              {% include icon-paragraph.html chapter='build' paragraph='no-code' name='No-code' folder='coding' %}
            </div>
            <div>
              <h3>No-code</h3>
              <p><strong>Instantly build and run</strong> forms to collect data.</p>
            </div>
          </li>
          <li data-anchor="#low-code">
            <div>
              {% include icon-paragraph.html chapter='automations' paragraph='low-code' name='Low-code' folder='coding' %}
            </div>
            <div>
              <h3>Low-code</h3>
              <p>Connect with a <strong>1.000+ low-code automations</strong>.</p>
            </div>
          </li>
          <li data-anchor="#custom-code">
            <div>
              {% include icon-paragraph.html chapter='sdk-custom' paragraph='custom-code' name='Custom code' folder='coding' %}
            </div>
            <div>
              <h3>Custom<span> code</span></h3>
              <p>Equip your site or app with a <strong>tailored form solution</strong>.</p>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
