---
base: ../../
videos: [build-welcome-message, build-closing-message, logic-branch-logic, logic-calculations, customization-style, automations-webhook, sharing-wordpress-gutenberg, hosting-wordpress-results]
---

<section class="help-videos-featured">
  <div class="container">
    <div class="row">
      <div class="col">
        <h2>Featured Video Tutorials</h2>
        <a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/" class="help-video-quickstart"><img src="{{ page.base }}images/help-videos-thumbnails/quickstart.png" width="1072" height="360" alt="Visual thumbnail for the video 'How to build a simple form'" /></a>
        {% if page.videos %}
        <ul class="help-videos">
          {% for video in page.videos %}
          {% assign video_item = site.data.help-videos[video] %}
          <li><a href="{{ page.base | append: video_item.url }}"><img src="{{ page.base }}images/help-videos-thumbnails/{{ video_item.thumbnail }}" width="256" height="144" alt="Visual thumbnail for the video '{{ video_item.name }}'" /></a></li>
          {% endfor %}
        </ul>
        {% endif %}
      </div>
    </div>
    <div class="row">
      <div class="col help-videos-featured-footer">
        <a href="{{ page.base }}wordpress/help/video-tutorials/" class="hyperlink"><span>Show all video tutorials</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
