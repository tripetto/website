---
base: ../../
---

<section class="wordpress-checkout-confirmation-hero block-first intro">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <h1>Thank You!</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-7 col-lg-6">
        <p>Now that you’ve purchased Tripetto Pro, you can <strong>unlock all advanced Tripetto features</strong> as follows.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-9 col-xl-8">
        <ul class="fa-ul list-arrows">
          <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><span class="list-arrows-text">Open the Tripetto email in your inbox</span></li>
          <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><span class="list-arrows-text">Download Tripetto Pro from there</span></li>
          <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><span class="list-arrows-text">Install Tripetto Pro, and off you are 🚀</span></li>
        </ul>
      </div>
    </div>
  </div>
</section>
