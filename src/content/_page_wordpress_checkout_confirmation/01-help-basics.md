---
base: ../../
---

{% assign categories_basics = site.page_wordpress_help_categories | where_exp: "item", "item.category_type == 'basics'" %}

<section class="wordpress-checkout-confirmation-basics help-basics">
  <div class="container">
    <div class="row">
      <div class="col">
        <ul class="help-tiles">
          {% for category_basics in categories_basics %}
            {% assign data_category_basics = site.data.help-categories[category_basics.category_id] %}
            {% include help-tile.html url=category_basics.url title=data_category_basics.tile_title description=data_category_basics.tile_description %}
          {% endfor %}
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
