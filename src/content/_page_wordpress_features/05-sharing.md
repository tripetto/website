---
base: ../../
---

<section class="wordpress-features-sharing content">
  <div class="container">
    <div class="row">
      <div class="col-sm-7 col-md-8 col-lg-9 shape-before" id="sharing">
        {% include icon-chapter.html chapter='sharing' size='small-medium' name='Easy Sharing' %}
        <span class="caption caption-rotated">Send out</span>
        <h2 class="palette-sharing shape-after">Easily share forms in WordPress with <span>flexible publication.</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-11 col-lg-10">
        <p>Tripetto lets you choose how to deploy forms and surveys for collecting data. As a <strong>shareable URL</strong>, or embedded in your page using the <strong>shortcode</strong>, <strong>Gutenberg block</strong> or <strong>Elementor widget</strong>.</p>
      </div>
    </div>
    <div class="row">
      <div class="col features-sharing-visual">
        <div class="features-sharing-visual-top">
          <a href="{{ page.base }}help/articles/how-to-share-a-link-to-your-form-in-your-wordpress-site/" target="_blank" class="features-sharing-option features-sharing-link">
            {% include icons/shareable-link.html %}
          </a>
          <a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-gutenberg-block/" target="_blank" class="features-sharing-option features-sharing-gutenberg">
            <img src="{{ page.base }}images/wordpress-features/sharing-gutenberg.png" alt="Logo of Gutenberg editor" loading="lazy" />
          </a>
        </div>
        <div class="features-sharing-visual-bottom">
          <a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-elementor-widget/" target="_blank" class="features-sharing-option features-sharing-elementor">
            {% include icons/elementor.html %}
          </a>
          <a href="{{ page.base }}help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-shortcode/" target="_blank" class="features-sharing-option features-sharing-shortcode">
            {% include icons/embed.html %}
          </a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <a href="{{ page.base }}wordpress/help/sharing-forms-and-surveys/" class="hyperlink palette-sharing"><span>How to share forms and surveys</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
