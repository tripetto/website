---
base: ../../
---

<nav class="breadcrumb-navigation block-first" aria-label="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col">
        <ol>
          <li><a href="{{ page.base }}wordpress/">Tripetto WordPress plugin</a></li>
          <li><span>Features</span></li>
        </ol>
      </div>
    </div>
  </div>
</nav>

<section class="wordpress-features-intro content">
  <div class="container">
    <div class="row">
      <div class="col-lg-11 shape-before shape-after">
        <small>Your visitors will love filling out Tripetto forms!</small>
        <h1>A pretty different form builder for WordPress.</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-11 col-lg-10 intro-text">
        <p>Create deeply conversational forms and surveys directly from WordPress with Tripetto. You control collected data, <strong>with responses stored in your WordPress database.</strong></p>
        <a href="{{ page.base }}wordpress/feature-list/" class="hyperlink"><span>Full feature list</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
    <div class="row features-anchors">
      <div class="col-xl-11">
        <ul class="anchors-icons">
          <li>
            <a href="#build">
              {% include icon-chapter.html chapter='build' size='big' name='Visual Builder' %}
              <span>Visual Builder</span>
            </a>
          </li>
          <li>
            <a href="#logic">
              {% include icon-chapter.html chapter='logic' size='big' name='Advanced Logic' %}
              <span>Advanced Logic</span>
            </a>
          </li>
          <li>
            <a href="#customization">
              {% include icon-chapter.html chapter='customization' size='big' name='Customization' %}
              <span>Customization</span>
            </a>
          </li>
          <li>
            <a href="#automations">
              {% include icon-chapter.html chapter='automations' size='big' name='Automations' %}
              <span>Automations</span>
            </a>
          </li>
          <li>
            <a href="#sharing">
              {% include icon-chapter.html chapter='sharing' size='big' name='Easy Sharing' %}
              <span>Easy Sharing</span>
            </a>
          </li>
          <li>
            <a href="#hosting">
              {% include icon-chapter.html chapter='hosting' size='big' name='Flexible Storage' %}
              <span>Flexible Storage</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
