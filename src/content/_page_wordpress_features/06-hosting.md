---
base: ../../
---

<section class="wordpress-features-hosting content">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-lg-10 col-xl-10 shape-before" id="hosting">
        {% include icon-chapter.html chapter='hosting' size='small-medium' name='Flexible Storage' %}
        <span class="caption caption-rotated">Take stock</span>
        <h2 class="palette-hosting">Secure form data storage is essential. <span>All inside WordPress.</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-10">
        <p>Tripetto is a fullblown form builder plugin for WordPress and runs without any dependencies on outside infrastructure. <strong>Data is autonomously stored inside your WordPress only.</strong></p>
      </div>
    </div>
    <div class="row">
      <div class="col features-hosting-visual">
        <div class="features-hosting-option features-hosting-roles">
          <img src="{{ page.base }}images/wordpress-features/hosting-lock.svg" alt="Icon illustrating a lock for data security" loading="lazy" />
          <p>With the extensive WordPress roles management inside the plugin you can <strong>assign data access and capabilities to designated roles.</strong> <a href="{{ page.base }}help/articles/how-to-configure-plugin-access-and-capabilities-with-wordpress-user-roles/">Read how</a>.</p>
        </div>
        <div class="features-hosting-option features-hosting-wordpress">
          <h3>All data sits right with you.</h3>
          <p>Everything you build and collect with Tripetto is exclusively stored inside your own WordPress. <strong>Nothing ever touches any other platform</strong>, unless you choose to hook things up. <a href="{{ page.base }}blog/dont-trust-someone-else-with-your-form-data/">Read our blog on GDPR.</a></p>
          <img src="{{ page.base }}images/wordpress-features/hosting-wordpress.svg" alt="Human illustrating WordPress hosting" loading="lazy" />
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <a href="{{ page.base }}wordpress/help/managing-data-and-results/" class="hyperlink palette-hosting"><span>How to manage form and survey data</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
