---
base: ../../
---

<section class="wordpress-features-automations content">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-md-7 col-lg-10 col-xl-9 shape-before" id="automations">
        {% include icon-chapter.html chapter='automations' size='small-medium' name='Automations' %}
        <span class="caption caption-rotated">Hook up</span>
        <h2 class="palette-automations">Automate form data workflows with <span>1.000+ connections.</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-11 col-lg-10">
        <p>Stay informed. Tripetto can send <strong>notifications to Slack or email</strong>, lets you <strong>connect to 1.000+ services</strong> and offers <strong>form activity tracking</strong> for valuable completion and drop-off insights.</p>
      </div>
    </div>
    <div class="row">
      <div class="col">{% include automations-visual.html %}</div>
    </div>
  </div>
  <div class="container container-bottom">
    <div class="row">
      <div class="col-md-4">
        <p class="content-explanation">Tripetto can keep you updated on new form completions with instant, automatic <strong>notifications straight into your Slack and inbox</strong>.</p>
        <a href="{{ page.base }}wordpress/help/automating-things/#notifications" class="hyperlink hyperlink-medium palette-automations"><span>How to get notified</span><i class="fas fa-arrow-right"></i></a>
      </div>
      <div class="col-md-4">
        <p class="content-explanation">Have Tripetto <strong>send your data to 1.000+ connected services</strong> with Make, Zapier, Pabbly Connect and custom webhooks.</p>
        <a href="{{ page.base }}help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/" class="hyperlink hyperlink-medium palette-automations"><span>How to connect data</span><i class="fas fa-arrow-right"></i></a>
      </div>
      <div class="col-md-4">
        <p class="content-explanation">Boost completions and reduce drop-offs by <strong>tracking activity with Google Analytics, Google Tag Manager</strong> and <strong>Facebook Pixel</strong>.</p>
        <a href="{{ page.base }}help/articles/how-to-automate-form-activity-tracking/" class="hyperlink hyperlink-medium palette-automations"><span>How to track activity</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
