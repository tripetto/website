---
base: ../../
---

<section class="wordpress-features-customization content">
  <div class="container container-content">
    <div class="row">
      <div class="col-sm-7 col-lg-9 col-xl-8 shape-before" id="customization">
        {% include icon-chapter.html chapter='customization' size='small-medium' name='Customization' %}
        <span class="caption caption-rotated">Dress up</span>
        <h2 class="palette-customization">Pick the perfect form layout with <span>convertible faces.</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-11 col-lg-10">
        <p>Not all forms and surveys need the same appearance. Switch easily between <strong>totally different experiences</strong> while you’re designing to see what looks and feels best for the occasion.</p>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-12 carousel-form-faces-buttons">
        <ul class="carousel-buttons">
          <li class="palette-autoscroll active" data-target="#carouselFormFaces" data-slide-to="0">
            <div>
              {% include icon-face.html face='autoscroll' size='small' name='Autoscroll Form Face' template='background' radius='medium' %}
            </div>
            <div>
              <h3>Autoscroll<span> Face</span></h3>
              <p>Fluently presents <strong>one question at a time</strong>.</p>
            </div>
          </li>
          <li class="palette-chat" data-target="#carouselFormFaces" data-slide-to="1">
            <div>
              {% include icon-face.html face='chat' size='small' name='Chat Form Face' template='background' radius='medium' %}
            </div>
            <div>
              <h3>Chat<span> Face</span></h3>
              <p>Presents all <strong>questions and answers as a chat</strong>.</p>
            </div>
          </li>
          <li class="palette-classic" data-target="#carouselFormFaces" data-slide-to="2">
            <div>
              {% include icon-face.html face='classic' size='small' name='Classic Form Face' template='background' radius='medium' %}
            </div>
            <div>
              <h3>Classic<span> Face</span></h3>
              <p>Presents question fields in a <strong>traditional format</strong>.</p>
            </div>
          </li>
        </ul>
      </div>
    </div>
    <div class="row carousel-slides">
      <div class="col">
        <div id="carouselFormFaces" class="carousel slide carousel-fade">
          <div class="carousel-inner">
            <div class="carousel-item form-face active">
              <div class="row">
                <div class="col-lg-8">
                  <img src="{{ page.base }}images/scenes/autoscroll-customer-satisfaction.webp" alt="Screenshots of a customer satisfaction survey (with NPS) in the autoscroll form face, shown on a tablet and a mobile phone." loading="lazy" />
                </div>
                <div class="col-lg-4 form-face-list">
                  <h3>Customize things</h3>
                  <p class="content-explanation">The autoscroll face is akin to <strong>Typeform’s conversational format</strong> and lets you tweak:</p>
                  <ul>
                    <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/fonts.html %}Fonts</a></li>
                    <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/colors.html %}Colors</a></li>
                    <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/buttons.html %}Buttons</a></li>
                    <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/backgrounds.html %}Backgrounds</a></li>
                    <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/input-controls.html %}Input Controls</a></li>
                    <li><a href="{{ page.base }}help/articles/discover-additional-options-for-autoscroll-form-face/">{% include icons/scroll-direction.html %}Scroll Direction</a></li>
                    <li><a href="{{ page.base }}help/articles/how-to-edit-or-translate-all-text-labels-in-your-forms/">{% include icons/translations.html %}Labels/Translations</a></li>
                    <li><a href="{{ page.base }}wordpress/pricing/">{% include icons/remove-branding.html %}Hide Tripetto branding</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="carousel-item form-face">
              <div class="row">
                <div class="col-lg-8">
                  <img src="{{ page.base }}images/scenes/chat-wedding-rsvp.webp" alt="Screenshots of a wedding RSVP form in the chat form face, shown on a tablet and a mobile phone." loading="lazy" />
                </div>
                <div class="col-lg-4 form-face-list">
                  <h3>Customize things</h3>
                  <p class="content-explanation">The chat face is partly inspired by <strong>Landbot's chat format</strong> and lets you tweak:</p>
                  <ul>
                    <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/fonts.html %}Fonts</a></li>
                    <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/colors.html %}Colors</a></li>
                    <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/buttons.html %}Buttons</a></li>
                    <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/backgrounds.html %}Backgrounds</a></li>
                    <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/input-controls.html %}Input Controls</a></li>
                    <li><a href="{{ page.base }}help/articles/discover-additional-options-for-chat-form-face/">{% include icons/chat-bubbles.html %}Chat Avatars & Bubbles</a></li>
                    <li><a href="{{ page.base }}help/articles/how-to-edit-or-translate-all-text-labels-in-your-forms/">{% include icons/translations.html %}Labels/Translations</a></li>
                    <li><a href="{{ page.base }}wordpress/pricing/">{% include icons/remove-branding.html %}Hide Tripetto branding</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="carousel-item form-face">
              <div class="row">
                <div class="col-lg-8">
                  <img src="{{ page.base }}images/scenes/classic-restaurant-reservation.webp" alt="Screenshots of a restaurant reservation form in the classic form face, shown on a tablet and a mobile phone." loading="lazy" />
                </div>
                <div class="col-lg-4 form-face-list">
                  <h3>Customize things</h3>
                  <p class="content-explanation">The classic face is inspired by <strong>SurveyMonkey's traditional format</strong> and lets you tweak:</p>
                  <ul>
                    <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/fonts.html %}Fonts</a></li>
                    <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/colors.html %}Colors</a></li>
                    <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/buttons.html %}Buttons</a></li>
                    <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/backgrounds.html %}Backgrounds</a></li>
                    <li><a href="{{ page.base }}help/articles/how-to-style-your-forms/">{% include icons/input-controls.html %}Input Controls</a></li>
                    <li><a href="{{ page.base }}help/articles/discover-additional-options-for-classic-form-face/">{% include icons/scroll-direction.html %}Form Appearance</a></li>
                    <li><a href="{{ page.base }}help/articles/how-to-edit-or-translate-all-text-labels-in-your-forms/">{% include icons/translations.html %}Labels/Translations</a></li>
                    <li><a href="{{ page.base }}wordpress/pricing/">{% include icons/remove-branding.html %}Hide Tripetto branding</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <a href="{{ page.base }}wordpress/help/styling-and-customizing/" class="hyperlink palette-customization"><span>How to customize form and survey faces</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
