---
base: ../
videos: [build-builder,build-simple-form]
---

<section class="form-builder-storyboard">
  <div class="container" id="storyboard">
    <div class="row content">
      <div class="col-12 form-builder-storyboard-visual">
        <img src="{{ page.base }}images/form-builder/storyboard.webp" width="1240" height="762" alt="Screenshots of the Tripetto form builder." loading="lazy">
      </div>
      <div class="col-12 col-md-6">
        <h2>Magnetic storyboard</h2>
        <p>Build forms like flowcharts. The magnetic storyboard shows you what goes where and <strong>actively aligns your form</strong>. Things just can’t get messy!</p>
        <ul class="help-videos">
          {% for video in page.videos %}
          {% assign video_item = site.data.help-videos[video] %}
          <li><a href="{{ page.base}}{{ video_item.url }}"><img src="{{ page.base }}images/help-videos-thumbnails/{{ video_item.thumbnail }}" width="256" height="144" alt="Visual thumbnail for the video '{{ video_item.name }}'" /></a></li>
          {% endfor %}
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="form-builder-storyboard-columns">
  <div class="container container-content">
    <div class="row">
      <div class="col-md-4 form-builder-storyboard-column">
        <p class="content-explanation"><strong>Drag and drop any element</strong> right where you need it on the magnetic storyboard, which will visibly guide you along the way.</p>
        <a href="{{ page.base }}help/articles/learn-the-basic-controls-to-use-the-form-builder/" class="hyperlink hyperlink-medium palette-build"><span>Arranging flows</span><i class="fas fa-arrow-right"></i></a>
      </div>
      <div class="col-md-4 form-builder-storyboard-column">
        <p class="content-explanation"><strong>Move and smart zoom</strong> let you go into and out of any area on the storyboard once your structure grows bigger than the screen.</p>
        <a href="{{ page.base }}help/articles/learn-the-basic-controls-to-use-the-form-builder/" class="hyperlink hyperlink-medium palette-build"><span>Handling the board</span><i class="fas fa-arrow-right"></i></a>
      </div>
      <div class="col-md-4 form-builder-storyboard-column">
        <p class="content-explanation">The <strong>real time preview</strong> right next to the storyboard always instantly shows what you’re building and styling as a working form.</p>
        <a href="{{ page.base }}help/articles/how-to-let-the-live-preview-work-for-you/" class="hyperlink hyperlink-medium palette-build"><span>Previewing forms</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
