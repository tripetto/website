---
base: ../
---

<section class="form-builder-intro block-first">
  <div class="container">
    <div class="row">
      <div class="col-12 col-sm-10 col-md-7 col-xl-6">
        <h1>Map it out on the magnetic storyboard.</h1>
        <p>The magnetic storyboard is perfect for <strong>building forms like flowcharts</strong>, and makes designing conversational flows clean, fast and fun. No-code!</p>
      </div>
      <div class="col-12 form-builder-intro-tutorial-small">
        <a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/"><img src="{{ page.base }}images/form-builder/tutorial-small.webp" width="1072" height="360" alt="Visual for video tutorial 'How to build a simple form'" loading="lazy"></a>
      </div>
    </div>
  </div>
  <a href="{{ page.base }}help/articles/how-to-build-your-forms-in-the-form-builder/" class="form-builder-intro-tutorial-large" title="Watch tutorial 'How to use the form builder'"></a>
</section>
