---
base: ../
---

<section class="form-builder-blocks content">
  <div class="container" id="blocks">
    <div class="row form-builder-blocks-intro">
      <div class="col-12 col-sm-11 col-md-9 col-lg-8 col-xl-7">
        <h2>Versatile blocks</h2>
        <p>Tripetto offers <strong>every question type you need</strong> to build advanced forms, surveys, quizzes and more. Including real-time, <strong>automatic actions</strong>.</p>
      </div>
    </div>
    <div class="row form-builder-blocks-columns">
      <div class="col-xl-11">
        <div class="row">
          <div class="col-md-6 form-builder-blocks-column form-builder-blocks-questions">
            <div>
              <div>
                <div>{% include icon-chapter.html chapter='build' size='small-medium' radius='medium' name='Question Blocks' %}</div>
                <div class="form-builder-blocks-title">
                  <h3>Questions</h3>
                </div>
              </div>
              <p class="content-explanation">Ask respondents anything with the <strong>complete range of question types</strong>. Every question type comes with advanced, optional settings and logic to get the most out of the interaction.</p>
            </div>
            <a href="{{ page.base }}question-types/#questions" class="hyperlink hyperlink-medium palette-build"><span>Browse question types</span><i class="fas fa-arrow-right"></i></a>
          </div>
          <div class="col-md-6 form-builder-blocks-column form-builder-blocks-actions">
            <div>
              <div>
                <div>{% include icon-chapter.html chapter='logic' size='small-medium' radius='medium' name='Action Blocks' %}</div>
                <div class="form-builder-blocks-title">
                  <h3>Actions</h3>
                </div>
              </div>
              <p class="content-explanation">Use action blocks inside your form’s flow for even smarter performance with <strong>instant actions and calculations</strong> based on supplied respondent inputs. All this without any code.</p>
            </div>
            <a href="{{ page.base }}question-types/#actions" class="hyperlink hyperlink-medium palette-logic"><span>Browse action blocks</span><i class="fas fa-arrow-right"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
