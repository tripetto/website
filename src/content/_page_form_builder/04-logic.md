---
base: ../
articles: [smart-forms,logic-branch,logic-skip,logic-piping]
---

<section class="form-builder-logic">
  <div class="container container-intro content" id="logic">
    <div class="row">
      <div class="col-md-9 col-lg-8">
        <h2>Advanced logic</h2>
        <p>Tripetto has all the form logic features you need to get conversational with respondents, and effectively <strong>increase completion rates and reduce drop-offs</strong>.</p>
      </div>
    </div>
  </div>
  <div class="container container-help">
    <div class="row">
      <div class="col-12">
        <ul class="landing-help-list landing-help-list-horizontal landing-help-list-horizontal-4">
          {% for article in page.articles %}
          {% assign article_item = site.articles_help | find: "article_id", article %}
            {% include landing-help-article.html url=article_item.url title=article_item.article_title description=article_item.description category_id=article_item.category_id %}
          {% endfor %}
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <hr/>
      </div>
    </div>
  </div>
  <div class="container-fluid ticker-holder ticker-logic">
    <div class="row">
      <ul class="ticker-blocks ticker-blocks-logic-basic">
        {% assign blocks_logic = site.data.blocks-logic | where_exp: "item", "item.group == 'basic'" %}
        {% for item in blocks_logic %}
        <li>
          <a href="{{ page.base | append: item.url }}" target="_blank" class="palette-build">
          {% assign icon_url = "icons/" | append: item.icon %}
            {% include {{ icon_url }}.html %}
            <span>{{ item.name }}</span>
          </a>
        </li>
        {% endfor %}
      </ul>
      <ul class="ticker-blocks ticker-blocks-logic-action">
        {% assign blocks_logic = site.data.blocks-logic | where_exp: "item", "item.group == 'advanced'" %}
        {% for item in blocks_logic %}
        <li>
          <a href="{{ page.base | append: item.url }}" target="_blank" class="palette-logic">
          {% assign icon_url = "icons/" | append: item.icon %}
            {% include {{ icon_url }}.html %}
            <span>{{ item.name }}</span>
          </a>
        </li>
        {% endfor %}
      </ul>
    </div>
  </div>
  <div class="container container-content">
    <div class="row">
      <div class="col-md-4 form-builder-logic-column">
        <p class="content-explanation">Ask all the right questions all the time by <strong>funneling respondents</strong> only where their own input sends them.</p>
        <a href="{{ page.base }}help/articles/discover-the-power-of-branches-for-your-logic/" class="hyperlink hyperlink-medium palette-logic"><span>Branch logic</span><i class="fas fa-arrow-right"></i></a>
      </div>
      <div class="col-md-4 form-builder-logic-column">
        <p class="content-explanation">Completely personalize follow-up questions by <strong>smartly recalling preceding respondent inputs</strong>.</p>
        <a href="{{ page.base }}help/articles/how-to-show-respondents-answers-in-your-form-using-piping-logic/" class="hyperlink hyperlink-medium palette-logic"><span>Pipe logic</span><i class="fas fa-arrow-right"></i></a>
      </div>
      <div class="col-md-4 form-builder-logic-column">
        <p class="content-explanation">Keep things appealing by skipping unnecessary steps to <strong>prevent drop-off and increase completion</strong>.</p>
        <a href="{{ page.base }}help/articles/how-to-skip-questions-or-parts-of-your-form/" class="hyperlink hyperlink-medium palette-logic"><span>Jump logic</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>

