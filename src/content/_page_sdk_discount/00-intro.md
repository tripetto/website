---
base: ../../
---

<section class="sdk-discount-intro block-first sdk-intro">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-md-12 col-lg-10 col-xl-8">
        <h1>Get 25% off your SDK license.</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-9 col-md-7 col-xl-6">
        <p>Register for a discount coupon below by entering your email address. We’ll send you the coupon immediately. <strong>It can be activated up to 3 months later.</strong></p>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
