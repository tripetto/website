---
base: ../
videos: [logic-branch-logic,logic-branch-conditions]
articles: [logic-branch,logic-branch-conditions,logic-branch-given-answers,logic-branch-repeat-options]
---

<section class="logic-branch">
  <div class="container" id="branch-logic">
    <div class="row">
      <div class="col-md-6 logic-branch-intro content">
        <h2>Branch Logic</h2>
        <p>Ask all the right questions all the time by <strong>funneling respondents only where their own previous inputs send them</strong>. Use the various so-called culling modes to present the perfect follow-up questions, tracks, and even loops.</p>
        <ul class="help-videos">
          {% for video in page.videos %}
          {% assign video_item = site.data.help-videos[video] %}
          <li><a href="{{ page.base}}{{ video_item.url }}"><img src="{{ page.base }}images/help-videos-thumbnails/{{ video_item.thumbnail }}" width="256" height="144" alt="Visual thumbnail for the video '{{ video_item.name }}'" /></a></li>
          {% endfor %}
        </ul>
        <small>Also known as logic, logic flows, question logic and/or routing.</small>
      </div>
      <div class="col-md-6 logic-branch-example">
        <div class="logic-example">
          <h3>Example</h3>
          <p>Let's say you're running a sport shop that wants to gather some customer feedback about their products. In such a case you could start with asking if your respondent has bought anything at all. If not, you could ask why not. And, in case they did make a purchase, the next step is to determine which product(s) were purchased. And after that, for each purchased product you can loop through a set of follow-up questions to get the right feedback.</p>
          <ul class="hyperlinks-narrow">
            <li><a href="https://tripetto.app/run/H07HLCMNN8" target="_blank" rel="noopener noreferrer" class="hyperlink hyperlink-small palette-logic"><span>Run this example</span><i class="fas fa-arrow-right"></i></a></li>
            <li><a href="https://tripetto.app/template/8009NFU8P1" target="_blank" rel="noopener noreferrer" class="hyperlink hyperlink-small palette-logic"><span>View this in the builder</span><i class="fas fa-arrow-right"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 logic-branch-help-articles">
        <ul class="landing-help-list landing-help-list-horizontal landing-help-list-horizontal-4">
          {% for article in page.articles %}
          {% assign article_item = site.articles_help | find: "article_id", article %}
            {% include landing-help-article.html url=article_item.url title=article_item.article_title description=article_item.description category_id=article_item.category_id %}
          {% endfor %}
        </ul>
      </div>
    </div>
  </div>
</section>
