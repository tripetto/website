---
base: ../
videos: [logic-branch-logic,logic-branch-endings]
articles: [logic-branch,logic-branch-endings,logic-skip,closings]
---

<section class="logic-jump">
  <div class="container" id="jump-logic">
    <div class="row logic-jump-intro">
      <div class="col-12 col-md-11 col-lg-10 content">
        <h2>Jump Logic</h2>
        <p>Keep things as appealing as possible by <strong>letting respondents automatically skip unnecessary questions based on their inputs</strong> as a way to prevent drop-offs and increase completion rates.</p>
        <small>Also known as logic, logic jumps and/or routing.</small>
      </div>
    </div>
    <div class="row">
      <div class="col-12 logic-jump-help-articles">
        <ul class="landing-help-list landing-help-list-horizontal landing-help-list-horizontal-4">
          {% for article in page.articles %}
          {% assign article_item = site.articles_help | find: "article_id", article %}
            {% include landing-help-article.html url=article_item.url title=article_item.article_title description=article_item.description category_id=article_item.category_id %}
          {% endfor %}
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-12 logic-jump-example">
        <hr />
        <div class="logic-example">
          <h3>Example</h3>
          <p>In this use case we have a population screening regarding healthcare. We only want quality data, so we start with filtering respondents that didn't get any healthcare during the past year. We let that group jump to the end of the form immediately. And for all other respondents we include the desired questions, but also create some jumps to the right locations in the form; in this case based on the type of healthcare.</p>
          <ul class="hyperlinks">
            <li><a href="https://tripetto.app/run/CECNCUJNQ5" target="_blank" rel="noopener noreferrer" class="hyperlink hyperlink-small palette-logic"><span>Run this example</span><i class="fas fa-arrow-right"></i></a></li>
            <li><a href="https://tripetto.app/template/T9WFCOD2OE" target="_blank" rel="noopener noreferrer" class="hyperlink hyperlink-small palette-logic"><span>View this in the builder</span><i class="fas fa-arrow-right"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="row logic-jump-help-videos">
      <div class="col-12 col-md-6">
        <ul class="help-videos">
          {% for video in page.videos %}
          {% assign video_item = site.data.help-videos[video] %}
          <li><a href="{{ page.base}}{{ video_item.url }}"><img src="{{ page.base }}images/help-videos-thumbnails/{{ video_item.thumbnail }}" width="256" height="144" alt="Visual thumbnail for the video '{{ video_item.name }}'" /></a></li>
          {% endfor %}
        </ul>
      </div>
    </div>
  </div>
</section>
