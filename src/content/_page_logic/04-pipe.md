---
base: ../
videos: [logic-piping,logic-branch-behaviors]
articles: [logic-piping,logic-branch-repeat-options,logic-branch-repeat-number]
---

<section class="logic-pipe">
  <div class="container" id="pipe-logic">
    <div class="row">
      <div class="col-md-6 order-md-last logic-pipe-intro content">
        <h2>Pipe Logic</h2>
        <p>Completely personalize follow-up questions, tracks, and loops by <strong>smartly recalling and presenting preceding respondent inputs any time</strong> and anywhere you need them.</p>
        <ul class="help-videos">
          {% for video in page.videos %}
          {% assign video_item = site.data.help-videos[video] %}
          <li><a href="{{ page.base}}{{ video_item.url }}"><img src="{{ page.base }}images/help-videos-thumbnails/{{ video_item.thumbnail }}" width="256" height="144" alt="Visual thumbnail for the video '{{ video_item.name }}'" /></a></li>
          {% endfor %}
        </ul>
        <small>Also known as piping, question piping, answer piping, recall information and/or merge codes.</small>
      </div>
      <div class="col-md-6 logic-pipe-example">
        <div class="logic-example">
          <h3>Example</h3>
          <p>Imagine you're on IT support for your company. Employees can ask for support with a form that's focused on personal help. The form could start with gathering some personal information, so we know who’s asking for support. And we could use that input to give a short summary inside the form on the go. Next, the applicant can select multiple problems and for each problem point out what's wrong. We use the name of each selected problem in the follow-up.</p>
          <ul class="hyperlinks-narrow">
            <li><a href="https://tripetto.app/run/6U6G2HXTZ1" target="_blank" rel="noopener noreferrer" class="hyperlink hyperlink-small palette-logic"><span>Run this example</span><i class="fas fa-arrow-right"></i></a></li>
            <li><a href="https://tripetto.app/template/3HS5LJJ3T4" target="_blank" rel="noopener noreferrer" class="hyperlink hyperlink-small palette-logic"><span>View this in the builder</span><i class="fas fa-arrow-right"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12 logic-pipe-help-articles">
        <ul class="landing-help-list landing-help-list-horizontal landing-help-list-horizontal-4">
          {% for article in page.articles %}
          {% assign article_item = site.articles_help | find: "article_id", article %}
            {% include landing-help-article.html url=article_item.url title=article_item.article_title description=article_item.description category_id=article_item.category_id %}
          {% endfor %}
        </ul>
      </div>
    </div>
  </div>
</section>
