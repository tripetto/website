---
base: ../
---

<section class="logic-intro block-first">
  <div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-10 col-lg-8">
        <h1>Boost your form completions with all the right logic.</h1>
      </div>
      <div class="col-sm-9 col-md-7 col-lg-6">
        <p><strong>Apply advanced logic to your form flows</strong> visually to make them smarter and only ask your audience the right questions for optimal response.</p>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
