---
base: ../
---

<section class="landing-overview">
  <div class="container">
    <div class="row landing-overview-intro">
      <div class="col-12">
        <small>Get personal and stay on point with respondents.</small>
        <h2>It’s the logic, stupid.</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-12 landing-overview-anchors">
        <ul class="tiles-text tiles-text-medium tiles-text-anchor">
          <li data-anchor="#branch-logic">
            <div>
              {% include icon-paragraph.html chapter='logic' paragraph='branch' name='Branch Logic' folder='logic' %}
            </div>
            <div>
              <h3>Branch<span> Logic</span></h3>
              <p><strong>Funnel respondents</strong> into the right follow-up flow.</p>
            </div>
          </li>
          <li data-anchor="#jump-logic">
            <div>
              {% include icon-paragraph.html chapter='logic' paragraph='jump' name='Jump Logic' folder='logic' %}
            </div>
            <div>
              <h3>Jump<span> Logic</span></h3>
              <p>Let respondents <strong>skip unnecessary questions</strong>.</p>
            </div>
          </li>
          <li data-anchor="#pipe-logic">
            <div>
              {% include icon-paragraph.html chapter='logic' paragraph='pipe' name='Pipe Logic' folder='logic' %}
            </div>
            <div>
              <h3>Pipe<span> Logic</span></h3>
              <p><strong>Reuse prior answers</strong> to personalize follow-ups.</p>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
