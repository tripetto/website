---
base: ../../
---

<nav class="breadcrumb-navigation block-first" aria-label="breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col">
        <ol>
          <li><a href="{{ page.base }}wordpress/">Tripetto WordPress plugin</a></li>
          <li><span>Blog</span></li>
        </ol>
      </div>
    </div>
  </div>
</nav>

<section class="blog-intro intro intro-breadcrumb">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-xl-8 shape-before">
        <h1>Plugin Blog</h1>
        <p>Read guides, comparisons, tips, and much more about form building with WordPress in general, and our <strong>powerful Tripetto WordPress plugin</strong> in particular.</p>
        <a href="{{ page.base }}subscribe/" class="hyperlink"><span>Subscribe to product news</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
