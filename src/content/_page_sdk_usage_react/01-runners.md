---
base: ../../
---

<section class="sdk-usage-runners">
  <div class="container">
    <div class="row content">
      <div class="col">
        <h2 class="palette-sdk-runners">Use the React component <span>to run forms in your app.</span></h2>
      </div>
    </div>
    <div class="row content">
      <div class="col-lg-11">
        <p>Create a stunning form in the free online <a href="{{ site.url_app }}" target="_blank">Tripetto studio</a> or <a href="#builder" class="anchor">integrated form builder</a> and implement it in your React app in no time. <strong>The form will run entirely inside your React project</strong> - with the style and UX of your choice, and without any dependencies on external infrastructure.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-12 sdk-carousel-runners-buttons">
        <ul class="carousel-buttons">
          <li class="palette-autoscroll active" data-target="#carouselSDKRunners" data-slide-to="0">
            <div>
              {% include icon-face.html face='autoscroll' size='small' name='Autoscroll Form Face' template='sdk' radius='medium' %}
            </div>
            <div>
              <h3>Autoscroll<span> Face</span></h3>
              <p>Fluently presents <strong>one question at a time</strong>.</p>
            </div>
          </li>
          <li class="palette-chat" data-target="#carouselSDKRunners" data-slide-to="1">
            <div>
              {% include icon-face.html face='chat' size='small' name='Chat Form Face' template='sdk' radius='medium' %}
            </div>
            <div>
              <h3>Chat<span> Face</span></h3>
              <p>Presents all <strong>questions and answers as a chat</strong>.</p>
            </div>
          </li>
          <li class="palette-classic" data-target="#carouselSDKRunners" data-slide-to="2">
            <div>
              {% include icon-face.html face='classic' size='small' name='Classic Form Face' template='sdk' radius='medium' %}
            </div>
            <div>
              <h3>Classic<span> Face</span></h3>
              <p>Presents question fields in a <strong>traditional format</strong>.</p>
            </div>
          </li>
        </ul>
      </div>
    </div>
    <div class="row carousel-slides">
      <div class="col">
        <div id="carouselSDKRunners" class="carousel slide">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="row">
                <div class="col-12">
                  <div class="sdk-device-code sdk-device-code-right">
                    <img src="{{ page.base }}images/sdk-scenes/runner-autoscroll.webp" width="2220" height="1435" alt="Screenshot of a fitness registration form in the autoscroll form face, shown on a tablet and a mobile phone." class="sdk-device-code-block sdk-device-code-device" loading="lazy" />
                    <div class="sdk-code-snippet sdk-device-code-block sdk-device-code-snippet">
                      <ul class="nav nav-tabs nav-fill sdk-code-snippet-nav sdk-code-snippet-nav-single" role="tablist">
                        <li class="nav-item"><a class="nav-link active" id="nav-runner-autoscroll-react" data-toggle="tab" href="#runner-autoscroll-react" role="tab" aria-controls="runner-autoscroll-react" aria-selected="true" title="Implement the autoscroll runner with React">{% include icons/react.html %}</a></li>
                      </ul>
                      <div class="tab-content sdk-code-snippet-content">
                        <div class="tab-pane fade show active" id="runner-autoscroll-react" role="tabpanel" aria-labelledby="nav-runner-autoscroll-react">
                        {% include sdk-code-snippet-pane.html active=true code="runners-react.html" face="autoscroll" face-name="Autoscroll" language="jsx" palette="runners" url-run="https://codesandbox.io/s/tripetto-sdk-runner-autoscroll-react-handling-response-data-mvj4n?file=/src/index.js" url-docs="https://tripetto.com/sdk/docs/runner/stock/quickstart/react/" %}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="row">
                <div class="col-12">
                  <div class="sdk-device-code sdk-device-code-right">
                    <img src="{{ page.base }}images/sdk-scenes/runner-chat.webp" width="2220" height="1435" alt="Screenshot of a fitness registration form in the chat form face, shown on a tablet and a mobile phone." class="sdk-device-code-block sdk-device-code-device" loading="lazy" />
                    <div class="sdk-code-snippet sdk-device-code-block sdk-device-code-snippet">
                      <ul class="nav nav-tabs nav-fill sdk-code-snippet-nav sdk-code-snippet-nav-single" role="tablist">
                        <li class="nav-item"><a class="nav-link active" id="nav-runner-chat-react" data-toggle="tab" href="#runner-chat-react" role="tab" aria-controls="runner-chat-react" aria-selected="true" title="Implement the chat runner with React">{% include icons/react.html %}</a></li>
                      </ul>
                      <div class="tab-content sdk-code-snippet-content">
                        <div class="tab-pane fade show active" id="runner-chat-react" role="tabpanel" aria-labelledby="nav-runner-chat-react">
                        {% include sdk-code-snippet-pane.html active=false code="runners-react.html" face="chat" face-name="Chat" language="jsx" palette="runners" url-run="https://codesandbox.io/s/tripetto-sdk-runner-chat-react-handling-response-data-1xq9h?file=/src/index.js" url-docs="https://tripetto.com/sdk/docs/runner/stock/quickstart/react/" %}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="row">
                <div class="col-12">
                  <div class="sdk-device-code sdk-device-code-right">
                    <img src="{{ page.base }}images/sdk-scenes/runner-classic.webp" width="2220" height="1435" alt="Screenshot of a fitness registration form in the classic form face, shown on a tablet and a mobile phone." class="sdk-device-code-block sdk-device-code-device" loading="lazy" />
                    <div class="sdk-code-snippet sdk-device-code-block sdk-device-code-snippet">
                      <ul class="nav nav-tabs nav-fill sdk-code-snippet-nav sdk-code-snippet-nav-single" role="tablist">
                        <li class="nav-item"><a class="nav-link active" id="nav-runner-classic-react" data-toggle="tab" href="#runner-classic-react" role="tab" aria-controls="runner-classic-react" aria-selected="true" title="Implement the classic runner with React">{% include icons/react.html %}</a></li>
                      </ul>
                      <div class="tab-content sdk-code-snippet-content">
                        <div class="tab-pane fade show active" id="runner-classic-react" role="tabpanel" aria-labelledby="nav-runner-classic-react">
                        {% include sdk-code-snippet-pane.html active=false code="runners-react.html" face="classic" face-name="Classic" language="jsx" palette="runners" url-run="https://codesandbox.io/s/tripetto-sdk-runner-classic-react-handling-response-data-u2zyr?file=/src/index.js" url-docs="https://tripetto.com/sdk/docs/runner/stock/quickstart/react/" %}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {% include sdk-features-runners.html %}
  </div>
</section>
