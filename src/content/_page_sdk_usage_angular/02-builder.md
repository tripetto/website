---
base: ../../
---

<section class="sdk-usage-builder" id="builder">
  <div class="container">
    <div class="row content">
      <div class="col">
        <h2 class="palette-sdk-builder">Add the Angular component <span>to build forms in your app.</span></h2>
      </div>
    </div>
    <div class="row content">
      <div class="col">
        <p>Optionally add powerful form building capabilities to your Angular app by neatly integrating the visual form builder in minutes. <strong>The builder will run entirely inside your Angular project</strong> - with custom extensions you develop, and without any dependencies on external infrastructure.</p>
      </div>
    </div>
    <div class="row">
      <div class="col sdk-usage-builder-visual">
        <div class="sdk-device-code sdk-device-code-left">
          <img src="{{ page.base }}images/sdk-scenes/builder.webp" width="2000" height="1441" alt="Screenshot of the form builder." class="sdk-device-code-block sdk-device-code-device" loading="lazy" />
          <div class="sdk-code-snippet sdk-device-code-block sdk-device-code-snippet">
            <ul class="nav nav-tabs nav-fill sdk-code-snippet-nav sdk-code-snippet-nav-single" role="tablist">
              <li class="nav-item"><a class="nav-link active" id="nav-builder-angular" data-toggle="tab" href="#builder-angular" role="tab" aria-controls="builder-angular" aria-selected="true" title="Implement the builder with Angular"><img src="{{ page.base }}images/sdk-usage/angular.png" width="48" height="48" alt="Angular logo" /></a></li>
            </ul>
            <div class="tab-content sdk-code-snippet-content">
              <div class="tab-pane fade show active" id="builder-angular" role="tabpanel" aria-labelledby="nav-builder-angular">
              {% include sdk-code-snippet-pane.html active=true code="builder-angular.html" language="javascript" palette="builder" url-run="https://codesandbox.io/s/tripetto-sdk-builder-angular-loading-blocks-y1ws5" url-docs="https://tripetto.com/sdk/docs/builder/integrate/quickstart/angular/" %}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {% include sdk-features-builder.html %}
  </div>
</section>
