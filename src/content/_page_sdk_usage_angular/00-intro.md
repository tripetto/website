---
base: ../../
---

<section class="sdk-usage-intro block-first sdk-intro">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-lg-9">
        <h1>Fully featured form solution for <span>Angular<span>.</span></span></h1>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-11 col-md-8 col-xl-6">
        <p>The FormBuilder SDK includes easy to use Angular components for building and running advanced forms. Equip Angular apps with a full-fledged form solution. <strong>All in just minutes.</strong></p>
        <ul class="buttons">
          <li><a href="{{ page.base }}sdk/docs/getting-started/usage-with-angular/" class="button button-large button-angular">Open Angular Docs</a></li>
          <li><a href="{{ page.base }}sdk/how-it-works/" class="button button-large button-light">Live Demo</a></li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
