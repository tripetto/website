---
layout: sdk-blog-article
base: ../../../
permalink: /sdk/blog/your-form-is-now-a-mail-agent/
canonical_url: https://tripetto.com/blog/your-form-is-now-a-mail-agent/
title: "Update: Send email block - Tripetto FormBuilder SDK Blog"
description: Introducing a new action block to send emails right from your form. Opening new possibilities for even smarter forms and surveys.
article_title: Your form is now a mail agent
article_slug: "Update: Send email block"
article_folder: 20190725
author: mark
time: 2
category: product
category_name: Product
tags: [product, product-update, feature, editor, blocks, showcase]
areas: [studio,wordpress,sdk]
year: 2019
---
<p>Introducing a new action block to send emails right from your form. Opening new possibilities for even smarter forms and surveys.</p>

<h2>Action blocks?</h2>
<p>Yes, action blocks! They bring something different to the mix because they don't have a visual representation in the form like regular blocks (text input, dropdown, checkbox, etc.) do. They have only one job: Triggering actions. In this case, sending an email.</p>

<h2>Ok, but why would I want to send an email from a form?</h2>
<p>Well, you can now use Tripetto to build a contact form that sends you a message when a form is submitted. And, since you can use multiple action blocks in a single form, you can also send a copy of the form to the sender of the form. Or, what about an <a href="https://tripetto.com/blog/custom-wordpress-registration-form/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=your-form-is-now-a-mail-agent" target="_blank">event registration form</a>? You can automatically send a confirmation message upon registration completion.</p>

<h2>Alright, show me how!</h2>
<p>Let's make a contact form that sends a confirmation to the website owner and a copy of the message to the sender. To do so, we use the new <a href="{{ page.base }}help/articles/how-to-use-the-send-email-block/" target="_blank"><strong>send email</strong></a> block. Below you can see how that looks in Tripetto. Or <a href="https://tripetto.app/template/U5WMASGBHX" target="_blank" rel="noopener noreferrer">click here</a> to open the form directly in the editor.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/mailer-block.png" width="3000" height="1710" alt="Screenshot of a mailer block in Tripetto" loading="lazy" />
  <figcaption><a href="https://tripetto.app/template/U5WMASGBHX" target="_blank" rel="noopener noreferrer">An example contact form that sends a message to the website owner and a copy to the sender</a></figcaption>
</figure>

<h2>That's awesome. I want to use this stuff!</h2>
<p>Just decide how. You can use them in the <a href="https://tripetto.app/" target="_blank" rel="noopener noreferrer">Tripetto studio</a> where you can build forms for free in our online SaaS offering. If you are a WordPress user, simply install the <a href="https://wordpress.org/plugins/tripetto" target="_blank" rel="noopener noreferrer">Tripetto WordPress plugin</a> in your WordPress admin (then you got all the functionality of Tripetto completely integrated into your WordPress instance). Or take it even further and implement Tripetto in your own application using the <a href="https://tripetto.com/sdk/">SDK</a>. It's really up to you. Enjoy!</p>
