---
layout: sdk-blog-article
base: ../../../
permalink: /sdk/blog/product-update-the-often-requested-ranking-block/
canonical_url: https://tripetto.com/blog/product-update-the-often-requested-ranking-block/
title: "Update: Ranking block - Tripetto FormBuilder SDK Blog"
description: The July 2023 update adds the often requested Ranking question type. And we made some important improvements in the Tripetto studio.
article_title: "Product Update: The often requested Ranking block"
article_slug: "Update: Ranking block"
article_folder: 20230622
author: mark
time: 4
category: product
category_name: Product
tags: [product, product-update, feature, editor, blocks]
areas: [studio,wordpress,sdk]
year: 2023
---
<p>The July 2023 update adds the often requested Ranking question type. And we made some important improvements in the Tripetto studio.</p>

<p>Your feedback and ideas are important to us. It gives us the ideal directions to know what we have to work on. This update is fully based on your input, with the introduction of one of the most requested question types and improvements to make some studio bottlenecks a thing of the past.</p>
<hr/>

<h2>The all new Ranking block</h2>
<p>Many of our users have requested a question type to let their respondents compose a ranking from a list of given options. And that's exactly what the all new <a href="{{ page.base }}help/articles/how-to-use-the-ranking-block/" target="_blank">Ranking block</a> does! Let's have a quick look how it looks and what it does!</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/ranking.gif" width="1200" height="760" alt="Screen recording of the ranking block in Tripetto" loading="lazy" />
  <figcaption>The Ranking question type in action.</figcaption>
</figure>
<p>As you can see, the ranking block is a highly interactive block which makes it very easy for your respondents to compose their rankings. You can simply drag-and-drop every item to the desired position, which of course works on desktops and touch devices. Or you can use the position dropdown within each option to quickly move an option to the desired position. The ranking block can work in 2 modes:</p>
<ul>
  <li><strong>Compose a full ranking</strong> - You can let your respondents compose a full ranking of all the options you give them. The complete ranking of all options will be stored for you as the form owner.</li>
  <li><strong>Compose a top ranking</strong> - It's also possible to let your respondents select a certain amount from the options that you give them and let them compose a ranking for their selected options. For example to compose a top-3 from a list of 10 options. Only the ranking of the selected options will be stored for you as the form owner.</li>
</ul>
<p>We're convinced you will love the new ranking block! Check out our Help center for all details:</p>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-ranking-block/">How to use the ranking block</a></li>
</ul>
<hr/>

<h2>Important studio improvements</h2>
<p>Based on your feedback and support requests, there were a few things in the studio that we wanted to address. The following improvements will smash some studio bottlenecks:</p>
<ul>
  <li>
    <p><strong>Added temporary code login</strong> - In some exceptional cases the magic link to login did not work as expected. For those cases, you can now also login with a temporary code. This code is included in the login email that you receive after entering your account's email address. Simply copy-paste the code and you're logged in! More information:</p>
    <ul class="fa-ul related">
      <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-login-to-an-existing-account-in-the-studio/" target="_blank">How to login to an existing account in the studio</a></li>
    </ul>
  </li>
  <li>
    <p><strong>Added password login</strong> - Besides the email login, you can now also login with a password! That was an often heard request, especially when you have multiple studio accounts and often switch between those. Creating a new account still goes via the email route only (for email verification), but once you're logged in, you can now set a password via the Account section in the studio. And from then on you can simply use that password to login. More information:</p>
    <ul class="fa-ul related">
      <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-set-a-password-for-your-account-in-the-studio/" target="_blank">How to set a password for your account in the studio</a></li>
    </ul>
  </li>
  <li>
    <p><strong>Added version conflict detection</strong> - If you had your studio account opened simultaneously in different sessions, there could occur version conflicts. This could lead to missing forms in your workspace, or form structure changes that were overwritten. With the newly added version conflict detection for workspaces and forms this is no longer possible. Your workspaces and forms will now detect different versions and let you reload the latest version first.</p>
  </li>
  <li>
    <p><strong>Improved file uploads usage in webhooks</strong> - Files that were uploaded via the File Upload block in your forms, were only available to the form owner, but not to additional webhook connections. This made it very difficult to use such uploaded files in automation scenarios. From this update on the file uploads are available to webhooks without the logged in studio account, with a maximum of 24 hours. This gives your automation scenarios the chance to process the file uploads, before they get restricted by the studio account within 24 hours after form completion. More information:</p>
    <ul class="fa-ul related">
      <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/use-file-uploads-from-the-studio-in-webhooks/" target="_blank">Use file uploads from the studio in webhooks</a></li>
    </ul>
  </li>
</ul>
<hr/>

<h2>Update now available! 🚀</h2>
<p>This update is available across all our platforms:</p>
<ul>
  <li>In the <strong>Tripetto studio</strong> the ranking block and studio improvements are directly available at <a href="{{ site.url_app }}" target="_blank" rel="noopener noreferrer">tripetto.app</a>.</li>
  <li>In the <strong>Tripetto WordPress plugin</strong> the ranking block becomes available after updating to version 7.0.1 of the plugin in your WP Admin.</li>
  <li>In the <strong>Tripetto FormBuilder SDK</strong> the ranking block is available as a <a href="{{ page.base }}sdk/docs/blocks/stock/ranking/">stock block</a>.</li>
</ul>
