---
layout: sdk-blog-article
base: ../../../
permalink: /sdk/blog/product-update-new-signature-block/
canonical_url: https://tripetto.com/blog/product-update-new-signature-block/
title: "Product Update: New Signature block - Tripetto SDK Blog"
description: The March 2024 update introduces the all new Signature question type. We also made some useful improvements to the Tripetto studio and WordPress plugin.
article_title: "Product Update: Collect signatures with the new Signature block"
article_slug: "Update: Signature block"
article_folder: 20240319
author: mark
time: 3
category: product
category_name: Product
tags: [product, product-update, feature, editor, blocks]
areas: [studio,wordpress,sdk]
year: 2024
---
<p>The March 2024 update introduces the all new Signature question type. We also made some useful improvements to the Tripetto studio and WordPress plugin.</p>

<p>It's update time again! We have heard the widespread wish to be able to collect signatures in forms. So that's exactly what the all new Signature block does. We also included some improvements to the Tripetto studio and WordPress plugin, so let's dive into this update!</p>
<hr/>

<h2>Signature block</h2>
<p>The <a href="{{ page.base }}help/articles/how-to-use-the-signature-block/" target="_blank">Signature block</a> enables you to collect signatures from your respondents inside your form. First a quick demo of how this looks in your form:</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/signature.gif" width="1200" height="760" alt="Screen recording of the signature block in Tripetto" loading="lazy" />
  <figcaption>The Signature question type in action.</figcaption>
</figure>

<h3>How does it work?</h3>
<p>The respondent can draw their signature using all inputs, like <strong>mouse</strong>, <strong>pen</strong> and <strong>touch</strong>. After a brief moment of inactivity the signature gets confirmed and the signing date is shown.</p>

<h3>Which settings are included?</h3>
<p>For you as the form owner, there are some settings to customize the display of the signature block. These settings are available for each individual signature block:</p>
<ul>
  <li>You can set the <strong>color</strong> of the signature ink.</li>
  <li>You can set the <strong>size</strong> of the signature field to small, medium, or large.</li>
</ul>

<h3>How is the signature stored?</h3>
<p>Each signature is stored as an image in your dataset. You can easily preview the signatures in the results, or download a copy of the image.</p>

<h3>More information</h3>
<ul class="fa-ul related">
  <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}help/articles/how-to-use-the-signature-block/" target="_blank">How to use the signature block</a></li>
</ul>
<hr/>

<h2>Improvements</h2>
<p>This update also includes some handy improvements and bugfixes, based on your feedback:</p>
<ul>
  <li>Made domain validation (URL validation) less strict so that local domains, IP addresses and relative paths are now allowed, for example in picture choices and redirects.</li>
  <li>Added the <code>alias</code> of a block to the tracking functionality.</li>
  <li>Added a live preview of uploaded images in the result panel (used for file uploads and signatures).</li>
  <li>Fixed a bug in the email address field of the notification panel.</li>
</ul>
<hr/>

<h2>Update now available! 🚀</h2>
<p>This update is available across all our platforms:</p>
<ul>
  <li>In the <strong>Tripetto studio</strong> the signature block and all improvements are directly available at <a href="{{ site.url_app }}" target="_blank" rel="noopener noreferrer">tripetto.app</a>.</li>
  <li>In the <strong>Tripetto WordPress plugin</strong> the signature block is available in the Pro plugin, after updating it to version 8.0.0. The other improvements are available for everyone after updating the plugin to version 8.0.0.</li>
  <li>In the <strong>Tripetto FormBuilder SDK</strong> the signature block is available as a <a href="{{ page.base }}sdk/docs/blocks/stock/signature/">stock block</a>.</li>
</ul>
