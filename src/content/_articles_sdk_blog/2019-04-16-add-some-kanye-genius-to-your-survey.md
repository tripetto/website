---
layout: sdk-blog-article
base: ../../../
permalink: /sdk/blog/add-some-kanye-genius-to-your-survey/
canonical_url: https://tripetto.com/blog/add-some-kanye-genius-to-your-survey/
title: Integrating an API to Tripetto - Tripetto FormBuilder SDK Blog
description: How to integrate an API in your survey with Tripetto.
article_title: Add some Kanye genius to your survey (tutorial on integrating an API)
article_slug: Integrating an API to Tripetto
article_folder: 20190416
article_image: header.png
article_image_width: 1716
article_image_height: 690
article_image_caption: Tripetto editor and collector side-by-side
author: martin
time: 4
category: coding-tutorial
category_name: Coding tutorial
tags: [coding-tutorial, editor, collector, blocks, feature]
areas: [sdk]
year: 2019
scripts: [prism]
stylesheets: [prism-dark]
---
<p>How to integrate an API in your survey with Tripetto.</p>

<blockquote>
  <h4>❌ This is a deprecated SDK blog article</h4>
  <p>Please note that the SDK concepts and code snippets in this article are about a deprecated version of the Tripetto SDK. In the meantime the SDK has evolved heavingly. For up-to-date SDK documentation, please have a look at the following links:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/">Tripetto FormBuilder SDK website</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/docs/">Tripetto FormBuilder SDK documentation</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/blog/formbuilder-sdk-implementation-guide/">Power Up Your Forms: A Comprehensive Guide to implementing the Tripetto FormBuilder SDK</a></li>
  </ul>
</blockquote>
<h2>Extend your survey with custom question types</h2>
<p>When designing a survey you usually have a number of basic question types to choose from like Number, Dropdown or Star Rating. Some vendors make more complex question types available like <a href="https://help.surveymonkey.com/articles/en_US/kb/Matrix-Question" target="_blank" rel="noopener noreferrer">Matrix</a> or <a href="https://www.typeform.com/help/payment-field-stripe-integration/" target="_blank" rel="noopener noreferrer">Payment</a>.</p>
<p>With <a href="{{ page.base }}sdk/">Tripetto</a> you can build your own question types. This brings endless opportunities to enrich your survey and collect more user data. In this article I’ll demonstrate how easy it is to create a question type that connects to an external API. This example isn’t a question actually, but it shows a random quote from Kanye West any where you need his genius.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/quote.png" width="839" height="137" alt="Screenshot of a Kanye West quote in Tripetto" loading="lazy" />
  <figcaption>Kanye West quote from <a href="https://api.kanye.rest/" target="_blank" rel="noopener noreferrer">https://api.kanye.rest</a></figcaption>
</figure>

<h2>Concept</h2>
<p>Tripetto distinguishes itself with the way it visually presents the flow of a survey in the <a href="{{ page.base }}sdk/docs/builder/introduction/">Editor</a>. When building a custom block (Tripetto uses blocks as a synonym for question types) you have to define how your custom block exposes itself in the editor and which properties you can edit.</p>
<p>Another part of the custom block is how it represents itself in the <a href="{{ page.base }}sdk/docs/runner/introduction/">Collector</a>. The collector is where your respondents file your survey. Tripetto gives you absolute freedom how to display your survey, but there are some <a href="{{ page.base }}sdk/docs/getting-started/try-it/">examples</a> to give you a head start.</p>
<p>This example is based on the Conversational UX demo with React.</p>

<h2>Editor</h2>
<p>To create a custom block in the editor you have to implement the <code>NodeBlock</code> abstract class, prefixed with the <code>@tripetto</code> decorator to supply (meta) information about the block.</p>
<blockquote>
  <h4>🚧 Warning: Deprecated SDK code snippets and packages</h4>
  <p>Please note that the below code snippets and used packages are about a deprecated version of the Tripetto SDK. In the meantime the SDK has evolved heavingly. For up-to-date SDK documentation, please have a look at the following links:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/">Tripetto FormBuilder SDK website</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/docs/">Tripetto FormBuilder SDK documentation</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/blog/formbuilder-sdk-implementation-guide/">Power Up Your Forms: A Comprehensive Guide to implementing the Tripetto FormBuilder SDK</a></li>
  </ul>
</blockquote>
<pre class="line-numbers"><code class="language-typescript">import { NodeBlock, Slots, slots, tripetto } from "tripetto";
import * as ICON from "./kanye.svg";

@tripetto({
    type: "node",
    identifier: "kanye",
    label: "Kanye Quote",
    icon: ICON
})
export class Kanye extends NodeBlock {
    @slots
    defineSlot(): void {
        this.slots.static({
            type: Slots.String,
            reference: "kanye-quote",
            label: "Kanye quote"
        });
    }

    @editor
    defineEditor(): void {
        this.editor.name(false, false, "Name");
        this.editor.visibility();
    }
}
</code></pre>
<p>Slots are a fundamental part of a custom block, because they define how response data is stored. In this example I’ve chosen for a single <a href="{{ page.base }}sdk/docs/blocks/custom/guides/slots/">static</a> slot of type <code>Slots.String</code> where I can store the random quote that was presented to the respondent.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/editor.png" width="635" height="312" alt="Screenshot of the form builder in Tripetto" loading="lazy" />
  <figcaption>Properties of the Kanye block in the Tripetto editor</figcaption>
</figure>

<h2>Collector</h2>
<p>The collector is where the real <i>API magic</i> happens! In the editor we’ve described the way our custom block can be managed. In the collector we’ll describe how it looks and how it works. Since this example is based on React and Bootstrap, this is where these frameworks come together with Tripetto.</p>
<blockquote>
  <h4>🚧 Warning: Deprecated SDK code snippets and packages</h4>
  <p>Please note that the below code snippets and used packages are about a deprecated version of the Tripetto SDK. In the meantime the SDK has evolved heavingly. For up-to-date SDK documentation, please have a look at the following links:</p>
  <ul class="fa-ul related">
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/">Tripetto FormBuilder SDK website</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/docs/">Tripetto FormBuilder SDK documentation</a></li>
    <li><span class="fa-li"><i class="fas fa-arrow-right"></i></span><a href="{{ page.base }}sdk/blog/formbuilder-sdk-implementation-guide/">Power Up Your Forms: A Comprehensive Guide to implementing the Tripetto FormBuilder SDK</a></li>
  </ul>
</blockquote>
<pre class="line-numbers"><code class="language-tsx">import * as React from "react";
import * as Tripetto from "tripetto-collector";
import * as Superagent from "superagent";
import { IBlockRenderer } from "collector/helpers/interfaces/renderer";
import { IBlockHelper } from "collector/helpers/interfaces/helper";
import "./kanye.scss";

const DEFAULT_QUOTE = "I feel like I'm too busy writing history to read it.";
@Tripetto.block({
  type: "node",
  identifier: "kanye"
})

export class KanyeBlock extends Tripetto.NodeBlock implements IBlockRenderer {
  readonly quoteSlot = Tripetto.assert(this.valueOf&lt;string>("kanye-quote"));

  constructor(node: Tripetto.Node, context: Tripetto.Context) {
    super(node, context);
    Superagent
      .get("https://api.kanye.rest")
      .then((response: Superagent.Response) => {
        this.quoteSlot.value = response.body.quote || DEFAULT_QUOTE;
      });
  }

  render(h: IBlockHelper): React.ReactNode {
    return (
      &lt;div tabIndex={h.tabIndex} className="kanye">
        &lt;blockquote>
          &lt;p className="quotation">
            {this.quoteSlot.value || "..."}&lt;/p>
          &lt;footer>
            — Kanye West
          &lt;/footer>
        &lt;/blockquote>
        &lt;div className={h.isActive ? "active" : ""}>
          &lt;button className="btn btn-lg btn-primary" onClick={() => h.next()}>
            Continue
            &lt;i className="fas fa-level-down-alt fa-fw fa-rotate-90 ml-2" />
          &lt;/button>
        &lt;/div>
      &lt;/div>
    );
  }
}
</code></pre>
<p>I’ve used Superagent to retrieve the quote from the API. React is used for updating the quote inside the paragraph tag. You can see that the slot is updated with the quote as soon it is retrieved. This way the quote is added to the response data and therefore stored when the respondent completes the survey. Add some Bootstrap styling and this results in a beautiful quote from Kanye West inside your survey 🕶️.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/collector.png" width="789" height="287" alt="Screenshot of a collector in Tripetto" loading="lazy" />
  <figcaption>Kanye West quote in action in Tripetto collector</figcaption>
</figure>

<h2>Try it yourself</h2>
<p>You can <em>experience</em> the Kanye West block on <a href="https://tripetto.gitlab.io/lab/kanye-west/" target="_blank" rel="noopener noreferrer">GitLab Pages</a>.</p>
<p>You can run this example on your local machine by downloading the source code from <a href="https://gitlab.com/tripetto/lab/kanye-west" target="_blank" rel="noopener noreferrer">https://gitlab.com/tripetto/lab/kanye-west</a>. Run <code>npm install</code> and <code>npm test</code> from your command prompt and you’re up and running on <code>http://localhost:9000</code>.</p>

<h2>Final thoughts</h2>
<p>The Kanye West quote block is obviously meant for demo purposes (unless you are a real Kanye West fan and you want to share his wisdom with your target audience 😎). Imagine what you can do with custom blocks in your survey or even in your application. <a href="https://tripetto.com/sdk/">Tripetto</a> can be fully integrated in your own software and its blocks are fully extendable as seen in this example.</p>
<p>Let me know what custom blocks you expect to create for your surveys.</p>
<p><strong>Enjoy Tripetto!</strong></p>
<p>(And very special thanks to <a href="https://x.com/andrewjazbec" target="_blank" rel="noopener noreferrer">@andrewjazbec</a> for his <a href="https://kanye.rest/" target="_blank" rel="noopener noreferrer">Kanye Rest API</a>!)</p>
