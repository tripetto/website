---
layout: sdk-blog-article
base: ../../../
permalink: /sdk/blog/dont-trust-someone-else-with-your-form-data/
canonical_url: https://tripetto.com/blog/dont-trust-someone-else-with-your-form-data/
title: Self-hosting for data ownership - Tripetto FormBuilder SDK Blog
description: SaaS platforms aren’t as safe as you think for collecting and storing your form and survey data. And they do get breached. Hello GDPR!
article_id: gdpr
article_title: Don’t trust someone else with your form data
article_slug: Self-hosting for data ownership
article_punchline: How self-hosting helps you keeping data ownership.
article_folder: 20190213
article_image: header.webp
article_image_width: 2500
article_image_height: 1379
article_image_caption: Illustrations by Saskia Keultjes
author: owen
time: 5
category: tips-tricks
category_name: Tips & tricks
tags: [tips-tricks, collector, privacy, gdpr, guide]
areas: [studio,wordpress,sdk]
year: 2019
---
<p>How self-hosting helps you keeping data ownership.</p>

<p>As we’ve all shifted to the cloud, the way we think about customer data has changed. Just a few years ago, many of us would have insisted on storing customer data on premise, or using infrastructure you control, but now it’s nothing more than an afterthought.</p>
<p>The advent of simple Software-as-a-Service tools (SaaS), like Typeform and SurveyMonkey, has made it easier to let someone else worry about customer data, in exchange for a great user experience.</p>
<p>In particular, the tools didn’t exist to create complex form or survey flows for the end user. If you needed meaningful form logic in your product, it was likely you’d need to know how to code, or would require custom application development. The SaaS revolution changed that, but it changed how we think about centralization as well.</p>
<p>A dark secret is lurking below, that many organizations are only waking to now: you no longer own your customer data, and you’re left at the mercy of someone else’s data security.</p>

<h2>One big honeypot</h2>
<p>Form data, even if it doesn’t seem that important, is regularly some of the most private information we put online.</p>
<p>We type our names, addresses, social security numbers and much more into forms on the internet, without considering who’s hosting it, and where — but the reality is that it’s now aggregated into monolithic datastores by the big survey players.</p>
<p>It’s simple to see how this is becoming a problem as hackers increasingly target consumer data en-masse: SaaS tools, and hosted platforms, are big honeypots, conveniently centrally located for the taking. It might seem like you’re handing off the responsibility to someone else, but the reality is a little different.</p>
<p>Recently, we saw an example of how bad this can get in reality: Typeform suffered a large data breach in 2018 that saw the company lose private survey data for tens of thousands of the businesses that use the platform.</p>
<p>That seems bad enough in isolation, but what was worse was the fallout for users: hackers gained access to everything from personal details submitted to local city councils, to Reddit’s large-scale user surveys that sometimes included anonymous account information, not linked to someone’s identity until then.</p>
<figure class="inline">
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/breach.webp" width="1500" height="2000" alt="Illustration representing a data breach" loading="lazy" />
</figure>
<p>The scale of the damage to consumers was hard to understand because it was so broad. Typeform dealt with the fallout well by notifying customers, but so much data was centralized that end users that had just filled out forms, like myself, started receiving multiple emails a day from corporations saying they lost control of my data, and there’s nothing they could do.</p>
<p>I had no idea Typeform was breached until I put together the pieces — all of these companies had used the same platform for their collection of my data, and each of them was now reaching out to me, separately, explaining that they had made a mistake.</p>

<h2>GDPR and data ownership</h2>
<p>Until recently, consumers had no recourse when these types of data breaches occurred. Now, however, there could be consequences for both the SaaS provider and the company that uses a tool that’s breached.</p>
<p>The General Data Protection Regulation (GDPR) changed everything in the global privacy landscape in 2018 by putting the onus on the data controller — the company providing a service — to keep data safe, secure and auditable. Additionally, users must be informed whenever data is being shared with another entity, such as Typeform, Google Analytics or any other third-party platform.</p>
<p>GDPR is a boon for consumers, who <a href="https://www.i-scoop.eu/gdprarticle/gdpr-article-82-right-compensation-liability/" target="_blank" rel="noopener noreferrer">now have the right to compensation for data breaches</a>, and may come with additional fines of up to €20 million.</p>
<p>For companies, that’s a big responsibility, and is likely to reverse the trend of centralized stores of data. The cloud is great in many instances, but when it comes to user data, it’s crucial to own the chain of custody and where it’s stored — especially with GDPR in play.</p>
<p>The problem, for years, has been that there simply weren’t that many options in the form building world to replace tools like SurveyMonkey, Typeform and the other centralized platforms. None of these solutions really offer self-hosted options, leaving organizations with particularly <a href="https://tripetto.com/sdk/blog/sensitive-form-data-mistakes/">sensitive data</a> in the lurch, often needing to build their own tools.</p>
<p>With the shifting data ownership expectations, that’s changing. New entrants are emerging, which offer a better option: rich, self-hosted form builders that allow control over both the experience <em>and </em>how data is stored.</p>
<p><a href="https://tripetto.com">Tripetto</a>, for example, offers a modular solution that runs inside your own infrastructure. Drop in the visual form builder for your employees, embedded inside your own application, and use the collector tools to store that data on your own terms and turf. You can drop it into your application, theme the frontend in a similar fashion to Typeform, and take control of your user’s security.</p>
<figure>
  <img src="{{ page.base }}images/blog/{{ page.article_folder }}/saves.webp" width="2500" height="1379" alt="Illustration of a data save" loading="lazy" />
</figure>
<p>Other self-hosted form building tools exist too, such as SurveyJS and LimeSurvey, for slotting into custom-built applications. However these are largely open-source focused and don’t have a business model to support larger companies. And they also tend to lack significant UI flexibility for the respondents’ interface.</p>
<p>Using these types of products instead of cloud-based forms flies in the face of the shift to SaaS tools, but it gives organizations the power to own their data. For security-focused organizations, such as government, healthcare and many others, they’re also able to use modern tooling without building a custom solution of their own.</p>

<h2>Consider your users</h2>
<p>As an internet user, you might be worried by how many (more) data breaches you’ll be caught up in. In the technology industry, data sovereignty was often an afterthought for the companies building products, but now it’s top of mind.</p>
<p>For those collecting user data, creating surveys, and building forms, it’s important to ask yourself: by centralizing this data, am I putting my users at a risk that I wouldn’t take myself? Typeform’s big breach, in my mind, proves the danger of centralization at scale, and it’s something product companies must consider a business risk.</p>
<p>You’re probably already self-hosting your code, using on-premise GitHub, or GitLab, if you’re reading this. It’s the life-blood of your company, right? Now it’s time to give your users’ data the same level of care, and build tools that better protect them from security breaches.</p>
<p>That starts with the humble form, and a change in mindset about where data lives long-term.</p>
