---
layout: sdk-blog-article
base: ../../../
permalink: /sdk/blog/formbuilder-sdk-implementation-guide/
canonical_url: https://tripetto.com/blog/formbuilder-sdk-implementation-guide/
title: FormBuilder SDK Implementation Guide - Tripetto FormBuilder SDK Blog
description: Tripetto’s FormBuilder SDK lets you integrate Tripetto’s form building powers into your own projects using React, Angular, JavaScript, and/or HTML.
article_title: "Power Up Your Forms: A Comprehensive Guide to implementing the Tripetto FormBuilder SDK"
article_slug: FormBuilder SDK Implementation Guide
author: mark
time: 7
category: guide
category_name: Guide
tags: [guide]
areas: [sdk]
year: 2024
---
<p>Tripetto’s FormBuilder SDK lets you integrate Tripetto’s form building powers into your own projects using React, Angular, JavaScript, and/or HTML. This guide will teach you the basics of these implementations.</p>

<p>Forms are the backbone of digital interactions, serving as a vital source for user input, feedback, and data collection across various platforms. There are many solutions out there to help you with this. Among them are very large players that have dominated the form world for decades. Most of them are SaaS oriented, like <a href="https://www.typeform.com/" target="_blank" rel="noopener noreferrer">Typeform</a> and <a href="https://www.surveymonkey.com/" target="_blank" rel="noopener noreferrer">SurveyMonkey</a>.</p>
<p>Such tools are ideal to quickly build a form and distribute it to your audience. An often-forgotten element though is the fact that everything you do with the form relies on the infrastructure of the providing SaaS company. That’s usually sufficient for simple forms without any sensitive data collected, but becomes a risk when you use forms as a main part of your <a href="https://tripetto.com/sdk/blog/enterprise-form-builder/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide">business</a> and <a href="https://tripetto.com/sdk/blog/sensitive-form-data-mistakes/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide">collect sensitive data</a>, which then gets stored on third-party infrastructure.</p>
<p>The old-fashioned form tools are typically designed as SaaS and thus (somewhat) limit your control over form implementation and data storage. In cases where these are important, you’d ideally have control over how you distribute your forms and where the <a href="https://tripetto.com/hosting-freedom/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide" target="_blank">sensitive data is stored</a>. And that’s where Tripetto’s FormBuilder SDK proves its value over the more traditional form tools.</p>
<p>In this comprehensive guide, we'll delve into the world of the Tripetto FormBuilder SDK and explore its implementation across different frameworks and languages like React, Angular, JavaScript, and HTML.</p>
<hr/>

<h2>What is Tripetto?</h2>
<p>Tripetto offers a form tool for various usages. It’s available <a href="https://tripetto.com/studio/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide" target="_blank">as a SaaS</a>, just like Typeform and SurveyMonkey. In many cases that’s a perfect solution to easily interact with your audience through a user-friendly form experience.</p>
<p>That same solution is also available as a <a href="https://tripetto.com/wordpress/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide" target="_blank">WordPress plugin</a>. With that, WordPress admins can implement the full Tripetto experience inside their own WordPress site. This already is a first step to get more control over the implementation and form data, as <a href="https://tripetto.com/blog/wordpress-forms-to-database/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide" target="_blank">everything runs entirely inside the WordPress environment</a>, without any connections to Tripetto infrastructure.</p>
<p>Underneath both offerings is a core set of in-house developed, largely <a href="https://tripetto.com/sdk/components/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide">open-source components</a>. That way we can offer both our end-user propositions (SaaS and WordPress plugin) with the same shared feature set. We’ll have a closer look at those features later.</p>
<p>The thing that makes Tripetto different from the old-school form solutions, is that we even offer these <a href="https://tripetto.com/sdk/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide">core components to developers</a>, too. In fact, we use the exact same components ourselves to develop the Tripetto SaaS and WordPress offerings. With them developers can unleash Tripetto in its full force inside their own applications.</p>

<h3>Tripetto form features</h3>
<p>Now that we know that Tripetto comes in different offerings with the same core features, let’s have a look at those features. Regardless of which offering you choose, the end-user features are always important of course.</p>
<p>In general, Tripetto is a form tool solution to build forms and collect data. It includes the basic features that any form tool will have, but also some unique functionalities:</p>
<ul>
  <li>Tripetto uses a <a href="https://tripetto.com/magnetic-form-builder/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide" target="_blank">drag-and-drop form builder</a> to create forms, with a live preview that lets you test out your logic and see how the form will look on the front end. You’re able to set up a form in minutes.</li>
  <li>Tripetto includes all of the typical <a href="https://tripetto.com/question-types/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide" target="_blank">question blocks</a> out of the box. Think of simple text inputs, checkboxes, and radio buttons. But also the more advanced question types like ratings, rankings and picture choices.</li>
  <li>Besides question types to collect data, Tripetto also includes so-called <a href="https://tripetto.com/question-types/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide" target="_blank">action blocks</a>. Such blocks can perform actions in the background. The most advanced example of that is the <a href="https://tripetto.com/calculator/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide" target="_blank">calculator block</a>, which can perform real-time calculations. Think of calculating a score based on given answers. Or performing advanced formulas.</li>
  <li>Tripetto focusses on logic more than any other form tool. You can use Tripetto’s <a href="https://tripetto.com/logic-types/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide" target="_blank">conditional logic</a> to guide the respondent through the form and react to previous answers. The drag-and-drop form builder helps you build such smart logic structures, because you build forms like flowcharts.</li>
  <li>Another unique feature of Tripetto is the way you present your form. Unlike other form tools, you can choose from 3 different <a href="https://tripetto.com/form-layouts/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide" target="_blank">form layouts</a>, each designed for a different type of interaction:
    <ul>
      <li>Autoscroll form layout to fluently present one question at a time.</li>
      <li>Chat form layout to present all questions and answers as a chat.</li>
      <li>Classic form layout to present question fields in a traditional format with multiple questions at a time.</li>
    </ul>
  </li>
</ul>

<h3>Understanding Tripetto FormBuilder SDK</h3>
<p>All of the above features are available in our end-user propositions, but also in our <a href="https://tripetto.com/sdk/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide">FormBuilder SDK</a>. In fact, the SDK is the actual workhorse behind it all.</p>
<p>If you’re looking for a form solution inside your own software, it would take years of development to do this in a robust way. You would have to develop a form builder, form layouts, form logic, etc. By using the FormBuilder SDK you instantly take advantage of all the heavy development work we did for you. In most cases within just minutes, as you can also see in our own <a href="https://tripetto.com/sdk/blog/making-of-sdk-demo/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide">SDK demo</a>.</p>
<p>The FormBuilder SDK contains <a href="https://tripetto.com/sdk/components/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide">three core components</a>:</p>

<p>1 - <strong>Form Builder</strong>, which is the <a href="https://tripetto.com/sdk/docs/builder/introduction/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide">drag-and-drop form builder</a> with the storyboard interface to build forms inside your own software. You can implement a real-time preview with that as well.</p>
<figure>
  <img src="{{ page.base }}images/sdk-scenes/builder.webp" width="2000" height="1441" alt="Form builder component" class="no-shadow" loading="lazy" />
  <figcaption>The form builder component included in the Tripetto FormBuilder SDK.</figcaption>
</figure>

<p>2 - <strong>Form Runner</strong>, which is a finite state machine that handles the complex logic and response collection during form execution. The 3 form layouts (<a href="https://tripetto.com/sdk/docs/runner/stock/introduction/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide">stock runners</a>) are ready to go straight out of the box. On top of that it’s even possible to develop/design your own <a href="https://tripetto.com/sdk/docs/runner/custom/introduction/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide">custom runner</a>.</p>
<figure>
  <img src="{{ page.base }}images/sdk-scenes/runner.webp" width="2000" height="1441" alt="Form runner component" class="no-shadow" loading="lazy" />
  <figcaption>The form runner component included in the Tripetto FormBuilder SDK.</figcaption>
</figure>

<p>3 - <strong>Blocks</strong>, which are the blocks (i.e. question types) that are used in both the form builder and the form runners to build your form. All <a href="https://tripetto.com/sdk/docs/blocks/stock/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide">stock blocks</a>, containing all question blocks and action blocks, can be used right away. If you need any other question type or action in your form, you can develop those yourself with <a href="https://tripetto.com/sdk/docs/blocks/custom/introduction/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide">custom blocks</a>.</p>
<figure>
  <img src="{{ page.base }}images/sdk-scenes/blocks.webp" width="2000" height="1441" alt="Form blocks component" class="no-shadow" loading="lazy" />
  <figcaption>The form blocks component included in the Tripetto FormBuilder SDK.</figcaption>
</figure>

<p>Depending on your exact needs for your form implementation, you can implement one or more of the SDK’s components. For example, if you only need to run forms in your application and one of the stock runners and our stock question blocks are sufficient, you only need to implement the form runner component. On the other hand, if you need a custom runner with custom blocks, you can implement all of the SDK components and customize them just the way you want and need.</p>
<hr/>

<h2>Implementing FormBuilder SDK</h2>
<p>The FormBuilder SDK is tailored to be easily integrated in different modern frameworks, like React, Angular, and JavaScript. Depending on the framework that your application is built in, you can choose the implementation of your need.</p>
<p>Let’s have a quick look at some different framework implementations.</p>

<h3>React</h3>
<p>For usage in <a href="https://react.dev/" target="_blank" rel="noopener noreferrer">React</a> apps there’s a dedicated React component available for both the form builder and the form runners. These are the steps to take:</p>
<ol>
  <li>Add the required packages to your project;</li>
  <li>Import the React component;</li>
  <li>Use the React component right away.</li>
</ol>
<p>Please visit the following links for more information about React implementation:</p>
<div>
  <a href="https://tripetto.com/sdk/react/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide" class="blocklink">
    <div>
      <span class="title">Using Tripetto FormBuilder SDK with React</span>
      <span class="description">The Tripetto FormBuilder SDK includes easy to use React components to equip React web and native apps with a full-fledged form solution.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/sdk-usage/react.svg" width="160" height="160" alt="React logo" />
    </div>
  </a>
</div>
<div>
  <a href="https://tripetto.com/sdk/docs/getting-started/usage-with-react/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide" class="blocklink">
    <div>
      <span class="title">React Documentation</span>
      <span class="description">Learn how to quickly start implementing the Tripetto FormBuilder SDK with React.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/sdk-usage/react.svg" width="160" height="160" alt="React logo" />
    </div>
  </a>
</div>

<h3>Angular</h3>
<p>For usage in <a href="https://angular.io/" target="_blank" rel="noopener noreferrer">Angular</a> apps there’s a dedicated Angular component available for both the form builder and the form runners. These are the steps to take:</p>
<ol>
  <li>Add the required packages to your project;</li>
  <li>Import the Angular component;</li>
  <li>Use the Angular component right away.</li>
</ol>
<p>Please visit the following links for more information about Angular implementation:</p>
<div>
  <a href="https://tripetto.com/sdk/angular/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide" class="blocklink">
    <div>
      <span class="title">Using Tripetto FormBuilder SDK with Angular</span>
      <span class="description">The Tripetto FormBuilder SDK includes easy to use Angular components to equip Angular apps with a full-fledged form solution.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/sdk-usage/angular.svg" width="160" height="160" alt="Angular logo" />
    </div>
  </a>
</div>
<div>
  <a href="https://tripetto.com/sdk/docs/getting-started/usage-with-angular/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide" class="blocklink">
    <div>
      <span class="title">Angular Documentation</span>
      <span class="description">Learn how to quickly start implementing the Tripetto FormBuilder SDK with Angular.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/sdk-usage/angular.svg" width="160" height="160" alt="Angular logo" />
    </div>
  </a>
</div>

<h3>Plain JavaScript</h3>
<p>For usage in plain JavaScript apps/sites there’s a JavaScript bundle available for both the form builder and the form runners. These are the steps to take:</p>
<ol>
  <li>Add the required packages to your project;</li>
  <li>Import the JavaScript function;</li>
  <li>Use the JavaScript function right away.</li>
</ol>
<p>Please visit the following links for more information about plain JavaScript implementation:</p>
<div>
  <a href="https://tripetto.com/sdk/plain-js/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide" class="blocklink">
    <div>
      <span class="title">Using Tripetto FormBuilder SDK with plain JS</span>
      <span class="description">The Tripetto FormBuilder SDK includes easy to use functions to equip plain JavaScript projects with a full-fledged form solution.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/sdk-usage/plainjs.svg" width="160" height="160" alt="JavaScript logo" />
    </div>
  </a>
</div>
<div>
  <a href="https://tripetto.com/sdk/docs/getting-started/usage-with-plain-js/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide" class="blocklink">
    <div>
      <span class="title">Plain JS Documentation</span>
      <span class="description">Learn how to quickly start implementing the Tripetto FormBuilder SDK with plain JS.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/sdk-usage/plainjs.svg" width="160" height="160" alt="JavaScript logo" />
    </div>
  </a>
</div>

<h3>HTML</h3>
<p>It’s even possible to implement the SDK components in good old HTML. These are the steps to take:</p>
<ol>
  <li>Load the required libraries from a CDN or self-host the libraries;</li>
  <li>Use the function right away.</li>
</ol>
<p>Please visit the following links for more information about HTML implementation:</p>
<div>
  <a href="https://tripetto.com/sdk/html/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide" class="blocklink">
    <div>
      <span class="title">Using Tripetto FormBuilder SDK with HTML</span>
      <span class="description">The Tripetto FormBuilder SDK includes easy to use scripts to equip HTML projects with a full-fledged form solution.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/sdk-usage/html.svg" width="160" height="160" alt="Angular logo" />
    </div>
  </a>
</div>
<div>
  <a href="https://tripetto.com/sdk/docs/getting-started/usage-with-html/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide" class="blocklink">
    <div>
      <span class="title">HTML Documentation</span>
      <span class="description">Learn how to quickly start implementing the Tripetto FormBuilder SDK with HTML.</span>
      <span class="url">tripetto.com</span>
    </div>
    <div>
      <img src="{{ page.base }}images/sdk-usage/html.svg" width="160" height="160" alt="Angular logo" />
    </div>
  </a>
</div>
<hr/>

<h2>Conclusion</h2>
<p>In this article we have seen that the old-fashioned form tools typically won’t let you implement a form solution completely into your own projects for reasons of data security, independence and customization. Tripetto’s FormBuilder SDK lets you do precisely that, offering you a full-fledged form solution inside your own app, with total control over the implementation and the form data storage.</p>
<p>For developers it’s really easy to implement the SDK, without having to develop a whole form solution themselves. With ready-to-use <a href="https://tripetto.com/sdk/react/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide">React</a> and <a href="https://tripetto.com/sdk/angular/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide">Angular</a> components you can level up your apps within no time. And for <a href="https://tripetto.com/sdk/plain-js/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide">plain JavaScript</a> and <a href="https://tripetto.com/sdk/html/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide">HTML</a> projects it’s just as easy.</p>
<p>Testing the FormBuilder SDK for implementation is totally free. Once you’re ready to deploy it to a production environment an <a href="https://tripetto.com/sdk/pricing/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide">SDK license</a> is required in most (not all) cases. You can determine license requirements and pricing via our <a href="https://tripetto.com/sdk/get-quote/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide">license request wizard</a>. If you have any questions, please reach out to us via a <a href="https://tripetto.com/sdk/chat-live/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide">live chat</a> or <a href="https://tripetto.com/sdk/schedule-call/?utm_source=Tripetto&utm_medium=content-marketing&utm_content=formbuilder-sdk-implementation-guide">schedule a call with us</a>. We’re happy to answer any of your questions and go over your project right away to determine the license.</p>
