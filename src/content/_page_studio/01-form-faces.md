---
base: ../
---

<section class="studio-form-faces">
  <div class="container">
    <div class="row studio-form-faces-intro">
      <div class="col-12">
        <small>Pick a conversational, chat or classic form layout - instantly!</small>
        <h2>Forms with faces.</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-12 carousel-form-faces-buttons">
        <ul class="carousel-buttons">
          <li class="palette-autoscroll active" data-target="#carouselFormFaces" data-slide-to="0">
            <div>
              {% include icon-face.html face='autoscroll' size='small' name='Autoscroll Form Face' template='background' radius='medium' %}
            </div>
            <div>
              <h3>Autoscroll<span> Face</span></h3>
              <p>Fluently presents <strong>one question at a time</strong>.</p>
            </div>
          </li>
          <li class="palette-chat" data-target="#carouselFormFaces" data-slide-to="1">
            <div>
              {% include icon-face.html face='chat' size='small' name='Chat Form Face' template='background' radius='medium' %}
            </div>
            <div>
              <h3>Chat<span> Face</span></h3>
              <p>Presents all <strong>questions and answers as a chat</strong>.</p>
            </div>
          </li>
          <li class="palette-classic" data-target="#carouselFormFaces" data-slide-to="2">
            <div>
              {% include icon-face.html face='classic' size='small' name='Classic Form Face' template='background' radius='medium' %}
            </div>
            <div>
              <h3>Classic<span> Face</span></h3>
              <p>Presents question fields in a <strong>traditional format</strong>.</p>
            </div>
          </li>
        </ul>
      </div>
    </div>
    <div class="row carousel-slides">
      <div class="col">
        <div id="carouselFormFaces" class="carousel slide carousel-fade">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="row">
                <div class="col-12">
                  <img src="{{ page.base }}images/scenes/autoscroll-fitness-registration.webp" width="2220" height="1435" alt="Screenshots of a fitness registration form in the autoscroll form face, shown on a tablet and a mobile phone." loading="lazy" />
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="row">
                <div class="col-12">
                  <img src="{{ page.base }}images/scenes/chat-fitness-registration.webp" width="2220" height="1435" alt="Screenshots of a fitness registration form in the autoscroll form face, shown on a tablet and a mobile phone." loading="lazy" />
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="row">
                <div class="col-12">
                  <img src="{{ page.base }}images/scenes/classic-fitness-registration.webp" width="2220" height="1435" alt="Screenshots of a fitness registration form in the autoscroll form face, shown on a tablet and a mobile phone." loading="lazy" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row studio-form-faces-suffix">
      <div class="col-12 col-md-10 col-lg-8 col-xl-7">
        <small>We merged the best of <a href="{{ page.base }}surveymonkey-alternative/">SurveyMonkey</a>, <a href="{{ page.base }}typeform-alternative/">Typeform</a> and Landbot in a full-fledged <strong>form tool for building and deploying beautiful, deeply customizable forms.</strong></small>
      </div>
    </div>
  </div>
</section>

