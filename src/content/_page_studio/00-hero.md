---
base: ../
---

<section class="studio-hero">
  <div class="container">
    <div class="row row-hero">
      <div class="col-sm-10 col-md-8 col-lg-8 col-xl-7 col-hero">
        <span>Tripetto studio</span>
        <h1>Create smart forms in the Tripetto cloud.</h1>
        <p>Build and run fully customizable form experiences from the Tripetto studio and <strong>choose where you store collected data.</strong></p>
        <ul class="buttons">
          <li><a href="{{ page.base }}studio/pricing/" class="button button-large">Start Free</a></li>
          <li><a href="{{ page.base }}studio/features/" class="button button-large button-white">Studio Features</a></li>
        </ul>
        <small>Also create <a href="{{ page.base }}examples/customer-satisfaction-with-net-promoter-score-nps/">surveys</a>, <a href="{{ page.base }}examples/trivia-quiz/">quizzes</a>, <a href="{{ page.base }}examples/body-mass-index-bmi-wizard/">wizards</a>, <a href="{{ page.base }}examples/wedding-rsvp/">registration forms</a> and <a href="{{ page.base }}examples/">so much more</a> with Tripetto.</small>
      </div>
    </div>
  </div>
</section>
