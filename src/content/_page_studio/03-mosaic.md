---
base: ../
---

<section class="studio-mosaic content">
  <div class="container container-content">
    <div class="row">
      <div class="col-sm-8 col-lg-10 col-xl-9 shape-before shape-after">
        <h2><span>Tripetto studio.</span> Wake-up call for form tool dinosaurs.</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-11 col-lg-10">
        <p>The free studio brings the best of SurveyMonkey, Typeform and Landbot in a single form tool and lets you build beautifully smart forms to <strong>boost completion rates and gain better insights.</strong></p>
        <a href="{{ page.base }}studio/features/" class="hyperlink"><span>See all studio features</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
  <div class="container container-mosaic">
    <div class="row">
      <div class="col mosaic shape-before shape-after">
        <a href="{{ page.base }}studio/features/#build" class="mosaic-block mosaic-block-horizontal palette-build">
          <div>
            <h3>Magnetic Storyboard</h3>
            <p>Build smart forms and surveys like flowcharts, with drag-and-drop.</p>
          </div>
        </a>
        <a href="{{ page.base }}studio/features/#logic" class="mosaic-block mosaic-block-horizontal palette-logic">
          <div>
            <h3>Conversational Logic Types</h3>
            <p>Build seriously conversational flows with logic, actions and calculations. <strong>All no-code!</strong></p>
          </div>
        </a>
        <a href="{{ page.base }}studio/features/#customization" class="mosaic-block mosaic-block-horizontal palette-customization">
          <div>
            <h3>Convertible Form Faces</h3>
            <p>Pick a Typeform-like, a chat or a classic form layout. And switch instantly.</p>
          </div>
        </a>
        <a href="{{ page.base }}studio/features/#automations" class="mosaic-block mosaic-block-vertical palette-automations">
          <div>
            <h3>Fast, No-Code Automations</h3>
            <p>Receive <strong>notifications</strong>, <strong>connect to 1.000+ services</strong> and <strong>track form activity</strong>.</p>
          </div>
        </a>
        <a href="{{ page.base }}studio/features/#sharing" class="mosaic-block mosaic-block-vertical palette-sharing">
          <div>
            <h3>Flexible Publication</h3>
            <p>Share a simple link or embed forms neatly on your page.</p>
          </div>
          <img src="{{ page.base }}images/chapters/sharing.svg" alt="Illustration representing flexible publication" />
        </a>
        <a href="{{ page.base }}studio/features/#hosting" class="mosaic-block mosaic-block-vertical palette-hosting">
          <div>
            <h3>Data Storage Autonomy</h3>
            <p>Store things at Tripetto or self-host wherever you prefer. <strong>Hello GDPR!</strong></p>
          </div>
          <img src="{{ page.base }}images/chapters/hosting.svg" alt="Illustration representing data storage freedom" />
        </a>
      </div>
    </div>
  </div>
</section>
