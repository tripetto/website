---
base: ../
---

<section class="wordpress-stairs stairs">
  <div class="container">
    <div class="row">
      <div class="col-12 stair-steps">
        <div class="stair-step stair-step-one">
          <img src="{{ page.base }}images/wordpress/taste.svg" alt="Illustration representing form examples." width="158" height="280" loading="lazy" />
          <div>
            <h2>Taste</h2>
            <p>See <a href="{{ page.base }}examples/">examples</a> of what forms and surveys you can build for your WordPress projects with Tripetto.</p>
            <a href="{{ page.base }}examples/" class="button button-full">View Examples</a>
          </div>
        </div>
        <div class="stair-step stair-step-two">
          <img src="{{ page.base }}images/wordpress/begin.svg" alt="Illustration representing tutorials." width="175" height="280" loading="lazy" />
          <div>
            <h2>Begin</h2>
            <p>Quickly get a grasp of what’s possible with Tripetto for WordPress and start building fast with <a href="{{ page.base }}wordpress/help/video-tutorials/">tutorials</a>.</p>
            <a href="{{ page.base }}wordpress/help/video-tutorials/" class="button button-full">Watch Tutorials</a>
          </div>
        </div>
        <div class="stair-step stair-step-three">
          <img src="{{ page.base }}images/wordpress/refine.svg" alt="Illustration representing help center." width="162" height="280" loading="lazy" />
          <div>
            <h2>Refine</h2>
            <p>Want pro tips? Something not working as expected? The <a href="{{ page.base }}wordpress/help/">help center</a> takes you to the next level.</p>
            <a href="{{ page.base }}wordpress/help/" class="button button-full">Get Tips & Tricks</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
