---
base: ../
---

<section class="wordpress-hero">
  <div class="container">
    <div class="row row-hero">
      <div class="col-sm-12 col-md-8 col-hero">
        <span>Tripetto WordPress plugin</span>
        <h1>Create stunning forms inside of WordPress.</h1>
        <p>Build powerful, deeply customizable form experiences and store collected data <strong>all right inside your WordPress.</strong></p>
        <ul class="buttons">
          <li><a href="{{ page.base }}wordpress/pricing/" class="button button-large">Get Tripetto</a></li>
          <li><a href="{{ page.base }}wordpress/features/" class="button button-large button-white">Plugin Features</a></li>
        </ul>
        <small>Also create <a href="{{ page.base }}examples/customer-satisfaction-with-net-promoter-score-nps/">surveys</a>, <a href="{{ page.base }}examples/trivia-quiz/">quizzes</a>, <a href="{{ page.base }}examples/body-mass-index-bmi-wizard/">wizards</a>, <a href="{{ page.base }}examples/wedding-rsvp/">registration forms</a> and <a href="{{ page.base }}examples/">so much more</a> with Tripetto.</small>
      </div>
    </div>
  </div>
</section>
