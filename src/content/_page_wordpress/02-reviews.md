---
base: ../
reviews: [remrick,danfadida,mohit,mike_monty,guilhermesouza,ari0,vladimir_markovic1,kgjermani,muyiwa,tasksdoneright,isyedme,filip_van_hoeckel1]
---

{% if page.reviews %}
{% assign reviews_count = 0 %}
<section class="wordpress-reviews content reviews">
  <div class="container-fluid ticker-holder ticker-reviews">
    <div class="row">
      <ul class="ticker-blocks-reviews reviews">
          {% for review in page.reviews %}
          {% assign review_item = site.data.reviews[review] %}
          <li>
            <div class="review-palette-{{ reviews_count }}">
              <div class="review-rating">
              {% for i in (1..review_item.rating) %}
                <i class="fas fa-star"></i>
              {% endfor %}
              </div>
              <div class="review-meta">
                <span>{{ review_item.author }}</span>
                <small>{{ review_item.date }}</small>
              </div>
              <a href="{{ review_item.url }}" target="_blank" rel="noopener noreferrer"><h4>{{ review_item.title }}</h4></a>
              <p>{{ review_item.text }}</p>
            </div>
          </li>
          {% assign reviews_count = reviews_count | plus: 1 %}
          {% if reviews_count == 3 %}
            {% assign reviews_count = 0 %}
          {% endif %}
          {% endfor %}
      </ul>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col shape-after">
        <a href="{{ page.base }}reviews/" class="hyperlink"><span>Read all user reviews</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
{% endif %}
