---
base: ../
---

<section class="wordpress-mosaic content">
  <div class="container container-content">
    <div class="row">
      <div class="col-lg-11 col-xl-10">
        <h2>A single plugin.<br/>Everything forms.<span>Totally stand-alone.</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-11 col-lg-10">
        <p>That’s right! The plugin runs a stand-alone, <strong>fullblown form builder and all storage</strong> right inside your WordPress without any dependencies on outside infrastructure. No strings attached!</p>
        <a href="{{ page.base }}wordpress/features/" class="hyperlink"><span>See all plugin features</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
  <div class="container container-mosaic">
    <div class="row">
      <div class="col mosaic shape-before shape-after">
        <a href="{{ page.base }}wordpress/features/#build" class="mosaic-block mosaic-block-horizontal palette-build">
          <div>
            <h3>Magnetic Storyboard</h3>
            <p>Build smart forms and surveys like flowcharts, with drag-and-drop.</p>
          </div>
        </a>
        <a href="{{ page.base }}wordpress/features/#logic" class="mosaic-block mosaic-block-horizontal palette-logic">
          <div>
            <h3>Conversational Logic Types</h3>
            <p>Build seriously conversational flows with logic, actions and calculations. <strong>All no-code!</strong></p>
          </div>
        </a>
        <a href="{{ page.base }}wordpress/features/#customization" class="mosaic-block mosaic-block-horizontal palette-customization">
          <div>
            <h3>Convertible Form Faces</h3>
            <p>Pick a Typeform-like, a chat or a classic form layout. And switch instantly.</p>
          </div>
        </a>
        <a href="{{ page.base }}wordpress/features/#automations" class="mosaic-block mosaic-block-vertical palette-automations">
          <div>
            <h3>Fast, No-Code Automations</h3>
            <p>Receive <strong>notifications</strong>, <strong>connect to 1.000+ services</strong> and <strong>track form activity</strong>.</p>
          </div>
        </a>
        <a href="{{ page.base }}wordpress/features/#sharing" class="mosaic-block mosaic-block-vertical palette-sharing">
          <div>
            <h3>Flexible Publication</h3>
            <p>Share a simple link or embed forms neatly on your page.</p>
          </div>
          <img src="{{ page.base }}images/chapters/sharing.svg" alt="Illustration representing flexible publication" />
        </a>
        <a href="{{ page.base }}wordpress/features/#hosting" class="mosaic-block mosaic-block-vertical palette-hosting">
          <div>
            <h3>Data Storage Autonomy</h3>
            <p>Host and store everything only inside your very own WordPress. <strong>Hello GDPR!</strong></p>
          </div>
          <img src="{{ page.base }}images/chapters/hosting.svg" alt="Illustration representing data storage freedom" />
        </a>
      </div>
    </div>
  </div>
</section>
