---
layout: help-category
base: ../../../
permalink: /wordpress/help/using-logic-features/
title: Using logic features in the Tripetto WordPress plugin
description: Learn how to make your forms smart with advanced logic features in the Tripetto WordPress plugin.
category_area: wordpress
category_id: logic
category_type: chapter
category_videos: [logic-branch-logic, logic-piping, logic-calculations, logic-prefill]
redirect_from:
- /help/wordpress/using-logic-features/
- /help/using-logic-features/
---
