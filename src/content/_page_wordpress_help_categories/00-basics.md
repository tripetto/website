---
layout: help-category
base: ../../../
permalink: /wordpress/help/basics/
title: Basics for the Tripetto WordPress plugin
description: Learn the basics on the usage of the Tripetto WordPress plugin.
category_area: wordpress
category_id: wordpress-basics
category_type: basics
redirect_from:
- /help/basics/
---
