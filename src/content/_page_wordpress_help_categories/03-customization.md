---
layout: help-category
base: ../../../
permalink: /wordpress/help/styling-and-customizing/
title: Styling and customizing in the Tripetto WordPress plugin
description: Learn how to customize your form experience, styling and translations in the Tripetto WordPress plugin.
category_area: wordpress
category_id: customization
category_type: chapter
category_videos: [customization-style, customization-form-faces, customization-translate]
redirect_from:
- /help/wordpress/customize-forms/
- /help/wordpress/styling-and-customization/
- /help/styling-and-customizing/
---
