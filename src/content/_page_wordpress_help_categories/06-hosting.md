---
layout: help-category
base: ../../../
permalink: /wordpress/help/managing-data-and-results/
title: Managing data and results in the Tripetto WordPress plugin
description: Find out how you can use your response data and get notified of new results in the Tripetto WordPress plugin.
category_area: wordpress
category_id: hosting
category_type: chapter
category_videos: [hosting-wordpress-results]
redirect_from:
- /help/wordpress/get-results/
- /help/wordpress/managing-data-and-results/
- /help/managing-data-and-results/
---
