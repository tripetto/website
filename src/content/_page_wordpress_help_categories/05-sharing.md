---
layout: help-category
base: ../../../
permalink: /wordpress/help/sharing-forms-and-surveys/
title: Sharing forms and surveys in the Tripetto WordPress plugin
description: See how you can distribute your forms from the Tripetto WordPress plugin to your audience.
category_area: wordpress
category_id: sharing
category_type: chapter
category_videos: [sharing-wordpress-link, sharing-wordpress-shortcode, sharing-wordpress-gutenberg, sharing-wordpress-elementor]
redirect_from:
- /help/wordpress/run-forms/
- /help/wordpress/sharing-forms-and-surveys/
- /help/sharing-forms-and-surveys/
---
