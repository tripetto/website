---
layout: help-category
base: ../../../
permalink: /wordpress/help/building-forms-and-surveys/
title: Building forms and surveys in the Tripetto WordPress plugin
description: Get to know how the form builder works in the Tripetto WordPress plugin, from the simple basics to advanced tutorials.
category_area: wordpress
category_id: build
category_type: chapter
category_videos: [build-builder,build-simple-form,build-welcome-message, build-closing-message]
redirect_from:
- /help/wordpress/build-forms/
- /help/wordpress/building-forms-and-surveys/
- /help/building-forms-and-surveys/
---
