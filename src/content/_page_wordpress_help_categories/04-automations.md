---
layout: help-category
base: ../../../
permalink: /wordpress/help/automating-things/
title: Automating things in the Tripetto WordPress plugin
description: Learn how to automate actions with your data in the Tripetto WordPress plugin.
category_area: wordpress
category_id: automations
category_type: chapter
category_videos: [automations-email, automations-slack, automations-webhook, automations-tracking]
redirect_from:
- /help/wordpress/automating-things/
- /help/automating-things/
---
