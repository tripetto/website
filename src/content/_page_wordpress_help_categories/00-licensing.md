---
layout: help-category
base: ../../../
permalink: /wordpress/help/licensing/
title: Licensing for the Tripetto WordPress plugin
description: See the possibilities of licensing for the Tripetto WordPress plugin.
category_area: wordpress
category_id: wordpress-licensing
category_type: basics
redirect_from:
- /help/licensing/
---
