---
base: ../../../
---

<section class="sdk-alternative-surveyjs-features-intro block-first sdk-intro">
  <div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-6 col-lg-8">
        <h1>Comparing the FormBuilder SDK and SurveyJS.</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-6">
        <p>Built for developers by developers, the FormBuilder SDK includes everything that the SurveyJS Essential and Basic versions have to offer. <strong>And then some.</strong></p>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
