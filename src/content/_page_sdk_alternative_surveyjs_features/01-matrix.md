---
base: ../../../
---

<section class="sdk-alternative-surveyjs-features-matrix matrix matrix-compare" id="compare">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="table-responsive">
          <table class="table matrix-head">
            <thead>
              <tr>
                <th scope="col" class="matrix-nav">
                  <div>
                    <span role="button" class="matrix-nav-left"><i class="fas fa-chevron-left"></i></span>
                    <span role="button" class="matrix-nav-right"><i class="fas fa-chevron-right"></i></span>
                  </div>
                </th>
                <th scope="col" class="matrix-area">
                  <div>
                    <div class="palette-surveyjs">
                      <span>SurveyJS</span>
                      <small>Form creation and deployment</small>
                      <small>From €499</small>
                    </div>
                  </div>
                </th>
                <th scope="col" class="matrix-area">
                  <div>
                    <div class="palette-sdk">
                      <span>FormBuilder SDK</span>
                      <small>Form creation and deployment</small>
                      <small>From €699</small>
                    </div>
                  </div>
                </th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
    {% assign groups = site.data.sdk-surveyjs-matrix-groups %}
    {% for group in groups %}
    {% assign features = site.data.sdk-surveyjs-matrix | where_exp: "item", "item.group == group.id" %}
    {% if features.size > 0 %}
    <div class="row">
      <div class="col-12">
        <div class="matrix-title{% if group.color %} matrix-title-{{ group.color }}{% endif %}"><h2>{{ group.title }}</h2>{% if group.subtitle %}<span>{{ group.subtitle }}</span>{% endif %}</div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
        <div class="table-responsive">
          <table class="table matrix-body">
            {% if group.navigation %}
            <thead class="d-md-none">
              <tr>
                <th scope="col" class="matrix-nav">
                  <div class="d-md-none">
                    <span role="button" class="matrix-nav-left"><i class="fas fa-chevron-left"></i></span>
                    <span role="button" class="matrix-nav-right"><i class="fas fa-chevron-right"></i></span>
                  </div>
                </th>
                <th scope="col">
                  <div class="products-label">SurveyJS</div>
                </th>
                <th scope="col">
                  <div class="products-label">FormBuilder SDK</div>
                </th>
              </tr>
            </thead>
            {% endif %}
            <tbody>
            {% for feature in features %}
              <tr>
                <td class="matrix-body-row"><h3>{% if feature.icon %}{% assign icon_url = "icons/" | append: feature.icon %}{% include {{ icon_url }}.html %}{% endif %}{% if feature.image %}<span><img src="{{ page.base | append: feature.image }}" alt="{{ feature.title }}" /></span>{% endif %}<span>{{ feature.title }}</span></h3>{% if feature.subtitle %}<small>{{ feature.subtitle }}</small>{% endif %}</td>
                <td>
                {% if feature.surveyjs == true %}
                  <div class="matrix-yes"><i class="fas fa-check fa-fw"></i><span>Yes</span></div>
                {% elsif feature.surveyjs == false %}
                  <div class="matrix-no"><i class="fas fa-times fa-fw"></i><span>No</span></div>
                {% else %}
                  <div>{{ feature.surveyjs }}</div>
                {% endif %}
                {% if feature.surveyjs_subtitle %}
                  <small>{{ feature.surveyjs_subtitle }}</small>
                {% endif %}
                </td>
                <td>
                {% if feature.sdk == true %}
                  <div class="matrix-yes"><i class="fas fa-check fa-fw"></i><span>Yes</span></div>
                {% elsif feature.sdk == false %}
                  <div class="matrix-no"><i class="fas fa-times fa-fw"></i><span>No</span></div>
                {% else %}
                  <div>{{ feature.sdk }}</div>
                {% endif %}
                {% if feature.sdk_subtitle %}
                  <small>{{ feature.sdk_subtitle }}</small>
                {% endif %}
                </td>
              </tr>
            {% endfor %}
            </tbody>
          </table>
        </div>
      </div>
    </div>
    {% endif %}
    {% endfor %}
  </div>
</section>
