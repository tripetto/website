---
base: ../
---

<section class="media-mentioned">
  <div class="container">
    <div class="row">
      <div class="col-lg-11 col-xl-9">
        <h2>And Mentioned In...</h2>
        <ul class="media-list">
          {% for item in site.data.media-mentioned -%}
          <li>
            <div>
              <h3>{{ item.name }}</h3>
              <a href="{{ item.url }}" target="_blank" rel="noopener noreferrer">{{ item.url }}</a>
            </div>
          </li>
          {% endfor -%}
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
