---
base: ../
---

<section class="media-wpmayor">
  <div class="container">
    <div class="row media-wpmayor-quote">
      <div class="col-xl-11">
        <img src="{{ page.base }}images/media/wpmayor.png" width="320" height="391" alt="Logo of WP Mayor" >
        <h2>According to WP Mayor</h2>
        <p>“In a world of WordPress form plugins that all do approximately the same thing, Tripetto is a new form plugin that brings something unique to the table. It helps you create conversational, engaging forms with branching logic where needed.”</p>
      </div>
    </div>
    <div class="row">
      <div class="col-12 media-wpmayor-articles">
        <ul class="tiles-text tiles-text-large tiles-text-link">
          <li>
            <div><img src="{{ page.base }}images/media/wpmayor-review.svg" width="64" height="64" alt="Icon for the review article written by WP Mayor" ></div>
            <div>
              <h3><a href="https://wpmayor.com/tripetto-review/" target="_blank" rel="noopener noreferrer">Tripetto Review</a></h3>
              <p>Read WP Mayor’s hands-on review about Tripetto.</p>
            </div>
          </li>
          <li>
            <div><img src="{{ page.base }}images/media/wpmayor-comparison.svg" width="64" height="64" alt="Icon for the comparison article with WPForms written by WP Mayor" ></div>
            <div>
              <h3><a href="https://wpmayor.com/tripetto-vs-wpforms-which-is-the-best-wordpress-form-plugin/" target="_blank" rel="noopener noreferrer">Tripetto vs. WPForms</a></h3>
              <p>In this comparison WP Mayor digs in to help you choose</p>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
