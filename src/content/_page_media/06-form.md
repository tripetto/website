---
base: ../
---

<section class="media-form">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-lg-10 col-xl-8">
        <h2>Become An Affiliate Partner</h2>
        <p><strong>Tripetto enthusiasts regularly ask us how they can help us grow.</strong> We actually prefer to grow together and have a generous affiliate program to do so. Want to join?</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-11 col-xl-10">
        <div id="TripettoRunner" class="runner-autoscroll-horizontal"></div>
      </div>
    </div>
  </div>
</section>
{% include tripetto-form.html token='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoibFV6Q2ROWXVWYnEvcmZSRkxObERPQ25JcGtEV0VLbkRLUWlRUVNZSzRSMD0iLCJkZWZpbml0aW9uIjoiM2NKMjJwTTFpSUpEOWozSmlFNEQwUGN2RDEzMml2NDVXZk5tb0E4Misxcz0iLCJ0eXBlIjoiY29sbGVjdCJ9.7PhVnghzq4rSSA7mGrk82L3ejEz4g_qnEnyPHUvJdN8' %}
