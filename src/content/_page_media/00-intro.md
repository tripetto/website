---
base: ../
---

<section class="media-intro block-first intro">
  <div class="container">
    <div class="row">
      <div class="col-md-9 shape-before">
        <h1>Take it from the people who know.</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-7 col-xl-6 shape-after">
        <p>Don’t just take our word on Tripetto. Take it from people who use and review it, and from our affiliates who decided to help us sell Tripetto. <strong>They know best.</strong></p>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
