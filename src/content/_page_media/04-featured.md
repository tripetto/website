---
base: ../
---

<section class="media-featured">
  <div class="container">
    <div class="row">
      <div class="col-lg-11 col-xl-9">
        <h2>Also Featured On...</h2>
        <ul class="media-list">
          {% for item in site.data.media-featured -%}
          <li>
            <div>
              <div><img src="{{ page.base }}images/media/{{ item.logo }}" width="96" height="96" alt="Logo of {{ item.name }}" /></div>
              <div>
                <a href="{{ item.url }}" target="_blank" rel="noopener noreferrer"><h3>{{ item.name }}</h3></a>
                {% if item.text %}<p>{{ item.text }}</p>{% endif %}
                <small class="pills">
                  {% if item.time > 0 %}<span><i class="fas fa-file-alt"></i>{{ item.time }} Min.</span>{% endif %}{% if item.time_video > 0 %}<span><i class="fas fa-video"></i>{{ item.time_video }} Min.</span>{% endif %}
                </small>
              </div>
            </div>
          </li>
          {% endfor -%}
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
