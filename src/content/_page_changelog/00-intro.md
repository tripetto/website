---
base: ../
---

<section class="changelog-intro changelog block-first intro">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-lg-9 col-xl-8 shape-before shape-after">
        <h1>Changelog</h1>
        <p>Tripetto gets frequently updated with new features, improvements and bugfixes <strong>based on user feedback</strong>. Have a look at what we’ve done so far with your input.</p>
        <ul class="hyperlinks">
          <li><a href="#request-feature" class="hyperlink anchor"><span>Request a feature</span><i class="fas fa-arrow-down"></i></a></li>
          <li><a href="{{ page.base }}roadmap/" class="hyperlink"><span>View roadmap</span><i class="fas fa-arrow-right"></i></a></li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
