---
base: ../../
---

<section class="sdk-usage-intro sdk-this-week-in-react-intro block-first sdk-intro">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-lg-9">
        <h1>Fully featured form solution for <span>React.</span></h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-9 col-lg-8 col-xl-7">
        <p>The FormBuilder SDK includes easy to use React components for building and running advanced forms. Equip web apps and native apps with a full-fledged form solution. <strong>All in just minutes.</strong></p>
        <ul class="buttons">
          <li><a href="{{ page.base }}sdk/docs/getting-started/usage-with-react/" class="button button-large button-react">Open React Docs</a></li>
          <li><a href="{{ page.base }}sdk/how-it-works/" class="button button-large button-light">Live Demo</a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
