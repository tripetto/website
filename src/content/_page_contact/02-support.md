---
base: ../
---

<section class="contact-support">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2>At Your Service</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-6 col-md-4 col-lg-3 contact-support-list">
        <h3>Get in touch</h3>
        <ul class="palette-red">
          <li><a href="{{ page.base }}support/"><i class="fas fa-comments fa-fw"></i><span>Request support</span></a></li>
          <li><a href="{{ page.base }}roadmap/#request-feature"><i class="fas fa-star fa-fw"></i><span>Request a feature</span></a></li>
          <li><a href="{{ page.base }}partner-with-us/"><i class="fas fa-handshake fa-fw"></i><span>Partner with us</span></a></li>
        </ul>
      </div>
      <div class="col-sm-6 col-md-5 col-lg-4 contact-support-list">
        <h3>Stay tuned</h3>
        <ul class="palette-green">
          <li><a href="https://x.com/tripetto" target="_blank" rel="noopener noreferrer"><i class="fab fa-x-twitter fa-fw"></i><span>Follow us on X</span></a></li>
          <li><a href="{{ page.base }}subscribe/"><i class="fas fa-bell fa-fw"></i><span>Subscribe to updates</span></a></li>
          <li><a href="{{ page.base }}blog/"><i class="fas fa-feather-alt fa-fw"></i><span>Read our blog</span></a></li>
          <li><a href="{{ page.base }}changelog/"><i class="fas fa-clipboard-list fa-fw"></i><span>Check the changelog</span></a></li>
          <li><a href="{{ page.base }}roadmap/"><i class="fas fa-road fa-fw"></i><span>View our roadmap</span></a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
