---
base: ../../
---

<section class="sdk-demo-runners">
  <div class="container">
    <div class="row sdk-demo-content">
      <div class="col">
        <i class="fas fa-arrow-down sdk-demo-arrow"></i>
      </div>
    </div>
    <div class="row sdk-demo-content">
      <div class="col">
        <h2>then embed the form runner,</h2>
      </div>
    </div>
    <div class="row sdk-demo-content">
      <div class="col-sm-10 col-md-9 col-lg-7 col-xl-6">
        <p class="sdk-demo-runners-intro"><strong>The runner is for parsing form definitions</strong> and handles form rendering, complex logic and response collection.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-12 sdk-carousel-runners-buttons sdk-carousel-runners-buttons-small">
        <ul class="carousel-buttons">
          <li class="palette-autoscroll active" data-target="#carouselSDKRunners" data-slide-to="0">
            <div>
              {% include icon-face.html face='autoscroll' size='small' name='Autoscroll Form Runner' template='sdk' radius='medium' %}
            </div>
            <div>
              <h3>Autoscroll<span> Runner</span></h3>
            </div>
          </li>
          <li class="palette-chat" data-target="#carouselSDKRunners" data-slide-to="1">
            <div>
              {% include icon-face.html face='chat' size='small' name='Chat Form Runner' template='sdk' radius='medium' %}
            </div>
            <div>
              <h3>Chat<span> Runner</span></h3>
            </div>
          </li>
          <li class="palette-classic" data-target="#carouselSDKRunners" data-slide-to="2">
            <div>
              {% include icon-face.html face='classic' size='small' name='Classic Form Runner' template='sdk' radius='medium' %}
            </div>
            <div>
              <h3>Classic<span> Runner</span></h3>
            </div>
          </li>
        </ul>
      </div>
    </div>
    <div class="row sdk-demo-flip carousel-slides">
      <div class="col">
        <div id="carouselSDKRunners" class="carousel slide">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="flip" id="runnerAutoscrollFlip">
                <div class="flipper">
                  <div class="flipper-front">
                    <div id="runnerAutoscrollLoader"><i class="fas fa-fw fa-spinner fa-spin"></i>Tripetto Autoscroll Form Runner loading...</div>
                    <div id="runnerAutoscroll"></div>
                  </div>
                  <div class="flipper-back">
                    <div>{% include sdk-code-snippet-runner-autoscroll.html id="runner-autoscroll" %}</div>
                  </div>
                </div>
              </div>
              <div class="sdk-demo-flip-button">
                <span class="flip-button" data-target="runnerAutoscrollFlip" data-back="Back to autoscroll runner"><i class="fas fa-magnifying-glass fa-fw"></i><span>View code</span></span>
              </div>
              <div class="row sdk-demo-content">
                <div class="col">
                  <i class="fas fa-arrow-down sdk-demo-arrow"></i>
                </div>
              </div>
              <div class="row sdk-demo-content">
                <div class="col-sm-9 col-md-7 col-lg-6 col-xl-5">
                  <p>The runner captures collected data in a JSON format for <strong>delivery directly to your endpoint.</strong></p>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <div class="sdk-demo-json prism-light">
                    <div class="sdk-demo-json-empty" id="runnerAutoscrollResponseEmpty">
                      <div><i class="fas fa-fw fa-turn-up"></i>Fill out the form above to see the response in JSON here.</div>
                    </div>
                    <pre id="runnerAutoscrollResponseParser"><code id="runnerAutoscrollResponse" class="language-json"></code></pre>
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="flip" id="runnerChatFlip">
                <div class="flipper">
                  <div class="flipper-front">
                    <div id="runnerChatLoader"><i class="fas fa-fw fa-spinner fa-spin"></i>Tripetto Chat Form Runner loading...</div>
                    <div id="runnerChat"></div>
                  </div>
                  <div class="flipper-back">
                    <div>{% include sdk-code-snippet-runner-chat.html id="runner-chat" %}</div>
                  </div>
                </div>
              </div>
              <div class="sdk-demo-flip-button">
                <span class="flip-button" data-target="runnerChatFlip" data-back="Back to chat runner"><i class="fas fa-magnifying-glass fa-fw"></i><span>View code</span></span>
              </div>
              <div class="row sdk-demo-content">
                <div class="col">
                  <i class="fas fa-arrow-down sdk-demo-arrow"></i>
                </div>
              </div>
              <div class="row sdk-demo-content">
                <div class="col-sm-9 col-md-7 col-lg-6 col-xl-5">
                  <p>The runner captures collected data in a JSON format for <strong>delivery directly to your endpoint.</strong></p>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <div class="sdk-demo-json prism-light">
                    <div class="sdk-demo-json-empty" id="runnerChatResponseEmpty">
                      <div><i class="fas fa-fw fa-turn-up"></i>Fill out the form above to see the response in JSON here.</div>
                    </div>
                    <pre id="runnerChatResponseParser"><code id="runnerChatResponse" class="language-json"></code></pre>
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="flip" id="runnerClassicFlip">
                <div class="flipper">
                  <div class="flipper-front">
                    <div id="runnerClassicLoader"><i class="fas fa-fw fa-spinner fa-spin"></i>Tripetto Classic Form Runner loading...</div>
                    <div id="runnerClassic"></div>
                  </div>
                  <div class="flipper-back">
                    <div>{% include sdk-code-snippet-runner-classic.html id="runner-classic" %}</div>
                  </div>
                </div>
              </div>
              <div class="sdk-demo-flip-button">
                <span class="flip-button" data-target="runnerClassicFlip" data-back="Back to classic runner"><i class="fas fa-magnifying-glass fa-fw"></i><span>View code</span></span>
              </div>
              <div class="row sdk-demo-content">
                <div class="col">
                  <i class="fas fa-arrow-down sdk-demo-arrow"></i>
                </div>
              </div>
              <div class="row sdk-demo-content">
                <div class="col-sm-9 col-md-7 col-lg-6 col-xl-5">
                  <p>The runner captures collected data in a JSON format for <strong>delivery directly to your endpoint.</strong></p>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <div class="sdk-demo-json prism-light">
                    <div class="sdk-demo-json-empty" id="runnerClassicResponseEmpty">
                      <div><i class="fas fa-fw fa-turn-up"></i>Fill out the form above to see the response in JSON here.</div>
                    </div>
                    <pre id="runnerClassicResponseParser"><code id="runnerClassicResponse" class="language-json"></code></pre>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
