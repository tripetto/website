---
base: ../../
---

<section class="sdk-demo-intro block-first">
  <div class="container">
    <div class="row">
      <div class="col">
        <h1>How It Works</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-11 col-lg-10 col-xl-8">
        <p><strong>The SDK is designed for fast implementation</strong> in straightforward cases as well as deep integration into advanced applications.</p>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <ul class="sdk-logos">
          <li>{% include icons/javascript.html %}</li>
          <li>{% include icons/react.html %}</li>
          <li>{% include icons/angular.html %}</li>
          <li>{% include icons/html5.html %}</li>
        </ul>
      </div>
    </div>
  </div>
</section>
