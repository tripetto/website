---
base: ../../
---

<section class="sdk-demo-builder">
  <div class="container">
    <div class="row sdk-demo-content">
      <div class="col">
        <i class="fas fa-arrow-down sdk-demo-arrow"></i>
      </div>
    </div>
    <div class="row sdk-demo-content">
      <div class="col">
        <h2>First, integrate the form builder</h2>
      </div>
    </div>
    <div class="row sdk-demo-content">
      <div class="col-md-10 col-lg-8 col-xl-7">
        <p class="sdk-demo-builder-intro"><strong>The drag-and-drop builder is for creating advanced forms</strong> on the magnetic storyboard and works in all mainstream browsers.</p>
      </div>
    </div>
    <div class="row sdk-demo-flip">
      <div class="col">
        <div class="flip" id="builderFlip">
          <div class="flipper">
            <div class="flipper-front">
              <div id="builderLoader">
                <div><i class="fas fa-fw fa-spinner fa-spin"></i>Tripetto Form Builder loading...</div>
              </div>
              <div class="sdk-demo-builder-holder">
                <div id="builder"></div>
              </div>
            </div>
            <div class="flipper-back">
              <div>{% include sdk-code-snippet-builder.html id="standard-builder" %}</div>
            </div>
          </div>
        </div>
        <div class="sdk-demo-flip-button">
          <span class="flip-button" data-target="builderFlip" data-back="Back to form builder"><i class="fas fa-magnifying-glass fa-fw"></i><span>View code</span></span>
        </div>
      </div>
    </div>
    <div class="row sdk-demo-content">
      <div class="col">
        <i class="fas fa-arrow-down sdk-demo-arrow"></i>
      </div>
    </div>
    <div class="row sdk-demo-content">
      <div class="col-sm-10 col-md-8 col-lg-6 col-xl-5">
        <p>The builder captures all of the form’s structure, logic and contents in a <strong>JSON-formatted form definition.</strong></p>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <div class="sdk-demo-json prism-light">
          <div class="sdk-demo-json-empty" id="definitionEmpty">
            <div><i class="fas fa-fw fa-turn-up"></i>Build a form in the builder above to see the form definition in JSON here.</div>
          </div>
          <pre id="definitionParser"><code id="definition" class="language-json"></code></pre>
        </div>
      </div>
    </div>
  </div>
</section>
