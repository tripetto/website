---
base: ../../
---

<section class="sdk-demo-storage">
  <div class="container">
    <div class="row sdk-demo-content">
      <div class="col">
        <i class="fas fa-arrow-down sdk-demo-arrow"></i>
      </div>
    </div>
    <div class="row sdk-demo-content">
      <div class="col">
        <h2>and take charge of data.</h2>
      </div>
    </div>
    <div class="row sdk-demo-content">
      <div class="col-sm-10 col-md-9 col-lg-7 col-xl-6">
        <p class="sdk-demo-storage-intro"><strong>Forms and data are exclusively self-hosted by you.</strong> Nothing touches any other platform, unless you let it.</p>
      </div>
    </div>
    <div class="row">
      <div class="col">
        {% include sdk-storage.html layout="text" %}
      </div>
    </div>
  </div>
</section>
