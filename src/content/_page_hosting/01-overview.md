---
base: ../
---

<section class="landing-overview">
  <div class="container">
    <div class="row landing-overview-intro">
      <div class="col-12">
        <small>We don’t need your data, so you can walk with it.</small>
        <h2>Be our guest, or host yourself.</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-12 landing-overview-anchors">
        <ul class="tiles-text tiles-text-medium tiles-text-anchor">
          <li data-anchor="#tripetto-hosted">
            <div>
              {% include icon-paragraph.html chapter='hosting' paragraph='tripetto-hosted' name='Hosted by Tripetto' folder='hosting' class='hosting-tripetto' %}
            </div>
            <div>
              <h3><span>Hosted by </span>Tripetto</h3>
              <p>Store all your data safely <strong>in your Tripetto cloud</strong>.</p>
            </div>
          </li>
          <li data-anchor="#wordpress-hosted">
            <div>
              {% include icon-paragraph.html chapter='hosting' paragraph='wordpress-hosted' name='Hosted in WordPress' folder='hosting' %}
            </div>
            <div>
              <h3><span>Hosted in </span>WordPress</h3>
              <p>Host everything entirely <strong>inside your WordPress</strong>.</p>
            </div>
          </li>
          <li data-anchor="#self-hosted">
            <div>
              {% include icon-paragraph.html chapter='hosting' paragraph='self-hosted' name='Self-hosted' folder='hosting' class='hosting-self' %}
            </div>
            <div>
              <h3>Self-host<span>ed</span></h3>
              <p>Host data wherever you want <strong>outside of Tripetto</strong>.</p>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
