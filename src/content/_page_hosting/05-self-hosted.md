---
base: ../
---

<section class="hosting-self">
  <div class="container" id="self-hosted">
    <div class="row">
      <div class="col-12 content hosting-self-title">
        <h2>Self-hosted</h2>
      </div>
    </div>
    <div class="row hosting-self-cards">
      <div class="col-lg-11">
        <div class="row">
          <div class="col-md-6">
            <div class="hosting-card palette-sdk-hybrid">
              <div>
                <h3>Hybrid</h3>
                <p>Tripetto studio + FormBuilder SDK</p>
                <ul>
                  <li>Build forms in Tripetto studio</li>
                  <li>Self-host all forms and data</li>
                  <li>Paid SDK license required</li>
                </ul>
              </div>
              <div>
                <img src="{{ page.base }}images/hosting/sdk-hybrid.svg" width="332" height="240" alt="Scene illustrating the Tripetto FormBuilder SDK with hybrid setup." loading="lazy" />
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="hosting-card palette-sdk-custom">
              <div>
                <h3>Custom</h3>
                <p>Tripetto FormBuilder SDK</p>
                <ul>
                  <li>Build forms inside your app or site</li>
                  <li>Self-host all forms and data</li>
                  <li>Paid SDK license required</li>
                </ul>
              </div>
              <div>
                <img src="{{ page.base }}images/hosting/sdk-custom.svg" width="216" height="240" alt="Scene illustrating the Tripetto FormBuilder SDK with custom setup." loading="lazy" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row hosting-self-suffix">
      <div class="col-md-10 col-lg-9 content">
        <p>With the hybrid and custom self-hosting scenarios you decide where your data is stored, bypassing Tripetto entirely. <strong>Your self-hosted data never reaches Tripetto servers at all.</strong> You’re in total charge of everything.</p>
        <small>The <a href="{{ page.base }}sdk/self-hosting/" target="_blank">self-hosting scenarios</a> require <a href="{{ page.base }}sdk/how-it-works/" target="_blank">developer skills</a> and our <a href="{{ page.base }}sdk/" target="_blank">FormBuilder SDK</a>.</small>
        <a href="{{ page.base }}sdk/" target="_blank" class="hyperlink palette-hosting"><span>About the FormBuilder SDK</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>

