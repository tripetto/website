---
base: ../
---

<section class="hosting-intro block-first intro">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-md-10 col-lg-8 col-xl-9">
        <h1>Hosting freedom is mandatory.</h1>
      </div>
      <div class="col-md-8 col-lg-7">
        <p><strong>Decide where your data is stored.</strong> Could be at Tripetto. But maybe better elsewhere, bypassing Tripetto entirely. That’s cool, too!</p>
        <a href="#gdpr" class="hyperlink palette-hosting anchor"><span>Host GDPR-friendly</span><i class="fas fa-arrow-down"></i></a>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <img src="{{ page.base }}images/hosting/intro.svg" width="920" height="328" alt="Visual of 2 humans representing hosting freedom." loading="lazy" />
        <hr />
      </div>
    </div>
  </div>
</section>
