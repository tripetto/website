---
base: ../
---

<section class="hosting-wordpress">
  <div class="container" id="wordpress-hosted">
    <div class="row">
      <div class="col-12 content hosting-wordpress-title">
        <h2>Hosted in WordPress</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="hosting-card palette-wordpress">
          <div>
            <h3>WP Plugin</h3>
            <p>Tripetto inside WordPress</p>
            <ul>
              <li>Runs completely inside WordPress</li>
              <li>Store data inside the plugin only</li>
              <li>Free and paid subscriptions</li>
            </ul>
          </div>
          <div>
            <img src="{{ page.base }}images/hosting/wordpress.svg" width="332" height="240" alt="Scene illustrating the Tripetto WordPress plugin." loading="lazy" />
          </div>
        </div>
      </div>
      <div class="col-md-6 content">
        <p>The WordPress plugin is a complete, stand-alone form solution. Everything you build and collect with the plugin is exclusively stored inside your own WordPress. <strong>Nothing ever touches any other platform</strong>, unless you choose to.</p>
        <small>And with the extensive <a href="{{ page.base }}help/articles/how-to-configure-plugin-access-and-capabilities-with-wordpress-user-roles/">WordPress roles management</a> in the plugin, you can assign data access and capabilities to designated roles.</small>
        <a href="{{ page.base }}wordpress/" class="hyperlink palette-hosting"><span>About the WordPress plugin</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>

