---
base: ../
---

<section class="hosting-tripetto">
  <div class="container" id="tripetto-hosted">
    <div class="row">
      <div class="col-12 content hosting-tripetto-title">
        <h2>Hosted by Tripetto</h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="hosting-card palette-studio">
          <div>
            <h3>Studio</h3>
            <p>Tripetto in the cloud</p>
            <ul>
              <li>Available at tripetto.app</li>
              <li>Store forms and data at Tripetto</li>
              <li>Free and pay-once per form (not recurring)</li>
            </ul>
          </div>
          <div>
            <img src="{{ page.base }}images/hosting/studio.svg" width="332" height="240" alt="Scene illustrating the Tripetto studio." loading="lazy" />
          </div>
        </div>
      </div>
      <div class="col-md-6 content">
        <p>Tripetto studio is a fullblown, online form solution. The forms you build and data you collect with the studio are <strong>all safely stored inside your own account in the Tripetto cloud</strong>. You can always easily extract your work and results.</p>
        <a href="{{ page.base }}studio/" class="hyperlink palette-hosting"><span>About the studio</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>

