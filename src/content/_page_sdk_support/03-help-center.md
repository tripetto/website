---
base: ../../
---

<section class="sdk-support-help-center">
  <div class="container">
    <div class="row">
      <div class="col-md-9 col-lg-10 col-xl-8">
        <h2>How to build good forms?</h2>
        <p>The help center for end users has articles, videos and examples for building forms with Tripetto.</p>
        <div>
          <a href="{{ page.base }}studio/help/" class="button button-wide" target="_blank">How To Build Forms</a>
        </div>
      </div>
    </div>
  </div>
</section>
