---
base: ../../
---
<section class="sdk-support-form" id="request-support">
  <div class="container">
    <div class="row">
      <div class="col-lg-9 col-xl-8">
        <h2>Request Support</h2>
        <p>If you can’t find your answer in the <a href="{{ page.base }}sdk/docs/">documentation</a>, please fill out the support request below and we’ll get back to you as soon as possible. Normally within one business day. <strong>Our office hours are Monday-Friday, 9-17 CET (Amsterdam).</strong></p>
        <small>Please note that <strong>technical SDK support is a paid service</strong>, charged by the hour. Upon receipt of your support request a quote will be prepared for you to accept. We won’t supply or invoice support without your prior consent. Other questions we’ll gladly answer free of charge, of course 🙂.</small>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-9 col-xl-8">
        <div id="TripettoRunner" class="runner-autoscroll-horizontal"></div>
      </div>
    </div>
  </div>
</section>
{% include tripetto-form.html token='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoibFV6Q2ROWXVWYnEvcmZSRkxObERPQ25JcGtEV0VLbkRLUWlRUVNZSzRSMD0iLCJkZWZpbml0aW9uIjoicW5ZQkxvRTcwMVdTb1h5TmZhOEdpVTdGclB2aHlnOHpLN0J6V3FKWEtzbz0iLCJ0eXBlIjoiY29sbGVjdCJ9.sSFX1byoLowd7SPkIdh-qAU-N1D0z_0u9d2HU1KZ_dM' %}
