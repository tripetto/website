---
layout: tutorials-article
base: ../../
level: 1
permalink: /tutorials/using-the-real-time-preview-in-the-builder/
title: Using the real-time preview in the builder - Tripetto Tutorials
description: A tutorial about the real-time preview in the builder to assist you while building forms and surveys.
article_title: Using the real-time preview in the builder
author: martijn
time: 2
category_id: build
common_content_core: true
---
<p>While building your form, you can immediately see what you're working on in the live preview.</p>

<div class="article-content-core">
<h2 id="live">Live preview</h2>
<p>An important part of our form builder is the <strong>live preview</strong> at the right side of the storyboard. This preview will  always show a realtime preview of what you're building. Every change you make in your form's structure, content, styling, translation, etc. will be visible immediately in the preview. Truly live!</p>

<h3 id="logic">Logic preview</h3>
<p>Depending on the phase you're in while building your form, it can be handy to let the preview execute the logic or not.</p>
<ul>
  <li>If you show the preview <strong>without logic</strong>, you'll see your whole form at once, making it easy to edit the form, but not easy to test the logic;</li>
  <li>If you show the preview <strong>with logic</strong>, you can immediately test the form and see the effect that your logic has on your form.</li>
</ul>

<h3 id="device">Device preview</h3>
<p>All Tripetto forms are optimized for all modern devices. The preview helps you to see how your form looks on all those different screen sizes.</p>

<hr/>
<h2 id="help">Help articles</h2>
<p>In our Help Center you can find a more detailed help article about the live preview:</p>
</div>
<ul class="tiles tiles-two">
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'preview'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url title=articles.article_title palette-top=articles.category_id category=data_category.name palette-category='faded' type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
</ul>
