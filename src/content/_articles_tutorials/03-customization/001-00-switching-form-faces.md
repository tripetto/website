---
layout: tutorials-article
base: ../../
level: 1
permalink: /tutorials/switching-form-faces/
title: Switching form faces - Tripetto Tutorials
description: A tutorial about form faces to present your form in the desired appearance and experience.
article_title: Switching form faces
author: martijn
time: 2
category_id: customization
common_content_core: true
---
<p>A unique feature in Tripetto is you can switch each form into three different form experiences. We call them form faces.</p>

<div class="article-content-core">
<h2 id="form_faces">Form faces</h2>
<p>Form faces help you to give your audience the best user experience to your forms and by doing so (hopefully) increase your completion rates.</p>
<p>What makes the form faces of Tripetto unique is that you don't have to worry about your form's structure, styling and content. All features are available in all form faces and you can switch form faces all the time.</p>
<p>We offer <strong>3 form faces</strong>:</p>
<h3 id="autoscroll">Autoscroll form face</h3>
<p>The autoscroll form face presents your questions one by one. This gives a great feeling of conversation and interaction to your user.</p>
<figure>
  <img src="{{ page.base }}images/scenes/autoscroll-customer-satisfaction.webp" width="2220" height="1435" alt="Screenshot of an autoscroll form face in Tripetto" class="no-shadow" />
  <figcaption>Example of a customer satisfaction survey in the autoscroll form face.</figcaption>
</figure>

<h3 id="chat">Chat form face</h3>
<p>The chat form face presents your form as a chat conversation, including speech bubbles.</p>
<figure>
  <img src="{{ page.base }}images/scenes/chat-wedding-rsvp.webp" width="2220" height="1435" alt="Screenshot of a chat form face in Tripetto" class="no-shadow" />
  <figcaption>Example of a wedding RSVP form in the chat form face.</figcaption>
</figure>

<h3 id="classic">Classic form face</h3>
<p>The classic face presents your form in a more traditional way and is perfect if you want to show more questions at a time.</p>
<figure>
  <img src="{{ page.base }}images/scenes/classic-restaurant-reservation.webp" width="2220" height="1435" alt="Screenshot of a classic form face in Tripetto" class="no-shadow" />
  <figcaption>Example of a restaurant reservation form in the classic form face.</figcaption>
</figure>

<hr/>
<h2 id="help">Help articles</h2>
<p>In our Help Center you can find more detailed help articles about form faces:</p>
</div>
<ul class="tiles tiles-two">
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'form_faces' or item.article_id == 'form_face'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url title=articles.article_title palette-top=articles.category_id category=data_category.name palette-category='faded' type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
</ul>
