---
layout: tutorials-article
base: ../../
level: 1
permalink: /tutorials/getting-automatic-slack-notifications/
title: Getting automatic Slack notifications - Tripetto Tutorials
description: A tutorial about Slack alerts for form and survey completions.
article_title: Getting automatic Slack notifications
author: jurgen
time: 1
category_id: automations
common_content_core: true
---
<p>Tripetto can send you automatic Slack notifications for each new entry.</p>

<div class="article-content-core">
<h2 id="notification">Slack notifications</h2>
<p>By enabling Slack notifications, Tripetto will send a notification to a Slack channel for each newly completed form entry. This lets you keep track of the entries, without logging into Tripetto.</p>
<h3 id="form_data">Form data</h3>
<p>To make this even more easy, you can also include all form data from the entry inside the Slack notification that you receive. That helps you to quickly get insights of the entry and determine if you have to take action.</p>
<h3 id="slack">Slack's reactions and threads</h3>
<p>A big benefit of Slack notifications is you can use Slack's built in features to easily keep track of each entry. For example by using Slack's reactions to mark an entry as seen and by using Slack's threads to discuss the entry with your team.</p>

<hr/>
<h2 id="help">Help articles</h2>
<p>In our Help Center you can find a more detailed help article about Slack notifications:</p>
</div>
<ul class="tiles tiles-two">
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'slack'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url title=articles.article_title palette-top=articles.category_id category=data_category.name palette-category='faded' type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
</ul>
