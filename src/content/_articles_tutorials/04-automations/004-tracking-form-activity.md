---
layout: tutorials-article
base: ../../
level: 1
permalink: /tutorials/tracking-form-activity/
title: Tracking form activity - Tripetto Tutorials
description: A tutorial about tracking form activity with Google Analytics, Google Tag Manager, Facebook Pixel or other tracking services.
article_title: Tracking form activity
author: mark
time: 2
category_id: automations
common_content_core: true
---
<p>Tripetto lets you get insights of form activity by connecting with tracking services like Google Analytics, Google Tag Manager and Facebook Pixel.</p>

<div class="article-content-core">
<h2 id="tracking">Tracking</h2>
<p>By tracking the activity of your respondents in your forms, you can get valuable insights on the performance of your forms. Tripetto forms send several events to your tracking service(s), so you can analyze form starts, form completions, interactions and drop-offs.</p>

<h3 id="services">Tracking services</h3>
<p>To help you receive and analyze these tracking data, you use a tracking service. Tripetto lets you instantly connect with the most used tracking services <a href="https://marketingplatform.google.com/about/analytics/" target="_blank" rel="noopener noreferrer"><strong>Google Analytics</strong></a>, <a href="https://marketingplatform.google.com/about/tag-manager/" target="_blank" rel="noopener noreferrer"><strong>Google Tag Manager</strong></a> and <a href="https://www.facebook.com/business/tools/meta-pixel" target="_blank" rel="noopener noreferrer"><strong>Facebook Pixel</strong></a>.</p>

<h3 id="custom">Custom tracking code</h3>
<p>Next to these tracking services it's even possible to develop your own <strong>custom tracking code</strong>, for example to connect with other tracking services or to do it all on your own.</p>

<hr/>
<h2 id="help">Help articles</h2>
<p>In our Help Center you can find a more detailed help article about tracking form activity:</p>
</div>
<ul class="tiles tiles-two">
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'tracking'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url title=articles.article_title palette-top=articles.category_id category=data_category.name palette-category='faded' type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
</ul>
