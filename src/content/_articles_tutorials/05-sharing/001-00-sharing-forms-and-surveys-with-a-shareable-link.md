---
layout: tutorials-article
base: ../../
level: 1
permalink: /tutorials/sharing-forms-and-surveys-with-a-shareable-link/
title: Sharing forms and surveys with a shareable link - Tripetto Tutorials
description: Learn how to share forms and surveys with a shareable link to collect data.
article_title: Sharing forms and surveys with a shareable link
author: jurgen
time: 1
category_id: sharing
common_content_core: true
---
<p>The easiest way to start the collection of data is by sharing the shareable link among your audience.</p>

<div class="article-content-core">
<h2 id="link">Shareable link</h2>
<p>Actually there's not much we can say about the shareable link: each form just comes with a handy shareable link and you can use that link to send your form or survey immediately to your audience. Easy peasy 😉.</p>

<hr/>
<h2 id="help">Help articles</h2>
<p>In our Help Center you can find more detailed help articles about the shareable link:</p>
</div>
<ul class="tiles tiles-two">
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'shareable-studio'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url title=articles.article_title palette-top='studio' category=data_category.name palette-category=articles.category_id type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
  {% assign help = site.articles_help | where_exp: "item", "item.article_id == 'shareable-wordpress'" %}
  {% for articles in help %}
    {% assign data_category = site.data.help-categories[articles.category_id] %}
    {% include tile.html url=articles.url title=articles.article_title palette-top='wordpress' category=data_category.name palette-category=articles.category_id type='Help' time=articles.time time_video=articles.time_video %}
  {% endfor %}
</ul>
