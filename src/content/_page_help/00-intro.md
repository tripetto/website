---
base: ../
---

<section class="help-global intro block-first">
  <div class="container">
    <div class="row">
      <div class="col">
        <h1>Help Center</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-9 col-xl-8 help-global-search">
        <form action="{{ page.base }}help/search/" method="get" class="help-search form-inline">
          <label class="sr-only" for="search-input">Search all help articles</label>
          <input type="text" name="q" class="form-input form-input-big" id="search-input" placeholder="Search all help articles" />
          <button class="button button-big"><i class="fas fa-search"></i><span>Search</span></button>
        </form>
        <small>Search all available articles above, or first select your Tripetto version below.</small>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <ul class="hyperlinks">
          <li><a href="{{ page.base }}studio/help/" class="hyperlink"><span>Studio Help Center</span><i class="fas fa-arrow-right"></i></a></li>
          <li><a href="{{ page.base }}wordpress/help/" class="hyperlink"><span>WordPress plugin Help Center</span><i class="fas fa-arrow-right"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
