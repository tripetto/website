---
base: ../../
---

<section class="sdk-self-hosting-solutions">
  <div class="container">
    <div class="row content">
      <div class="col-12">
        <h2>Self-host Flexibility</h2>
      </div>
    </div>
    <div class="row sdk-self-hosting-solutions-cards">
      <div class="col-lg-11">
        <div class="row">
          <div class="col-md-6">
            <div class="hosting-card palette-sdk-hybrid">
              <div>
                <h3>Hybrid</h3>
                <p>Tripetto studio + FormBuilder SDK</p>
                <ul>
                  <li>Build forms in Tripetto studio</li>
                  <li>Self-host all forms and data</li>
                  <li>Paid SDK license required</li>
                </ul>
              </div>
              <div>
                <img src="{{ page.base }}images/hosting/sdk-hybrid.svg" width="332" height="240" alt="Scene illustrating the Tripetto FormBuilder SDK with hybrid setup." loading="lazy" />
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="hosting-card palette-sdk-custom">
              <div>
                <h3>Custom</h3>
                <p>Tripetto FormBuilder SDK</p>
                <ul>
                  <li>Build forms inside your app or site</li>
                  <li>Self-host all forms and data</li>
                  <li>Paid SDK license required</li>
                </ul>
              </div>
              <div>
                <img src="{{ page.base }}images/hosting/sdk-custom.svg" width="216" height="240" alt="Scene illustrating the Tripetto FormBuilder SDK with custom setup." loading="lazy" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row content">
      <div class="col-md-11 col-lg-10">
        <p>With the hybrid and custom self-hosting scenarios you decide where forms and data are stored, also bypassing Tripetto entirely. <strong>Your self-hosted data never reaches any other infrastructure than your own.</strong> You’re in total charge of everything.</p>
        <small>The self-hosting scenarios require <a href="{{ page.base }}sdk/solutions/">developer skills</a> and our <a href="{{ page.base }}sdk/">FormBuilder SDK</a>.</small>
        <a href="{{ page.base }}sdk/solutions/" class="hyperlink palette-hosting"><span>Learn about the solutions</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>

