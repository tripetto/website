---
base: ../../
---

<section class="sdk-self-hosting-gdpr gdpr">
  <div class="container" id="gdpr">
    <div class="row gdpr-quote">
      <div class="col-12 col-xl-10">
        <h2>Hello GDPR!</h2>
        <p>“<strong>The General Data Protection Regulation (GDPR) changed everything in the global privacy landscape in 2018</strong> by putting the onus on the data controller — the company providing a service — to keep data safe, secure and auditable.”</p>
      </div>
    </div>
    <div class="row gdpr-blog">
      <div class="col-12">
        {% assign blog_gdpr = site.articles_sdk_blog | where_exp: "item", "item.article_id == 'gdpr'" %}
        {% for article in blog_gdpr %}
        <div class="gdpr-blog-article" onclick="window.location='{{ article.url }}';">
          <img src="{{ page.base }}images/gdpr/cover.png" width="507" height="220" alt="Illustration for GDPR">
          <div>
            <a href="{{ article.url }}"><h3>{{ article.article_title }}</h3></a>
            <p>{{ article.article_punchline }}</p>
            {% include blog-article-info.html date=article.date author=article.author %}
          </div>
        </div>
        {% endfor %}
      </div>
    </div>
  </div>
</section>
