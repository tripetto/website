---
base: ../../
---

<section class="sdk-self-hosting-hero block-first">
  <div class="container">
    <div class="row">
      <div class="col-sm-10 col-lg-9">
        <h1>Self-host how and where you want.</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-10 col-md-8 col-lg-7 col-xl-6">
        <p>You decide how and where forms and data are stored. Without any dependencies on unwanted infrastructure. <strong>Nothing touches any other platform, unless you let it.</strong></p>
        <ul class="buttons">
          <li><a href="{{ page.base }}sdk/solutions/" class="button button-large button-black">Self-host Solutions</a></li>
          <li><a href="{{ page.base }}sdk/how-it-works/" class="button button-large button-white">Live Demo</a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
