---
base: ../../
---

<section class="sdk-self-hosting-industries">
  <div class="container">
    <div class="row content">
      <div class="col-sm-10 col-lg-9">
        <h2 class="palette-hosting"><span>Secure form data</span> storage is essential.</h2>
      </div>
    </div>
    <div class="row content">
      <div class="col-sm-11 col-md-10 col-lg-9 col-xl-9">
        <p>Build and run forms, and store collected responses entirely within the infrastructure of your choice to <strong>ensure respondent privacy, sensitive data security and compliance by self-hosting the FormBuilder SDK.</strong></p>
      </div>
    </div>
    <div class="row">
      <div class="col-12 sdk-carousel-industries-buttons">
        <ul class="carousel-buttons">
          <li class="active" data-target="#carouselSDKIndustries" data-slide-to="0"><i class="fas fa-briefcase-medical"></i><span>Health<span>care</span></span></li>
          <li data-target="#carouselSDKIndustries" data-slide-to="1"><i class="fas fa-graduation-cap"></i><span>Education</span></li>
          <li data-target="#carouselSDKIndustries" data-slide-to="2"><i class="fas fa-landmark-dome"></i><span>Gov<span>ernment</span></span></li>
          <li data-target="#carouselSDKIndustries" data-slide-to="3"><i class="fas fa-sack-dollar"></i><span>Business</span></li>
        </ul>
      </div>
    </div>
    <div class="row carousel-slides">
      <div class="col">
        <div id="carouselSDKIndustries" class="carousel slide">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="sdk-carousel-industries-item">
                <div class="row">
                  <div class="col-md-3 col-lg-4 sdk-carousel-industries-item-summary">
                    <div>
                      <i class="fas fa-briefcase-medical"></i>
                    </div>
                    <div>
                      <h3>Healthcare</h3>
                      <small>Smart patient information collection flows with self-hosted data storage and security.</small>
                    </div>
                  </div>
                  <div class="col-md-9 col-lg-8 sdk-carousel-industries-item-text">
                    <h4>Have patients and employees complete paperwork online. Host sensitive data only where you want it.</h4>
                    <p>The FormBuilder SDK is suited for building and running smarter, time-saving patient information flows. Among the numerous features are advanced conditional logic capabilities for smooth patient intake, document uploads, form accessibility, and a visual builder. <strong>Even more important, only you decide where forms and sensitive data are stored.</strong> Without unwanted dependencies.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="sdk-carousel-industries-item">
                <div class="row">
                  <div class="col-md-3 col-lg-4 sdk-carousel-industries-item-summary">
                    <div>
                      <i class="fas fa-graduation-cap"></i>
                    </div>
                    <div>
                      <h3>Education</h3>
                      <small>Advanced exam functionality with secure, self-hosted test score and data storage.</small>
                    </div>
                  </div>
                  <div class="col-md-9 col-lg-8 sdk-carousel-industries-item-text">
                    <h4>Securely run student exams, application forms and more. Store collected data only on your own servers.</h4>
                    <p>The FormBuilder SDK is ideal for creating exams, assessments, application forms and so much more for educational professionals worldwide from grade school to university. Among the countless powerful features it includes advanced scoring and flow logic capabilities. Moreover, it lets you host sensitive test results and student data locally. <strong>Without a need for third-party infrastructure.</strong></p>
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="sdk-carousel-industries-item">
                <div class="row">
                  <div class="col-md-3 col-lg-4 sdk-carousel-industries-item-summary">
                    <div>
                      <i class="fas fa-landmark-dome"></i>
                    </div>
                    <div>
                      <h3>Government</h3>
                      <small>Smart form and process automations with secure, self-hosted response handling.</small>
                    </div>
                  </div>
                  <div class="col-md-9 col-lg-8 sdk-carousel-industries-item-text">
                    <h4>Automate building permits, event licenses, job applications etc. Securely self-host collected data.</h4>
                    <p>The FormBuilder SDK is perfect for automating time-consuming application processes for local and regional governmental departments with smart online forms. Its features also include conditional logic capabilities, form chaining, and easy visual form building. All forms, applications and sensitive data only ever live on your end. <strong>Bypassing unwanted external storage ploys.</strong></p>
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="sdk-carousel-industries-item">
                <div class="row">
                  <div class="col-md-3 col-lg-4 sdk-carousel-industries-item-summary">
                    <div>
                      <i class="fas fa-sack-dollar"></i>
                    </div>
                    <div>
                      <h3>Business</h3>
                      <small>Smart business data collection automation with secure, self-hosted response storage.</small>
                    </div>
                  </div>
                  <div class="col-md-9 col-lg-8 sdk-carousel-industries-item-text">
                    <h4>Smarten up and simplify business data collection flows. Store sensitive data securely on your own end.</h4>
                    <p>The FormBuilder SDK is ideal for automating endless business processes, ranging from online order collection to customer lead generation, job applications and much more for business of all sizes. It includes a drag-and-drop builder, flexible form layouts, and calculations. <strong>Also, it lets you host things exclusively on your own servers.</strong> Without having to use any external infrastructure.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
