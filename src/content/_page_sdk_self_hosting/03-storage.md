---
base: ../../
---

<section class="sdk-self-hosting-storage">
  <div class="container">
    <div class="row content">
      <div class="col-sm-10 col-md-9 col-lg-12 col-xl-11">
        <h2 class="palette-hosting">The FormBuilder SDK puts <span class="palette-inline">only you in charge of data.</span></h2>
      </div>
    </div>
    <div class="row content">
      <div class="col-sm-10 col-lg-9">
        <p>By hosting things in-house, <strong>you exclusively control all data flows from front to back.</strong> This lets you manage data security and also eliminates the need to store valuable, sensitive data on third-party platforms.</p>
      </div>
    </div>
    <div class="row">
      <div class="col">
        {% include sdk-storage.html layout="text" %}
      </div>
    </div>
    <div class="row content">
      <div class="col-12">
        <a href="{{ page.base }}sdk/docs/runner/stock/guides/collecting/" class="hyperlink palette-hosting"><span>How to collect and handle responses</span><i class="fas fa-arrow-right"></i></a>
      </div>
    </div>
  </div>
</section>
