---
base: ../../
---

<section class="studio-pricing-upgrades" id="upgrades">
  <div class="container">
    <div class="row studio-pricing-upgrades-intro">
      <div class="col-12 col-sm-10 col-md-12 col-lg-11 col-xl-10">
        <h2>Optional Upgrades</h2>
        <p>The free account includes all Tripetto features. Plain and simple. On top of that, you can <strong>boost your forms or account with optional upgrades.</strong></p>
      </div>
    </div>
    <div class="row">
      <div class="col studio-pricing-upgrades-overview">
        <div>
          <div class="studio-pricing-upgrade-plan">
            <div class="studio-pricing-upgrade-plan-intro">
              <span>Optional upgrade per form</span>
              <h3>Unlock 1</h3>
              <p>Remove Tripetto branding, connect your data to multiple webhooks and smartly track form activity for <u>a single designated form</u>. This upgrade is <strong>pay-once and per form</strong>. <a href="{{ page.base }}help/articles/how-to-purchase-and-activate-form-upgrades-in-the-studio/">Purchase it</a> directly from the studio.</p>
            </div>
            <div class="studio-pricing-upgrade-plan-details">
              <h4>For <u>1 Designated</u> Form</h4>
              <ul class="pricing-features">
                <li class="included">Everything in the free plan</li>
                <li class="featured included">Advanced connections</li>
                <li class="featured included">Form activity tracking</li>
                <li class="featured included">Removable Tripetto branding</li>
              </ul>
              <div class="pricing-price palette-purple">
                <div class="pricing-price-prefix">
                  <div class="valuta">$</div>
                  <div class="amount amount-medium">{{ site.pricing_studio_unlock }}</div>
                  <div class="conditions">
                    <span class="conditions-title">Per form</span>
                    <span class="conditions-subtitle">Pay-once</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <ul class="studio-pricing-upgrade-footer hyperlinks">
            <li><a href="{{ page.base }}help/articles/how-to-purchase-and-activate-form-upgrades-in-the-studio/" class="hyperlink hyperlink-small"><span>See how to activate form upgrades</span><i class="fas fa-arrow-right"></i></a></li>
          </ul>
        </div>
        <div>
          <div class="studio-pricing-upgrade-plan">
            <div class="studio-pricing-upgrade-plan-intro">
              <span>Optional upgrade per account</span>
              <h3>Unlock All</h3>
              <p>Remove Tripetto branding, connect your data to multiple webhooks and smartly track form activity for <u>any and all of your forms</u>. This upgrade is a <strong>total unlock at the account level</strong>. <a href="{{ page.base }}studio/unlock-all/">Contact us</a> to get it.</p>
            </div>
            <div class="studio-pricing-upgrade-plan-details">
              <h4>For <u>All Forms</u> In Your Account</h4>
              <ul class="pricing-features">
                <li class="included">Everything in the free plan</li>
                <li class="featured included">Advanced connections</li>
                <li class="featured included">Form activity tracking</li>
                <li class="featured included">Removable Tripetto branding</li>
              </ul>
              <a href="{{ page.base }}studio/unlock-all/" class="button button-full">Contact Sales</a>
            </div>
          </div>
          <ul class="studio-pricing-upgrade-footer hyperlinks">
            <li><a href="{{ page.base }}studio/unlock-all/" class="hyperlink hyperlink-small"><span>Contact us to upgrade your account</span><i class="fas fa-arrow-right"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
