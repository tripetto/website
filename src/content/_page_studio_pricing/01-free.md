---
base: ../../
---

<section class="studio-pricing-free">
  <div class="container jumbo-container">
    <div class="row jumbo-row">
      <div class="col jumbo-col">
        <div class="row jumbo-content">
          <div class="col-md-6 col-lg-7 jumbo-content-left">
            <span>Standard account</span>
            <h2>Free Plan</h2>
            <p>Tripetto <strong>studio accounts are free and jam-packed with features</strong> out of the box. One catch... All forms have Tripetto branding. Removing this is an <a href="#upgrades" class="anchor">optional upgrade</a>, which will also let you use advanced connections and track form activity.</p>
          </div>
          <div class="col-md-6 col-lg-5 jumbo-content-right">
            <div class="pricing-price palette-yellow">
              <div class="pricing-price-prefix">
                <div class="valuta">$</div>
                <div class="amount amount-medium">0</div>
                <div class="conditions">
                  <span class="conditions-title">Per account</span>
                  <span class="conditions-subtitle">Forever</span>
                </div>
              </div>
            </div>
            <div class="pricing-features-primary">
              <ul class="pricing-features">
                <li class="featured included">All Tripetto features</li>
                <li class="included">Unlimited form building</li>
                <li class="included">Unlimited form logic</li>
                <li class="included">All layout features</li>
                <li class="included">Advanced blocks</li>
                <li class="included">Action blocks</li>
                <li class="included">Notifications</li>
                <li class="included">Custom webhook</li>
                <li class="excluded">Advanced connections<sup>1</sup></li>
                <li class="excluded">Form activity tracking<sup>1</sup></li>
                <li class="excluded">Removable Tripetto branding<sup>1</sup></li>
              </ul>
            </div>
            <div class="pricing-features-secondary">
              <ul class="pricing-features">
                <li class="included">Access to <a href="{{ page.base }}studio/help/">help center</a><small>[24/7]</small></li>
                <li class="excluded">Priority support<small>[Mon-Fri, 9-17 CET]</small></li>
              </ul>
            </div>
            <a href="{{ site.url_app }}" class="button button-full" target="_blank" rel="noopener noreferrer">Start Now</a>
            <small class="pricing-footer">1. <a href="#upgrades" class="anchor">Available together as an optional upgrade.</a></small>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
