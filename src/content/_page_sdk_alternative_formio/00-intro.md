---
base: ../../
---

{% include sdk-alternatives-breadcrumb.html selected='Form.io' %}
<section class="sdk-alternative-intro sdk-alternative-formio-intro sdk-intro">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-xl-9">
        <h1>The FormBuilder SDK is a complete Form.io alternative.</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-11 col-md-8 col-lg-7 col-xl-6">
        <p>The FormBuilder SDK includes everything the Form.io platform offers to equip enterprise apps and websites with a full-fledged form solution. <strong>All in just minutes.</strong></p>
        <ul class="buttons">
          <li><a href="{{ page.base }}sdk/how-it-works/" class="button button-large">Live Demo</a></li>
          <li><a href="{{ page.base }}sdk/docs/" class="button button-large button-light">Get Started</a></li>
        </ul>
        <ul class="sdk-logos">
          <li><a href="{{ page.base }}sdk/plain-js/" title="Tripetto's FormBuilder SDK works in JavaScript">{% include icons/javascript.html %}</a></li>
          <li><a href="{{ page.base }}sdk/react/" title="Tripetto's FormBuilder SDK works in React">{% include icons/react.html %}</a></li>
          <li><a href="{{ page.base }}sdk/angular/" title="Tripetto's FormBuilder SDK works in Angular">{% include icons/angular.html %}</a></li>
          <li><a href="{{ page.base }}sdk/html/" title="Tripetto's FormBuilder SDK works in HTML5">{% include icons/html5.html %}</a></li>
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <hr />
      </div>
    </div>
  </div>
</section>
