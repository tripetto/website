---
base: ../../
---

<section class="sdk-components-blocks" id="blocks">
  <div class="container container-content content">
    <div class="row">
      <div class="col-sm-9 col-md-6 col-lg-9">
        <span class="caption">Customizing question blocks</span>
        <h2 class="palette-sdk-blocks">The development kit is thoroughly <span class="palette-inline">customizable.</span></h2>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-10">
        <p>The development kit also comes with a rich set of <strong>customizable, open source question types</strong> for developers to use out of the box or enhance for use in the Tripetto builder and runners. Also, developers can create original question blocks from scratch.</p>
      </div>
    </div>
  </div>
  <div class="container container-visual">
    <div class="row">
      <div class="col-xl-10">
        <div class="sdk-components-blocks-block sdk-components-blocks-stock">
          <div>
            <img src="{{ page.base }}images/sdk-components/blocks-stock.svg" width="128" height="96" alt="Icon representing stock blocks." loading="lazy">
            <h3>Use or customize our stock blocks.</h3>
            <p>Blocks make up the forms created in the builder and executed by the runner. <strong>Use whichever of our stock blocks, and even customize them to fit your needs.</strong></p>
            <ul class="hyperlinks-list">
              <li><a href="{{ page.base }}question-types/" class="hyperlink hyperlink-small" target="_blank"><span>Discover our stock blocks</span><i class="fas fa-arrow-right"></i></a></li>
              <li><a href="{{ page.base }}sdk/docs/blocks/stock/" class="hyperlink hyperlink-small"><span>Learn how to implement stock blocks</span><i class="fas fa-arrow-right"></i></a></li>
            </ul>
          </div>
          <div>
            <ul>
              <li>
                <div>
                  <img src="{{ page.base }}images/sdk-components/blocks-stock-question.svg" width="65" height="65" alt="Icon representing stock question blocks." width="65" height="65" loading="lazy">
                </div>
                <div>
                  <h4>Question Blocks</h4>
                  <small>Ask for respondent input with <strong>all the regular question types.</strong></small>
                </div>
              </li>
              <li>
                <div>
                  <img src="{{ page.base }}images/sdk-components/blocks-stock-action.svg" width="65" height="65" alt="Icon representing stock action blocks." width="65" height="65" loading="lazy">
                </div>
                <div>
                  <h4>Action Blocks</h4>
                  <small>Perform real-time actions <strong>based on respondent inputs.</strong></small>
                </div>
              </li>
              <li>
                <div>
                  <img src="{{ page.base }}images/sdk-components/blocks-stock-condition.svg" width="65" height="65" alt="Icon representing stock condition blocks." width="65" height="65" loading="lazy">
                </div>
                <div>
                  <h4>Condition Blocks</h4>
                  <small>Contain logic and are used to <strong>define branching conditions.</strong></small>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="sdk-components-blocks-block sdk-components-blocks-custom">
          <div>
            <h3>Develop your own custom blocks.</h3>
            <p>You can extend Tripetto with more question types and functionality by <strong>building custom blocks for anything.</strong></p>
            <ul class="sdk-components-blocks-custom-examples"><li><img src="{{ page.base }}images/sdk-components/blocks-custom-address.svg" width="72" height="72" alt="Icon representing a custom address block." /><h4>Address</h4></li><li><img src="{{ page.base }}images/sdk-components/blocks-custom-api.svg" width="72" height="72" alt="Icon representing a custom API block." /><h4>API's</h4></li><li><img src="{{ page.base }}images/sdk-components/blocks-custom-timer.svg" width="72" height="72" alt="Icon representing a custom timer block." /><h4>Timer</h4></li><li><img src="{{ page.base }}images/sdk-components/blocks-custom-more.svg" width="72" height="72" alt="Icon representing more custom blocks." /><h4>More</h4></li></ul>
            <ul class="hyperlinks-list">
              <li><a href="{{ page.base }}sdk/docs/blocks/custom/introduction/" class="hyperlink hyperlink-small palette-sdk-blocks"><span>Learn how to implement custom blocks</span><i class="fas fa-arrow-right"></i></a></li>
              <li><a href="{{ page.base }}sdk/docs/blocks/custom/guides/boilerplate/" class="hyperlink hyperlink-small palette-sdk-blocks"><span>Custom block boilerplate</span><i class="fas fa-arrow-right"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-xl-5">
        <div class="sdk-components-blocks-typescript">
          <div class="frame">
            <div><a href="https://www.typescriptlang.org/" target="_blank" rel="noopener noreferrer">{% include icons/typescript.html %}</a></div>
            <div>
              <h3>TypeScript typings included.</h3>
              <p>Tripetto contains type declarations and supports <strong><a href="https://www.typescriptlang.org/" target="_blank" rel="noopener noreferrer">TypeScript</a> out of the box</strong>.</p>
            </div>
          </div>
          </div>
        <div class="sdk-components-blocks-payoff">When working with the SDK, only your imagination is the limit.</div>
      </div>
    </div>
  </div>
  <div class="container container-hyperlink">
    <div class="row">
      <div class="col-12"><a href="{{ page.base }}sdk/docs/blocks/introduction/" class="hyperlink palette-sdk-blocks"><span>Learn how to customize and develop blocks</span><i class="fas fa-arrow-right"></i></a></div>
    </div>
  </div>
</section>
