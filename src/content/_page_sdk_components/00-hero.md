---
base: ../../
---

<section class="sdk-components-hero block-first">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 col-md-9">
        <h1>Tripetto in pieces for developers.</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 col-xl-6">
        <p>Pick what you need from the complete toolset with advanced form builder, form runners and countless question types. <strong>Open source, customizable, extensible and documented.</strong></p>
        <ul class="buttons">
          <li><a href="{{ page.base }}sdk/docs/" class="button button-large">Get Started</a></li>
          <li><a href="{{ page.base }}sdk/solutions/" class="button button-large button-white">SDK Solutions</a></li>
        </ul>
        <small>Fun fact, the <a href="{{ page.base }}studio/" target="_blank">Tripetto studio</a> and <a href="{{ page.base }}wordpress/" target="_blank">Tripetto WordPress plugin</a> are both built with this SDK.</small>
      </div>
      <div class="col-md-4 col-xl-6 sdk-components-hero-logos">
        <ul class="sdk-logos">
          <li><a href="{{ page.base }}sdk/plain-js/" title="Tripetto's FormBuilder SDK works in JavaScript">{% include icons/javascript.html %}</a></li>
          <li><a href="{{ page.base }}sdk/react/" title="Tripetto's FormBuilder SDK works in React">{% include icons/react.html %}</a></li>
          <li><a href="{{ page.base }}sdk/angular/" title="Tripetto's FormBuilder SDK works in Angular">{% include icons/angular.html %}</a></li>
          <li><a href="{{ page.base }}sdk/html/" title="Tripetto's FormBuilder SDK works in HTML5">{% include icons/html5.html %}</a></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="sdk-components-anchors">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-lg-6 col-xl-5">
        <ul class="anchors-icons">
          <li>
            <a href="#builder">
              {% include sdk-icon-chapter.html chapter='builder' size='big' name='Form Builder' %}
              <span>Form Builder</span>
            </a>
          </li>
          <li>
            <a href="#runners">
              {% include sdk-icon-chapter.html chapter='runners' size='big' name='Form Runners' %}
              <span>Form Runners</span>
            </a>
          </li>
          <li>
            <a href="#blocks">
              {% include sdk-icon-chapter.html chapter='blocks' size='big' name='Question Blocks' %}
              <span>Question Blocks</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section>
