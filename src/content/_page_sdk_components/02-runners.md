---
base: ../../
---

<section class="sdk-components-runners" id="runners">
  <div class="container">
    <div class="row content">
      <div class="col-sm-7 col-lg-9">
        <span class="caption">Embedding the form runners</span>
        <h2 class="palette-sdk-runners">Deploy forms in your app with the <span class="palette-inline">embedded runner.</span></h2>
      </div>
    </div>
    <div class="row content">
      <div class="col-lg-11">
        <p>The runner is for <strong>deploying forms in websites and applications.</strong> It turns a form definition into an executable program; a finite state machine that handles the complex logic and response collection during form execution. Choose any of our stock runners for your project.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-12 sdk-carousel-runners-buttons">
        <ul class="carousel-buttons">
          <li class="palette-autoscroll active" data-target="#carouselSDKRunners" data-slide-to="0">
            <div>
              {% include icon-face.html face='autoscroll' size='small' name='Autoscroll Form Face' template='sdk' radius='medium' %}
            </div>
            <div>
              <h3>Autoscroll<span> Face</span></h3>
              <p>Fluently presents <strong>one question at a time</strong>.</p>
            </div>
          </li>
          <li class="palette-chat" data-target="#carouselSDKRunners" data-slide-to="1">
            <div>
              {% include icon-face.html face='chat' size='small' name='Chat Form Face' template='sdk' radius='medium' %}
            </div>
            <div>
              <h3>Chat<span> Face</span></h3>
              <p>Presents all <strong>questions and answers as a chat</strong>.</p>
            </div>
          </li>
          <li class="palette-classic" data-target="#carouselSDKRunners" data-slide-to="2">
            <div>
              {% include icon-face.html face='classic' size='small' name='Classic Form Face' template='sdk' radius='medium' %}
            </div>
            <div>
              <h3>Classic<span> Face</span></h3>
              <p>Presents question fields in a <strong>traditional format</strong>.</p>
            </div>
          </li>
        </ul>
      </div>
    </div>
    <div class="row carousel-slides">
      <div class="col">
        <div id="carouselSDKRunners" class="carousel slide">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <div class="row">
                <div class="col-12">
                  <div class="sdk-device-code sdk-device-code-right">
                    <img src="{{ page.base }}images/sdk-scenes/runner-autoscroll.webp" width="2220" height="1435" alt="Screenshot of a fitness registration form in the autoscroll form face, shown on a tablet and a mobile phone." class="sdk-device-code-block sdk-device-code-device" loading="lazy" />
                    {% include sdk-code-snippet-runner-autoscroll.html id="runner-autoscroll" class="sdk-device-code-block sdk-device-code-snippet" %}
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="row">
                <div class="col-12">
                  <div class="sdk-device-code sdk-device-code-right">
                    <img src="{{ page.base }}images/sdk-scenes/runner-chat.webp" width="2220" height="1435" alt="Screenshot of a fitness registration form in the chat form face, shown on a tablet and a mobile phone." class="sdk-device-code-block sdk-device-code-device" loading="lazy" />
                    {% include sdk-code-snippet-runner-chat.html id="runner-chat" class="sdk-device-code-block sdk-device-code-snippet" %}
                  </div>
                </div>
              </div>
            </div>
            <div class="carousel-item">
              <div class="row">
                <div class="col-12">
                  <div class="sdk-device-code sdk-device-code-right">
                    <img src="{{ page.base }}images/sdk-scenes/runner-classic.webp" width="2220" height="1435" alt="Screenshot of a fitness registration form in the classic form face, shown on a tablet and a mobile phone." class="sdk-device-code-block sdk-device-code-device" loading="lazy" />
                    {% include sdk-code-snippet-runner-classic.html id="runner-classic" class="sdk-device-code-block sdk-device-code-snippet" %}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    {% include sdk-features-runners.html %}
    <div class="row">
      <div class="col-12"><a href="{{ page.base }}sdk/docs/runner/introduction/" class="hyperlink palette-sdk-runners"><span>Learn how to implement runners</span><i class="fas fa-arrow-right"></i></a></div>
    </div>
  </div>
</section>
