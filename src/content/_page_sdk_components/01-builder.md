---
base: ../../
---

<section class="sdk-components-builder" id="builder">
  <div class="container">
    <div class="row content">
      <div class="col-sm-7 col-lg-9">
        <span class="caption">Integrating the form builder</span>
        <h2 class="palette-sdk-builder">Build forms inside your app with the <span class="palette-inline">integrated builder.</span></h2>
      </div>
    </div>
    <div class="row content">
      <div class="col">
        <p>The visual builder is for creating advanced forms with logic and conditional flows on a magnetic storyboard. It can easily be <strong>tightly integrated into any website or app</strong> and works smoothly in mainstream browsers; mouse, touch or pen. Form structures are stored in a JSON-formatted form definition.</p>
      </div>
    </div>
    <div class="row">
      <div class="col sdk-components-builder-visual">
        <div class="sdk-device-code sdk-device-code-right">
          <img src="{{ page.base }}images/sdk-scenes/builder.webp" width="2000" height="1441" alt="Screenshot of the form builder." class="sdk-device-code-block sdk-device-code-device" loading="lazy" />
          {% include sdk-code-snippet-builder.html id="builder" class="sdk-device-code-block sdk-device-code-snippet" %}
        </div>
      </div>
    </div>
    {% include sdk-features-builder.html %}
    <div class="row">
      <div class="col-12"><a href="{{ page.base }}sdk/docs/builder/introduction/" class="hyperlink palette-sdk-builder"><span>Learn how to implement the builder</span><i class="fas fa-arrow-right"></i></a></div>
    </div>
  </div>
</section>
