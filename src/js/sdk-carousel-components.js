$(document).ready(function () {
    $("#carouselSDKComponents").carousel({
        interval: false,
        touch: false
    });

    $("#carouselSDKComponents").on("slid.bs.carousel", function () {
        $(".carousel-buttons li.active").removeClass("active");
        $(".carousel-buttons li[data-slide-to=" + $("div.carousel-item.active").index() + "]").addClass("active");
    });

    $(".carousel-buttons li").on("click", function () {
        $("#carouselSDKComponents").carousel(parseInt($(this).data("slide-to")));
        $(".carousel-buttons li.active").removeClass("active");
        $(this).addClass("active");
    });
});
