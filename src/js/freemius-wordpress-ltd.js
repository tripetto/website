var handler = new FS.Checkout({
    product_id: "3825",
    public_key: "pk_bbe3e39e20ddf86c6ff4721c5e30e",
    plan_id: "14293",
    name: "Tripetto WordPress Pro",
    title: "Tripetto Pro for WordPress",
    image: "https://tripetto.com/images/pricing/freemius_checkout.png",
    currency: "usd",
    billing_cycle: "lifetime",
    hide_billing_cycles: true,
    hide_coupon: true,
    disable_licenses_selector: true,
    checkout_style: "phase2",
    form_position: "right",
    show_upsells: false,
    show_reviews: false,
    show_refund_badge: false,
    always_show_renewals_amount: true,
    purchaseCompleted: function (response) {
        window.location.replace(
            "/wordpress/thank-you-ltd/"
        );
    }
});

var fnValidate = function (bInvalidate) {
    if (!bInvalidate && $("#freemius-license-key").val().length == 32 && $("#freemius-license-key").val().indexOf("sk_") == 0) {
        $("#freemius-license-validation").addClass("wordpress-ltd-instructions-license-valid").removeClass("wordpress-ltd-instructions-license-invalid").text("Valid license key");
        $(".button-purchase-pro-single, .button-purchase-pro-multi, .button-purchase-pro-unlimited").removeClass("button-disabled").tooltip('disable');
    } else {
        $("#freemius-license-validation").addClass("wordpress-ltd-instructions-license-invalid").removeClass("wordpress-ltd-instructions-license-valid").text(bInvalidate ? "" : "Invalid license key");
        $(".button-purchase-pro-single, .button-purchase-pro-multi, .button-purchase-pro-unlimited").addClass("button-disabled").tooltip('enable');
    }
};

$(document).ready(function () {
    fnValidate(true);

    $(".button-purchase-pro-single").on("click", function (e) {
        if (!$(this).hasClass("button-disabled")) {
            handler.open({
                licenses: 1,
                license_key: document.getElementById("freemius-license-key").value
            });
            e.preventDefault();
        }
    });

    $(".button-purchase-pro-multi").on("click", function (e) {
        if (!$(this).hasClass("button-disabled")) {
            handler.open({
                licenses: 5,
                license_key: document.getElementById("freemius-license-key").value
            });
            e.preventDefault();
        }
    });

    $(".button-purchase-pro-unlimited").on("click", function (e) {
        if (!$(this).hasClass("button-disabled")) {
            handler.open({
                licenses: "unlimited",
                license_key: document.getElementById("freemius-license-key").value
            });
            e.preventDefault();
        }
    });

    $("#freemius-license").on("submit", function (e) {
        e.preventDefault();
        fnValidate();
    });

    $("#freemius-license-key").on("paste keyup change", function(e) {
        fnValidate(true);
    });
});
