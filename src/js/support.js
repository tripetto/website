$(document).ready(function () {
    $("#support-popup").toast({
        autohide: false,
    });

    $("#support-bubble").on("click", function () {
        var bIsHidden = $("#support-popup").hasClass("hide");

        $("#support-popup").toast(bIsHidden ? "show" : "hide");

        if (bIsHidden) {
            $(this).addClass("support-bubble-opened");
        } else {
            $(this).removeClass("support-bubble-opened");
        }
    });
});
