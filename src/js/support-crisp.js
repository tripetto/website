$(document).ready(function () {
    var crispSession = (typeof URLSearchParams !== "undefined" && new URLSearchParams(window.location.search).get("crisp_sid")) || undefined;

    if (crispSession && crispSession != "" && crispSession.indexOf("session_") > -1) {
        var crispChat = document.createElement("iframe");
        crispChat.src = "https://go.crisp.chat/chat/embed/?website_id=ea3a0b7a-6fe7-4de2-bd85-7f7656da680b&crisp_sid=" + encodeURIComponent(crispSession);

        $("#SupportCrisp").append(crispChat);
    } else {
        $("#SupportCrisp").append("<p>You don't have access to Tripetto's priority support.</p>");
    }
});
