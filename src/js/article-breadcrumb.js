$(document).ready(function() {
    var sArea = window.localStorage.getItem("tripetto_help_area");

    if (sArea == "studio" || sArea == "wordpress") {
        var sAreaName = "";
        var sAreaURL = $("#help_breadcrumb_area_link").attr("href");
        var sHelpURL = $("#help_breadcrumb_help").attr("href");

        switch (sArea) {
            case "studio":
                sAreaName = "Tripetto studio";
                sAreaURL = sAreaURL + "studio/";
                sHelpURL = sHelpURL.replace("help", "studio/help");
                break;
            case "wordpress":
                sAreaName = "Tripetto WordPress plugin";
                sAreaURL = sAreaURL + "wordpress/";
                sHelpURL = sHelpURL.replace("help", "wordpress/help");
                break;
        }

        $("#help_breadcrumb_area_link").text(sAreaName);
        $("#help_breadcrumb_area_link").attr("href", sAreaURL);
        $("#help_breadcrumb_area_item").removeClass("d-none");
        $("#help_breadcrumb_help").attr("href", sHelpURL);

        if (sArea == "studio") {
            $(".help_article_wp_only").addClass("d-none");
        }

        if (sArea == "wordpress") {
            $(".help_article_studio_only").addClass("d-none");
        }
    }
});
