var handler = new FS.Checkout({
    product_id: "3825",
    public_key: "pk_bbe3e39e20ddf86c6ff4721c5e30e",
    plan_id: "14293",
    name: "Tripetto WordPress Pro",
    title: "Tripetto Pro for WordPress",
    image: "https://tripetto.com/images/pricing/freemius_checkout.png",
    currency: "usd",
    billing_cycle: "annual",
    hide_billing_cycles: true,
    hide_coupon: true,
    disable_licenses_selector: true,
    coupon: (typeof URLSearchParams !== "undefined" && new URLSearchParams(window.location.search).get("coupon")) || undefined,
    checkout_style: "phase2",
    form_position: "right",
    show_upsells: false,
    show_reviews: false,
    show_refund_badge: true,
    always_show_renewals_amount: true
});

$(document).ready(function() {
    $("#proButton").on("click", function(e) {
        if ($(this).hasClass("button-purchase-pro-single")) {
            handler.open({
                licenses: 1,
                purchaseCompleted: function(response) {
                    window.location.replace("/wordpress/thank-you-pro-single-site/");
                }
            });
            e.preventDefault();
        } else if ($(this).hasClass("button-purchase-pro-multi")) {
            handler.open({
                licenses: 5,
                purchaseCompleted: function(response) {
                    window.location.replace("/wordpress/thank-you-pro-5-sites/");
                }
            });
            e.preventDefault();
        } else if ($(this).hasClass("button-purchase-pro-unlimited")) {
            handler.open({
                licenses: "unlimited",
                purchaseCompleted: function(response) {
                    window.location.replace("/wordpress/thank-you-pro-unlimited-sites/");
                }
            });
            e.preventDefault();
        }
    });
});
