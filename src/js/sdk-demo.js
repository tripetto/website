$(document).ready(function () {
    Tripetto.Builder.open({"name":"FormBuilder SDK Demo","language":"en","sections":[{"id":"85c608dbe2bebc67fdd10874dec2b0a4031c76206cd94a7de788394f7016dd0f","name":"Let's meet","nodes":[{"id":"b13201e0d55c3f320101d94ea50ea2871a7df8ed3779da18e3f97fe43ba48547","explanation":"Nice to meet you, @6c9bc2603311fd4af624fe91d7930a667d890cf19be022c5d150ab7081a7dd09! 🤝","name":"👋 Hi, what's your name?","nameVisible":true,"slots":[{"id":"6c9bc2603311fd4af624fe91d7930a667d890cf19be022c5d150ab7081a7dd09","type":"text","kind":"static","reference":"value","label":"Text","alias":"Name"}],"block":{"type":"@tripetto/block-text","version":"6.0.2"}}]}],"builder":{"name":"@tripetto/builder","version":"6.2.1"}}, {
        element: document.getElementById("builder"),
        fonts: "../../sdk-demo/fonts/",
        license: "KwBvJgt8X8guGUOcAHo+wM/fvUU85bwe2NCIdRXc1zqA8wN29XueYk99Go5wDWP3FSlXEQW8QOKwP33743FtMOZ7ZeRI1aPbXvebMV26fcVU+R2dMx0JAOB8nvV3tp2fSmQy7hS62+zOCAx9D0L2faA37xtoW445jPWbJCr3wNm0spE2YtvIX6C1vOyA+Yw6fzIpDHF5+uAk1pl68yD6BwW23/hXr9m2XwLLuZG5p4ZlGSB15ezbxjc1fuUWHeu59mwYtabdU24T6uz1jspHW7Y5KLtLEVEdbgSofJV6+p8mJca2iz3RlaF8qL/3liEawxj6Qyp8WIknkE50FGoKB+w4jcxM8TV3dzUfgM56DSWYM4ObDjWoPwG26C2x9Y5dzE+jDFc3il6dIDj5znAJiWG0m4HDsSEclniD16v8/HqYnPT5AApv4Cf1r0zuqhjbE3cluOk7f/HH23T5dvNAwck4n9u8EIrb3lT1AlYc+ElDP1Q4SJkKZhb2HZzQsQU5UuZjS8nt7xBzt7DkfuWnGdMhhTNJxha1wpBtCv1f",
        onReady: function(builder) {
            document.getElementById("builderLoader").style.display = "none";
            prettier.format(JSON.stringify(builder.definition), { parser: "json", plugins: prettierPlugins }).then(function(formatted) {
                document.getElementById("definitionParser").style.display = formatted === "" ? "none" : "block";
                document.getElementById("definitionEmpty").style.display = formatted !== "" ? "none" : "flex";
                document.getElementById("definition").textContent = formatted;
                Prism.highlightElement(document.getElementById("definition"));
            });
            TripettoAutoscroll.run({
                element: document.getElementById("runnerAutoscroll"),
                definition: builder.definition,
                view: "test",
                builder: builder,
                onReady: function() {
                    document.getElementById("runnerAutoscrollLoader").style.display = "none";
                },
                onChange: function(instance) {
                    var NVPs = JSON.stringify(TripettoRunner.Export.NVPs(instance));

                    prettier.format(NVPs, { parser: "json", plugins: prettierPlugins, printWidth: 1 }).then(function(formatted) {
                        document.getElementById("runnerAutoscrollResponseParser").style.display = NVPs === "" || NVPs === "{}" ? "none" : "block";
                        document.getElementById("runnerAutoscrollResponseEmpty").style.display = NVPs !== "" && NVPs !== "{}" ? "none" : "flex";
                        document.getElementById("runnerAutoscrollResponse").textContent = formatted;
                        Prism.highlightElement(document.getElementById("runnerAutoscrollResponse"));
                    });
                }
            });
            TripettoChat.run({
                element: document.getElementById("runnerChat"),
                definition: builder.definition,
                view: "test",
                builder: builder,
                onReady: function() {
                    document.getElementById("runnerChatLoader").style.display = "none";
                },
                onChange: function(instance) {
                    var NVPs = JSON.stringify(TripettoRunner.Export.NVPs(instance));

                    prettier.format(NVPs, { parser: "json", plugins: prettierPlugins, printWidth: 1 }).then(function(formatted) {
                        document.getElementById("runnerChatResponseParser").style.display = NVPs === "" || NVPs === "{}" ? "none" : "block";
                        document.getElementById("runnerChatResponseEmpty").style.display = NVPs !== "" && NVPs !== "{}" ? "none" : "flex";
                        document.getElementById("runnerChatResponse").textContent = formatted;
                        Prism.highlightElement(document.getElementById("runnerChatResponse"));
                    });
                }
            });
            TripettoClassic.run({
                element: document.getElementById("runnerClassic"),
                definition: builder.definition,
                view: "test",
                builder: builder,
                onReady: function() {
                    document.getElementById("runnerClassicLoader").style.display = "none";
                },
                onChange: function(instance) {
                    var NVPs = JSON.stringify(TripettoRunner.Export.NVPs(instance));

                    prettier.format(NVPs, { parser: "json", plugins: prettierPlugins, printWidth: 1 }).then(function(formatted) {
                        document.getElementById("runnerClassicResponseParser").style.display = NVPs === "" || NVPs === "{}" ? "none" : "block";
                        document.getElementById("runnerClassicResponseEmpty").style.display = NVPs !== "" && NVPs !== "{}" ? "none" : "flex";
                        document.getElementById("runnerClassicResponse").textContent = formatted;
                        Prism.highlightElement(document.getElementById("runnerClassicResponse"));
                    });
                }
            });
        },
        onChange: function(definition) {
            prettier.format(JSON.stringify(definition), { parser: "json", plugins: prettierPlugins }).then(function(formatted) {
                document.getElementById("definition").textContent = formatted;
                Prism.highlightElement(document.getElementById("definition"));
            });
        },
        disableSaveButton: true,
        disableCloseButton: true,
        disablePrologue: true,
        disableEpilogue: true,
        controls: "left"
    });

    $('.flip-button').bind("click", function() {
        var target = "#"+$(this).data("target");

        $(target).toggleClass('flipped');
        $(this).find("i").toggleClass("fa-magnifying-glass");
        $(this).find("i").toggleClass("fa-rotate-left");
        $(this).find("span").text($(target).hasClass('flipped') ? $(this).data("back") : "View code");
    });
});
