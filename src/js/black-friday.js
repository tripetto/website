var countdownUTC = new Date("Dec 2, 2024 23:59:59").getTime();

var x = setInterval(function() {
    var nowLocal = new Date();
    var nowUTC = new Date(nowLocal.getTime() + nowLocal.getTimezoneOffset() * 60000);
    var distance = countdownUTC - nowUTC;
    var labelDays = "";
    var labelHours = "";
    var labelMinutes = "";
    var labelSeconds = "";

    if (distance <= 0) {
        clearInterval(x);

        labelDays = "--";
        labelHours = "--";
        labelMinutes = "--";
        labelSeconds = "--";
    } else {
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        labelDays = (days < 10 ? "0" : "") + days;
        labelHours = (hours < 10 ? "0" : "") + hours;
        labelMinutes = (minutes < 10 ? "0" : "") + minutes;
        labelSeconds = (seconds < 10 ? "0" : "") + seconds;
    }

    document.getElementById("countdown_d").innerHTML = labelDays;
    document.getElementById("countdown_h").innerHTML = labelHours;
    document.getElementById("countdown_m").innerHTML = labelMinutes;
    document.getElementById("countdown_s").innerHTML = labelSeconds;
}, 1000);
