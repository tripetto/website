$(document).ready(function () {
    var header = $("#header");
    var offset = $(".notification").length && !$(".breadcrumb-navigation").length ? 65 : 1;

    if (header.length) {
        var fnScroll = function () {
            requestAnimationFrame(function () {
                if ($(this).scrollTop() >= offset) {
                    header.addClass("background");
                } else {
                    header.removeClass("background");
                }
            });
        };

        $(document).scroll(fnScroll);
        $(document).ready(fnScroll);
    }

    $(".header-vertical nav .dropdown").on("click", function() {
        if (!$(this).find(".dropdown-menu").hasClass("show")) {
            $(".header-vertical nav .dropdown-menu").not(this).removeClass('show');
            $(".header-vertical nav .dropdown i.dropdown-mobile").not(this).removeClass('dropdown-mobile-active');
        }

        $(this).find(".dropdown-menu").toggleClass('show');
        $(this).find("i.dropdown-mobile").toggleClass('dropdown-mobile-active');
    });

    $(".header-horizontal nav .dropdown").on("mouseover", function() {
        $(this).find(".dropdown-menu").addClass('show');
    });

    $(".header-horizontal nav .dropdown").on("mouseout", function() {
        $(this).find(".dropdown-menu").removeClass('show');
    });

    $(".hamburger").on("click", function() {
        $(this).toggleClass('opened');
        $("#header").toggleClass('opened');
        $("#navOverlay").toggleClass('opened');
    });
});
