function play(div) {
    var iframe = document.createElement("iframe");
    iframe.setAttribute("src", "https://www.youtube.com/embed/" + div.dataset.id + "?autoplay=1&color=white");
    iframe.setAttribute("frameborder", "0");
    iframe.setAttribute("allowfullscreen", "1");
    iframe.setAttribute("allow", "accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture");

    div.parentNode.replaceChild(iframe, div);
}

$(document).ready(function () {
    var players = document.querySelectorAll(".youtube-player");

    for (var n = 0; n < players.length; n++) {
        var id = players[n].dataset.id;

        var div = document.createElement("div");
        div.setAttribute("data-id", id);

        var thumbnail = document.createElement("img");
        thumbnail.src = "https://i.ytimg.com/vi/ID/sddefault.jpg".replace("ID", id);
        thumbnail.width = 640;
        thumbnail.height = 480;
        thumbnail.alt = players[n].nextElementSibling.textContent;
        div.appendChild(thumbnail);

        var button = document.createElement("div");
        button.setAttribute("class", "play");
        div.appendChild(button);

        div.onclick = function () {
            play(this);
        };

        players[n].appendChild(div);
    }
});
