var handler = new FS.Checkout({
    product_id: "9142",
    public_key: "pk_8cdcd6a3e86b8c09fdc40bc6dfc88",
    plan_id: "15336",
    name: "Tripetto Studio Unlock 1",
    subtitle: "Checkout for your form upgrade",
    image: "https://tripetto.com/images/pricing/freemius_checkout.png",
    currency: "usd",
    billing_cycle: "lifetime",
    hide_licenses: true,
    hide_billing_cycles: true,
    hide_coupon: true,
    hide_license_key: true,
    coupon: (typeof URLSearchParams !== "undefined" && new URLSearchParams(window.location.search).get("coupon")) || undefined,
    user_email: (typeof URLSearchParams !== "undefined" && new URLSearchParams(window.location.search).get("email")) || undefined,
    checkout_style: "phase2",
    fullscreen: true,
    layout: "vertical",
    form_position: "right",
    show_upsells: false,
    show_reviews: false,
    show_refund_badge: false,
    purchaseCompleted: function() {
        window.open("https://tripetto.com/studio/checkout-confirmation/", "_self");
    }
});

$(document).ready(function() {
    handler.open({
        licenses: 1
    });
});
