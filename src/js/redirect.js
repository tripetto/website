// Workaround for this issue: https://issuetracker.google.com/issues/151713401
if (typeof window !== "undefined") {
    var url = window.location.href;

    if (url.includes("index.html")) {
        window.location.href = url.replace("index.html", "");
    }
}
