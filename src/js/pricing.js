$(document).ready(function () {
    $(".dropdown-item").each(function () {
        $(this).on("click", function () {
            $("#proButton").removeClass("button-purchase-pro-single").removeClass("button-purchase-pro-multi").removeClass("button-purchase-pro-unlimited").addClass($(this).data("button"));
            $("#proPrice").html($(this).data("price"));
            $("#proName").html($(this).data("name"));
            $("#proCondition").html($(this).data("condition"));
            $("#proDescription").html($(this).data("description"));
        });
    });
});
