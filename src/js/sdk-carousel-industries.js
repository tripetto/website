$(document).ready(function () {
    $("#carouselSDKIndustries").carousel({
        interval: 7500,
        pause: "hover",
        ride: "carousel",
        touch: false
    });

    $("#carouselSDKIndustries").on("slid.bs.carousel", function () {
        $(".carousel-buttons li.active").removeClass("active");
        $(".carousel-buttons li[data-slide-to=" + $("div.carousel-item.active").index() + "]").addClass("active");
    });

    $(".carousel-buttons li").on("click", function () {
        $("#carouselSDKIndustries").carousel(parseInt($(this).data("slide-to")));
        $(".carousel-buttons li.active").removeClass("active");
        $(this).addClass("active");
    });
});
